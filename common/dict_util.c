/*
 * dict_util.c
 *
 *  Created on: 5 mars 2012
 *      Author: oliviers
 */


#include "processor.h"

#ifdef PROC_COM
  #include "uC.h"
  //SYS/BIOS
  #include <ti/sysbios/BIOS.h>
  #include <xdc/runtime/System.h>
  #include <xdc/runtime/Log.h>
  #include <ti/sysbios/knl/Mailbox.h>
  #include <ti/sysbios/knl/Semaphore.h>
  #include <ti/sysbios/knl/Task.h>
  #include <ti/sysbios/knl/Clock.h>
  #include <xdc/cfg/global.h> //for sys/bios variables

  #include "inc/hw_memmap.h"
  #include "inc/hw_types.h"
  #include "driverlib/sysctl.h"
#endif

#ifdef PROC_DSP
  #include "mmscfg.h"    //must be include before uC.h
  #include "uC.h"
  #include <clk.h>
  //SysBios to DSPBios translation macros
  #define Mailbox_post(mbox, msg, t)   MBX_post(&mbox, msg, t)
  #define Mailbox_pend(mbox, msg, t)   MBX_pend(&mbox, msg, t)
  #define Semaphore_post(sem)          SEM_post(&sem)
  #define Semaphore_pend(sem,t)        SEM_pend(&sem, t)
  #define Task_sleep(t)                TSK_sleep(t)
  #define Task_exit()                  TSK_exit(t)
  #define BIOS_WAIT_FOREVER            SYS_FOREVER
  #define Clock_getTicks()             CLK_getltime()
#endif

//object dictionary
#include <co_data.h>
#include "dict_util.h"


UNS8 DIC_NodeId = 1;
bool1 WriteDictEnabled(UNS16 wIndex, UNS8 bSubindex){
  return TRUE;
}

void DIC_SetNodeId(UNS8 fnodeId){
  DIC_NodeId = fnodeId;
}

bool1 DIC_CheckEqual(uint16 index, uint8 subindex, uint16 index2, uint8 subindex2){
  return ((index == index2) && (subindex == subindex2));
}

//******************** functions and vars for reading local dict ******************
bool1 DIC_ReadLocalDict(UNS16 wIndex, UNS8 bSubindex) { //, void *data
  UNS32 size = 0;
  UNS8 data_type;
  UNS32 errorCode;
  void * pdest_data = 0;

  errorCode = _getODentry(BoardODdata, wIndex, bSubindex, &pdest_data, &size,
      &data_type, 0,1,GET_OD_ENTRY_MODE_0);
  return(errorCode == OD_SUCCESSFUL);
}


bool1 DIC_WriteLocalDict(UNS16 wIndex, UNS8 bSubindex) {
  UNS32 size = 0;
  //UNS8 data_type;
  UNS32 errorCode;
  void * pdest_data = 0;
  if(WriteDictEnabled(wIndex, bSubindex)){
    errorCode = _setODentry(BoardODdata, wIndex, bSubindex, &pdest_data, &size, 1,0,SET_OD_ENTRY_MODE_2);
    return(errorCode == OD_SUCCESSFUL);
  }
  else{
    return(FALSE);
  }
}


//******************** functions for reading the network dict ******************
#define DIC_ACCESS_RETRY    3
#define SDO_RESULT_TIMEOUT  SDO_TIMEOUT_MS+2

bool1 DIC_ReadNetworkDict(UNS8 nodeid, UNS16 wIndex, UNS8 bSubindex) {
  UNS8 statuscan = SDO_PENDING;
  UNS8 i = 0;
  bool1 res = FALSE;
  UNS32 start;
  while ((i<DIC_ACCESS_RETRY) && !res){
    if (DIC_ReadNetworkDict2(nodeid,wIndex,bSubindex,bSubindex,&statuscan)){
      start = Clock_getTicks();
      while ((statuscan <= SDO_PROCESSING) && (Clock_getTicks() - start < SDO_RESULT_TIMEOUT)){
        Task_sleep(1);
      }
      if ((statuscan == SDO_READ_OK))
        res = TRUE;
    }
    ++i;
  }
  return res;
}

bool1 DIC_ReadNetworkDictTimeout(UNS16 wIndex, UNS8 bSubindex, UNS16 timeout) {
  UNS8 statuscan = SDO_PENDING;
  bool1 res = FALSE;
  UNS32 start;
  if (DIC_ReadNetworkDict2(DIC_NodeId,wIndex,bSubindex,bSubindex,&statuscan)){
    start = Clock_getTicks();
    while ((statuscan <= SDO_PROCESSING) && (Clock_getTicks() - start < timeout)){
      Task_sleep(1);
    }
    if ((statuscan == SDO_READ_OK))
      res = TRUE;
  }
  return res;
}

//******************** function for writing to network dict ******************
bool1 DIC_WriteNetworkDict(UNS8 nodeid, UNS16 wIndex, UNS8 bSubindex) {
  UNS8 statuscan = SDO_PENDING;
  UNS8 i = 0;
  bool1 res = FALSE;
  UNS32 start = Clock_getTicks();
  if(WriteDictEnabled(wIndex, bSubindex)){
    while ((i<DIC_ACCESS_RETRY) && !res){
      if (DIC_WriteNetworkDict2(nodeid,wIndex,bSubindex,bSubindex,&statuscan)){
        while ((statuscan <= SDO_PROCESSING) && (Clock_getTicks() - start < SDO_RESULT_TIMEOUT)){
          Task_sleep(1);
        }
        if ((statuscan == SDO_WRITE_OK))
          res = TRUE;
      }
      ++i;
    }
  }
  return res;
}

//global callback
UNS32 DIC_GlobalDictCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access) {
  UNS32 error = OD_SUCCESSFUL;
  UNS8 line;
  if (getSDOlineOnUseFromIndex(d,indextable->index, bSubindex,&line)==0){
    //true if called by external can
    if (access == DIC_READ) {
      if (!DIC_ReadNetworkDict(DIC_NodeId, indextable->index, bSubindex))
        error = SDOABT_LOCAL_CTRL_ERROR;
    }
    else if (access == DIC_WRITE) {
      if (!DIC_WriteNetworkDict(DIC_NodeId, indextable->index, bSubindex))
        error = SDOABT_LOCAL_CTRL_ERROR;
    }
    else {
      // should not go here ...
      error = SDOABT_LOCAL_CTRL_ERROR;
    }
  }
  else{
    //we are r/wing local dict don't want to update value from another dict
    error = OD_SUCCESSFUL;
  }
  return(error);
}

//*************************functions and vars for r/w any dictionary **********************
typedef struct{
  void *pdata;     //pointer to local data
  UNS32 size;      //size of data
  UNS8 *status;    //a pointer for returning the status of the can operation !!pointer are aligned on multiple of 32bits address
  UNS8  dataType;  //type of data
  UNS8  node_id;   //remote node id
  UNS16 index;     //remote index
  UNS8  subindex;  //remote subindex
  UNS8  access;    //DIC_READ or DIC_WRITE
}TDicMessage; //total size in MADUs for mailbox: DSP 11 words, ARM 18 bytes

bool1 SDO_Successful = FALSE;

/* =============================== SDO READ DSP DICTIONARY ================================== */

void *Read_pdata;
UNS8 Dummy_Status;

bool1 DIC_ReadNetworkDict2(UNS8 node_id, UNS16 wIndex, UNS8 bLocalSubindex, UNS8 bSubindex, UNS8 *status) {
  //this variant scans the own local dictionary to find the data pointer, size and type (slower)
  TDicMessage msg;
  msg.size = 0; //to get the actual size
  //get the data pointer in own dictionary for storing the returned data
  if (_getODentry(BoardODdata, wIndex, bLocalSubindex, &msg.pdata, &msg.size,
      &msg.dataType, 0, 1, GET_OD_ENTRY_MODE_4) == OD_SUCCESSFUL) {
    msg.node_id = node_id;
    msg.index = wIndex;
    msg.subindex = bSubindex;
    msg.access = DIC_READ;
    msg.status = status;
    return Mailbox_post(mailboxSDOout,&msg,0); //post the message into the mailbox
  }
  else //couldn't find entry in local OD
    return FALSE;
}

bool1 DIC_ReadNetworkDictDirect(void *pdata, UNS32 size, UNS8 dataType, UNS8 node_id, UNS16 wIndex, UNS8 bSubindex,UNS8 *status) {
  //this direct variant uses data pointer, size and type passed as arguments and is faster (no dictionary scan)
  TDicMessage msg;
  msg.pdata = pdata;
  msg.size = size;
  msg.dataType = dataType;
  msg.node_id = node_id;
  msg.index = wIndex;
  msg.subindex = bSubindex;
  msg.access = DIC_READ;
  msg.status = status;
  return Mailbox_post(mailboxSDOout,&msg,0); //post the message into the mailbox
}

bool1 DIC_ReadNetworkDictDirect2(void *pdata, UNS32 size, UNS8 dataType, UNS8 node_id, UNS16 wIndex, UNS8 bSubindex, UNS8* status) {
  //this direct variant uses data pointer, size and type passed as arguments and is faster (no dictionary scan)
  TDicMessage msg;
  msg.pdata = pdata;
  msg.size = size;
  msg.dataType = dataType;
  msg.node_id = node_id;
  msg.index = wIndex;
  msg.subindex = bSubindex;
  msg.access = DIC_READ;
  msg.status = status;
  return Mailbox_post(mailboxSDOout,&msg,0); //post the message into the mailbox
}

void AfterReadDict_Callback(CO_Data* d, UNS8 nodeId){
  UNS32 abortCode;
  UNS32 size = 0;

  if(getReadResultNetworkDict(d, nodeId, Read_pdata, &size, &abortCode) == SDO_FINISHED){
    if (abortCode == OD_SUCCESSFUL) SDO_Successful = TRUE;
  }
  closeSDOtransfer(d, nodeId, SDO_CLIENT) ;
  Semaphore_post(semaphoreSDOdone);
}

/* =============================== SDO WRITE DSP DICTIONARY ================================== */

bool1 DIC_WriteNetworkDict2(UNS8 node_id, UNS16 wIndex, UNS8 bLocalSubindex, UNS8 bSubindex, UNS8 *status) {
  //this variant scans the own local dictionary to find the data pointer, size and type (slower)
  TDicMessage msg;
  msg.size = 0; //to get the actual size
  //get the data pointer in own dictionary for storing the returned data
  if (_getODentry(BoardODdata, wIndex, bLocalSubindex, &msg.pdata, &msg.size,
      &msg.dataType, 0, 1, GET_OD_ENTRY_MODE_4) == OD_SUCCESSFUL) {
    msg.node_id = node_id;
    msg.index = wIndex;
    msg.subindex = bSubindex;
    msg.access = DIC_WRITE;
    msg.status = status;
    return Mailbox_post(mailboxSDOout,&msg,0); //post the message into the mailbox
  }
  else //couldn't find entry in local OD
    return FALSE;
}

bool1 DIC_WriteNetworkDictDirect(void *pdata, UNS32 size, UNS8 dataType, UNS8 node_id, UNS16 wIndex, UNS8 bSubindex, UNS8 *status) {
  //this direct variant uses data pointer, size and type passed as arguments and is faster (no dictionary scan)
  TDicMessage msg;
  msg.pdata = pdata;
  msg.size = size;
  msg.dataType = dataType;
  msg.node_id = node_id;
  msg.index = wIndex;
  msg.subindex = bSubindex;
  msg.access = DIC_WRITE;
  msg.status = status;
  return Mailbox_post(mailboxSDOout,&msg,0);  //post the message into the mailbox
}

void AfterWriteDict_Callback(CO_Data* d, UNS8 nodeId){
  UNS32 abortCode;

  if(getWriteResultNetworkDict(d, nodeId, &abortCode) == SDO_FINISHED){
    if (abortCode == OD_SUCCESSFUL) SDO_Successful = TRUE;
  }
  closeSDOtransfer(d, nodeId, SDO_CLIENT);
  Semaphore_post(semaphoreSDOdone);
}


/* ==================== TASK : HANDLE THE SENDING OF READ/WRITE SDOs ===========================
 *  look for messages in the mailbox and sends them on the CAN bus.
 *  Retry if the operation fails
 */

bool1 StartSDOTransaction(TDicMessage *msg) {
  UNS32 errorCode;
#ifdef PROC_COM
  CAN_PORT port = BoardODdata->canHandle; //remember original port
  BoardODdata->canHandle = CAN_PORT_INTERNAL;
#endif
  if (msg->access == DIC_READ) {
    //set the data pointer used by the callback to update the data
    Read_pdata = msg->pdata;
    errorCode = readNetworkDictCallback(BoardODdata, msg->node_id, msg->index, msg->subindex, msg->dataType,
        AfterReadDict_Callback, msg->pdata);
  }
  else { //DIC_WRITE
    errorCode = writeNetworkDictCallBack(BoardODdata, msg->node_id, msg->index, msg->subindex, msg->size, msg->dataType,
        msg->pdata, AfterWriteDict_Callback);
  }
#ifdef PROC_COM
  BoardODdata->canHandle = port;  //restore original port
#endif
  return(errorCode == OD_SUCCESSFUL);
}

#define SDO_MAX_TRIALS    0
#define SDO_RETRY_DELAY  10  //ms

void taskFnMailboxSDOout(void){
  TDicMessage msg;
  uint8 trials;

  while(1){
    Mailbox_pend(mailboxSDOout,&msg,BIOS_WAIT_FOREVER); //wait for message in the mailbox
    /* now try to send it:
     * if operation succeeds, SDO_Successful will be set to TRUE by AfterRead(Write)DspDict_Callback */
    SDO_Successful = FALSE; trials = 0;
    *msg.status = SDO_PROCESSING;
    do {
      Semaphore_pend(semaphoreSDOdone, 0); //clear the semaphore if set
      if (StartSDOTransaction(&msg)){
        //message was send, now wait for acknowledge and data (AfterRead(Write)Dict_Callback)
        Semaphore_pend(semaphoreSDOdone, SDO_TIMEOUT_MS+2); //wait a little longer than the SDO_TIMEOUT
      }
      else //message could not be send on the bus (busy bus)
        Task_sleep(SDO_RETRY_DELAY);  //wait a bit before trying again
      //and give other tasks (with priority <=) the opportunity to run
      //now check if the Callback was called and the SDO transaction was OK
    }while (!SDO_Successful && ++trials <= SDO_MAX_TRIALS);
    if (SDO_Successful){
      if (msg.access == DIC_READ) *msg.status = SDO_READ_OK;
      else *msg.status = SDO_WRITE_OK;
    }
    else {
      if (msg.access == DIC_READ) *msg.status = SDO_READ_FAILED;
      else *msg.status = SDO_WRITE_FAILED;
    }
  }
}

