unit mms_dict;

interface

uses
 Def;

const

mms_dictDictionaryParam: array[0..411] of TDictionaryParam =
((name: 'Device Type';index: $1000; subindex: 0; fsize: 4 ) //0
,(name: 'Error Register';index: $1001; subindex: 0; fsize: 1 ) //1
,(name: 'SYNC COB ID';index: $1005; subindex: 0; fsize: 4 ) //2
,(name: 'Communication Cycle Period';index: $1006; subindex: 0; fsize: 4 ) //3
,(name: 'Manufacturer Software Version';index: $100A; subindex: 0; fsize: 2 ) //4
,(name: 'Emergency COB ID';index: $1014; subindex: 0; fsize: 4 ) //5
,(name: 'Identity_Vendor ID';index: $1018; subindex: 1; fsize: 4 ) //6
,(name: 'Identity_Product Code';index: $1018; subindex: 2; fsize: 4 ) //7
,(name: 'Identity_Revision Number';index: $1018; subindex: 3; fsize: 4 ) //8
,(name: 'Identity_Serial Number';index: $1018; subindex: 4; fsize: 4 ) //9
,(name: 'Server SDO Parameter_COB ID Client to Server (Receive SDO)';index: $1200; subindex: 1; fsize: 4 ) //10
,(name: 'Server SDO Parameter_COB ID Server to Client (Transmit SDO)';index: $1200; subindex: 2; fsize: 4 ) //11
,(name: 'Client SDO 1 Parameter_COB ID Client to Server (Transmit SDO)';index: $1280; subindex: 1; fsize: 4 ) //12
,(name: 'Client SDO 1 Parameter_COB ID Server to Client (Receive SDO)';index: $1280; subindex: 2; fsize: 4 ) //13
,(name: 'Client SDO 1 Parameter_Node ID of the SDO Server';index: $1280; subindex: 3; fsize: 1 ) //14
,(name: 'Receive PDO 1 Parameter_COB ID used by PDO';index: $1400; subindex: 1; fsize: 4 ) //15
,(name: 'Receive PDO 1 Parameter_Transmission Type';index: $1400; subindex: 2; fsize: 1 ) //16
,(name: 'Receive PDO 1 Parameter_Inhibit Time';index: $1400; subindex: 3; fsize: 2 ) //17
,(name: 'Receive PDO 1 Parameter_Compatibility Entry';index: $1400; subindex: 4; fsize: 1 ) //18
,(name: 'Receive PDO 1 Parameter_Event Timer';index: $1400; subindex: 5; fsize: 2 ) //19
,(name: 'Receive PDO 1 Mapping_PDO 1 Mapping for an application object 1';index: $1600; subindex: 1; fsize: 4 ) //20
,(name: 'Receive PDO 1 Mapping_PDO 1 Mapping for an application object 2';index: $1600; subindex: 2; fsize: 4 ) //21
,(name: 'Receive PDO 1 Mapping_PDO 1 Mapping for an application object 3';index: $1600; subindex: 3; fsize: 4 ) //22
,(name: 'Transmit PDO 1 Parameter_COB ID used by PDO';index: $1800; subindex: 1; fsize: 4 ) //23
,(name: 'Transmit PDO 1 Parameter_Transmission Type';index: $1800; subindex: 2; fsize: 1 ) //24
,(name: 'Transmit PDO 1 Parameter_Inhibit Time';index: $1800; subindex: 3; fsize: 2 ) //25
,(name: 'Transmit PDO 1 Parameter_Compatibility Entry';index: $1800; subindex: 4; fsize: 1 ) //26
,(name: 'Transmit PDO 1 Parameter_Event Timer';index: $1800; subindex: 5; fsize: 2 ) //27
,(name: 'Transmit PDO 2 Parameter_COB ID used by PDO';index: $1801; subindex: 1; fsize: 4 ) //28
,(name: 'Transmit PDO 2 Parameter_Transmission Type';index: $1801; subindex: 2; fsize: 1 ) //29
,(name: 'Transmit PDO 2 Parameter_Inhibit Time';index: $1801; subindex: 3; fsize: 2 ) //30
,(name: 'Transmit PDO 2 Parameter_Compatibility Entry';index: $1801; subindex: 4; fsize: 1 ) //31
,(name: 'Transmit PDO 2 Parameter_Event Timer';index: $1801; subindex: 5; fsize: 2 ) //32
,(name: 'Transmit PDO 1 Mapping_PDO 1 Mapping for a process data variable 1';index: $1A00; subindex: 1; fsize: 4 ) //33
,(name: 'Transmit PDO 1 Mapping_PDO 1 Mapping for a process data variable 2';index: $1A00; subindex: 2; fsize: 4 ) //34
,(name: 'Transmit PDO 1 Mapping_PDO 1 Mapping for a process data variable 3';index: $1A00; subindex: 3; fsize: 4 ) //35
,(name: 'Transmit PDO 1 Mapping_PDO 1 Mapping for a process data variable 4';index: $1A00; subindex: 4; fsize: 4 ) //36
,(name: 'Transmit PDO 1 Mapping_PDO 1 Mapping for a process data variable 5';index: $1A00; subindex: 5; fsize: 4 ) //37
,(name: 'Transmit PDO 1 Mapping_PDO 1 Mapping for a process data variable 6';index: $1A00; subindex: 6; fsize: 4 ) //38
,(name: 'Transmit PDO 2 Mapping_PDO 2 Mapping for a process data variable 1';index: $1A01; subindex: 1; fsize: 4 ) //39
,(name: 'Transmit PDO 2 Mapping_PDO 2 Mapping for a process data variable 2';index: $1A01; subindex: 2; fsize: 4 ) //40
,(name: 'Transmit PDO 2 Mapping_PDO 2 Mapping for a process data variable 3';index: $1A01; subindex: 3; fsize: 4 ) //41
,(name: 'Transmit PDO 2 Mapping_PDO 2 Mapping for a process data variable 4';index: $1A01; subindex: 4; fsize: 4 ) //42
,(name: 'Transmit PDO 2 Mapping_PDO 2 Mapping for a process data variable 5';index: $1A01; subindex: 5; fsize: 4 ) //43
,(name: 'Transmit PDO 2 Mapping_PDO 2 Mapping for a process data variable 6';index: $1A01; subindex: 6; fsize: 4 ) //44
,(name: 'OnTime';index: $2000; subindex: 0; fsize: 4 ) //45
,(name: 'Password';index: $2001; subindex: 0; fsize: 4 ) //46
,(name: 'VersionParameters';index: $2002; subindex: 0; fsize: 2 ) //47
,(name: 'CrcParameters';index: $2003; subindex: 0; fsize: 4 ) //48
,(name: 'Recorder_Period';index: $2004; subindex: 1; fsize: 4 ) //49
,(name: 'Recorder_NbOfSamples';index: $2004; subindex: 2; fsize: 4 ) //50
,(name: 'Recorder_Vectors';index: $2004; subindex: 3; fsize: 8 ) //51
,(name: 'Recorder_Multiunits';index: $2004; subindex: 4; fsize: 2 ) //52
,(name: 'Recorder_Variables';index: $2004; subindex: 5; fsize: 2 ) //53
,(name: 'Recorder_Start';index: $2004; subindex: 6; fsize: 2 ) //54
,(name: 'Recorder_ReadIndex';index: $2004; subindex: 7; fsize: 2 ) //55
,(name: 'Recorder_TriggerIndex';index: $2004; subindex: 8; fsize: 2 ) //56
,(name: 'Recorder_PreTrigger';index: $2004; subindex: 9; fsize: 2 ) //57
,(name: 'Recorder_TriggerLevel';index: $2004; subindex: 10; fsize: 4 ) //58
,(name: 'Recorder_Control';index: $2004; subindex: 11; fsize: 2 ) //59
,(name: 'Recorder_Size';index: $2004; subindex: 12; fsize: 2 ) //60
,(name: 'RecorderData1';index: $2005; subindex: 0; fsize: 8192 ) //61
,(name: 'ErrorDsp_ErrorNumber';index: $2006; subindex: 1; fsize: 4 ) //62
,(name: 'ErrorDsp_WarningNumber';index: $2006; subindex: 2; fsize: 4 ) //63
,(name: 'ErrorDsp_ErrorLevel';index: $2006; subindex: 3; fsize: 1 ) //64
,(name: 'SciSend';index: $2007; subindex: 0; fsize: 1 ) //65
,(name: 'Gateway_Voltage';index: $2008; subindex: 1; fsize: 2 ) //66
,(name: 'Gateway_Current';index: $2008; subindex: 2; fsize: 2 ) //67
,(name: 'Gateway_Temperature';index: $2008; subindex: 3; fsize: 1 ) //68
,(name: 'Gateway_SOC';index: $2008; subindex: 4; fsize: 1 ) //69
,(name: 'Gateway_SOH';index: $2008; subindex: 5; fsize: 1 ) //70
,(name: 'Gateway_State';index: $2008; subindex: 6; fsize: 1 ) //71
,(name: 'Gateway_Errorcode';index: $2008; subindex: 7; fsize: 4 ) //72
,(name: 'Gateway_Status';index: $2008; subindex: 8; fsize: 1 ) //73
,(name: 'Gateway_Date_Time';index: $2008; subindex: 9; fsize: 8 ) //74
,(name: 'Gateway_LogNB';index: $2008; subindex: 10; fsize: 1 ) //75
,(name: 'Gateway_WhCounter';index: $2008; subindex: 11; fsize: 4 ) //76
,(name: 'Gateway_Power';index: $2008; subindex: 12; fsize: 4 ) //77
,(name: 'Gateway_MinCellVoltage';index: $2008; subindex: 13; fsize: 1 ) //78
,(name: 'Gateway_MaxCellVoltage';index: $2008; subindex: 14; fsize: 1 ) //79
,(name: 'Gateway_MaxDeltaCellVoltage';index: $2008; subindex: 15; fsize: 1 ) //80
,(name: 'SafetyLimits_Umax';index: $2009; subindex: 1; fsize: 2 ) //81
,(name: 'SafetyLimits_Umin';index: $2009; subindex: 2; fsize: 2 ) //82
,(name: 'SafetyLimits_Tmax';index: $2009; subindex: 3; fsize: 1 ) //83
,(name: 'SafetyLimits_Tmin';index: $2009; subindex: 4; fsize: 1 ) //84
,(name: 'SafetyLimits_Imax_charge';index: $2009; subindex: 5; fsize: 2 ) //85
,(name: 'SafetyLimits_Umax_bal_delta';index: $2009; subindex: 6; fsize: 2 ) //86
,(name: 'SafetyLimits_Imax_dis';index: $2009; subindex: 7; fsize: 2 ) //87
,(name: 'SafetyLimits_Umin_bal_delta';index: $2009; subindex: 8; fsize: 2 ) //88
,(name: 'SafetyLimits_Charge_In_Thres_Cur';index: $2009; subindex: 9; fsize: 1 ) //89
,(name: 'SafetyLimits_Overcurrent';index: $2009; subindex: 10; fsize: 2 ) //90
,(name: 'SafetyLimits_OverVoltage';index: $2009; subindex: 11; fsize: 2 ) //91
,(name: 'SafetyLimits_UnderVoltage';index: $2009; subindex: 12; fsize: 2 ) //92
,(name: 'SafetyLimits_Resistor_Tmax';index: $2009; subindex: 13; fsize: 1 ) //93
,(name: 'SafetyLimits_Resistor_Tmin';index: $2009; subindex: 14; fsize: 1 ) //94
,(name: 'SafetyLimits_Resistor_Delay';index: $2009; subindex: 15; fsize: 1 ) //95
,(name: 'SafetyLimits_Mosfet_Tmax';index: $2009; subindex: 16; fsize: 1 ) //96
,(name: 'SafetyLimits_Mosfet_Tmin';index: $2009; subindex: 17; fsize: 1 ) //97
,(name: 'SafetyLimits_Cell_Nb';index: $2009; subindex: 18; fsize: 1 ) //98
,(name: 'SafetyLimits_T3_max';index: $2009; subindex: 19; fsize: 1 ) //99
,(name: 'SafetyLimits_T3_min';index: $2009; subindex: 20; fsize: 1 ) //100
,(name: 'SafetyLimits_Voltage_delay';index: $2009; subindex: 21; fsize: 1 ) //101
,(name: 'SafetyLimits_Current_delay';index: $2009; subindex: 22; fsize: 1 ) //102
,(name: 'SafetyLimits_UnderCurrent';index: $2009; subindex: 23; fsize: 2 ) //103
,(name: 'SafetyLimits_SleepTimeout';index: $2009; subindex: 24; fsize: 1 ) //104
,(name: 'SafetyLimits_BalancingTimeout';index: $2009; subindex: 25; fsize: 1 ) //105
,(name: 'SafetyLimits_DamagedVoltage';index: $2009; subindex: 26; fsize: 1 ) //106
,(name: 'SafetyLimits_Temp_Delay';index: $2009; subindex: 27; fsize: 1 ) //107
,(name: 'SafetyLimits_Low_Voltage_Current_Delay';index: $2009; subindex: 28; fsize: 1 ) //108
,(name: 'SafetyLimits_Low_Voltage_Current_Allow';index: $2009; subindex: 29; fsize: 2 ) //109
,(name: 'SafetyLimits_Low_Voltage_Charge_Delay';index: $2009; subindex: 30; fsize: 1 ) //110
,(name: 'Power_ChargeLimits_Power_ChargeLimits 1';index: $200A; subindex: 1; fsize: 1 ) //111
,(name: 'Power_ChargeLimits_Power_ChargeLimits 2';index: $200A; subindex: 2; fsize: 1 ) //112
,(name: 'Power_ChargeLimits_Power_ChargeLimits 3';index: $200A; subindex: 3; fsize: 1 ) //113
,(name: 'Power_ChargeLimits_Power_ChargeLimits 4';index: $200A; subindex: 4; fsize: 1 ) //114
,(name: 'Power_ChargeLimits_Power_ChargeLimits 5';index: $200A; subindex: 5; fsize: 1 ) //115
,(name: 'Power_ChargeLimits_Power_ChargeLimits 6';index: $200A; subindex: 6; fsize: 1 ) //116
,(name: 'Power_ChargeLimits_Power_ChargeLimits 7';index: $200A; subindex: 7; fsize: 1 ) //117
,(name: 'Power_ChargeLimits_Power_ChargeLimits 8';index: $200A; subindex: 8; fsize: 1 ) //118
,(name: 'Temperature_ChargeLimits_Temperature_ChargeLimits 1';index: $200B; subindex: 1; fsize: 1 ) //119
,(name: 'Temperature_ChargeLimits_Temperature_ChargeLimits 2';index: $200B; subindex: 2; fsize: 1 ) //120
,(name: 'Temperature_ChargeLimits_Temperature_ChargeLimits 3';index: $200B; subindex: 3; fsize: 1 ) //121
,(name: 'Temperature_ChargeLimits_Temperature_ChargeLimits 4';index: $200B; subindex: 4; fsize: 1 ) //122
,(name: 'Temperature_ChargeLimits_Temperature_ChargeLimits 5';index: $200B; subindex: 5; fsize: 1 ) //123
,(name: 'Temperature_ChargeLimits_Temperature_ChargeLimits 6';index: $200B; subindex: 6; fsize: 1 ) //124
,(name: 'Temperature_ChargeLimits_Temperature_ChargeLimits 7';index: $200B; subindex: 7; fsize: 1 ) //125
,(name: 'Temperature_ChargeLimits_Temperature_ChargeLimits 8';index: $200B; subindex: 8; fsize: 1 ) //126
,(name: 'Power_DischargeLimits_Power_DischargeLimits 1';index: $200C; subindex: 1; fsize: 1 ) //127
,(name: 'Power_DischargeLimits_Power_DischargeLimits 2';index: $200C; subindex: 2; fsize: 1 ) //128
,(name: 'Power_DischargeLimits_Power_DischargeLimits 3';index: $200C; subindex: 3; fsize: 1 ) //129
,(name: 'Power_DischargeLimits_Power_DischargeLimits 4';index: $200C; subindex: 4; fsize: 1 ) //130
,(name: 'Power_DischargeLimits_Power_DischargeLimits 5';index: $200C; subindex: 5; fsize: 1 ) //131
,(name: 'Power_DischargeLimits_Power_DischargeLimits 6';index: $200C; subindex: 6; fsize: 1 ) //132
,(name: 'Power_DischargeLimits_Power_DischargeLimits 7';index: $200C; subindex: 7; fsize: 1 ) //133
,(name: 'Power_DischargeLimits_Power_DischargeLimits 8';index: $200C; subindex: 8; fsize: 1 ) //134
,(name: 'Temperature_DischargeLimits_Temperature_DischargeLimits 1';index: $200D; subindex: 1; fsize: 1 ) //135
,(name: 'Temperature_DischargeLimits_Temperature_DischargeLimits 2';index: $200D; subindex: 2; fsize: 1 ) //136
,(name: 'Temperature_DischargeLimits_Temperature_DischargeLimits 3';index: $200D; subindex: 3; fsize: 1 ) //137
,(name: 'Temperature_DischargeLimits_Temperature_DischargeLimits 4';index: $200D; subindex: 4; fsize: 1 ) //138
,(name: 'Temperature_DischargeLimits_Temperature_DischargeLimits 5';index: $200D; subindex: 5; fsize: 1 ) //139
,(name: 'Temperature_DischargeLimits_Temperature_DischargeLimits 6';index: $200D; subindex: 6; fsize: 1 ) //140
,(name: 'Temperature_DischargeLimits_Temperature_DischargeLimits 7';index: $200D; subindex: 7; fsize: 1 ) //141
,(name: 'Temperature_DischargeLimits_Temperature_DischargeLimits 8';index: $200D; subindex: 8; fsize: 1 ) //142
,(name: 'MachineState';index: $200E; subindex: 0; fsize: 2 ) //143
,(name: 'Temperature_Min';index: $200F; subindex: 1; fsize: 1 ) //144
,(name: 'Temperature_Max';index: $200F; subindex: 2; fsize: 1 ) //145
,(name: 'Temperature_Average_data';index: $200F; subindex: 3; fsize: 1 ) //146
,(name: 'Voltage_Min';index: $2010; subindex: 1; fsize: 2 ) //147
,(name: 'Voltage_Max';index: $2010; subindex: 2; fsize: 2 ) //148
,(name: 'Current_Min';index: $2011; subindex: 1; fsize: 2 ) //149
,(name: 'Current_Max';index: $2011; subindex: 2; fsize: 2 ) //150
,(name: 'Current_ChargeAllowed';index: $2011; subindex: 3; fsize: 2 ) //151
,(name: 'Current_DischargeAllowed';index: $2011; subindex: 4; fsize: 2 ) //152
,(name: 'Current_C_D_Mode';index: $2011; subindex: 5; fsize: 2 ) //153
,(name: 'Contactor_Setup';index: $2012; subindex: 1; fsize: 2 ) //154
,(name: 'Contactor_Hold';index: $2012; subindex: 2; fsize: 2 ) //155
,(name: 'Contactor_Overcurrent';index: $2012; subindex: 3; fsize: 2 ) //156
,(name: 'Contactor_Slope';index: $2012; subindex: 4; fsize: 1 ) //157
,(name: 'Contactor_Undercurrent';index: $2012; subindex: 5; fsize: 2 ) //158
,(name: 'Contactor_Delay';index: $2012; subindex: 6; fsize: 1 ) //159
,(name: 'Contactor_Undefined';index: $2012; subindex: 7; fsize: 1 ) //160
,(name: 'Heater_OnTemp';index: $2013; subindex: 1; fsize: 1 ) //161
,(name: 'Heater_OffTemp';index: $2013; subindex: 2; fsize: 1 ) //162
,(name: 'Heater_Off_Cell_Voltage';index: $2013; subindex: 3; fsize: 2 ) //163
,(name: 'Heater_Heater_Status';index: $2013; subindex: 4; fsize: 1 ) //164
,(name: 'Heater_Heater_SCI';index: $2013; subindex: 5; fsize: 2 ) //165
,(name: 'Heater_Control_Heater';index: $2013; subindex: 6; fsize: 1 ) //166
,(name: 'Unused_Undefined1';index: $2014; subindex: 1; fsize: 1 ) //167
,(name: 'Unused_Undefined2';index: $2014; subindex: 2; fsize: 1 ) //168
,(name: 'Unused_Undefined3';index: $2014; subindex: 3; fsize: 1 ) //169
,(name: 'Unused_Undefined4';index: $2014; subindex: 4; fsize: 1 ) //170
,(name: 'Unused_Undefined5';index: $2014; subindex: 5; fsize: 1 ) //171
,(name: 'Unused_Undefined6';index: $2014; subindex: 6; fsize: 1 ) //172
,(name: 'Unused_Undefined7';index: $2014; subindex: 7; fsize: 1 ) //173
,(name: 'Unused_Undefined8';index: $2014; subindex: 8; fsize: 1 ) //174
,(name: 'Master_Module1_Voltage';index: $2015; subindex: 1; fsize: 2 ) //175
,(name: 'Master_Module1_Error';index: $2015; subindex: 2; fsize: 1 ) //176
,(name: 'Master_Module1_Temp';index: $2015; subindex: 3; fsize: 1 ) //177
,(name: 'Master_Module1_Undefined1';index: $2015; subindex: 4; fsize: 1 ) //178
,(name: 'Master_Module1_Undefined2';index: $2015; subindex: 5; fsize: 1 ) //179
,(name: 'Master_Module1_Undefined3';index: $2015; subindex: 6; fsize: 1 ) //180
,(name: 'MachineMode';index: $201C; subindex: 0; fsize: 1 ) //181
,(name: 'MachineEvent';index: $201E; subindex: 0; fsize: 1 ) //182
,(name: 'CommError_Counter';index: $2023; subindex: 1; fsize: 4 ) //183
,(name: 'CommError_StateCounter';index: $2023; subindex: 2; fsize: 4 ) //184
,(name: 'CommError_Set';index: $2023; subindex: 3; fsize: 1 ) //185
,(name: 'CommError_TimeOut';index: $2023; subindex: 4; fsize: 1 ) //186
,(name: 'CommError_Delay';index: $2023; subindex: 5; fsize: 1 ) //187
,(name: 'CommError_OverTemp_ErrCounter';index: $2023; subindex: 6; fsize: 1 ) //188
,(name: 'CommError_OverVoltage_ErrCounter';index: $2023; subindex: 7; fsize: 1 ) //189
,(name: 'CommError_LowVoltage_ErrCounter';index: $2023; subindex: 8; fsize: 1 ) //190
,(name: 'Board_RevisionNumber';index: $2040; subindex: 1; fsize: 4 ) //191
,(name: 'Board_SerialNumber';index: $2040; subindex: 2; fsize: 4 ) //192
,(name: 'Board_Config';index: $2040; subindex: 3; fsize: 2 ) //193
,(name: 'Board_BaudRate';index: $2040; subindex: 4; fsize: 2 ) //194
,(name: 'Board_Bootloader';index: $2040; subindex: 5; fsize: 2 ) //195
,(name: 'Board_HW_Version';index: $2040; subindex: 6; fsize: 2 ) //196
,(name: 'Board_Deactive_FF';index: $2040; subindex: 7; fsize: 1 ) //197
,(name: 'Board_Chip';index: $2040; subindex: 8; fsize: 1 ) //198
,(name: 'Board_Manufactuer';index: $2040; subindex: 9; fsize: 4 ) //199
,(name: 'Board_Type';index: $2040; subindex: 10; fsize: 1 ) //200
,(name: 'Board_SerialNum';index: $2040; subindex: 11; fsize: 3 ) //201
,(name: 'Board_Product';index: $2040; subindex: 12; fsize: 3 ) //202
,(name: 'SleepCurrent';index: $2041; subindex: 0; fsize: 2 ) //203
,(name: 'RTC_Pos';index: $2100; subindex: 1; fsize: 1 ) //204
,(name: 'RTC_Text';index: $2100; subindex: 2; fsize: 8 ) //205
,(name: 'RTC_Len';index: $2100; subindex: 3; fsize: 1 ) //206
,(name: 'Version';index: $2101; subindex: 0; fsize: 1 ) //207
,(name: 'Security';index: $2102; subindex: 0; fsize: 8 ) //208
,(name: 'RandomNB';index: $2103; subindex: 0; fsize: 4 ) //209
,(name: 'SysTick_ms';index: $2104; subindex: 0; fsize: 4 ) //210
,(name: 'Battery_Capacity';index: $2105; subindex: 1; fsize: 4 ) //211
,(name: 'Battery_kWh';index: $2105; subindex: 2; fsize: 2 ) //212
,(name: 'Battery_kWh_Life';index: $2105; subindex: 3; fsize: 2 ) //213
,(name: 'Battery_Cycles';index: $2105; subindex: 4; fsize: 2 ) //214
,(name: 'Battery_Total_Cycles';index: $2105; subindex: 5; fsize: 2 ) //215
,(name: 'Battery_Total_ChargedkWh';index: $2105; subindex: 6; fsize: 2 ) //216
,(name: 'Battery_Operating_Hours';index: $2105; subindex: 7; fsize: 3 ) //217
,(name: 'Battery_TotalLife_Charged';index: $2105; subindex: 8; fsize: 2 ) //218
,(name: 'SOC_SOC1';index: $2106; subindex: 1; fsize: 1 ) //219
,(name: 'SOC_SOC2';index: $2106; subindex: 2; fsize: 1 ) //220
,(name: 'Module1_Status';index: $2200; subindex: 1; fsize: 1 ) //221
,(name: 'Module1_SOC';index: $2200; subindex: 2; fsize: 1 ) //222
,(name: 'Module1_Alarms';index: $2200; subindex: 3; fsize: 1 ) //223
,(name: 'Module1_Voltage';index: $2200; subindex: 4; fsize: 2 ) //224
,(name: 'Module1_Current';index: $2200; subindex: 5; fsize: 2 ) //225
,(name: 'Module1_Temperature';index: $2200; subindex: 6; fsize: 1 ) //226
,(name: 'Module1_Throughput';index: $2200; subindex: 7; fsize: 2 ) //227
,(name: 'Module1_MinCellVoltage';index: $2200; subindex: 8; fsize: 1 ) //228
,(name: 'Module1_MaxCellVoltage';index: $2200; subindex: 9; fsize: 1 ) //229
,(name: 'Module1_Warnings';index: $2200; subindex: 10; fsize: 1 ) //230
,(name: 'Module1_Temperature2';index: $2200; subindex: 11; fsize: 1 ) //231
,(name: 'Module1_Capacity_set';index: $2200; subindex: 12; fsize: 2 ) //232
,(name: 'Module1_MaxDeltaVoltage';index: $2200; subindex: 13; fsize: 1 ) //233
,(name: 'Module1_SOH';index: $2200; subindex: 14; fsize: 1 ) //234
,(name: 'Module1_Capacity_Used';index: $2200; subindex: 15; fsize: 2 ) //235
,(name: 'Module1_Time_Remaining';index: $2200; subindex: 16; fsize: 2 ) //236
,(name: 'Module1_Alive_Counter';index: $2200; subindex: 17; fsize: 1 ) //237
,(name: 'Module1_Module_SOC_Current';index: $2200; subindex: 18; fsize: 1 ) //238
,(name: 'Module1_Module_SOC_Calibration';index: $2200; subindex: 19; fsize: 1 ) //239
,(name: 'Module1_ErrorCounterDelay';index: $2200; subindex: 20; fsize: 1 ) //240
,(name: 'Module1_ErrorReset';index: $2200; subindex: 21; fsize: 1 ) //241
,(name: 'Module1_Current_Average';index: $2200; subindex: 22; fsize: 2 ) //242
,(name: 'Module1_Current_Resolution';index: $2200; subindex: 23; fsize: 2 ) //243
,(name: 'Module1_StandbyCurrent';index: $2200; subindex: 24; fsize: 1 ) //244
,(name: 'StoreParameters';index: $2F00; subindex: 0; fsize: 4 ) //245
,(name: 'RestoreDefaultParameters';index: $2F01; subindex: 0; fsize: 4 ) //246
,(name: 'ResetHW';index: $2F02; subindex: 0; fsize: 4 ) //247
,(name: 'Debug';index: $2F03; subindex: 0; fsize: 1 ) //248
,(name: 'Read Inputs 16 Bit_Read Inputs 0x1 to 0x10';index: $6100; subindex: 1; fsize: 2 ) //249
,(name: 'Read Inputs 16 Bit_Read Inputs 0x11 to 0x20';index: $6100; subindex: 2; fsize: 2 ) //250
,(name: 'Write Outputs 16 Bit_Write Outputs 0x1 to 0x10';index: $6300; subindex: 1; fsize: 2 ) //251
,(name: 'Write Outputs 16 Bit_Write Outputs 0x11 to 0x20';index: $6300; subindex: 2; fsize: 2 ) //252
,(name: 'Read Analogue Input 16 Bit_Analogue Input 1';index: $6401; subindex: 1; fsize: 2 ) //253
,(name: 'Read Analogue Input 16 Bit_Analogue Input 2';index: $6401; subindex: 2; fsize: 2 ) //254
,(name: 'Read Analogue Input 16 Bit_Analogue Input 3';index: $6401; subindex: 3; fsize: 2 ) //255
,(name: 'Read Analogue Input 16 Bit_Analogue Input 4';index: $6401; subindex: 4; fsize: 2 ) //256
,(name: 'Read Analogue Input 16 Bit_Analogue Input 5';index: $6401; subindex: 5; fsize: 2 ) //257
,(name: 'Read Analogue Input 16 Bit_Analogue Input 6';index: $6401; subindex: 6; fsize: 2 ) //258
,(name: 'Read Analogue Input 16 Bit_Analogue Input 7';index: $6401; subindex: 7; fsize: 2 ) //259
,(name: 'Read Analogue Input 16 Bit_Analogue Input 8';index: $6401; subindex: 8; fsize: 2 ) //260
,(name: 'Read Analogue Input 16 Bit_Analogue Input 9';index: $6401; subindex: 9; fsize: 2 ) //261
,(name: 'Read Analogue Input 16 Bit_Analogue Input 10';index: $6401; subindex: 10; fsize: 2 ) //262
,(name: 'Read Analogue Input 16 Bit_Analogue Input 11';index: $6401; subindex: 11; fsize: 2 ) //263
,(name: 'Read Analogue Input 16 Bit_Analogue Input 12';index: $6401; subindex: 12; fsize: 2 ) //264
,(name: 'Read Analogue Input 16 Bit_Analogue Input 13';index: $6401; subindex: 13; fsize: 2 ) //265
,(name: 'Read Analogue Input 16 Bit_Analogue Input 14';index: $6401; subindex: 14; fsize: 2 ) //266
,(name: 'Read Analogue Input 16 Bit_Analogue Input 15';index: $6401; subindex: 15; fsize: 2 ) //267
,(name: 'Read Analogue Input 16 Bit_Analogue Input 16';index: $6401; subindex: 16; fsize: 2 ) //268
,(name: 'Read Analogue Input 16 Bit_Analogue Input 17';index: $6401; subindex: 17; fsize: 2 ) //269
,(name: 'Read Analogue Input 16 Bit_Analogue Input 18';index: $6401; subindex: 18; fsize: 2 ) //270
,(name: 'Read Analogue Input 16 Bit_Analogue Input 19';index: $6401; subindex: 19; fsize: 2 ) //271
,(name: 'Read Analogue Input 16 Bit_Analogue Input 20';index: $6401; subindex: 20; fsize: 2 ) //272
,(name: 'Read Analogue Input 16 Bit_Analogue Input 21';index: $6401; subindex: 21; fsize: 2 ) //273
,(name: 'Read Analogue Input 16 Bit_Analogue Input 22';index: $6401; subindex: 22; fsize: 2 ) //274
,(name: 'Read Analogue Input 16 Bit_Analogue Input 23';index: $6401; subindex: 23; fsize: 2 ) //275
,(name: 'Write Analogue Output 16 Bit_Analogue Output 1';index: $6411; subindex: 1; fsize: 2 ) //276
,(name: 'Write Analogue Output 16 Bit_Analogue Output 2';index: $6411; subindex: 2; fsize: 2 ) //277
,(name: 'Write Analogue Output 16 Bit_Analogue Output 3';index: $6411; subindex: 3; fsize: 2 ) //278
,(name: 'Write Analogue Output 16 Bit_Analogue Output 4';index: $6411; subindex: 4; fsize: 2 ) //279
,(name: 'Write Analogue Output 16 Bit_Analogue Output 5';index: $6411; subindex: 5; fsize: 2 ) //280
,(name: 'Write Analogue Output 16 Bit_Analogue Output 6';index: $6411; subindex: 6; fsize: 2 ) //281
,(name: 'Write Analogue Output 16 Bit_Analogue Output 7';index: $6411; subindex: 7; fsize: 2 ) //282
,(name: 'Write Analogue Output 16 Bit_Analogue Output 8';index: $6411; subindex: 8; fsize: 2 ) //283
,(name: 'Write Analogue Output 16 Bit_Analogue Output 9';index: $6411; subindex: 9; fsize: 2 ) //284
,(name: 'Write Analogue Output 16 Bit_Analogue Output 10';index: $6411; subindex: 10; fsize: 2 ) //285
,(name: 'Write Analogue Output 16 Bit_Analogue Output 11';index: $6411; subindex: 11; fsize: 2 ) //286
,(name: 'Write Analogue Output 16 Bit_Analogue Output 12';index: $6411; subindex: 12; fsize: 2 ) //287
,(name: 'Write Analogue Output 16 Bit_Analogue Output 13';index: $6411; subindex: 13; fsize: 2 ) //288
,(name: 'Write Analogue Output 16 Bit_Analogue Output 14';index: $6411; subindex: 14; fsize: 2 ) //289
,(name: 'Write Analogue Output 16 Bit_Analogue Output 15';index: $6411; subindex: 15; fsize: 2 ) //290
,(name: 'Write Analogue Output 16 Bit_Analogue Output 16';index: $6411; subindex: 16; fsize: 2 ) //291
,(name: 'Analogue Input Scaling Float_Analogue Input 1';index: $642F; subindex: 1; fsize: 4 ) //292
,(name: 'Analogue Input Scaling Float_Analogue Input 2';index: $642F; subindex: 2; fsize: 4 ) //293
,(name: 'Analogue Input Scaling Float_Analogue Input 3';index: $642F; subindex: 3; fsize: 4 ) //294
,(name: 'Analogue Input Scaling Float_Analogue Input 4';index: $642F; subindex: 4; fsize: 4 ) //295
,(name: 'Analogue Input Scaling Float_Analogue Input 5';index: $642F; subindex: 5; fsize: 4 ) //296
,(name: 'Analogue Input Scaling Float_Analogue Input 6';index: $642F; subindex: 6; fsize: 4 ) //297
,(name: 'Analogue Input Scaling Float_Analogue Input 7';index: $642F; subindex: 7; fsize: 4 ) //298
,(name: 'Analogue Input Scaling Float_Analogue Input 8';index: $642F; subindex: 8; fsize: 4 ) //299
,(name: 'Analogue Input Scaling Float_Analogue Input 9';index: $642F; subindex: 9; fsize: 4 ) //300
,(name: 'Analogue Input Scaling Float_Analogue Input 10';index: $642F; subindex: 10; fsize: 4 ) //301
,(name: 'Analogue Input Scaling Float_Analogue Input 11';index: $642F; subindex: 11; fsize: 4 ) //302
,(name: 'Analogue Input Scaling Float_Analogue Input 12';index: $642F; subindex: 12; fsize: 4 ) //303
,(name: 'Analogue Input Scaling Float_Analogue Input 13';index: $642F; subindex: 13; fsize: 4 ) //304
,(name: 'Analogue Input Scaling Float_Analogue Input 14';index: $642F; subindex: 14; fsize: 4 ) //305
,(name: 'Analogue Input Scaling Float_Analogue Input 15';index: $642F; subindex: 15; fsize: 4 ) //306
,(name: 'Analogue Input Scaling Float_Analogue Input 16';index: $642F; subindex: 16; fsize: 4 ) //307
,(name: 'Analogue Input Scaling Float_Analogue Input 17';index: $642F; subindex: 17; fsize: 4 ) //308
,(name: 'Analogue Input Scaling Float_Analogue Input 18';index: $642F; subindex: 18; fsize: 4 ) //309
,(name: 'Analogue Input Scaling Float_Analogue Input 19';index: $642F; subindex: 19; fsize: 4 ) //310
,(name: 'Analogue Input Scaling Float_Analogue Input 20';index: $642F; subindex: 20; fsize: 4 ) //311
,(name: 'Analogue Input Scaling Float_Analogue Input 21';index: $642F; subindex: 21; fsize: 4 ) //312
,(name: 'Analogue Input Scaling Float_Analogue Input 22';index: $642F; subindex: 22; fsize: 4 ) //313
,(name: 'Analogue Input Scaling Float_Analogue Input 23';index: $642F; subindex: 23; fsize: 4 ) //314
,(name: 'Analogue Input SI unit_Analogue Input 1';index: $6430; subindex: 1; fsize: 4 ) //315
,(name: 'Analogue Input SI unit_Analogue Input 2';index: $6430; subindex: 2; fsize: 4 ) //316
,(name: 'Analogue Input SI unit_Analogue Input 3';index: $6430; subindex: 3; fsize: 4 ) //317
,(name: 'Analogue Input SI unit_Analogue Input 4';index: $6430; subindex: 4; fsize: 4 ) //318
,(name: 'Analogue Input SI unit_Analogue Input 5';index: $6430; subindex: 5; fsize: 4 ) //319
,(name: 'Analogue Input SI unit_Analogue Input 6';index: $6430; subindex: 6; fsize: 4 ) //320
,(name: 'Analogue Input SI unit_Analogue Input 7';index: $6430; subindex: 7; fsize: 4 ) //321
,(name: 'Analogue Input SI unit_Analogue Input 8';index: $6430; subindex: 8; fsize: 4 ) //322
,(name: 'Analogue Input SI unit_Analogue Input 9';index: $6430; subindex: 9; fsize: 4 ) //323
,(name: 'Analogue Input SI unit_Analogue Input 10';index: $6430; subindex: 10; fsize: 4 ) //324
,(name: 'Analogue Input SI unit_Analogue Input 11';index: $6430; subindex: 11; fsize: 4 ) //325
,(name: 'Analogue Input SI unit_Analogue Input 12';index: $6430; subindex: 12; fsize: 4 ) //326
,(name: 'Analogue Input SI unit_Analogue Input 13';index: $6430; subindex: 13; fsize: 4 ) //327
,(name: 'Analogue Input SI unit_Analogue Input 14';index: $6430; subindex: 14; fsize: 4 ) //328
,(name: 'Analogue Input SI unit_Analogue Input 15';index: $6430; subindex: 15; fsize: 4 ) //329
,(name: 'Analogue Input SI unit_Analogue Input 16';index: $6430; subindex: 16; fsize: 4 ) //330
,(name: 'Analogue Input SI unit_Analogue Input 17';index: $6430; subindex: 17; fsize: 4 ) //331
,(name: 'Analogue Input SI unit_Analogue Input 18';index: $6430; subindex: 18; fsize: 4 ) //332
,(name: 'Analogue Input SI unit_Analogue Input 19';index: $6430; subindex: 19; fsize: 4 ) //333
,(name: 'Analogue Input SI unit_Analogue Input 20';index: $6430; subindex: 20; fsize: 4 ) //334
,(name: 'Analogue Input SI unit_Analogue Input 21';index: $6430; subindex: 21; fsize: 4 ) //335
,(name: 'Analogue Input SI unit_Analogue Input 22';index: $6430; subindex: 22; fsize: 4 ) //336
,(name: 'Analogue Input SI unit_Analogue Input 23';index: $6430; subindex: 23; fsize: 4 ) //337
,(name: 'Analogue Input Offset Integer_Analogue Input 1';index: $6431; subindex: 1; fsize: 4 ) //338
,(name: 'Analogue Input Offset Integer_Analogue Input 2';index: $6431; subindex: 2; fsize: 4 ) //339
,(name: 'Analogue Input Offset Integer_Analogue Input 3';index: $6431; subindex: 3; fsize: 4 ) //340
,(name: 'Analogue Input Offset Integer_Analogue Input 4';index: $6431; subindex: 4; fsize: 4 ) //341
,(name: 'Analogue Input Offset Integer_Analogue Input 5';index: $6431; subindex: 5; fsize: 4 ) //342
,(name: 'Analogue Input Offset Integer_Analogue Input 6';index: $6431; subindex: 6; fsize: 4 ) //343
,(name: 'Analogue Input Offset Integer_Analogue Input 7';index: $6431; subindex: 7; fsize: 4 ) //344
,(name: 'Analogue Input Offset Integer_Analogue Input 8';index: $6431; subindex: 8; fsize: 4 ) //345
,(name: 'Analogue Input Offset Integer_Analogue Input 9';index: $6431; subindex: 9; fsize: 4 ) //346
,(name: 'Analogue Input Offset Integer_Analogue Input 10';index: $6431; subindex: 10; fsize: 4 ) //347
,(name: 'Analogue Input Offset Integer_Analogue Input 11';index: $6431; subindex: 11; fsize: 4 ) //348
,(name: 'Analogue Input Offset Integer_Analogue Input 12';index: $6431; subindex: 12; fsize: 4 ) //349
,(name: 'Analogue Input Offset Integer_Analogue Input 13';index: $6431; subindex: 13; fsize: 4 ) //350
,(name: 'Analogue Input Offset Integer_Analogue Input 14';index: $6431; subindex: 14; fsize: 4 ) //351
,(name: 'Analogue Input Offset Integer_Analogue Input 15';index: $6431; subindex: 15; fsize: 4 ) //352
,(name: 'Analogue Input Offset Integer_Analogue Input 16';index: $6431; subindex: 16; fsize: 4 ) //353
,(name: 'Analogue Input Offset Integer_Analogue Input 17';index: $6431; subindex: 17; fsize: 4 ) //354
,(name: 'Analogue Input Offset Integer_Analogue Input 18';index: $6431; subindex: 18; fsize: 4 ) //355
,(name: 'Analogue Input Offset Integer_Analogue Input 19';index: $6431; subindex: 19; fsize: 4 ) //356
,(name: 'Analogue Input Offset Integer_Analogue Input 20';index: $6431; subindex: 20; fsize: 4 ) //357
,(name: 'Analogue Input Offset Integer_Analogue Input 21';index: $6431; subindex: 21; fsize: 4 ) //358
,(name: 'Analogue Input Offset Integer_Analogue Input 22';index: $6431; subindex: 22; fsize: 4 ) //359
,(name: 'Analogue Input Offset Integer_Analogue Input 23';index: $6431; subindex: 23; fsize: 4 ) //360
,(name: 'Analogue Output Scaling Float_Analogue Output 1';index: $6442; subindex: 1; fsize: 4 ) //361
,(name: 'Analogue Output Scaling Float_Analogue Output 2';index: $6442; subindex: 2; fsize: 4 ) //362
,(name: 'Analogue Output Scaling Float_Analogue Output 3';index: $6442; subindex: 3; fsize: 4 ) //363
,(name: 'Analogue Output Scaling Float_Analogue Output 4';index: $6442; subindex: 4; fsize: 4 ) //364
,(name: 'Analogue Output Scaling Float_Analogue Output 5';index: $6442; subindex: 5; fsize: 4 ) //365
,(name: 'Analogue Output Scaling Float_Analogue Output 6';index: $6442; subindex: 6; fsize: 4 ) //366
,(name: 'Analogue Output Scaling Float_Analogue Output 7';index: $6442; subindex: 7; fsize: 4 ) //367
,(name: 'Analogue Output Scaling Float_Analogue Output 8';index: $6442; subindex: 8; fsize: 4 ) //368
,(name: 'Analogue Output Scaling Float_Analogue Output 9';index: $6442; subindex: 9; fsize: 4 ) //369
,(name: 'Analogue Output Scaling Float_Analogue Output 10';index: $6442; subindex: 10; fsize: 4 ) //370
,(name: 'Analogue Output Scaling Float_Analogue Output 11';index: $6442; subindex: 11; fsize: 4 ) //371
,(name: 'Analogue Output Scaling Float_Analogue Output 12';index: $6442; subindex: 12; fsize: 4 ) //372
,(name: 'Analogue Output Scaling Float_Analogue Output 13';index: $6442; subindex: 13; fsize: 4 ) //373
,(name: 'Analogue Output Scaling Float_Analogue Output 14';index: $6442; subindex: 14; fsize: 4 ) //374
,(name: 'Analogue Output Scaling Float_Analogue Output 15';index: $6442; subindex: 15; fsize: 4 ) //375
,(name: 'Analogue Output Scaling Float_Analogue Output 16';index: $6442; subindex: 16; fsize: 4 ) //376
,(name: 'Analogue Output Offset Integer_Analogue Output 1';index: $6446; subindex: 1; fsize: 4 ) //377
,(name: 'Analogue Output Offset Integer_Analogue Output 2';index: $6446; subindex: 2; fsize: 4 ) //378
,(name: 'Analogue Output Offset Integer_Analogue Output 3';index: $6446; subindex: 3; fsize: 4 ) //379
,(name: 'Analogue Output Offset Integer_Analogue Output 4';index: $6446; subindex: 4; fsize: 4 ) //380
,(name: 'Analogue Output Offset Integer_Analogue Output 5';index: $6446; subindex: 5; fsize: 4 ) //381
,(name: 'Analogue Output Offset Integer_Analogue Output 6';index: $6446; subindex: 6; fsize: 4 ) //382
,(name: 'Analogue Output Offset Integer_Analogue Output 7';index: $6446; subindex: 7; fsize: 4 ) //383
,(name: 'Analogue Output Offset Integer_Analogue Output 8';index: $6446; subindex: 8; fsize: 4 ) //384
,(name: 'Analogue Output Offset Integer_Analogue Output 9';index: $6446; subindex: 9; fsize: 4 ) //385
,(name: 'Analogue Output Offset Integer_Analogue Output 10';index: $6446; subindex: 10; fsize: 4 ) //386
,(name: 'Analogue Output Offset Integer_Analogue Output 11';index: $6446; subindex: 11; fsize: 4 ) //387
,(name: 'Analogue Output Offset Integer_Analogue Output 12';index: $6446; subindex: 12; fsize: 4 ) //388
,(name: 'Analogue Output Offset Integer_Analogue Output 13';index: $6446; subindex: 13; fsize: 4 ) //389
,(name: 'Analogue Output Offset Integer_Analogue Output 14';index: $6446; subindex: 14; fsize: 4 ) //390
,(name: 'Analogue Output Offset Integer_Analogue Output 15';index: $6446; subindex: 15; fsize: 4 ) //391
,(name: 'Analogue Output Offset Integer_Analogue Output 16';index: $6446; subindex: 16; fsize: 4 ) //392
,(name: 'Analogue Output SI Unit_Analogue Output 1';index: $6450; subindex: 1; fsize: 4 ) //393
,(name: 'Analogue Output SI Unit_Analogue Output 2';index: $6450; subindex: 2; fsize: 4 ) //394
,(name: 'Analogue Output SI Unit_Analogue Output 3';index: $6450; subindex: 3; fsize: 4 ) //395
,(name: 'Analogue Output SI Unit_Analogue Output 4';index: $6450; subindex: 4; fsize: 4 ) //396
,(name: 'Analogue Output SI Unit_Analogue Output 5';index: $6450; subindex: 5; fsize: 4 ) //397
,(name: 'Analogue Output SI Unit_Analogue Output 6';index: $6450; subindex: 6; fsize: 4 ) //398
,(name: 'Analogue Output SI Unit_Analogue Output 7';index: $6450; subindex: 7; fsize: 4 ) //399
,(name: 'Analogue Output SI Unit_Analogue Output 8';index: $6450; subindex: 8; fsize: 4 ) //400
,(name: 'Analogue Output SI Unit_Analogue Output 9';index: $6450; subindex: 9; fsize: 4 ) //401
,(name: 'Analogue Output SI Unit_Analogue Output 10';index: $6450; subindex: 10; fsize: 4 ) //402
,(name: 'Analogue Output SI Unit_Analogue Output 11';index: $6450; subindex: 11; fsize: 4 ) //403
,(name: 'Analogue Output SI Unit_Analogue Output 12';index: $6450; subindex: 12; fsize: 4 ) //404
,(name: 'Analogue Output SI Unit_Analogue Output 13';index: $6450; subindex: 13; fsize: 4 ) //405
,(name: 'Analogue Output SI Unit_Analogue Output 14';index: $6450; subindex: 14; fsize: 4 ) //406
,(name: 'Analogue Output SI Unit_Analogue Output 15';index: $6450; subindex: 15; fsize: 4 ) //407
,(name: 'Analogue Output SI Unit_Analogue Output 16';index: $6450; subindex: 16; fsize: 4 ) //408
,(name: 'Controlword';index: $8040; subindex: 0; fsize: 2 ) //409
,(name: 'Statusword';index: $8041; subindex: 0; fsize: 2 ) //410
,(name: 'Supported drive modes';index: $8502; subindex: 0; fsize: 4 ) //411
);

implementation

end.

