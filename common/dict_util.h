/*
 * dict_util.h
 *
 *  Created on: 5 mars 2012
 *      Author: oliviers
 */

#ifndef DICT_UTIL_H_
#define DICT_UTIL_H_

#include "co_data.h"
#include "ptmtype.h"


#define RETURN_MACRO if( d && indextable && bSubindex)return(0);else return(0)

#define DIC_POWERMAN_INDEX_MIN 0x3000
#define DIC_POWERMAN_INDEX_MAX 0x3FFF

#define IS_WRITE_CALLBACK (access == DIC_WRITE)

#define SDO_PENDING        0 //this state is set by the posting task as the initial value for untreated can message
#define SDO_PROCESSING     1 //this state indicates that the message is currently being processed and send to the bus
#define SDO_READ_OK       10 //this state indicates that the read operation succeeded
#define SDO_WRITE_OK      11 //indicates that the write operation succeeded
#define SDO_READ_FAILED   20 //indicated that the read operation failed
#define SDO_WRITE_FAILED  21 //indicated that the write operation failed

#define SDO_NONE         100 //No previous SDO communication

bool1 DIC_WriteNetworkDict(UNS8 nodeid, UNS16 wIndex, UNS8 bSubindex);
bool1 DIC_WriteLocalDict(UNS16 wIndex, UNS8 bSubindex);
bool1 DIC_ReadLocalDict(UNS16 wIndex, UNS8 bSubindex);
bool1 DIC_ReadNetworkDict(UNS8 nodeid, UNS16 wIndex, UNS8 bSubindex);
bool1 DIC_ReadNetworkDictTimeout(UNS16 wIndex, UNS8 bSubindex, UNS16 timeout);
UNS32 DIC_GlobalDictCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access);


bool1 DIC_CheckEqual(uint16 index, uint8 subindex, uint16 index2, uint8 subindex2);
void DIC_SetNodeId(UNS8 fnodeId);

//====================== functions to read an object from a remote dictionary ==========================

bool1 DIC_ReadNetworkDict2(UNS8 node_id, UNS16 wIndex, UNS8 bLocalSubindex, UNS8 bSubindex, UNS8 *status);
//this variant scans the own local dictionary to find the local data pointer, size and type (slower)

bool1 DIC_ReadNetworkDictDirect(void *pdata, UNS32 size, UNS8 dataType, UNS8 node_id, UNS16 wIndex, UNS8 bSubindex,UNS8 *status);
//this direct variant uses data pointer, size and type passed as arguments and is faster (no dictionary scan)
bool1 DIC_ReadNetworkDictDirect2(void *pdata, UNS32 size, UNS8 dataType, UNS8 node_id, UNS16 wIndex, UNS8 bSubindex, UNS8* status);

//====================== functions to write an object to a remote dictionary ==========================

bool1 DIC_WriteNetworkDict2(UNS8 node_id, UNS16 wIndex, UNS8 bLocalSubindex, UNS8 bSubindex, UNS8 *status);
//this variant scans the own local dictionary to find the local data pointer, size and type (slower)

bool1 DIC_WriteNetworkDictDirect(void *pdata, UNS32 size, UNS8 dataType, UNS8 node_id, UNS16 wIndex, UNS8 bSubindex, UNS8* status);
//this direct variant uses data pointer, size and type passed as arguments and is faster (no dictionary scan)

extern UNS8 DIC_NodeId;

#endif /* DICT_UTIL_H_ */
