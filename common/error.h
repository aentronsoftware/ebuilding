/***************************************************************************

   error.h - error handling

 ---------------------------------------------------------------------------
   Author: PTM: PT/RP        Date: 14.05.13
   For   : PTM
   Compil: CCSv5.2     Target: non specific
   descr.: handles system errors
   Edit  :

 **************************************************************************/


#ifndef _H_ERR_
#define _H_ERR_

/* exported constant definitions */
/* ============================= */
/* ERROR PRIORITY concept :
   MAX priority errors = Serious hardware or software failures
   ===========================================================
   MAX priority errors cannot be overwritten, even not by another MAX priority
   MAX priority errors can overwrite MID and STD errors
   
   MID priority errors = Exception errors
   ======================================
   MID priority errors can only be overwritten by MAX priority errors
   MID priority errors can overwrite STD errors

   STD priority errors = Positionning & Safety errors
   ==================================================
   STD priority errors can be overwritten by any other error
   STD priority errors can overwrite STD errors
*/

                           /* priority level */
#define ERRPRIO_MAX    3   /* Maximum error priority */
#define ERRPRIO_MID    2   /* Medium error priority */
#define ERRPRIO_STD    1   /* Standard error priority */
#define ERRPRIO_NUL    0   /* Not an error state */

/* error numbers */
#define ERR_NOERROR       0x00
#define ERR_CHIPSYS       0x01	// bit0 not included in xml
#define ERR_OVERCURRENT   0x02  // bit1		//OK 
#define ERR_OVERVOLTAGE   0x04	// bit2 	//OK	
#define ERR_UNDERVOLTAGE  0x08	// bit3  	//OK

#define ERR_CONTACTOR_WELDED_PLUS       0x10  //-- bit4 -- 00 02
#define ERR_CONTACTOR_WELDED_NEGATIVE   0x40  //-- bit6 -- 00 04

#define ERR_DIGOVLD       0x10  //contactor fault

#define ERR_TEMPERATURE   0x20	// bit5		//OK 
#define ERR_COMM          0x80	// bit7		//OK

#define WARN_TEMPERATURE  0x01  // bit0
#define WARN_INSULATION   0x02 // bit1 

//#define WARN_CRC          0x02
//#define WARN_RANGE        0x04
#define WARN_PARAMINIT     0x04	// bit2	
#define WARN_OVERCAPACITY 0x08  // bit3
#define WARN_SLEEP        0x10  // bit4
#define WARN_LOW_CAPACITY 0x20  // bit5
#define WARN_CURRENT      0x40  // bit6
#define WARN_CYCLE        0x80  // bit7

#define UNKNOWN_ERROR     255  /* Unknown error number            */

#define NO_WARNING          0
#define UNKNOWN_WARNING   255  /* Unknown warning number */


/* exported functions */
/* ================== */
bool1  ERR_ClearError(void);
/* clear the ERROR output + led, the ERR_ErrorNumber and REG_Status.BError

   CLEAR ERROR RESTRICTION
   Some errors cannot be cleared or need special conditions to be cleared.
   If the error could not be cleared the function returns FALSE
   
   MAX priority errors cannot be cleared, except ERR_WATCHDOG when ClearWDog
   is set in SysConfig2
   
*/
void  ERR_HandleErrorNb(uint8 errornum);
/* for startup errors only : call the error function corresponding
   to the given error number */
void ERR_DisplayError(uint8 error);

void ERR_HandleWarning(uint32 warningnum);
/* display a warning message */
bool1  ERR_ClearWarning(void);
/* clear the ODV_ErrorDsp_WarningNumber and REG_Status.BWarning */


/* these are the error functions */

/* serious hardware failures */
void ERR_ErrorDigOvld(void);
void WARN_ErrorParam(void);
void ERR_CONTACTOR_PLUS(void);
void ERR_CONTACTOR_NEG(void);

/* emergency & safety errors */
#define OVERCUR_DETECT
#define TEMP_SENSOR

#ifdef OVERCUR_DETECT
void ERR_ErrorOverCurrent(void);
#endif
#ifdef TEMP_SENSOR
void ERR_ErrorOverTemp(void);
#endif
void ERR_ErrorComm(void);
void WARN_Insulation(void);
void ERR_ErrorOverVoltage(void);
void ERR_ErrorUnderVoltage(void);
void ERR_SetError(uint32 error_code);

/* exported variables */        /* R = READ ONLY   RW = READ AND WRITE */
/* ================== */
                                   

#endif /* !_H_ERR_ */
/*****************************************************/
/* END OF ERROR.H */

