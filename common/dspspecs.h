/**********************************************************************

   dspspecs.h - hardware description file

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.: define hardware constants for C and assembler
   Edit  :

 *********************************************************************/


/* important note: since this header is used both by C and assembler
   modules, only the #ifdef and #if x conditions are allowed.
   #if x==y is not supported by the assembler ! */

#ifndef _H_HARDWARE_
#define _H_HARDWARE_

/// 35 develop normal Charge swicth
/// 36 no Charge_In Switch wait 1 min to charge again 



// 50 Delays in Temp Voltage current with deep discharge
#define NGTEST_VERSION 52  /* version of the NGtest runtime library */
/* Normal users should only edit the two first sections */
#define EQUALS       ==
#define HW_VERSION     68
/*===========================================================================
                    FLAGS FOR OPTIONAL CODE FEATURES
  ===========================================================================*/
#define WATCHDOG 0   /* This flag controls the generation of watchdog code :
                        0: no code, 1: code for firmware, 2: code for ROM */
#define CUR_AD_RESOL 4095  /* for 12 bit resolution current measurement */

/*===========================================================================
                    SELECTION OF HARDWARE PLATFORM
  ===========================================================================*/
#define AD_VOLTAGE 3300
#define REV_60_1A               /* revision card */

#define USB_VID_PHILIPS 0x0471
#define USB_PID_SMTEC   0x1A90
#define USB_PID_ELSIUM  0x1068
#define USB_PID_AENTRON 0x1069
/*===========================================================================
                   DESCRIPTION OF CLOCK AND INTERRUPTS
  ===========================================================================*/
#ifdef UC_TI_C28
  #define EnableAllInterrupts IER = 0x3FF; EINT
  #define EnableCaptureInterrupt IER = M_INT3; asm(" RPT #4 || NOP"); EINT
  #define EnableCaptureCurrentInterrupts IER = M_INT1 | M_INT11; EINT
  #define DisableAllInterrupts IER = 0

  #define InterruptDisable   DINT
  #define InterruptP2Disable DINT
  #define InterruptEnable    EINT
  #define InterruptP1Enable  EINT
/* TI_C28 operating mode */

//  #define UC_QUARTZ     16000000                 // quartz frequency
  #define UC_QUARTZ     10000000                 // quartz frequency
  #define LOW_CLOCK_PRESCALER 1                  // 1=sysclk/2 2=sysclk/4, 3=sysclk/6
  //#define LOW_CLOCK_PRESCALER 3                  // 2=sysclk/4, 3=sysclk/6
//  #define UC_DIV        10                       // PLL multiplicator
  #define UC_DIV        8                       // PLL multiplicator
  #define UC_PHI        (UC_QUARTZ * UC_DIV / 2) //SYSCLKOUT

  #define CPU_RATE   25.000L   // CPU_RATE = 1000.0 / fSYSCLKOUT(MHZ)
  extern void DSP28x_usDelay(unsigned long Count);
  #define DELAY_US(A)  DSP28x_usDelay(((((long double) A * 1000.0L) / (long double)CPU_RATE) - 9.0L) / 5.0L)
#endif //end #ifdef UC_TI_C28
  
/*===========================================================================
                   CURRENT & POSITION REGULATION
  ===========================================================================*/

#define CUR_FREQUENCY   12500   /* current loop sample frequency in Hz */
#define POS_FREQUENCY    1000   /* position loop sample frequency in Hz */

#define CUR_PERIOD  (1000000/CUR_FREQUENCY)   /* current loop sample period in us */
#define POS_PERIOD  (1000000/POS_FREQUENCY)   /* position loop sample period in us */

#define PWM_FREQ    80000      /* pulse width modulation frequecy in Hz */
#define PWM_COUNT   (UC_PHI/PWM_FREQ/2) /* max value of PWM counter = 499@80k, 1999@20k */

#define PWM_VALVE   5000      /* pulse width modulation frequecy in Hz */
#define PWM_LOCK    15000      /* pulse width modulation frequecy in Hz */
#define LOCK_COUNT  (UC_PHI/PWM_LOCK/2) /* max value of PWM counter = 499@80k, 1999@20k */
#define VALVE_COUNT 1000//(LOCK_COUNT*3) /* max value of PWM counter = 499@80k, 1999@20k */

#define POS_COUNT   (UC_PHI/POS_FREQUENCY) /* max value of PWM counter */

#define MEAS_FREQUENCY  2000 //16 measures @ 125Hz
#define MEAS_TB_CLK   (UC_PHI / 10) //CLKDIV = 1, HSPCLKDIV = 10
#define MEAS_COUNT (MEAS_TB_CLK / MEAS_FREQUENCY /2) //3125


/* the PWM period will be PWM_COUNT + 1 ! see HM page 329 */

//****************************************************************
//             TI C280x
//****************************************************************
#ifdef UC_TI_C28
  //POSITION
  #define ClearUTO       EQep1Regs.QCLR.bit.UTO = 1
  #define ClearINT       EQep1Regs.QCLR.bit.INT = 1
  #define GetPosition    EQep1Regs.QPOSLAT
  #define SetPosition(A) EQep1Regs.QPOSCNT = (long)A
  #define EnablePosIrq   PieCtrlRegs.PIEIER5.bit.INTx1  //EQEP1_INT
  #define SetPosIrq      EnablePosIrq = 1 
  #define ClearPosIrq    EnablePosIrq = 0
  //INTERRUPTS
  #define AcknowlegeInt  PieCtrlRegs.PIEACK.all
  #define EnableCPU0Irq  PieCtrlRegs.PIEIER1.bit.INTx7 = 1
  #define DisablePIE1    PieCtrlRegs.PIEIER1.all = 0
  #define DisablePIE3    PieCtrlRegs.PIEIER3.all = 0
  //ADs
  #define ReadCurrent0   AdcResult.ADCRESULT0
  #define ReadCurrent1   AdcResult.ADCRESULT1
  #define ReadCurrent2   AdcResult.ADCRESULT2
  #define ReadCurrent3   AdcResult.ADCRESULT3
  #define IsSafetyLine1Ok (AdcResult.ADCRESULT6 > 2048)
  #define IsSafetyLine2Ok (AdcResult.ADCRESULT7 > 2048)
  #define ClearSeq1      AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1
  #define ResetSeq1      AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1
  #define StartSeq2      AdcRegs.ADCTRL2.bit.SOC_SEQ2 = 1
  //Motor Driver
  #define NotAmpFault    GpioDataRegs.GPBDAT.bit.GPIO58   /* X2.16 *RIGHT/IRQ1 => Motor amplifier fault */
  #define AmpEnabled     GpioDataRegs.GPADAT.bit.GPIO13   /* X1.24 *IN3/IRQ6   => Enable for motor amplifier */
  #define EnableAmp		   GpioDataRegs.GPASET.all = BIT_13+BIT_14+BIT_15
  #define DisableAmp	   GpioDataRegs.GPACLEAR.all = BIT_13+BIT_14+BIT_15
  
  #define ToggleTestPin1 GpioDataRegs.GPBTOGGLE.bit.GPIO34 = 1
  #define ToggleTestPin2 //GpioDataRegs.GPATOGGLE.bit.GPIO17 = 1
  #define ToggleTestPin3 //GpioDataRegs.GPATOGGLE.bit.GPIO16 = 1
  #define ToggleErrorPin //GpioDataRegs.GPBTOGGLE.bit.GPIO34 = 1

/*===========================================================================
                         MISCELLANEOUS MACROS
  ===========================================================================*/
  // LSPCLK = SYSCLKOUT / (2 x LOSPCP)  (SPRU712d p47)
  // if modified recalculate baudrate for sci AND spi /!/
  #define SetClkPrescaler {\
    EALLOW;\
    SysCtrlRegs.LOSPCP.all = LOW_CLOCK_PRESCALER;\
    EDIS;\
    }
  #define ReadTemperature AdcResult.ADCRESULT0
  #define ReadVMOT        AdcResult.ADCRESULT7
  #define ReadIN1         AdcResult.ADCRESULT8
  #define ReadIN2         AdcResult.ADCRESULT9
  
  #define ReadGpio17      GpioDataRegs.GPADAT.bit.GPIO17
  
  #define NmiEnabled  FALSE
  #define SetNmi
  #define ClearNmi
  #define putstr(a) 
/* Watchdog control */
 #define DisableWDog  {\
    EALLOW;\
    SysCtrlRegs.WDCR = 0x0068; /*disable watchdog*/ \
    EDIS;\
    } 
 /* use CLR_WATCHDOG in waiting loops to refresh the watchdog */
 #define CLR_WATCHDOG
 #if WATCHDOG == 1
   #define CLR_WATCHDOG
 #else
   #if WATCHDOG == 2
     #define CLR_WATCHDOG
   #endif
 #endif
#endif //#ifdef UC_TI_C28

/*===========================================================================
                         EXPORTED FUNCTIONS
  ===========================================================================*/
void Delay_ms(long time);
//Set the duty cycle for the pwms
void HW_InitPwmVars(void);
//init compare A and compare B of epwm1,2&3 
void HW_SetOutput(uint16 pin);
//set output corresponding to the one selected in parameters
void HW_SetOutputByte(uint16 pin);
//set all outputs selected at once
void HW_ClearOutput(uint16 pin);
//clear output corresponding to the one selected in parameters
void HW_InitGpios(void);
//Initialize all gpios according to parameters
bool1 HW_GetInput(uint16 pin);
//get input
void HW_AcquireInputs(void);
//get all inputs
void HW_UpdateOutputs(void);
//update temporised outputs

#endif /* !_H_HARDWARE_ */
/*****************************************************/
/* end dspspecs.h */
