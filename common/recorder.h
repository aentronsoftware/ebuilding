/**********************************************************************

   recorder.h - definition of variables types and record functions

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 27.09.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.: definition of variable types and lists for easy interfacing 
	   with generic PC Graphic User Interface.
   Edit  :

 *********************************************************************/


#ifndef _H_RECORDER_
#define _H_RECORDER_

/* exported type definitions */
/* ========================= */

/* TYPE OF TVARIABLES */
/* ----------------- */
typedef struct  {
  uint16      Index;
  uint16      Subindex; //8 bits
  char*     Suffix;  //Concatenate with VariablePrefix from T_Unit
  uint16      MultiUnitTypeKey;
  //Value[base unit] = Value[internal unit]*gain + offset
  float*    Gain;   //gain to convert from "internal unit" in "base unit" 
  float*    Offset; //offset to convert from "internal unit" in "base unit"
}T_Variable;

typedef T_Variable T_Variables[];


/* TYPE OF TUNIT     */
/* ----------------- */

typedef struct {
  //Value[display unit] = Value[base unit]*gain + offset
  float *Gain;   //gain   to convert from "base unit" into "display unit" 
  float *Offset; //offset to convert from "base unit" into "display unit"
  uint32 *Modulo; //0 = no modulo, other = modulo for internal unit
  char * Name;   //name of the axis ie Current
  char * UnitName;  //name of the unit ie [mA]
}T_Unit;

typedef T_Unit T_MultiUnit[];

typedef struct {
  int  Size;        
  uint16 Key;
  T_MultiUnit *MultiUnit;
}T_MultiUnitType;

typedef T_MultiUnitType T_MultiUnitList[];

typedef struct  {
  int Size;
  T_MultiUnitList* Multi;
}T_UserMulti;

typedef struct  {
  int Size;
  T_Variables* Var;
}T_UserVar;

#define BUFF_SIZE           8192 //size of graph buffer in bytes only power of 2 allowed !!!
#define BUFF_SIZE_BIT      65536 //size of graph buffer in bits
#define RECORDER_ADDRESS  0x2005 //recorder index in the dictionary

typedef struct{
  UNS16 Pos_Record :1;
  UNS16 Cur_Record :1;
  UNS16 AutoTrigg  :1;
  UNS16 AutoRecord :1;
  UNS16 ContinuousRecord:1;
  UNS16 Trig_Record:1;
  UNS16 unused    :10;
}TControl;


#define POSITION_COUNTER_MAX 200 //ms


/* Standard unit key */
#define UNIT_TIME        0 //do not change 0 must be time !!
#define UNIT_CRT         1
#define UNIT_VOLT        2
#define UNIT_DERIVATE    3
#define UNIT_BAR         4
#define UNIT_POS         5
#define UNIT_CAPACITY    6
#define UNIT_TEMPS      10
#define UNIT_AD         11
#define UNIT_FLOW       12
#define UNIT_RAD        13
#define UNIT_SPEED      14
#define UNIT_TEMPER     15
#define UNIT_NONE       16
#define UNIT_POWER      17

/* Standard gain and offset */
extern const float VAR_GAIN_NORM;
extern const float VAR_OFFSET_NULL;
extern const float VAR_INC1;
extern const float VAR_INC0_1;
extern const uint32 VAR_MODULO_0;
extern const float gainvalve;
extern const float gainpack;
extern const float gaincell;

extern const T_MultiUnitList MultiUnitListe1;
extern const T_Variables Variables;

extern Uint32 BitIndex;
extern int64 REC_OldRecPoint;
extern int64 REC_RecPoint;
extern uint16  REC_PosSampleInterval; /* (R)W sampling interval in 2^x POS_PERIOD */
extern uint8  REC_RecordSize[7];          // Size of data stored in OBD_RecorderData1
extern long  *REC_RecordSource[6];  /* RW   pointer to the source of data to be recorded */

uint16 GetVarSize(void);
void AddData(void *data, uint16 nbbit);
void AddData2(void *data, uint16 nbbit);
uint16 PAR_AddVariables(uint16 nb);
uint16 PAR_AddMultiUnits(uint16 nb);
void REC_Record(void);
void REC_StartRecorder(void);
//pre configured record
void REC_RecordCurrent(uint16 CurToRecord);
/* Starts the data recorder for the current regulator. Data are recorded
   every CUR_PERIOD.
   The REG_CurToRecord variable determines which variables are recorded
   The default is REC_DUTY_CUR. If you need to record REC_DUTY_VEL_CUR change
   REG_CurToRecord before calling the function and restore it afterwards.
*/
void REC_RecordPosition(uint16 PosToRecord);
/* Starts the data recorder for the position regulator. Data are recorded
   every "REC_PosSampleInterval" of POS_PERIOD.
   The REG_PosToRecord variable determines which variables are recorded
*/


#endif

