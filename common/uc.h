/**********************************************************************

   uc.h - includes file

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.: define includes files to add for each processor
   Edit  :

 *********************************************************************/


#ifndef _H_UC_
#define _H_UC_
  #include "processor.h"
  #ifdef PROC_COM
	  #define UC_TI_STELLARIS
	  #include "ptmtype.h"
  #endif
	#ifdef PROC_DSP
	  #define UC_TI_C280
    #define UC_TI_C28
	  #include "ptmtype.h"
    #include <tistdtypes.h>
	  #include "F2806x_Device.h"
	#endif
#endif /* !_H_UC_ */
/*****************************************************/
/* END OF UC.H */
