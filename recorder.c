/***************************************************************************

   recorder.c - definition of variables types and record function
 ---------------------------------------------------------------------------
   Author: PTM: PT/RP        Date: 27.09.12
   For   : PTM
   Compil: TI CCS5.2
   Target: any
   descr.: definition of variable types and lists for easy interfacing with
           generic PC Graphic User Interface.
   Edit  :

 ***************************************************************************/

#include "uc.h"
#include "dspspecs.h"
#include "powerman.h"
#include "mms_dict.h"
#include "convert.h"
#include "recorder.h"

const float gain3  = 3.0;
const float gainlv25 = 0.19553;
const float gainhas500  = 3000.0/32767.0;
const float gainshunt  = 8000.0/32767.0;
const float gain0_25  = 0.25;
const float gain0_125  = 0.125;
const float gain0_0625  = 0.0625;
const float gain0_001= 0.001;
const float gain_1000  = 1000.0;
const float gainvalve  = (float)3300/4095/0.5;
const float gainI2  = 1.0/3.2;
const float gain01  = 0.1;
const float inc0001 = 0.001;
const float gainsec = 1.0/60;
const float gaindelta = 20.0;
const float gaincell = 5000.0/32767.0;
const float gainpack = 125000.0/65535.0; //gaincell*25

const float gain_speed = 1000.0/POSITION_COUNTER_MAX;
const float gain_rpm = 15.0;
const float offsettemper = -50.0;
const float offsetad2 = 2048.0;
const float VAR_GAIN_NORM   = 1.0;
const float VAR_OFFSET_NULL = 0.0;
const float VAR_INC1        = 1.0;
const float VAR_INC0_1      = 0.1;
const uint32 VAR_MODULO_0   = 0.0;

const T_MultiUnit MultiUnite1 =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Current","[A]"}
,{(float*)&gain_1000,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Current","[mA]"}
};

const T_MultiUnit MultiUnitPow =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Power","[W]"}
//,{(float*)&gain0_001,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Courant","[A]"}
};

const T_MultiUnit MultiUnite2 =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Position","[qc]"}
,{(float*)&CNV_DegUnit,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Position","[Deg]"}
};

const T_MultiUnit MultiUnitDerivate =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"current slope","[mA/time]"}
,{(float*)&gain0_001,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"current slope","[A/time]"}
};

const T_MultiUnit MultiUnite3 =
{{(float*)&gain0_001,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Voltage","[V]" }
,{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Voltage","[mV]"}
};

const T_MultiUnit NoMultiUnit =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"No Unit","[-]"}
};

const T_MultiUnit ADMultiUnit =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"AD","[incAD]"}
,{(float*)&gainvalve,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Current","[mA]"}
};

const T_MultiUnit RadMultiUnit =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"angle","[rad]"}
};

const T_MultiUnit TimeMultiUnit =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Time","[ms]"}
,{(float*)&gain0_001,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Time","[s]"}
,{(float*)&gain_1000,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Time","[us]"}
};

const T_MultiUnit SpeedMultiUnit =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Speed","[qc/s]"}
,{(float*)&gain_rpm,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Speed","[rpm]"}
};

const T_MultiUnit TempMultiUnit =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Temperature","[�C]"}
};

const T_MultiUnit CapacityMultiUnit =
{{(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL,(uint32*)&VAR_MODULO_0,"Capacity","[Ah]"}
};

const T_MultiUnitList MultiUnitListe1 =
    /* array containing all the multiunits */
{{sizeof(MultiUnite2)/sizeof(T_Unit),UNIT_POS ,(T_MultiUnit*)MultiUnite2}
,{sizeof(MultiUnite1)/sizeof(T_Unit),UNIT_CRT ,(T_MultiUnit*)MultiUnite1}
,{sizeof(MultiUnite3)/sizeof(T_Unit),UNIT_VOLT,(T_MultiUnit*)MultiUnite3}
,{sizeof(NoMultiUnit)/sizeof(T_Unit),UNIT_NONE,(T_MultiUnit*)NoMultiUnit}
,{sizeof(ADMultiUnit)/sizeof(T_Unit),UNIT_AD,(T_MultiUnit*)ADMultiUnit}
,{sizeof(TimeMultiUnit)/sizeof(T_Unit),UNIT_TEMPS,(T_MultiUnit*)TimeMultiUnit}
,{sizeof(MultiUnitDerivate)/sizeof(T_Unit),UNIT_DERIVATE,(T_MultiUnit*)MultiUnitDerivate}
,{sizeof(RadMultiUnit)/sizeof(T_Unit),UNIT_RAD,(T_MultiUnit*)RadMultiUnit}
,{sizeof(SpeedMultiUnit)/sizeof(T_Unit),UNIT_SPEED,(T_MultiUnit*)SpeedMultiUnit}
,{sizeof(TempMultiUnit)/sizeof(T_Unit),UNIT_TEMPER,(T_MultiUnit*)TempMultiUnit}
,{sizeof(MultiUnitPow)/sizeof(T_Unit),UNIT_POWER,(T_MultiUnit*)MultiUnitPow}
,{sizeof(CapacityMultiUnit)/sizeof(T_Unit),UNIT_CAPACITY,(T_MultiUnit*)CapacityMultiUnit}
};

const T_Variables Variables =
    /* array containing all the graph vars */
{{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_16,"NTC-2",UNIT_TEMPER,&ODP_Analogue_Input_Scaling_Float[1],(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_15,"NTC-1",UNIT_TEMPER,&ODP_Analogue_Input_Scaling_Float[1],(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_21,"NTC-3",UNIT_TEMPER,&ODP_Analogue_Input_Scaling_Float[1],(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_18,"Mos Temp",UNIT_TEMPER,&ODP_Analogue_Input_Scaling_Float[0],(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_22,"Res Temp",UNIT_TEMPER,&ODP_Analogue_Input_Scaling_Float[0],(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Module1_Voltage,"M_Voltage",UNIT_VOLT,(float*)&ODP_Analogue_Input_Scaling_Float[2],(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Module1_Current,"M_Current",UNIT_CRT,&ODP_Analogue_Input_Scaling_Float[3],(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Module1_Temperature,"M_Temperature",UNIT_TEMPER,(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Module1_Temperature2,"M_Temperature2",UNIT_TEMPER,(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Module1_Throughput,"M_Power",UNIT_POWER,(float*)&gain0_0625,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Module1_MinCellVoltage,"MinCell_Voltage",UNIT_VOLT,(float*)&gaindelta,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Module1_MaxCellVoltage,"MaxCell_Voltage",UNIT_VOLT,(float*)&gaindelta,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Module1_MaxDeltaVoltage,"DeltaCell_Voltage",UNIT_VOLT,(float*)&gaindelta,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Module1_Capacity_Used,"Used Capacity",UNIT_CAPACITY,(float*)&gain01,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_23,"Relay current1",UNIT_AD,&ODP_Analogue_Input_Scaling_Float[6],(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Write_Analogue_Output_16_Bit_Analogue_Output_1,"12V",UNIT_VOLT,(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_1,"Cell1",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_2,"Cell2",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_3,"Cell3",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_4,"Cell4",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_5,"Cell5",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_6,"Cell6",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_7,"Cell7",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_8,"Cell8",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_9,"Cell9",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_10,"Cell10",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_11,"Cell11",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_12,"Cell12",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_13,"Cell13",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Analogue_Input_16_Bit_Analogue_Input_14,"Cell14",UNIT_VOLT,(float*)&gaincell,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Gateway_Power,"G_Power",UNIT_POWER,(float*)&gain0_0625,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Gateway_State,"G_State",UNIT_NONE,(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Inputs_16_Bit_Read_Inputs_0x1_to_0x10,"Digital in 1..16",UNIT_NONE,(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_Read_Inputs_16_Bit_Read_Inputs_0x11_to_0x20,"balancing 1..16",UNIT_NONE,(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL}
,{ODA_VAR_SysTick_ms,"OnTime",UNIT_TEMPS,(float*)&VAR_GAIN_NORM,(float*)&VAR_OFFSET_NULL}
};

//locals
uint16 DataIndex = 0;
Uint32 BitIndex = 0;
uint64  RecordOffset[6];     // if data is signed then offset is 2^(size-1)
//globals
int64   REC_OldRecPoint = 0;
int64   REC_RecPoint = 0;
uint8   REC_RecordSize[7];        // Size of data stored in OBD_RecorderData1
int32  *REC_RecordSource[6];     /* RW   pointer to the source of data to be recorded */
uint16  REC_PosSampleInterval = 1;      /* (R)W measuring interval in interrupt period */


void AddByte(uint8 data);
void AddWord(uint16 data);
void AddLWord(uint32 data);

/******************************************************************************
                             RECORDING FUNCTIONS
*******************************************************************************/

void REC_Record(void){
  uint16 size;
  if ((ODV_SysTick_ms % REC_PosSampleInterval) == 0)
  {
    if (ODV_Recorder_NbOfSamples == 0) // first measure /
    {
      if (ODV_Recorder_Control.Trig_Record) {
        size = (ODV_Recorder_Vectors>>48);
        if ((long)REC_RecordSource[size & 0x7] & 1)
          REC_RecPoint = (*(UNS16*)REC_RecordSource[size & 0x7] & 0xFFFF);
        else
          REC_RecPoint = (*(UNS64*)REC_RecordSource[size & 0x7] & (((UNS64)1<<REC_RecordSize[size & 0x7])-1));
        if (RecordOffset[size & 0x7]){//if signed
          if (REC_RecPoint > RecordOffset[size & 0x7]) REC_RecPoint -= RecordOffset[size & 0x7]<<1;
        }
        if (ODV_Recorder_Start == 0) {
          if (BitIndex >= ODV_Recorder_PreTrigger*REC_RecordSize[6])
            ODV_Recorder_Start = 0xFFFF;
        }
        else if (((REC_RecPoint >= ODV_Recorder_TriggerLevel) && (REC_OldRecPoint < ODV_Recorder_TriggerLevel) && (size & 0x8))
            || ((REC_RecPoint <= ODV_Recorder_TriggerLevel) && (REC_OldRecPoint > ODV_Recorder_TriggerLevel) && (size & 0x10))) {
          ODV_Recorder_Control.Trig_Record = FALSE;
          //ODV_Recorder_ReadIndex = BitIndex>>3;
          ODV_Recorder_TriggerIndex = (BitIndex + BUFF_SIZE_BIT - ODV_Recorder_PreTrigger*REC_RecordSize[6]) & (BUFF_SIZE_BIT-1);
          ODV_Recorder_NbOfSamples = ODV_Recorder_PreTrigger;
          ODV_Recorder_ReadIndex = ODV_Recorder_TriggerIndex>>3;
        }
        REC_OldRecPoint = REC_RecPoint;
      }
    }
    //size = REC_RecordSize[0]+REC_RecordSize[1]+REC_RecordSize[2]+REC_RecordSize[3]+REC_RecordSize[4]+REC_RecordSize[5];
    AddData(REC_RecordSource[0],REC_RecordSize[0]);
    AddData(REC_RecordSource[1],REC_RecordSize[1]);
    AddData(REC_RecordSource[2],REC_RecordSize[2]);
    AddData(REC_RecordSource[3],REC_RecordSize[3]);
    AddData(REC_RecordSource[4],REC_RecordSize[4]);
    AddData(REC_RecordSource[5],REC_RecordSize[5]);
    if (!ODV_Recorder_Control.Trig_Record) {
      if ((BitIndex >= BUFF_SIZE_BIT-REC_RecordSize[6]) && (!ODV_Recorder_Control.ContinuousRecord))
        ODV_Recorder_Control.Pos_Record = FALSE; //last measure
      if ((ODV_Recorder_Control.ContinuousRecord) && ((BitIndex>>3)<=((ODV_Recorder_ReadIndex+BUFF_SIZE) & (BUFF_SIZE-1)))
          && ((((BitIndex+REC_RecordSize[6])>>3) & (BUFF_SIZE-1)) >= ODV_Recorder_ReadIndex))
        ODV_Recorder_Control.Pos_Record = FALSE;
      ++ODV_Recorder_NbOfSamples;
    }
  }
}

/********************************  RECORDER SETUP FUNCTION **********************************/
/*
 ** REC_StartRecorder
 * configure the recorder and start it
 */
void REC_StartRecorder(void){
  uint16 i, vec;
  UNS32 size;
  UNS8  type;

  REC_RecordSize[6]=0;
  for (i=0; i<6; i++) {
    vec = (ODV_Recorder_Vectors>>(i*8)) & 0xFF;
    size = 0;
    RecordOffset[i] = 0;
    if (vec == 0)
    {
      REC_RecordSize[i] = 0;
    }
    else {
      if (--vec < GetVarSize())
        _getODentry(BoardODdata,Variables[vec].Index,Variables[vec].Subindex,
                   (void*)&REC_RecordSource[i],
                   &size,&type,0,0,GET_OD_ENTRY_MODE_1);
      if (ODV_Recorder_Period < 1000) {
        switch (type) {
          case DATA_TYPE_BOOLEAN:
          case DATA_TYPE_INT8:
          case DATA_TYPE_UINT8:  REC_RecordSize[i] = 8; break;
          case DATA_TYPE_UINT12:
          case DATA_TYPE_INT16:
          case DATA_TYPE_UINT16: REC_RecordSize[i] = 16; break;
          case DATA_TYPE_INT32:
          case DATA_TYPE_UINT32:
          case DATA_TYPE_REAL32: REC_RecordSize[i] = 32; break;
          case DATA_TYPE_REAL64:
          case DATA_TYPE_INT64:
          case DATA_TYPE_UINT64: REC_RecordSize[i] = 64; break;
          default: REC_RecordSize[i] = 0; break;
        }
      }else{
        switch (type) {
          case DATA_TYPE_BOOLEAN: REC_RecordSize[i] = 1; break;
          case DATA_TYPE_INT8:   RecordOffset[i] = 128;
          case DATA_TYPE_UINT8:  REC_RecordSize[i] = 8; break;
          case DATA_TYPE_UINT12: REC_RecordSize[i] = 12; break;
          case DATA_TYPE_INT16:  RecordOffset[i] = 32768;
          case DATA_TYPE_UINT16: REC_RecordSize[i] = 16; break;
          case DATA_TYPE_INT32:  RecordOffset[i] = 2147483648;
          case DATA_TYPE_UINT32:
          case DATA_TYPE_REAL32: REC_RecordSize[i] = 32; break;
          case DATA_TYPE_INT64:  RecordOffset[i] = 9223372036854775808;
          case DATA_TYPE_REAL64:
          case DATA_TYPE_UINT64: REC_RecordSize[i] = 64; break;
          default: REC_RecordSize[i] = 0; break;
        }
      }
    }
    REC_RecordSize[6] += REC_RecordSize[i];
  }
  ODV_Recorder_Control.Cur_Record = 0;
  ODV_Recorder_Control.Pos_Record = 0;
  if (ODV_Recorder_Start != 0xFFFF) {
    ODV_Recorder_NbOfSamples = 0;
    ODV_Recorder_ReadIndex = 0;
    BitIndex = 0;
    ODV_Recorder_TriggerIndex = 0;
    if (ODV_Recorder_Control.AutoTrigg) ODV_Recorder_Control.Trig_Record = 1;
    if (ODV_Recorder_Period < POS_PERIOD) {
      REC_PosSampleInterval = ODV_Recorder_Period / CUR_PERIOD;
      ODV_Recorder_Control.Cur_Record = 1;
    }
    else{
      REC_PosSampleInterval = ODV_Recorder_Period / POS_PERIOD;
      ODV_Recorder_Control.Pos_Record = 1;
    }
  }
}

/*
 ** AddData
 *
 *  PARAMETERS: data : pointer on the data
 *              nbbit : number of bit to add
 *  DESCRIPTION: add nbbits to the buffer
 *
 *  RETURNS: none
 *
 */

#pragma CODE_SECTION(AddData,"ramfuncs")
void AddData(void *data, uint16 nbbit)
{
  union U_LongLong{
    UNS64 datall;
    UNS16 dataw[4];
  };
  union U_LongLong ldatal;
//  UNS64 mask = 1 << (nbbit-1);
  UNS16 index;
  UNS16 decal;
  ldatal.datall = *(UNS64*)data;
  if (((long)data & 0x1) == 1)//for vars <=16bits on odd address corrects misalignement error
    ldatal.datall >>= 16;
  ldatal.datall <<= 64 - nbbit;
  while (nbbit > 0) {
    index = BitIndex/16;
    decal = 15 - (BitIndex & 15);
    if (ldatal.dataw[3] & 0x8000)
      ODV_RecorderData1[index] |= 1<<decal;
    else
      ODV_RecorderData1[index] &= ~(1<<decal); //erase bit where we're going to write
    --nbbit;
    ldatal.datall <<= 1;
    if (++BitIndex == BUFF_SIZE_BIT) BitIndex = 0;
  }
}

/*
 ** AddData2
 *
 *  PARAMETERS: data : pointer on the data
 *              nbbit : number of bit to add
 *  DESCRIPTION: add 8, 16 or 32 bits to the buffer
 *  same as AddData but optimized for speed and with reduced functionality
 *
 *  RETURNS: none
 *
 */
#pragma CODE_SECTION(AddData2,"ramfuncs")
void AddData2(void *data, uint16 nbbit)
{
  UNS32 ldata = *(UNS32*)data;
  UNS16 index = BitIndex/16;
  if ((long)data & 1) ldata >>= 16;
  if (nbbit <= 8) {
    if (BitIndex & 0x8)
      ODV_RecorderData1[index] += ldata & 0xFF;
    else
      ODV_RecorderData1[index] = (ldata<<8);
    BitIndex += 8;
  }
  else if (nbbit <= 16) {
    if (BitIndex & 0x8) {
      ODV_RecorderData1[index] += (ldata>>8);
      ODV_RecorderData1[index+1] = (ldata<<8);
    }
    else
      ODV_RecorderData1[index] = ldata & 0xFFFF;
    BitIndex += 16;
  }
  else {
    if (BitIndex & 0x8) {
      ODV_RecorderData1[index] += (ldata>>24);
      ODV_RecorderData1[index+1] = (ldata>>8);
      ODV_RecorderData1[index+2] = (ldata<<8);
    }
    else {
      ODV_RecorderData1[index] = (ldata>>16);
      ODV_RecorderData1[index+1] = ldata & 0xFFFF;
    }
    BitIndex += 32;
  }
  if (BitIndex >= BUFF_SIZE_BIT) BitIndex -= BUFF_SIZE_BIT;
}

/* *************************************************** */
/* functions for adding units and vars into the buffer */
/* *************************************************** */

void AddByte(uint8 data)
{
  data &= 0xFF; //to be sure that high uint8 is 0 because a uint8 is stored in 16bits on dsp28x
  if (DataIndex & 1)
    ODV_RecorderData1[DataIndex/2] += (data<<8);
  else
    ODV_RecorderData1[DataIndex/2] = data;
  DataIndex++;
}

void AddWord(uint16 data)
{
  if (DataIndex < sizeof(ODV_RecorderData1)*2) {
    if (DataIndex & 1)
    {
      ODV_RecorderData1[DataIndex/2] += (data<<8);
      DataIndex++;
	  ODV_RecorderData1[DataIndex/2] = (data>>8);
	  DataIndex++;
    }
    else
    {
      ODV_RecorderData1[DataIndex/2] = data;
      DataIndex += 2;
    }
  }
}

void AddLWord(uint32 data)
{
  uint16 wdata;
  wdata = data & 0xFFFF;
  AddWord(wdata);
  wdata = data / 0x10000;
  AddWord(wdata);
}


void AddFloat(float fdata)
{
  uint32 data;
  data = *(uint32*)&fdata;
  AddLWord(data);
}

void AddString(char* mystr)
/* add a string */
{
  char ch;
  do
  {
    ch = *mystr++;
    AddByte(ch);
  } while ((ch != 0) && (DataIndex < sizeof(ODV_RecorderData1)*2));
}

uint16 PAR_AddVariables(uint16 nb)
{
  uint8 nb1, nb2, i;
  T_Variable *Var1;
  T_UserVar *uservar;
  nb1 = 0;
  nb2 = sizeof(Variables)/sizeof(T_Variable);
  DataIndex = 0;
  i = 0;
  if (nb == 0)
  {
    do {
      Var1 = (T_Variable*)Variables+i;
      AddWord(Var1->Index);
      AddWord(Var1->Subindex);
      AddString(Var1->Suffix);
      AddWord(Var1->MultiUnitTypeKey);
      AddFloat(*Var1->Gain);
      AddFloat(*Var1->Offset);
      i++;
    }while(i<nb2);
    if (nb1) {
      i = 0;
      do {
        Var1 = (T_Variable*)uservar->Var+i;
        AddWord(Var1->Index);
        AddWord(Var1->Subindex);
        AddString(Var1->Suffix);
        if (Var1->MultiUnitTypeKey < 100)
          AddWord(Var1->MultiUnitTypeKey+0x100);
        else
          AddWord(Var1->MultiUnitTypeKey);
        AddFloat(*Var1->Gain);
        AddFloat(*Var1->Offset);
        i++;
      }while(i<nb1);
    }
  }
  return DataIndex;
}

uint16 PAR_AddMultiUnits(uint16 nb)
{
  uint8 nb1, nb2, i, j;
  int size;
  T_Unit *Unit;
  T_UserMulti *usermulti;
  T_MultiUnitType *multiunit;
  DataIndex = 0;
  nb1 = 0;
  nb2 = sizeof(MultiUnitListe1)/ sizeof(T_MultiUnitType);
  i=0;
  multiunit = (T_MultiUnitType*)MultiUnitListe1;
  if (nb == 0) {
    do {
      size = multiunit[i].Size;
      AddWord(size);
      AddWord(multiunit[i].Key);
      for (j=0; j<size; j++) {
        Unit = (T_Unit*)multiunit[i].MultiUnit+j;
        AddFloat(*Unit->Gain);
        AddFloat(*Unit->Offset);
        AddLWord(*Unit->Modulo);
        AddString(Unit->Name);
        AddString(Unit->UnitName);
      }
      i++;
    } while (i<nb2);
    if (nb1) {
      i=0;
      multiunit = (T_MultiUnitType*)usermulti->Multi;
      do {
        size = multiunit[i].Size;
        AddWord(size);
        if (multiunit[i].Key < 100)
          AddWord(multiunit[i].Key+0x100);
        else
          AddWord(multiunit[i].Key);
        for (j=0; j<size; j++) {
          Unit = (T_Unit*)multiunit[i].MultiUnit+j;
          AddFloat(*Unit->Gain);
          AddFloat(*Unit->Offset);
          AddLWord(*Unit->Modulo);
          AddString(Unit->Name);
          AddString(Unit->UnitName);
        }
        i++;
      } while (i<nb1);
    }
  }
  return(DataIndex);
}

uint16 GetVarSize(void)
{
  return(sizeof(Variables)/sizeof(T_Variable));
}
