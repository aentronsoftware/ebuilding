/***************************************************************************

   convert.h - MINIPOS library: number and unit conversion

 ---------------------------------------------------------------------------
   Author: PTM: PT/OT        			Date: Thu 28.09.1995
   For   : Interelectric AG CH-6072 Sachseln
   Compil: IAR H8-300H V3.23A/DXT     Target: MIP-10, MIP-0
   descr.: number and unit conversion
   Edit  :

 **************************************************************************/
#ifndef _H_CONVERT_
#define _H_CONVERT_

#define PI        3.14159265359

/* Exported vars*/
extern volatile float CNV_CurrentUnit;
extern volatile int   CNV_CurrentRange;
extern float CNV_CoupleUnit;
extern float CNV_DegUnit;
extern int   CNV_ADPRange;
extern int   CNV_ADMRange;
extern int   CNV_ISafe5s;
extern int   CNV_ISafe0s2;

/* Number conversion functions */
/* =========================== */
long CNV_Round(float arg);
/* rounds the argument to the nearest integer value */
uint16 CNV_SwapWord(uint16 w);
/* swap the bytes of w */
uint32 CNV_SwapLWord(uint32 lw);
/* swap the bytes of lw */

/* Unit conversion functions */	   /* T = sample period */
/* ========================= */
int   CNV_Convert_mAmp2Sys(int milliampere);
/* convert a current in [mA] to a current in [A/D inc] */
int   CNV_Convert_Sys2mAmp(int current);
/* convert a current in [A/D inc] to a current in [mA] */
long  CNV_Convert_Sec2Sys(float second);
/* convert a time [s] to a time in [T] */
float CNV_Convert_Sys2Sec(long time);
/* convert a time in [T] to a time [s] */
long CNV_Convert_0_01Deg2Inc(long angle);
/* convert a angle [0.01deg] to a number of increments [inc] */
long CNV_Convert_Inc20_01Deg(long inc);
/* convert a number of increments [inc] to a angle [deg] */
float CNV_Convert_RpM2Sys(float velocity);
/* convert a velocity in [rpm] to a velocity in [inc/T] */
float CNV_Convert_Sys2RpM(float velocity);
/* convert a velocity in [inc/T] to a velocity in [rpm] */
float CNV_Convert_AccTime2Acc(uint16 profiletype, float vmax, float acctime);
/* convert a acceleration time (0 -> vmax in [ms]) to acc in [qc/ms*ms]
   profiletype = PARABOLIC or CYCLOIDAL */
float CNV_Convert_Acc2AccTime(uint16 profiletype, float vmax, float acc);
/* convert internal acc value [qc/ms*ms] to acc.time [ms] (0 -> vmax) */
int CNV_Convert_mNm2Sys(int torque);
/* convert a torque in [mNm] to a current in [S.U] */
int CNV_Convert_Sys2mNm(int current);
/* convert a current in [SU] to a torque in [mNm] */
float CNV_Convert_DegParSecCarre2AccelSys(float accel);
float CNV_Convert_AccelSys2DegParSecCarre(float accel);
float CNV_Convert_DegParSec2SpeedSys(float speed);
float CNV_Convert_SpeedSys2DegParSec(float speed);
int CNV_Convert_qc2mNm(int position);
long CNV_Convert_Deg2Inc(float degree);
float CNV_Convert_Inc2Deg(long inc);

#endif /* !_H_CONVERT_ */
/* END OF CONVERT.H */
