 /**********************************************************************

   sci1.h - hardware description file

 ----------------------------------------------------------------------
   Author: PTM: RP                     Date: 09.07.14
   For   : Smtec
   Compil: CCSv5     Target: DSP F2806x
   descr.: define hardware constants for C and assembler
   Edit  :
 ******************************************************************************/


#ifndef _H_SCI_
#define _H_SCI_

/* baudrate : 19200,38400,57600   */
/*      new : 100000, 125000, 250000, 500000 */
/* mode : N81,E81,O81,N71,E71,O71 */


#define SCI_N81 0x07
/*#define SCI_E81 0x37
#define SCI_O81 0x27
#define SCI_N71 0x06
#define SCI_E71 0x36
#define SCI_O71 0x26
#define SCI_MIB 0x0F*/

#define BAUD_9600   0  /* index of default baudrate */
#define BAUD_19200  1  /* index of default baudrate */
#define BAUD_38400  2
#define BAUD_57600  3
#define BAUD_115200 4
#define BAUD_230400 5
#define BAUD_250000 6
#define BAUD_500000 7
#define MAXBAUD     8  /* number of baudrate entries */

// State name for the reception irq state machine
#define  STATE_RX_IDLE                0
#define  STATE_RX_WAIT_CMD            1
#define  STATE_RX_WAIT_DATA_TO_CRC    2
#define  STATE_RX_CKS                 3
#define  STATE_TX_WAIT_PACKET_END   100

#define RS_232_TIMEOUT  10 //ms

typedef struct 
{ long baud;
  uint8 n;
  uint8 brr;
} T_BaudCfg;

#define MAX_DATA 70 //10 word + 2 crc
#define MAX_PACKET_SIZEW (MAX_DATA/2-1)
typedef struct {
  uint8 cmd;
  uint8 len;
  uint8 data[MAX_DATA];
} T_UsbMessage;


/* Masks for the communication control */
#define SCI_SMR_INIT       SCI_MIB /* Multiprocessor mode        */
#define SCI_SCR_INIT       0x10 /* 0001'0000 Receiver enabled           */
#define SCI_SSR_INIT       0x80 /* 1000'0000 Clear all except TDRE      */
#define DMA_DTCR_INIT_REC  0x05 /* 0000'0101 Receiver (101)             */
#define DMA_DTCR_INIT_TRS  0x04 /* 0000'0100 Sender   (100)             */
#define SCI_ERROR_MSK      0x78 /* 0111'1000 Error & RDFR flags in SSR  */
#define ENABLED_MSK        0x01 /* Enabled bit in MIB_Status            */

/************** COMMAND NUMBER **************/
#define CMD_READ_CRC             0x41
#define CMD_READ_FAULT           0x42
#define CMD_READ_CHIP            0x43
#define CMD_READ_CTO             0x44
#define CMD_READ_ERROR           0x45
#define CMD_SET_CHANNELS         0x46
#define CMD_READ_VALUES          0x47
#define CMD_CLEAR_ERROR          0x48
#define CMD_SET_GPIO             0x49
#define CMD_CLEAR_GPIO           0x50
#define CMD_SET_UNDERCELL        0x51
#define CMD_WRITE_EEPROM         0x52
#define CMD_READ_EEPROM_C        0x53
#define CMD_ERROR                0xFF

#define BOOT_MODE_PROGRAMM_OK   0
#define BOOT_MODE_PROGRAMM_NONE 1
#define FIRMWARE_MODE           2

//****************************************************************
//             TI C280x
//****************************************************************
#ifdef UC_TI_C28
//See SPRU078A p6.23
  #define SetSCI0IrqMipbus {\
    EALLOW;\
    PieVectTable.SCIRXINTA = &RS232_RXI_Int;\
    EDIS;\
    }
  //Disable receiver and transmitter
  #define SCI0_Disable SciaRegs.SCICTL1.all =0x0000
  #define SCI0_EnableReception SciaRegs.SCICTL1.all =0x0021
  #define SCI0_MIBFormatInit SciaRegs.SCICCR.all = SCI_MIB 
  // Define baudrate SCIHBAUD and SCILBAUD p2.8
  // Baudrate = LSPCLK /((BRR+1)*8)
  // BRR = LSPCLK /(Baudrate*8) - 1
  #define SCI0_SetBaudrate {\
    SciaRegs.SCIHBAUD    =BaudTable[baudindex].n;\
    SciaRegs.SCILBAUD    =BaudTable[baudindex].brr;\
    }
  #define SCI0_DisableRecIrq SciaRegs.SCIFFRX.bit.RXFFIENA = 0;\
    SciaRegs.SCICTL1.bit.RXERRINTENA = 0 
  #define SCI0_DisableRec SciaRegs.SCICTL1.bit.RXENA = 0
  #define SCI0_EnableRecIrq  SciaRegs.SCICTL1.bit.RXERRINTENA = 1;\
                             SciaRegs.SCIFFRX.bit.RXFFIENA = 1
  #define SCI0_EnableRec SciaRegs.SCICTL1.bit.RXENA = 1
  #define SCI0_DisableTransmitIrq SCI0_DisableRecIrq
  //we use the reception irq with the RX internally connect to TX
  //to know exactly the end of the transmission
  #define SCI0_EnableTransmitIrq SCI0_EnableRecIrq 

  #define SCI0_DisableTransmit SciaRegs.SCICTL1.bit.TXENA = 0
  #define SCI0_EnableTransmit  SciaRegs.SCICTL1.bit.TXENA = 1

  #define SCI0_DisableTransmitEndIrq SCI0_DisableRecIrq
  #define SCI0_EnableTransmitEndIrq SCI0_EnableRecIrq 

  #define SCI0_ClearError {\
    SciaRegs.SCICTL1.bit.SWRESET = 0;         \
    SciaRegs.SCIFFTX.bit.TXFIFOXRESET = 0;    \
    SciaRegs.SCIFFRX.bit.RXFIFORESET = 0;     \
    SciaRegs.SCICTL1.bit.SWRESET = 1;         \
    SciaRegs.SCIFFTX.bit.TXFIFOXRESET = 1;    \
    SciaRegs.SCIFFRX.bit.RXFIFORESET = 1;     \
    }
  #define SCI0_WaitAddressByte  SciaRegs.SCICTL1.bit.SLEEP = 1
  #define SCI0_WriteAddressByte SciaRegs.SCICTL1.bit.TXWAKE = 1
  #define SCI0_WaitDataBytes    SciaRegs.SCICTL1.bit.SLEEP = 0
  #define SCI0_WriteDataByte    SciaRegs.SCICTL1.bit.TXWAKE = 0
  #define SCI0_RecChar          SciaRegs.SCIRXBUF.all
  #define SCI0_TransmitChar     SciaRegs.SCITXBUF
  #define SCI0_TransmitRegEmpty (SciaRegs.SCIFFTX.bit.TXFFST ==0)
  #define SCI0_TransmitShiftRegEmpty SciaRegs.SCICTL2.bit.TXEMPTY

  #define SCI0_RecFifoLevel   SciaRegs.SCIFFRX.bit.RXFFIL
  #define SCI0_RecFifoLevel1  SCI0_RecFifoLevel = 1
  #define SCI0_RecFifoLevel16 SCI0_RecFifoLevel = 16
  #define SCI0_ConnectTXtoRX {\
    SciaRegs.SCICCR.bit.LOOPBKENA = 1;\
    SciaRegs.SCICTL1.bit.RXENA = 1;\
    }
  #define SCI0_DisconnectTXtoRX SciaRegs.SCICCR.bit.LOOPBKENA = 0
  #define SCI0_WaitAckSended {while(RS232RecvState == STATE_RX_WAIT_ACK_SENDED);}
  #define TransmitCommAddr SCI_CharPtr
  #define TransmitCommLen  SCI_CharLen
  #define SCI0_RxError          SciaRegs.SCIRXST.bit.RXERROR
  #define SCI0_ClearOverflow    SciaRegs.SCIFFRX.bit.RXFFOVRCLR = 1
  #define SCI0_ResetRxInt       SciaRegs.SCIFFRX.bit.RXFFINTCLR = 1
  #define SCI0_ReceivedLen      SciaRegs.SCIFFRX.bit.RXFFST
  #define SCI0_ResetTxInt       SciaRegs.SCIFFTX.bit.TXFFINTCLR = 1
#else
//*************************************************************
//         NO UC
//*************************************************************
#define SCI0_Disable
#define SCI0_MIBFormatInit
#define SetClkPrescaler
#define SCI0_DisableRecInt
#define SCI0_DisableRec
#define SCI0_ClearError
#define SCI0_DisableTransmitEndIrq
#define SCI0_WaitAddressByte 
#define SCI0_RecChar
#define SCI0_WaitDataBytes
#define SCI0_RecFifoLevel
#define SCI0_RecFifoLevel1
#define SCI0_RecFifoLevel16
#define SCI0_ConnectTXtoRX
#define SCI0_DisconnectTXtoRX 
#define SCI0_TransmitRegEmpty 
#define SCI0_TransmitShiftRegEmpty
#endif

#endif  

extern volatile bool1 SCI_MsgAvailable;
extern T_UsbMessage SCI_MsgInRS232;
extern bool1 SCI_Available;
extern uint16 SCI_GpioState;

void SCI1_RS232Init(void);
void RS232ReceiveEnable(void);
void SCI1_Update(void);
void SCI1_Command(void);
void SCI1_Receive(void);
void SCI1_InitGpio(void);
void SCI1_SendMessage(T_UsbMessage *msg);
void SCI1_WriteMsg(uint8 reg, uint16 *buf, uint8 len);
void SCI1_ReadMsg(uint8 reg, uint16 *buf, uint8 len);
interrupt void RS232_RXI_Int(void);
interrupt void RS232_TEI_Int(void);


