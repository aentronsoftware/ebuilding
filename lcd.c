/*
 * lcd.c
 *
 *  Created on: 23 nov. 2015
 *      Author: remip
 */

#include "mmscfg.h"

#include "uc.h"
#include "dspspecs.h"
#include "mms_dict.h"   //for ODP_VersionParameters, also #include <includes.h> (festival)
#include "F2806x_I2c_defines.h"
#include "i2c.h"
#include "convert.h"
#include "param.h"
#include "ERROR.H"


void LCD_Start(void){
  uint16 buffer[3];
#if LCD_DEVICECODE == PCF2116_DEVICECODE
  buffer[0] = 0x0C3E;//0x8+4(display on)<<8; 0x20+0x10(8bits)+0xC(3lignes 12 char)+2(voltage generator)
  buffer[1] = 0x0206;//2(set home)<<8; 4+2(set cursor right shift)
  I2C_Command(LCD_WRITE, (char*)&buffer, 4, 0);
#else
  buffer[0] = 0x092A;//set RE=1 and N/W=1 (4lines)
  buffer[1] = 0x2806;//set entry mode com0->31 and seg99->0 and set RE=0
  buffer[2] = 0x0C01;//clear display and set display on
  //buffer[3] = 0x0080;//go home
  I2C_Command(LCD_WRITE, (char*)&buffer, 6, 0x80);
#endif

}

void LCD_WriteText(uint16 pos,char* text,uint16 lenght){
  uint16 res, temp, i;
  uint16 buffer[11];
  buffer[0] = 0x4080 + pos;
  if (lenght > 0 && lenght < LCD_MAXLEN){
    for (i=2;i<lenght+2;i++){
      res = text[i-2];
      temp = BASETEXT; //space
      if(res >= (int)' ' && res <= (int)'/'){
        temp += res - (int)' ';
      }
      else if(res >= (int)'0' && res <= (int)'?'){
        temp += 0x10 + res - (int)'0';
      }
      else if(res >= (int)'A' && res <= (int)'O'){
        temp += 0x21 + res - (int)'A';
      }
      else if(res >= (int)'P' && res <= (int)'Z'){
        temp += 0x30 + res - (int)'P';
      }
      else if(res >= (int)'a' && res <= (int)'o'){
        temp += 0x41 + res - (int)'a';
      }
      else if(res >= (int)'p' && res <= (int)'z'){
        temp += 0x50 + res - (int)'p';
      }
      if ((i&1) == 0) buffer[i/2] = temp;
      else buffer[i/2] += (temp<<8);
    }
    res = I2C_Command(LCD_WRITE, (char*)&buffer, lenght+2, 0x80);
  }
}


void LCD_ReadText(uint16 pos,char* text,uint16 lenght){
  uint16 res, temp, i;
  uint16 buffer[11];
  buffer[0] = 0x0080 + pos;
#if LCD_DEVICECODE == PCF2116_DEVICECODE
  res = I2C_Command(LCD_WRITE, (char*)&buffer, 1, 0); //set cursor address
#else
  res = I2C_Command(LCD_WRITE, (char*)&buffer, 1, 0x80); //set cursor address
#endif
  if (lenght < LCD_MAXLEN){
#if LCD_DEVICECODE == PCF2116_DEVICECODE
    res = I2C_Command(LCD_READ, (char*)&buffer, lenght, 0x60);
#else
    lenght+=1;//1st read is dummy
    res = I2C_Command(LCD_READ, (char*)&buffer, lenght, 0x40);
#endif
    for (i=0;i<lenght;i++){
      if ((i&1) == 0) res = buffer[i/2] & 0xFF;
      else res = buffer[i/2] >> 8;
      if(res >= BASETEXT)
        res -= BASETEXT;
      else temp = (int)' '; //space
      if(res >= 0x50){
        temp = (int)'p' + res - 0x50;
      }
      else if(res >= 0x41){
        temp = (int)'a' + res - 0x41;
      }
      else if(res >= 0x30){
        temp = (int)'P' + res - 0x30;
      }
      else if(res >= 0x21){
        temp = (int)'A' + res - 0x21;
      }
      else if(res >= 0x10){
        temp = (int)'0' + res - 0x10;
      }
      else {
        temp = (int)' ' + res;
      }

      text[i] = (char)temp;
    }
  }
}

const uint64 DIVISOR[12] = {1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,10000000000,100000000000};

void LCD_WriteInteger(uint16 pos,uint64* text,uint16 lenght,bool1 signe){
  uint16 res, temp, i = 0;
  uint16 buffer[11];
  int64 itemp;
  uint64 temp64 = *text;
  if (signe){
    itemp = *(int64*)text;
    if (itemp < 0){
      itemp = -itemp;
      i = 1;
      buffer[1] = BASETEXT+0xD;
    }
    temp64 = itemp;
  }
  buffer[0] = 0x4080 + pos;
  if (lenght < LCD_MAXLEN){
    while (i<lenght){
      res = temp64/DIVISOR[lenght-i-1];//partie entiere
      temp64 = temp64 - res * DIVISOR[lenght-i-1];//reste
      temp = BASETEXT + 0x10 + res;
      if ((i&1) == 0) buffer[(i+2)/2] = temp;
      else buffer[(i+2)/2] += (temp<<8);
      i++;
    }
    res = I2C_Command(LCD_WRITE, (char*)&buffer, lenght+2, 0x80);
  }
}

void LCD_ReadInteger(uint16 pos,uint64* text,uint16 lenght){
  bool1 res = FALSE;
  uint16 i, retry = 3;
  uint16 buffer[11];
  uint64 temp64 = *text;
  while (retry > 0 && !res){
    buffer[0] = 0x0080 + pos;
#if LCD_DEVICECODE == PCF2116_DEVICECODE
    res = I2C_Command(LCD_WRITE, (char*)&buffer, 1, 0); //set cursor address
#else
    res = I2C_Command(LCD_WRITE, (char*)&buffer, 1, 0x80); //set cursor address
#endif
    if (lenght < LCD_MAXLEN && res){
      TSK_sleep(2);
#if LCD_DEVICECODE == PCF2116_DEVICECODE
      res = I2C_Command(LCD_READ, (char*)&buffer, lenght, 0x60);
      i=0;
#else
      lenght+=1;//1st read is dummy
      res = I2C_Command(LCD_READ, (char*)&buffer, lenght, 0x40);
      i=1;
#endif
    }
    TSK_sleep(2);
    --retry;
  }
  if (lenght < LCD_MAXLEN && res){
    while (i<lenght){
      temp64 *= 10;
      if ((i&1) == 0) res = buffer[i/2] & 0xFF;
      else res = buffer[i/2] >> 8;
      res = res - BASETEXT - 0x10;
      temp64 += res;
      i++;
    }
    *text = temp64;
  }
}

void LCD_SetPosition(uint16 pos, bool1 blink){
  uint16 buffer[2];
#if LCD_DEVICECODE == PCF2116_DEVICECODE
  buffer[0] = 0x0080 + pos;
  if (blink) buffer += (0xF<<8); //0x8+4(display on)+2(cursor on)+1(blink on)
  else buffer += (0xC<<8);       //0x8+4(display on)
  I2C_Command(LCD_WRITE, (char*)&buffer, 2, 0); //set cursor address
#else
  buffer[0] = 0x8080 + pos;
  buffer[1] = 0x000C;
  if (blink) buffer[1] += (0x3); //+2(cursor on)+1(blink on)
  I2C_Command(LCD_WRITE, (char*)&buffer, 3, 0x80);
#endif
}


