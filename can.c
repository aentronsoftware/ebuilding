/**********************************************************************

   can.c - function for can communication

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   : PTM
   Compil: TI CCS5.2
   Target: TI C2000 processor
   descr.: can communication
   Edit  :

 *********************************************************************/


#include "mmscfg.h"

#include "uc.h"
#include "dspspecs.h"
#include <co_data.h>

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "string.h"

#include "usblib/usb.h"
#include "usblib/usb-ids.h"
#include "usblib/usblib.h"
#include "usblib/device/usbdcomp.h"
#include "usblib/usbhid.h"
#include "usblib/device/usbdevice.h"
#include "usblib/device/usbdhid.h"

#include "usb_dev_hid.h"

int dummy;
#define GREEN_CAN_LED    dummy
#define RED_CAN_LED      dummy


Int can_bus_state;	/*!< State of the CAN bus */

/*!	Function that returns BRPREG value to configure eCAN for a given \a baudrate
	\param baudrate Baudrate value to configure

*/

int calculateBRPREG2(int baudrate)
{
  switch(baudrate)
  {
    case 1000:
      return(1);//3 @80MHz, 1 @40MHz
    case 500:
      return(3);//7 @80MHz, 3 @40MHz
    case 250:
      return(7);//15@80MHz, 7 @40MHz
	case 125:
      return(15);//31@80MHz, 15 @40MHz
    default:
//      _LOGmessage(0x0001,"baudrate not supported, 1M used", 0, 0);
      return(1);
  }
}

void canInit(UNS16 baudrate, UNS8 nodeID)
{
	volatile struct MBOX *p_mbox;
	volatile union CANLAM_REG *p_lam_reg;
	union CANBTC_REG ECanaShadow_CANBTC;
	int i;
	unsigned long aux32;

	asm("  EALLOW");
/* Configure eCAN RX and TX pins for eCAN transmissions using eCAN regs*/
	ECanaRegs.CANTIOC.bit.TXFUNC = 1;
	ECanaRegs.CANRIOC.bit.RXFUNC = 1;

/* Configure eCAN for HECC mode - (regs to access mailboxes 16 thru 31) */
	ECanaRegs.CANMC.bit.SCB = 1;		/* HECC mode also enables time-stamping feature */

/* Activate Auto Bus On (to recover after a "buss-off" state) */
	ECanaRegs.CANMC.bit.ABO = 1;

/* Configure eCAN for Self-Test mode if switch 8 of IDswitch = 1 */
//	if (readIDswitch() & 0x80) ECanaRegs.CANMC.bit.STM = 1;		/* self-test mode */

/* Configure bit timing parameters */
	ECanaRegs.CANMC.bit.CCR = 1 ;				/* Set CCR = 1 */
	while(ECanaRegs.CANES.bit.CCE != 1 ) {}	/* Wait for CCE bit to be set.. */

	ECanaShadow_CANBTC.all = 0;
	ECanaShadow_CANBTC.bit.BRPREG = calculateBRPREG2(baudrate);
	ECanaShadow_CANBTC.bit.TSEG2REG = 1;
	ECanaShadow_CANBTC.bit.TSEG1REG = 6;	/* sample point = 80% */
	ECanaShadow_CANBTC.bit.SJWREG = 1;		/* sjw=2,  2 TQ allowed for a bit to be shortened or lengthened when resynchronizing */
	ECanaRegs.CANBTC.all = ECanaShadow_CANBTC.all;

	ECanaRegs.CANMC.bit.CCR = 0;					/* Set CCR = 0 */
	while(ECanaRegs.CANES.bit.CCE == !0 ) {}	/* Wait for CCE bit to be cleared.. */

	can_bus_state = ERROR_ACTIVE;

/* Disable all Mailboxes */
 	ECanaRegs.CANME.all = 0;		/* Required before writing the MSGIDs */
	asm("  EDIS");

	/* Specific initialization for this program */

	/* Write to the MSGID field */
	for(i=0;i<CAN_RX_MAILBOXES+CAN_TX_MAILBOXES;i++)
	{
		p_mbox = &(ECanaMboxes.MBOX0) + i;
		(*p_mbox).MSGID.bit.IDE = 0;			/* standard identifier (for all rx mboxes) */
		(*p_mbox).MSGID.bit.AME = 1;			/* local acceptance mask used (for all rx mboxes) */
		(*p_mbox).MSGID.bit.AAM = 0;			/* no auto answer mode */

		if(i >= CAN_RX_MAILBOXES)			/* tx mailboxes	 */
			(*p_mbox).MSGID.bit.STDMSGID = 0;	/* standard_identifier=0 and msgid=0 (changed before every sent message) */
		else if(i < 10)		/* mailboxes 0-9 are for nodeID-specific messages */
			(*p_mbox).MSGID.bit.STDMSGID = nodeID;	/* messageID=nodeID (all node specific messages:Emergency, PDO, SDO and NMT-monitoring messages) */
		else if(i < 13)   /* mailboxes 10-12 are for NODE_ID_GATEWAY-specific messages */
      (*p_mbox).MSGID.bit.STDMSGID = NODE_ID_GATEWAY;  /* messageID=NODE_ID_GATEWAY  */
		else 			/* messages with ID = 0x0 (NMT), 0x80 (sync) and 0x100 (time stamp) */
			(*p_mbox).MSGID.bit.STDMSGID = 0x0;
	}

	/* Configure Mailboxes 0-15 as rx and 16-31 as tx */
	ECanaRegs.CANMD.all = 0x0000FFFF; 		/* this register is only accessible by 32-bits accesses? */

	/* Enable Mailboxes (16 for reception and CAN_TX_MAILBOXES for transmission) */
	aux32 = 0x0000FFFF;
	for(i=0;i<CAN_TX_MAILBOXES;i++) aux32 = (aux32 << 1) + 1;
	ECanaRegs.CANME.all = aux32; 		/* this register is only accessible by 32-bits accesses? */

	/* Define Local Acceptance Masks for rx mailboxes */
	for(i=0;i<CAN_RX_MAILBOXES;i++)
	{
		p_lam_reg = &ECanaLAMRegs.LAM0 + i;		/* pointer to every Local Acceptance Mask */

		(*p_lam_reg).bit.LAMI = 0;				/* only standard identifiers (as MBOX own identifier) */
		(*p_lam_reg).bit.LAM_L = 0x0000;
		if(i > 12)			/* mailboxes 13-15 */
			(*p_lam_reg).bit.LAM_H = 0x0604;	/* bits 0, 7 and 8 from 11-bit identifier are ignored */
												/* valid for ID = 0x000 (NMT), 0x080 (sync) and 0x100 (time stamp) and 0x81 state change*/
		else if (i > 9)       /* mailboxes 10-12 */
      (*p_lam_reg).bit.LAM_H = 0x0600;  /* TPDO1 from node 99 */
		else if (i < 6)				/* mailboxes 0-5 */
		  (*p_lam_reg).bit.LAM_H = 0x1E04;	/* only messages for this nodeID (bit 0 ignored) */
    else                            /* mailboxes 6-9 */
      (*p_lam_reg).bit.LAM_H = 0x0C00;	/* only PDO1&2 messages for this nodeID */
	}
	/*! rx mailboxes 1-5 and 7-15 are protected against overwrite. When a message arrives it goes to the highest mailbox with right LAM, if it is already full and protected, it goes to the next with right LAM.
	Mailboxes 0 and 6 are unprotected so that, if they have a message, it is overwritten and a RML interrupt is generated */
	ECanaRegs.CANOPC.all = 0x0000FFBE;

	/* mailboxes interrupts generated on HEC_INT_REQ[1] line */
	ECanaRegs.CANMIL.all = 0xFFFFFFFF;

	EALLOW;
	/* rx mailboxes interrupt enable */
	ECanaRegs.CANMIM.all = 0x0000FFFF;		/* this register is only accessible by 32-bits accesses? */

	/* enable important interrupts, and enable both interrupt lines */
	ECanaRegs.CANGIM.all = 0x00022F03;		/* this register is only accessible by 32-bits accesses? */
	EDIS;

	PieCtrlRegs.PIEIER9.bit.INTx5 = 1;		/* Enable ECAN0INT in the PIE: Group 9 interrupt 5 */
	PieCtrlRegs.PIEIER9.bit.INTx6 = 1;		/* Enable ECAN1INT in the PIE: Group 9 interrupt 6 */
	IER |= M_INT9;							/* Enable CPU INT9 */
}

#define DINT2
#define EINT2


/*!	called from ECAN1 HWI to process serial received data and to acknowledge finished-transmission interrupts
*/
#pragma CODE_SECTION(hwiFnCan0,"ramfuncs")
interrupt void hwiFnCan0(void)
{
  union CANGIF1_REG ECanaShadow_CANGIF1;
  union CANTOS_REG ECanaShadow_CANTOS;
  union CANTOC_REG ECanaShadow_CANTOC;
  volatile struct MBOX *p_mbox;
  unsigned long tmp_u32;
  Message m;
  PieCtrlRegs.PIEACK.bit.ACK9 = 1;
  EINT;
  EnableCaptureCurrentInterrupts; //allow adc et cla int to interrupt
  ECanaShadow_CANGIF1.all = ECanaRegs.CANGIF1.all;
  if(ECanaShadow_CANGIF1.bit.GMIF1 == 1)
  {
    if(ECanaShadow_CANGIF1.bit.MIV1 < CAN_RX_MAILBOXES) 		/* rx mailboxes */
    {
      p_mbox = &(ECanaMboxes.MBOX0) + ECanaShadow_CANGIF1.bit.MIV1;

      DINT2;	/* workaround to avoid eCAN register access error (see silicon errata) */
      tmp_u32 = (*p_mbox).MSGID.all;
      if ( tmp_u32 == 0) tmp_u32 = (*p_mbox).MSGID.all;
      EINT2;
      m.cob_id = ((tmp_u32 >> 18) & 0x07FF);		/* copy message ID */
      /* if ((m.cob_id == 0x201)||(m.cob_id == 0x301)) {
           LOG_printf(&trace, "%u %u",(uint16)(ECanaShadow_CANGIF1.bit.MIV1),(uint16)(ODV_HW_Time & 0xFFFF));
           LOG_printf(&trace, "cob id 0x%x, message pending 0x%x", (uint16)m.cob_id, (uint16)(ECanaRegs.CANRMP.all & 0xFFFF));
         } */
      DINT2;	/* workaround to avoid eCAN register access error (see silicon errata) */
      tmp_u32 = (*p_mbox).MSGCTRL.all;
      if ( tmp_u32 == 0) tmp_u32 = (*p_mbox).MSGCTRL.all;
      EINT2;
      m.len = (tmp_u32 & 0x000F);					/* copy message length */
      m.rtr = ((tmp_u32 >> 4) & 0x0001);			/* copy message RTR bit */

      DINT2;	/* workaround to avoid eCAN register access error (see silicon errata) */
      tmp_u32 = (*p_mbox).MDL.all;
      if ( tmp_u32 == 0) tmp_u32 = (*p_mbox).MDL.all;
      EINT2;
      m.data[0] = ((tmp_u32 >> 24) & 0x00FF);	/* copy message data bytes */
      m.data[1] = ((tmp_u32 >> 16) & 0x00FF);
      m.data[2] = ((tmp_u32 >>  8) & 0x00FF);
      m.data[3] = ((tmp_u32      ) & 0x00FF);

      DINT2;	/* workaround to avoid eCAN register access error (see silicon errata) */
      tmp_u32 = (*p_mbox).MDH.all;
      if ( tmp_u32 == 0) tmp_u32 = (*p_mbox).MDH.all;
      EINT2;
      m.data[4] = ((tmp_u32 >> 24) & 0x00FF);
      m.data[5] = ((tmp_u32 >> 16) & 0x00FF);
      m.data[6] = ((tmp_u32 >>  8) & 0x00FF);
      m.data[7] = ((tmp_u32      ) & 0x00FF);

      if(!MBX_post(&can_rx_mbox, &m, 0))
      {
        //				_WARNINGmessage(0xfffc, 0x01, 0x00, "can_rx_mbox full", 0, 0);
      }
      GREEN_CAN_LED = 1;			/* turn on green CAN led */
      ECanaRegs.CANRMP.all = ((unsigned long)1 << ECanaShadow_CANGIF1.bit.MIV1);		/* clear interrupt flag of the received mailbox */
    }
    else			/* tx mailboxes interrups should be disabled, but... */
    {
      ECanaRegs.CANTA.all = ((unsigned long)1 << ECanaShadow_CANGIF1.bit.MIV1);	/* clear interrupt flag */
    }
  }
  else if(ECanaShadow_CANGIF1.bit.MTOF1 == 1) {	/* time-out of a tx mailbox */
    ECanaShadow_CANTOS.all = ECanaRegs.CANTOS.all;	/* read Time-Out Status Register */

    /* disable Time-Out so that it does not generate another interrupt while the ongoing message ends */
    ECanaShadow_CANTOC.all = ECanaRegs.CANTOC.all;
    ECanaShadow_CANTOC.all &= ~ECanaShadow_CANTOS.all;
    ECanaRegs.CANTOC.all = ECanaShadow_CANTOC.all;

    ECanaRegs.CANTRR.all = ECanaShadow_CANTOS.all;	/* cancel the transmission of timed-out mailbox */
    ECanaRegs.CANTOS.all = ECanaShadow_CANTOS.all;	/* clear the time-out flags */

    //		_LOGmessage(0x0035,"CAN tx message timed out", 0, 0);
  } else {
    /* do nothing */
    /* _LOGmessage(0x0002,"Only mailboxes interrupts should be in CAN1 interrupt", 0, 0); */
  }
  EnableAllInterrupts;
}

/*!	Task that waits for CANOpen frames to send
*/
extern tUSBDHIDDevice* USB_HID_DevicePtr;
void TaskCanSendLoop()
{
	Message m;
	union CANME_REG ECanaShadow_CANME;
	union CANTOC_REG ECanaShadow_CANTOC;
	union CANMDL_REG ECanaShadow_CANMDL;
	union CANMDH_REG ECanaShadow_CANMDH;
	int i;
	bool1 found;
	unsigned long aux32;
	volatile struct MBOX *p_mbox;
	volatile Uint32 *p_moto_reg;
	unsigned char mes[64];

	while (1)
	{
		if (MBX_pend(&can_tx_mbox, &m, SYS_POLL)){ 		/* wait for a message in can tx mailbox */
      found = FALSE;
      i=CAN_RX_MAILBOXES; aux32=(uint32)1<<CAN_RX_MAILBOXES;
      while (!found){
        if(!(ECanaRegs.CANTRS.all & aux32)){
          found = TRUE;
          /* loop until found a tx mailbox that is not transmitting */
        }
        else {
          aux32 = aux32 << 1;
          if (++i == CAN_TX_MAILBOXES+CAN_RX_MAILBOXES){
            i=CAN_RX_MAILBOXES; aux32=(uint32)1<<CAN_RX_MAILBOXES;
            TSK_sleep(1);
          }
        }
      }

      if(i<CAN_TX_MAILBOXES+CAN_RX_MAILBOXES)
      {
        p_mbox = &(ECanaMboxes.MBOX0) + i;		/* pointing to the first free mailbox */

        /* clear TA if set */
        if ( ECanaRegs.CANTA.all & aux32 ) {
          ECanaRegs.CANTA.all = aux32;		/* clear TA */
          while ( ECanaRegs.CANTA.all & aux32 ) {;}		/* wait until TA flag is cleared */
        }

        /* disable mailbox for changing message identifier */
        ECanaShadow_CANME.all = ECanaRegs.CANME.all;
        ECanaShadow_CANME.all &= ~aux32;		/* disable the mailbox that is going to be used to transmit */
        ECanaRegs.CANME.all = ECanaShadow_CANME.all;

        /* (*p_mbox).MSGID.bit.STDMSGID = m.cob_id; */
        DINT2;	/* workaround to avoid eCAN register access error (see silicon errata) */
        (*p_mbox).MSGID.all = ((unsigned long)(m.cob_id) << 18);
        asm("	RPT #3 || NOP");	/* do nothing for 4 cycles */
        (*p_mbox).MSGID.all = ((unsigned long)(m.cob_id) << 18);
        EINT2;

        /* (*p_mbox).MSGCTRL.bit.DLC = m.len; */
        /* (*p_mbox).MSGCTRL.bit.RTR = m.rtr; */
        DINT2;	/* workaround to avoid eCAN register access error (see silicon errata) */
        (*p_mbox).MSGCTRL.all = (unsigned long)(m.len + (m.rtr << 4));
        asm("	RPT #3 || NOP");	/* do nothing for 4 cycles */
        (*p_mbox).MSGCTRL.all = (unsigned long)(m.len + (m.rtr << 4));
        EINT2;

        /* enable mailbox */
        ECanaShadow_CANME.all = ECanaRegs.CANME.all;
        ECanaShadow_CANME.all |= aux32;
        ECanaRegs.CANME.all = ECanaShadow_CANME.all;

        ECanaShadow_CANMDL.byte.BYTE0 = m.data[0];
        ECanaShadow_CANMDL.byte.BYTE1 = m.data[1];
        ECanaShadow_CANMDL.byte.BYTE2 = m.data[2];
        ECanaShadow_CANMDL.byte.BYTE3 = m.data[3];
        DINT2;	/* workaround to avoid eCAN register access error (see silicon errata) */
        (*p_mbox).MDL.all = ECanaShadow_CANMDL.all;
        asm("	RPT #3 || NOP");	/* do nothing for 4 cycles */
        (*p_mbox).MDL.all = ECanaShadow_CANMDL.all;
        EINT2;

        ECanaShadow_CANMDH.byte.BYTE4 = m.data[4];
        ECanaShadow_CANMDH.byte.BYTE5 = m.data[5];
        ECanaShadow_CANMDH.byte.BYTE6 = m.data[6];
        ECanaShadow_CANMDH.byte.BYTE7 = m.data[7];
        DINT2;	/* workaround to avoid eCAN register access error (see silicon errata) */
        (*p_mbox).MDH.all = ECanaShadow_CANMDH.all;
        asm("	RPT #3 || NOP");	/* do nothing for 4 cycles */
        (*p_mbox).MDH.all = ECanaShadow_CANMDH.all;
        EINT2;

        /* disable Time-Out to change time-out value */
        ECanaShadow_CANTOC.all = ECanaRegs.CANTOC.all;
        ECanaShadow_CANTOC.all &= ~aux32;
        ECanaRegs.CANTOC.all = ECanaShadow_CANTOC.all;
        while(ECanaRegs.CANTOC.all & aux32);	/* wait until Time-out is disabled */
        /* set time-out value as TSC + time-out */
        p_moto_reg = &ECanaMOTORegs.MOTO0 + i;		/* pointer to every Message-Object Time-Out Register */
        DINT2;	/* workaround to avoid eCAN register access error (see silicon errata) */
        *p_moto_reg = ECanaRegs.CANTSC + CAN_MSG_TIME_OUT;
        asm("	RPT #3 || NOP");	/* do nothing for 4 cycles */
        *p_moto_reg = ECanaRegs.CANTSC + CAN_MSG_TIME_OUT;
        EINT2;

        /* enable Time-Out */
        ECanaShadow_CANTOC.all = ECanaRegs.CANTOC.all;
        ECanaShadow_CANTOC.all |= aux32;
        ECanaRegs.CANTOC.all = ECanaShadow_CANTOC.all;

        /* send data in mailbox i */
        ECanaRegs.CANTRS.all = aux32;

        GREEN_CAN_LED = 1;			/* turn on green CAN led */
      }
		}
		if (MBX_pend(&usb_tx_mbox, &mes[1], SYS_POLL)){    // wait for a message in usb rx mailbox
      mes[0]=(mes[1]&0xFF);
      mes[1]=mes[1]/0x100;
      USBDHIDReportWrite(USB_HID_DevicePtr, mes, MESSAGE_CANOPEN_OUT_LEN,TRUE);
    }
		if (can_tx_mbox.dataSem.count == 0 && usb_tx_mbox.dataSem.count == 0)
      TSK_sleep(1);  //sleep if queue empty
	}
}


/*!	Task that manages the CAN bus state and the red CAN LED
*/
interrupt void manage_can_bus(void)
{
	static unsigned int i_blink = 0;
	union CANES_REG ECanaShadow_CANES;

	ECanaShadow_CANES.all = ECanaRegs.CANES.all;		/*Clear the Status and Error Register*/
	ECanaRegs.CANES.all = ECanaShadow_CANES.all;

	/* when warning level, bus-off or error-passive modes are entered, can_bus_state is updated in the ISR */
	if ((ECanaShadow_CANES.bit.BO == 0) && (ECanaShadow_CANES.bit.EP == 0)) {
		if(ECanaShadow_CANES.bit.EW == 1) {
			if (ATM_seti(&can_bus_state, WARNING_LEVEL) != WARNING_LEVEL){ 	/* error passive mode has been left */
//				_LOGmessage(0x0036,"CAN bus left ERROR_PASSIVE mode to WARNING_LEVEL", 0, 0);
			}
		} else {
			if (ATM_seti(&can_bus_state, ERROR_ACTIVE) != ERROR_ACTIVE){ 	/* returned to ERROR_ACTIVE mode */
//				_LOGmessage(0x0037,"CAN bus returned to ERROR_ACTIVE mode", 0, 0);
			}
		}
	}

	switch (can_bus_state) {
		case ERROR_ACTIVE:
			RED_CAN_LED = 0;
			break;
		case WARNING_LEVEL:
			if (++i_blink >= 50) {
				RED_CAN_LED = !RED_CAN_LED;	/* blink red CAN led */
				i_blink = 0;
			}
			break;
		case ERROR_PASSIVE:
		case BUS_OFF:
			RED_CAN_LED = 1;
			break;
	}
  PieCtrlRegs.PIEACK.bit.ACK9 = 1;
}

void TaskCanWaitMessage(void){
  Message m;
  int i;

  m.cob_id = 0;
  m.len = 8;
  m.rtr = 0;
  for (i=0;i<8 ;i++ ) {
    m.data[i] = i;
  }

  while (1) {
    if (MBX_pend(&can_rx_mbox, &m, SYS_POLL)){		      // wait for a message in can rx mailbox
      canDispatch(BoardODdata, &m, CAN_PORT_INTERNAL);  // process it
    }
    if (MBX_pend(&usb_rx_mbox, &m, SYS_POLL)){          // wait for a message in usb rx mailbox
      canDispatch(BoardODdata, &m, CAN_PORT_USB);       // process it
    }
    if (can_rx_mbox.dataSem.count == 0 && usb_rx_mbox.dataSem.count == 0)
      TSK_sleep(1);  //sleep if queue empty
  }
}

/* =========================================================================== */
/*  End of SourceCode. */
/* =========================================================================== */
