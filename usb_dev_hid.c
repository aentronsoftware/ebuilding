
#include "mmscfg.h"

#include "uc.h"
#include "dspspecs.h"
#include <co_data.h>

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "string.h"

#include "usblib/usb.h"
#include "usblib/usb-ids.h"
#include "usblib/usblib.h"
#include "usblib/device/usbdcomp.h"
#include "usblib/usbhid.h"
#include "usblib/device/usbdevice.h"
#include "usblib/device/usbdhid.h"

#include "usb_dev_hid.h"



unsigned long USBReceiveEventCallback(void *pvCBData, unsigned long ulEvent,
    unsigned long ulMsgData, void *pvMsgData);

static uint16 UsbCounter = 0;

//*****************************************************************************
//
// The languages supported by this device.
//
//*****************************************************************************
const unsigned char g_pLangDescriptor[] = {4, USB_DTYPE_STRING,
    USBShort(USB_LANG_EN_US)};
//*****************************************************************************
//
// The manufacturer string.
//
//*****************************************************************************
const unsigned char g_pManufacturerString[] = {(7 + 1) * 2, USB_DTYPE_STRING,
    'A', 0, 'E', 0, 'N', 0, 'T', 0, 'R', 0, 'O', 0, 'N', 0};

//*****************************************************************************
//
// The product string.
//
//*****************************************************************************
const unsigned char g_pProductString[] = {(4 + 1) * 2, USB_DTYPE_STRING,
    'M', 0, 'M', 0, 'S', 0, ' ', 0 };

//*****************************************************************************
//
// The serial number string.
//
//*****************************************************************************
const unsigned char g_pSerialNumberString[] = {(1 + 1) * 2, USB_DTYPE_STRING,
    '0',0};


//*****************************************************************************
//
// The interface description string.
//
//*****************************************************************************
const unsigned char g_pHIDInterfaceString[] = {(13 + 1) * 2, USB_DTYPE_STRING,
    'H', 0, 'I', 0, 'D', 0, ' ', 0, 'I', 0, 'n', 0, 't', 0, 'e', 0, 'r', 0, 'f',
    0, 'a', 0, 'c', 0, 'e', 0};

//*****************************************************************************
//
// The configuration description string.
//
//*****************************************************************************
const unsigned char g_pConfigString[] = {(17 + 1) * 2, USB_DTYPE_STRING, 'H', 0,
    'I', 0, 'D', 0, ' ', 0, 'C', 0, 'o', 0, 'n', 0, 'f', 0, 'i', 0, 'g', 0, 'u',
    0, 'r', 0, 'a', 0, 't', 0, 'i', 0, 'o', 0, 'n', 0};

//*****************************************************************************
//
// The descriptor string table.
//
//*****************************************************************************
const unsigned char * const g_pStringDescriptors[] = {g_pLangDescriptor,
    g_pManufacturerString, g_pProductString, g_pSerialNumberString,
    g_pHIDInterfaceString, g_pConfigString};
#define NUM_STRING_DESCRIPTORS (sizeof(g_pStringDescriptors) /                \
                                sizeof(unsigned char *))



/*typedef struct T_UsbString
{
  uint8 size;      //byte bLength;
  uint8 type;      //byte bDescriptorType;
  uint8 str[];     //word bUnicodeString[63];
}T_UsbString;
*/

void USB_Init(void){

}

//*****************************************************************************
//
// The HID device private data.
//
//*****************************************************************************
tHIDInstance g_sHIDInstance;

//*****************************************************************************
//
// The report descriptor for the BIOS mouse class device.
//
//*****************************************************************************



static const unsigned char g_pucPumpReportDescriptor[] =
    {
        UsagePage(USB_HID_GENERIC_DESKTOP),
        Usage(0xA5),
        Collection(USB_HID_PHYSICAL),
        Usage(0xA6),
        Usage(0xA7),
        LogicalMinimum(0),
        LogicalMaximum(255),
        //ReportID(MESSAGE_CANOPEN_REPORT_ID),
        ReportSize(8),
        ReportCount(MESSAGE_CANOPEN_IN_LEN),
        Input((USB_HID_INPUT_DATA | USB_HID_INPUT_VARIABLE | USB_HID_INPUT_ABS)),
        Usage(0xA9),
        LogicalMinimum(0),
        LogicalMaximum(255),
        //ReportID(MESSAGE_CANOPEN_REPORT_ID),
        ReportSize(8),
        ReportCount(MESSAGE_CANOPEN_OUT_LEN),
        Output((USB_HID_OUTPUT_DATA | USB_HID_OUTPUT_VARIABLE | USB_HID_OUTPUT_ABS)),
        EndCollection
    };

//*****************************************************************************
//
// The HID class descriptor table. For the mouse class, we have only a single
// report descriptor.
//
//*****************************************************************************
static const unsigned char * const g_pPumpClassDescriptors[] = {
    g_pucPumpReportDescriptor, };

// YourUSBReceiveEventCallback

//*****************************************************************************
//
// The HID descriptor for the mouse device. //class descriptor ok
//
//*****************************************************************************
static const tHIDDescriptor g_sPumpHIDDescriptor =
{   9, // bLength
    USB_HID_DTYPE_HID, // bDescriptorType
    USBShort(0x111), // bcdHID (version 1.11 compliant)
    0, // bCountryCode (not localized)
    1, // bNumDescriptors
    USB_HID_DTYPE_REPORT, /* Report descriptor */
    USBShort(sizeof(g_pucPumpReportDescriptor)) // Size of report descriptor
};

tHIDReportIdle g_psReportIdle[1] = { {0, 0, 0, 0}};

//****************************************************************************
//
// A buffer into which the composite device can write the combined config
// descriptor.
//
//****************************************************************************

#define BUFFER_RX_LEN_MAX 66
unsigned char BufferRx[BUFFER_RX_LEN_MAX];

unsigned long BufferRxLen;

//tUSBDHIDDevice* pUsbDevice;
tUSBDHIDDevice* USB_HID_DevicePtr;


unsigned long USBReceiveEventCallback(void *pvCBData, unsigned long ulEvent,
    unsigned long ulMsgData, void *pvMsgData)
{
  uint16 counter;
  unsigned long result;

  //tUSBDHIDDevice *psDevice;
  //tUSBDHIDDevice *psInst;


  //psInst = USB_HID_DevicePtr;

  // Which event were we sent?
  //
  counter = 2000;
  result = 0;
  switch (ulEvent) {
    //
    // The host has connected to us and configured the device.
    //
    case USB_EVENT_CONNECTED:
    break;

      //
      // The host has disconnected from us.
      //
    case USB_EVENT_DISCONNECTED:
      counter = 0;
    break;

    case USB_EVENT_RX_AVAILABLE:
      BufferRxLen = USBDHIDPacketRead(USB_HID_DevicePtr, (uint8*)&BufferRx[0], MESSAGE_CANOPEN_IN_LEN, false);
      if (BufferRxLen > 0) {
        BufferRx[1] = BufferRx[1]*256+BufferRx[0];//cobid
        //si nodeid == pour nous alors on process le message
        //if (BufferRx[0] == NODE_ID_BOOT_PICOLLO)
        MBX_post(&usb_rx_mbox, &BufferRx[1], SYS_POLL);//accept all usb message
        /*else //sinon on envoie sur le can
          MBX_post(&can_tx_mbox, &BufferRx[1], SYS_POLL);*/
      }
    break;

      //
      // The host is polling us for a particular report and the HID driver
      // is asking for the latest version to transmit.
      //
    case USBD_HID_EVENT_IDLE_TIMEOUT:
    case USBD_HID_EVENT_GET_REPORT:
      *(unsigned char **)pvMsgData = BufferRx;
      result = 64;
      break;

      //
      // The device class driver has completed sending a report to the
      // host in response to a Get_Report request.
      //
    case USBD_HID_EVENT_REPORT_SENT:
      //
      // We have nothing to do here.
      //
    break;

      //
      // This event is sent in response to a host Set_Report request.  We
      // must return a pointer to a buffer large enough to receive the
      // report into.
      //
    case USBD_HID_EVENT_GET_REPORT_BUFFER:
      result = (unsigned long)BufferRx;
      break;
      //
      // This event indicates that the host has sent us an Output or
      // Feature report and that the report is now in the buffer we provided
      // on the previous USBD_HID_EVENT_GET_REPORT_BUFFER callback.
      //
    case USBD_HID_EVENT_SET_REPORT:

    break;

      //
      // The host is asking us to set either boot or report protocol (not
      // that it makes any difference to this particular mouse).
      //
    case USBD_HID_EVENT_SET_PROTOCOL:
      USB_HID_DevicePtr->ucProtocol = ulMsgData;
    break;

      //
      // The host is asking us to tell it which protocol we are currently
      // using, boot or request.
      //
    case USBD_HID_EVENT_GET_PROTOCOL:
      result = USB_HID_DevicePtr->ucProtocol;
      break;

      //
      // Pass ERROR, SUSPEND and RESUME to the client unchanged.
      //
    case USB_EVENT_ERROR:
    case USB_EVENT_SUSPEND:
    case USB_EVENT_RESUME:
      result = 0;
      break;

      //
      // We ignore all other events.
      //
    default:
    break;
  }
  UsbCounter = counter;
  return (result);
}

unsigned long USBTransmitEventCallback(void *pvCBData, unsigned long ulEvent,
    unsigned long ulMsgData, void *pvMsgData)
{
  switch (ulEvent) {
    case USB_EVENT_TX_COMPLETE: {
      //Semaphore_post(semaphoreUsbTx);
      return (0);
    }
    default: {
      break;
    }
  }
  return 0;
}

const tUSBDHIDDevice g_sHIDPumpDevice = {
//
// The Vendor ID you have been assigned by USB-IF.
//
    USB_VID_PHILIPS,
//
// The product ID you have assigned for this device.
//
    USB_PID_AENTRON,
//
// The power consumption of your device in milliamps.
//
    10,
//
// The value to be passed to the host in the USB configuration descriptor�s
// bmAttributes field.
//
    USB_CONF_ATTR_SELF_PWR,
//
// This mouse supports the boot subclass.
//
    USB_HID_SCLASS_NONE,
//
// This device supports the BIOS mouse report protocol.
//
    USB_HID_PROTOCOL_NONE,
//
// The device has a single input report.
//
    1,
//
// A pointer to our array of tHIDReportIdle structures. For this device,
// the array must have 1 element (matching the value of the previous field).
//
    g_psReportIdle,
//
// A pointer to your receive callback event handler.
//
    &USBReceiveEventCallback,
//
// A value that you want passed to the receive callback alongside every
// event.
//
    (void *)&g_sHIDPumpDevice,
//
// A pointer to your transmit callback event handler.
//
    &USBTransmitEventCallback,
//
// A value that you want passed to the transmit callback alongside every
// event.
//
    (void *)&g_sHIDPumpDevice,
//
// This device does not want to use a dedicated interrupt OUT endpoint
// since there are no output or feature reports required.
//
    true,
    //TODO OS
//
// A pointer to the HID descriptor for the device.
//
    &g_sPumpHIDDescriptor,
//
// A pointer to the array of HID class descriptor pointers for this device.
// The number of elements in this array and their order must match the
// information in the HID descriptor provided abov e.
//
    g_pPumpClassDescriptors,
//
// The composite device does not use the strings from the class.
//
    g_pStringDescriptors, NUM_STRING_DESCRIPTORS,
//
// A pointer to the private instance data allocated for the class driver to
// use.
//
    &g_sHIDInstance
};

extern tConfigHeader g_sHIDConfigHeader;
extern unsigned char g_pHIDDeviceDescriptor[];
extern const tConfigSection *g_psHIDSections[];

#define INT_IN_EP_MAX_SIZE2      USB_FIFO_SZ_TO_BYTES(USB_FIFO_SZ_64)
#define INT_OUT_EP_MAX_SIZE2     USB_FIFO_SZ_TO_BYTES(USB_FIFO_SZ_64)

unsigned char g_pHIDInEndpoint2[] =
{
    //
    // Interrupt IN endpoint descriptor
    //
    7,                          // The size of the endpoint descriptor.
    USB_DTYPE_ENDPOINT,         // Descriptor type is an endpoint.
    USB_EP_DESC_IN | USB_EP_TO_INDEX(USB_EP_3),
    USB_EP_ATTR_INT,            // Endpoint is an interrupt endpoint.
    USBShort(INT_IN_EP_MAX_SIZE2), // The maximum packet size.
    1,                         // The polling interval for this endpoint.
};

unsigned char g_pHIDOutEndpoint2[] =
{
    //
    // Interrupt OUT endpoint descriptor
    //
    7,                          // The size of the endpoint descriptor.
    USB_DTYPE_ENDPOINT,         // Descriptor type is an endpoint.
    USB_EP_DESC_OUT | USB_EP_TO_INDEX(USB_EP_3),
    USB_EP_ATTR_INT,            // Endpoint is an interrupt endpoint.
    USBShort(INT_OUT_EP_MAX_SIZE2), // The maximum packet size.
    1,                         // The polling interval for this endpoint.
};

const tConfigSection g_sHIDInEndpointSection2 =
{
    sizeof(g_pHIDInEndpoint2),
    g_pHIDInEndpoint2
};

const tConfigSection g_sHIDOutEndpointSection2 =
{
    sizeof(g_pHIDOutEndpoint2),
    g_pHIDOutEndpoint2
};


#define USB_TIMER_PERIOD 1 //ms

void USB_Tick(void){
  if(UsbCounter>USB_TIMER_PERIOD){
    UsbCounter -=USB_TIMER_PERIOD;
  }
  else{
    UsbCounter =0;
  }
}

bool1 USB_Connected(void){
  return (UsbCounter>0);
}

void USB_Stop(void){
  USBDHIDTerm(USB_HID_DevicePtr);
}


extern UNS32 ODP_Board_RevisionNumber;
bool1 USB_Start(void)
{
  tDeviceDescriptor *devicedescr = (tDeviceDescriptor *)g_pHIDDeviceDescriptor;
  devicedescr->bcdDevice.LSB = (ODP_Board_RevisionNumber & 0xFF) ; //ok
  devicedescr->bcdDevice.MSB = 0;
  g_psHIDSections[3] = &g_sHIDInEndpointSection2;
  g_psHIDSections[4] = &g_sHIDOutEndpointSection2;
  //devicedescr->bMaxPacketSize0 = 16;//marche pas
  USB_HID_DevicePtr = (tUSBDHIDDevice*)(USBDHIDInit(0, &g_sHIDPumpDevice));
  //g_psCompDevices[1].pvInstance = (tUSBDHIDDevice*)(USBDHIDInit2(0,&g_sHIDPumpDevice2));
  if (USB_HID_DevicePtr == NULL)
    return false;
  else
    return true;
}

void TaskUsbRx(void){
  Message m;
  int i;

  m.cob_id = 0;
  m.len = 8;
  m.rtr = 0;
  for (i=0;i<8 ;i++ ) {
    m.data[i] = i;
  }
  while (1) {
    if (MBX_pend(&usb_rx_mbox, &m, SYS_POLL)){    // wait for a message in can rx mailbox
      canDispatch(BoardODdata, &m, CAN_PORT_USB);         // process it
    }
    TSK_sleep(1);//in order to usb interrupt to finish before dispatching
  }
}

void TaskUsbTx(void){
  unsigned char m[64];
  while (1) {
    MBX_pend(&usb_tx_mbox, &m[1], SYS_FOREVER);    // wait for a message in can rx mailbox
    m[0]=(m[1]&0xFF);
    m[1]=m[1]/0x100;
    USBDHIDReportWrite(USB_HID_DevicePtr, m, MESSAGE_CANOPEN_OUT_LEN,true);
  }
}


