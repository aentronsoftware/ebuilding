//###########################################################################
//
// FILE:   usbdevicepriv.h
//
// TITLE:  Private header file used to share internal variables and
//         function prototypes between the various device-related
//         modules in the USB library.  This header MUST NOT be
//         used by application code.
//
//###########################################################################
// $TI Release: F2806x C/C++ Header Files and Peripheral Examples V136 $
// $Release Date: Apr 15, 2013 $
//###########################################################################

#ifndef __USBDEVICEPRIV_H__
#define __USBDEVICEPRIV_H__

//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

//*****************************************************************************
//
// Device enumeration functions provided by device/usbenum.c and called from
// the interrupt handler in device/usbhandler.c
//
//*****************************************************************************
    extern tBoolean USBDeviceConfig(unsigned long ulIndex,
                                    const tConfigHeader *psConfig,
                                    const tFIFOConfig *psFIFOConfig);
    extern tBoolean USBDeviceConfigAlternate(unsigned long ulIndex,
            const tConfigHeader *psConfig,
            unsigned char ucInterfaceNum,
            unsigned char ucAlternateSetting);
    extern void USBDeviceResumeTickHandler(unsigned long ulIndex);

//*****************************************************************************
//
// Mark the end of the C bindings section for C++ compilers.
//
//*****************************************************************************
#ifdef __cplusplus
}
#endif

#endif // __USBDEVICEPRIV_H__


