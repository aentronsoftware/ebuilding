/**********************************************************************

   hal.c - hardware abstraction layer

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 02.05.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.:
   Edit  :

 *********************************************************************/

#include "mmscfg.h"
#include "uc.h"
#include <clk.h>
#include "dspspecs.h"
#include "ngtestspec.h"
#include "mms_dict.h"
#include "objdatatype.h"
#include "convert.h"
#include "recorder.h"
#include "hal.h"
#include "ERROR.H"
#include "sci1.h"
#include "IQmathLib.h"
#include "pid_reg3.h"
#include "buffer.h"
#include "param.h"

uint16  UpdateTimer = 0;
#define UPDATE_TIMER 10

bool1 HAL_NewCurPoint = FALSE;

int16 PositionCounter = POSITION_COUNTER_MAX;
int32 HAL_Position = 0;
int32 HAL_LastPosition = 0;
uint8 HAL_CellOK = 2;
uint16 HAL_DoorClosed = 0;
uint16 HAL_RelayState = RELAY_STILL_STATE;
bool1 HAL_Enable = FALSE;
int32 HAL_Current_Sum = 0;
int32 SOC_Delay_Sec = 0;
uint16 OnceRecalibrateSOC = 0;

extern UNS32 GV_voltage_counter = 0;
extern UNS32 GV_voltage_counter1 = 0;
extern UNS32 GV_Temp_counter = 0;
extern UNS32 GV_Blinking_counter = 0;
uint16 Temp_Module_Voltage = 0;
extern UNS32 GV_Delatvoltage_counter = 0;
int MVTemp35 =0;
int MVTemp1 =0;
int MVTemp2 =0;
int MVTemp3 =0;
int MVTempCounter1 =0;
int MVTempCounter2 =0;
int MVTempCounter3 =0;
bool1 ResetCounterbool = FALSE;
extern UNS32 ResetCounter = 0;
int ErrorState =0;

TMMSConfig* MMSConfig;

void HAL_Init(void){

}

#define BLINK_PERIOD 500
#define BLINK_DUTY   250

int16 BlinkCounter = 0;
extern UNS32 CommTimeout;
bool1 ErrorPinOn = FALSE;
UNS16 ErrorCounter = 0;
uint16 RelayTimer = 0;
uint8 RelayTimeOut = 0;
uint16 HAL_TimeSOC = 0;
uint8 Temparature_Delay = 0;
uint16 WorkingHours = 0;
uint16 ResetCountertime = 0;
uint16 CurrentDelayTime = 0;
bool1 EventResetTemp  = TRUE;
bool1 EventResetVoltH = TRUE;

int16 OldVal;
int16 OldCurrent = 0;

/******************************************************************************
          millisecint
      ========================

INTERRUPT SOURCE : QEP1 interrupt  EQEP1_INT
FREQUENCY : 1ms

FUNCTION :
*******************************************************************************/
interrupt void millisecint(void)
{
  uint8 msg;
  int16 newval;
  EINT;
  AcknowlegeInt = PIEACK_GROUP5;
  EnableCaptureCurrentInterrupts; //allow adc et cla int to interrupt

  /////////////////////////////////////////////////////////////////////
   int16 voltsoc = CNV_Round((float)ODV_Module1_Voltage / 10.0 / ODP_SafetyLimits_Cell_Nb);

   GpioDataRegs.GPADAT.bit.GPIO16 = 0;
   GpioDataRegs.GPADAT.bit.GPIO17 = 0;
    GpioDataRegs.GPADAT.bit.GPIO18 = 0;

    /// GPIO28 LED5 -> RED

    if (ODV_ErrorDsp_ErrorNumber != 0)
    {
    	/// red flashing
    	GpioDataRegs.GPADAT.bit.GPIO19 = 0;

       	 GV_Blinking_counter++;
       	 if (GV_Blinking_counter > 200)
       	 {
       		 if (GpioDataRegs.GPADAT.bit.GPIO28 == 1)
       		 {
       			 GpioDataRegs.GPADAT.bit.GPIO28 = 0;
       			 GV_Blinking_counter = 0;
       		 }
       		 else
       		 {
       			 GpioDataRegs.GPADAT.bit.GPIO28 = 1;
       			 GV_Blinking_counter = 0;
       		 }
       	 }
    }
    else
    {
    	/// Green stays
    	GpioDataRegs.GPADAT.bit.GPIO19 = 1;
    	GpioDataRegs.GPADAT.bit.GPIO28 = 0;
    }

////////////////////////////////////////////////////////////////////////////////////

  //inc milisec time
  ++ODV_SysTick_ms;
  //inc second time
  if (ODV_SysTick_ms%1000 == 999){
    ++ODP_OnTime;
    ++HAL_TimeSOC;
    ++Temparature_Delay;
	++WorkingHours;
    ++ResetCountertime;
    ++CurrentDelayTime;
    ++SOC_Delay_Sec;
  }

  if(CurrentDelayTime > 2)
  {
	  if(ODV_Module1_Current == 0) ODV_Module1_Current = ODV_Module1_Current_Average;

	  int16 NewVurrent = ODV_Module1_Current_Average;
	  if (abs((abs(OldCurrent) - abs(NewVurrent))) > (ODP_Module1_Current_Resolution*0.03125))
	  {
		  ODV_Module1_Current = ODV_Module1_Current_Average;
		  OldCurrent = NewVurrent;
	  }
	  CurrentDelayTime = 0;
  }

  if ((Temparature_Delay == ODP_SafetyLimits_Temp_Delay/2) || (Temparature_Delay == 1))
     {
      	ODV_Module1_Temperature = ODV_Temperature_Average_data;
      	ODV_Module1_Temperature2 = ODV_Temperature_Min;
      	Temparature_Delay = 2;

      	if(ODV_Module1_Temperature > 45)
      	{
      		if(EventResetTemp)
      		{
      			if(ODP_CommError_OverTemp_ErrCounter >250) ODP_CommError_OverTemp_ErrCounter=255;
      			ODP_CommError_OverTemp_ErrCounter++;
      			EventResetTemp = FALSE;
      			PAR_WriteStatisticParam(-5);
      		}
      	}
      	else
      	{
      		EventResetTemp = TRUE;
      	}

     }

  if (ODP_CommError_TimeOut > 0){
    if (ODV_SysTick_ms - CommTimeout > ODP_CommError_TimeOut && ODV_MachineMode > GATEWAY_MODE_STILL
        && ODV_MachineMode < GATEWAY_MODE_24ON){
      ERR_ErrorComm();
      ODV_MachineMode = GATEWAY_MODE_ERROR_IN;
    }
  }

  ///////////////////////////////////////////////
  // 1 Hour counter Start 3600 Sec = 1 Hour
  /////////////////////////////////////////////
   if (WorkingHours > 3600)
     {
 	  PAR_Operating_Hours_day++;
 	   WorkingHours = 0;
     }

   /*PAR_Operating_Hours_day = 0;
   ODP_CommError_OverTemp_ErrCounter = 0;
   ODP_CommError_OverVoltage_ErrCounter = 0;
   ODP_CommError_LowVoltage_ErrCounter = 0;*/

    ODV_Battery_Operating_Hours = PAR_Operating_Hours_day;
    if (ODV_Battery_Operating_Hours>87600) ODV_Battery_Operating_Hours = 87600;

    /////////////////////////////////////////////
    //End of Working Hours Log into EEPROM
    ////////////////////////////////////////////

  ClearUTO; // Clear UTO flag and clear INT flag              pdf: spru790a.pdf page=29
  ClearINT;

  //start 1st conversion in order to stabilize adc because 1st sampling is very bad
  //AdcRegs.ADCSOCFRC1.bit.SOC0 = 1;
  //while(AdcRegs.ADCSOCFLG1.bit.SOC0){}
  //start all adc, will be done in order from 0 to 15 and will take approximately 13us
  //AdcRegs.ADCSOCFRC1.all = 0xFFFF;
  //end of conversion will call the readinput

  /**************** Compute real time vars ********************/
  /************************************************************/
  if (RelayTimer > 0)
  {
	  if (ODP_Board_Deactive_FF !=1)
	  {
		  --RelayTimer; /// Test
	  }
    if (RelayTimer == 0){
      ODV_Write_Outputs_16_Bit[0] = 0;
      ODV_Read_Inputs_16_Bit[0] &= ~RELAY_BIT1;
      RELAY_VALUE1 = 0;
      LED_VALUE = 1; //clear sta led
      GpioDataRegs.GPADAT.bit.GPIO4 = 0; //clear relay led
    }
    if (GpioDataRegs.GPADAT.bit.GPIO20 == 1 && ODV_ErrorDsp_ErrorNumber == 0 && ODV_MachineMode != GATEWAY_MODE_ERROR){
      ErrorCounter = 1;
      RelayTimer = 0;
      ODV_MachineMode = GATEWAY_STATE_STILL;
      ERR_ClearError();
    }
  }
  if ((GpioDataRegs.GPADAT.bit.GPIO20 == 0) && HAL_Enable && ODV_ErrorDsp_ErrorNumber == 0 && ODV_MachineMode != GATEWAY_MODE_ERROR){//fault
    if (!ErrorPinOn && (ODV_ErrorDsp_ErrorNumber==0)){
      ODV_Read_Inputs_16_Bit[0] |= 8;
      //ODV_MachineMode = GATEWAY_MODE_ERROR;
      //ODV_Module1_Status |= GATEWAY_STATE_ERROR_BIT;
      RelayTimer = ODP_CommError_Delay * 200;
      msg = CMD_READ_ERROR;
      MBX_post(&sci_rx_mbox, &msg, 0);
      ErrorCounter = 5;
      HAL_CellOK = 0;
      ErrorPinOn = TRUE;
    }
    if (ErrorCounter > 0) --ErrorCounter;
    if (ErrorCounter == 0){
      if (RelayTimer > 20) ErrorCounter = 20;
      else {
        ErrorCounter = 1000;
        if (ODP_Board_Deactive_FF !=1)
        {
        	//if (ODV_ErrorDsp_ErrorNumber == 0) ERR_SetError(UNKNOWN_ERROR);
        }
      }
      msg = CMD_READ_ERROR;
      MBX_post(&sci_rx_mbox, &msg, 0);
      msg = CMD_CLEAR_ERROR;
      MBX_post(&sci_rx_mbox, &msg, 0);
    }
  }
  else {
    if (ErrorCounter > 0) --ErrorCounter;
    else if(ErrorPinOn){
      HAL_CellOK = 2;
      ODV_Read_Inputs_16_Bit[0] &= ~8;
    //  if (ODV_ErrorDsp_ErrorNumber == ERR_TEMPERATURE) ODV_ErrorDsp_ErrorNumber = 0; // will clear errors and restart module
      //ODV_MachineMode = GATEWAY_MODE_CLR;
      ErrorPinOn = FALSE;
    }
  }

  SCI1_Update();
  switch (HAL_RelayState){
    //relay1
    case 0:
      RelayTimeOut = RELAY_TIMEOUT;
      RELAY_VALUE1 = RELAY_SEUILA;//switch on relay
      HAL_RelayState = 1;
      OldVal = ReadCurrent1;
      break;
    case 1:
      newval = ReadCurrent1;
      if (--RelayTimeOut == 0) HAL_RelayState = RELAY_ERROR;
      if (OldVal-newval >= ODP_Contactor_Slope) HAL_RelayState = 2;
      OldVal = newval;
      break;
    case 2:
      RELAY_VALUE1 = RELAY_SEUILB;//
      HAL_RelayState = RELAY_STILL_STATE;
      ODV_Read_Inputs_16_Bit[0] |= RELAY_BIT1;
      LED_VALUE = 0; //set sta led
      GpioDataRegs.GPADAT.bit.GPIO4 = 1; // set relay led
      break;

    case RELAY_STILL_STATE:
      if(((ODV_Write_Outputs_16_Bit[0] & RELAY_BIT1)) && ((ODV_Read_Inputs_16_Bit[0] & RELAY_BIT1) == 0)){
        HAL_RelayState = 0; //switch on
      }
      else{
        if(((ODV_Write_Outputs_16_Bit[0] & RELAY_BIT1) == 0) && (ODV_Read_Inputs_16_Bit[0] & RELAY_BIT1)){
          RELAY_VALUE1 = 0;// switch off
          ODV_Read_Inputs_16_Bit[0] &= ~RELAY_BIT1;
          LED_VALUE = 1; //clear sta led
          GpioDataRegs.GPADAT.bit.GPIO4 = 0; //clear relay led
        }
      }
      ODV_Gateway_Status = (ODV_Read_Inputs_16_Bit[0] & RELAY_BIT1);
      break;

    case RELAY_ERROR:
      //switch off relays
      RELAY_VALUE1 = 0;
      RELAY_VALUE2 = 0;
      RELAY_VALUE3 = 0;
      ODV_Write_Outputs_16_Bit[0] = 0;
      ODV_Read_Inputs_16_Bit[0] = 0;
      ERR_ErrorDigOvld();
      HAL_RelayState = RELAY_STILL_STATE;
      LED_VALUE = 1; //clear sta led
      GpioDataRegs.GPADAT.bit.GPIO4 = 0;
      break;
    default:
      break;
  }
  /* record the must and is values for future analysis */
  if (ODV_Recorder_Control.Pos_Record)
  {
    REC_Record();
  }
}


#define AD_VREF 3300.0 //3300mV
#define AD_RESOL 4095  //AD 10 bits
#define VALVE_RMES 0.5 //5.1 ohm

//#define AD_TO_mVOLT  (AD_VREF/AD_RESOL*20.2/2.2)       //convert ad result to voltage for current source

#define VALVE_AD_TO_0_1mAMP   (AD_VREF*10/AD_RESOL/VALVE_RMES) //convert ad result to current


//called by end of conversion 3 //5kHz VALVE_COUNT frequency PWMEPwm4Regs.ETPS.bit.SOCAPRD  = ET_1ST;
#pragma CODE_SECTION(HAL_ReadAnalogueInputs,"ramfuncs")
interrupt void HAL_ReadAnalogueInputs(void){
  //ToggleTestPin1;
  AcknowlegeInt = PIEACK_GROUP1;   // Acknowledge interrupt to PIE
  if (ReadCurrent1 >= ODP_Contactor_Overcurrent) HAL_RelayState = RELAY_ERROR;
  if ((ODV_Read_Inputs_16_Bit[0] & RELAY_BIT1) && ReadCurrent1 <= ODP_Contactor_Undercurrent) HAL_RelayState = RELAY_ERROR;
  ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_RELAY] = ReadCurrent1;
  ODV_Write_Analogue_Output_16_Bit[0] = CNV_Round(ReadCurrent0*ODP_Analogue_Output_Scaling_Float[0]);//33/3*3300/4095; //12V
  //ToggleTestPin1;
  AdcRegs.ADCINTFLGCLR.bit.ADCINT9 = 1;
}



#define MEASURE_COMPUTE_PERIOD 16

int MeasureCounter = MEASURE_COMPUTE_PERIOD;

//called by end of measure conversions
interrupt void HAL_ReadAnalogueInputs2(void){
  //TMeasure measure;

  AcknowlegeInt = PIEACK_GROUP1;   // Acknowledge interrupt to PIE

  //measure[PIC_MEASURE_TEMP1] = (int16)AdcResult.ADCRESULT8;
  //MBX_post(&mailboxMeasures,&measure,0);
  AdcRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;
}


#define MEAS_T_NB 16
#define RTEMP_REF 10000 //10 kohm
#define NTC_TEMP0         250  //�C*10
#define NTC_STEP_DEGREES   50  //�C*10

//Resultat en �C*10
//int16 HAL_LectureTemperature(int measure_index){
int16 HAL_LectureTemperature(float val, uint8 type){
  //float val;
  int i;// = measure_index;

  //val = BUF_SumLast(i,MEAS_T_NB,TRUE) / MEAS_T_NB;
  i = 0;
  while((i<TEMP_VAL_NB-1) && (val < TempTable[i].adval[type])){
    ++i;
  }
  if((i > 0) && (i<TEMP_VAL_NB)){
    val = (TempTable[i-1].adval[type] - val) / (TempTable[i-1].adval[type]-TempTable[i].adval[type]);
    i = val * NTC_STEP_DEGREES + TempTable[i-1].temp*10;
  }
  else{
    i = TempTable[i].temp*10;
  }
  return i;
}

uint16 HAL_LectureADTemperature(int16 vali, uint8 type){
  float val;
  uint16 i;// = measure_index;

  //val = BUF_SumLast(i,MEAS_T_NB,TRUE) / MEAS_T_NB;
  i = 0;
  while((i<TEMP_VAL_NB-1) && (vali > TempTable[i].temp)){
    ++i;
  }
  if((i > 0) && (i<TEMP_VAL_NB)){
    val = ((float)TempTable[i].temp - vali) / (TempTable[i].temp-TempTable[i-1].temp);
    val = val * (TempTable[i-1].adval[type] - TempTable[i].adval[type]) + TempTable[i].adval[type];
  }
  else{
    val = TempTable[i].adval[type];
  }
  return (uint16)CNV_Round(val*16);
}

void TskFnComputeMeasures(void){
  //TMeasure measure;
  int16 i, volt, oldstate = 0, SOC_Temp;
  uint8 msg = 0;
  float temp=0, temp1=0;

  ODV_Recorder_Period = 1000;
  ODV_Recorder_Vectors = 0x8000000000706;
  ODV_Recorder_Start = 0;
  TSK_sleep(200);
  //REC_StartRecorder();
  while(1){
    /*while(MBX_pend(&mailboxMeasures,&measure,0)){
      for(i=PIC_MEASURE_TEMP1;i<=PIC_MEASURE_INSULATION2;i++){
        BUF_Write(i,measure[i]);
      }
    }*/
	    temp = ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP3]/10;   /// on board sensor 2
	    if (temp < ODP_SafetyLimits_Tmin) temp = ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP4]/10; //if sensor 1 is disconnected/unplugged or damaged switch to sensor2  on board sensor1
	    if (temp < ODP_SafetyLimits_Tmin) temp = ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_NTC3]/10; //if sensor 2 is disconnected/unplugged or damaged switch to sensor3 on board sensor 3

	    if (ODV_Module1_Temperature2 < ODP_SafetyLimits_Tmin || ODV_Module1_Temperature > ODP_SafetyLimits_Tmax)
	    {
	    	GV_Temp_counter++;
	    	if (GV_Temp_counter > (uint32)ODP_SafetyLimits_Temp_Delay*10)
	    	{
	    		if (ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorOverTemp();
	    	      ODV_Current_ChargeAllowed = 0;
	    	      ODV_Current_DischargeAllowed = 0;
	    	}
	    }
	    else
	    {
	    	//     if (ODV_ErrorDsp_ErrorNumber == ERR_TEMPERATURE) ODV_ErrorDsp_ErrorNumber = 0; // will clear errors and restart module
		  if (temp < ODP_Temperature_ChargeLimits[0])
		  {
			ODV_Current_ChargeAllowed = CNV_Round(ODP_Power_ChargeLimits[0] * ODP_SafetyLimits_Imax_charge / 100.0);
		  }
		  else for (i=0; i<7; i++){
			if (ODP_Temperature_ChargeLimits[i] >= ODP_Temperature_ChargeLimits[i+1]) {
			  ODV_Current_ChargeAllowed = CNV_Round(ODP_Power_ChargeLimits[i] * ODP_SafetyLimits_Imax_charge / 100.0);
			  break;
			}
			if (temp > ODP_Temperature_ChargeLimits[i] && temp <= ODP_Temperature_ChargeLimits[i+1]){
			  temp1 = (temp - ODP_Temperature_ChargeLimits[i]) / (ODP_Temperature_ChargeLimits[i+1] - ODP_Temperature_ChargeLimits[i]);
			  temp1 = temp1 * ((int16)(ODP_Power_ChargeLimits[i+1]-ODP_Power_ChargeLimits[i])) + ODP_Power_ChargeLimits[i];
			  ODV_Current_ChargeAllowed = CNV_Round(temp1 * ODP_SafetyLimits_Imax_charge / 100.0);
			  break;
			}
		  }
	    }
    if (temp < ODP_SafetyLimits_Tmin || temp > ODP_SafetyLimits_Tmax){
      //ODV_Current_DischargeAllowed = 0;
    } else {
      if (temp < ODP_Temperature_DischargeLimits[0]){
        ODV_Current_DischargeAllowed = CNV_Round(ODP_Power_DischargeLimits[0] * ODP_SafetyLimits_Imax_dis / 100.0);
      }
      else for (i=0; i<7; i++){
        if (ODP_Temperature_DischargeLimits[i] >= ODP_Temperature_DischargeLimits[i+1]) {
          ODV_Current_DischargeAllowed = CNV_Round(ODP_Power_DischargeLimits[i] * ODP_SafetyLimits_Imax_dis / 100.0);
          break;
        }
        if (temp > ODP_Temperature_DischargeLimits[i] && temp <= ODP_Temperature_DischargeLimits[i+1]){
          temp1 = (temp - ODP_Temperature_DischargeLimits[i]) / (ODP_Temperature_DischargeLimits[i+1] - ODP_Temperature_DischargeLimits[i]);
          temp1 = temp1 * ((int16)(ODP_Power_DischargeLimits[i+1]-ODP_Power_DischargeLimits[i])) + ODP_Power_DischargeLimits[i];
          ODV_Current_DischargeAllowed = CNV_Round(temp1 * ODP_SafetyLimits_Imax_dis / 100.0);
          break;
        }
      }
    }
    if (ODV_Module1_Voltage == 0)
    {
    	volt = CNV_Round((float)Temp_Module_Voltage / 10.0 / ODP_SafetyLimits_Cell_Nb);
    	/// Johnny Test 11.08.2020  V51 Added 0V when Error
    	/// Johnny Test 11.08.2020  V51 Added Check the OverVoltage Protectin on Max Cell Voltage not on Total Voltage
    }
    else
    {
        volt = CNV_Round((float)ODV_Module1_Voltage / 10.0 / ODP_SafetyLimits_Cell_Nb);
        Temp_Module_Voltage = ODV_Module1_Voltage;
    }

    if ((abs(ODV_Module1_Current) < (ODP_Module1_Module_SOC_Current*32)))
    {
    	if (OnceRecalibrateSOC != 1)
    	{
    		OnceRecalibrateSOC  = 0;
    	}
    }
    else
    {
    	OnceRecalibrateSOC = 2;
    	SOC_Delay_Sec = 4;
    }

    if (MMSConfig->SOC2 == 1)
    {
    	if (((SOC_Delay_Sec > 60) || (SOC_Delay_Sec < 3)) && (SOC_Delay_Sec > 1))
    	{
    		SOC_Delay_Sec = 4;
    		if((OnceRecalibrateSOC == 0) && (ODV_ErrorDsp_ErrorNumber == 0))
    		{
    			int SOC_Calibration = 100 * (volt - ODP_SafetyLimits_Umin) / (ODP_SafetyLimits_Umax - ODP_SafetyLimits_Umin) + 4;
    			if (SOC_Calibration>100) SOC_Calibration=100;
    			if (SOC_Calibration<0) SOC_Calibration=0;
    			PAR_Capacity_Left   = (SOC_Calibration) * (PAR_Capacity_Total/100);
    			OnceRecalibrateSOC = 1;
    		}
    	}
    }
    else
    {
    	//SOC_Temp = 100 * (volt - ODP_SafetyLimits_UnderVoltage) / (ODP_SafetyLimits_OverVoltage - ODP_SafetyLimits_UnderVoltage);
    }
      /// Test Voltage delay Test 10.05.2019
      /// Johnny Test: make Error on MAx Cell Voltage not total voltage
	  if ((ODV_Module1_MaxCellVoltage *2) >= ODP_SafetyLimits_OverVoltage)
	  {
		  GV_voltage_counter1++;
		  if (GV_voltage_counter1 > (uint32)(ODP_SafetyLimits_Voltage_delay)*10)
	    	  {
			  if(ODP_CommError_OverVoltage_ErrCounter > 250) ODP_CommError_OverVoltage_ErrCounter =255;

			  if (ODV_ErrorDsp_ErrorNumber == 0)
				  {
				  	  ODP_CommError_OverVoltage_ErrCounter++;
				  	  PAR_WriteStatisticParam(-5);

				  	  ERR_ErrorOverVoltage();
				  }
	    	  	  ODV_Write_Outputs_16_Bit[0] = 0;
	    	      ODV_Read_Inputs_16_Bit[0] &= ~RELAY_BIT1;
	    	      RELAY_VALUE1 = 0;
	    	      LED_VALUE = 1; //clear sta led
	    	      GpioDataRegs.GPADAT.bit.GPIO4 = 0; //clear relay led

	    	     /* if(EventResetVoltH)
	    	      {
	    	    	  if(ODP_CommError_OverVoltage_ErrCounter > 250) ODP_CommError_OverVoltage_ErrCounter =255;
	    	    	  ODP_CommError_OverVoltage_ErrCounter++;
	    	    	  EventResetVoltH = FALSE;
	    	    	  PAR_WriteStatisticParam(-5);
	    	      }*/
	    	  }
	  }
	  else
	  {
		 // GV_voltage_counter1 = 0;
		  EventResetVoltH = TRUE;
	  }
	
	  if (volt < ODP_SafetyLimits_UnderVoltage)
	  {
		  GV_voltage_counter++;
	      if (GV_voltage_counter > (uint32)ODP_SafetyLimits_Voltage_delay*1000)
	      {
	      		  //ERR_ErrorUnderVoltage();
	      }
	  }

	/// Test Johnny SOC 01.06.2019
	// if (SOC_Temp>100) SOC_Temp=100;
	// if (SOC_Temp<0) SOC_Temp=0;

	 if (ABS(ODV_Module1_Current) < (32 * ODP_Module1_Module_SOC_Current) || (ODV_SOC_SOC2 == 0 && SOC_Temp > 0))
	 {
	 	 // 32 internal 1amp
	   	 // ODV_SOC_SOC2 = SOC_Temp;
	   	 // HAL_Current_Sum = 0;
	}

    if (PAR_Capacity_Total == 0) PAR_Capacity_Total = 3600*320;  /// Johnny Testing 29.03.2021 Current measurments 1Sec

    ODV_SOC_SOC1 = CNV_Round(100.0 * PAR_Capacity_Left / PAR_Capacity_Total);
    SOC_Temp = CNV_Round(100.0 * PAR_Capacity_Left / PAR_Capacity_Total);; //ODV_SOC_SOC2 + CNV_Round(100.0 * HAL_Current_Sum / PAR_Capacity_Total);

    if (SOC_Temp>100) SOC_Temp=100;
    if (SOC_Temp<0) SOC_Temp=0;
    ODV_Module1_SOC = SOC_Temp;

    if (HAL_TimeSOC > 600) { //every 10mins
      HAL_TimeSOC = 0;
      PAR_WriteStatisticParam(-5);
	}

    if (MMSConfig->foil == 1)
    {
    	if (ODV_Heater_Control_Heater == 0)
    	{
    		if (ODV_Module1_Temperature2 < 35)
    		{
				if (volt <= ODP_Heater_Off_Cell_Voltage || ODV_Module1_Temperature2 >= ODP_Heater_OffTemp)  /// V 39
				{
					SCI_GpioState = 0;
					ODV_Heater_Heater_Status = 0;
				}
				else if (ODV_Module1_Temperature2 < ODP_Heater_OnTemp && ODV_ErrorDsp_ErrorNumber == 0)  /// V 39
					{
						SCI_GpioState = 0x2000;
						ODV_Heater_Heater_Status = 1;
						if (ODV_Heater_Heater_SCI == 0)
						 {
							msg = CMD_SET_GPIO;
							MBX_post(&sci_rx_mbox, &msg, 0);
						 }
					}
    		}
    		else
    		{
    			SCI_GpioState = 0;
    			ODV_Heater_Heater_Status = 0;
    		}
    	}
    	else if (ODV_Heater_Control_Heater == 1)
    	{
    		if (ODV_Heater_Heater_SCI == 0)
    		{
    			msg = CMD_SET_GPIO;
    			MBX_post(&sci_rx_mbox, &msg, 0);
    		}
    		SCI_GpioState = 0x2000;
    		ODV_Heater_Heater_Status = 1;
    	}
    }
    else
    {
    	SCI_GpioState = 0;
    	ODV_Heater_Heater_Status = 0;
    }
    if (oldstate != SCI_GpioState){
    	oldstate = SCI_GpioState;
    	msg = CMD_SET_GPIO;
    	MBX_post(&sci_rx_mbox, &msg, 0);
    }

    //ODV_Gateway_SOH = 100;
    ODV_Module1_Throughput = (UNS16)ABS((long)ODV_Module1_Voltage * ODV_Module1_Current / 2000); //0.0625 W
    ODV_Battery_Total_Cycles = PAR_Capacity_TotalLife_Used / (PAR_Capacity_Total);
    ODV_Battery_Total_ChargedkWh = PAR_Capacity_TotalLife_Used / 360000 / 32 * ODP_SafetyLimits_Cell_Nb * 0.00343; //kWh  : 360000 / 32 why we need?  /// Johnny Testing 29.03.2021 Current measurments 1Sec

    if (ODP_Battery_Cycles != 0) ODV_Module1_SOH = 100 - (100*ODV_Battery_Total_Cycles / ODP_Battery_Cycles);

    ODV_Module1_Capacity_Used = CNV_Round((ODV_SOC_SOC1) * ODP_Battery_Capacity / 10); //0.1Ah

    if (ODP_Module1_Module_SOC_Current == 0) ODP_Module1_Module_SOC_Current = 2;

    if (ODV_Module1_Current < (-1 *(ODP_Module1_Module_SOC_Current*32)))
    {
    	ODV_Module1_Time_Remaining = PAR_Capacity_Left / 6 / ABS(ODV_Module1_Current);  //min   // 6000
    }
    else if (ODV_Module1_Current > (ODP_Module1_Module_SOC_Current* 32))
    {
    	ODV_Module1_Time_Remaining =  (PAR_Capacity_Total - PAR_Capacity_Left) / 60 / (ODV_Module1_Current); /// Old:60000 try with 6000
    }
    else
    {
    	ODV_Module1_Time_Remaining = 0;
    }

    ODV_Module1_Capacity_set = CNV_Round(ODP_Battery_Capacity * 10);
    if (ODV_Battery_Total_Cycles > 2500) ERR_HandleWarning(WARN_CYCLE);
    TSK_sleep(100);
  }
}

const uint16 permtable[11] = {1,3,5,7,11,13,17,19,23,29,31};
#define permutebit(val,val2,bit) (((val2 & bit)>0) ? (val |= (uint32)bit) : (val &= ~(uint32)bit))

void HAL_Unlock(void)
{
  uint32 num, pass, num2;
  uint16 i;
  num = ODV_Security & 0xFFFFFFFF;
  num2 = num;
  pass = (ODV_Security>>32) & 0xFFFFFFFF;
  for (i=0; i<11; i++) {
    permutebit(num,pass,((uint32)1<<permtable[i]));
    permutebit(pass,num2,((uint32)1<<permtable[i]));
  }
  if (num == ODP_RandomNB) {
    ODP_Password += pass;
    if (pass == 0){ //reset counter
      ODP_Password = ODP_OnTime;
    }
    ODP_RandomNB = 0;
    PAR_StoreODSubIndex(BoardODdata,ODA_PAR_Password);
    PAR_StoreODSubIndex(BoardODdata,ODA_PAR_RandomNB);
    ERR_ClearWarning();
  }
  ODV_Security = 0;
}

void HAL_Random(void)
{
  /* use new seed */
  srand(ODP_OnTime & 0xFFFF);
  ODP_RandomNB = (65535.0*rand()/(RAND_MAX+1.0)); //genere nb aleatoire entre 0 et 65535
  srand(((ODP_OnTime>>16) + (ODV_SysTick_ms>>8)) & 0xFFFF);
  ODP_RandomNB += ((UNS32)(65535.0*rand()/(RAND_MAX+1.0))<<16); //genere nb aleatoire entre 0 et 65535
  PAR_StoreODSubIndex(BoardODdata,ODA_PAR_RandomNB);
}

interrupt void HAL_Dummy(void)
{
  AcknowlegeInt = 0xFFF;   // Acknowledge interrupt to PIE
}

void HAL_Reset(void){
  DINT;
  EALLOW;
  AdcRegs.INTSEL9N10.bit.INT9E = 0;  /* INT9 enable */
  AdcRegs.INTSEL1N2.bit.INT2E  = 0;  /* INT2 enable */

  PieVectTable.I2CINT1A = &HAL_Dummy;
  PieVectTable.ADCINT2 = &HAL_Dummy;
  PieVectTable.ADCINT9 = &HAL_Dummy;
  PieVectTable.EQEP1_INT = &HAL_Dummy;
  PieVectTable.ECAN1INTA = &HAL_Dummy;
  PieVectTable.ECAN0INTA = &HAL_Dummy;
  PieVectTable.USB0_INT = &HAL_Dummy;

  EQep1Regs.QEINT.bit.UTO = 0;      // Disable unit timer interrupt
  EDIS;
  EINT;
  while(PieCtrlRegs.PIEIFR1.all!=0);
  PieCtrlRegs.PIEIER1.all = 0;
  while(PieCtrlRegs.PIEIFR5.all!=0);
  PieCtrlRegs.PIEIER5.all = 0;
  while(PieCtrlRegs.PIEIFR8.all!=0);
  PieCtrlRegs.PIEIER8.all = 0;
  while(PieCtrlRegs.PIEIFR9.all!=0);
  PieCtrlRegs.PIEIER9.all = 0;

  PieCtrlRegs.PIEACK.all = 0xFFFF;
  PieCtrlRegs.PIEIER1.bit.INTx6 = 0;
  //
  IER = 0;
}


