
/**********************************************************************
   picollo.c - 

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 02.05.13
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060.1AA uC picollo TI DSP TMS320C2869
   descr.:
   Edit  :

 *********************************************************************/

#include "mmscfg.h"
#include "uc.h"
#include <clk.h>
#include "dspspecs.h"
#include "mms_dict.h"
#include "recorder.h"
#include "hal.h"
#include "dict_util.h"
#include "sci1.h"
#include "error.h"
#include "param.h"
#include "crc.h"
#include "crc_tbl.h"
//#include "sincos.h"
#include "buffer.h"
#include "usb_dev_hid.h"
#include "convert.h"
#include "i2c.h"
#include "sci1.h"

extern UNS32 mms_dict_obj1400_COB_ID_used_by_PDO;
extern UNS32 mms_dict_obj1800_COB_ID_used_by_PDO;
extern UNS32 mms_dict_obj1201_COB_ID_Client_to_Server_Receive_SDO;
extern UNS32 mms_dict_obj1201_COB_ID_Server_to_Client_Transmit_SDO;

extern UNS32 GV_current_counter = 0;
extern UNS32 GV_Delta_Counter = 0;

bool1 TestCurrentEnable = FALSE;
bool1 UnderCurrent = FALSE;
uint16 InitOK = 0;
bool1 TestPositionEnable = TRUE;
bool1 SequenceRunning = FALSE;
bool1 ReceiveNew = FALSE;
bool1 EventResetVoltL = TRUE;

/**
 * @ingroup
 * @fn check_firmwareCRC
 * @brief
 * @param
 * - type: void
 * - range:
 * - description:
 * @return
 * - type: unsigned short
 * - range:
 * - description: returns 1 if CRCs match, 0 otherwise
 */
extern CRC_TABLE golden_CRC_values;
uint16 CheckfirmwareCRC(void)
{
  int i;
  uint32 temp;
  CRC_RECORD crc_rec;
  for (i = 0; i < golden_CRC_values.num_recs; i++)
  {
    crc_rec = golden_CRC_values.recs[i];
    temp = getCRC32_cpu(INIT_CRC32, (uint16*)crc_rec.addr, EVEN, crc_rec.size*2);
    if (temp != crc_rec.crc_value){
      return 0;
    }
    /**************************************************/
    /* COMPUTE CRC OF DATA STARTING AT crc_rec.addr */
    /* FOR crc_rec.size UNITS. USE */
    /* crc_rec.crc_alg_ID to select algorithm. */
    /* COMPARE COMPUTED VALUE TO crc_rec.crc_value. */
    /**************************************************/
  }
  /*if all CRCs match, return 1;
  else return 0;*/
  return 1;
}

//======================== NMT change state callback functions ================================
//called after a NMT_ResetNode (0x81)
void FnResetNode(CO_Data* d){
  //after this function, the state is changed to Initialization
  //and FnInitialiseNode(d) will be called
}

//called after a NMT_ResetNode (0x81)
void FnResetCommunications(CO_Data* d){
  uint16 node_id;
  char dummy_m[16]; //should be >= largest mailbox message

  //empty all send and receive mailboxes
  while (MBX_pend(&mailboxSDOout,&dummy_m,0));
  while (MBX_pend(&can_tx_mbox,&dummy_m,0));
  while (MBX_pend(&can_rx_mbox,&dummy_m,0));

  node_id = ODP_Board_RevisionNumber;//NODE_ID_BOOT_PICOLLO; //default value if reading canID fails

  //adjust all CANopen communication parameters for the real node_id
  //mms_dict_obj1400_COB_ID_used_by_PDO += node_id;
  //mms_dict_obj1800_COB_ID_used_by_PDO += node_id;
  //mms_dict_obj1201_COB_ID_Client_to_Server_Receive_SDO += node_id;
  //mms_dict_obj1201_COB_ID_Server_to_Client_Transmit_SDO += node_id;
  setNodeId(d,(UNS8)node_id);  //set the node_id of the CO object

  //now initialize the CAN hardware
  canInit(ODP_Board_BaudRate, (UNS8)node_id);
  //set de nodeID of default networkdict
  //DIC_SetNodeId(NODE_ID_GATEWAY);
}  //after this function, the state is changed to Initialization
   //and FnInitialiseNode(d) will be called


// called at startup (setState(OD,Initialization) and after NMT_Reset(0x81),
// or NMT_ResetCommunication(0x81)
void FnInitialiseNode(CO_Data* d) {
  uint16 CrcFirmwareTest = 0;
  //generate CRC32 table
  genCRC32Table();
  //compute firmware CRC
  CrcFirmwareTest = CheckfirmwareCRC();
  PieCtrlRegs.PIEIFR9.bit.INTx6 = 1; //to unlock can in case a com happened during reset
  ODV_Controlword.ControlWord = 0; //initialize motor control
  MMSConfig = (TMMSConfig*)&ODP_Board_Config;
  if (PAR_InitParam(d) && (CrcFirmwareTest == 1))
  {
    InitOK = 1;
  }
  else{
    InitOK = 2;
    WARN_ErrorParam();
  }
  PAR_SetParamDependantVars();
}

// always called after FnInitialisation (startup, NMT_Reset, NMT_ResetCommunication)
// and after NMT_Enter PreOperational(0x80)
void FnEnterPreOperational(CO_Data* d) {
  ODV_Controlword.ControlWord = 6;
}

//called after a NMT_StartNode (0x01)
void FnStartNode(CO_Data* d) {

}

//called after a NMT_StopNode (0x01)
void FnStopNode(CO_Data* d) {
  //put here any code you think is useful if the node is stopped by the master
  ODV_Controlword.ControlWord = 0; //switch off motor control
}

CO_Data *BoardODdata;  //this is the pointer to our CANopen object

void canOpenInit(void)
/* CanOpen stack init */
{
  //Initialize the CO object
  BoardODdata = &ODI_mms_dict_Data;
  //set the NMT change state callback functions
  BoardODdata->initialisation = FnInitialiseNode;
  BoardODdata->preOperational = FnEnterPreOperational;
  BoardODdata->operational = FnStartNode;
  BoardODdata->stopped = FnStopNode;
  BoardODdata->NMT_Slave_Node_Reset_Callback = FnResetNode;
  BoardODdata->NMT_Slave_Communications_Reset_Callback = FnResetCommunications;
  //set global callback functions
  BoardODdata->storeODSubIndex = PAR_StoreODSubIndex;
  //function called after writing to TO_BE_SAVED entries (write to non-volatile memory)
  BoardODdata->globalCallback = NULL; //function called when accessing any OD entry

  //Initialize the PDO and SDO setting and the low level can
  //FnResetCommunications(BoardODdata);
  setState(BoardODdata, Initialisation);
  FnResetCommunications(BoardODdata);
  /* this will call FnInitialisation followed by FnPreOperational
   * so at this point ODV_MachineState == STATE_PREOPERATIONAL
   * note that after NMT 80,81,82 the ODV_MachineState will be changed to
   * STATE_PREOPERATIONAL which covers all pre-operational states
   * (Stopped, PreOperational)
   */
  setState(BoardODdata,Operational);

}

#define BOOT_COMMAND_DOWNLOAD_WAIT 0x52656D69
#define BOOT_COMMAND_NONE          0

#pragma DATA_SECTION(BootST,"BootCommand");
//#pragma DATA_SECTION(BootNodeId,"BootCommand");
typedef struct {
  uint32 Command;// = BOOT_COMMAND_NONE; //0x8ffc
  uint32 NodeId;  //0x8ffe
}TBoot;
TBoot BootST;

void taskFnGestionMachine(void){
  uint8 msg = 0, statusi2c = I2C_READ_FAILED;
  uint32 old_time, count = 0;
  //Initialize the CanOpen stack and BoardODdata pointer;

  //canOpenInit();
  TSK_sleep(2);
  HAL_Init();
  while(InitOK == 0) {
    TSK_sleep(1);
  }
  USB_Start();
  SCI_MsgInRS232.cmd = CMD_SET_CHANNELS; SCI1_Command();
  TSK_sleep(1);
  SCI_MsgInRS232.cmd = CMD_READ_CRC; SCI1_Command();
  while (SCI_MsgAvailable == FALSE)
  {TSK_sleep(1);}//wait for answer
  //handle message
  SCI1_Receive();
  SCI_MsgAvailable = FALSE;
  if (ODP_VersionParameters <= 0x80){  /// 0x7E
    TSK_sleep(1);
    SCI_MsgInRS232.cmd = CMD_WRITE_EEPROM; SCI1_Command();
    ODP_VersionParameters = 0x81;     /// 0x7F
    PAR_StoreODSubIndex(BoardODdata,ODA_PAR_VersionParameters);
  }
  SCI_Available = TRUE;
  old_time = ODP_OnTime;
  while(BootST.Command == BOOT_COMMAND_NONE){

    /********** RTC Handling *********/
    // toutes les 64s de fonctionnement
    if (((ODP_OnTime & 63) <= 1)&&((old_time & 63) >= 62))//seconds !!!
    {
      statusi2c = I2C_READ_FAILED;
    }
    if (statusi2c == I2C_READ_FAILED){ //will retry until ok
      I2C_CommandNoWait(RTC_READ,(char *)&ODV_Gateway_Date_Time,7,0,&statusi2c);
    }
    if (statusi2c == I2C_READ_OK){
      statusi2c = I2C_PROCESSED;
      PAR_WriteStatisticParam(-1);
      PAR_WriteStatisticParam(-5);
    }
    old_time = ODP_OnTime;
    if (ODV_SysTick_ms - count >= 10){
      count = ODV_SysTick_ms;
      msg = CMD_READ_VALUES;
      if (ODV_CommError_Counter < 3){
        if (!MBX_post(&sci_rx_mbox, &msg, 0)){
          ++ODV_CommError_Counter;
          SCI_Available = FALSE;
          RS232ReceiveEnable();
          //if (ODV_CommError_Counter >= 3)  // Johnny Test 30.03.2021
          //   ERR_ErrorComm();
        }
        else if (ODV_CommError_Counter) --ODV_CommError_Counter;
      }
      USB_Unlock();
    }
    if (ODV_CommError_Set == 1) {
      ODV_CommError_Set = 0;
      ODV_CommError_Counter = 0;
      while(MBX_pend(&sci_rx_mbox, &msg, 0));
      RS232ReceiveEnable();
    }
    if (ODV_CommError_Set == 2) {
      ODV_CommError_Set = 0;
      ODV_CommError_Counter = 0;
      while(MBX_pend(&sci_rx_mbox, &msg, 0));//empty mailbox
      msg = CMD_SET_CHANNELS; MBX_post(&sci_rx_mbox, &msg, 0);
    }
    if (SCI_Available){
      if (MBX_pend(&sci_rx_mbox, &SCI_MsgInRS232.cmd, 0)){
        SCI_Available = FALSE;
        SCI1_Command();
      }
    }
    if (SCI_MsgAvailable){
      SCI_MsgAvailable = FALSE;
      //handle message
      SCI1_Receive();
      SCI_Available = TRUE;
    }
    TSK_sleep(1);//ms
  } //while(1);
  USB_Stop();
  TSK_sleep(2);
  HAL_Reset();
  BootST.Command = BOOT_COMMAND_DOWNLOAD_WAIT;
  BootST.NodeId = ODP_Board_RevisionNumber;
  asm(" LB 0x3F7FF6 ");//code_start
}

#define INSULATION_THRESHOLD  4080
#define INSULATION_TIMEOUT   60000
#define RELAY_VALUE ODV_Write_Outputs_16_Bit[0]
//#define CHARGE_IN  0 //GpioDataRegs.GPADAT.bit.GPIO12 /// PIn to read SW Johnny Test 27.08.2019
void TaskSciSendReceive(void){
  volatile uint32 start, insulation_counter = 0, current_counter = 0, sleep_counter = 0, volt_count = 0, Temp_counter_Delay = 0, Low_VoltaCounter = 0, Low_Voltage_CurrentCounter = 0;
  int16 cur, volt, volt1;
  float curf;
  uint8 msg;
  bool1 checkenable = FALSE;
  ODV_MachineMode = GATEWAY_MODE_NOINI;
  while(InitOK == 0){//wait for init
    TSK_sleep(1);
  };
  while (InitOK != 1) //|| ((ODV_Gateway_Errorcode & ERR_INSULATION) != 0)){//if bad init or insulation error //Johnny 26.03.2019
  {
    TSK_sleep(1);//stop here
  }
  ODV_MachineEvent = EVENT_NONE;
  ODV_Gateway_State = GATEWAY_MODE_STILL;
  ODV_MachineMode = GATEWAY_MODE_STILL;
  ODV_Gateway_Errorcode = 0;
  //I2C_Command(RTC_READ,(char *)&ODV_Gateway_Date_Time,7,0); is done in gestion machine
  ODV_Module1_Status = 0;
  start = ODV_SysTick_ms;
  while(HAL_Enable == FALSE){//wait for init
    TSK_sleep(1);
  };
  if (MMSConfig->can_wk == 0 && MMSConfig->sw_wk == 1)
    {
		ODP_CommError_TimeOut = 0;
    }
  while (1) {

    if (ODV_Gateway_State == GATEWAY_STATE_CRASH && ODV_MachineMode != GATEWAY_MODE_CRASH){
      start = ODV_SysTick_ms;
      checkenable = FALSE;
      ODV_MachineMode = GATEWAY_MODE_CRASH;
    }
    volt = ODV_Module1_MaxCellVoltage * 2;//V*100
    volt1 = ODV_Module1_MinCellVoltage * 2;//V*100
    curf = (float)ODV_Module1_Current/32;//A
    if (MMSConfig->lem == 0) curf = 0;
    cur = CNV_Round(curf);//A

    /// Johnny Test 11.09.2019 working with ODV_Module1_MaxDeltaVoltage
    if (((ODV_Module1_MaxDeltaVoltage / 50)*100) >= (100*(ODP_SafetyLimits_Umax_bal_delta/2))) /// 500mV Warnung
    {
    	ERR_HandleWarning(WARN_LOW_CAPACITY);
    }
    if (((ODV_Module1_MaxDeltaVoltage / 50)*100) >= (100*ODP_SafetyLimits_Umax_bal_delta)) /// 1000mV error in Intercae as V Units is enough
    {
    	GV_Delta_Counter++;

      	if (cur > 3) /// charging 3A then send Error
       	{
      		if (GV_Delta_Counter > ODP_SafetyLimits_Temp_Delay *1000)
      		{
      			if (ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorDigOvld();
      		}

       	}
    }
    else
    {
    	GV_Delta_Counter = 0;
    }

    ODV_Module1_Status = (ODV_MachineMode&0xF) + (ODV_Module1_Status&0xC0);
    if (ODV_MachineMode > GATEWAY_MODE_STILL && ODV_MachineMode <= GATEWAY_MODE_DONLY){
      if (cur > ODP_SafetyLimits_Overcurrent || cur < ODP_SafetyLimits_UnderCurrent)      	  /// Johnny Test 25.04.2019
      {
    	  GV_current_counter++;
    	  if (MMSConfig->lem == 1 && GV_current_counter > (uint32)ODP_SafetyLimits_Current_delay*1000){
    		  ERR_ErrorOverCurrent();
    	  }
      }
	  else
      {
    	  GV_current_counter = 0;
      }

      if (volt1 < ODP_SafetyLimits_Umin)
      {
    	  ERR_HandleWarning(WARN_LOW_CAPACITY);
      }

      if (volt1 > ODP_SafetyLimits_Umax)
      {
    	  ERR_HandleWarning(WARN_OVERCAPACITY);
      }

      if (ABS(curf*1000) < (float)ODP_SleepCurrent){ //mA
        ++sleep_counter;
        if (sleep_counter > (uint32)ODP_SafetyLimits_SleepTimeout*60000){
          sleep_counter = 0;
          ODV_MachineMode = GATEWAY_MODE_ERROR_IN;
          ERR_HandleWarning(WARN_SLEEP);
        }
      }
      else if (sleep_counter > 0) --sleep_counter;
      if (cur < -(int16)ODV_Current_DischargeAllowed || cur > (int16)ODV_Current_ChargeAllowed ){
        ERR_HandleWarning(WARN_CURRENT);
        ++current_counter;
        if (MMSConfig->lem == 1 && current_counter > (uint32)ODP_SafetyLimits_Current_delay*1000){
			//ERR_ErrorOverCurrent();
        }
      }
      else if (current_counter > 0) --current_counter;
      if (ODV_Gateway_State == GATEWAY_STATE_STILL){//switch off
        if (MMSConfig->rel_sta == 1){
          RELAY_VALUE = 0;
        }
        if (MMSConfig->relay_on == 1){
          RELAY_VALUE = 0;
        }
        ODV_MachineMode = GATEWAY_MODE_STILL;
      }
    }
    switch (ODV_MachineMode){
      case GATEWAY_MODE_STILL:
        //Mode still relay opened
        if (MMSConfig->sw_wk == 1){    
          ODV_Gateway_State = GATEWAY_MODE_NORM;
        }
        else if (MMSConfig->can_wk == 0){
          ODV_Gateway_State = GATEWAY_STATE_STILL;
        }
        if (volt1 < ODP_SafetyLimits_UnderVoltage && ODV_ErrorDsp_ErrorNumber == 0 && ODV_MachineMode != GATEWAY_MODE_ERROR) {//(MMSConfig->ch_wk == 1 && (CHARGE_IN==0) && (volt1 < ODP_SafetyLimits_UnderVoltage)){  /// Charge In
          ODV_Gateway_State = GATEWAY_STATE_NORMAL;
        }
        if (ODV_Gateway_State > GATEWAY_STATE_STILL && ODV_ErrorDsp_ErrorNumber == 0 && ODV_MachineMode != GATEWAY_MODE_ERROR){
          if (MMSConfig->relay_on == 1){
            RELAY_VALUE = 1;
          }
          if (MMSConfig->rel_sta == 1){
            RELAY_VALUE = 1;
          }
          start = ODV_SysTick_ms;
          checkenable = FALSE;
          ODV_MachineMode = GATEWAY_MODE_NORM;
        }
      break;

      case GATEWAY_MODE_DONLY:
        if (ODV_SysTick_ms - start >= 50) checkenable = TRUE;
        if (ODV_Current_ChargeAllowed > ODP_Current_C_D_Mode) ODV_Current_ChargeAllowed = ODP_Current_C_D_Mode;

        if (volt < ODP_SafetyLimits_Umax && checkenable && ODV_ErrorDsp_ErrorNumber == 0 && ODV_MachineMode != GATEWAY_MODE_ERROR){
          ODV_MachineMode = GATEWAY_MODE_NORM;
          start = ODV_SysTick_ms;
          checkenable = FALSE;
          ERR_ClearWarning();
        }
        if (volt1 < ODP_SafetyLimits_UnderVoltage) { //(MMSConfig->ch_wk == 1 && CHARGE_IN == 0 && checkenable && (volt1 < ODP_SafetyLimits_UnderVoltage)){//charger in  /// Charge In only mode
          ODV_MachineMode = GATEWAY_MODE_CONLY;
          start = ODV_SysTick_ms;
          checkenable = FALSE;
        }
        if (volt1 > ODP_SafetyLimits_Umax && checkenable && ODV_Module1_Current > 6)
        {//0.2A
         // PAR_Capacity_Left = PAR_Capacity_Total; ///Test
        }
      break;

      case GATEWAY_MODE_CONLY:
    	ODV_Module1_SOC = 0;
    	ERR_HandleWarning(WARN_LOW_CAPACITY);
        if (ODV_SysTick_ms - start >= 50) checkenable = FALSE;
       
        if ((ODV_Module1_MinCellVoltage*2) <= ODP_SafetyLimits_UnderVoltage)// && ODV_ErrorDsp_ErrorNumber == 0 && ODV_MachineMode != GATEWAY_MODE_ERROR) /// can be replaced as to get only error but stays relay ON 28.08.2019
        {
        	if (cur > 2)
        	{

        	}
        	else if ((-1 > cur) && (cur > ODP_SafetyLimits_Low_Voltage_Current_Allow))
        	{
        		Low_Voltage_CurrentCounter++;
				if (ODV_SysTick_ms > (ODP_SafetyLimits_Low_Voltage_Current_Delay*1000))
				{
				/*	if (ODV_Wassberbauer_ErrorReset_OverCurrent2 == 1)
					{
						ODV_Wassberbauer_ErrorReset_OverCurrent2 = 0;
						ODV_MachineMode = GATEWAY_MODE_NORM;
						Low_Voltage_CurrentCounter = 0;
					} */
				if (Low_Voltage_CurrentCounter > (ODP_SafetyLimits_Low_Voltage_Current_Delay*1000))
					{
						if (ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorOverCurrent();
					}
				}
        	}
        	else
        	{
        		Low_VoltaCounter++;
        		if ((ODV_SysTick_ms > (ODP_SafetyLimits_Low_Voltage_Charge_Delay*1000)))
        		{
        			if (Low_VoltaCounter > (ODP_SafetyLimits_Low_Voltage_Charge_Delay*1000))
        				{
        					if (ODV_ErrorDsp_ErrorNumber == 0)
        						{
        							if(ODP_CommError_LowVoltage_ErrCounter>250) ODP_CommError_LowVoltage_ErrCounter = 255;
        							ODP_CommError_LowVoltage_ErrCounter++;
        						  	PAR_WriteStatisticParam(-5);
        							ERR_ErrorUnderVoltage();
        						}
        				}
        			}
        		}
        	}
        	else
        	{
        		//EventResetVoltL = TRUE;
        		Low_Voltage_CurrentCounter = 0;
        	}


        if(ODV_Current_DischargeAllowed > ODP_Current_C_D_Mode) ODV_Current_DischargeAllowed = ODP_Current_C_D_Mode;
		
        if(ODV_Current_DischargeAllowed > ODP_SafetyLimits_Charge_In_Thres_Cur && (volt1 < ODP_SafetyLimits_UnderVoltage)) //MMSConfig->ch_wk == 1 && CHARGE_IN == 0 && (volt1 < ODP_SafetyLimits_UnderVoltage))  /// Charge In
          ODV_Current_DischargeAllowed = ODP_SafetyLimits_Charge_In_Thres_Cur;
		  
        if (volt1 > ODP_SafetyLimits_UnderVoltage && ODV_ErrorDsp_ErrorNumber == 0 && ODV_MachineMode != GATEWAY_MODE_ERROR || (MMSConfig->ch_wk == 0) && (volt1 > ODP_SafetyLimits_UnderVoltage)){  /// Charge In
          start = ODV_SysTick_ms;
          checkenable = FALSE;
          ERR_ClearWarning();
          msg = CMD_SET_UNDERCELL;
          ODV_MachineMode = GATEWAY_MODE_NORM;
          MBX_post(&sci_rx_mbox, &msg, 0);
        }
      break;

      case GATEWAY_MODE_NORM:
      //Mode normal relay closed
        if (ODV_SysTick_ms - start >= 50 && !checkenable){
          if (volt1 < ODP_SafetyLimits_UnderVoltage){
            ERR_HandleWarning(WARN_LOW_CAPACITY);
            ODV_MachineMode = GATEWAY_MODE_CONLY;
            start = ODV_SysTick_ms;
            checkenable = FALSE;
          }
          else{
            msg = CMD_SET_UNDERCELL;
            MBX_post(&sci_rx_mbox, &msg, 0);
            checkenable = TRUE;
          }
        }
        if (volt1 < ODP_SafetyLimits_UnderVoltage) { //(MMSConfig->ch_wk == 1 && CHARGE_IN == 0 && checkenable && (volt1 < ODP_SafetyLimits_UnderVoltage)){//charger in  /// test Charge In
          ODV_MachineMode = GATEWAY_MODE_CONLY;
          start = ODV_SysTick_ms;
          checkenable = FALSE;
        }
        if (ODV_Gateway_State == GATEWAY_STATE_CHARGE_ONLY && ODV_ErrorDsp_ErrorNumber == 0 || volt1 < ODP_SafetyLimits_UnderVoltage && ODV_ErrorDsp_ErrorNumber == 0){ // Johnny test ODP_SafetyLimits_Umin 25.04.2019
          ++volt_count;
          ERR_HandleWarning(WARN_LOW_CAPACITY);
          if (volt_count > (uint32)ODP_SafetyLimits_Voltage_delay*1000){
            ERR_HandleWarning(WARN_LOW_CAPACITY);
			//ERR_ErrorUnderVoltage();
            ODV_MachineMode = GATEWAY_MODE_CONLY;
            volt_count = 0;
            start = ODV_SysTick_ms;
            checkenable = FALSE;
          }
        }
        else if (ODV_Gateway_State == GATEWAY_STATE_DISCHARGE_ONLY && ODV_ErrorDsp_ErrorNumber == 0 || volt > ODP_SafetyLimits_Umax && checkenable && ODV_ErrorDsp_ErrorNumber == 0){ // Johnny test ODP_SafetyLimits_Umax 25.04.2019
          ++volt_count;
          if (volt_count > (uint32)ODP_SafetyLimits_Voltage_delay*1000){
             ERR_HandleWarning(WARN_OVERCAPACITY); //johnny 25.04.2019
			//ERR_ErrorOverVoltage();
            ODV_MachineMode = GATEWAY_MODE_DONLY;
            volt_count = 0;
            start = ODV_SysTick_ms;
            checkenable = FALSE;
          }
        }
        else if (volt_count > 0) --volt_count;
      break;

      case GATEWAY_MODE_ERROR_IN:
        start = ODV_SysTick_ms;
        checkenable = FALSE;
        ODV_MachineMode = GATEWAY_MODE_ERROR;
      case GATEWAY_MODE_CRASH:
      case GATEWAY_MODE_ERROR:
      default:
        if (ODV_SysTick_ms - start >= ODP_CommError_Delay && !checkenable){
          checkenable = TRUE;
          RELAY_VALUE = 0;
          PAR_AddLog();
        }
        if (ODV_Gateway_State == GATEWAY_STATE_STILL && ODV_MachineMode != GATEWAY_MODE_CRASH){
          ODV_MachineMode = GATEWAY_MODE_CLR;
        }
        if (HAL_CellOK == 2 && ODV_ErrorDsp_ErrorNumber == 0){
          ODV_MachineMode = GATEWAY_MODE_CLR;
        }
      break;
      case GATEWAY_MODE_CLR:
        if (ERR_ClearError()){
          ERR_ClearWarning();
          msg = CMD_SET_CHANNELS;
          if (!MBX_post(&sci_rx_mbox, &msg, 0))
          {
        	//clear error
           // ERR_ErrorComm();  //Johnny Test 30.03.2021
          } else {
            //ODV_Gateway_Errorcode = 0;
            ODV_MachineMode = GATEWAY_MODE_STILL;
            start = ODV_SysTick_ms;
            checkenable = FALSE;
          }
        }
      break;
    }//end switch
    TSK_sleep(1);
  }
}


/***************************** Object Dictionary Callback functions ********************************/
UNS32 SecurityCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_READ) {
    if (ODP_RandomNB == 0) HAL_Random();
  }
  if (access == DIC_WRITE) {
    if (ODP_RandomNB == 0) HAL_Random();
    else HAL_Unlock();
  }
  return(0);
}

extern int8 TimeLogIndex;
UNS32 LogNBCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  /* callback to get log */
  if (access == DIC_WRITE) {
    PAR_GetLogNB(ODV_Gateway_LogNB);
  }
  else{
    ODV_Gateway_LogNB = TimeLogIndex;
  }
  return(OD_SUCCESSFUL);
}


UNS32 WriteTextCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  /* callback for the lcd to write some text */
  if (access == DIC_WRITE) {
    //LCD_WriteText(ODV_LCD_Pos,(char *)&ODV_LCD_Text,ODV_LCD_Len);
    I2C_Command(RTC_WRITE,(char *)&ODV_RTC_Text,8,0);
  }
  else{
    //LCD_ReadText(ODV_LCD_Pos,(char *)&ODV_LCD_Text,ODV_LCD_Len);
    I2C_Command(RTC_READ,(char *)&ODV_RTC_Text,8,0);
  }
  return(OD_SUCCESSFUL);
}

UNS32 MotCurCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if(access == DIC_WRITE){
    if (bSubindex == 2){
      HAL_NewCurPoint = TRUE;
    }
  }
  return(OD_SUCCESSFUL);
}


UNS32 SciSendCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE) {
    MBX_post(&sci_rx_mbox, &ODV_SciSend, 0);
    //SCI_MsgInRS232.cmd = ODV_SciSend;
  }
  return(OD_SUCCESSFUL);
}

UNS32 ConfigCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE) {
    if (MMSConfig->relay_on == 1) RELAY_VALUE = 1;
    if (MMSConfig->relay_on == 0) RELAY_VALUE = 0;    
  }
  return(OD_SUCCESSFUL);
}

UNS32 BatteryCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE){
    if (bSubindex == 1)
    {
    	/// Johnny Testing 29.03.2021 Current measurments 1Sec
      PAR_Capacity_Total = CNV_Round(ODP_Battery_Capacity*3600*32);  /// 100 Amperes-hour to Amperes-second = 360000  40 Amperes-hour to Amperes-second = 144000...32 ist Ampere Schritte or scaling factor
     // PAR_Capacity_Left = (uint64)PAR_Capacity_Total * ODV_SOC_SOC2 / 100;
      PAR_Capacity_TotalLife_Used = 0; /// add a parameter to set
    }
  }
  return(OD_SUCCESSFUL);
}

UNS32 StartRecorderCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
/* callback for the recorder to record what desired */
  if (access == DIC_WRITE) {
    REC_StartRecorder();
  }
  return(OD_SUCCESSFUL);
}

UNS32 MultiunitsCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
/* Function called whenever the GUI want to know all the units of the board */
  if (access == DIC_WRITE) {
    ODV_Recorder_Multiunits = PAR_AddMultiUnits(ODV_Recorder_Multiunits);
  }
  return(OD_SUCCESSFUL);
}

UNS32 VariablesCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
/* Function called whenever the GUI want to know all the vars of the board */
  if (access == DIC_WRITE){
    ODV_Recorder_Variables = PAR_AddVariables(ODV_Recorder_Variables);
  }
  return(OD_SUCCESSFUL);
}


UNS32 WriteOutputs8BitCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE);
  return(OD_SUCCESSFUL);
}

UNS32 WriteOutputs16BitCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE){
    //nothing to do see relay state machine
  }
  return(OD_SUCCESSFUL);
}

UNS32 ReadInputs8BitsCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  return(OD_SUCCESSFUL);
}

UNS32 ControlWordCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE) {}
  return(OD_SUCCESSFUL);
}

UNS32 VersionCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  //if (access == DIC_READ) ODV_Version = NGTEST_VERSION;
  return(OD_SUCCESSFUL);
}

UNS32 WriteAnalogueOutputsCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  //if (access == DIC_WRITE) HAL_SetValveDuty(bSubindex-1,ODV_Write_Analogue_Output_16_Bit[bSubindex-1]);
  return(OD_SUCCESSFUL);
}


UNS32 CommErrorSetCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE){

  }
  return(OD_SUCCESSFUL);
}

UNS32 SaveAllParameters(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (ODV_StoreParameters == 0x73617665) {
    ODV_StoreParameters = 0;
    PAR_WriteAllPermanentParam(BoardODdata);
  }
  return(0);
}

UNS32 LoadDefaultParameters(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (ODV_RestoreDefaultParameters == 0x6C6F6164) {
    ODV_RestoreDefaultParameters = 0;
    PAR_UpdateCode(FALSE);
    BootST.Command = 1;
  }
  return(0);
}


UNS32 DebugCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  ODV_Debug = 0;
  return(0);
}

UNS32 ResetCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (ODV_ResetHW == 0x7A65726F) {
    ODV_ResetHW = 0;
    BootST.Command = 1;
  }
  return(0);
}




