 /**********************************************************************

   sci1.c - sci1 hardware description file

 ----------------------------------------------------------------------
   Author: PTM: RP/OS                     Date: Wed 09.07.2014
   For   : SMTEC SA
   Compil: CCSv5     Target: DSP2806x
   descr.: define hardware constants for C and assembler
   Edit  :
 ******************************************************************************/

#include "uc.h"
#include "dspspecs.h"
#include "ngtestspec.h"
#include "mms_dict.h"
#include "sci1.h"
#include "hal.h"
#include "error.h"
#include "convert.h"

//Definition pour la communication SCI entre processeurs
#define SCI_START_FLAG 0xA8
#define SCI_END_FLAG   0xA8
#define SCI_CTRL       0xA9
#define SCI_STUFFING   0xDF
#define SCI_DESTUFFING 0x20

#if UC_PHI == 80000000   //TI F2806x LOSPCP=2
const T_BaudCfg BaudTable[MAXBAUD]=
 {{9600,1,4},{19200,0,129},{38400,0,64},{57600,0,42},{115200,0,21}
 ,{230400,0,10},{250000,0,9},{500000,0,4}
 };
#else
//  ERROR : no baud rate table defined for your UC_PHI
const T_BaudCfg BaudTable[MAXBAUD]=
 {{9600,1,4},{19200,0,129},{38400,0,64},{57600,0,42},{115200,0,21}
 ,{230400,0,10},{250000,0,9},{500000,0,4}
 };
#endif

uint16 RS232RecvState = STATE_RX_IDLE;  /* Reset state machine*/
uint16 SCI1_CheckSum = 0;
uint16 RS232Timer = 0;
uint16 SCI_CharLen;
uint16 *SCI_CharPtr;
T_UsbMessage SCI_MsgInRS232;
bool1 ByteStuffed = FALSE;
bool1 ComStarted = FALSE;
volatile bool1 SCI_MsgAvailable = FALSE;
bool1 SCI_Available = FALSE;
uint16 SCI_GpioState = 0;
volatile struct SCI_REGS* SciRegs;
uint16 ModuleVolt = 0;				/// Save Old Module Voltage
uint32 Low_Voltage_ResetCounter = 0;

/// Johnny Test
int8 MaxTemp;
int8 MinTemp;
uint8 i16 = 0;
int32 Sumtemp_Max = 0;
int32 Sumtemp_Min = 0;
int16 Module1_Average_Temp_Max = 0;
int16 Module1_Average_Temp_Min = 0;
int8 Temp_Tempareture_Max = 0;
int8 Temp_Tempareture_Min = 0;
int8 Temp_Tempareture1 = 0;
int8 Temp_Tempareture2 = 0;
int8 Temp_Tempareture3 = 0;
uint16 CurrCounterSec = 0;
///

void SCI1_InitGpio(void)
{
//EN FAIT 078
#ifdef PTM068
  EALLOW;
  GpioCtrlRegs.GPAPUD.bit.GPIO7 = 0;
  GpioCtrlRegs.GPAPUD.bit.GPIO29 = 0;
  //GpioCtrlRegs.GPAQSEL2.bit.GPIO23 = 3;
  //GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 3;
  //GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 3;
  EDIS;
#endif
}

void SCI1_RS232Init(void)
{
  uint16 baudindex = BAUD_250000;

  EALLOW;
#ifdef PTM068
  SciRegs = &SciaRegs;
  PieVectTable.SCIRXINTA = &RS232_RXI_Int;
  //PieVectTable.SCITXINTA = &RS232_TEI_Int;
#endif
  SysCtrlRegs.LOSPCP.all = LOW_CLOCK_PRESCALER; //already in device init put again just to be sure
  EDIS;

  SciRegs->SCICTL1.all =0x0000; //disable sci
  SciRegs->SCIFFTX.all=0x4040; //SCIFFENA(b14), Reset the FIFO pointer to 0 and hold in reset (, Clear TXFIFO interrupt (b6)
  SciRegs->SCIFFRX.all=0x404F; //clear overflow, Reset the FIFO pointer to zero, Clear RXFFINT (b6)

  /* sci */
  //set mode N81 and baudrate
  SciRegs->SCICCR.all = SCI_N81;  //N81
  if (baudindex >= MAXBAUD) baudindex = 0;
  SciRegs->SCIHBAUD = BaudTable[baudindex].n;
  SciRegs->SCILBAUD = BaudTable[baudindex].brr;
  //initalize the SCI FIFO p2.17,p2.18,p2.19
  SciRegs->SCIFFTX.all=0xE000; // (/reset)b15, SCIFFENA(b14), /FIFOreset(b13)
  SciRegs->SCIFFRX.all=0x2021; //(b13)/fiforeset, fifo int enable(b5), Set RXFFIL to 1 00001(b0-4)
  SciRegs->SCIFFCT.all=0x0;    //No delay between every transfer from FIFO
  //enable the interrupt
#ifdef PTM068
  PieCtrlRegs.PIEIER9.bit.INTx1 = 1; //enable sciarx int
  //PieCtrlRegs.PIEIER9.bit.INTx2 = 1; //enable sciatx int
#endif
  SciRegs->SCICTL1.all = 0x0063; //enable error int, transmitter and receiver
  RS232RecvState = STATE_RX_IDLE;  /* Reset state machine*/
  IER |= M_INT9;
}

void RS232ReceiveEnable(void)
{
  uint8 msg;
  //disable
  EALLOW;
  SciRegs->SCIFFRX.bit.RXFFIENA = 0;    /*| Disable rx ints  */
  SciRegs->SCICTL1.bit.RXERRINTENA = 0; /*| */
  SciRegs->SCICTL1.bit.RXENA = 0;       /*| Disable rx  */
  SciRegs->SCICTL1.bit.TXENA = 0;       /*| Disable tx  */
  SciRegs->SCICTL1.bit.SWRESET = 0;     /*| Set and hold in reset sci */
  SciRegs->SCIFFTX.bit.TXFIFOXRESET = 0;/*| Set /reset tx fifo */
  SciRegs->SCIFFRX.bit.RXFIFORESET = 0; /*| Set /reset rx fifo */
  SciRegs->SCICTL1.bit.SWRESET = 1;     /*| Release reset */
  SciRegs->SCIFFTX.bit.TXFIFOXRESET = 1;/*| Release tx fifo reset */
  SciRegs->SCIFFRX.bit.RXFIFORESET = 1; /*| Release rx fifo reset */
  GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 0; //set gpio as gpio
  GpioDataRegs.GPADAT.bit.GPIO29 = 0;  //set gpio at 0
  DELAY_US(250);  //reset chip
  GpioDataRegs.GPADAT.bit.GPIO29 = 1;  //set gpio at 1
  GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 1; //set gpio as uart
  //enable
  SciRegs->SCIFFRX.bit.RXFFIL = 1;      /*| set receive fifo int to 1 char*/
  SciRegs->SCICTL1.bit.TXENA = 1;       /*| Enable tx */
  SciRegs->SCICTL1.bit.RXENA = 1;       /*| Enable RX     */
  SciRegs->SCICTL1.bit.RXERRINTENA = 1; /*| Enable rx err int */
  SciRegs->SCIFFRX.bit.RXFFIENA = 1;    /*| Enable rx int   */
  EDIS;
  while(MBX_pend(&sci_rx_mbox, &msg, 0));
  msg = CMD_SET_CHANNELS; MBX_post(&sci_rx_mbox, &msg, 0);
  RS232RecvState = STATE_RX_IDLE;/* Reset state machine */
  ByteStuffed = FALSE;
  SCI_Available = TRUE;
  ComStarted = FALSE;
}


void RS232ReceiveDisable(void)
{ //disable
  SciRegs->SCIFFRX.bit.RXFFIENA = 0;    /*| Disable rx ints */
  SciRegs->SCICTL1.bit.RXERRINTENA = 0; /*| */
  SciRegs->SCICTL1.bit.RXENA = 0;       /*| Disable rx  */
  SciRegs->SCICTL1.bit.TXENA = 0;       /*| Disable tx  */
}

void SCI1_Update(void){//called by millisecint
  if (RS232Timer){
    if (--RS232Timer == 0) //timeout
      ODV_CommError_Set = 1; //reset
  }
}

uint16 GetWord(uint16 index){
  if (index < MAX_DATA/2){
    return (SCI_MsgInRS232.data[index*2]*256 + SCI_MsgInRS232.data[index*2+1]);
  }
  else
    return 0;
}

void SetWord(uint16 index, uint16 data){
  if (index < MAX_DATA/2){
    SCI_MsgInRS232.data[index*2] = data>>8;
    SCI_MsgInRS232.data[index*2+1] = data & 0xFF;
  }
}

void SCI1_ReadMsg(uint8 reg, uint16 *buf, uint8 len){
  uint8 i = 0;
  uint16 data;
  if (len > 0 && len < 9){
    SCI_MsgInRS232.len = len+3;
    SCI_MsgInRS232.data[0] = 0x80+len;
    if (len == 8) SCI_MsgInRS232.data[0] = 0x87;
    SCI_MsgInRS232.data[1] = 00;
    SCI_MsgInRS232.data[2] = reg;
    while (i<len){
      data = *buf++;
      SCI_MsgInRS232.data[i+3] = data>>8;
      SCI_MsgInRS232.data[i+4] = data&0xFF;
      i+=2;
    }
    SCI1_SendMessage(&SCI_MsgInRS232); //answer expected so keep sci in use
    RS232Timer = 20; //10ms timeout
  }
  else
    SCI_Available = TRUE; //error free sci
}

void SCI1_WriteMsg(uint8 reg, uint16 *buf, uint8 len){
  uint8 i = 0;
  uint16 data;
  if (len > 0 && len < 9){
    SCI_MsgInRS232.len = len+3;
    SCI_MsgInRS232.data[0] = 0x90+len;
    if (len == 8) SCI_MsgInRS232.data[0] = 0x97;
    SCI_MsgInRS232.data[1] = 00;
    SCI_MsgInRS232.data[2] = reg;
    while (i<len){
      data = *buf++;
      SCI_MsgInRS232.data[i+3] = data>>8;
      SCI_MsgInRS232.data[i+4] = data&0xFF;
      i+=2;
    }
    SCI1_SendMessage(&SCI_MsgInRS232);
  }
}

uint16 toto = 0x1200;
void SCI1_Command(void){//called by task
  uint16 data[4], temp;
  float val;
  switch (SCI_MsgInRS232.cmd){
    case CMD_ERROR:
    break;
    case CMD_WRITE_EEPROM:
      data[0] = 0x8C2D; //magic1
      data[1] = 0xB194; //magic1
      SCI1_WriteMsg(0x82,data,4);
      TSK_sleep(1);
      data[0] = 0xA375; //magic2
      data[1] = 0xE60F; //magic2
      SCI1_WriteMsg(0xFC,data,4);
      TSK_sleep(1);
      data[0] = 0x1000; //write eeprom
      SCI1_WriteMsg(0x0C,data,1);
      TSK_sleep(200);//delay for writing
      SCI_Available = TRUE;//no answer expected
    break;
    case CMD_CLEAR_ERROR:
      data[0] = 0x38FF; //clear faults
      data[1] = 0xC000; //clear faults
      SCI1_WriteMsg(0x51,data,3);
      SCI_Available = TRUE;//no answer expected
    break;
    case CMD_READ_EEPROM_C:
      data[0] = 0x0000;
      SCI1_ReadMsg(0xFA,data,1);
    break;
    case CMD_READ_ERROR:
      data[0] = 0x0700;
      SCI1_ReadMsg(0x52,data,1);
    break;
    case CMD_READ_CRC:
      data[0] = 0x0300;
      SCI1_ReadMsg(0xF4,data,1);
    break;
    case CMD_READ_CHIP:
      data[0] = 0x0100;
      SCI1_ReadMsg(0xC6,data,1);
    break;
    case CMD_READ_CTO:
      data[0] = 0x0000;
      SCI1_ReadMsg(0x28,data,1);
    break;
    case CMD_SET_GPIO:
      data[0] = SCI_GpioState;
      SCI1_WriteMsg(0x79,data,1);
      SCI_Available = TRUE;//no answer expected
    break;
    case CMD_CLEAR_GPIO:
      data[0] = 0x0000;
      SCI1_WriteMsg(0x79,data,1);
      SCI_Available = TRUE;//no answer expected
    break;
    case CMD_SET_UNDERCELL:
      val = (float)ODP_SafetyLimits_UnderVoltage / 100.0 * 65535 / 5.0;
      temp = CNV_Round(val) & 0xFFFC;
      data[0] = temp; //set cell undervoltage
      SCI1_WriteMsg(0x8E,data,2);
      SCI_Available = TRUE;//no answer expected
    break;
    case CMD_SET_CHANNELS:
      data[0] = 0x8000; //mask cks error
      SCI1_WriteMsg(0x6B,data,2);
      TSK_sleep(1);
      data[0] = 0x3FFF; //14 cell
      data[1] = 0xFF00; //all aux, sum total
      data[2] = 0xFA00; //average 4 samples
      SCI1_WriteMsg(3,data,5);
      TSK_sleep(1);
      data[0] = ODP_SafetyLimits_Cell_Nb*256; //14 cells
      SCI1_WriteMsg(0x0D,data,1);
      TSK_sleep(1);
      data[0] = 0x3200; //set com timeout to 0x3=1.0s and fault to 2=0.5s.
      SCI1_WriteMsg(0x28,data,1);
      TSK_sleep(1);
      data[0] = 0x3F00; //set gpio as outputs and at 0.

      SCI1_WriteMsg(0x78,data,2);
      TSK_sleep(1);
      data[0] = 0x38FF; //clear faults
      data[1] = 0xC000; //clear faults
      SCI1_WriteMsg(0x51,data,3);//must be done before devconfig.
      TSK_sleep(1);
      data[0] = 0x1200;//devconfig enable hysteresis and unlatch fault.
      //SCI1_WriteMsg(0x0E,data,1);
      //TSK_sleep(1);
      temp = ODP_SafetyLimits_BalancingTimeout;
      if (temp == 0) data[0] = 0; else if (temp == 1) data[0] = 0x2000; else if (temp == 2) data[0] = 0x3000;
      else if (temp <= 5) data[0] = 0x4000; else if (temp <= 10) data[0] = 0x5000; else if (temp <= 15) data[0] = 0x6000;
      else if (temp <= 20) data[0] = 0x7000; else if (temp <= 30) data[0] = 0x8000; else data[0] = 0x9000;
      SCI1_WriteMsg(0x13,data,1);
      TSK_sleep(1);
      val = (float)ODP_SafetyLimits_DamagedVoltage / 10.0 - 0.7;
      temp = CNV_Round(val / 0.025);
      data[0] = temp*512; //set comparator undervoltage
      val = (float)(ODP_SafetyLimits_OverVoltage+10) / 100.0 - 2.0;//set comparator limit to +0.1V above safety to allow 0.1V spikes above safety.
      temp = CNV_Round(val / 0.025);
      data[0] += temp*2; //set comparator overvoltage
      val = (float)ODP_SafetyLimits_DamagedVoltage / 10.0 * 65535 / 5.0;
      temp = CNV_Round(val) & 0xFFFC;
      data[1] = temp; //set cell undervoltage
      val = (float)ODP_SafetyLimits_OverVoltage / 100.0 * 65535 / 5.0;
      temp = CNV_Round(val+4.0) & 0xFFFC;
      data[2] = temp; //set cell overvoltage
      SCI1_WriteMsg(0x8C,data,6);
      TSK_sleep(1);

      temp = HAL_LectureADTemperature(ODP_SafetyLimits_Resistor_Tmax,NTC_TYPE0);
      data[0] = temp & 0xFFFC; //set aux0 undervoltage
      temp = HAL_LectureADTemperature(ODP_SafetyLimits_Resistor_Tmin,NTC_TYPE0);
      data[1] = (temp) & 0xFFFC; //set aux0 overvoltage
      //ntc3 - NTC T5
      temp = HAL_LectureADTemperature(ODP_SafetyLimits_T3_max,NTC_TYPE1);
      data[2] = temp & 0xFFFC; //set aux1 undervoltage
      temp = HAL_LectureADTemperature(ODP_SafetyLimits_T3_min,NTC_TYPE1);
      data[3] = (temp) & 0xFFFC; //set aux1 overvoltage
      SCI1_WriteMsg(0x92,data,8);
      TSK_sleep(1);
      temp = CNV_Round((float)ODP_SafetyLimits_UnderCurrent / ODP_Analogue_Input_Scaling_Float[PIC_MEASURE_HAS500]
                       / ODP_Analogue_Input_Scaling_Float[3] - ODP_Analogue_Input_Offset_Integer[PIC_MEASURE_HAS500]);
      if (temp > 0x8000) temp = 0;
      if (MMSConfig->shunt == 0) temp = 0;
      data[0] = temp & 0xFFFC; //set aux2 undervoltage
      temp = CNV_Round((float)ODP_SafetyLimits_Overcurrent / ODP_Analogue_Input_Scaling_Float[PIC_MEASURE_HAS500]
                       / ODP_Analogue_Input_Scaling_Float[3] - ODP_Analogue_Input_Offset_Integer[PIC_MEASURE_HAS500]);
      if (temp < 0x8000) temp = 0xFFFF;
      if (MMSConfig->shunt == 0) temp = 0xFFFC;
      data[1] = (temp) & 0xFFFC; //set aux2 overvoltage
      data[2] = 0x0000; //set aux3 undervoltage
      data[3] = 0xFFFC; //set aux3 overvoltage
      SCI1_WriteMsg(0x9A,data,8);
      TSK_sleep(1);
      temp = HAL_LectureADTemperature(ODP_SafetyLimits_Mosfet_Tmax,NTC_TYPE0);
      data[0] = temp & 0xFFFC; //set aux4 undervoltage
      temp = HAL_LectureADTemperature(ODP_SafetyLimits_Mosfet_Tmin,NTC_TYPE0);
      data[1] = (temp) & 0xFFFC; //set aux4 overvoltage
      temp = CNV_Round((float)ODP_SafetyLimits_UnderCurrent / ODP_Analogue_Input_Scaling_Float[PIC_MEASURE_HAS500]
                       / ODP_Analogue_Input_Scaling_Float[3] - ODP_Analogue_Input_Offset_Integer[PIC_MEASURE_HAS500]);
      if (temp > 0x8000) temp = 0;
      if (MMSConfig->lem == 0) temp = 0;
      data[2] = temp & 0xFFFC; //set aux2 undervoltage
      temp = CNV_Round((float)ODP_SafetyLimits_Overcurrent / ODP_Analogue_Input_Scaling_Float[PIC_MEASURE_HAS500]
                       / ODP_Analogue_Input_Scaling_Float[3] - ODP_Analogue_Input_Offset_Integer[PIC_MEASURE_HAS500]);
      if (temp < 0x8000) temp = 0xFFFF;
      if (MMSConfig->lem == 0) temp = 0xFFFC;
      data[3] = (temp) & 0xFFFC; //set aux2 overvoltage
      SCI1_WriteMsg(0xA2,data,8);
      TSK_sleep(1);
      //ntc2 - NTC T4
      temp = HAL_LectureADTemperature(ODP_SafetyLimits_T3_max,NTC_TYPE1);
      data[0] = temp & 0xFFFC; //set aux6 undervoltage
      temp = HAL_LectureADTemperature(ODP_SafetyLimits_T3_min,NTC_TYPE1);
      data[1] = (temp) & 0xFFFC; //set aux6 overvoltage
      //ntc1 - NTC T3
      temp = HAL_LectureADTemperature(ODP_SafetyLimits_T3_max,NTC_TYPE1);
      data[2] = temp & 0xFFFC; //set aux7 undervoltage
      temp = HAL_LectureADTemperature(ODP_SafetyLimits_T3_min,NTC_TYPE1);
      data[3] = (temp) & 0xFFFC; //set aux7 overvoltage
      SCI1_WriteMsg(0xAA,data,8);
      TSK_sleep(1);
      data[0] = 0x38FF; //clear faults
      data[1] = 0xC000; //clear faults
      SCI1_WriteMsg(0x51,data,3);
      SCI_Available = TRUE;//no answer expected
    break;
    case CMD_READ_VALUES:
      data[0] = 0x0000;
      SCI1_ReadMsg(2,data,1);
    break;
    default:
    break;
  }
}

void SCI1_Receive(void){//called by task
  uint16 i, max, min, val, nb, val2;
  uint16 data[4];
  uint32 sum = 0;
  switch (SCI_MsgInRS232.cmd){
      case CMD_ERROR:
        //SCI_MsgInRS232.len = 0;
      break;
      case CMD_READ_ERROR|0x80:
        for (i=0;i<4;i++){
          data[i] = GetWord(i);
          //DV_Write_Analogue_Output_16_Bit[i+1] = data[i] = GetWord(i);
        }
        if (data[0]){
          //if (data[0] & 0x8000) ERR_ErrorUnderVoltage();
          //if (data[0] & 0x4000) ERR_ErrorOverVoltage();
          if (data[0] & 0x2000) {
          // if (data[3] & 0xD300) ERR_ErrorOverTemp(); //aux0,1,4,6,7
          // if (data[3] & 0x2C00) ERR_ErrorOverCurrent(); //aux2,3,5
          }
          if (data[0] & 0x1000) {
          // if (data[3] & 0xD3) ERR_ErrorOverTemp(); //aux0,1,4,6,7
          //  if (data[3] & 0x2C) ERR_ErrorOverCurrent(); //aux2,3,5
          }
          // if (data[0] & 0x0800) ERR_ErrorUnderVoltage();
          // if (data[0] & 0x0400) ERR_ErrorOverVoltage();
          // if (data[0] & 0x0200) ERR_ErrorComm1();
          // if (data[0] & 0x0200) ERR_ErrorComm2();
          // if (data[0] & 0x0200) ERR_ErrorComm();
          // if (data[0] & 0x0180) ERR_SetError(ERR_CHIPSYS);
          // if (data[0] & 0x0040) ERR_CONTACTOR_PLUS();
        }
        //else ERR_ClearError();
      break;
      case CMD_READ_EEPROM_C|0x80:
        ODV_SciSend = SCI_MsgInRS232.data[0];
      break;
      case CMD_READ_CRC|0x80:
        data[0] = GetWord(0);
        data[1] = GetWord(1);
        SCI1_WriteMsg(0xF0,data,4);
        //data[2] = GetWord(2);
      break;
      case CMD_READ_CHIP|0x80:
        ODP_Board_SerialNumber = GetWord(0);
      break;
      case CMD_READ_CTO|0x80:
        ODV_SciSend = SCI_MsgInRS232.data[0];
      break;
      case CMD_READ_VALUES|0x80:
        max = 0; min = 65535; nb = ODP_SafetyLimits_Cell_Nb;
        for (i=0;i<nb;i++){
          val = GetWord(13-i);
          sum += val;
          if (val > max) max = val;
          if (val < min) min = val;
          ODV_Read_Analogue_Input_16_Bit[i] = val/2;
        }		
        val = CNV_Round((float)ODP_SafetyLimits_Umax_bal_delta/gaincell + min/2);//on
        val2 = CNV_Round((float)ODP_SafetyLimits_Umin_bal_delta/gaincell + min/2);//off
        ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP1] = HAL_LectureTemperature((float)GetWord(PIC_MEASURE_TEMP1)/16,NTC_TYPE0);
        ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP2] = HAL_LectureTemperature((float)GetWord(PIC_MEASURE_TEMP2)/16,NTC_TYPE0);
        ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP3] = HAL_LectureTemperature((float)GetWord(PIC_MEASURE_TEMP3)/16,NTC_TYPE1);
        ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP4] = HAL_LectureTemperature((float)GetWord(PIC_MEASURE_TEMP4)/16,NTC_TYPE1);
        ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_NTC3]  = HAL_LectureTemperature((float)GetWord(PIC_MEASURE_NTC3)/16,NTC_TYPE1);;
        //only on M&R version
        ODV_Read_Analogue_Input_16_Bit[MEASURE_SHUNT] = GetWord(MEASURE_SHUNT) + ODP_Analogue_Input_Offset_Integer[MEASURE_SHUNT];
        ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_FOIL] = GetWord(PIC_MEASURE_FOIL);
        ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_HAS500] = GetWord(PIC_MEASURE_HAS500) + ODP_Analogue_Input_Offset_Integer[PIC_MEASURE_HAS500];
        //value computation
        if (ODV_ErrorDsp_ErrorNumber != 0)
        {
        	ODV_Module1_Voltage = CNV_Round((float)sum * gaincell / 2); //mV;
        }
        else
        {
        	uint16 NeuModuleVolt = CNV_Round((float)sum * gaincell / 2); //mV
        	if (abs(NeuModuleVolt - ModuleVolt) > (100))
        	{
        		ODV_Module1_Voltage = NeuModuleVolt;
        		ModuleVolt = NeuModuleVolt;
        	}

       // ODV_Module1_Voltage = CNV_Round((float)sum * gaincell / 2); //mV
        }
        ODV_Module1_MinCellVoltage = CNV_Round((float)min * gaincell / 40); //V*50
        ODV_Module1_MaxCellVoltage = CNV_Round((float)max * gaincell / 40); //V*50
        ODV_Module1_MaxDeltaVoltage = ODV_Module1_MaxCellVoltage - ODV_Module1_MinCellVoltage;
        
		Temp_Tempareture1 = ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP3]/10;
        Temp_Tempareture2 = ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP4]/10;
        Temp_Tempareture3 = ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_NTC3]/10;

        int Temp_Tempareture21 = Temp_Tempareture1;
        int Temp_Tempareture22 = Temp_Tempareture2;
        int Temp_Tempareture23 = Temp_Tempareture3;

        if((Temp_Tempareture21) < 100 && (Temp_Tempareture21 > -50))
        {
        }else
        {
        	Temp_Tempareture21 = 100;
        }
        if((Temp_Tempareture22) < 100 && (Temp_Tempareture22 > -50))
        {
        }else
        {
        	Temp_Tempareture22 = 100;
        }
        if((Temp_Tempareture23) < 100 && (Temp_Tempareture23 > -50))
        {
        }else
        {
        	Temp_Tempareture23 = 100;
        }

        if ( Temp_Tempareture1 > Temp_Tempareture2 && Temp_Tempareture1 > Temp_Tempareture3 )
        {
        	Temp_Tempareture_Max = Temp_Tempareture1;
        }
        else if ( Temp_Tempareture2 > Temp_Tempareture1 && Temp_Tempareture2 > Temp_Tempareture3 )
        {
        	Temp_Tempareture_Max = Temp_Tempareture2;
        }
        else if ( Temp_Tempareture3 > Temp_Tempareture1 && Temp_Tempareture3 > Temp_Tempareture2 )
        {
        	Temp_Tempareture_Max = Temp_Tempareture3;
        }
        else
        {
        	Temp_Tempareture_Max = Temp_Tempareture3;
        }

        if ( Temp_Tempareture21 < Temp_Tempareture22 && Temp_Tempareture21 < Temp_Tempareture23 )
         {
        	 Temp_Tempareture_Min = Temp_Tempareture21;
         }
         else if ( Temp_Tempareture22 < Temp_Tempareture21 && Temp_Tempareture22 < Temp_Tempareture23 )
         {
        	 Temp_Tempareture_Min = Temp_Tempareture22;
         }
         else if ( Temp_Tempareture23 < Temp_Tempareture21 && Temp_Tempareture23 < Temp_Tempareture22 )
         {
        	 Temp_Tempareture_Min = Temp_Tempareture23;
         }
         else
         {
        	 Temp_Tempareture_Min = Temp_Tempareture21;
         }

        /// Johnny Test
      /*  if (Temp_Tempareture <ODV_Module1_Temperature2)
        {
        	MaxTemp = ODV_Module1_Temperature2;
        	MinTemp = Temp_Tempareture;
        	Temp_Tempareture = MaxTemp;
        	ODV_Module1_Temperature2 = MinTemp;
        }*/

        i16++;
        if (i16 == 200)
        {
        	Module1_Average_Temp_Max = Sumtemp_Max/200;
        	Sumtemp_Max = 0;
        	Module1_Average_Temp_Min = Sumtemp_Min/200;
        	Sumtemp_Min = 0;
        }
        if (i16 < 200)
        {
        	Sumtemp_Max = Sumtemp_Max + ((Temp_Tempareture_Max));
        	Sumtemp_Min = Sumtemp_Min + ((Temp_Tempareture_Min));
        }
        else
        {
        	i16 = 0;
        }
        ODV_Temperature_Average_data = Module1_Average_Temp_Max;
        ODV_Temperature_Min = Module1_Average_Temp_Min;
		
        //F version no current measure
        if (MMSConfig->lem)
        {
        	ODV_Module1_Current_Average  = CNV_Round((float)ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_HAS500] * ODP_Analogue_Input_Scaling_Float[PIC_MEASURE_HAS500]);
        	// ODV_Module1_Current = CNV_Round((float)ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_HAS500] * ODP_Analogue_Input_Scaling_Float[PIC_MEASURE_HAS500]);

        }
       /* else if (MMSConfig->shunt)
        {
          ODV_Module1_Current = CNV_Round((float)ODV_Read_Analogue_Input_16_Bit[MEASURE_SHUNT] * ODP_Analogue_Input_Scaling_Float[MEASURE_SHUNT]);
        }*/
        else
        ODV_Module1_Current = ODV_Gateway_Current*8; //0.25A *8 [0.03125A]

        ////////////////////////////////////////////////////////
        /*
         * Current Calibratiion and Caluculation with SOC related
         */

        /*
         *  when Battery is First Time Flasched then we reset the Battery SOC to 50% Other than that
         *  ODV_Module1_MaxCellVoltage should have a new battery range  3,5V
         */
       /* if(PAR_Capacity_Left == 0)
        {
        	if (((ODV_Module1_MaxCellVoltage*2) < 360) && ((ODV_Module1_MaxCellVoltage*2) > 340))
        	{
        		PAR_Capacity_Left = (50) * (PAR_Capacity_Total/100);
        	}
        }*/
		/*
		 *
		 */
        if (ODV_Module1_Current > 0)
        {
        	PAR_Capacity_TotalLife_Used += ODV_Module1_Current;
        }

        if(ODV_Module1_Module_SOC_Calibration != 0)
        {
        	PAR_Capacity_Left = (ODV_Module1_Module_SOC_Calibration) * (PAR_Capacity_Total/100);
        	CurrCounterSec = 0;
        }
        else
        {
        	if(CurrCounterSec > 100)
        	{
        		CurrCounterSec = 0;

        		HAL_Current_Sum += ODV_Module1_Current; /// change this to Seconds  /// Johnny Testing 29.03.2021 Current measurments 1Sec

        		if ((int64)PAR_Capacity_Left + ODV_Module1_Current < 0)
        		{
        			if(ODV_Module1_Module_SOC_Calibration == 0)
        			{
        				//PAR_Capacity_Left = 0;
        			}
        		}
        		else
        		{
        			if(ODV_Module1_Module_SOC_Calibration == 0)
        			{
        				if ((abs(ODV_Module1_Current) < (ODP_Module1_Module_SOC_Current*32)))
        				{
        					PAR_Capacity_Left += ODP_Module1_StandbyCurrent; /// around 60mA Standby Mode Current#
        				}
        				else
        				{
        					PAR_Capacity_Left += ODV_Module1_Current;
        				}
        			}
        			else
        			{
        				//PAR_Capacity_Left = (ODV_Module1_Module_SOC_Calibration) * (PAR_Capacity_Total/100);
        			}
        		}
        	}
        }
        CurrCounterSec = CurrCounterSec+1;

        if (((ODV_Module1_MaxCellVoltage*2)  >= ODP_SafetyLimits_Umax) && ODV_Module1_Current >= 6)
        {
        	PAR_Capacity_Left = PAR_Capacity_Total;
        }
        else if(((ODV_Module1_MaxCellVoltage*2)  <= ODP_SafetyLimits_Umin) && (ODV_ErrorDsp_ErrorNumber == 0))
        {
        	Low_Voltage_ResetCounter++;

        	if (Low_Voltage_ResetCounter > 1200)
        	{
        		PAR_Capacity_Left = 0;

        		/*if(PAR_Capacity_Left > PAR_Capacity_Total + 1000)
        		{

        		}*/
        	}
        }
        else
        {
        	Low_Voltage_ResetCounter = 0;
        }

        /// 6000 -> 60sec Testing with Counter
        /*
         * Testing 60Sec -> Counter Current 236 * 0.03125 = 7.375A/60Sec
         *  It was for every 10ms Counter used now 1Sec Counter
         *
         */

      //  if (PAR_Capacity_Left > PAR_Capacity_Total) PAR_Capacity_Left = PAR_Capacity_Total;
        if ((PAR_Capacity_Left > PAR_Capacity_Total) && (PAR_Capacity_Left < PAR_Capacity_Total + 3000))
         {
         	//PAR_Capacity_Left = PAR_Capacity_Total;
         }

        //set balancing
        if (((ODV_Module1_Current > 2 && MMSConfig->bal_ch) || (ODV_Module1_Current < -2 && MMSConfig->bal_dic))
            && (ODV_Module1_MaxCellVoltage*2 <= ODP_SafetyLimits_Umax) && (ODV_Module1_MinCellVoltage*2 >= ODP_SafetyLimits_Umin)){
          min = ODV_Read_Inputs_16_Bit[1];
          if (min&2){
            ODV_Read_Analogue_Input_16_Bit[0] -= ODP_Analogue_Input_Offset_Integer[MEASURE_CELL1];
            ODV_Read_Analogue_Input_16_Bit[1] += ODP_Analogue_Input_Offset_Integer[MEASURE_CELL2];
          }
          if (min&1) ODV_Read_Analogue_Input_16_Bit[0] += ODP_Analogue_Input_Offset_Integer[MEASURE_CELL1];
          for (i=0;i<nb;i++){
            if (ODV_Read_Analogue_Input_16_Bit[i] > val)
              ODV_Read_Inputs_16_Bit[1] |= 1<<i;
            if ((ODV_Read_Inputs_16_Bit[1] & (1<<i)) && (ODV_Read_Analogue_Input_16_Bit[i] < val2))
              ODV_Read_Inputs_16_Bit[1] &= ~(1<<i);
          }
          if (min != ODV_Read_Inputs_16_Bit[1])
            SCI1_WriteMsg(0x14,&ODV_Read_Inputs_16_Bit[1],2);
        }
        else if (ODV_Read_Inputs_16_Bit[1] != ODV_Write_Outputs_16_Bit[1]){
          ODV_Read_Inputs_16_Bit[1] = ODV_Write_Outputs_16_Bit[1];
          SCI1_WriteMsg(0x14,&ODV_Read_Inputs_16_Bit[1],2);
        }
        HAL_Enable = TRUE;
      break;
  }
}


#define SCI_FIFO_WORD_MAX 4

void Put_Sci1(uint8 ch){
  uint16 fifo_len = SciRegs->SCIFFTX.bit.TXFFST;
  while(fifo_len >= SCI_FIFO_WORD_MAX){
    fifo_len = SciRegs->SCIFFTX.bit.TXFFST;
  }
  SciRegs->SCITXBUF = ch;
}

uint16 SCI1_ComputeCRC16(uint8* buf, uint16 len){
  uint32 crc = 0, temp = 0;
  uint16 j;
  while (len--) {
    temp = *buf++;
    crc |= (temp<<16);
    for (j = 0; j < 8; j++)
      crc = (crc >> 1) ^ ((crc & 1) ? 0xa001 : 0);
  }
  return crc;
}

void SCI1_SendMessage(T_UsbMessage *msg){
  uint16 len, cks;
  uint8 i;
  
  len =  msg->len;
  SCI1_CheckSum = 0;
  for (i=0; i<len; i++) {
    Put_Sci1(msg->data[i]);
  }
  msg->data[len] = 0; msg->data[len+1] = 0;
  cks = SCI1_ComputeCRC16(msg->data,len+2);
  Put_Sci1(cks & 0xFF);
  Put_Sci1(cks / 0x100);
  msg->data[len] = cks;
  msg->cmd |= 0x80;
}


#if 0
interrupt void RS232_RXI_Int(void)
/* Receive interrupt : triggered by receive data register full (SSR.RDRF) */
{
  uint8 incar;

  incar = SciRegs->SCIRXBUF.bit.RXDT;  /* Read received char */
  SciRegs->SCIFFRX.bit.RXFFOVRCLR = 1;//clear overflow
  SciRegs->SCIFFRX.bit.RXFFINTCLR = 1;//Reset interrupt flag
  AcknowlegeInt |= M_INT9;

  if (SciRegs->SCIRXST.bit.RXERROR)
  { //A reception error occured -> Reset the communication
    RS232ReceiveEnable();
    return;
  }
  if(!MBX_post(&sci_rx_mbox, &incar, 0)){

  }
}



#else
interrupt void RS232_RXI_Int(void)
/* Receive interrupt : triggered by receive data register full (SSR.RDRF) */
{
  uint8 incar;
  uint16 cks = 0, len;

  incar = SciRegs->SCIRXBUF.bit.RXDT;  /* Read received char */
  SciRegs->SCIFFRX.bit.RXFFOVRCLR = 1;//clear overflow
  SciRegs->SCIFFRX.bit.RXFFINTCLR = 1;//Reset interrupt flag
  AcknowlegeInt |= M_INT9;

  if (SciRegs->SCIRXST.bit.RXERROR)
  { //A reception error occured -> Reset the communication
    ODV_CommError_Set = 1;
    return;
  }
  switch (RS232RecvState)
  {
    case STATE_RX_IDLE:
    //if (incar < MAX_DATA-3){
      SCI_MsgInRS232.len = incar;
      ComStarted = TRUE;
      SCI_CharPtr = (uint16*)&SCI_MsgInRS232.data;
      RS232RecvState = STATE_RX_WAIT_DATA_TO_CRC;
      SCI_CharLen = 1; //1 byte received
    //}
    break;

    case STATE_RX_WAIT_DATA_TO_CRC:
    //SCI_MsgInRS232.len += 1;
    ++SCI_CharLen;
    *SCI_CharPtr++ = (uint16)incar;
    if (SCI_CharLen >= SCI_MsgInRS232.len+3)
      RS232RecvState = STATE_RX_CKS;
    break;

    case STATE_RX_CKS:
    ComStarted = FALSE;
    len = ++SCI_CharLen;//total msg len including len and crc
    *SCI_CharPtr++ = (uint16)incar;//last crc byte
    cks = SCI1_ComputeCRC16(&SCI_MsgInRS232.len,len);//compute whole msg with crc
    SCI_MsgInRS232.len += 1; //real data length
    if (cks == 0) //(((uint16)SCI_MsgInRS232.data[len]<<8) + SCI_MsgInRS232.data[len+1]))
    { // cks OK
      SCI_MsgAvailable = TRUE;
    }
    else
    { /* BAD cks */
      SCI_MsgInRS232.cmd = CMD_ERROR;
      SCI_MsgAvailable = TRUE;
    }
    RS232RecvState = STATE_RX_IDLE;
    RS232Timer = 0; //stop timer
    break;

    default:
    break;
  }/*switch*/
}/*end RS232_RXI_Int*/
#endif

interrupt void RS232_TEI_Int(void)
{
  SciRegs->SCIFFTX.bit.TXFFINTCLR = 1;//reset scitx int
  AcknowlegeInt |= M_INT9;
}


