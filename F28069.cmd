/*
// TI File $Revision: /main/3 $
// Checkin $Date: March 3, 2011   13:45:53 $
//###########################################################################
//
// FILE:    F28069.cmd
//
// TITLE:   Linker Command File For F28069 Device
//
//###########################################################################
// $TI Release: 2806x C/C++ Header Files V1.10 $ 
// $Release Date: April 7, 2011 $ 
//###########################################################################
*/

/* ======================================================
// For Code Composer Studio V2.2 and later
// ---------------------------------------
// In addition to this memory linker command file,
// add the header linker command file directly to the project.
// The header linker command file is required to link the
// peripheral structures to the proper locations within
// the memory map.
//
// The header linker files are found in <base>\F2806x_headers\cmd
//
// For BIOS applications add:      F2806x_Headers_BIOS.cmd
// For nonBIOS applications add:   F2806x_Headers_nonBIOS.cmd
========================================================= */

/* ======================================================
// For Code Composer Studio prior to V2.2
// --------------------------------------
// 1) Use one of the following -l statements to include the
// header linker command file in the project. The header linker
// file is required to link the peripheral structures to the proper
// locations within the memory map                                    */

/* Uncomment this line to include file only for non-BIOS applications */
/* -l F2806x_Headers_nonBIOS.cmd */

/* Uncomment this line to include file only for BIOS applications */
/* -l F2806x_Headers_BIOS.cmd */

/* 2) In your project add the path to <base>\F2806x_headers\cmd to the
   library search path under project->build options, linker tab,
   library search path (-i).
/*========================================================= */

/* Define the memory block start/length for the F2806x
   PAGE 0 will be used to organize program sections
   PAGE 1 will be used to organize data sections

   Notes:
         Memory blocks on F28069 are uniform (ie same
         physical memory) in both PAGE 0 and PAGE 1.
         That is the same memory region should not be
         defined for both PAGE 0 and PAGE 1.
         Doing so will result in corruption of program
         and/or data.

         Contiguous SARAM memory blocks can be combined
         if required to create a larger memory block.
*/

MEMORY
{
PAGE 0 :   /* Program Memory */
           /* Memory (RAM/FLASH/OTP) blocks can be moved to PAGE1 for data allocation */
   /* FLASH : origin = 0x3e8150, length = 0x9eaf for information set in bios script file */
   /*RAML012     : origin = 0x008000, length = 0x000FFC*/
   RAML3       : origin = 0x009000, length = 0x001000	  /* CLA Program RAM */
   HEADER      : origin = 0x3E8050, length = 0x000100
   SYS_INIT    : origin = 0x3E8000, length = 0x000050
   FLASHEF     : origin = 0x3F2000, length = 0x002E00
   BEGIN       : origin = 0x3F7FF6, length = 0x000002     /* Part of FLASHA.  Used for "boot to Flash" bootloader mode. */
   FPUTABLES   : origin = 0x3FD860, length = 0x0006A0	  /* FPU Tables in Boot ROM */
   IQTABLES    : origin = 0x3FDF00, length = 0x000B50     /* IQ Math Tables in Boot ROM */
   IQTABLES2   : origin = 0x3FEA50, length = 0x00008C     /* IQ Math Tables in Boot ROM */
   IQTABLES3   : origin = 0x3FEADC, length = 0x0000AA	  /* IQ Math Tables in Boot ROM */

   ROM         : origin = 0x3FF3B0, length = 0x000C10     /* Boot ROM */
   RESET       : origin = 0x3FFFC0, length = 0x000002     /* part of boot ROM  */
   VECTORS     : origin = 0x3FFFC2, length = 0x00003E     /* part of boot ROM  */

PAGE 1 :
   /*CLA1_MSGRAMLOW       : origin = 0x001480, length = 0x000080
   CLA1_MSGRAMHIGH      : origin = 0x001500, length = 0x000080*/
   RAMUSB      : origin = 0x00dc00, length = 0x000400
   RAML2END    : origin = 0x008FFC, length = 0x000004
}

/* Allocate sections to memory blocks.
   Note:
         codestart user defined section in DSP28_CodeStartBranch.asm used to redirect code
                   execution when booting to flash
         ramfuncs  user defined section to store functions that will be copied from Flash into RAM
*/


SECTIONS
{
   USBText :
   {
           -l usblib.lib (.text)
           -l driverlib.lib (.text)
           -l usblib.lib (.econst)
           //-l usblib.lib (.cinit)
   }       > FLASHEF, PAGE = 0, TYPE = NOLOAD

   USBRam :
   {
            -l usblib.lib (.ebss)
   }        > RAMUSB PAGE = 1

   /* Allocate program areas: */
   init				   : > SYS_INIT		PAGE = 0, crc_table(_golden_CRC_values, algorithm=CRC32_PRIME)
   //codestart           : > BEGIN,       PAGE = 0
   codestart           : > FLASH,       PAGE = 0
   ramfuncs            : LOAD = FLASH,
                         RUN = L03DPSARAM,
                         LOAD_START(_RamfuncsLoadStart),
                         LOAD_END(_RamfuncsLoadEnd),
                         RUN_START(_RamfuncsRunStart),
                         PAGE = 0,
                         crc_table(_golden_CRC_values, algorithm=CRC32_PRIME)
	HeaderActiv        : > HEADER       PAGE = 0
	
	.bios			   : START(_bios_loadstart),
   						 END(_bios_loadend),
   						 RUN_START(_bios_runstart),
						 LOAD = FLASH 	PAGE = 0,
						 RUN  = L4SARAM 	PAGE = 0,
						 crc_table(_golden_CRC_values, algorithm=CRC32_PRIME)

   .sysinit			   : START(_sysinit_loadstart),
   						 END(_sysinit_loadend),
   						 RUN_START(_sysinit_runstart),
						 LOAD = FLASH 	PAGE = 0,
						 RUN  = L4SARAM 	PAGE = 0,
						 crc_table(_golden_CRC_values, algorithm=CRC32_PRIME)

   .text			   : > FLASH, PAGE = 0, crc_table(_golden_CRC_values, algorithm=CRC32_PRIME)
   .cinit			   : > FLASH, PAGE = 0, crc_table(_golden_CRC_values, algorithm=CRC32_PRIME)
   .econst			 : > FLASH, PAGE = 0, crc_table(_golden_CRC_values, algorithm=CRC32_PRIME)
   .ebss        : > L03DPSARAM, PAGE = 0

	MeasureData		   : > RAML3	    PAGE = 0
	BootCommand      : > RAML2END       PAGE = 1
	CRC32_TABLE      : > L4SARAM,       PAGE = 0
	.TI.crctab		   : > FLASH,       PAGE = 0

	/*CLAmathTables      : > RAML2,     	PAGE = 1
	.bss_cla		   : > RAML2,   	PAGE = 1
    .const_cla	       : > RAML2,   	PAGE = 1*/

  /*  CLAmathTables      : LOAD = FLASHEF,    PAGE = 0,
                         RUN = RAML2,	  PAGE = 1,
                         LOAD_START(_Cla1mathLoadStart),
                         LOAD_END(_Cla1mathLoadEnd),
                         RUN_START(_Cla1mathRunStart)

    Cla1Prog           : LOAD = FLASHEF,
                         RUN = RAML3,
                         LOAD_START(_Cla1funcsLoadStart),
                         LOAD_END(_Cla1funcsLoadEnd),
                         RUN_START(_Cla1funcsRunStart),
                         PAGE = 0

    Cla1ToCpuMsgRAM    : > CLA1_MSGRAMLOW,   PAGE = 1
    CpuToCla1MsgRAM    : > CLA1_MSGRAMHIGH,  PAGE = 1*/

   /* Allocate IQ math areas: */
   IQmath              : > FLASH,     PAGE = 0            /* Math Code */
   IQmathTables        : > IQTABLES,  PAGE = 0, TYPE = NOLOAD
   
   /* Allocate FPU math areas: */
   FPUmathTables       : > FPUTABLES, PAGE = 0, TYPE = NOLOAD
   
  
   /* Uncomment the section below if calling the IQNexp() or IQexp()
      functions from the IQMath.lib library in order to utilize the
      relevant IQ Math table in Boot ROM (This saves space and Boot ROM
      is 1 wait-state). If this section is not uncommented, IQmathTables2
      will be loaded into other memory (SARAM, Flash, etc.) and will take
      up space, but 0 wait-state is possible.
   */
   /*
   IQmathTables2    : > IQTABLES2, PAGE = 0, TYPE = NOLOAD
   {

              IQmath.lib<IQNexpTable.obj> (IQmathTablesRam)

   }
   */
    /* Uncomment the section below if calling the IQNasin() or IQasin()
       functions from the IQMath.lib library in order to utilize the
       relevant IQ Math table in Boot ROM (This saves space and Boot ROM
       is 1 wait-state). If this section is not uncommented, IQmathTables2
       will be loaded into other memory (SARAM, Flash, etc.) and will take
       up space, but 0 wait-state is possible.
    */

    /*IQmathTables3    : > IQTABLES3, PAGE = 0, TYPE = NOLOAD
    {

               IQmath_fpu32.lib<IQNasinTable.obj> (IQmathTablesRam)

    }*/


   /* .reset is a standard section used by the compiler.  It contains the */
   /* the address of the start of _c_int00 for C Code.   /*
   /* When using the boot ROM this section and the CPU vector */
   /* table is not needed.  Thus the default type is set here to  */
   /* DSECT  */
 
}

/*
//===========================================================================
// End of file.
//===========================================================================
*/

