/*
 * buffer.h
 *
 *  Created on: 05.09.2014
 *      Author: oliviers
 */

#ifndef BUFFER_H_
#define BUFFER_H_

void BUF_Init(int channel);
uint16 BUF_Count(int channel);
void BUF_Write(int channel,uint16 val);
uint32 BUF_Sum(int channel);
uint16 BUF_Mean(int channel);
uint32 BUF_SumLast(int channel,int nb,Bool filter);
int32 BUF_Delta(int channel,int nb);


#endif /* BUFFER_H_ */
