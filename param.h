/**********************************************************************

   param.h - parameters

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 27.09.12
   For   : Zimmer Surgical
   Compil: TI CCS5.x
   Target: PTM059
   descr.: parameters
   Edit  :

 *********************************************************************/

#ifndef _H_PARAM_
#define _H_PARAM_

#include <co_data.h>

typedef struct {
  void* pdata;    //Object dictionary subindex
  UNS16 size;     //data byte size
  UNS16 address;  //eeprom address
}T_StatIndexes;

typedef struct {
  UNS16 size;     //size of page
  UNS16 address;  //start address of page
  UNS16 nb;       //nb of T_StatIndexes
  const T_StatIndexes* indexes;
}T_StatPages;

typedef struct {
  UNS64 date;
  UNS32 error;
}T_StatLog;

#define PAR_STAT_PAGE1 0
#define PAR_STAT_PAGE2 1
#define PAR_STAT_PAGE3 2
#define PAR_STAT_PAGE4 3

#define PAR_STAT_PAGE  5
extern const T_StatPages PAR_EEPROM_INDEXES[PAR_STAT_PAGE];

#define EEPROM_COMMAND_NONE         0
#define EEPROM_COMMAND_PENDING      1
#define EEPROM_COMMAND_TERMINATED   2

extern uint16 PAR_StatEepromCommandState[PAR_STAT_PAGE];


bool1  PAR_SetDefaultParameters(CO_Data* d);
bool1  PAR_InitParam(CO_Data* d);
uint32 PAR_ReadPermanentParam(CO_Data* d, T_EepromIndexes indexes);
bool1  PAR_ReadAllPermanentParam(CO_Data* d);
bool1  PAR_FindODPermanentParamIndex(CO_Data* d, uint16 index, uint8 subindex, uint16 *array_index);
uint32 PAR_WriteAllPermanentParam(CO_Data* d);
bool1  PAR_FindODPermanentParamIndex2(CO_Data* d, uint16 address,uint16 *array_index);
bool1  PAR_GetEepromIndexes(CO_Data* d,uint16 array_index,T_EepromIndexes *indexes);
uint32 PAR_StoreODSubIndex(CO_Data* d, uint16 wIndex, uint8 bSubindex);
void   PAR_StoreParamTask(void);
bool1  PAR_WriteAllCollePermanentParam(CO_Data* d);
bool1  PAR_UpdateCode(bool1 set);
uint8  PAR_TestEeprom(void);
uint32 PAR_WriteStatisticParam(int16 index);
bool1  PAR_ReadAllStatisticParam(void);
void   PAR_AddLog(void);
void   PAR_GetLogNB(uint8 nb);

void   PAR_SetParamDependantVars(void);
/* Update all variables that depends on the system parameters (including autocalc
   parameters and CRC). Uses the T_SysParam struct pointed by PAR_SysParam */

#endif /* !_H_PARAM_ */
/***********************************************************/
/* END OF PARAM.H */
