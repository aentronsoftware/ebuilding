;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue Feb 02 11:43:31 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\R_Module_48V_Standard\PTM078_7s")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("SetAlarm")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_SetAlarm")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$66)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$13)
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$124)
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$15)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$15)
	.dwendtag $C$DW$1


$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$120)
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$121)
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$34)
	.dwendtag $C$DW$7


$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("DelAlarm")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_DelAlarm")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$8)
	.dwendtag $C$DW$11


$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("send_consise_dcf")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_send_consise_dcf")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$66)
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$13


$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("RegisterSetODentryCallBack")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_RegisterSetODentryCallBack")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$66)
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$9)
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$6)
$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$86)
	.dwendtag $C$DW$16

$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("usb_tx_mbox")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_usb_tx_mbox")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1574412 
	.sect	".text"
	.global	_getNodeState

$C$DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("getNodeState")
	.dwattr $C$DW$23, DW_AT_low_pc(_getNodeState)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_getNodeState")
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$23, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0x37)
	.dwattr $C$DW$23, DW_AT_TI_begin_column(0x0d)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 56,column 1,is_stmt,address _getNodeState

	.dwfde $C$DW$CIE, _getNodeState
$C$DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg12]
$C$DW$25	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nodeId")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _getNodeState                 FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_getNodeState:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -2]
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("nodeId")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -3]
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("networkNodeState")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_networkNodeState")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[3],AL            ; [CPU_] |56| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |56| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 57,column 32,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |57| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |57| 
        ADDU      ACC,AR6               ; [CPU_] |57| 
        ADDB      ACC,#108              ; [CPU_] |57| 
        MOVL      XAR7,ACC              ; [CPU_] |57| 
        MOV       AL,*XAR7              ; [CPU_] |57| 
        MOV       *-SP[4],AL            ; [CPU_] |57| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 58,column 3,is_stmt
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 59,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$23, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0x3b)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$23

	.sect	".text"
	.global	_ConsumerHearbeatAlarm

$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("ConsumerHearbeatAlarm")
	.dwattr $C$DW$30, DW_AT_low_pc(_ConsumerHearbeatAlarm)
	.dwattr $C$DW$30, DW_AT_high_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_ConsumerHearbeatAlarm")
	.dwattr $C$DW$30, DW_AT_external
	.dwattr $C$DW$30, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$30, DW_AT_TI_begin_line(0x43)
	.dwattr $C$DW$30, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$30, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 68,column 1,is_stmt,address _ConsumerHearbeatAlarm

	.dwfde $C$DW$CIE, _ConsumerHearbeatAlarm
$C$DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg12]
$C$DW$32	.dwtag  DW_TAG_formal_parameter, DW_AT_name("id")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_id")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _ConsumerHearbeatAlarm        FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_ConsumerHearbeatAlarm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -2]
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("id")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_id")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],ACC           ; [CPU_] |68| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |68| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 73,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |73| 
        MOVB      XAR0,#100             ; [CPU_] |73| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |73| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |73| 
        MOVL      XAR4,ACC              ; [CPU_] |73| 
        MOV       *+XAR4[0],#-1         ; [CPU_] |73| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 75,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |75| 
        MOVB      XAR0,#98              ; [CPU_] |75| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |75| 
        MOVL      XAR5,*+XAR4[AR0]      ; [CPU_] |75| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |75| 
        LSL       ACC,1                 ; [CPU_] |75| 
        MOVB      XAR0,#106             ; [CPU_] |75| 
        ADDL      XAR5,ACC              ; [CPU_] |75| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |75| 
        MOVL      ACC,*+XAR5[0]         ; [CPU_] |75| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |75| 
        ANDB      AH,#255               ; [CPU_] |75| 
        MOVU      ACC,AH                ; [CPU_] |75| 
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_call
	.dwattr $C$DW$35, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |75| 
        ; call occurs [XAR7] ; [] |75| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 76,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$30, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$30, DW_AT_TI_end_line(0x4c)
	.dwattr $C$DW$30, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$30

	.sect	".text"
	.global	_proceedNODE_GUARD

$C$DW$37	.dwtag  DW_TAG_subprogram, DW_AT_name("proceedNODE_GUARD")
	.dwattr $C$DW$37, DW_AT_low_pc(_proceedNODE_GUARD)
	.dwattr $C$DW$37, DW_AT_high_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_proceedNODE_GUARD")
	.dwattr $C$DW$37, DW_AT_external
	.dwattr $C$DW$37, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$37, DW_AT_TI_begin_line(0x54)
	.dwattr $C$DW$37, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$37, DW_AT_TI_max_frame_size(-28)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 85,column 1,is_stmt,address _proceedNODE_GUARD

	.dwfde $C$DW$CIE, _proceedNODE_GUARD
$C$DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg12]
$C$DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_name("m")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _proceedNODE_GUARD            FR SIZE:  26           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            6 Parameter, 17 Auto,  2 SOE     *
;***************************************************************

_proceedNODE_GUARD:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR1            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 7, 2
	.dwcfi	cfa_offset, -4
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -28
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_breg20 -8]
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_breg20 -10]
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("nodeId")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -11]
        MOVL      *-SP[10],XAR5         ; [CPU_] |85| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |85| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 86,column 15,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |86| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |86| 
        ANDB      AL,#0x7f              ; [CPU_] |86| 
        MOV       *-SP[11],AL           ; [CPU_] |86| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 88,column 3,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |88| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |88| 
        CMPB      AL,#1                 ; [CPU_] |88| 
        BF        $C$L4,NEQ             ; [CPU_] |88| 
        ; branchcc occurs ; [] |88| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 100,column 7,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |100| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |100| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |100| 
        CMP       AL,*-SP[11]           ; [CPU_] |100| 
        BF        $C$L9,NEQ             ; [CPU_] |100| 
        ; branchcc occurs ; [] |100| 

$C$DW$43	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_breg20 -22]
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("tmp")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_tmp")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_breg20 -23]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 103,column 21,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |103| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |103| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |103| 
        ADD       AL,#1792              ; [CPU_] |103| 
        MOV       *-SP[23],AL           ; [CPU_] |103| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 104,column 11,is_stmt
        MOV       *-SP[22],AL           ; [CPU_] |104| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 105,column 11,is_stmt
        MOVB      *-SP[20],#1,UNC       ; [CPU_] |105| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 106,column 11,is_stmt
        MOV       *-SP[21],#0           ; [CPU_] |106| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 107,column 11,is_stmt
        MOVL      XAR7,*-SP[8]          ; [CPU_] |107| 
        ADDB      XAR7,#76              ; [CPU_] |107| 
        MOV       AL,*XAR7              ; [CPU_] |107| 
        MOV       *-SP[19],AL           ; [CPU_] |107| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 108,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |108| 
        MOVB      XAR0,#248             ; [CPU_] |108| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |108| 
        BF        $C$L1,EQ              ; [CPU_] |108| 
        ; branchcc occurs ; [] |108| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 110,column 15,is_stmt
        OR        *-SP[19],#0x0080      ; [CPU_] |110| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 111,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |111| 
        MOVB      XAR0,#248             ; [CPU_] |111| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |111| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 112,column 13,is_stmt
        B         $C$L2,UNC             ; [CPU_] |112| 
        ; branch occurs ; [] |112| 
$C$L1:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 114,column 13,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |114| 
        MOVB      XAR0,#248             ; [CPU_] |114| 
        MOVB      *+XAR4[AR0],#1,UNC    ; [CPU_] |114| 
$C$L2:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 117,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |117| 
        MOVB      XAR0,#249             ; [CPU_] |117| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |117| 
        BF        $C$L3,NEQ             ; [CPU_] |117| 
        ; branchcc occurs ; [] |117| 
        MOVZ      AR5,SP                ; [CPU_U] |117| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |117| 
        MOVB      AL,#0                 ; [CPU_] |117| 
        SUBB      XAR5,#22              ; [CPU_U] |117| 
$C$DW$46	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$46, DW_AT_low_pc(0x00)
	.dwattr $C$DW$46, DW_AT_name("_MBX_post")
	.dwattr $C$DW$46, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |117| 
        ; call occurs [#_MBX_post] ; [] |117| 
        B         $C$L9,UNC             ; [CPU_] |117| 
        ; branch occurs ; [] |117| 
$C$L3:    
        MOVZ      AR5,SP                ; [CPU_U] |117| 
        MOVL      XAR4,#_usb_tx_mbox    ; [CPU_U] |117| 
        MOVB      AL,#0                 ; [CPU_] |117| 
        SUBB      XAR5,#22              ; [CPU_U] |117| 
$C$DW$47	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$47, DW_AT_low_pc(0x00)
	.dwattr $C$DW$47, DW_AT_name("_MBX_post")
	.dwattr $C$DW$47, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |117| 
        ; call occurs [#_MBX_post] ; [] |117| 
	.dwendtag $C$DW$43

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 120,column 5,is_stmt
        B         $C$L9,UNC             ; [CPU_] |120| 
        ; branch occurs ; [] |120| 
$C$L4:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 125,column 7,is_stmt
        MOVZ      AR6,*-SP[11]          ; [CPU_] |125| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |125| 
        MOVL      XAR5,*-SP[10]         ; [CPU_] |125| 
        MOVB      XAR0,#108             ; [CPU_] |125| 
        ADDU      ACC,AR6               ; [CPU_] |125| 
        MOVL      XAR4,ACC              ; [CPU_] |125| 
        MOV       AL,*+XAR5[3]          ; [CPU_] |125| 
        ANDB      AL,#0x7f              ; [CPU_] |125| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |125| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 128,column 7,is_stmt
        MOVZ      AR6,*-SP[11]          ; [CPU_] |128| 
        MOVB      XAR0,#108             ; [CPU_] |128| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |128| 
        ADDU      ACC,AR6               ; [CPU_] |128| 
        MOVL      XAR4,ACC              ; [CPU_] |128| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |128| 
        BF        $C$L5,NEQ             ; [CPU_] |128| 
        ; branchcc occurs ; [] |128| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 141,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |141| 
        MOV       AL,*-SP[11]           ; [CPU_] |141| 
$C$DW$48	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$48, DW_AT_low_pc(0x00)
	.dwattr $C$DW$48, DW_AT_name("_send_consise_dcf")
	.dwattr $C$DW$48, DW_AT_TI_call
        LCR       #_send_consise_dcf    ; [CPU_] |141| 
        ; call occurs [#_send_consise_dcf] ; [] |141| 
        CMPB      AL,#0                 ; [CPU_] |141| 
        BF        $C$L5,NEQ             ; [CPU_] |141| 
        ; branchcc occurs ; [] |141| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 143,column 6,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |143| 
        MOVB      XAR0,#246             ; [CPU_] |143| 
        MOV       AL,*-SP[11]           ; [CPU_] |143| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |143| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |143| 
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_TI_call
	.dwattr $C$DW$49, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |143| 
        ; call occurs [XAR7] ; [] |143| 
$C$L5:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 147,column 7,is_stmt
        MOVZ      AR6,*-SP[11]          ; [CPU_] |147| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |147| 
        MOVB      XAR0,#108             ; [CPU_] |147| 
        ADDU      ACC,AR6               ; [CPU_] |147| 
        MOVL      XAR4,ACC              ; [CPU_] |147| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |147| 
        CMPB      AL,#15                ; [CPU_] |147| 
        BF        $C$L9,EQ              ; [CPU_] |147| 
        ; branchcc occurs ; [] |147| 

$C$DW$50	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -12]
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("ConsummerHeartBeat_nodeId")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_ConsummerHeartBeat_nodeId")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg20 -13]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 149,column 14,is_stmt
        MOV       *-SP[12],#0           ; [CPU_] |149| 
        B         $C$L8,UNC             ; [CPU_] |149| 
        ; branch occurs ; [] |149| 
$C$L6:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 151,column 13,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |151| 
        MOVB      XAR0,#98              ; [CPU_] |151| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |151| 
        MOVU      ACC,*-SP[12]          ; [CPU_] |151| 
        LSL       ACC,1                 ; [CPU_] |151| 
        ADDL      XAR4,ACC              ; [CPU_] |151| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |151| 
        ANDB      AH,#255               ; [CPU_] |151| 
        MOVU      ACC,AH                ; [CPU_] |151| 
        MOV       *-SP[13],AL           ; [CPU_] |151| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 152,column 13,is_stmt
        CMP       AL,*-SP[11]           ; [CPU_] |152| 
        BF        $C$L7,NEQ             ; [CPU_] |152| 
        ; branchcc occurs ; [] |152| 

$C$DW$53	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("time")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_time")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_breg20 -18]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 154,column 30,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |154| 
        MOVB      XAR0,#98              ; [CPU_] |154| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |154| 
        MOVU      ACC,*-SP[12]          ; [CPU_] |154| 
        LSL       ACC,1                 ; [CPU_] |154| 
        ADDL      XAR4,ACC              ; [CPU_] |154| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |154| 
        ANDB      AH,#0                 ; [CPU_] |154| 
        MOVL      *-SP[18],ACC          ; [CPU_] |154| 
        MOV       *-SP[16],#0           ; [CPU_] |154| 
        MOV       *-SP[15],#0           ; [CPU_] |154| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 156,column 17,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |156| 
        MOVZ      AR0,*-SP[12]          ; [CPU_] |156| 
        MOVB      XAR1,#100             ; [CPU_] |156| 
        MOVL      XAR4,*+XAR4[AR1]      ; [CPU_] |156| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |156| 
$C$DW$55	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$55, DW_AT_low_pc(0x00)
	.dwattr $C$DW$55, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$55, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |156| 
        ; call occurs [#_DelAlarm] ; [] |156| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 157,column 17,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |157| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |157| 
        MOV       *-SP[2],#0            ; [CPU_] |157| 
        MOV       *-SP[1],#0            ; [CPU_] |157| 
        MOVL      P,*-SP[18]            ; [CPU_] |157| 
        MOVL      ACC,*-SP[16]          ; [CPU_] |157| 
$C$DW$56	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$56, DW_AT_low_pc(0x00)
	.dwattr $C$DW$56, DW_AT_name("LL$$MPY")
	.dwattr $C$DW$56, DW_AT_TI_call
        FFC       XAR7,#LL$$MPY         ; [CPU_] |157| 
        ; call occurs [#LL$$MPY] ; [] |157| 
        MOVZ      AR7,*-SP[12]          ; [CPU_] |157| 
        MOVB      XAR6,#0               ; [CPU_] |157| 
        MOVL      *-SP[2],XAR7          ; [CPU_] |157| 
        MOVL      *-SP[6],XAR6          ; [CPU_] |157| 
        MOV       *-SP[4],#0            ; [CPU_] |157| 
        MOV       *-SP[3],#0            ; [CPU_] |157| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |157| 
        MOVL      XAR5,#_ConsumerHearbeatAlarm ; [CPU_U] |157| 
$C$DW$57	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$57, DW_AT_low_pc(0x00)
	.dwattr $C$DW$57, DW_AT_name("_SetAlarm")
	.dwattr $C$DW$57, DW_AT_TI_call
        LCR       #_SetAlarm            ; [CPU_] |157| 
        ; call occurs [#_SetAlarm] ; [] |157| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |157| 
        MOVZ      AR0,*-SP[12]          ; [CPU_] |157| 
        MOVB      XAR1,#100             ; [CPU_] |157| 
        MOVL      XAR4,*+XAR4[AR1]      ; [CPU_] |157| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |157| 
	.dwendtag $C$DW$53

$C$L7:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 149,column 70,is_stmt
        INC       *-SP[12]              ; [CPU_] |149| 
$C$L8:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 149,column 34,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |149| 
        MOVB      XAR0,#96              ; [CPU_] |149| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |149| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |149| 
        CMP       AL,*-SP[12]           ; [CPU_] |149| 
        B         $C$L6,HI              ; [CPU_] |149| 
        ; branchcc occurs ; [] |149| 
	.dwendtag $C$DW$50

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 162,column 1,is_stmt
$C$L9:    
        SUBB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR1,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 7
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$37, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$37, DW_AT_TI_end_line(0xa2)
	.dwattr $C$DW$37, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$37

	.sect	".text"
	.global	_ProducerHearbeatAlarm

$C$DW$59	.dwtag  DW_TAG_subprogram, DW_AT_name("ProducerHearbeatAlarm")
	.dwattr $C$DW$59, DW_AT_low_pc(_ProducerHearbeatAlarm)
	.dwattr $C$DW$59, DW_AT_high_pc(0x00)
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_ProducerHearbeatAlarm")
	.dwattr $C$DW$59, DW_AT_external
	.dwattr $C$DW$59, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$59, DW_AT_TI_begin_line(0xaa)
	.dwattr $C$DW$59, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$59, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 171,column 1,is_stmt,address _ProducerHearbeatAlarm

	.dwfde $C$DW$CIE, _ProducerHearbeatAlarm
$C$DW$60	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg12]
$C$DW$61	.dwtag  DW_TAG_formal_parameter, DW_AT_name("id")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_id")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _ProducerHearbeatAlarm        FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 16 Auto,  0 SOE     *
;***************************************************************

_ProducerHearbeatAlarm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_breg20 -2]
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("id")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_id")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],ACC           ; [CPU_] |171| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |171| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 172,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |172| 
        MOVB      XAR0,#102             ; [CPU_] |172| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |172| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |172| 
        BF        $C$L11,EQ             ; [CPU_] |172| 
        ; branchcc occurs ; [] |172| 

$C$DW$64	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_breg20 -15]
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("tmp")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_tmp")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_breg20 -16]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 180,column 17,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |180| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |180| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |180| 
        ADD       AL,#1792              ; [CPU_] |180| 
        MOV       *-SP[16],AL           ; [CPU_] |180| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 181,column 7,is_stmt
        MOV       *-SP[15],AL           ; [CPU_] |181| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 182,column 7,is_stmt
        MOVB      *-SP[13],#1,UNC       ; [CPU_] |182| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 183,column 7,is_stmt
        MOV       *-SP[14],#0           ; [CPU_] |183| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 184,column 7,is_stmt
        MOVL      XAR7,*-SP[2]          ; [CPU_] |184| 
        ADDB      XAR7,#76              ; [CPU_] |184| 
        MOV       AL,*XAR7              ; [CPU_] |184| 
        MOV       *-SP[12],AL           ; [CPU_] |184| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 187,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |187| 
        MOVB      XAR0,#249             ; [CPU_] |187| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |187| 
        BF        $C$L10,NEQ            ; [CPU_] |187| 
        ; branchcc occurs ; [] |187| 
        MOVZ      AR5,SP                ; [CPU_U] |187| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |187| 
        MOVB      AL,#0                 ; [CPU_] |187| 
        SUBB      XAR5,#15              ; [CPU_U] |187| 
$C$DW$67	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$67, DW_AT_low_pc(0x00)
	.dwattr $C$DW$67, DW_AT_name("_MBX_post")
	.dwattr $C$DW$67, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |187| 
        ; call occurs [#_MBX_post] ; [] |187| 
        B         $C$L12,UNC            ; [CPU_] |187| 
        ; branch occurs ; [] |187| 
$C$L10:    
        MOVZ      AR5,SP                ; [CPU_U] |187| 
        MOVL      XAR4,#_usb_tx_mbox    ; [CPU_U] |187| 
        MOVB      AL,#0                 ; [CPU_] |187| 
        SUBB      XAR5,#15              ; [CPU_U] |187| 
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_name("_MBX_post")
	.dwattr $C$DW$68, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |187| 
        ; call occurs [#_MBX_post] ; [] |187| 
	.dwendtag $C$DW$64

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 189,column 5,is_stmt
        B         $C$L12,UNC            ; [CPU_] |189| 
        ; branch occurs ; [] |189| 
$C$L11:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 190,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |190| 
        MOVB      XAR0,#104             ; [CPU_] |190| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |190| 
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$69, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |190| 
        ; call occurs [#_DelAlarm] ; [] |190| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |190| 
        MOVB      XAR0,#104             ; [CPU_] |190| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |190| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 192,column 1,is_stmt
$C$L12:    
        SUBB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$70	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$70, DW_AT_low_pc(0x00)
	.dwattr $C$DW$70, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$59, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$59, DW_AT_TI_end_line(0xc0)
	.dwattr $C$DW$59, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$59

	.sect	".text"
	.global	_OnHeartbeatProducerUpdate

$C$DW$71	.dwtag  DW_TAG_subprogram, DW_AT_name("OnHeartbeatProducerUpdate")
	.dwattr $C$DW$71, DW_AT_low_pc(_OnHeartbeatProducerUpdate)
	.dwattr $C$DW$71, DW_AT_high_pc(0x00)
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_OnHeartbeatProducerUpdate")
	.dwattr $C$DW$71, DW_AT_external
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$71, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$71, DW_AT_TI_begin_line(0xcb)
	.dwattr $C$DW$71, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$71, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 204,column 1,is_stmt,address _OnHeartbeatProducerUpdate

	.dwfde $C$DW$CIE, _OnHeartbeatProducerUpdate
$C$DW$72	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg12]
$C$DW$73	.dwtag  DW_TAG_formal_parameter, DW_AT_name("unsused_indextable")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_unsused_indextable")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg14]
$C$DW$74	.dwtag  DW_TAG_formal_parameter, DW_AT_name("unsused_bSubindex")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_unsused_bSubindex")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg0]
$C$DW$75	.dwtag  DW_TAG_formal_parameter, DW_AT_name("unused_access")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_unused_access")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _OnHeartbeatProducerUpdate    FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_OnHeartbeatProducerUpdate:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_breg20 -2]
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("unsused_indextable")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_unsused_indextable")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$77, DW_AT_location[DW_OP_breg20 -4]
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("unsused_bSubindex")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_unsused_bSubindex")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_breg20 -5]
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("unused_access")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_unused_access")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |204| 
        MOV       *-SP[5],AL            ; [CPU_] |204| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |204| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |204| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 205,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |205| 
$C$DW$80	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$80, DW_AT_low_pc(0x00)
	.dwattr $C$DW$80, DW_AT_name("_heartbeatStop")
	.dwattr $C$DW$80, DW_AT_TI_call
        LCR       #_heartbeatStop       ; [CPU_] |205| 
        ; call occurs [#_heartbeatStop] ; [] |205| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 206,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |206| 
$C$DW$81	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$81, DW_AT_low_pc(0x00)
	.dwattr $C$DW$81, DW_AT_name("_heartbeatInit")
	.dwattr $C$DW$81, DW_AT_TI_call
        LCR       #_heartbeatInit       ; [CPU_] |206| 
        ; call occurs [#_heartbeatInit] ; [] |206| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 207,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |207| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 208,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$82	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$82, DW_AT_low_pc(0x00)
	.dwattr $C$DW$82, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$71, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$71, DW_AT_TI_end_line(0xd0)
	.dwattr $C$DW$71, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$71

	.sect	".text"
	.global	_heartbeatInit

$C$DW$83	.dwtag  DW_TAG_subprogram, DW_AT_name("heartbeatInit")
	.dwattr $C$DW$83, DW_AT_low_pc(_heartbeatInit)
	.dwattr $C$DW$83, DW_AT_high_pc(0x00)
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_heartbeatInit")
	.dwattr $C$DW$83, DW_AT_external
	.dwattr $C$DW$83, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$83, DW_AT_TI_begin_line(0xd7)
	.dwattr $C$DW$83, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$83, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 216,column 1,is_stmt,address _heartbeatInit

	.dwfde $C$DW$CIE, _heartbeatInit
$C$DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _heartbeatInit                FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            6 Parameter,  8 Auto,  2 SOE     *
;***************************************************************

_heartbeatInit:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR1            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 7, 2
	.dwcfi	cfa_offset, -4
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -8]
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -9]
        MOVL      *-SP[8],XAR4          ; [CPU_] |216| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 219,column 3,is_stmt
        MOVL      XAR5,#_OnHeartbeatProducerUpdate ; [CPU_U] |219| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |219| 
        MOVB      AH,#0                 ; [CPU_] |219| 
        MOV       AL,#4119              ; [CPU_] |219| 
$C$DW$87	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$87, DW_AT_low_pc(0x00)
	.dwattr $C$DW$87, DW_AT_name("_RegisterSetODentryCallBack")
	.dwattr $C$DW$87, DW_AT_TI_call
        LCR       #_RegisterSetODentryCallBack ; [CPU_] |219| 
        ; call occurs [#_RegisterSetODentryCallBack] ; [] |219| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 221,column 3,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |221| 
        MOVB      XAR0,#248             ; [CPU_] |221| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |221| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 223,column 8,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |223| 
        B         $C$L15,UNC            ; [CPU_] |223| 
        ; branch occurs ; [] |223| 
$C$L13:    

$C$DW$88	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("time")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_time")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -14]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 225,column 20,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |225| 
        MOVB      XAR0,#98              ; [CPU_] |225| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |225| 
        MOVU      ACC,*-SP[9]           ; [CPU_] |225| 
        LSL       ACC,1                 ; [CPU_] |225| 
        ADDL      XAR4,ACC              ; [CPU_] |225| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |225| 
        MOVL      *-SP[14],ACC          ; [CPU_] |225| 
        MOV       *-SP[12],#0           ; [CPU_] |225| 
        MOV       *-SP[11],#0           ; [CPU_] |225| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 227,column 7,is_stmt
        CMP64     ACC:P                 ; [CPU_] |227| 
        MOVL      P,*-SP[14]            ; [CPU_] |227| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |227| 
        CMP64     ACC:P                 ; [CPU_] |227| 
        BF        $C$L14,EQ             ; [CPU_] |227| 
        ; branchcc occurs ; [] |227| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 229,column 11,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |229| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |229| 
        MOV       *-SP[2],#0            ; [CPU_] |229| 
        MOV       *-SP[1],#0            ; [CPU_] |229| 
        MOVL      P,*-SP[14]            ; [CPU_] |229| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |229| 
$C$DW$90	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$90, DW_AT_low_pc(0x00)
	.dwattr $C$DW$90, DW_AT_name("LL$$MPY")
	.dwattr $C$DW$90, DW_AT_TI_call
        FFC       XAR7,#LL$$MPY         ; [CPU_] |229| 
        ; call occurs [#LL$$MPY] ; [] |229| 
        MOVZ      AR7,*-SP[9]           ; [CPU_] |229| 
        MOVB      XAR6,#0               ; [CPU_] |229| 
        MOVL      *-SP[2],XAR7          ; [CPU_] |229| 
        MOVL      *-SP[6],XAR6          ; [CPU_] |229| 
        MOV       *-SP[4],#0            ; [CPU_] |229| 
        MOV       *-SP[3],#0            ; [CPU_] |229| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |229| 
        MOVL      XAR5,#_ConsumerHearbeatAlarm ; [CPU_U] |229| 
$C$DW$91	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$91, DW_AT_low_pc(0x00)
	.dwattr $C$DW$91, DW_AT_name("_SetAlarm")
	.dwattr $C$DW$91, DW_AT_TI_call
        LCR       #_SetAlarm            ; [CPU_] |229| 
        ; call occurs [#_SetAlarm] ; [] |229| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |229| 
        MOVZ      AR0,*-SP[9]           ; [CPU_] |229| 
        MOVB      XAR1,#100             ; [CPU_] |229| 
        MOVL      XAR4,*+XAR4[AR1]      ; [CPU_] |229| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |229| 
$C$L14:    
	.dwendtag $C$DW$88

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 223,column 64,is_stmt
        INC       *-SP[9]               ; [CPU_] |223| 
$C$L15:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 223,column 28,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |223| 
        MOVB      XAR0,#96              ; [CPU_] |223| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |223| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |223| 
        CMP       AL,*-SP[9]            ; [CPU_] |223| 
        B         $C$L13,HI             ; [CPU_] |223| 
        ; branchcc occurs ; [] |223| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 233,column 3,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |233| 
        MOVB      XAR0,#102             ; [CPU_] |233| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |233| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |233| 
        BF        $C$L16,EQ             ; [CPU_] |233| 
        ; branchcc occurs ; [] |233| 

$C$DW$92	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("time")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_time")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_breg20 -14]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 235,column 20,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |235| 
        MOVB      XAR0,#102             ; [CPU_] |235| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |235| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |235| 
        MOVL      *-SP[14],ACC          ; [CPU_] |235| 
        MOV       *-SP[12],#0           ; [CPU_] |235| 
        MOV       *-SP[11],#0           ; [CPU_] |235| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 236,column 7,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |236| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |236| 
        MOV       *-SP[2],#0            ; [CPU_] |236| 
        MOV       *-SP[1],#0            ; [CPU_] |236| 
        MOVL      P,*-SP[14]            ; [CPU_] |236| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |236| 
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("LL$$MPY")
	.dwattr $C$DW$94, DW_AT_TI_call
        FFC       XAR7,#LL$$MPY         ; [CPU_] |236| 
        ; call occurs [#LL$$MPY] ; [] |236| 
        MOVL      XAR4,#1000            ; [CPU_U] |236| 
        MOVL      XAR6,P                ; [CPU_] |236| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |236| 
        MOV       *-SP[2],#0            ; [CPU_] |236| 
        MOV       *-SP[1],#0            ; [CPU_] |236| 
        MOVL      P,*-SP[14]            ; [CPU_] |236| 
        MOVL      XAR5,ACC              ; [CPU_] |236| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |236| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("LL$$MPY")
	.dwattr $C$DW$95, DW_AT_TI_call
        FFC       XAR7,#LL$$MPY         ; [CPU_] |236| 
        ; call occurs [#LL$$MPY] ; [] |236| 
        MOVB      XAR7,#0               ; [CPU_] |236| 
        MOVL      *-SP[2],XAR7          ; [CPU_] |236| 
        MOVL      *-SP[6],P             ; [CPU_] |236| 
        MOVL      *-SP[4],ACC           ; [CPU_] |236| 
        MOVL      ACC,XAR5              ; [CPU_] |236| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |236| 
        MOVL      P,XAR6                ; [CPU_] |236| 
        MOVL      XAR5,#_ProducerHearbeatAlarm ; [CPU_U] |236| 
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_name("_SetAlarm")
	.dwattr $C$DW$96, DW_AT_TI_call
        LCR       #_SetAlarm            ; [CPU_] |236| 
        ; call occurs [#_SetAlarm] ; [] |236| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |236| 
        MOVB      XAR0,#104             ; [CPU_] |236| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |236| 
	.dwendtag $C$DW$92

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 238,column 1,is_stmt
$C$L16:    
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR1,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 7
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$83, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$83, DW_AT_TI_end_line(0xee)
	.dwattr $C$DW$83, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$83

	.sect	".text"
	.global	_heartbeatStop

$C$DW$98	.dwtag  DW_TAG_subprogram, DW_AT_name("heartbeatStop")
	.dwattr $C$DW$98, DW_AT_low_pc(_heartbeatStop)
	.dwattr $C$DW$98, DW_AT_high_pc(0x00)
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_heartbeatStop")
	.dwattr $C$DW$98, DW_AT_external
	.dwattr $C$DW$98, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$98, DW_AT_TI_begin_line(0xf5)
	.dwattr $C$DW$98, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$98, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 246,column 1,is_stmt,address _heartbeatStop

	.dwfde $C$DW$CIE, _heartbeatStop
$C$DW$99	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _heartbeatStop                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  2 SOE     *
;***************************************************************

_heartbeatStop:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR1            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 7, 2
	.dwcfi	cfa_offset, -4
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_breg20 -2]
$C$DW$101	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_breg20 -3]
        MOVL      *-SP[2],XAR4          ; [CPU_] |246| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 248,column 8,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |248| 
        B         $C$L18,UNC            ; [CPU_] |248| 
        ; branch occurs ; [] |248| 
$C$L17:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 250,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |250| 
        MOVZ      AR0,*-SP[3]           ; [CPU_] |250| 
        MOVB      XAR1,#100             ; [CPU_] |250| 
        MOVL      XAR4,*+XAR4[AR1]      ; [CPU_] |250| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |250| 
$C$DW$102	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$102, DW_AT_low_pc(0x00)
	.dwattr $C$DW$102, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$102, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |250| 
        ; call occurs [#_DelAlarm] ; [] |250| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |250| 
        MOVZ      AR0,*-SP[3]           ; [CPU_] |250| 
        MOVB      XAR1,#100             ; [CPU_] |250| 
        MOVL      XAR4,*+XAR4[AR1]      ; [CPU_] |250| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |250| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 248,column 64,is_stmt
        INC       *-SP[3]               ; [CPU_] |248| 
$C$L18:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 248,column 28,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |248| 
        MOVB      XAR0,#96              ; [CPU_] |248| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |248| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |248| 
        CMP       AL,*-SP[3]            ; [CPU_] |248| 
        B         $C$L17,HI             ; [CPU_] |248| 
        ; branchcc occurs ; [] |248| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 253,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |253| 
        MOVB      XAR0,#104             ; [CPU_] |253| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |253| 
$C$DW$103	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$103, DW_AT_low_pc(0x00)
	.dwattr $C$DW$103, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$103, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |253| 
        ; call occurs [#_DelAlarm] ; [] |253| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |253| 
        MOVB      XAR0,#104             ; [CPU_] |253| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |253| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 254,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR1,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 7
$C$DW$104	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$104, DW_AT_low_pc(0x00)
	.dwattr $C$DW$104, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$98, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$98, DW_AT_TI_end_line(0xfe)
	.dwattr $C$DW$98, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$98

	.sect	".text"
	.global	__heartbeatError

$C$DW$105	.dwtag  DW_TAG_subprogram, DW_AT_name("_heartbeatError")
	.dwattr $C$DW$105, DW_AT_low_pc(__heartbeatError)
	.dwattr $C$DW$105, DW_AT_high_pc(0x00)
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("__heartbeatError")
	.dwattr $C$DW$105, DW_AT_external
	.dwattr $C$DW$105, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$105, DW_AT_TI_begin_line(0x105)
	.dwattr $C$DW$105, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$105, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 261,column 51,is_stmt,address __heartbeatError

	.dwfde $C$DW$CIE, __heartbeatError
$C$DW$106	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_reg12]
$C$DW$107	.dwtag  DW_TAG_formal_parameter, DW_AT_name("heartbeatID")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_heartbeatID")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: __heartbeatError              FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

__heartbeatError:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$108	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_breg20 -2]
$C$DW$109	.dwtag  DW_TAG_variable, DW_AT_name("heartbeatID")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_heartbeatID")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AL            ; [CPU_] |261| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |261| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 261,column 52,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$105, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$105, DW_AT_TI_end_line(0x105)
	.dwattr $C$DW$105, DW_AT_TI_end_column(0x34)
	.dwendentry
	.dwendtag $C$DW$105

	.sect	".text"
	.global	__post_SlaveBootup

$C$DW$111	.dwtag  DW_TAG_subprogram, DW_AT_name("_post_SlaveBootup")
	.dwattr $C$DW$111, DW_AT_low_pc(__post_SlaveBootup)
	.dwattr $C$DW$111, DW_AT_high_pc(0x00)
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("__post_SlaveBootup")
	.dwattr $C$DW$111, DW_AT_external
	.dwattr $C$DW$111, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$111, DW_AT_TI_begin_line(0x106)
	.dwattr $C$DW$111, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$111, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 262,column 49,is_stmt,address __post_SlaveBootup

	.dwfde $C$DW$CIE, __post_SlaveBootup
$C$DW$112	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg12]
$C$DW$113	.dwtag  DW_TAG_formal_parameter, DW_AT_name("SlaveID")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_SlaveID")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: __post_SlaveBootup            FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

__post_SlaveBootup:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$114	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_breg20 -2]
$C$DW$115	.dwtag  DW_TAG_variable, DW_AT_name("SlaveID")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_SlaveID")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AL            ; [CPU_] |262| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |262| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c",line 262,column 50,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$111, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/lifegrd.c")
	.dwattr $C$DW$111, DW_AT_TI_end_line(0x106)
	.dwattr $C$DW$111, DW_AT_TI_end_column(0x32)
	.dwendentry
	.dwendtag $C$DW$111

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_SetAlarm
	.global	_MBX_post
	.global	_DelAlarm
	.global	_send_consise_dcf
	.global	_RegisterSetODentryCallBack
	.global	_usb_tx_mbox
	.global	_can_tx_mbox
	.global	LL$$MPY

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$117, DW_AT_name("cob_id")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$118, DW_AT_name("rtr")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$119, DW_AT_name("len")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$120, DW_AT_name("data")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)
$C$DW$T$116	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$116, DW_AT_address_class(0x16)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$121, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$122, DW_AT_name("csSDO")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$123, DW_AT_name("csEmergency")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$124, DW_AT_name("csSYNC")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$125, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$126, DW_AT_name("csPDO")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$127, DW_AT_name("csLSS")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$128, DW_AT_name("errCode")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$129, DW_AT_name("errRegMask")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$130, DW_AT_name("active")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)

$C$DW$T$99	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x18)
$C$DW$131	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$131, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$99


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$132, DW_AT_name("index")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$133, DW_AT_name("subindex")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$134, DW_AT_name("size")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$135, DW_AT_name("address")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)

$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x08)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$136, DW_AT_name("wListElem")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$137, DW_AT_name("wCount")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$138, DW_AT_name("fxn")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31

$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$26	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$26, DW_AT_address_class(0x16)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)

$C$DW$T$38	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$38, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x30)
$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$139, DW_AT_name("dataQue")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$140, DW_AT_name("freeQue")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$141, DW_AT_name("dataSem")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$142, DW_AT_name("freeSem")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$143, DW_AT_name("segid")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$144, DW_AT_name("size")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$145, DW_AT_name("length")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$146, DW_AT_name("name")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38

$C$DW$T$117	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$T$119	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$119, DW_AT_address_class(0x16)
$C$DW$T$120	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$120, DW_AT_language(DW_LANG_C)

$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x04)
$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$147, DW_AT_name("next")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$148, DW_AT_name("prev")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x16)

$C$DW$T$42	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$42, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x10)
$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$149, DW_AT_name("job")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$150, DW_AT_name("count")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$151, DW_AT_name("pendQ")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$152, DW_AT_name("name")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$121	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$121, DW_AT_language(DW_LANG_C)

$C$DW$T$28	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$153	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$27)
	.dwendtag $C$DW$T$28

$C$DW$T$29	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_address_class(0x16)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)

$C$DW$T$67	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$154	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$T$67

$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x16)
$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$T$81	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("post_sync_t")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)

$C$DW$T$76	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$155	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$66)
$C$DW$156	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$76

$C$DW$T$77	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x16)
$C$DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$T$82	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)

$C$DW$T$100	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$157	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$66)
$C$DW$158	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$6)
$C$DW$159	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$9)
$C$DW$160	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$100

$C$DW$T$101	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_address_class(0x16)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)

$C$DW$T$122	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)
$C$DW$161	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$66)
$C$DW$162	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$T$122

$C$DW$T$123	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$T$123, DW_AT_address_class(0x16)
$C$DW$T$124	.dwtag  DW_TAG_typedef, DW_AT_name("TimerCallback_t")
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$T$124, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$163	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$163, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$164	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$6)
$C$DW$T$55	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$164)
$C$DW$T$56	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$T$56, DW_AT_address_class(0x16)
$C$DW$T$103	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$132	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$132, DW_AT_language(DW_LANG_C)
$C$DW$165	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$9)
$C$DW$T$53	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$165)
$C$DW$T$54	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x16)
$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$57	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$166	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$6)
$C$DW$167	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$57

$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x16)

$C$DW$T$84	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$168	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$66)
$C$DW$169	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$47)
$C$DW$170	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$6)
$C$DW$171	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$84

$C$DW$T$85	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_address_class(0x16)
$C$DW$T$86	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_language(DW_LANG_C)
$C$DW$172	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$86)
$C$DW$T$87	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$172)
$C$DW$T$88	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$88, DW_AT_address_class(0x16)
$C$DW$T$89	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_address_class(0x16)

$C$DW$T$93	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)
$C$DW$173	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$66)
$C$DW$174	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$9)
$C$DW$175	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$93

$C$DW$T$94	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_address_class(0x16)
$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x16)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)

$C$DW$T$96	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$96, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x01)
$C$DW$176	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$177	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$96

$C$DW$T$97	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)

$C$DW$T$62	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$62, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)
$C$DW$178	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$179	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$180	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$181	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$182	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$183	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$184	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$185	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$62

$C$DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)

$C$DW$T$79	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$79, DW_AT_byte_size(0x80)
$C$DW$186	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$186, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$79


$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x06)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$187, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$188, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$189, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$190, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$191, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$192, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$43

$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$193	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$50)
$C$DW$T$51	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$193)
$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x16)

$C$DW$T$106	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$106, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x132)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$194, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$195, DW_AT_name("objdict")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$196, DW_AT_name("PDO_status")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$197	.dwtag  DW_TAG_member
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$197, DW_AT_name("firstIndex")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$197, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$197, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$198, DW_AT_name("lastIndex")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$198, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$199, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$200	.dwtag  DW_TAG_member
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$200, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$200, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$201	.dwtag  DW_TAG_member
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$201, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$201, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$201, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$202	.dwtag  DW_TAG_member
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$202, DW_AT_name("transfers")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$202, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$203	.dwtag  DW_TAG_member
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$203, DW_AT_name("nodeState")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$203, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$203, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$204	.dwtag  DW_TAG_member
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$204, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$204, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$204, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$205	.dwtag  DW_TAG_member
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$205, DW_AT_name("initialisation")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$205, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$205, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$206	.dwtag  DW_TAG_member
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$206, DW_AT_name("preOperational")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$206, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$207	.dwtag  DW_TAG_member
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$207, DW_AT_name("operational")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$207, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$208	.dwtag  DW_TAG_member
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$208, DW_AT_name("stopped")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$208, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$208, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$209	.dwtag  DW_TAG_member
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$209, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$209, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$210	.dwtag  DW_TAG_member
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$210, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$210, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$210, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$211	.dwtag  DW_TAG_member
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$211, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$211, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$211, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$212	.dwtag  DW_TAG_member
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$212, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$212, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$212, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$213	.dwtag  DW_TAG_member
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$213, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$213, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$213, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$214	.dwtag  DW_TAG_member
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$214, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$214, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$214, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$215	.dwtag  DW_TAG_member
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$215, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$215, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$215, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$216	.dwtag  DW_TAG_member
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$216, DW_AT_name("heartbeatError")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$216, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$216, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$217	.dwtag  DW_TAG_member
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$217, DW_AT_name("NMTable")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$217, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$217, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$218	.dwtag  DW_TAG_member
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$218, DW_AT_name("syncTimer")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$218, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$218, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$219	.dwtag  DW_TAG_member
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$219, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$219, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$219, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$220	.dwtag  DW_TAG_member
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$220, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$220, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$220, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$221	.dwtag  DW_TAG_member
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$221, DW_AT_name("post_sync")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_post_sync")
	.dwattr $C$DW$221, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$221, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$222	.dwtag  DW_TAG_member
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$222, DW_AT_name("post_TPDO")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$222, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$222, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$223	.dwtag  DW_TAG_member
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$223, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$223, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$223, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$224	.dwtag  DW_TAG_member
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$224, DW_AT_name("toggle")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$224, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$224, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$225	.dwtag  DW_TAG_member
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$225, DW_AT_name("canHandle")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$225, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$225, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$226	.dwtag  DW_TAG_member
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$226, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$226, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$226, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$227	.dwtag  DW_TAG_member
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$227, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$227, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$227, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$228	.dwtag  DW_TAG_member
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$228, DW_AT_name("globalCallback")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$228, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$228, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$229	.dwtag  DW_TAG_member
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$229, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$229, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$229, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$230	.dwtag  DW_TAG_member
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$230, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$230, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$231, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$231, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$232	.dwtag  DW_TAG_member
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$232, DW_AT_name("dcf_request")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$232, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$233, DW_AT_name("error_state")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$234	.dwtag  DW_TAG_member
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$234, DW_AT_name("error_history_size")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$234, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$235, DW_AT_name("error_number")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$236	.dwtag  DW_TAG_member
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$236, DW_AT_name("error_first_element")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$236, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$236, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$237, DW_AT_name("error_register")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$238, DW_AT_name("error_cobid")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$239, DW_AT_name("error_data")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$240	.dwtag  DW_TAG_member
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$240, DW_AT_name("post_emcy")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$240, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$240, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$241, DW_AT_name("lss_transfer")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$242, DW_AT_name("eeprom_index")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$243, DW_AT_name("eeprom_size")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$106

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x16)

$C$DW$T$108	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$108, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x0e)
$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$244, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$245, DW_AT_name("event_timer")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$246, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$247, DW_AT_name("last_message")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$108

$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$49	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x16)

$C$DW$T$110	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$110, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x14)
$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$248, DW_AT_name("nodeId")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$249, DW_AT_name("whoami")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$250, DW_AT_name("state")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$251, DW_AT_name("toggle")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$252, DW_AT_name("abortCode")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$253, DW_AT_name("index")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$254, DW_AT_name("subIndex")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$255, DW_AT_name("port")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$256, DW_AT_name("count")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$257, DW_AT_name("offset")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$258, DW_AT_name("datap")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$259, DW_AT_name("dataType")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$260, DW_AT_name("timer")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$261, DW_AT_name("Callback")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$110

$C$DW$T$60	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)

$C$DW$T$61	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x3c)
$C$DW$262	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$262, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$61


$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x04)
$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$263, DW_AT_name("pSubindex")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$264, DW_AT_name("bSubCount")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$264, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$265, DW_AT_name("index")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114

$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$266	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$45)
$C$DW$T$46	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$266)
$C$DW$T$47	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x16)

$C$DW$T$90	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$267	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$9)
$C$DW$268	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$73)
$C$DW$269	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$89)
	.dwendtag $C$DW$T$90

$C$DW$T$91	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x16)
$C$DW$T$92	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)

$C$DW$T$115	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$115, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x08)
$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$270, DW_AT_name("bAccessType")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$270, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$271	.dwtag  DW_TAG_member
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$271, DW_AT_name("bDataType")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$271, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$272	.dwtag  DW_TAG_member
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$272, DW_AT_name("size")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$272, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$272, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$273	.dwtag  DW_TAG_member
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$273, DW_AT_name("pObject")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$273, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$274	.dwtag  DW_TAG_member
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$274, DW_AT_name("bProcessor")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$274, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$274, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$115

$C$DW$275	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$115)
$C$DW$T$111	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$275)
$C$DW$T$112	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$112, DW_AT_language(DW_LANG_C)
$C$DW$T$113	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$276	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$276, DW_AT_location[DW_OP_reg0]
$C$DW$277	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$277, DW_AT_location[DW_OP_reg1]
$C$DW$278	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$278, DW_AT_location[DW_OP_reg2]
$C$DW$279	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$279, DW_AT_location[DW_OP_reg3]
$C$DW$280	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$280, DW_AT_location[DW_OP_reg20]
$C$DW$281	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$281, DW_AT_location[DW_OP_reg21]
$C$DW$282	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$282, DW_AT_location[DW_OP_reg22]
$C$DW$283	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$283, DW_AT_location[DW_OP_reg23]
$C$DW$284	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$284, DW_AT_location[DW_OP_reg24]
$C$DW$285	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$285, DW_AT_location[DW_OP_reg25]
$C$DW$286	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$286, DW_AT_location[DW_OP_reg26]
$C$DW$287	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$287, DW_AT_location[DW_OP_reg28]
$C$DW$288	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$288, DW_AT_location[DW_OP_reg29]
$C$DW$289	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$289, DW_AT_location[DW_OP_reg30]
$C$DW$290	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$290, DW_AT_location[DW_OP_reg31]
$C$DW$291	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$291, DW_AT_location[DW_OP_regx 0x20]
$C$DW$292	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$292, DW_AT_location[DW_OP_regx 0x21]
$C$DW$293	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$293, DW_AT_location[DW_OP_regx 0x22]
$C$DW$294	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$294, DW_AT_location[DW_OP_regx 0x23]
$C$DW$295	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$295, DW_AT_location[DW_OP_regx 0x24]
$C$DW$296	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$296, DW_AT_location[DW_OP_regx 0x25]
$C$DW$297	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$297, DW_AT_location[DW_OP_regx 0x26]
$C$DW$298	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$298, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$299	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$299, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$300	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$300, DW_AT_location[DW_OP_reg4]
$C$DW$301	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$301, DW_AT_location[DW_OP_reg6]
$C$DW$302	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$302, DW_AT_location[DW_OP_reg8]
$C$DW$303	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$303, DW_AT_location[DW_OP_reg10]
$C$DW$304	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$304, DW_AT_location[DW_OP_reg12]
$C$DW$305	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$305, DW_AT_location[DW_OP_reg14]
$C$DW$306	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$306, DW_AT_location[DW_OP_reg16]
$C$DW$307	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$307, DW_AT_location[DW_OP_reg17]
$C$DW$308	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$308, DW_AT_location[DW_OP_reg18]
$C$DW$309	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$309, DW_AT_location[DW_OP_reg19]
$C$DW$310	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$310, DW_AT_location[DW_OP_reg5]
$C$DW$311	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$311, DW_AT_location[DW_OP_reg7]
$C$DW$312	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$312, DW_AT_location[DW_OP_reg9]
$C$DW$313	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$313, DW_AT_location[DW_OP_reg11]
$C$DW$314	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$314, DW_AT_location[DW_OP_reg13]
$C$DW$315	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$315, DW_AT_location[DW_OP_reg15]
$C$DW$316	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$316, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$317	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$317, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$318	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$318, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$319	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$319, DW_AT_location[DW_OP_regx 0x30]
$C$DW$320	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$320, DW_AT_location[DW_OP_regx 0x33]
$C$DW$321	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$321, DW_AT_location[DW_OP_regx 0x34]
$C$DW$322	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$322, DW_AT_location[DW_OP_regx 0x37]
$C$DW$323	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$323, DW_AT_location[DW_OP_regx 0x38]
$C$DW$324	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$324, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$325	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$325, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$326	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$326, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$327	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$327, DW_AT_location[DW_OP_regx 0x40]
$C$DW$328	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$328, DW_AT_location[DW_OP_regx 0x43]
$C$DW$329	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$329, DW_AT_location[DW_OP_regx 0x44]
$C$DW$330	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$330, DW_AT_location[DW_OP_regx 0x47]
$C$DW$331	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$331, DW_AT_location[DW_OP_regx 0x48]
$C$DW$332	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$332, DW_AT_location[DW_OP_regx 0x49]
$C$DW$333	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$333, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$334	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$334, DW_AT_location[DW_OP_regx 0x27]
$C$DW$335	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$335, DW_AT_location[DW_OP_regx 0x28]
$C$DW$336	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$336, DW_AT_location[DW_OP_reg27]
$C$DW$337	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$337, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

