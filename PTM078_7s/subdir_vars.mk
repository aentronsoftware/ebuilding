################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../F28069.cmd \
../F2806x_Headers_BIOS.cmd 

TCF_SRCS += \
../mms.tcf 

S??_SRCS += \
./mmscfg.s?? 

C_SRCS += \
../F2806x_DevInit068.c \
../Init280x068.c \
../buffer.c \
../can.c \
../convert.c \
../error.c \
../hal.c \
../i2c.c \
../mms.c \
./mmscfg_c.c \
../param.c \
../recorder.c \
../sci1.c \
../temperature.c \
../usb_dev_hid.c 

OBJS += \
./F2806x_DevInit068.obj \
./Init280x068.obj \
./buffer.obj \
./can.obj \
./convert.obj \
./error.obj \
./hal.obj \
./i2c.obj \
./mms.obj \
./mmscfg.obj \
./mmscfg_c.obj \
./param.obj \
./recorder.obj \
./sci1.obj \
./temperature.obj \
./usb_dev_hid.obj 

GEN_MISC_FILES += \
./mms.cdb 

GEN_HDRS += \
./mmscfg.h \
./mmscfg.h?? 

S??_DEPS += \
./mmscfg.pp 

C_DEPS += \
./F2806x_DevInit068.pp \
./Init280x068.pp \
./buffer.pp \
./can.pp \
./convert.pp \
./error.pp \
./hal.pp \
./i2c.pp \
./mms.pp \
./mmscfg_c.pp \
./param.pp \
./recorder.pp \
./sci1.pp \
./temperature.pp \
./usb_dev_hid.pp 

GEN_CMDS += \
./mmscfg.cmd 

GEN_FILES += \
./mmscfg.cmd \
./mmscfg.s?? \
./mmscfg_c.c 

GEN_HDRS__QUOTED += \
"mmscfg.h" \
"mmscfg.h??" 

GEN_MISC_FILES__QUOTED += \
"mms.cdb" 

GEN_FILES__QUOTED += \
"mmscfg.cmd" \
"mmscfg.s??" \
"mmscfg_c.c" 

C_DEPS__QUOTED += \
"F2806x_DevInit068.pp" \
"Init280x068.pp" \
"buffer.pp" \
"can.pp" \
"convert.pp" \
"error.pp" \
"hal.pp" \
"i2c.pp" \
"mms.pp" \
"mmscfg_c.pp" \
"param.pp" \
"recorder.pp" \
"sci1.pp" \
"temperature.pp" \
"usb_dev_hid.pp" 

S??_DEPS__QUOTED += \
"mmscfg.pp" 

OBJS__QUOTED += \
"F2806x_DevInit068.obj" \
"Init280x068.obj" \
"buffer.obj" \
"can.obj" \
"convert.obj" \
"error.obj" \
"hal.obj" \
"i2c.obj" \
"mms.obj" \
"mmscfg.obj" \
"mmscfg_c.obj" \
"param.obj" \
"recorder.obj" \
"sci1.obj" \
"temperature.obj" \
"usb_dev_hid.obj" 

C_SRCS__QUOTED += \
"../F2806x_DevInit068.c" \
"../Init280x068.c" \
"../buffer.c" \
"../can.c" \
"../convert.c" \
"../error.c" \
"../hal.c" \
"../i2c.c" \
"../mms.c" \
"./mmscfg_c.c" \
"../param.c" \
"../recorder.c" \
"../sci1.c" \
"../temperature.c" \
"../usb_dev_hid.c" 

GEN_CMDS__FLAG += \
-l"./mmscfg.cmd" 

S??_SRCS__QUOTED += \
"./mmscfg.s??" 

S??_OBJS__QUOTED += \
"mmscfg.obj" 


