################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../LIB2806x/source/F2806x_CodeStartBranch.asm \
../LIB2806x/source/F2806x_usDelay.asm 

C_SRCS += \
../LIB2806x/source/F2806x_CpuTimers.c \
../LIB2806x/source/F2806x_GlobalVariableDefs.c \
../LIB2806x/source/crc32.c 

OBJS += \
./LIB2806x/source/F2806x_CodeStartBranch.obj \
./LIB2806x/source/F2806x_CpuTimers.obj \
./LIB2806x/source/F2806x_GlobalVariableDefs.obj \
./LIB2806x/source/F2806x_usDelay.obj \
./LIB2806x/source/crc32.obj 

ASM_DEPS += \
./LIB2806x/source/F2806x_CodeStartBranch.pp \
./LIB2806x/source/F2806x_usDelay.pp 

C_DEPS += \
./LIB2806x/source/F2806x_CpuTimers.pp \
./LIB2806x/source/F2806x_GlobalVariableDefs.pp \
./LIB2806x/source/crc32.pp 

C_DEPS__QUOTED += \
"LIB2806x\source\F2806x_CpuTimers.pp" \
"LIB2806x\source\F2806x_GlobalVariableDefs.pp" \
"LIB2806x\source\crc32.pp" 

OBJS__QUOTED += \
"LIB2806x\source\F2806x_CodeStartBranch.obj" \
"LIB2806x\source\F2806x_CpuTimers.obj" \
"LIB2806x\source\F2806x_GlobalVariableDefs.obj" \
"LIB2806x\source\F2806x_usDelay.obj" \
"LIB2806x\source\crc32.obj" 

ASM_DEPS__QUOTED += \
"LIB2806x\source\F2806x_CodeStartBranch.pp" \
"LIB2806x\source\F2806x_usDelay.pp" 

ASM_SRCS__QUOTED += \
"../LIB2806x/source/F2806x_CodeStartBranch.asm" \
"../LIB2806x/source/F2806x_usDelay.asm" 

C_SRCS__QUOTED += \
"../LIB2806x/source/F2806x_CpuTimers.c" \
"../LIB2806x/source/F2806x_GlobalVariableDefs.c" \
"../LIB2806x/source/crc32.c" 


