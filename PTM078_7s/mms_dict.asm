;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue Feb 02 11:43:33 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\R_Module_48V_Standard\PTM078_7s")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_SOH+0,32
	.bits	0,16			; _ODV_Module1_SOH @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Capacity_Used+0,32
	.bits	0,16			; _ODV_Module1_Capacity_Used @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Capacity_set+0,32
	.bits	0,16			; _ODV_Module1_Capacity_set @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_MaxDeltaVoltage+0,32
	.bits	0,16			; _ODV_Module1_MaxDeltaVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Module_SOC_Current+0,32
	.bits	32,16			; _ODV_Module1_Module_SOC_Current @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Module_SOC_Time+0,32
	.bits	0,16			; _ODV_Module1_Module_SOC_Time @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Time_Remaining+0,32
	.bits	0,16			; _ODV_Module1_Time_Remaining @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Alive_Counter+0,32
	.bits	0,16			; _ODV_Module1_Alive_Counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Temperature+0,32
	.bits	22,16			; _ODV_Module1_Temperature @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Throughput+0,32
	.bits	0,16			; _ODV_Module1_Throughput @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Voltage+0,32
	.bits	0,16			; _ODV_Module1_Voltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Current+0,32
	.bits	0,16			; _ODV_Module1_Current @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Warnings+0,32
	.bits	0,16			; _ODV_Module1_Warnings @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Temperature2+0,32
	.bits	20,16			; _ODV_Module1_Temperature2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_MinCellVoltage+0,32
	.bits	0,16			; _ODV_Module1_MinCellVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1016+0,32
	.bits	0,16			; _mms_dict_highestSubIndex_obj1016 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1017+0,32
	.bits	0,16			; _mms_dict_obj1017 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1003+0,32
	.bits	0,16			; _mms_dict_highestSubIndex_obj1003 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj100A+0,32
	.bits	0,16			; _mms_dict_obj100A @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1200+0,32
	.bits	2,16			; _mms_dict_highestSubIndex_obj1200 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1280+0,32
	.bits	3,16			; _mms_dict_highestSubIndex_obj1280 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1018+0,32
	.bits	4,16			; _mms_dict_highestSubIndex_obj1018 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj6450+0,32
	.bits	16,16			; _mms_dict_highestSubIndex_obj6450 @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_1,16
	.field  	_ODV_Controlword+0,32
	.bits	0,16			; _ODV_Controlword._ControlWord @ 0
$C$IR_1:	.set	1

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_2,16
	.field  	_ODV_Statusword+0,32
	.bits	0,16			; _ODV_Statusword._StatusWord @ 0
$C$IR_2:	.set	1

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_MaxCellVoltage+0,32
	.bits	0,16			; _ODV_Module1_MaxCellVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Debug+0,32
	.bits	0,16			; _ODV_Debug @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1001+0,32
	.bits	0,16			; _mms_dict_obj1001 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_bDeviceNodeId+0,32
	.bits	0,16			; _mms_dict_bDeviceNodeId @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Board_Config+0,32
	.bits	1698,16			; _ODP_Board_Config @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Board_BaudRate+0,32
	.bits	250,16			; _ODP_Board_BaudRate @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_CommError_TimeOut+0,32
	.bits	0,16			; _ODP_CommError_TimeOut @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_CommError_Delay+0,32
	.bits	100,16			; _ODP_CommError_Delay @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Board_Deactive_FF+0,32
	.bits	1,16			; _ODP_Board_Deactive_FF @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Board_Chip+0,32
	.bits	69,16			; _ODV_Board_Chip @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Board_Bootloader+0,32
	.bits	250,16			; _ODP_Board_Bootloader @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Board_HW_Version+0,32
	.bits	301,16			; _ODV_Board_HW_Version @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Master_Module1_Undefined1+0,32
	.bits	0,16			; _ODV_Master_Module1_Undefined1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Master_Module1_Undefined2+0,32
	.bits	0,16			; _ODV_Master_Module1_Undefined2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Master_Module1_Error+0,32
	.bits	0,16			; _ODV_Master_Module1_Error @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Master_Module1_Temp+0,32
	.bits	0,16			; _ODV_Master_Module1_Temp @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_MachineEvent+0,32
	.bits	0,16			; _ODV_MachineEvent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_CommError_Set+0,32
	.bits	0,16			; _ODV_CommError_Set @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Master_Module1_Undefined3+0,32
	.bits	0,16			; _ODV_Master_Module1_Undefined3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Battery_Total_ChargedkWh+0,32
	.bits	0,16			; _ODV_Battery_Total_ChargedkWh @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_SOC_SOC1+0,32
	.bits	0,16			; _ODV_SOC_SOC1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Battery_Cycles+0,32
	.bits	2500,16			; _ODP_Battery_Cycles @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Battery_Total_Cycles+0,32
	.bits	0,16			; _ODV_Battery_Total_Cycles @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_SOC+0,32
	.bits	0,16			; _ODV_Module1_SOC @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Alarms+0,32
	.bits	0,16			; _ODV_Module1_Alarms @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_SOC_SOC2+0,32
	.bits	0,16			; _ODV_SOC_SOC2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Module1_Status+0,32
	.bits	0,16			; _ODV_Module1_Status @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SleepCurrent+0,32
	.bits	0,16			; _ODP_SleepCurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_RTC_Pos+0,32
	.bits	0,16			; _ODV_RTC_Pos @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_MachineMode+0,32
	.bits	0,16			; _ODV_MachineMode @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Board_Type+0,32
	.bits	48,16			; _ODV_Board_Type @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Battery_kWh+0,32
	.bits	2,16			; _ODP_Battery_kWh @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Battery_kWh_Life+0,32
	.bits	6000,16			; _ODP_Battery_kWh_Life @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_RTC_Len+0,32
	.bits	0,16			; _ODV_RTC_Len @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj6100+0,32
	.bits	2,16			; _mms_dict_highestSubIndex_obj6100 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2006+0,32
	.bits	3,16			; _mms_dict_highestSubIndex_obj2006 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1A03+0,32
	.bits	5,16			; _mms_dict_highestSubIndex_obj1A03 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2004+0,32
	.bits	12,16			; _mms_dict_highestSubIndex_obj2004 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj200A+0,32
	.bits	8,16			; _mms_dict_highestSubIndex_obj200A @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj200B+0,32
	.bits	8,16			; _mms_dict_highestSubIndex_obj200B @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2008+0,32
	.bits	15,16			; _mms_dict_highestSubIndex_obj2008 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2009+0,32
	.bits	30,16			; _mms_dict_highestSubIndex_obj2009 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1803_Event_Timer+0,32
	.bits	0,16			; _mms_dict_obj1803_Event_Timer @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj6401+0,32
	.bits	23,16			; _mms_dict_highestSubIndex_obj6401 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1803_Inhibit_Time+0,32
	.bits	0,16			; _mms_dict_obj1803_Inhibit_Time @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1803_Compatibility_Entry+0,32
	.bits	0,16			; _mms_dict_obj1803_Compatibility_Entry @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1A02+0,32
	.bits	5,16			; _mms_dict_highestSubIndex_obj1A02 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj6300+0,32
	.bits	2,16			; _mms_dict_highestSubIndex_obj6300 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1A00+0,32
	.bits	7,16			; _mms_dict_highestSubIndex_obj1A00 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2014+0,32
	.bits	8,16			; _mms_dict_highestSubIndex_obj2014 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2105+0,32
	.bits	6,16			; _mms_dict_highestSubIndex_obj2105 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2106+0,32
	.bits	2,16			; _mms_dict_highestSubIndex_obj2106 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2013+0,32
	.bits	4,16			; _mms_dict_highestSubIndex_obj2013 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2040+0,32
	.bits	10,16			; _mms_dict_highestSubIndex_obj2040 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2100+0,32
	.bits	3,16			; _mms_dict_highestSubIndex_obj2100 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2015+0,32
	.bits	6,16			; _mms_dict_highestSubIndex_obj2015 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2023+0,32
	.bits	5,16			; _mms_dict_highestSubIndex_obj2023 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj200D+0,32
	.bits	8,16			; _mms_dict_highestSubIndex_obj200D @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj200F+0,32
	.bits	3,16			; _mms_dict_highestSubIndex_obj200F @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1A01+0,32
	.bits	8,16			; _mms_dict_highestSubIndex_obj1A01 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj200C+0,32
	.bits	8,16			; _mms_dict_highestSubIndex_obj200C @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2011+0,32
	.bits	5,16			; _mms_dict_highestSubIndex_obj2011 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2012+0,32
	.bits	7,16			; _mms_dict_highestSubIndex_obj2012 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2200+0,32
	.bits	19,16			; _mms_dict_highestSubIndex_obj2200 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj2010+0,32
	.bits	2,16			; _mms_dict_highestSubIndex_obj2010 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj6431+0,32
	.bits	23,16			; _mms_dict_highestSubIndex_obj6431 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1800+0,32
	.bits	5,16			; _mms_dict_highestSubIndex_obj1800 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj6442+0,32
	.bits	16,16			; _mms_dict_highestSubIndex_obj6442 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1600+0,32
	.bits	3,16			; _mms_dict_highestSubIndex_obj1600 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1800_Compatibility_Entry+0,32
	.bits	0,16			; _mms_dict_obj1800_Compatibility_Entry @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1800_Event_Timer+0,32
	.bits	0,16			; _mms_dict_obj1800_Event_Timer @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1800_Transmission_Type+0,32
	.bits	32,16			; _mms_dict_obj1800_Transmission_Type @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1800_Inhibit_Time+0,32
	.bits	0,16			; _mms_dict_obj1800_Inhibit_Time @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj6446+0,32
	.bits	16,16			; _mms_dict_highestSubIndex_obj6446 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1400+0,32
	.bits	5,16			; _mms_dict_highestSubIndex_obj1400 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Version+0,32
	.bits	30,16			; _ODV_Version @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1280_Node_ID_of_the_SDO_Server+0,32
	.bits	0,16			; _mms_dict_obj1280_Node_ID_of_the_SDO_Server @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1400_Compatibility_Entry+0,32
	.bits	0,16			; _mms_dict_obj1400_Compatibility_Entry @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1400_Event_Timer+0,32
	.bits	0,16			; _mms_dict_obj1400_Event_Timer @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1400_Transmission_Type+0,32
	.bits	254,16			; _mms_dict_obj1400_Transmission_Type @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1802_Inhibit_Time+0,32
	.bits	0,16			; _mms_dict_obj1802_Inhibit_Time @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1802_Compatibility_Entry+0,32
	.bits	0,16			; _mms_dict_obj1802_Compatibility_Entry @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1802+0,32
	.bits	5,16			; _mms_dict_highestSubIndex_obj1802 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1802_Transmission_Type+0,32
	.bits	32,16			; _mms_dict_obj1802_Transmission_Type @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1803+0,32
	.bits	5,16			; _mms_dict_highestSubIndex_obj1803 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1803_Transmission_Type+0,32
	.bits	32,16			; _mms_dict_obj1803_Transmission_Type @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1802_Event_Timer+0,32
	.bits	0,16			; _mms_dict_obj1802_Event_Timer @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj6411+0,32
	.bits	16,16			; _mms_dict_highestSubIndex_obj6411 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj1801+0,32
	.bits	5,16			; _mms_dict_highestSubIndex_obj1801 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1801_Transmission_Type+0,32
	.bits	32,16			; _mms_dict_obj1801_Transmission_Type @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1400_Inhibit_Time+0,32
	.bits	0,16			; _mms_dict_obj1400_Inhibit_Time @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj6430+0,32
	.bits	23,16			; _mms_dict_highestSubIndex_obj6430 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1801_Event_Timer+0,32
	.bits	0,16			; _mms_dict_obj1801_Event_Timer @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_highestSubIndex_obj642F+0,32
	.bits	23,16			; _mms_dict_highestSubIndex_obj642F @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1801_Inhibit_Time+0,32
	.bits	0,16			; _mms_dict_obj1801_Inhibit_Time @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_mms_dict_obj1801_Compatibility_Entry+0,32
	.bits	0,16			; _mms_dict_obj1801_Compatibility_Entry @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Tmax+0,32
	.bits	60,16			; _ODP_SafetyLimits_Tmax @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_SciSend+0,32
	.bits	0,16			; _ODV_SciSend @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_T3_max+0,32
	.bits	60,16			; _ODP_SafetyLimits_T3_max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Voltage_delay+0,32
	.bits	10,16			; _ODP_SafetyLimits_Voltage_delay @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_T3_min+0,32
	.bits	236,16			; _ODP_SafetyLimits_T3_min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Imax_charge+0,32
	.bits	70,16			; _ODP_SafetyLimits_Imax_charge @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Tmin+0,32
	.bits	236,16			; _ODP_SafetyLimits_Tmin @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_ErrorDsp_ErrorLevel+0,32
	.bits	0,16			; _ODV_ErrorDsp_ErrorLevel @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Cell_Nb+0,32
	.bits	7,16			; _ODP_SafetyLimits_Cell_Nb @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Current_delay+0,32
	.bits	10,16			; _ODP_SafetyLimits_Current_delay @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Low_Voltage_Current_Allow+0,32
	.bits	-10,16			; _ODP_SafetyLimits_Low_Voltage_Current_Allow @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Low_Voltage_Current_Delay+0,32
	.bits	10,16			; _ODP_SafetyLimits_Low_Voltage_Current_Delay @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Low_Voltage_Charge_Delay+0,32
	.bits	60,16			; _ODP_SafetyLimits_Low_Voltage_Charge_Delay @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Umax+0,32
	.bits	400,16			; _ODP_SafetyLimits_Umax @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Umin+0,32
	.bits	320,16			; _ODP_SafetyLimits_Umin @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_SleepTimeout+0,32
	.bits	0,16			; _ODP_SafetyLimits_SleepTimeout @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_UnderCurrent+0,32
	.bits	-200,16			; _ODP_SafetyLimits_UnderCurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_BalancingTimeout+0,32
	.bits	2,16			; _ODP_SafetyLimits_BalancingTimeout @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Temp_Delay+0,32
	.bits	60,16			; _ODP_SafetyLimits_Temp_Delay @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_DamagedVoltage+0,32
	.bits	21,16			; _ODP_SafetyLimits_DamagedVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_VersionParameters+0,32
	.bits	171,16			; _ODP_VersionParameters @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Recorder_TriggerIndex+0,32
	.bits	0,16			; _ODV_Recorder_TriggerIndex @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_OverVoltage+0,32
	.bits	430,16			; _ODP_SafetyLimits_OverVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_UnderVoltage+0,32
	.bits	280,16			; _ODP_SafetyLimits_UnderVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Charge_In_Thres_Cur+0,32
	.bits	2,16			; _ODP_SafetyLimits_Charge_In_Thres_Cur @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Recorder_Start+0,32
	.bits	65535,16			; _ODV_Recorder_Start @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Overcurrent+0,32
	.bits	70,16			; _ODP_SafetyLimits_Overcurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Recorder_Variables+0,32
	.bits	0,16			; _ODV_Recorder_Variables @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Recorder_ReadIndex+0,32
	.bits	0,16			; _ODV_Recorder_ReadIndex @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Recorder_Multiunits+0,32
	.bits	0,16			; _ODV_Recorder_Multiunits @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Recorder_PreTrigger+0,32
	.bits	0,16			; _ODV_Recorder_PreTrigger @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Mosfet_Tmin+0,32
	.bits	201,16			; _ODP_SafetyLimits_Mosfet_Tmin @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Mosfet_Tmax+0,32
	.bits	125,16			; _ODP_SafetyLimits_Mosfet_Tmax @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_3,16
	.field  	_ODV_Recorder_Control+0,32
	.bits	0,1			; _ODV_Recorder_Control._Pos_Record @ 0
$C$IR_3:	.set	1

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Umax_bal_delta+0,32
	.bits	1000,16			; _ODP_SafetyLimits_Umax_bal_delta @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Recorder_Size+0,32
	.bits	8192,16			; _ODV_Recorder_Size @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Resistor_Tmax+0,32
	.bits	125,16			; _ODP_SafetyLimits_Resistor_Tmax @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Umin_bal_delta+0,32
	.bits	50,16			; _ODP_SafetyLimits_Umin_bal_delta @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Resistor_Tmin+0,32
	.bits	201,16			; _ODP_SafetyLimits_Resistor_Tmin @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Imax_dis+0,32
	.bits	180,16			; _ODP_SafetyLimits_Imax_dis @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_SafetyLimits_Resistor_Delay+0,32
	.bits	30,16			; _ODP_SafetyLimits_Resistor_Delay @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Heater_OffTemp+0,32
	.bits	15,16			; _ODP_Heater_OffTemp @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Heater_OnTemp+0,32
	.bits	5,16			; _ODP_Heater_OnTemp @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Heater_Off_Cell_Voltage+0,32
	.bits	380,16			; _ODP_Heater_Off_Cell_Voltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_Status+0,32
	.bits	0,16			; _ODV_Gateway_Status @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_SOC+0,32
	.bits	0,16			; _ODV_Gateway_SOC @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Heater_Heater_Status+0,32
	.bits	0,16			; _ODV_Heater_Heater_Status @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Contactor_Slope+0,32
	.bits	100,16			; _ODP_Contactor_Slope @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Contactor_Overcurrent+0,32
	.bits	2000,16			; _ODP_Contactor_Overcurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_Temperature+0,32
	.bits	0,16			; _ODV_Gateway_Temperature @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Contactor_Delay+0,32
	.bits	100,16			; _ODP_Contactor_Delay @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Contactor_Undercurrent+0,32
	.bits	100,16			; _ODP_Contactor_Undercurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Unused_Undefined5+0,32
	.bits	0,16			; _ODP_Unused_Undefined5 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Unused_Undefined4+0,32
	.bits	0,16			; _ODP_Unused_Undefined4 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Unused_Undefined6+0,32
	.bits	0,16			; _ODP_Unused_Undefined6 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Unused_Undefined8+0,32
	.bits	0,16			; _ODP_Unused_Undefined8 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Unused_Undefined7+0,32
	.bits	0,16			; _ODP_Unused_Undefined7 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Master_Module1_Voltage+0,32
	.bits	0,16			; _ODV_Master_Module1_Voltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Unused_Undefined1+0,32
	.bits	0,16			; _ODP_Unused_Undefined1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Contactor_Undefined+0,32
	.bits	0,16			; _ODV_Contactor_Undefined @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Unused_Undefined2+0,32
	.bits	0,16			; _ODP_Unused_Undefined2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_State+0,32
	.bits	0,16			; _ODV_Gateway_State @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_SOH+0,32
	.bits	0,16			; _ODV_Gateway_SOH @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Current_Min+0,32
	.bits	0,16			; _ODV_Current_Min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Current_Max+0,32
	.bits	0,16			; _ODV_Current_Max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Voltage_Min+0,32
	.bits	0,16			; _ODV_Voltage_Min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Current_DischargeAllowed+0,32
	.bits	0,16			; _ODV_Current_DischargeAllowed @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_MaxCellVoltage+0,32
	.bits	0,16			; _ODV_Gateway_MaxCellVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Voltage_Max+0,32
	.bits	0,16			; _ODV_Voltage_Max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_MachineState+0,32
	.bits	0,16			; _ODV_MachineState @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Temperature_Min+0,32
	.bits	0,16			; _ODV_Temperature_Min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_Voltage+0,32
	.bits	0,16			; _ODV_Gateway_Voltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Temperature_Average_data+0,32
	.bits	0,16			; _ODV_Temperature_Average_data @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Current_ChargeAllowed+0,32
	.bits	0,16			; _ODV_Current_ChargeAllowed @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_MaxDeltaCellVoltage+0,32
	.bits	0,16			; _ODV_Gateway_MaxDeltaCellVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Contactor_Setup+0,32
	.bits	800,16			; _ODP_Contactor_Setup @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Current_C_D_Mode+0,32
	.bits	100,16			; _ODP_Current_C_D_Mode @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Contactor_Hold+0,32
	.bits	200,16			; _ODP_Contactor_Hold @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_LogNB+0,32
	.bits	0,16			; _ODV_Gateway_LogNB @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Temperature_Max+0,32
	.bits	0,16			; _ODV_Temperature_Max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_MinCellVoltage+0,32
	.bits	0,16			; _ODV_Gateway_MinCellVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODP_Unused_Undefined3+0,32
	.bits	0,16			; _ODP_Unused_Undefined3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ODV_Gateway_Current+0,32
	.bits	0,16			; _ODV_Gateway_Current @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1802_COB_ID_used_by_PDO+0,32
	.bits	896,32			; _mms_dict_obj1802_COB_ID_used_by_PDO @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_ErrorDsp_WarningNumber+0,32
	.bits	0,32			; _ODV_ErrorDsp_WarningNumber @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1801_COB_ID_used_by_PDO+0,32
	.bits	640,32			; _mms_dict_obj1801_COB_ID_used_by_PDO @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_Recorder_TriggerLevel+0,32
	.bits	0,32			; _ODV_Recorder_TriggerLevel @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_ErrorDsp_ErrorNumber+0,32
	.bits	0,32			; _ODV_ErrorDsp_ErrorNumber @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1803_COB_ID_used_by_PDO+0,32
	.bits	1152,32			; _mms_dict_obj1803_COB_ID_used_by_PDO @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODP_OnTime+0,32
	.bits	0,32			; _ODP_OnTime @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_Recorder_Period+0,32
	.bits	8000,32			; _ODV_Recorder_Period @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_Recorder_NbOfSamples+0,32
	.bits	0,32			; _ODV_Recorder_NbOfSamples @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODP_Password+0,32
	.bits	100,32			; _ODP_Password @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODP_CrcParameters+0,32
	.bits	1479920862,32			; _ODP_CrcParameters @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODP_Board_RevisionNumber+0,32
	.bits	1,32			; _ODP_Board_RevisionNumber @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1006+0,32
	.bits	0,32			; _mms_dict_obj1006 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1005+0,32
	.bits	128,32			; _mms_dict_obj1005 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_StoreParameters+0,32
	.bits	0,32			; _ODV_StoreParameters @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1014+0,32
	.bits	128,32			; _mms_dict_obj1014 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_CommError_Counter+0,32
	.bits	0,32			; _ODV_CommError_Counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_4,16
	.field  	_ODV_Read_Inputs_16_Bit+0,32
	.bits	0,16			; _ODV_Read_Inputs_16_Bit[0] @ 0
	.bits	0,16			; _ODV_Read_Inputs_16_Bit[1] @ 16
$C$IR_4:	.set	2

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_CommError_StateCounter+0,32
	.bits	0,32			; _ODV_CommError_StateCounter @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_5,16
	.field  	_mms_dict_obj1003+0,32
	.bits	0,32			; _mms_dict_obj1003[0] @ 0
$C$IR_5:	.set	2

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODP_RandomNB+0,32
	.bits	0,32			; _ODP_RandomNB @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_ResetHW+0,32
	.bits	0,32			; _ODV_ResetHW @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_Board_Manufactuer+0,32
	.bits	5129537,32			; _ODV_Board_Manufactuer @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODP_Battery_Capacity+0,32
	.xfloat	$strtod("0x1.4p+5")		; _ODP_Battery_Capacity @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1000+0,32
	.bits	-65135,32			; _mms_dict_obj1000 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_SysTick_ms+0,32
	.bits	0,32			; _ODV_SysTick_ms @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODP_Board_SerialNumber+0,32
	.bits	0,32			; _ODP_Board_SerialNumber @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_RestoreDefaultParameters+0,32
	.bits	0,32			; _ODV_RestoreDefaultParameters @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_Gateway_Power+0,32
	.bits	0,32			; _ODV_Gateway_Power @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_Gateway_WhCounter+0,32
	.bits	0,32			; _ODV_Gateway_WhCounter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1280_COB_ID_Server_to_Client_Receive_SDO+0,32
	.bits	1408,32			; _mms_dict_obj1280_COB_ID_Server_to_Client_Receive_SDO @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1400_COB_ID_used_by_PDO+0,32
	.bits	483,32			; _mms_dict_obj1400_COB_ID_used_by_PDO @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1800_COB_ID_used_by_PDO+0,32
	.bits	384,32			; _mms_dict_obj1800_COB_ID_used_by_PDO @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ODV_Gateway_Errorcode+0,32
	.bits	0,32			; _ODV_Gateway_Errorcode @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_6,16
	.field  	_ODV_Supported_drive_modes+0,32
	.bits	0,16			; _ODV_Supported_drive_modes._tototo @ 0
$C$IR_6:	.set	1

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_7,16
	.field  	_mms_dict_obj1016+0,32
	.bits	0,32			; _mms_dict_obj1016[0] @ 0
$C$IR_7:	.set	2

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1018_Serial_Number+0,32
	.bits	0,32			; _mms_dict_obj1018_Serial_Number @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1018_Product_Code+0,32
	.bits	0,32			; _mms_dict_obj1018_Product_Code @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1018_Revision_Number+0,32
	.bits	0,32			; _mms_dict_obj1018_Revision_Number @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_8,16
	.field  	_ODV_Write_Outputs_16_Bit+0,32
	.bits	0,16			; _ODV_Write_Outputs_16_Bit[0] @ 0
	.bits	0,16			; _ODV_Write_Outputs_16_Bit[1] @ 16
$C$IR_8:	.set	2

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO+0,32
	.bits	1536,32			; _mms_dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1200_COB_ID_Client_to_Server_Receive_SDO+0,32
	.bits	1536,32			; _mms_dict_obj1200_COB_ID_Client_to_Server_Receive_SDO @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1200_COB_ID_Server_to_Client_Transmit_SDO+0,32
	.bits	1408,32			; _mms_dict_obj1200_COB_ID_Server_to_Client_Transmit_SDO @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mms_dict_obj1018_Vendor_ID+0,32
	.bits	0,32			; _mms_dict_obj1018_Vendor_ID @ 0

	.sect	".cinit"
	.align	1
	.field  	-4,16
	.field  	_ODV_RTC_Text+0,32
	.bits		0,32
	.bits		0,32			; _ODV_RTC_Text @ 0

	.sect	".cinit"
	.align	1
	.field  	-4,16
	.field  	_ODV_Security+0,32
	.bits		0,32
	.bits		0,32			; _ODV_Security @ 0

	.sect	".cinit"
	.align	1
	.field  	-4,16
	.field  	_ODV_Recorder_Vectors+0,32
	.bits		0x1010101,32
	.bits		0x101,32			; _ODV_Recorder_Vectors @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_9,16
	.field  	_mms_dict_Index1003_callbacks+0,32
	.bits	0,32			; _mms_dict_Index1003_callbacks[0] @ 0
	.bits	0,32			; _mms_dict_Index1003_callbacks[1] @ 32
$C$IR_9:	.set	4

	.sect	".cinit"
	.align	1
	.field  	-4,16
	.field  	_ODV_Gateway_Date_Time+0,32
	.bits		0,32
	.bits		0,32			; _ODV_Gateway_Date_Time @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_10,16
	.field  	_mms_dict_obj1600+0,32
	.bits	538247440,32			; _mms_dict_obj1600[0] @ 0
	.bits	538247688,32			; _mms_dict_obj1600[1] @ 32
	.bits	538247944,32			; _mms_dict_obj1600[2] @ 64
$C$IR_10:	.set	6

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_11,16
	.field  	_ODP_Power_ChargeLimits+0,32
	.bits	30,16			; _ODP_Power_ChargeLimits[0] @ 0
	.bits	100,16			; _ODP_Power_ChargeLimits[1] @ 16
	.bits	100,16			; _ODP_Power_ChargeLimits[2] @ 32
	.bits	0,16			; _ODP_Power_ChargeLimits[3] @ 48
	.bits	0,16			; _ODP_Power_ChargeLimits[4] @ 64
	.bits	0,16			; _ODP_Power_ChargeLimits[5] @ 80
	.bits	0,16			; _ODP_Power_ChargeLimits[6] @ 96
	.bits	0,16			; _ODP_Power_ChargeLimits[7] @ 112
$C$IR_11:	.set	8

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_12,16
	.field  	_ODP_Temperature_ChargeLimits+0,32
	.bits	236,16			; _ODP_Temperature_ChargeLimits[0] @ 0
	.bits	246,16			; _ODP_Temperature_ChargeLimits[1] @ 16
	.bits	50,16			; _ODP_Temperature_ChargeLimits[2] @ 32
	.bits	60,16			; _ODP_Temperature_ChargeLimits[3] @ 48
	.bits	0,16			; _ODP_Temperature_ChargeLimits[4] @ 64
	.bits	0,16			; _ODP_Temperature_ChargeLimits[5] @ 80
	.bits	0,16			; _ODP_Temperature_ChargeLimits[6] @ 96
	.bits	0,16			; _ODP_Temperature_ChargeLimits[7] @ 112
$C$IR_12:	.set	8

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_13,16
	.field  	_ODP_Temperature_DischargeLimits+0,32
	.bits	236,16			; _ODP_Temperature_DischargeLimits[0] @ 0
	.bits	246,16			; _ODP_Temperature_DischargeLimits[1] @ 16
	.bits	60,16			; _ODP_Temperature_DischargeLimits[2] @ 32
	.bits	70,16			; _ODP_Temperature_DischargeLimits[3] @ 48
	.bits	0,16			; _ODP_Temperature_DischargeLimits[4] @ 64
	.bits	0,16			; _ODP_Temperature_DischargeLimits[5] @ 80
	.bits	0,16			; _ODP_Temperature_DischargeLimits[6] @ 96
	.bits	0,16			; _ODP_Temperature_DischargeLimits[7] @ 112
$C$IR_13:	.set	8

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_14,16
	.field  	_ODP_Power_DischargeLimits+0,32
	.bits	30,16			; _ODP_Power_DischargeLimits[0] @ 0
	.bits	100,16			; _ODP_Power_DischargeLimits[1] @ 16
	.bits	100,16			; _ODP_Power_DischargeLimits[2] @ 32
	.bits	0,16			; _ODP_Power_DischargeLimits[3] @ 48
	.bits	0,16			; _ODP_Power_DischargeLimits[4] @ 64
	.bits	0,16			; _ODP_Power_DischargeLimits[5] @ 80
	.bits	0,16			; _ODP_Power_DischargeLimits[6] @ 96
	.bits	0,16			; _ODP_Power_DischargeLimits[7] @ 112
$C$IR_14:	.set	8

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_15,16
	.field  	_mms_dict_obj1A03+0,32
	.bits	537985808,32			; _mms_dict_obj1A03[0] @ 0
	.bits	537986064,32			; _mms_dict_obj1A03[1] @ 32
	.bits	570429704,32			; _mms_dict_obj1A03[2] @ 64
	.bits	570426640,32			; _mms_dict_obj1A03[3] @ 96
	.bits	538181896,32			; _mms_dict_obj1A03[4] @ 128
$C$IR_15:	.set	10

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_16,16
	.field  	_mms_dict_obj1A02+0,32
	.bits	570429200,32			; _mms_dict_obj1A02[0] @ 0
	.bits	570429456,32			; _mms_dict_obj1A02[1] @ 32
	.bits	553976592,32			; _mms_dict_obj1A02[2] @ 64
	.bits	538181896,32			; _mms_dict_obj1A02[3] @ 96
	.bits	538181896,32			; _mms_dict_obj1A02[4] @ 128
$C$IR_16:	.set	10

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_17,16
	.field  	_mms_dict_obj1A00+0,32
	.bits	570425608,32			; _mms_dict_obj1A00[0] @ 0
	.bits	570425864,32			; _mms_dict_obj1A00[1] @ 32
	.bits	570426120,32			; _mms_dict_obj1A00[2] @ 64
	.bits	538181896,32			; _mms_dict_obj1A00[3] @ 96
	.bits	538181896,32			; _mms_dict_obj1A00[4] @ 128
	.bits	538181896,32			; _mms_dict_obj1A00[5] @ 160
	.bits	570426384,32			; _mms_dict_obj1A00[6] @ 192
$C$IR_17:	.set	14

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_18,16
	.field  	_mms_dict_obj1A01+0,32
	.bits	570427656,32			; _mms_dict_obj1A01[0] @ 0
	.bits	570427400,32			; _mms_dict_obj1A01[1] @ 32
	.bits	538117128,32			; _mms_dict_obj1A01[2] @ 64
	.bits	570428936,32			; _mms_dict_obj1A01[3] @ 96
	.bits	570426888,32			; _mms_dict_obj1A01[4] @ 128
	.bits	570428168,32			; _mms_dict_obj1A01[5] @ 160
	.bits	553713672,32			; _mms_dict_obj1A01[6] @ 192
	.bits	537397256,32			; _mms_dict_obj1A01[7] @ 224
$C$IR_18:	.set	16

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_19,16
	.field  	_ODV_Write_Analogue_Output_16_Bit+0,32
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[0] @ 0
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[1] @ 16
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[2] @ 32
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[3] @ 48
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[4] @ 64
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[5] @ 80
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[6] @ 96
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[7] @ 112
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[8] @ 128
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[9] @ 144
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[10] @ 160
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[11] @ 176
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[12] @ 192
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[13] @ 208
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[14] @ 224
	.bits	0,16			; _ODV_Write_Analogue_Output_16_Bit[15] @ 240
$C$IR_19:	.set	16

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_20,16
	.field  	_ODV_Read_Analogue_Input_16_Bit+0,32
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[0] @ 0
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[1] @ 16
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[2] @ 32
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[3] @ 48
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[4] @ 64
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[5] @ 80
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[6] @ 96
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[7] @ 112
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[8] @ 128
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[9] @ 144
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[10] @ 160
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[11] @ 176
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[12] @ 192
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[13] @ 208
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[14] @ 224
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[15] @ 240
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[16] @ 256
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[17] @ 272
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[18] @ 288
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[19] @ 304
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[20] @ 320
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[21] @ 336
	.bits	0,16			; _ODV_Read_Analogue_Input_16_Bit[22] @ 352
$C$IR_20:	.set	23

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_21,16
	.field  	_ODP_Analogue_Output_Offset_Integer+0,32
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[0] @ 0
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[1] @ 32
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[2] @ 64
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[3] @ 96
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[4] @ 128
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[5] @ 160
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[6] @ 192
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[7] @ 224
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[8] @ 256
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[9] @ 288
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[10] @ 320
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[11] @ 352
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[12] @ 384
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[13] @ 416
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[14] @ 448
	.bits	0,32			; _ODP_Analogue_Output_Offset_Integer[15] @ 480
$C$IR_21:	.set	32

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_22,16
	.field  	_ODP_Analogue_Output_SI_Unit+0,32
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[0] @ 0
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[1] @ 32
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[2] @ 64
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[3] @ 96
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[4] @ 128
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[5] @ 160
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[6] @ 192
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[7] @ 224
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[8] @ 256
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[9] @ 288
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[10] @ 320
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[11] @ 352
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[12] @ 384
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[13] @ 416
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[14] @ 448
	.bits	-50069504,32			; _ODP_Analogue_Output_SI_Unit[15] @ 480
$C$IR_22:	.set	32

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_23,16
	.field  	_ODP_Analogue_Output_Scaling_Float+0,32
	.xfloat	$strtod("0x1.1ba92ap+3")		; _ODP_Analogue_Output_Scaling_Float[0] @ 0
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[1] @ 32
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[2] @ 64
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[3] @ 96
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[4] @ 128
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[5] @ 160
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[6] @ 192
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[7] @ 224
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[8] @ 256
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[9] @ 288
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[10] @ 320
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[11] @ 352
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[12] @ 384
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[13] @ 416
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[14] @ 448
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Output_Scaling_Float[15] @ 480
$C$IR_23:	.set	32

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_24,16
	.field  	_ODP_Analogue_Input_Scaling_Float+0,32
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Input_Scaling_Float[0] @ 0
	.xfloat	$strtod("0x1.99999ap-4")		; _ODP_Analogue_Input_Scaling_Float[1] @ 32
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[2] @ 64
	.xfloat	$strtod("0x1p-5")		; _ODP_Analogue_Input_Scaling_Float[3] @ 96
	.xfloat	$strtod("0x1p-3")		; _ODP_Analogue_Input_Scaling_Float[4] @ 128
	.xfloat	$strtod("0x1p-3")		; _ODP_Analogue_Input_Scaling_Float[5] @ 160
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[6] @ 192
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[7] @ 224
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[8] @ 256
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[9] @ 288
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[10] @ 320
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[11] @ 352
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[12] @ 384
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[13] @ 416
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[14] @ 448
	.xfloat	$strtod("0x1p+0")		; _ODP_Analogue_Input_Scaling_Float[15] @ 480
	.xfloat	$strtod("0x1.9p-2")		; _ODP_Analogue_Input_Scaling_Float[16] @ 512
	.xfloat	$strtod("0x0p+0")		; _ODP_Analogue_Input_Scaling_Float[17] @ 544
	.xfloat	$strtod("0x0p+0")		; _ODP_Analogue_Input_Scaling_Float[18] @ 576
	.xfloat	$strtod("-0x1.28f5c2p-1")		; _ODP_Analogue_Input_Scaling_Float[19] @ 608
	.xfloat	$strtod("0x0p+0")		; _ODP_Analogue_Input_Scaling_Float[20] @ 640
	.xfloat	$strtod("0x0p+0")		; _ODP_Analogue_Input_Scaling_Float[21] @ 672
	.xfloat	$strtod("0x0p+0")		; _ODP_Analogue_Input_Scaling_Float[22] @ 704
$C$IR_24:	.set	46

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_25,16
	.field  	_ODP_Analogue_Input_SI_unit+0,32
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[0] @ 0
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[1] @ 32
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[2] @ 64
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[3] @ 96
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[4] @ 128
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[5] @ 160
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[6] @ 192
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[7] @ 224
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[8] @ 256
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[9] @ 288
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[10] @ 320
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[11] @ 352
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[12] @ 384
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[13] @ 416
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[14] @ 448
	.bits	-47841280,32			; _ODP_Analogue_Input_SI_unit[15] @ 480
	.bits	0,32			; _ODP_Analogue_Input_SI_unit[16] @ 512
	.bits	0,32			; _ODP_Analogue_Input_SI_unit[17] @ 544
	.bits	0,32			; _ODP_Analogue_Input_SI_unit[18] @ 576
	.bits	0,32			; _ODP_Analogue_Input_SI_unit[19] @ 608
	.bits	0,32			; _ODP_Analogue_Input_SI_unit[20] @ 640
	.bits	0,32			; _ODP_Analogue_Input_SI_unit[21] @ 672
	.bits	0,32			; _ODP_Analogue_Input_SI_unit[22] @ 704
$C$IR_25:	.set	46

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_26,16
	.field  	_ODP_Analogue_Input_Offset_Integer+0,32
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[0] @ 0
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[1] @ 32
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[2] @ 64
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[3] @ 96
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[4] @ 128
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[5] @ 160
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[6] @ 192
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[7] @ 224
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[8] @ 256
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[9] @ 288
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[10] @ 320
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[11] @ 352
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[12] @ 384
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[13] @ 416
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[14] @ 448
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[15] @ 480
	.bits	-32720,32			; _ODP_Analogue_Input_Offset_Integer[16] @ 512
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[17] @ 544
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[18] @ 576
	.bits	-32768,32			; _ODP_Analogue_Input_Offset_Integer[19] @ 608
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[20] @ 640
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[21] @ 672
	.bits	0,32			; _ODP_Analogue_Input_Offset_Integer[22] @ 704
$C$IR_26:	.set	46

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_27,16
	.field  	_mms_dict_PDO_status+0,32
	.bits	0,16			; _mms_dict_PDO_status[0]._transmit_type_parameter @ 0
	.bits	-1,16			; _mms_dict_PDO_status[0]._event_timer @ 16
	.bits	-1,16			; _mms_dict_PDO_status[0]._inhibit_timer @ 32
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._cob_id @ 48
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._rtr @ 64
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._len @ 80
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._data[0] @ 96
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._data[1] @ 112
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._data[2] @ 128
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._data[3] @ 144
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._data[4] @ 160
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._data[5] @ 176
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._data[6] @ 192
	.bits	0,16			; _mms_dict_PDO_status[0]._last_message._data[7] @ 208
	.bits	0,16			; _mms_dict_PDO_status[1]._transmit_type_parameter @ 224
	.bits	-1,16			; _mms_dict_PDO_status[1]._event_timer @ 240
	.bits	-1,16			; _mms_dict_PDO_status[1]._inhibit_timer @ 256
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._cob_id @ 272
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._rtr @ 288
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._len @ 304
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._data[0] @ 320
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._data[1] @ 336
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._data[2] @ 352
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._data[3] @ 368
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._data[4] @ 384
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._data[5] @ 400
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._data[6] @ 416
	.bits	0,16			; _mms_dict_PDO_status[1]._last_message._data[7] @ 432
	.bits	0,16			; _mms_dict_PDO_status[2]._transmit_type_parameter @ 448
	.bits	-1,16			; _mms_dict_PDO_status[2]._event_timer @ 464
	.bits	-1,16			; _mms_dict_PDO_status[2]._inhibit_timer @ 480
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._cob_id @ 496
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._rtr @ 512
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._len @ 528
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._data[0] @ 544
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._data[1] @ 560
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._data[2] @ 576
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._data[3] @ 592
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._data[4] @ 608
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._data[5] @ 624
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._data[6] @ 640
	.bits	0,16			; _mms_dict_PDO_status[2]._last_message._data[7] @ 656
	.bits	0,16			; _mms_dict_PDO_status[3]._transmit_type_parameter @ 672
	.bits	-1,16			; _mms_dict_PDO_status[3]._event_timer @ 688
	.bits	-1,16			; _mms_dict_PDO_status[3]._inhibit_timer @ 704
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._cob_id @ 720
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._rtr @ 736
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._len @ 752
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._data[0] @ 768
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._data[1] @ 784
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._data[2] @ 800
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._data[3] @ 816
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._data[4] @ 832
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._data[5] @ 848
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._data[6] @ 864
	.bits	0,16			; _mms_dict_PDO_status[3]._last_message._data[7] @ 880
$C$IR_27:	.set	56

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_28,16
	.field  	_ODI_mms_dict_Data+0,32
	.bits	_mms_dict_bDeviceNodeId,32		; _ODI_mms_dict_Data._bDeviceNodeId @ 0
	.bits	_mms_dict_objdict,32		; _ODI_mms_dict_Data._objdict @ 32
	.bits	_mms_dict_PDO_status,32		; _ODI_mms_dict_Data._PDO_status @ 64
	.bits	_mms_dict_firstIndex,32		; _ODI_mms_dict_Data._firstIndex @ 96
	.bits	_mms_dict_lastIndex,32		; _ODI_mms_dict_Data._lastIndex @ 128
	.bits	_mms_dict_ObjdictSize,32		; _ODI_mms_dict_Data._ObjdictSize @ 160
	.bits	_mms_dict_iam_a_slave,32		; _ODI_mms_dict_Data._iam_a_slave @ 192
	.bits	_ODI_mms_dict_valueRangeTest,32		; _ODI_mms_dict_Data._valueRangeTest @ 224
	.bits	0,16			; _ODI_mms_dict_Data._transfers[0]._nodeId @ 256
	.bits	0,16			; _ODI_mms_dict_Data._transfers[0]._whoami @ 272
	.bits	0,16			; _ODI_mms_dict_Data._transfers[0]._state @ 288
	.bits	0,16			; _ODI_mms_dict_Data._transfers[0]._toggle @ 304
	.bits	0,32			; _ODI_mms_dict_Data._transfers[0]._abortCode @ 320
	.bits	0,16			; _ODI_mms_dict_Data._transfers[0]._index @ 352
	.bits	0,16			; _ODI_mms_dict_Data._transfers[0]._subIndex @ 368
	.bits	0,16			; _ODI_mms_dict_Data._transfers[0]._port @ 384
	.space	16
	.bits	0,32			; _ODI_mms_dict_Data._transfers[0]._count @ 416
	.bits	0,32			; _ODI_mms_dict_Data._transfers[0]._offset @ 448
	.bits	0,32			; _ODI_mms_dict_Data._transfers[0]._datap @ 480
	.bits	0,16			; _ODI_mms_dict_Data._transfers[0]._dataType @ 512
	.bits	-1,16			; _ODI_mms_dict_Data._transfers[0]._timer @ 528
	.bits	0,32			; _ODI_mms_dict_Data._transfers[0]._Callback @ 544
	.bits	0,16			; _ODI_mms_dict_Data._transfers[1]._nodeId @ 576
	.bits	0,16			; _ODI_mms_dict_Data._transfers[1]._whoami @ 592
	.bits	0,16			; _ODI_mms_dict_Data._transfers[1]._state @ 608
	.bits	0,16			; _ODI_mms_dict_Data._transfers[1]._toggle @ 624
	.bits	0,32			; _ODI_mms_dict_Data._transfers[1]._abortCode @ 640
	.bits	0,16			; _ODI_mms_dict_Data._transfers[1]._index @ 672
	.bits	0,16			; _ODI_mms_dict_Data._transfers[1]._subIndex @ 688
	.bits	0,16			; _ODI_mms_dict_Data._transfers[1]._port @ 704
	.space	16
	.bits	0,32			; _ODI_mms_dict_Data._transfers[1]._count @ 736
	.bits	0,32			; _ODI_mms_dict_Data._transfers[1]._offset @ 768
	.bits	0,32			; _ODI_mms_dict_Data._transfers[1]._datap @ 800
	.bits	0,16			; _ODI_mms_dict_Data._transfers[1]._dataType @ 832
	.bits	-1,16			; _ODI_mms_dict_Data._transfers[1]._timer @ 848
	.bits	0,32			; _ODI_mms_dict_Data._transfers[1]._Callback @ 864
	.bits	0,16			; _ODI_mms_dict_Data._transfers[2]._nodeId @ 896
	.bits	0,16			; _ODI_mms_dict_Data._transfers[2]._whoami @ 912
	.bits	0,16			; _ODI_mms_dict_Data._transfers[2]._state @ 928
	.bits	0,16			; _ODI_mms_dict_Data._transfers[2]._toggle @ 944
	.bits	0,32			; _ODI_mms_dict_Data._transfers[2]._abortCode @ 960
	.bits	0,16			; _ODI_mms_dict_Data._transfers[2]._index @ 992
	.bits	0,16			; _ODI_mms_dict_Data._transfers[2]._subIndex @ 1008
	.bits	0,16			; _ODI_mms_dict_Data._transfers[2]._port @ 1024
	.space	16
	.bits	0,32			; _ODI_mms_dict_Data._transfers[2]._count @ 1056
	.bits	0,32			; _ODI_mms_dict_Data._transfers[2]._offset @ 1088
	.bits	0,32			; _ODI_mms_dict_Data._transfers[2]._datap @ 1120
	.bits	0,16			; _ODI_mms_dict_Data._transfers[2]._dataType @ 1152
	.bits	-1,16			; _ODI_mms_dict_Data._transfers[2]._timer @ 1168
	.bits	0,32			; _ODI_mms_dict_Data._transfers[2]._Callback @ 1184
	.bits	15,16			; _ODI_mms_dict_Data._nodeState @ 1216
	.bits	0,16			; _ODI_mms_dict_Data._CurrentCommunicationState._csBoot_Up @ 1232
	.bits	0,16			; _ODI_mms_dict_Data._CurrentCommunicationState._csSDO @ 1248
	.bits	0,16			; _ODI_mms_dict_Data._CurrentCommunicationState._csEmergency @ 1264
	.bits	0,16			; _ODI_mms_dict_Data._CurrentCommunicationState._csSYNC @ 1280
	.bits	0,16			; _ODI_mms_dict_Data._CurrentCommunicationState._csHeartbeat @ 1296
	.bits	0,16			; _ODI_mms_dict_Data._CurrentCommunicationState._csPDO @ 1312
	.bits	0,16			; _ODI_mms_dict_Data._CurrentCommunicationState._csLSS @ 1328
	.bits	__initialisation,32		; _ODI_mms_dict_Data._initialisation @ 1344
	.bits	__preOperational,32		; _ODI_mms_dict_Data._preOperational @ 1376
	.bits	__operational,32		; _ODI_mms_dict_Data._operational @ 1408
	.bits	__stopped,32		; _ODI_mms_dict_Data._stopped @ 1440
	.bits	0,32			; _ODI_mms_dict_Data._NMT_Slave_Node_Reset_Callback @ 1472
	.bits	0,32			; _ODI_mms_dict_Data._NMT_Slave_Communications_Reset_Callback @ 1504
	.bits	_mms_dict_highestSubIndex_obj1016,32		; _ODI_mms_dict_Data._ConsumerHeartbeatCount @ 1536
	.bits	_mms_dict_obj1016,32		; _ODI_mms_dict_Data._ConsumerHeartbeatEntries @ 1568
	.bits	_mms_dict_heartBeatTimers,32		; _ODI_mms_dict_Data._ConsumerHeartBeatTimers @ 1600
	.bits	_mms_dict_obj1017,32		; _ODI_mms_dict_Data._ProducerHeartBeatTime @ 1632
	.bits	-1,16			; _ODI_mms_dict_Data._ProducerHeartBeatTimer @ 1664
	.space	16
	.bits	__heartbeatError,32		; _ODI_mms_dict_Data._heartbeatError @ 1696
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[0] @ 1728
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[1] @ 1744
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[2] @ 1760
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[3] @ 1776
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[4] @ 1792
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[5] @ 1808
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[6] @ 1824
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[7] @ 1840
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[8] @ 1856
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[9] @ 1872
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[10] @ 1888
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[11] @ 1904
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[12] @ 1920
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[13] @ 1936
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[14] @ 1952
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[15] @ 1968
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[16] @ 1984
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[17] @ 2000
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[18] @ 2016
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[19] @ 2032
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[20] @ 2048
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[21] @ 2064
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[22] @ 2080
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[23] @ 2096
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[24] @ 2112
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[25] @ 2128
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[26] @ 2144
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[27] @ 2160
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[28] @ 2176
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[29] @ 2192
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[30] @ 2208
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[31] @ 2224
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[32] @ 2240
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[33] @ 2256
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[34] @ 2272
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[35] @ 2288
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[36] @ 2304
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[37] @ 2320
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[38] @ 2336
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[39] @ 2352
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[40] @ 2368
	.bits	15,16			; _ODI_mms_dict_Data._NMTable[41] @ 2384
$C$IR_28:	.set	150

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_29,16
	.field  	_ODI_mms_dict_Data+236,32
	.bits	-1,16			; _ODI_mms_dict_Data._syncTimer @ 3776
	.space	16
	.bits	_mms_dict_obj1005,32		; _ODI_mms_dict_Data._COB_ID_Sync @ 3808
	.bits	_mms_dict_obj1006,32		; _ODI_mms_dict_Data._Sync_Cycle_Period @ 3840
	.bits	__post_sync,32		; _ODI_mms_dict_Data._post_sync @ 3872
	.bits	__post_TPDO,32		; _ODI_mms_dict_Data._post_TPDO @ 3904
	.bits	__post_SlaveBootup,32		; _ODI_mms_dict_Data._post_SlaveBootup @ 3936
	.bits	0,16			; _ODI_mms_dict_Data._toggle @ 3968
	.bits	0,16			; _ODI_mms_dict_Data._canHandle @ 3984
	.bits	_ODI_mms_dict_scanIndexOD,32		; _ODI_mms_dict_Data._scanIndexOD @ 4000
	.bits	__storeODSubIndex,32		; _ODI_mms_dict_Data._storeODSubIndex @ 4032
	.bits	0,32			; _ODI_mms_dict_Data._globalCallback @ 4064
	.bits	0,32			; _ODI_mms_dict_Data._dcf_odentry @ 4096
	.bits	0,32			; _ODI_mms_dict_Data._dcf_cursor @ 4128
	.bits	1,32			; _ODI_mms_dict_Data._dcf_entries_count @ 4160
	.bits	0,16			; _ODI_mms_dict_Data._dcf_request @ 4192
	.bits	0,16			; _ODI_mms_dict_Data._error_state @ 4208
	.bits	1,16			; _ODI_mms_dict_Data._error_history_size @ 4224
	.space	16
	.bits	_mms_dict_highestSubIndex_obj1003,32		; _ODI_mms_dict_Data._error_number @ 4256
	.bits	_mms_dict_obj1003,32		; _ODI_mms_dict_Data._error_first_element @ 4288
	.bits	_mms_dict_obj1001,32		; _ODI_mms_dict_Data._error_register @ 4320
	.bits	_mms_dict_obj1014,32		; _ODI_mms_dict_Data._error_cobid @ 4352
	.bits	0,16			; _ODI_mms_dict_Data._error_data[0]._errCode @ 4384
	.bits	0,16			; _ODI_mms_dict_Data._error_data[0]._errRegMask @ 4400
	.bits	0,16			; _ODI_mms_dict_Data._error_data[0]._active @ 4416
	.bits	0,16			; _ODI_mms_dict_Data._error_data[1]._errCode @ 4432
	.bits	0,16			; _ODI_mms_dict_Data._error_data[1]._errRegMask @ 4448
	.bits	0,16			; _ODI_mms_dict_Data._error_data[1]._active @ 4464
	.bits	0,16			; _ODI_mms_dict_Data._error_data[2]._errCode @ 4480
	.bits	0,16			; _ODI_mms_dict_Data._error_data[2]._errRegMask @ 4496
	.bits	0,16			; _ODI_mms_dict_Data._error_data[2]._active @ 4512
	.bits	0,16			; _ODI_mms_dict_Data._error_data[3]._errCode @ 4528
	.bits	0,16			; _ODI_mms_dict_Data._error_data[3]._errRegMask @ 4544
	.bits	0,16			; _ODI_mms_dict_Data._error_data[3]._active @ 4560
	.bits	0,16			; _ODI_mms_dict_Data._error_data[4]._errCode @ 4576
	.bits	0,16			; _ODI_mms_dict_Data._error_data[4]._errRegMask @ 4592
	.bits	0,16			; _ODI_mms_dict_Data._error_data[4]._active @ 4608
	.bits	0,16			; _ODI_mms_dict_Data._error_data[5]._errCode @ 4624
	.bits	0,16			; _ODI_mms_dict_Data._error_data[5]._errRegMask @ 4640
	.bits	0,16			; _ODI_mms_dict_Data._error_data[5]._active @ 4656
	.bits	0,16			; _ODI_mms_dict_Data._error_data[6]._errCode @ 4672
	.bits	0,16			; _ODI_mms_dict_Data._error_data[6]._errRegMask @ 4688
	.bits	0,16			; _ODI_mms_dict_Data._error_data[6]._active @ 4704
	.bits	0,16			; _ODI_mms_dict_Data._error_data[7]._errCode @ 4720
	.bits	0,16			; _ODI_mms_dict_Data._error_data[7]._errRegMask @ 4736
	.bits	0,16			; _ODI_mms_dict_Data._error_data[7]._active @ 4752
	.bits	__post_emcy,32		; _ODI_mms_dict_Data._post_emcy @ 4768
	.bits	0,16			; _ODI_mms_dict_Data._lss_transfer @ 4800
	.space	16
	.bits	_ODI_EEPROM_INDEXES,32		; _ODI_mms_dict_Data._eeprom_index @ 4832
	.bits	215,16			; _ODI_mms_dict_Data._eeprom_size @ 4864
$C$IR_29:	.set	69

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_30,16
	.field  	_ODV_RecorderData1+0,32
	.bits	0,16			; _ODV_RecorderData1[0] @ 0
$C$IR_30:	.set	1


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("_post_TPDO")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("__post_TPDO")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$58)
	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("_post_sync")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("__post_sync")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$58)
	.dwendtag $C$DW$3


$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("_heartbeatError")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("__heartbeatError")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$58)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$5


$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("_post_emcy")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("__post_emcy")
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$58)
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$6)
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$9)
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$8


$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("_post_SlaveBootup")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("__post_SlaveBootup")
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$58)
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$13


$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("_preOperational")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("__preOperational")
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$58)
	.dwendtag $C$DW$16


$C$DW$18	.dwtag  DW_TAG_subprogram, DW_AT_name("_initialisation")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("__initialisation")
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$58)
	.dwendtag $C$DW$18


$C$DW$20	.dwtag  DW_TAG_subprogram, DW_AT_name("_stopped")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("__stopped")
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external
$C$DW$21	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$58)
	.dwendtag $C$DW$20


$C$DW$22	.dwtag  DW_TAG_subprogram, DW_AT_name("_operational")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("__operational")
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$58)
	.dwendtag $C$DW$22

	.global	_ODV_Module1_SOH
_ODV_Module1_SOH:	.usect	".ebss",1,1,0
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_SOH")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_ODV_Module1_SOH")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_addr _ODV_Module1_SOH]
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$24, DW_AT_external
	.global	_ODV_Module1_Capacity_Used
_ODV_Module1_Capacity_Used:	.usect	".ebss",1,1,0
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Capacity_Used")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_ODV_Module1_Capacity_Used")
	.dwattr $C$DW$25, DW_AT_location[DW_OP_addr _ODV_Module1_Capacity_Used]
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$25, DW_AT_external
	.global	_ODV_Module1_Capacity_set
_ODV_Module1_Capacity_set:	.usect	".ebss",1,1,0
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Capacity_set")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_ODV_Module1_Capacity_set")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_addr _ODV_Module1_Capacity_set]
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$26, DW_AT_external
	.global	_ODV_Module1_MaxDeltaVoltage
_ODV_Module1_MaxDeltaVoltage:	.usect	".ebss",1,1,0
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MaxDeltaVoltage")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_ODV_Module1_MaxDeltaVoltage")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_addr _ODV_Module1_MaxDeltaVoltage]
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$27, DW_AT_external
	.global	_ODV_Module1_Module_SOC_Current
_ODV_Module1_Module_SOC_Current:	.usect	".ebss",1,1,0
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Module_SOC_Current")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ODV_Module1_Module_SOC_Current")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_addr _ODV_Module1_Module_SOC_Current]
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$28, DW_AT_external
	.global	_ODV_Module1_Module_SOC_Time
_ODV_Module1_Module_SOC_Time:	.usect	".ebss",1,1,0
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Module_SOC_Time")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_ODV_Module1_Module_SOC_Time")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_addr _ODV_Module1_Module_SOC_Time]
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$29, DW_AT_external
	.global	_ODV_Module1_Time_Remaining
_ODV_Module1_Time_Remaining:	.usect	".ebss",1,1,0
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Time_Remaining")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_ODV_Module1_Time_Remaining")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_addr _ODV_Module1_Time_Remaining]
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$30, DW_AT_external
	.global	_ODV_Module1_Alive_Counter
_ODV_Module1_Alive_Counter:	.usect	".ebss",1,1,0
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Alive_Counter")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_ODV_Module1_Alive_Counter")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_addr _ODV_Module1_Alive_Counter]
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$31, DW_AT_external
	.global	_ODV_Module1_Temperature
_ODV_Module1_Temperature:	.usect	".ebss",1,1,0
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Temperature")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_ODV_Module1_Temperature")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_addr _ODV_Module1_Temperature]
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$32, DW_AT_external
	.global	_ODV_Module1_Throughput
_ODV_Module1_Throughput:	.usect	".ebss",1,1,0
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Throughput")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_ODV_Module1_Throughput")
	.dwattr $C$DW$33, DW_AT_location[DW_OP_addr _ODV_Module1_Throughput]
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$33, DW_AT_external
	.global	_ODV_Module1_Voltage
_ODV_Module1_Voltage:	.usect	".ebss",1,1,0
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Voltage")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ODV_Module1_Voltage")
	.dwattr $C$DW$34, DW_AT_location[DW_OP_addr _ODV_Module1_Voltage]
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$34, DW_AT_external
	.global	_ODV_Module1_Current
_ODV_Module1_Current:	.usect	".ebss",1,1,0
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Current")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_ODV_Module1_Current")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_addr _ODV_Module1_Current]
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$35, DW_AT_external
	.global	_ODV_Module1_Warnings
_ODV_Module1_Warnings:	.usect	".ebss",1,1,0
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Warnings")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_ODV_Module1_Warnings")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_addr _ODV_Module1_Warnings]
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$36, DW_AT_external
	.global	_ODV_Module1_Temperature2
_ODV_Module1_Temperature2:	.usect	".ebss",1,1,0
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Temperature2")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ODV_Module1_Temperature2")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_addr _ODV_Module1_Temperature2]
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$37, DW_AT_external
	.global	_ODV_Module1_MinCellVoltage
_ODV_Module1_MinCellVoltage:	.usect	".ebss",1,1,0
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MinCellVoltage")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_ODV_Module1_MinCellVoltage")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_addr _ODV_Module1_MinCellVoltage]
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$38, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1016
_mms_dict_highestSubIndex_obj1016:	.usect	".ebss",1,1,0
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1016")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1016")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1016]
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$39, DW_AT_external
	.global	_mms_dict_obj1017
_mms_dict_obj1017:	.usect	".ebss",1,1,0
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1017")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_mms_dict_obj1017")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_addr _mms_dict_obj1017]
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$40, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1003
_mms_dict_highestSubIndex_obj1003:	.usect	".ebss",1,1,0
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1003")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1003")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1003]
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$41, DW_AT_external
	.global	_mms_dict_obj100A
_mms_dict_obj100A:	.usect	".ebss",1,1,0
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj100A")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_mms_dict_obj100A")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_addr _mms_dict_obj100A]
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$42, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1200
_mms_dict_highestSubIndex_obj1200:	.usect	".ebss",1,1,0
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1200")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1200")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1200]
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$43, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1280
_mms_dict_highestSubIndex_obj1280:	.usect	".ebss",1,1,0
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1280")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1280")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1280]
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$44, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1018
_mms_dict_highestSubIndex_obj1018:	.usect	".ebss",1,1,0
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1018")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1018")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1018]
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$45, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj6450
_mms_dict_highestSubIndex_obj6450:	.usect	".ebss",1,1,0
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj6450")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj6450")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj6450]
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$46, DW_AT_external
	.global	_ODV_Controlword
_ODV_Controlword:	.usect	".ebss",1,1,0
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Controlword")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_ODV_Controlword")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_addr _ODV_Controlword]
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$47, DW_AT_external
	.global	_ODV_Statusword
_ODV_Statusword:	.usect	".ebss",1,1,0
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Statusword")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ODV_Statusword")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_addr _ODV_Statusword]
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$48, DW_AT_external
	.global	_ODV_Module1_MaxCellVoltage
_ODV_Module1_MaxCellVoltage:	.usect	".ebss",1,1,0
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MaxCellVoltage")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_ODV_Module1_MaxCellVoltage")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_addr _ODV_Module1_MaxCellVoltage]
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$49, DW_AT_external
	.global	_ODV_Debug
_ODV_Debug:	.usect	".ebss",1,1,0
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Debug")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_ODV_Debug")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_addr _ODV_Debug]
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$50, DW_AT_external
	.global	_mms_dict_heartBeatTimers
_mms_dict_heartBeatTimers:	.usect	".ebss",1,1,0
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_heartBeatTimers")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_mms_dict_heartBeatTimers")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_addr _mms_dict_heartBeatTimers]
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$51, DW_AT_external
	.global	_mms_dict_obj1001
_mms_dict_obj1001:	.usect	".ebss",1,1,0
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1001")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_mms_dict_obj1001")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_addr _mms_dict_obj1001]
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$52, DW_AT_external
	.global	_mms_dict_bDeviceNodeId
_mms_dict_bDeviceNodeId:	.usect	".ebss",1,1,0
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_bDeviceNodeId")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_mms_dict_bDeviceNodeId")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_addr _mms_dict_bDeviceNodeId]
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$53, DW_AT_external
	.global	_mms_dict_iam_a_slave
	.sect	".econst"
	.align	1
_mms_dict_iam_a_slave:
	.bits	1,16			; _mms_dict_iam_a_slave @ 0

$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_iam_a_slave")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_mms_dict_iam_a_slave")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_addr _mms_dict_iam_a_slave]
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$54, DW_AT_external
	.global	_ODP_Board_Config
_ODP_Board_Config:	.usect	".ebss",1,1,0
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_Config")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_ODP_Board_Config")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_addr _ODP_Board_Config]
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$55, DW_AT_external
	.global	_ODP_Board_BaudRate
_ODP_Board_BaudRate:	.usect	".ebss",1,1,0
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_BaudRate")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_ODP_Board_BaudRate")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_addr _ODP_Board_BaudRate]
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$56, DW_AT_external
	.global	_ODP_CommError_TimeOut
_ODP_CommError_TimeOut:	.usect	".ebss",1,1,0
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_TimeOut")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ODP_CommError_TimeOut")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_addr _ODP_CommError_TimeOut]
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$57, DW_AT_external
	.global	_ODP_CommError_Delay
_ODP_CommError_Delay:	.usect	".ebss",1,1,0
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_Delay")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_ODP_CommError_Delay")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_addr _ODP_CommError_Delay]
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$58, DW_AT_external
	.global	_ODP_Board_Deactive_FF
_ODP_Board_Deactive_FF:	.usect	".ebss",1,1,0
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_Deactive_FF")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_ODP_Board_Deactive_FF")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_addr _ODP_Board_Deactive_FF]
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$59, DW_AT_external
	.global	_ODV_Board_Chip
_ODV_Board_Chip:	.usect	".ebss",1,1,0
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Board_Chip")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ODV_Board_Chip")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_addr _ODV_Board_Chip]
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$60, DW_AT_external
	.global	_ODP_Board_Bootloader
_ODP_Board_Bootloader:	.usect	".ebss",1,1,0
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_Bootloader")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_ODP_Board_Bootloader")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_addr _ODP_Board_Bootloader]
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$61, DW_AT_external
	.global	_ODV_Board_HW_Version
_ODV_Board_HW_Version:	.usect	".ebss",1,1,0
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Board_HW_Version")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_ODV_Board_HW_Version")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_addr _ODV_Board_HW_Version]
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$62, DW_AT_external
	.global	_ODV_Master_Module1_Undefined1
_ODV_Master_Module1_Undefined1:	.usect	".ebss",1,1,0
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Master_Module1_Undefined1")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ODV_Master_Module1_Undefined1")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_addr _ODV_Master_Module1_Undefined1]
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$63, DW_AT_external
	.global	_ODV_Master_Module1_Undefined2
_ODV_Master_Module1_Undefined2:	.usect	".ebss",1,1,0
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Master_Module1_Undefined2")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_ODV_Master_Module1_Undefined2")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_addr _ODV_Master_Module1_Undefined2]
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$64, DW_AT_external
	.global	_ODV_Master_Module1_Error
_ODV_Master_Module1_Error:	.usect	".ebss",1,1,0
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Master_Module1_Error")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_ODV_Master_Module1_Error")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_addr _ODV_Master_Module1_Error]
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$65, DW_AT_external
	.global	_ODV_Master_Module1_Temp
_ODV_Master_Module1_Temp:	.usect	".ebss",1,1,0
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Master_Module1_Temp")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ODV_Master_Module1_Temp")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_addr _ODV_Master_Module1_Temp]
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$66, DW_AT_external
	.global	_ODV_MachineEvent
_ODV_MachineEvent:	.usect	".ebss",1,1,0
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineEvent")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_ODV_MachineEvent")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_addr _ODV_MachineEvent]
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$67, DW_AT_external
	.global	_ODV_CommError_Set
_ODV_CommError_Set:	.usect	".ebss",1,1,0
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("ODV_CommError_Set")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_ODV_CommError_Set")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_addr _ODV_CommError_Set]
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$68, DW_AT_external
	.global	_ODV_Master_Module1_Undefined3
_ODV_Master_Module1_Undefined3:	.usect	".ebss",1,1,0
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Master_Module1_Undefined3")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ODV_Master_Module1_Undefined3")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_addr _ODV_Master_Module1_Undefined3]
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$69, DW_AT_external
	.global	_ODV_Battery_Total_ChargedkWh
_ODV_Battery_Total_ChargedkWh:	.usect	".ebss",1,1,0
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Battery_Total_ChargedkWh")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_ODV_Battery_Total_ChargedkWh")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_addr _ODV_Battery_Total_ChargedkWh]
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$70, DW_AT_external
	.global	_ODV_SOC_SOC1
_ODV_SOC_SOC1:	.usect	".ebss",1,1,0
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SOC_SOC1")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_ODV_SOC_SOC1")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_addr _ODV_SOC_SOC1]
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$71, DW_AT_external
	.global	_ODP_Battery_Cycles
_ODP_Battery_Cycles:	.usect	".ebss",1,1,0
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Cycles")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_ODP_Battery_Cycles")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_addr _ODP_Battery_Cycles]
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$72, DW_AT_external
	.global	_ODV_Battery_Total_Cycles
_ODV_Battery_Total_Cycles:	.usect	".ebss",1,1,0
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Battery_Total_Cycles")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_ODV_Battery_Total_Cycles")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_addr _ODV_Battery_Total_Cycles]
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$73, DW_AT_external
	.global	_ODV_Module1_SOC
_ODV_Module1_SOC:	.usect	".ebss",1,1,0
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_SOC")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_ODV_Module1_SOC")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_addr _ODV_Module1_SOC]
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$74, DW_AT_external
	.global	_ODV_Module1_Alarms
_ODV_Module1_Alarms:	.usect	".ebss",1,1,0
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Alarms")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_ODV_Module1_Alarms")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_addr _ODV_Module1_Alarms]
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$75, DW_AT_external
	.global	_ODV_SOC_SOC2
_ODV_SOC_SOC2:	.usect	".ebss",1,1,0
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SOC_SOC2")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_ODV_SOC_SOC2")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_addr _ODV_SOC_SOC2]
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$76, DW_AT_external
	.global	_ODV_Module1_Status
_ODV_Module1_Status:	.usect	".ebss",1,1,0
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Status")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_ODV_Module1_Status")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_addr _ODV_Module1_Status]
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$77, DW_AT_external
	.global	_ODP_SleepCurrent
_ODP_SleepCurrent:	.usect	".ebss",1,1,0
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SleepCurrent")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_ODP_SleepCurrent")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_addr _ODP_SleepCurrent]
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$78, DW_AT_external
	.global	_ODV_RTC_Pos
_ODV_RTC_Pos:	.usect	".ebss",1,1,0
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RTC_Pos")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_ODV_RTC_Pos")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_addr _ODV_RTC_Pos]
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$79, DW_AT_external
	.global	_ODV_MachineMode
_ODV_MachineMode:	.usect	".ebss",1,1,0
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineMode")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_ODV_MachineMode")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_addr _ODV_MachineMode]
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$80, DW_AT_external
	.global	_ODV_Board_Type
_ODV_Board_Type:	.usect	".ebss",1,1,0
$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Board_Type")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_ODV_Board_Type")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_addr _ODV_Board_Type]
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$81, DW_AT_external
	.global	_ODP_Battery_kWh
_ODP_Battery_kWh:	.usect	".ebss",1,1,0
$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_kWh")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_ODP_Battery_kWh")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_addr _ODP_Battery_kWh]
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$82, DW_AT_external
	.global	_ODP_Battery_kWh_Life
_ODP_Battery_kWh_Life:	.usect	".ebss",1,1,0
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_kWh_Life")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_ODP_Battery_kWh_Life")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_addr _ODP_Battery_kWh_Life]
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$83, DW_AT_external
	.global	_ODV_RTC_Len
_ODV_RTC_Len:	.usect	".ebss",1,1,0
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RTC_Len")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_ODV_RTC_Len")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_addr _ODV_RTC_Len]
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$84, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj6100
_mms_dict_highestSubIndex_obj6100:	.usect	".ebss",1,1,0
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj6100")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj6100")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj6100]
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$85, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2006
_mms_dict_highestSubIndex_obj2006:	.usect	".ebss",1,1,0
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2006")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2006")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2006]
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$86, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1A03
_mms_dict_highestSubIndex_obj1A03:	.usect	".ebss",1,1,0
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1A03")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1A03")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1A03]
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$87, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2004
_mms_dict_highestSubIndex_obj2004:	.usect	".ebss",1,1,0
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2004")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2004")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2004]
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$88, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj200A
_mms_dict_highestSubIndex_obj200A:	.usect	".ebss",1,1,0
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj200A")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj200A")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj200A]
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$89, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj200B
_mms_dict_highestSubIndex_obj200B:	.usect	".ebss",1,1,0
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj200B")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj200B")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj200B]
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$90, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2008
_mms_dict_highestSubIndex_obj2008:	.usect	".ebss",1,1,0
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2008")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2008")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2008]
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$91, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2009
_mms_dict_highestSubIndex_obj2009:	.usect	".ebss",1,1,0
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2009")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2009")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2009]
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$92, DW_AT_external
	.global	_mms_dict_obj1803_Event_Timer
_mms_dict_obj1803_Event_Timer:	.usect	".ebss",1,1,0
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1803_Event_Timer")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_mms_dict_obj1803_Event_Timer")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_addr _mms_dict_obj1803_Event_Timer]
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$93, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj6401
_mms_dict_highestSubIndex_obj6401:	.usect	".ebss",1,1,0
$C$DW$94	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj6401")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj6401")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj6401]
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$94, DW_AT_external
	.global	_mms_dict_obj1803_Inhibit_Time
_mms_dict_obj1803_Inhibit_Time:	.usect	".ebss",1,1,0
$C$DW$95	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1803_Inhibit_Time")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_mms_dict_obj1803_Inhibit_Time")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_addr _mms_dict_obj1803_Inhibit_Time]
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$95, DW_AT_external
	.global	_mms_dict_obj1803_Compatibility_Entry
_mms_dict_obj1803_Compatibility_Entry:	.usect	".ebss",1,1,0
$C$DW$96	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1803_Compatibility_Entry")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_mms_dict_obj1803_Compatibility_Entry")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_addr _mms_dict_obj1803_Compatibility_Entry]
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$96, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1A02
_mms_dict_highestSubIndex_obj1A02:	.usect	".ebss",1,1,0
$C$DW$97	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1A02")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1A02")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1A02]
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$97, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj6300
_mms_dict_highestSubIndex_obj6300:	.usect	".ebss",1,1,0
$C$DW$98	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj6300")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj6300")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj6300]
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$98, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1A00
_mms_dict_highestSubIndex_obj1A00:	.usect	".ebss",1,1,0
$C$DW$99	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1A00")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1A00")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1A00]
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$99, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2014
_mms_dict_highestSubIndex_obj2014:	.usect	".ebss",1,1,0
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2014")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2014")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2014]
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$100, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2105
_mms_dict_highestSubIndex_obj2105:	.usect	".ebss",1,1,0
$C$DW$101	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2105")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2105")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2105]
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$101, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2106
_mms_dict_highestSubIndex_obj2106:	.usect	".ebss",1,1,0
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2106")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2106")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2106]
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$102, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2013
_mms_dict_highestSubIndex_obj2013:	.usect	".ebss",1,1,0
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2013")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2013")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2013]
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$103, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2040
_mms_dict_highestSubIndex_obj2040:	.usect	".ebss",1,1,0
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2040")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2040")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2040]
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$104, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2100
_mms_dict_highestSubIndex_obj2100:	.usect	".ebss",1,1,0
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2100")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2100")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2100]
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$105, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2015
_mms_dict_highestSubIndex_obj2015:	.usect	".ebss",1,1,0
$C$DW$106	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2015")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2015")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2015]
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$106, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2023
_mms_dict_highestSubIndex_obj2023:	.usect	".ebss",1,1,0
$C$DW$107	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2023")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2023")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2023]
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$107, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj200D
_mms_dict_highestSubIndex_obj200D:	.usect	".ebss",1,1,0
$C$DW$108	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj200D")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj200D")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj200D]
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$108, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj200F
_mms_dict_highestSubIndex_obj200F:	.usect	".ebss",1,1,0
$C$DW$109	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj200F")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj200F")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj200F]
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$109, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1A01
_mms_dict_highestSubIndex_obj1A01:	.usect	".ebss",1,1,0
$C$DW$110	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1A01")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1A01")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1A01]
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$110, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj200C
_mms_dict_highestSubIndex_obj200C:	.usect	".ebss",1,1,0
$C$DW$111	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj200C")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj200C")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj200C]
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$111, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2011
_mms_dict_highestSubIndex_obj2011:	.usect	".ebss",1,1,0
$C$DW$112	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2011")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2011")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2011]
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$112, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2012
_mms_dict_highestSubIndex_obj2012:	.usect	".ebss",1,1,0
$C$DW$113	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2012")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2012")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2012]
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$113, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2200
_mms_dict_highestSubIndex_obj2200:	.usect	".ebss",1,1,0
$C$DW$114	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2200")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2200")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2200]
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$114, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj2010
_mms_dict_highestSubIndex_obj2010:	.usect	".ebss",1,1,0
$C$DW$115	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj2010")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj2010")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj2010]
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$115, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj6431
_mms_dict_highestSubIndex_obj6431:	.usect	".ebss",1,1,0
$C$DW$116	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj6431")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj6431")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj6431]
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$116, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1800
_mms_dict_highestSubIndex_obj1800:	.usect	".ebss",1,1,0
$C$DW$117	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1800")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1800")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1800]
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$117, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj6442
_mms_dict_highestSubIndex_obj6442:	.usect	".ebss",1,1,0
$C$DW$118	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj6442")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj6442")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj6442]
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$118, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1600
_mms_dict_highestSubIndex_obj1600:	.usect	".ebss",1,1,0
$C$DW$119	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1600")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1600")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1600]
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$119, DW_AT_external
	.global	_mms_dict_obj1800_Compatibility_Entry
_mms_dict_obj1800_Compatibility_Entry:	.usect	".ebss",1,1,0
$C$DW$120	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1800_Compatibility_Entry")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_mms_dict_obj1800_Compatibility_Entry")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_addr _mms_dict_obj1800_Compatibility_Entry]
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$120, DW_AT_external
	.global	_mms_dict_obj1800_Event_Timer
_mms_dict_obj1800_Event_Timer:	.usect	".ebss",1,1,0
$C$DW$121	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1800_Event_Timer")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_mms_dict_obj1800_Event_Timer")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_addr _mms_dict_obj1800_Event_Timer]
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$121, DW_AT_external
	.global	_mms_dict_obj1800_Transmission_Type
_mms_dict_obj1800_Transmission_Type:	.usect	".ebss",1,1,0
$C$DW$122	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1800_Transmission_Type")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_mms_dict_obj1800_Transmission_Type")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_addr _mms_dict_obj1800_Transmission_Type]
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$122, DW_AT_external
	.global	_mms_dict_obj1800_Inhibit_Time
_mms_dict_obj1800_Inhibit_Time:	.usect	".ebss",1,1,0
$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1800_Inhibit_Time")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_mms_dict_obj1800_Inhibit_Time")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_addr _mms_dict_obj1800_Inhibit_Time]
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$123, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj6446
_mms_dict_highestSubIndex_obj6446:	.usect	".ebss",1,1,0
$C$DW$124	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj6446")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj6446")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj6446]
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$124, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1400
_mms_dict_highestSubIndex_obj1400:	.usect	".ebss",1,1,0
$C$DW$125	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1400")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1400")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1400]
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$125, DW_AT_external
	.global	_ODV_Version
_ODV_Version:	.usect	".ebss",1,1,0
$C$DW$126	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Version")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_ODV_Version")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_addr _ODV_Version]
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$126, DW_AT_external
	.global	_mms_dict_obj1280_Node_ID_of_the_SDO_Server
_mms_dict_obj1280_Node_ID_of_the_SDO_Server:	.usect	".ebss",1,1,0
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1280_Node_ID_of_the_SDO_Server")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_mms_dict_obj1280_Node_ID_of_the_SDO_Server")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_addr _mms_dict_obj1280_Node_ID_of_the_SDO_Server]
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$127, DW_AT_external
	.global	_mms_dict_obj1400_Compatibility_Entry
_mms_dict_obj1400_Compatibility_Entry:	.usect	".ebss",1,1,0
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1400_Compatibility_Entry")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_mms_dict_obj1400_Compatibility_Entry")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_addr _mms_dict_obj1400_Compatibility_Entry]
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$128, DW_AT_external
	.global	_mms_dict_obj1400_Event_Timer
_mms_dict_obj1400_Event_Timer:	.usect	".ebss",1,1,0
$C$DW$129	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1400_Event_Timer")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_mms_dict_obj1400_Event_Timer")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_addr _mms_dict_obj1400_Event_Timer]
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$129, DW_AT_external
	.global	_mms_dict_obj1400_Transmission_Type
_mms_dict_obj1400_Transmission_Type:	.usect	".ebss",1,1,0
$C$DW$130	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1400_Transmission_Type")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_mms_dict_obj1400_Transmission_Type")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_addr _mms_dict_obj1400_Transmission_Type]
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$130, DW_AT_external
	.global	_mms_dict_obj1802_Inhibit_Time
_mms_dict_obj1802_Inhibit_Time:	.usect	".ebss",1,1,0
$C$DW$131	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1802_Inhibit_Time")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_mms_dict_obj1802_Inhibit_Time")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_addr _mms_dict_obj1802_Inhibit_Time]
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$131, DW_AT_external
	.global	_mms_dict_obj1802_Compatibility_Entry
_mms_dict_obj1802_Compatibility_Entry:	.usect	".ebss",1,1,0
$C$DW$132	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1802_Compatibility_Entry")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_mms_dict_obj1802_Compatibility_Entry")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_addr _mms_dict_obj1802_Compatibility_Entry]
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$132, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1802
_mms_dict_highestSubIndex_obj1802:	.usect	".ebss",1,1,0
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1802")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1802")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1802]
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$133, DW_AT_external
	.global	_mms_dict_obj1802_Transmission_Type
_mms_dict_obj1802_Transmission_Type:	.usect	".ebss",1,1,0
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1802_Transmission_Type")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_mms_dict_obj1802_Transmission_Type")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_addr _mms_dict_obj1802_Transmission_Type]
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$134, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1803
_mms_dict_highestSubIndex_obj1803:	.usect	".ebss",1,1,0
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1803")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1803")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1803]
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$135, DW_AT_external
	.global	_mms_dict_obj1803_Transmission_Type
_mms_dict_obj1803_Transmission_Type:	.usect	".ebss",1,1,0
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1803_Transmission_Type")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_mms_dict_obj1803_Transmission_Type")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_addr _mms_dict_obj1803_Transmission_Type]
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$136, DW_AT_external
	.global	_mms_dict_obj1802_Event_Timer
_mms_dict_obj1802_Event_Timer:	.usect	".ebss",1,1,0
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1802_Event_Timer")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_mms_dict_obj1802_Event_Timer")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_addr _mms_dict_obj1802_Event_Timer]
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$137, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj6411
_mms_dict_highestSubIndex_obj6411:	.usect	".ebss",1,1,0
$C$DW$138	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj6411")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj6411")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj6411]
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$138, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj1801
_mms_dict_highestSubIndex_obj1801:	.usect	".ebss",1,1,0
$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj1801")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj1801")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj1801]
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$139, DW_AT_external
	.global	_mms_dict_obj1801_Transmission_Type
_mms_dict_obj1801_Transmission_Type:	.usect	".ebss",1,1,0
$C$DW$140	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1801_Transmission_Type")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_mms_dict_obj1801_Transmission_Type")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_addr _mms_dict_obj1801_Transmission_Type]
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$140, DW_AT_external
	.global	_mms_dict_obj1400_Inhibit_Time
_mms_dict_obj1400_Inhibit_Time:	.usect	".ebss",1,1,0
$C$DW$141	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1400_Inhibit_Time")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_mms_dict_obj1400_Inhibit_Time")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_addr _mms_dict_obj1400_Inhibit_Time]
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$141, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj6430
_mms_dict_highestSubIndex_obj6430:	.usect	".ebss",1,1,0
$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj6430")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj6430")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj6430]
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$142, DW_AT_external
	.global	_mms_dict_obj1801_Event_Timer
_mms_dict_obj1801_Event_Timer:	.usect	".ebss",1,1,0
$C$DW$143	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1801_Event_Timer")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_mms_dict_obj1801_Event_Timer")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_addr _mms_dict_obj1801_Event_Timer]
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$143, DW_AT_external
	.global	_mms_dict_highestSubIndex_obj642F
_mms_dict_highestSubIndex_obj642F:	.usect	".ebss",1,1,0
$C$DW$144	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_highestSubIndex_obj642F")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_mms_dict_highestSubIndex_obj642F")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_addr _mms_dict_highestSubIndex_obj642F]
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$144, DW_AT_external
	.global	_mms_dict_obj1801_Inhibit_Time
_mms_dict_obj1801_Inhibit_Time:	.usect	".ebss",1,1,0
$C$DW$145	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1801_Inhibit_Time")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_mms_dict_obj1801_Inhibit_Time")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_addr _mms_dict_obj1801_Inhibit_Time]
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$145, DW_AT_external
	.global	_mms_dict_obj1801_Compatibility_Entry
_mms_dict_obj1801_Compatibility_Entry:	.usect	".ebss",1,1,0
$C$DW$146	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1801_Compatibility_Entry")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_mms_dict_obj1801_Compatibility_Entry")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_addr _mms_dict_obj1801_Compatibility_Entry]
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$146, DW_AT_external
	.global	_ODP_SafetyLimits_Tmax
_ODP_SafetyLimits_Tmax:	.usect	".ebss",1,1,0
$C$DW$147	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Tmax]
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$147, DW_AT_external
	.global	_ODV_SciSend
_ODV_SciSend:	.usect	".ebss",1,1,0
$C$DW$148	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SciSend")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_ODV_SciSend")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_addr _ODV_SciSend]
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$148, DW_AT_external
	.global	_ODP_SafetyLimits_T3_max
_ODP_SafetyLimits_T3_max:	.usect	".ebss",1,1,0
$C$DW$149	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_T3_max")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_ODP_SafetyLimits_T3_max")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_T3_max]
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$149, DW_AT_external
	.global	_ODP_SafetyLimits_Voltage_delay
_ODP_SafetyLimits_Voltage_delay:	.usect	".ebss",1,1,0
$C$DW$150	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Voltage_delay]
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$150, DW_AT_external
	.global	_ODP_SafetyLimits_T3_min
_ODP_SafetyLimits_T3_min:	.usect	".ebss",1,1,0
$C$DW$151	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_T3_min")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_ODP_SafetyLimits_T3_min")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_T3_min]
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$151, DW_AT_external
	.global	_mms_dict_ObjdictSize
	.sect	".econst"
	.align	1
_mms_dict_ObjdictSize:
	.bits	71,16			; _mms_dict_ObjdictSize @ 0

$C$DW$152	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_ObjdictSize")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_mms_dict_ObjdictSize")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_addr _mms_dict_ObjdictSize]
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$152, DW_AT_external
	.global	_ODP_SafetyLimits_Imax_charge
_ODP_SafetyLimits_Imax_charge:	.usect	".ebss",1,1,0
$C$DW$153	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Imax_charge")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Imax_charge")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Imax_charge]
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$153, DW_AT_external
	.global	_ODP_SafetyLimits_Tmin
_ODP_SafetyLimits_Tmin:	.usect	".ebss",1,1,0
$C$DW$154	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Tmin]
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$154, DW_AT_external
	.global	_ODV_ErrorDsp_ErrorLevel
_ODV_ErrorDsp_ErrorLevel:	.usect	".ebss",1,1,0
$C$DW$155	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorLevel")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorLevel")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_addr _ODV_ErrorDsp_ErrorLevel]
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$155, DW_AT_external
	.global	_ODP_SafetyLimits_Cell_Nb
_ODP_SafetyLimits_Cell_Nb:	.usect	".ebss",1,1,0
$C$DW$156	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Cell_Nb")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Cell_Nb")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Cell_Nb]
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$156, DW_AT_external
	.global	_ODP_SafetyLimits_Current_delay
_ODP_SafetyLimits_Current_delay:	.usect	".ebss",1,1,0
$C$DW$157	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Current_delay]
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$157, DW_AT_external
	.global	_ODP_SafetyLimits_Low_Voltage_Current_Allow
_ODP_SafetyLimits_Low_Voltage_Current_Allow:	.usect	".ebss",1,1,0
$C$DW$158	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Low_Voltage_Current_Allow")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Low_Voltage_Current_Allow")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Low_Voltage_Current_Allow]
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$158, DW_AT_external
	.global	_ODP_SafetyLimits_Low_Voltage_Current_Delay
_ODP_SafetyLimits_Low_Voltage_Current_Delay:	.usect	".ebss",1,1,0
$C$DW$159	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Low_Voltage_Current_Delay")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Low_Voltage_Current_Delay")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Low_Voltage_Current_Delay]
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$159, DW_AT_external
	.global	_ODP_SafetyLimits_Low_Voltage_Charge_Delay
_ODP_SafetyLimits_Low_Voltage_Charge_Delay:	.usect	".ebss",1,1,0
$C$DW$160	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Low_Voltage_Charge_Delay")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Low_Voltage_Charge_Delay")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Low_Voltage_Charge_Delay]
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$160, DW_AT_external
	.global	_ODP_SafetyLimits_Umax
_ODP_SafetyLimits_Umax:	.usect	".ebss",1,1,0
$C$DW$161	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Umax]
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$161, DW_AT_external
	.global	_ODP_SafetyLimits_Umin
_ODP_SafetyLimits_Umin:	.usect	".ebss",1,1,0
$C$DW$162	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Umin]
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$162, DW_AT_external
	.global	_ODP_SafetyLimits_SleepTimeout
_ODP_SafetyLimits_SleepTimeout:	.usect	".ebss",1,1,0
$C$DW$163	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_SleepTimeout")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_ODP_SafetyLimits_SleepTimeout")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_SleepTimeout]
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$163, DW_AT_external
	.global	_ODP_SafetyLimits_UnderCurrent
_ODP_SafetyLimits_UnderCurrent:	.usect	".ebss",1,1,0
$C$DW$164	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_UnderCurrent]
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$164, DW_AT_external
	.global	_ODP_SafetyLimits_BalancingTimeout
_ODP_SafetyLimits_BalancingTimeout:	.usect	".ebss",1,1,0
$C$DW$165	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_BalancingTimeout")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_ODP_SafetyLimits_BalancingTimeout")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_BalancingTimeout]
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$165, DW_AT_external
	.global	_ODP_SafetyLimits_Temp_Delay
_ODP_SafetyLimits_Temp_Delay:	.usect	".ebss",1,1,0
$C$DW$166	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Temp_Delay")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Temp_Delay")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Temp_Delay]
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$166, DW_AT_external
	.global	_ODP_SafetyLimits_DamagedVoltage
_ODP_SafetyLimits_DamagedVoltage:	.usect	".ebss",1,1,0
$C$DW$167	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_DamagedVoltage")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_ODP_SafetyLimits_DamagedVoltage")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_DamagedVoltage]
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$167, DW_AT_external
	.global	_ODP_VersionParameters
_ODP_VersionParameters:	.usect	".ebss",1,1,0
$C$DW$168	.dwtag  DW_TAG_variable, DW_AT_name("ODP_VersionParameters")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_ODP_VersionParameters")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_addr _ODP_VersionParameters]
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$168, DW_AT_external
	.global	_ODV_Recorder_TriggerIndex
_ODV_Recorder_TriggerIndex:	.usect	".ebss",1,1,0
$C$DW$169	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_TriggerIndex")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_ODV_Recorder_TriggerIndex")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_addr _ODV_Recorder_TriggerIndex]
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$169, DW_AT_external
	.global	_ODP_SafetyLimits_OverVoltage
_ODP_SafetyLimits_OverVoltage:	.usect	".ebss",1,1,0
$C$DW$170	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_OverVoltage]
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$170, DW_AT_external
	.global	_ODP_SafetyLimits_UnderVoltage
_ODP_SafetyLimits_UnderVoltage:	.usect	".ebss",1,1,0
$C$DW$171	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_UnderVoltage]
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$171, DW_AT_external
	.global	_ODP_SafetyLimits_Charge_In_Thres_Cur
_ODP_SafetyLimits_Charge_In_Thres_Cur:	.usect	".ebss",1,1,0
$C$DW$172	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Charge_In_Thres_Cur]
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$172, DW_AT_external
	.global	_ODV_Recorder_Start
_ODV_Recorder_Start:	.usect	".ebss",1,1,0
$C$DW$173	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Start")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_ODV_Recorder_Start")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_addr _ODV_Recorder_Start]
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$173, DW_AT_external
	.global	_ODP_SafetyLimits_Overcurrent
_ODP_SafetyLimits_Overcurrent:	.usect	".ebss",1,1,0
$C$DW$174	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Overcurrent]
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$174, DW_AT_external
	.global	_ODV_Recorder_Variables
_ODV_Recorder_Variables:	.usect	".ebss",1,1,0
$C$DW$175	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Variables")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_ODV_Recorder_Variables")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_addr _ODV_Recorder_Variables]
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$175, DW_AT_external
	.global	_ODV_Recorder_ReadIndex
_ODV_Recorder_ReadIndex:	.usect	".ebss",1,1,0
$C$DW$176	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_ReadIndex")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_ODV_Recorder_ReadIndex")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_addr _ODV_Recorder_ReadIndex]
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$176, DW_AT_external
	.global	_ODV_Recorder_Multiunits
_ODV_Recorder_Multiunits:	.usect	".ebss",1,1,0
$C$DW$177	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Multiunits")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_ODV_Recorder_Multiunits")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_addr _ODV_Recorder_Multiunits]
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$177, DW_AT_external
	.global	_ODV_Recorder_PreTrigger
_ODV_Recorder_PreTrigger:	.usect	".ebss",1,1,0
$C$DW$178	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_PreTrigger")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_ODV_Recorder_PreTrigger")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_addr _ODV_Recorder_PreTrigger]
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$178, DW_AT_external
	.global	_ODP_SafetyLimits_Mosfet_Tmin
_ODP_SafetyLimits_Mosfet_Tmin:	.usect	".ebss",1,1,0
$C$DW$179	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Mosfet_Tmin")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Mosfet_Tmin")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Mosfet_Tmin]
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$179, DW_AT_external
	.global	_ODP_SafetyLimits_Mosfet_Tmax
_ODP_SafetyLimits_Mosfet_Tmax:	.usect	".ebss",1,1,0
$C$DW$180	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Mosfet_Tmax")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Mosfet_Tmax")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Mosfet_Tmax]
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$180, DW_AT_external
	.global	_ODV_Recorder_Control
_ODV_Recorder_Control:	.usect	".ebss",1,1,0
$C$DW$181	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Control")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_ODV_Recorder_Control")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_addr _ODV_Recorder_Control]
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$181, DW_AT_external
	.global	_ODP_SafetyLimits_Umax_bal_delta
_ODP_SafetyLimits_Umax_bal_delta:	.usect	".ebss",1,1,0
$C$DW$182	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Umax_bal_delta]
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$182, DW_AT_external
	.global	_ODV_Recorder_Size
_ODV_Recorder_Size:	.usect	".ebss",1,1,0
$C$DW$183	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Size")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_ODV_Recorder_Size")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_addr _ODV_Recorder_Size]
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$183, DW_AT_external
	.global	_ODP_SafetyLimits_Resistor_Tmax
_ODP_SafetyLimits_Resistor_Tmax:	.usect	".ebss",1,1,0
$C$DW$184	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Resistor_Tmax]
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$184, DW_AT_external
	.global	_ODP_SafetyLimits_Umin_bal_delta
_ODP_SafetyLimits_Umin_bal_delta:	.usect	".ebss",1,1,0
$C$DW$185	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Umin_bal_delta]
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$185, DW_AT_external
	.global	_ODP_SafetyLimits_Resistor_Tmin
_ODP_SafetyLimits_Resistor_Tmin:	.usect	".ebss",1,1,0
$C$DW$186	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Resistor_Tmin]
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$186, DW_AT_external
	.global	_ODP_SafetyLimits_Imax_dis
_ODP_SafetyLimits_Imax_dis:	.usect	".ebss",1,1,0
$C$DW$187	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Imax_dis")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Imax_dis")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Imax_dis]
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$187, DW_AT_external
	.global	_ODP_SafetyLimits_Resistor_Delay
_ODP_SafetyLimits_Resistor_Delay:	.usect	".ebss",1,1,0
$C$DW$188	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Delay")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Delay")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_addr _ODP_SafetyLimits_Resistor_Delay]
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$188, DW_AT_external
	.global	_ODP_Heater_OffTemp
_ODP_Heater_OffTemp:	.usect	".ebss",1,1,0
$C$DW$189	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Heater_OffTemp")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_ODP_Heater_OffTemp")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_addr _ODP_Heater_OffTemp]
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$189, DW_AT_external
	.global	_ODP_Heater_OnTemp
_ODP_Heater_OnTemp:	.usect	".ebss",1,1,0
$C$DW$190	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Heater_OnTemp")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_ODP_Heater_OnTemp")
	.dwattr $C$DW$190, DW_AT_location[DW_OP_addr _ODP_Heater_OnTemp]
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$190, DW_AT_external
	.global	_ODP_Heater_Off_Cell_Voltage
_ODP_Heater_Off_Cell_Voltage:	.usect	".ebss",1,1,0
$C$DW$191	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Heater_Off_Cell_Voltage")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_ODP_Heater_Off_Cell_Voltage")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_addr _ODP_Heater_Off_Cell_Voltage]
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$191, DW_AT_external
	.global	_ODV_Gateway_Status
_ODV_Gateway_Status:	.usect	".ebss",1,1,0
$C$DW$192	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Status")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_ODV_Gateway_Status")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_addr _ODV_Gateway_Status]
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$192, DW_AT_external
	.global	_ODV_Gateway_SOC
_ODV_Gateway_SOC:	.usect	".ebss",1,1,0
$C$DW$193	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_SOC")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_ODV_Gateway_SOC")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_addr _ODV_Gateway_SOC]
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$193, DW_AT_external
	.global	_ODV_Heater_Heater_Status
_ODV_Heater_Heater_Status:	.usect	".ebss",1,1,0
$C$DW$194	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Heater_Heater_Status")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_ODV_Heater_Heater_Status")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_addr _ODV_Heater_Heater_Status]
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$194, DW_AT_external
	.global	_ODP_Contactor_Slope
_ODP_Contactor_Slope:	.usect	".ebss",1,1,0
$C$DW$195	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Slope")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_ODP_Contactor_Slope")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_addr _ODP_Contactor_Slope]
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$195, DW_AT_external
	.global	_ODP_Contactor_Overcurrent
_ODP_Contactor_Overcurrent:	.usect	".ebss",1,1,0
$C$DW$196	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Overcurrent")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_ODP_Contactor_Overcurrent")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_addr _ODP_Contactor_Overcurrent]
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$196, DW_AT_external
	.global	_ODV_Gateway_Temperature
_ODV_Gateway_Temperature:	.usect	".ebss",1,1,0
$C$DW$197	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Temperature")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_ODV_Gateway_Temperature")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_addr _ODV_Gateway_Temperature]
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$197, DW_AT_external
	.global	_ODP_Contactor_Delay
_ODP_Contactor_Delay:	.usect	".ebss",1,1,0
$C$DW$198	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Delay")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_ODP_Contactor_Delay")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_addr _ODP_Contactor_Delay]
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$198, DW_AT_external
	.global	_ODP_Contactor_Undercurrent
_ODP_Contactor_Undercurrent:	.usect	".ebss",1,1,0
$C$DW$199	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Undercurrent")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_ODP_Contactor_Undercurrent")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_addr _ODP_Contactor_Undercurrent]
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$199, DW_AT_external
	.global	_ODP_Unused_Undefined5
_ODP_Unused_Undefined5:	.usect	".ebss",1,1,0
$C$DW$200	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Unused_Undefined5")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_ODP_Unused_Undefined5")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_addr _ODP_Unused_Undefined5]
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$200, DW_AT_external
	.global	_ODP_Unused_Undefined4
_ODP_Unused_Undefined4:	.usect	".ebss",1,1,0
$C$DW$201	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Unused_Undefined4")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_ODP_Unused_Undefined4")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_addr _ODP_Unused_Undefined4]
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$201, DW_AT_external
	.global	_ODP_Unused_Undefined6
_ODP_Unused_Undefined6:	.usect	".ebss",1,1,0
$C$DW$202	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Unused_Undefined6")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_ODP_Unused_Undefined6")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_addr _ODP_Unused_Undefined6]
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$202, DW_AT_external
	.global	_ODP_Unused_Undefined8
_ODP_Unused_Undefined8:	.usect	".ebss",1,1,0
$C$DW$203	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Unused_Undefined8")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_ODP_Unused_Undefined8")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_addr _ODP_Unused_Undefined8]
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$203, DW_AT_external
	.global	_ODP_Unused_Undefined7
_ODP_Unused_Undefined7:	.usect	".ebss",1,1,0
$C$DW$204	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Unused_Undefined7")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_ODP_Unused_Undefined7")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_addr _ODP_Unused_Undefined7]
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$204, DW_AT_external
	.global	_ODV_Master_Module1_Voltage
_ODV_Master_Module1_Voltage:	.usect	".ebss",1,1,0
$C$DW$205	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Master_Module1_Voltage")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_ODV_Master_Module1_Voltage")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_addr _ODV_Master_Module1_Voltage]
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$205, DW_AT_external
	.global	_ODP_Unused_Undefined1
_ODP_Unused_Undefined1:	.usect	".ebss",1,1,0
$C$DW$206	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Unused_Undefined1")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_ODP_Unused_Undefined1")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_addr _ODP_Unused_Undefined1]
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$206, DW_AT_external
	.global	_ODV_Contactor_Undefined
_ODV_Contactor_Undefined:	.usect	".ebss",1,1,0
$C$DW$207	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Contactor_Undefined")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_ODV_Contactor_Undefined")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_addr _ODV_Contactor_Undefined]
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$207, DW_AT_external
	.global	_ODP_Unused_Undefined2
_ODP_Unused_Undefined2:	.usect	".ebss",1,1,0
$C$DW$208	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Unused_Undefined2")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_ODP_Unused_Undefined2")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_addr _ODP_Unused_Undefined2]
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$208, DW_AT_external
	.global	_ODV_Gateway_State
_ODV_Gateway_State:	.usect	".ebss",1,1,0
$C$DW$209	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_State")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_ODV_Gateway_State")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_addr _ODV_Gateway_State]
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$209, DW_AT_external
	.global	_ODV_Gateway_SOH
_ODV_Gateway_SOH:	.usect	".ebss",1,1,0
$C$DW$210	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_SOH")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_ODV_Gateway_SOH")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_addr _ODV_Gateway_SOH]
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$210, DW_AT_external
	.global	_ODV_Current_Min
_ODV_Current_Min:	.usect	".ebss",1,1,0
$C$DW$211	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_Min")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_ODV_Current_Min")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_addr _ODV_Current_Min]
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$211, DW_AT_external
	.global	_ODV_Current_Max
_ODV_Current_Max:	.usect	".ebss",1,1,0
$C$DW$212	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_Max")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_ODV_Current_Max")
	.dwattr $C$DW$212, DW_AT_location[DW_OP_addr _ODV_Current_Max]
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$212, DW_AT_external
	.global	_ODV_Voltage_Min
_ODV_Voltage_Min:	.usect	".ebss",1,1,0
$C$DW$213	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Voltage_Min")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_ODV_Voltage_Min")
	.dwattr $C$DW$213, DW_AT_location[DW_OP_addr _ODV_Voltage_Min]
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$213, DW_AT_external
	.global	_ODV_Current_DischargeAllowed
_ODV_Current_DischargeAllowed:	.usect	".ebss",1,1,0
$C$DW$214	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_DischargeAllowed")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_ODV_Current_DischargeAllowed")
	.dwattr $C$DW$214, DW_AT_location[DW_OP_addr _ODV_Current_DischargeAllowed]
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$214, DW_AT_external
	.global	_ODV_Gateway_MaxCellVoltage
_ODV_Gateway_MaxCellVoltage:	.usect	".ebss",1,1,0
$C$DW$215	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxCellVoltage")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_ODV_Gateway_MaxCellVoltage")
	.dwattr $C$DW$215, DW_AT_location[DW_OP_addr _ODV_Gateway_MaxCellVoltage]
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$215, DW_AT_external
	.global	_ODV_Voltage_Max
_ODV_Voltage_Max:	.usect	".ebss",1,1,0
$C$DW$216	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Voltage_Max")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_ODV_Voltage_Max")
	.dwattr $C$DW$216, DW_AT_location[DW_OP_addr _ODV_Voltage_Max]
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$216, DW_AT_external
	.global	_ODV_MachineState
_ODV_MachineState:	.usect	".ebss",1,1,0
$C$DW$217	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineState")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_ODV_MachineState")
	.dwattr $C$DW$217, DW_AT_location[DW_OP_addr _ODV_MachineState]
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$217, DW_AT_external
	.global	_ODV_Temperature_Min
_ODV_Temperature_Min:	.usect	".ebss",1,1,0
$C$DW$218	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Temperature_Min")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_ODV_Temperature_Min")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_addr _ODV_Temperature_Min]
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$218, DW_AT_external
	.global	_ODV_Gateway_Voltage
_ODV_Gateway_Voltage:	.usect	".ebss",1,1,0
$C$DW$219	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Voltage")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_ODV_Gateway_Voltage")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_addr _ODV_Gateway_Voltage]
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$219, DW_AT_external
	.global	_ODV_Temperature_Average_data
_ODV_Temperature_Average_data:	.usect	".ebss",1,1,0
$C$DW$220	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Temperature_Average_data")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_ODV_Temperature_Average_data")
	.dwattr $C$DW$220, DW_AT_location[DW_OP_addr _ODV_Temperature_Average_data]
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$220, DW_AT_external
	.global	_ODV_Current_ChargeAllowed
_ODV_Current_ChargeAllowed:	.usect	".ebss",1,1,0
$C$DW$221	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_ChargeAllowed")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_ODV_Current_ChargeAllowed")
	.dwattr $C$DW$221, DW_AT_location[DW_OP_addr _ODV_Current_ChargeAllowed]
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$221, DW_AT_external
	.global	_ODV_Gateway_MaxDeltaCellVoltage
_ODV_Gateway_MaxDeltaCellVoltage:	.usect	".ebss",1,1,0
$C$DW$222	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxDeltaCellVoltage")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_ODV_Gateway_MaxDeltaCellVoltage")
	.dwattr $C$DW$222, DW_AT_location[DW_OP_addr _ODV_Gateway_MaxDeltaCellVoltage]
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$222, DW_AT_external
	.global	_ODP_Contactor_Setup
_ODP_Contactor_Setup:	.usect	".ebss",1,1,0
$C$DW$223	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Setup")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_ODP_Contactor_Setup")
	.dwattr $C$DW$223, DW_AT_location[DW_OP_addr _ODP_Contactor_Setup]
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$223, DW_AT_external
	.global	_ODP_Current_C_D_Mode
_ODP_Current_C_D_Mode:	.usect	".ebss",1,1,0
$C$DW$224	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Current_C_D_Mode")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_ODP_Current_C_D_Mode")
	.dwattr $C$DW$224, DW_AT_location[DW_OP_addr _ODP_Current_C_D_Mode]
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$224, DW_AT_external
	.global	_ODP_Contactor_Hold
_ODP_Contactor_Hold:	.usect	".ebss",1,1,0
$C$DW$225	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Hold")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_ODP_Contactor_Hold")
	.dwattr $C$DW$225, DW_AT_location[DW_OP_addr _ODP_Contactor_Hold]
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$225, DW_AT_external
	.global	_ODV_Gateway_LogNB
_ODV_Gateway_LogNB:	.usect	".ebss",1,1,0
$C$DW$226	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_LogNB")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_ODV_Gateway_LogNB")
	.dwattr $C$DW$226, DW_AT_location[DW_OP_addr _ODV_Gateway_LogNB]
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$226, DW_AT_external
	.global	_ODV_Temperature_Max
_ODV_Temperature_Max:	.usect	".ebss",1,1,0
$C$DW$227	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Temperature_Max")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_ODV_Temperature_Max")
	.dwattr $C$DW$227, DW_AT_location[DW_OP_addr _ODV_Temperature_Max]
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$227, DW_AT_external
	.global	_ODV_Gateway_MinCellVoltage
_ODV_Gateway_MinCellVoltage:	.usect	".ebss",1,1,0
$C$DW$228	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MinCellVoltage")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_ODV_Gateway_MinCellVoltage")
	.dwattr $C$DW$228, DW_AT_location[DW_OP_addr _ODV_Gateway_MinCellVoltage]
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$228, DW_AT_external
	.global	_ODP_Unused_Undefined3
_ODP_Unused_Undefined3:	.usect	".ebss",1,1,0
$C$DW$229	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Unused_Undefined3")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_ODP_Unused_Undefined3")
	.dwattr $C$DW$229, DW_AT_location[DW_OP_addr _ODP_Unused_Undefined3]
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$229, DW_AT_external
	.global	_ODV_Gateway_Current
_ODV_Gateway_Current:	.usect	".ebss",1,1,0
$C$DW$230	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Current")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_ODV_Gateway_Current")
	.dwattr $C$DW$230, DW_AT_location[DW_OP_addr _ODV_Gateway_Current]
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$230, DW_AT_external
	.global	_mms_dict_obj1802_COB_ID_used_by_PDO
_mms_dict_obj1802_COB_ID_used_by_PDO:	.usect	".ebss",2,1,1
$C$DW$231	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1802_COB_ID_used_by_PDO")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_mms_dict_obj1802_COB_ID_used_by_PDO")
	.dwattr $C$DW$231, DW_AT_location[DW_OP_addr _mms_dict_obj1802_COB_ID_used_by_PDO]
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$231, DW_AT_external
	.global	_ODV_ErrorDsp_WarningNumber
_ODV_ErrorDsp_WarningNumber:	.usect	".ebss",2,1,1
$C$DW$232	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_WarningNumber")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_ODV_ErrorDsp_WarningNumber")
	.dwattr $C$DW$232, DW_AT_location[DW_OP_addr _ODV_ErrorDsp_WarningNumber]
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$232, DW_AT_external
	.global	_mms_dict_obj1801_COB_ID_used_by_PDO
_mms_dict_obj1801_COB_ID_used_by_PDO:	.usect	".ebss",2,1,1
$C$DW$233	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1801_COB_ID_used_by_PDO")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_mms_dict_obj1801_COB_ID_used_by_PDO")
	.dwattr $C$DW$233, DW_AT_location[DW_OP_addr _mms_dict_obj1801_COB_ID_used_by_PDO]
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$233, DW_AT_external
	.global	_ODV_Recorder_TriggerLevel
_ODV_Recorder_TriggerLevel:	.usect	".ebss",2,1,1
$C$DW$234	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_TriggerLevel")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_ODV_Recorder_TriggerLevel")
	.dwattr $C$DW$234, DW_AT_location[DW_OP_addr _ODV_Recorder_TriggerLevel]
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$234, DW_AT_external
	.global	_ODV_ErrorDsp_ErrorNumber
_ODV_ErrorDsp_ErrorNumber:	.usect	".ebss",2,1,1
$C$DW$235	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_addr _ODV_ErrorDsp_ErrorNumber]
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$235, DW_AT_external
	.global	_mms_dict_obj1803_COB_ID_used_by_PDO
_mms_dict_obj1803_COB_ID_used_by_PDO:	.usect	".ebss",2,1,1
$C$DW$236	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1803_COB_ID_used_by_PDO")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_mms_dict_obj1803_COB_ID_used_by_PDO")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_addr _mms_dict_obj1803_COB_ID_used_by_PDO]
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$236, DW_AT_external

$C$DW$237	.dwtag  DW_TAG_subprogram, DW_AT_name("ConfigCallback")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_ConfigCallback")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$237, DW_AT_declaration
	.dwattr $C$DW$237, DW_AT_external
$C$DW$238	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$58)
$C$DW$239	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$39)
$C$DW$240	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$6)
$C$DW$241	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$237


$C$DW$242	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteTextCallback")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_WriteTextCallback")
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$242, DW_AT_declaration
	.dwattr $C$DW$242, DW_AT_external
$C$DW$243	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$58)
$C$DW$244	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$39)
$C$DW$245	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$6)
$C$DW$246	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$242


$C$DW$247	.dwtag  DW_TAG_subprogram, DW_AT_name("LogNBCallback")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_LogNBCallback")
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$247, DW_AT_declaration
	.dwattr $C$DW$247, DW_AT_external
$C$DW$248	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$58)
$C$DW$249	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$39)
$C$DW$250	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$6)
$C$DW$251	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$247


$C$DW$252	.dwtag  DW_TAG_subprogram, DW_AT_name("CommErrorSetCallback")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_CommErrorSetCallback")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$252, DW_AT_declaration
	.dwattr $C$DW$252, DW_AT_external
$C$DW$253	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$58)
$C$DW$254	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$39)
$C$DW$255	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$6)
$C$DW$256	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$252

	.global	_ODC_StoreParameters_callbacks
	.sect	".econst:_ODC_StoreParameters_callbacks"
	.clink
	.align	2
_ODC_StoreParameters_callbacks:
	.bits	_SaveAllParameters,32		; _ODC_StoreParameters_callbacks[0] @ 0

$C$DW$257	.dwtag  DW_TAG_variable, DW_AT_name("ODC_StoreParameters_callbacks")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_ODC_StoreParameters_callbacks")
	.dwattr $C$DW$257, DW_AT_location[DW_OP_addr _ODC_StoreParameters_callbacks]
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$257, DW_AT_external

$C$DW$258	.dwtag  DW_TAG_subprogram, DW_AT_name("BatteryCallBack")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_BatteryCallBack")
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$258, DW_AT_declaration
	.dwattr $C$DW$258, DW_AT_external
$C$DW$259	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$58)
$C$DW$260	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$39)
$C$DW$261	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$6)
$C$DW$262	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$258


$C$DW$263	.dwtag  DW_TAG_subprogram, DW_AT_name("VersionCallback")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_VersionCallback")
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$263, DW_AT_declaration
	.dwattr $C$DW$263, DW_AT_external
$C$DW$264	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$58)
$C$DW$265	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$39)
$C$DW$266	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$6)
$C$DW$267	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$263


$C$DW$268	.dwtag  DW_TAG_subprogram, DW_AT_name("SecurityCallBack")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_SecurityCallBack")
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$268, DW_AT_declaration
	.dwattr $C$DW$268, DW_AT_external
$C$DW$269	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$58)
$C$DW$270	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$39)
$C$DW$271	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$6)
$C$DW$272	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$268


$C$DW$273	.dwtag  DW_TAG_subprogram, DW_AT_name("MultiunitsCallback")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_MultiunitsCallback")
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$273, DW_AT_declaration
	.dwattr $C$DW$273, DW_AT_external
$C$DW$274	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$58)
$C$DW$275	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$39)
$C$DW$276	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$6)
$C$DW$277	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$273

	.global	_ODC_Security_callbacks
	.sect	".econst:_ODC_Security_callbacks"
	.clink
	.align	2
_ODC_Security_callbacks:
	.bits	_SecurityCallBack,32		; _ODC_Security_callbacks[0] @ 0

$C$DW$278	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Security_callbacks")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_ODC_Security_callbacks")
	.dwattr $C$DW$278, DW_AT_location[DW_OP_addr _ODC_Security_callbacks]
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$278, DW_AT_external

$C$DW$279	.dwtag  DW_TAG_subprogram, DW_AT_name("_storeODSubIndex")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("__storeODSubIndex")
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$279, DW_AT_declaration
	.dwattr $C$DW$279, DW_AT_external
$C$DW$280	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$58)
$C$DW$281	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$9)
$C$DW$282	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$279


$C$DW$283	.dwtag  DW_TAG_subprogram, DW_AT_name("ClearErrorCallback")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_ClearErrorCallback")
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$283, DW_AT_declaration
	.dwattr $C$DW$283, DW_AT_external
$C$DW$284	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$58)
$C$DW$285	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$39)
$C$DW$286	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$6)
$C$DW$287	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$283


$C$DW$288	.dwtag  DW_TAG_subprogram, DW_AT_name("SciSendCallback")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_SciSendCallback")
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$288, DW_AT_declaration
	.dwattr $C$DW$288, DW_AT_external
$C$DW$289	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$58)
$C$DW$290	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$39)
$C$DW$291	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$6)
$C$DW$292	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$288


$C$DW$293	.dwtag  DW_TAG_subprogram, DW_AT_name("VariablesCallback")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_VariablesCallback")
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$293, DW_AT_declaration
	.dwattr $C$DW$293, DW_AT_external
$C$DW$294	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$58)
$C$DW$295	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$39)
$C$DW$296	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$6)
$C$DW$297	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$293


$C$DW$298	.dwtag  DW_TAG_subprogram, DW_AT_name("StartRecorderCallBack")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_StartRecorderCallBack")
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$298, DW_AT_declaration
	.dwattr $C$DW$298, DW_AT_external
$C$DW$299	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$58)
$C$DW$300	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$39)
$C$DW$301	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$6)
$C$DW$302	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$298

	.global	_ODC_Version_callbacks
	.sect	".econst:_ODC_Version_callbacks"
	.clink
	.align	2
_ODC_Version_callbacks:
	.bits	_VersionCallback,32		; _ODC_Version_callbacks[0] @ 0

$C$DW$303	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Version_callbacks")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_ODC_Version_callbacks")
	.dwattr $C$DW$303, DW_AT_location[DW_OP_addr _ODC_Version_callbacks]
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$303, DW_AT_external
	.global	_ODP_OnTime
_ODP_OnTime:	.usect	".ebss",2,1,1
$C$DW$304	.dwtag  DW_TAG_variable, DW_AT_name("ODP_OnTime")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_ODP_OnTime")
	.dwattr $C$DW$304, DW_AT_location[DW_OP_addr _ODP_OnTime]
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$304, DW_AT_external

$C$DW$305	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteAnalogueOutputsCallback")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_WriteAnalogueOutputsCallback")
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$305, DW_AT_declaration
	.dwattr $C$DW$305, DW_AT_external
$C$DW$306	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$58)
$C$DW$307	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$39)
$C$DW$308	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$6)
$C$DW$309	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$305


$C$DW$310	.dwtag  DW_TAG_subprogram, DW_AT_name("ControlWordCallBack")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_ControlWordCallBack")
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$310, DW_AT_declaration
	.dwattr $C$DW$310, DW_AT_external
$C$DW$311	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$58)
$C$DW$312	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$39)
$C$DW$313	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$6)
$C$DW$314	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$310

	.global	_ODV_Recorder_Period
_ODV_Recorder_Period:	.usect	".ebss",2,1,1
$C$DW$315	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Period")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_ODV_Recorder_Period")
	.dwattr $C$DW$315, DW_AT_location[DW_OP_addr _ODV_Recorder_Period]
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$315, DW_AT_external
	.global	_ODV_Recorder_NbOfSamples
_ODV_Recorder_NbOfSamples:	.usect	".ebss",2,1,1
$C$DW$316	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_NbOfSamples")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_ODV_Recorder_NbOfSamples")
	.dwattr $C$DW$316, DW_AT_location[DW_OP_addr _ODV_Recorder_NbOfSamples]
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$316, DW_AT_external
	.global	_ODP_Password
_ODP_Password:	.usect	".ebss",2,1,1
$C$DW$317	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Password")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_ODP_Password")
	.dwattr $C$DW$317, DW_AT_location[DW_OP_addr _ODP_Password]
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$317, DW_AT_external
	.global	_ODP_CrcParameters
_ODP_CrcParameters:	.usect	".ebss",2,1,1
$C$DW$318	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CrcParameters")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_ODP_CrcParameters")
	.dwattr $C$DW$318, DW_AT_location[DW_OP_addr _ODP_CrcParameters]
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$318, DW_AT_external

$C$DW$319	.dwtag  DW_TAG_subprogram, DW_AT_name("ResetCallBack")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_ResetCallBack")
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$319, DW_AT_declaration
	.dwattr $C$DW$319, DW_AT_external
$C$DW$320	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$58)
$C$DW$321	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$39)
$C$DW$322	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$6)
$C$DW$323	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$319

	.global	_ODC_ResetHW_callbacks
	.sect	".econst:_ODC_ResetHW_callbacks"
	.clink
	.align	2
_ODC_ResetHW_callbacks:
	.bits	_ResetCallBack,32		; _ODC_ResetHW_callbacks[0] @ 0

$C$DW$324	.dwtag  DW_TAG_variable, DW_AT_name("ODC_ResetHW_callbacks")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_ODC_ResetHW_callbacks")
	.dwattr $C$DW$324, DW_AT_location[DW_OP_addr _ODC_ResetHW_callbacks]
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$324, DW_AT_external
	.global	_ODC_RestoreDefaultParameters_callbacks
	.sect	".econst:_ODC_RestoreDefaultParameters_callbacks"
	.clink
	.align	2
_ODC_RestoreDefaultParameters_callbacks:
	.bits	_LoadDefaultParameters,32		; _ODC_RestoreDefaultParameters_callbacks[0] @ 0

$C$DW$325	.dwtag  DW_TAG_variable, DW_AT_name("ODC_RestoreDefaultParameters_callbacks")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_ODC_RestoreDefaultParameters_callbacks")
	.dwattr $C$DW$325, DW_AT_location[DW_OP_addr _ODC_RestoreDefaultParameters_callbacks]
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$325, DW_AT_external

$C$DW$326	.dwtag  DW_TAG_subprogram, DW_AT_name("LoadDefaultParameters")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_LoadDefaultParameters")
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$326, DW_AT_declaration
	.dwattr $C$DW$326, DW_AT_external
$C$DW$327	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$58)
$C$DW$328	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$39)
$C$DW$329	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$6)
$C$DW$330	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$326


$C$DW$331	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs16BitCallback")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_WriteOutputs16BitCallback")
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$331, DW_AT_declaration
	.dwattr $C$DW$331, DW_AT_external
$C$DW$332	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$58)
$C$DW$333	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$39)
$C$DW$334	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$6)
$C$DW$335	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$331

	.global	_ODC_SciSend_callbacks
	.sect	".econst:_ODC_SciSend_callbacks"
	.clink
	.align	2
_ODC_SciSend_callbacks:
	.bits	_SciSendCallback,32		; _ODC_SciSend_callbacks[0] @ 0

$C$DW$336	.dwtag  DW_TAG_variable, DW_AT_name("ODC_SciSend_callbacks")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_ODC_SciSend_callbacks")
	.dwattr $C$DW$336, DW_AT_location[DW_OP_addr _ODC_SciSend_callbacks]
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$336, DW_AT_external

$C$DW$337	.dwtag  DW_TAG_subprogram, DW_AT_name("DebugCallBack")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_DebugCallBack")
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$337, DW_AT_declaration
	.dwattr $C$DW$337, DW_AT_external
$C$DW$338	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$58)
$C$DW$339	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$39)
$C$DW$340	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$6)
$C$DW$341	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$337

	.global	_ODC_Debug_callbacks
	.sect	".econst:_ODC_Debug_callbacks"
	.clink
	.align	2
_ODC_Debug_callbacks:
	.bits	_DebugCallBack,32		; _ODC_Debug_callbacks[0] @ 0

$C$DW$342	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Debug_callbacks")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_ODC_Debug_callbacks")
	.dwattr $C$DW$342, DW_AT_location[DW_OP_addr _ODC_Debug_callbacks]
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$342, DW_AT_external

$C$DW$343	.dwtag  DW_TAG_subprogram, DW_AT_name("SaveAllParameters")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_SaveAllParameters")
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$343, DW_AT_declaration
	.dwattr $C$DW$343, DW_AT_external
$C$DW$344	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$58)
$C$DW$345	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$39)
$C$DW$346	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$6)
$C$DW$347	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$343

	.global	_ODP_Board_RevisionNumber
_ODP_Board_RevisionNumber:	.usect	".ebss",2,1,1
$C$DW$348	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$348, DW_AT_location[DW_OP_addr _ODP_Board_RevisionNumber]
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$348, DW_AT_external
	.global	_mms_dict_obj1006
_mms_dict_obj1006:	.usect	".ebss",2,1,1
$C$DW$349	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1006")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_mms_dict_obj1006")
	.dwattr $C$DW$349, DW_AT_location[DW_OP_addr _mms_dict_obj1006]
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$349, DW_AT_external
	.global	_mms_dict_obj1005
_mms_dict_obj1005:	.usect	".ebss",2,1,1
$C$DW$350	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1005")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_mms_dict_obj1005")
	.dwattr $C$DW$350, DW_AT_location[DW_OP_addr _mms_dict_obj1005]
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$350, DW_AT_external
	.global	_ODV_StoreParameters
_ODV_StoreParameters:	.usect	".ebss",2,1,1
$C$DW$351	.dwtag  DW_TAG_variable, DW_AT_name("ODV_StoreParameters")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_ODV_StoreParameters")
	.dwattr $C$DW$351, DW_AT_location[DW_OP_addr _ODV_StoreParameters]
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$351, DW_AT_external
	.global	_mms_dict_obj1014
_mms_dict_obj1014:	.usect	".ebss",2,1,1
$C$DW$352	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1014")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_mms_dict_obj1014")
	.dwattr $C$DW$352, DW_AT_location[DW_OP_addr _mms_dict_obj1014]
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$352, DW_AT_external
	.global	_ODV_CommError_Counter
_ODV_CommError_Counter:	.usect	".ebss",2,1,1
$C$DW$353	.dwtag  DW_TAG_variable, DW_AT_name("ODV_CommError_Counter")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_ODV_CommError_Counter")
	.dwattr $C$DW$353, DW_AT_location[DW_OP_addr _ODV_CommError_Counter]
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$353, DW_AT_external
	.global	_ODV_Read_Inputs_16_Bit
_ODV_Read_Inputs_16_Bit:	.usect	".ebss",2,1,0
$C$DW$354	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Read_Inputs_16_Bit")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_ODV_Read_Inputs_16_Bit")
	.dwattr $C$DW$354, DW_AT_location[DW_OP_addr _ODV_Read_Inputs_16_Bit]
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$354, DW_AT_external
	.global	_ODV_CommError_StateCounter
_ODV_CommError_StateCounter:	.usect	".ebss",2,1,1
$C$DW$355	.dwtag  DW_TAG_variable, DW_AT_name("ODV_CommError_StateCounter")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_ODV_CommError_StateCounter")
	.dwattr $C$DW$355, DW_AT_location[DW_OP_addr _ODV_CommError_StateCounter]
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$355, DW_AT_external
	.global	_mms_dict_obj1003
_mms_dict_obj1003:	.usect	".ebss",2,1,1
$C$DW$356	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1003")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_mms_dict_obj1003")
	.dwattr $C$DW$356, DW_AT_location[DW_OP_addr _mms_dict_obj1003]
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$356, DW_AT_external
	.global	_ODP_RandomNB
_ODP_RandomNB:	.usect	".ebss",2,1,1
$C$DW$357	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RandomNB")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_ODP_RandomNB")
	.dwattr $C$DW$357, DW_AT_location[DW_OP_addr _ODP_RandomNB]
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$357, DW_AT_external
	.global	_ODV_ResetHW
_ODV_ResetHW:	.usect	".ebss",2,1,1
$C$DW$358	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ResetHW")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_ODV_ResetHW")
	.dwattr $C$DW$358, DW_AT_location[DW_OP_addr _ODV_ResetHW]
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$358, DW_AT_external
	.global	_ODV_Board_Manufactuer
_ODV_Board_Manufactuer:	.usect	".ebss",2,1,1
$C$DW$359	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Board_Manufactuer")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_ODV_Board_Manufactuer")
	.dwattr $C$DW$359, DW_AT_location[DW_OP_addr _ODV_Board_Manufactuer]
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$359, DW_AT_external
	.global	_ODP_Battery_Capacity
_ODP_Battery_Capacity:	.usect	".ebss",2,1,1
$C$DW$360	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Capacity")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_ODP_Battery_Capacity")
	.dwattr $C$DW$360, DW_AT_location[DW_OP_addr _ODP_Battery_Capacity]
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$360, DW_AT_external
	.global	_mms_dict_obj1000
_mms_dict_obj1000:	.usect	".ebss",2,1,1
$C$DW$361	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1000")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_mms_dict_obj1000")
	.dwattr $C$DW$361, DW_AT_location[DW_OP_addr _mms_dict_obj1000]
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$361, DW_AT_external
	.global	_ODV_SysTick_ms
_ODV_SysTick_ms:	.usect	".ebss",2,1,1
$C$DW$362	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SysTick_ms")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_ODV_SysTick_ms")
	.dwattr $C$DW$362, DW_AT_location[DW_OP_addr _ODV_SysTick_ms]
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$362, DW_AT_external
	.global	_ODP_Board_SerialNumber
_ODP_Board_SerialNumber:	.usect	".ebss",2,1,1
$C$DW$363	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_SerialNumber")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_ODP_Board_SerialNumber")
	.dwattr $C$DW$363, DW_AT_location[DW_OP_addr _ODP_Board_SerialNumber]
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$363, DW_AT_external
	.global	_ODV_RestoreDefaultParameters
_ODV_RestoreDefaultParameters:	.usect	".ebss",2,1,1
$C$DW$364	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RestoreDefaultParameters")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_ODV_RestoreDefaultParameters")
	.dwattr $C$DW$364, DW_AT_location[DW_OP_addr _ODV_RestoreDefaultParameters]
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$364, DW_AT_external
	.global	_ODV_Gateway_Power
_ODV_Gateway_Power:	.usect	".ebss",2,1,1
$C$DW$365	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Power")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_ODV_Gateway_Power")
	.dwattr $C$DW$365, DW_AT_location[DW_OP_addr _ODV_Gateway_Power]
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$365, DW_AT_external
	.global	_ODV_Gateway_WhCounter
_ODV_Gateway_WhCounter:	.usect	".ebss",2,1,1
$C$DW$366	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_WhCounter")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_ODV_Gateway_WhCounter")
	.dwattr $C$DW$366, DW_AT_location[DW_OP_addr _ODV_Gateway_WhCounter]
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$366, DW_AT_external
	.global	_mms_dict_obj1280_COB_ID_Server_to_Client_Receive_SDO
_mms_dict_obj1280_COB_ID_Server_to_Client_Receive_SDO:	.usect	".ebss",2,1,1
$C$DW$367	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1280_COB_ID_Server_to_Client_Receive_SDO")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_mms_dict_obj1280_COB_ID_Server_to_Client_Receive_SDO")
	.dwattr $C$DW$367, DW_AT_location[DW_OP_addr _mms_dict_obj1280_COB_ID_Server_to_Client_Receive_SDO]
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$367, DW_AT_external
	.global	_mms_dict_obj1400_COB_ID_used_by_PDO
_mms_dict_obj1400_COB_ID_used_by_PDO:	.usect	".ebss",2,1,1
$C$DW$368	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1400_COB_ID_used_by_PDO")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_mms_dict_obj1400_COB_ID_used_by_PDO")
	.dwattr $C$DW$368, DW_AT_location[DW_OP_addr _mms_dict_obj1400_COB_ID_used_by_PDO]
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$368, DW_AT_external
	.global	_mms_dict_obj1800_COB_ID_used_by_PDO
_mms_dict_obj1800_COB_ID_used_by_PDO:	.usect	".ebss",2,1,1
$C$DW$369	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1800_COB_ID_used_by_PDO")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_mms_dict_obj1800_COB_ID_used_by_PDO")
	.dwattr $C$DW$369, DW_AT_location[DW_OP_addr _mms_dict_obj1800_COB_ID_used_by_PDO]
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$369, DW_AT_external
	.global	_ODV_Gateway_Errorcode
_ODV_Gateway_Errorcode:	.usect	".ebss",2,1,1
$C$DW$370	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Errorcode")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_ODV_Gateway_Errorcode")
	.dwattr $C$DW$370, DW_AT_location[DW_OP_addr _ODV_Gateway_Errorcode]
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$370, DW_AT_external
	.global	_ODV_Supported_drive_modes
_ODV_Supported_drive_modes:	.usect	".ebss",2,1,1
$C$DW$371	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Supported_drive_modes")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_ODV_Supported_drive_modes")
	.dwattr $C$DW$371, DW_AT_location[DW_OP_addr _ODV_Supported_drive_modes]
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$371, DW_AT_external
	.global	_ODC_Controlword_callbacks
	.sect	".econst:_ODC_Controlword_callbacks"
	.clink
	.align	2
_ODC_Controlword_callbacks:
	.bits	_ControlWordCallBack,32		; _ODC_Controlword_callbacks[0] @ 0

$C$DW$372	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Controlword_callbacks")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_ODC_Controlword_callbacks")
	.dwattr $C$DW$372, DW_AT_location[DW_OP_addr _ODC_Controlword_callbacks]
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$372, DW_AT_external
	.global	_mms_dict_obj1016
_mms_dict_obj1016:	.usect	".ebss",2,1,1
$C$DW$373	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1016")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_mms_dict_obj1016")
	.dwattr $C$DW$373, DW_AT_location[DW_OP_addr _mms_dict_obj1016]
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$373, DW_AT_external
	.global	_mms_dict_obj1018_Serial_Number
_mms_dict_obj1018_Serial_Number:	.usect	".ebss",2,1,1
$C$DW$374	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1018_Serial_Number")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_mms_dict_obj1018_Serial_Number")
	.dwattr $C$DW$374, DW_AT_location[DW_OP_addr _mms_dict_obj1018_Serial_Number]
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$374, DW_AT_external
	.global	_mms_dict_obj1018_Product_Code
_mms_dict_obj1018_Product_Code:	.usect	".ebss",2,1,1
$C$DW$375	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1018_Product_Code")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_mms_dict_obj1018_Product_Code")
	.dwattr $C$DW$375, DW_AT_location[DW_OP_addr _mms_dict_obj1018_Product_Code]
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$375, DW_AT_external
	.global	_mms_dict_obj1018_Revision_Number
_mms_dict_obj1018_Revision_Number:	.usect	".ebss",2,1,1
$C$DW$376	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1018_Revision_Number")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_mms_dict_obj1018_Revision_Number")
	.dwattr $C$DW$376, DW_AT_location[DW_OP_addr _mms_dict_obj1018_Revision_Number]
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$376, DW_AT_external
	.global	_ODV_Write_Outputs_16_Bit
_ODV_Write_Outputs_16_Bit:	.usect	".ebss",2,1,0
$C$DW$377	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$377, DW_AT_location[DW_OP_addr _ODV_Write_Outputs_16_Bit]
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$377, DW_AT_external
	.global	_mms_dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO
_mms_dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO:	.usect	".ebss",2,1,1
$C$DW$378	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_mms_dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO")
	.dwattr $C$DW$378, DW_AT_location[DW_OP_addr _mms_dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO]
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$378, DW_AT_external
	.global	_mms_dict_obj1200_COB_ID_Client_to_Server_Receive_SDO
_mms_dict_obj1200_COB_ID_Client_to_Server_Receive_SDO:	.usect	".ebss",2,1,1
$C$DW$379	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1200_COB_ID_Client_to_Server_Receive_SDO")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_mms_dict_obj1200_COB_ID_Client_to_Server_Receive_SDO")
	.dwattr $C$DW$379, DW_AT_location[DW_OP_addr _mms_dict_obj1200_COB_ID_Client_to_Server_Receive_SDO]
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$379, DW_AT_external
	.global	_mms_dict_obj1200_COB_ID_Server_to_Client_Transmit_SDO
_mms_dict_obj1200_COB_ID_Server_to_Client_Transmit_SDO:	.usect	".ebss",2,1,1
$C$DW$380	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1200_COB_ID_Server_to_Client_Transmit_SDO")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_mms_dict_obj1200_COB_ID_Server_to_Client_Transmit_SDO")
	.dwattr $C$DW$380, DW_AT_location[DW_OP_addr _mms_dict_obj1200_COB_ID_Server_to_Client_Transmit_SDO]
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$380, DW_AT_external
	.global	_mms_dict_obj1018_Vendor_ID
_mms_dict_obj1018_Vendor_ID:	.usect	".ebss",2,1,1
$C$DW$381	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1018_Vendor_ID")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_mms_dict_obj1018_Vendor_ID")
	.dwattr $C$DW$381, DW_AT_location[DW_OP_addr _mms_dict_obj1018_Vendor_ID]
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$381, DW_AT_external
	.global	_ODV_RTC_Text
_ODV_RTC_Text:	.usect	".ebss",4,1,1
$C$DW$382	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RTC_Text")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_ODV_RTC_Text")
	.dwattr $C$DW$382, DW_AT_location[DW_OP_addr _ODV_RTC_Text]
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$382, DW_AT_external
	.global	_ODV_Security
_ODV_Security:	.usect	".ebss",4,1,1
$C$DW$383	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Security")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_ODV_Security")
	.dwattr $C$DW$383, DW_AT_location[DW_OP_addr _ODV_Security]
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$383, DW_AT_external
	.global	_ODV_Recorder_Vectors
_ODV_Recorder_Vectors:	.usect	".ebss",4,1,1
$C$DW$384	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Vectors")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_ODV_Recorder_Vectors")
	.dwattr $C$DW$384, DW_AT_location[DW_OP_addr _ODV_Recorder_Vectors]
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$384, DW_AT_external
	.global	_mms_dict_Index1003_callbacks
_mms_dict_Index1003_callbacks:	.usect	".ebss",4,1,1
$C$DW$385	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1003_callbacks")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_mms_dict_Index1003_callbacks")
	.dwattr $C$DW$385, DW_AT_location[DW_OP_addr _mms_dict_Index1003_callbacks]
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$385, DW_AT_external
	.global	_ODV_Gateway_Date_Time
_ODV_Gateway_Date_Time:	.usect	".ebss",4,1,1
$C$DW$386	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Date_Time")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_ODV_Gateway_Date_Time")
	.dwattr $C$DW$386, DW_AT_location[DW_OP_addr _ODV_Gateway_Date_Time]
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$386, DW_AT_external
	.global	_mms_dict_lastIndex
	.sect	".econst"
	.align	1
_mms_dict_lastIndex:
	.bits	7,16			; _mms_dict_lastIndex._SDO_SVR @ 0
	.bits	8,16			; _mms_dict_lastIndex._SDO_CLT @ 16
	.bits	9,16			; _mms_dict_lastIndex._PDO_RCV @ 32
	.bits	10,16			; _mms_dict_lastIndex._PDO_RCV_MAP @ 48
	.bits	14,16			; _mms_dict_lastIndex._PDO_TRS @ 64
	.bits	18,16			; _mms_dict_lastIndex._PDO_TRS_MAP @ 80

$C$DW$387	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_lastIndex")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_mms_dict_lastIndex")
	.dwattr $C$DW$387, DW_AT_location[DW_OP_addr _mms_dict_lastIndex]
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$387, DW_AT_external
	.global	_mms_dict_obj1600
_mms_dict_obj1600:	.usect	".ebss",6,1,1
$C$DW$388	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1600")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_mms_dict_obj1600")
	.dwattr $C$DW$388, DW_AT_location[DW_OP_addr _mms_dict_obj1600]
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$388, DW_AT_external
	.global	_ODC_Write_Outputs_16_Bit_callbacks
	.sect	".econst:_ODC_Write_Outputs_16_Bit_callbacks"
	.clink
	.align	2
_ODC_Write_Outputs_16_Bit_callbacks:
	.bits	0,32			; _ODC_Write_Outputs_16_Bit_callbacks[0] @ 0
	.bits	_WriteOutputs16BitCallback,32		; _ODC_Write_Outputs_16_Bit_callbacks[1] @ 32
	.bits	_WriteOutputs16BitCallback,32		; _ODC_Write_Outputs_16_Bit_callbacks[2] @ 64

$C$DW$389	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Write_Outputs_16_Bit_callbacks")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_ODC_Write_Outputs_16_Bit_callbacks")
	.dwattr $C$DW$389, DW_AT_location[DW_OP_addr _ODC_Write_Outputs_16_Bit_callbacks]
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$389, DW_AT_external
	.global	_mms_dict_firstIndex
	.sect	".econst"
	.align	1
_mms_dict_firstIndex:
	.bits	7,16			; _mms_dict_firstIndex._SDO_SVR @ 0
	.bits	8,16			; _mms_dict_firstIndex._SDO_CLT @ 16
	.bits	9,16			; _mms_dict_firstIndex._PDO_RCV @ 32
	.bits	10,16			; _mms_dict_firstIndex._PDO_RCV_MAP @ 48
	.bits	11,16			; _mms_dict_firstIndex._PDO_TRS @ 64
	.bits	15,16			; _mms_dict_firstIndex._PDO_TRS_MAP @ 80

$C$DW$390	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_firstIndex")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_mms_dict_firstIndex")
	.dwattr $C$DW$390, DW_AT_location[DW_OP_addr _mms_dict_firstIndex]
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$390, DW_AT_external
	.global	_mms_dict_Index200E
	.sect	".econst:_mms_dict_Index200E"
	.clink
	.align	2
_mms_dict_Index200E:
	.bits	0,16			; _mms_dict_Index200E[0]._bAccessType @ 0
	.bits	6,16			; _mms_dict_Index200E[0]._bDataType @ 16
	.bits	2,32			; _mms_dict_Index200E[0]._size @ 32
	.bits	_ODV_MachineState,32		; _mms_dict_Index200E[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index200E[0]._bProcessor @ 96
	.space	16

$C$DW$391	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index200E")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_mms_dict_Index200E")
	.dwattr $C$DW$391, DW_AT_location[DW_OP_addr _mms_dict_Index200E]
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$391, DW_AT_external
	.global	_ODP_Power_ChargeLimits
_ODP_Power_ChargeLimits:	.usect	".ebss",8,1,0
$C$DW$392	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Power_ChargeLimits")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_ODP_Power_ChargeLimits")
	.dwattr $C$DW$392, DW_AT_location[DW_OP_addr _ODP_Power_ChargeLimits]
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$392, DW_AT_external
	.global	_mms_dict_Index2F00
	.sect	".econst:_mms_dict_Index2F00"
	.clink
	.align	2
_mms_dict_Index2F00:
	.bits	0,16			; _mms_dict_Index2F00[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index2F00[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index2F00[0]._size @ 32
	.bits	_ODV_StoreParameters,32		; _mms_dict_Index2F00[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2F00[0]._bProcessor @ 96
	.space	16

$C$DW$393	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2F00")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_mms_dict_Index2F00")
	.dwattr $C$DW$393, DW_AT_location[DW_OP_addr _mms_dict_Index2F00]
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$393, DW_AT_external
	.global	_ODP_Temperature_ChargeLimits
_ODP_Temperature_ChargeLimits:	.usect	".ebss",8,1,0
$C$DW$394	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_ChargeLimits")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_ODP_Temperature_ChargeLimits")
	.dwattr $C$DW$394, DW_AT_location[DW_OP_addr _ODP_Temperature_ChargeLimits]
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$394, DW_AT_external
	.global	_mms_dict_Index2102
	.sect	".econst:_mms_dict_Index2102"
	.clink
	.align	2
_mms_dict_Index2102:
	.bits	0,16			; _mms_dict_Index2102[0]._bAccessType @ 0
	.bits	27,16			; _mms_dict_Index2102[0]._bDataType @ 16
	.bits	8,32			; _mms_dict_Index2102[0]._size @ 32
	.bits	_ODV_Security,32		; _mms_dict_Index2102[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2102[0]._bProcessor @ 96
	.space	16

$C$DW$395	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2102")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_mms_dict_Index2102")
	.dwattr $C$DW$395, DW_AT_location[DW_OP_addr _mms_dict_Index2102]
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$395, DW_AT_external
	.global	_mms_dict_Index2103
	.sect	".econst:_mms_dict_Index2103"
	.clink
	.align	2
_mms_dict_Index2103:
	.bits	6,16			; _mms_dict_Index2103[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index2103[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index2103[0]._size @ 32
	.bits	_ODP_RandomNB,32		; _mms_dict_Index2103[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2103[0]._bProcessor @ 96
	.space	16

$C$DW$396	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2103")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_mms_dict_Index2103")
	.dwattr $C$DW$396, DW_AT_location[DW_OP_addr _mms_dict_Index2103]
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$396, DW_AT_external
	.global	_ODC_RTC_callbacks
	.sect	".econst:_ODC_RTC_callbacks"
	.clink
	.align	2
_ODC_RTC_callbacks:
	.bits	0,32			; _ODC_RTC_callbacks[0] @ 0
	.bits	0,32			; _ODC_RTC_callbacks[1] @ 32
	.bits	_WriteTextCallback,32		; _ODC_RTC_callbacks[2] @ 64
	.bits	0,32			; _ODC_RTC_callbacks[3] @ 96

$C$DW$397	.dwtag  DW_TAG_variable, DW_AT_name("ODC_RTC_callbacks")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_ODC_RTC_callbacks")
	.dwattr $C$DW$397, DW_AT_location[DW_OP_addr _ODC_RTC_callbacks]
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$397, DW_AT_external
	.global	_mms_dict_Index2041
	.sect	".econst:_mms_dict_Index2041"
	.clink
	.align	2
_mms_dict_Index2041:
	.bits	4,16			; _mms_dict_Index2041[0]._bAccessType @ 0
	.bits	6,16			; _mms_dict_Index2041[0]._bDataType @ 16
	.bits	2,32			; _mms_dict_Index2041[0]._size @ 32
	.bits	_ODP_SleepCurrent,32		; _mms_dict_Index2041[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2041[0]._bProcessor @ 96
	.space	16

$C$DW$398	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2041")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_mms_dict_Index2041")
	.dwattr $C$DW$398, DW_AT_location[DW_OP_addr _mms_dict_Index2041]
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$398, DW_AT_external
	.global	_mms_dict_Index2101
	.sect	".econst:_mms_dict_Index2101"
	.clink
	.align	2
_mms_dict_Index2101:
	.bits	2,16			; _mms_dict_Index2101[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2101[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2101[0]._size @ 32
	.bits	_ODV_Version,32		; _mms_dict_Index2101[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2101[0]._bProcessor @ 96
	.space	16

$C$DW$399	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2101")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_mms_dict_Index2101")
	.dwattr $C$DW$399, DW_AT_location[DW_OP_addr _mms_dict_Index2101]
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$399, DW_AT_external
	.global	_ODP_Temperature_DischargeLimits
_ODP_Temperature_DischargeLimits:	.usect	".ebss",8,1,0
$C$DW$400	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_DischargeLimits")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_ODP_Temperature_DischargeLimits")
	.dwattr $C$DW$400, DW_AT_location[DW_OP_addr _ODP_Temperature_DischargeLimits]
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$400, DW_AT_external
	.global	_ODP_Power_DischargeLimits
_ODP_Power_DischargeLimits:	.usect	".ebss",8,1,0
$C$DW$401	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Power_DischargeLimits")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_ODP_Power_DischargeLimits")
	.dwattr $C$DW$401, DW_AT_location[DW_OP_addr _ODP_Power_DischargeLimits]
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$401, DW_AT_external
	.global	_mms_dict_Index201C
	.sect	".econst:_mms_dict_Index201C"
	.clink
	.align	2
_mms_dict_Index201C:
	.bits	0,16			; _mms_dict_Index201C[0]._bAccessType @ 0
	.bits	2,16			; _mms_dict_Index201C[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index201C[0]._size @ 32
	.bits	_ODV_MachineMode,32		; _mms_dict_Index201C[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index201C[0]._bProcessor @ 96
	.space	16

$C$DW$402	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index201C")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_mms_dict_Index201C")
	.dwattr $C$DW$402, DW_AT_location[DW_OP_addr _mms_dict_Index201C]
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$402, DW_AT_external
	.global	_mms_dict_Index2104
	.sect	".econst:_mms_dict_Index2104"
	.clink
	.align	2
_mms_dict_Index2104:
	.bits	2,16			; _mms_dict_Index2104[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index2104[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index2104[0]._size @ 32
	.bits	_ODV_SysTick_ms,32		; _mms_dict_Index2104[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2104[0]._bProcessor @ 96
	.space	16

$C$DW$403	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2104")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_mms_dict_Index2104")
	.dwattr $C$DW$403, DW_AT_location[DW_OP_addr _mms_dict_Index2104]
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$403, DW_AT_external
	.global	_mms_dict_Index201E
	.sect	".econst:_mms_dict_Index201E"
	.clink
	.align	2
_mms_dict_Index201E:
	.bits	0,16			; _mms_dict_Index201E[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index201E[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index201E[0]._size @ 32
	.bits	_ODV_MachineEvent,32		; _mms_dict_Index201E[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index201E[0]._bProcessor @ 96
	.space	16

$C$DW$404	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index201E")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_mms_dict_Index201E")
	.dwattr $C$DW$404, DW_AT_location[DW_OP_addr _mms_dict_Index201E]
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$404, DW_AT_external
	.global	_mms_dict_Index2000
	.sect	".econst:_mms_dict_Index2000"
	.clink
	.align	2
_mms_dict_Index2000:
	.bits	6,16			; _mms_dict_Index2000[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index2000[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index2000[0]._size @ 32
	.bits	_ODP_OnTime,32		; _mms_dict_Index2000[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2000[0]._bProcessor @ 96
	.space	16

$C$DW$405	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2000")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_mms_dict_Index2000")
	.dwattr $C$DW$405, DW_AT_location[DW_OP_addr _mms_dict_Index2000]
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$405, DW_AT_external
	.global	_mms_dict_Index1000
	.sect	".econst:_mms_dict_Index1000"
	.clink
	.align	2
_mms_dict_Index1000:
	.bits	2,16			; _mms_dict_Index1000[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index1000[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index1000[0]._size @ 32
	.bits	_mms_dict_obj1000,32		; _mms_dict_Index1000[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1000[0]._bProcessor @ 96
	.space	16

$C$DW$406	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1000")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_mms_dict_Index1000")
	.dwattr $C$DW$406, DW_AT_location[DW_OP_addr _mms_dict_Index1000]
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$406, DW_AT_external
	.global	_mms_dict_Index2001
	.sect	".econst:_mms_dict_Index2001"
	.clink
	.align	2
_mms_dict_Index2001:
	.bits	6,16			; _mms_dict_Index2001[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index2001[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index2001[0]._size @ 32
	.bits	_ODP_Password,32		; _mms_dict_Index2001[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2001[0]._bProcessor @ 96
	.space	16

$C$DW$407	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2001")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_mms_dict_Index2001")
	.dwattr $C$DW$407, DW_AT_location[DW_OP_addr _mms_dict_Index2001]
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$407, DW_AT_external
	.global	_mms_dict_Index2003
	.sect	".econst:_mms_dict_Index2003"
	.clink
	.align	2
_mms_dict_Index2003:
	.bits	4,16			; _mms_dict_Index2003[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index2003[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index2003[0]._size @ 32
	.bits	_ODP_CrcParameters,32		; _mms_dict_Index2003[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2003[0]._bProcessor @ 96
	.space	16

$C$DW$408	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2003")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_mms_dict_Index2003")
	.dwattr $C$DW$408, DW_AT_location[DW_OP_addr _mms_dict_Index2003]
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$408, DW_AT_external
	.global	_mms_dict_Index2002
	.sect	".econst:_mms_dict_Index2002"
	.clink
	.align	2
_mms_dict_Index2002:
	.bits	6,16			; _mms_dict_Index2002[0]._bAccessType @ 0
	.bits	6,16			; _mms_dict_Index2002[0]._bDataType @ 16
	.bits	2,32			; _mms_dict_Index2002[0]._size @ 32
	.bits	_ODP_VersionParameters,32		; _mms_dict_Index2002[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2002[0]._bProcessor @ 96
	.space	16

$C$DW$409	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2002")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_mms_dict_Index2002")
	.dwattr $C$DW$409, DW_AT_location[DW_OP_addr _mms_dict_Index2002]
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$409, DW_AT_external
	.global	_mms_dict_Index100A
	.sect	".econst:_mms_dict_Index100A"
	.clink
	.align	2
_mms_dict_Index100A:
	.bits	2,16			; _mms_dict_Index100A[0]._bAccessType @ 0
	.bits	6,16			; _mms_dict_Index100A[0]._bDataType @ 16
	.bits	2,32			; _mms_dict_Index100A[0]._size @ 32
	.bits	_mms_dict_obj100A,32		; _mms_dict_Index100A[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index100A[0]._bProcessor @ 96
	.space	16

$C$DW$410	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index100A")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_mms_dict_Index100A")
	.dwattr $C$DW$410, DW_AT_location[DW_OP_addr _mms_dict_Index100A]
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$410, DW_AT_external
	.global	_mms_dict_Index1014
	.sect	".econst:_mms_dict_Index1014"
	.clink
	.align	2
_mms_dict_Index1014:
	.bits	0,16			; _mms_dict_Index1014[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index1014[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index1014[0]._size @ 32
	.bits	_mms_dict_obj1014,32		; _mms_dict_Index1014[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1014[0]._bProcessor @ 96
	.space	16

$C$DW$411	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1014")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_mms_dict_Index1014")
	.dwattr $C$DW$411, DW_AT_location[DW_OP_addr _mms_dict_Index1014]
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$411, DW_AT_external
	.global	_mms_dict_Index1006
	.sect	".econst:_mms_dict_Index1006"
	.clink
	.align	2
_mms_dict_Index1006:
	.bits	0,16			; _mms_dict_Index1006[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index1006[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index1006[0]._size @ 32
	.bits	_mms_dict_obj1006,32		; _mms_dict_Index1006[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1006[0]._bProcessor @ 96
	.space	16

$C$DW$412	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1006")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_mms_dict_Index1006")
	.dwattr $C$DW$412, DW_AT_location[DW_OP_addr _mms_dict_Index1006]
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$412, DW_AT_external
	.global	_mms_dict_Index1001
	.sect	".econst:_mms_dict_Index1001"
	.clink
	.align	2
_mms_dict_Index1001:
	.bits	2,16			; _mms_dict_Index1001[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1001[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1001[0]._size @ 32
	.bits	_mms_dict_obj1001,32		; _mms_dict_Index1001[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1001[0]._bProcessor @ 96
	.space	16

$C$DW$413	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1001")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_mms_dict_Index1001")
	.dwattr $C$DW$413, DW_AT_location[DW_OP_addr _mms_dict_Index1001]
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$413, DW_AT_external
	.global	_mms_dict_Index1005
	.sect	".econst:_mms_dict_Index1005"
	.clink
	.align	2
_mms_dict_Index1005:
	.bits	0,16			; _mms_dict_Index1005[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index1005[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index1005[0]._size @ 32
	.bits	_mms_dict_obj1005,32		; _mms_dict_Index1005[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1005[0]._bProcessor @ 96
	.space	16

$C$DW$414	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1005")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_mms_dict_Index1005")
	.dwattr $C$DW$414, DW_AT_location[DW_OP_addr _mms_dict_Index1005]
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$414, DW_AT_external
	.global	_mms_dict_Index2005
	.sect	".econst:_mms_dict_Index2005"
	.clink
	.align	2
_mms_dict_Index2005:
	.bits	0,16			; _mms_dict_Index2005[0]._bAccessType @ 0
	.bits	15,16			; _mms_dict_Index2005[0]._bDataType @ 16
	.bits	8192,32			; _mms_dict_Index2005[0]._size @ 32
	.bits	_ODV_RecorderData1,32		; _mms_dict_Index2005[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2005[0]._bProcessor @ 96
	.space	16

$C$DW$415	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2005")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_mms_dict_Index2005")
	.dwattr $C$DW$415, DW_AT_location[DW_OP_addr _mms_dict_Index2005]
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$415, DW_AT_external
	.global	_mms_dict_Index8502
	.sect	".econst:_mms_dict_Index8502"
	.clink
	.align	2
_mms_dict_Index8502:
	.bits	2,16			; _mms_dict_Index8502[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index8502[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index8502[0]._size @ 32
	.bits	_ODV_Supported_drive_modes,32		; _mms_dict_Index8502[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index8502[0]._bProcessor @ 96
	.space	16

$C$DW$416	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index8502")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_mms_dict_Index8502")
	.dwattr $C$DW$416, DW_AT_location[DW_OP_addr _mms_dict_Index8502]
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$416, DW_AT_external
	.global	_mms_dict_Index2F03
	.sect	".econst:_mms_dict_Index2F03"
	.clink
	.align	2
_mms_dict_Index2F03:
	.bits	0,16			; _mms_dict_Index2F03[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2F03[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2F03[0]._size @ 32
	.bits	_ODV_Debug,32		; _mms_dict_Index2F03[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2F03[0]._bProcessor @ 96
	.space	16

$C$DW$417	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2F03")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_mms_dict_Index2F03")
	.dwattr $C$DW$417, DW_AT_location[DW_OP_addr _mms_dict_Index2F03]
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$417, DW_AT_external
	.global	_mms_dict_Index2007
	.sect	".econst:_mms_dict_Index2007"
	.clink
	.align	2
_mms_dict_Index2007:
	.bits	0,16			; _mms_dict_Index2007[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2007[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2007[0]._size @ 32
	.bits	_ODV_SciSend,32		; _mms_dict_Index2007[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2007[0]._bProcessor @ 96
	.space	16

$C$DW$418	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2007")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_mms_dict_Index2007")
	.dwattr $C$DW$418, DW_AT_location[DW_OP_addr _mms_dict_Index2007]
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$418, DW_AT_external
	.global	_mms_dict_Index8040
	.sect	".econst:_mms_dict_Index8040"
	.clink
	.align	2
_mms_dict_Index8040:
	.bits	0,16			; _mms_dict_Index8040[0]._bAccessType @ 0
	.bits	6,16			; _mms_dict_Index8040[0]._bDataType @ 16
	.bits	2,32			; _mms_dict_Index8040[0]._size @ 32
	.bits	_ODV_Controlword,32		; _mms_dict_Index8040[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index8040[0]._bProcessor @ 96
	.space	16

$C$DW$419	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index8040")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_mms_dict_Index8040")
	.dwattr $C$DW$419, DW_AT_location[DW_OP_addr _mms_dict_Index8040]
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$419, DW_AT_external
	.global	_mms_dict_Index2F02
	.sect	".econst:_mms_dict_Index2F02"
	.clink
	.align	2
_mms_dict_Index2F02:
	.bits	0,16			; _mms_dict_Index2F02[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index2F02[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index2F02[0]._size @ 32
	.bits	_ODV_ResetHW,32		; _mms_dict_Index2F02[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2F02[0]._bProcessor @ 96
	.space	16

$C$DW$420	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2F02")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_mms_dict_Index2F02")
	.dwattr $C$DW$420, DW_AT_location[DW_OP_addr _mms_dict_Index2F02]
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$420, DW_AT_external
	.global	_mms_dict_Index2F01
	.sect	".econst:_mms_dict_Index2F01"
	.clink
	.align	2
_mms_dict_Index2F01:
	.bits	0,16			; _mms_dict_Index2F01[0]._bAccessType @ 0
	.bits	7,16			; _mms_dict_Index2F01[0]._bDataType @ 16
	.bits	4,32			; _mms_dict_Index2F01[0]._size @ 32
	.bits	_ODV_RestoreDefaultParameters,32		; _mms_dict_Index2F01[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2F01[0]._bProcessor @ 96
	.space	16

$C$DW$421	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2F01")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_mms_dict_Index2F01")
	.dwattr $C$DW$421, DW_AT_location[DW_OP_addr _mms_dict_Index2F01]
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$421, DW_AT_external
	.global	_ODC_ErrorDsp_callbacks
	.sect	".econst:_ODC_ErrorDsp_callbacks"
	.clink
	.align	2
_ODC_ErrorDsp_callbacks:
	.bits	0,32			; _ODC_ErrorDsp_callbacks[0] @ 0
	.bits	_ClearErrorCallback,32		; _ODC_ErrorDsp_callbacks[1] @ 32
	.bits	_ClearErrorCallback,32		; _ODC_ErrorDsp_callbacks[2] @ 64
	.bits	0,32			; _ODC_ErrorDsp_callbacks[3] @ 96

$C$DW$422	.dwtag  DW_TAG_variable, DW_AT_name("ODC_ErrorDsp_callbacks")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_ODC_ErrorDsp_callbacks")
	.dwattr $C$DW$422, DW_AT_location[DW_OP_addr _ODC_ErrorDsp_callbacks]
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$422, DW_AT_external
	.global	_mms_dict_Index8041
	.sect	".econst:_mms_dict_Index8041"
	.clink
	.align	2
_mms_dict_Index8041:
	.bits	0,16			; _mms_dict_Index8041[0]._bAccessType @ 0
	.bits	6,16			; _mms_dict_Index8041[0]._bDataType @ 16
	.bits	2,32			; _mms_dict_Index8041[0]._size @ 32
	.bits	_ODV_Statusword,32		; _mms_dict_Index8041[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index8041[0]._bProcessor @ 96
	.space	16

$C$DW$423	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index8041")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_mms_dict_Index8041")
	.dwattr $C$DW$423, DW_AT_location[DW_OP_addr _mms_dict_Index8041]
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$423, DW_AT_external
	.global	_mms_dict_obj1A03
_mms_dict_obj1A03:	.usect	".ebss",10,1,1
$C$DW$424	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1A03")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_mms_dict_obj1A03")
	.dwattr $C$DW$424, DW_AT_location[DW_OP_addr _mms_dict_obj1A03]
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$424, DW_AT_external
	.global	_mms_dict_obj1A02
_mms_dict_obj1A02:	.usect	".ebss",10,1,1
$C$DW$425	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1A02")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_mms_dict_obj1A02")
	.dwattr $C$DW$425, DW_AT_location[DW_OP_addr _mms_dict_obj1A02]
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$425, DW_AT_external
	.global	_ODC_CommError_callbacks
	.sect	".econst:_ODC_CommError_callbacks"
	.clink
	.align	2
_ODC_CommError_callbacks:
	.bits	0,32			; _ODC_CommError_callbacks[0] @ 0
	.bits	0,32			; _ODC_CommError_callbacks[1] @ 32
	.bits	0,32			; _ODC_CommError_callbacks[2] @ 64
	.bits	_CommErrorSetCallback,32		; _ODC_CommError_callbacks[3] @ 96
	.bits	0,32			; _ODC_CommError_callbacks[4] @ 128
	.bits	0,32			; _ODC_CommError_callbacks[5] @ 160

$C$DW$426	.dwtag  DW_TAG_variable, DW_AT_name("ODC_CommError_callbacks")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_ODC_CommError_callbacks")
	.dwattr $C$DW$426, DW_AT_location[DW_OP_addr _ODC_CommError_callbacks]
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$426, DW_AT_external
	.global	_mms_dict_obj1A00
_mms_dict_obj1A00:	.usect	".ebss",14,1,1
$C$DW$427	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1A00")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_mms_dict_obj1A00")
	.dwattr $C$DW$427, DW_AT_location[DW_OP_addr _mms_dict_obj1A00]
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$427, DW_AT_external
	.global	_ODC_Battery_callbacks
	.sect	".econst:_ODC_Battery_callbacks"
	.clink
	.align	2
_ODC_Battery_callbacks:
	.bits	0,32			; _ODC_Battery_callbacks[0] @ 0
	.bits	_BatteryCallBack,32		; _ODC_Battery_callbacks[1] @ 32
	.bits	_BatteryCallBack,32		; _ODC_Battery_callbacks[2] @ 64
	.bits	_BatteryCallBack,32		; _ODC_Battery_callbacks[3] @ 96
	.bits	_BatteryCallBack,32		; _ODC_Battery_callbacks[4] @ 128
	.bits	0,32			; _ODC_Battery_callbacks[5] @ 160
	.bits	0,32			; _ODC_Battery_callbacks[6] @ 192

$C$DW$428	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Battery_callbacks")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_ODC_Battery_callbacks")
	.dwattr $C$DW$428, DW_AT_location[DW_OP_addr _ODC_Battery_callbacks]
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$428, DW_AT_external
	.global	_mms_dict_obj1A01
_mms_dict_obj1A01:	.usect	".ebss",16,1,1
$C$DW$429	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1A01")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_mms_dict_obj1A01")
	.dwattr $C$DW$429, DW_AT_location[DW_OP_addr _mms_dict_obj1A01]
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$429, DW_AT_external
	.global	_mms_dict_Index1003
	.sect	".econst:_mms_dict_Index1003"
	.clink
	.align	2
_mms_dict_Index1003:
	.bits	0,16			; _mms_dict_Index1003[0]._bAccessType @ 0
	.bits	159,16			; _mms_dict_Index1003[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1003[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1003,32		; _mms_dict_Index1003[0]._pObject @ 64
	.space	32
	.bits	2,16			; _mms_dict_Index1003[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1003[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1003[1]._size @ 160
	.bits	_mms_dict_obj1003,32		; _mms_dict_Index1003[1]._pObject @ 192
	.space	32

$C$DW$430	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1003")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_mms_dict_Index1003")
	.dwattr $C$DW$430, DW_AT_location[DW_OP_addr _mms_dict_Index1003]
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$430, DW_AT_external
	.global	_ODV_Write_Analogue_Output_16_Bit
_ODV_Write_Analogue_Output_16_Bit:	.usect	".ebss",16,1,0
$C$DW$431	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Analogue_Output_16_Bit")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_ODV_Write_Analogue_Output_16_Bit")
	.dwattr $C$DW$431, DW_AT_location[DW_OP_addr _ODV_Write_Analogue_Output_16_Bit]
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$431, DW_AT_external
	.global	_ODC_Board_callbacks
	.sect	".econst:_ODC_Board_callbacks"
	.clink
	.align	2
_ODC_Board_callbacks:
	.bits	0,32			; _ODC_Board_callbacks[0] @ 0
	.bits	0,32			; _ODC_Board_callbacks[1] @ 32
	.bits	0,32			; _ODC_Board_callbacks[2] @ 64
	.bits	_ConfigCallback,32		; _ODC_Board_callbacks[3] @ 96
	.bits	0,32			; _ODC_Board_callbacks[4] @ 128
	.bits	0,32			; _ODC_Board_callbacks[5] @ 160
	.bits	0,32			; _ODC_Board_callbacks[6] @ 192
	.bits	0,32			; _ODC_Board_callbacks[7] @ 224
	.bits	0,32			; _ODC_Board_callbacks[8] @ 256
	.bits	0,32			; _ODC_Board_callbacks[9] @ 288
	.bits	0,32			; _ODC_Board_callbacks[10] @ 320

$C$DW$432	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Board_callbacks")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_ODC_Board_callbacks")
	.dwattr $C$DW$432, DW_AT_location[DW_OP_addr _ODC_Board_callbacks]
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$432, DW_AT_external
	.global	_ODV_Read_Analogue_Input_16_Bit
_ODV_Read_Analogue_Input_16_Bit:	.usect	".ebss",23,1,0
$C$DW$433	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$433, DW_AT_location[DW_OP_addr _ODV_Read_Analogue_Input_16_Bit]
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$433, DW_AT_external
	.global	_mms_dict_Index2010
	.sect	".econst:_mms_dict_Index2010"
	.clink
	.align	2
_mms_dict_Index2010:
	.bits	2,16			; _mms_dict_Index2010[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2010[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2010[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2010,32		; _mms_dict_Index2010[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2010[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index2010[1]._bAccessType @ 128
	.bits	6,16			; _mms_dict_Index2010[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index2010[1]._size @ 160
	.bits	_ODV_Voltage_Min,32		; _mms_dict_Index2010[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2010[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index2010[2]._bAccessType @ 256
	.bits	6,16			; _mms_dict_Index2010[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index2010[2]._size @ 288
	.bits	_ODV_Voltage_Max,32		; _mms_dict_Index2010[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2010[2]._bProcessor @ 352
	.space	16

$C$DW$434	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2010")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_mms_dict_Index2010")
	.dwattr $C$DW$434, DW_AT_location[DW_OP_addr _mms_dict_Index2010]
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$434, DW_AT_external
	.global	_mms_dict_Index1200
	.sect	".econst:_mms_dict_Index1200"
	.clink
	.align	2
_mms_dict_Index1200:
	.bits	2,16			; _mms_dict_Index1200[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1200[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1200[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1200,32		; _mms_dict_Index1200[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1200[0]._bProcessor @ 96
	.space	16
	.bits	2,16			; _mms_dict_Index1200[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1200[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1200[1]._size @ 160
	.bits	_mms_dict_obj1200_COB_ID_Client_to_Server_Receive_SDO,32		; _mms_dict_Index1200[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1200[1]._bProcessor @ 224
	.space	16
	.bits	2,16			; _mms_dict_Index1200[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index1200[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index1200[2]._size @ 288
	.bits	_mms_dict_obj1200_COB_ID_Server_to_Client_Transmit_SDO,32		; _mms_dict_Index1200[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1200[2]._bProcessor @ 352
	.space	16

$C$DW$435	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1200")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_mms_dict_Index1200")
	.dwattr $C$DW$435, DW_AT_location[DW_OP_addr _mms_dict_Index1200]
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$435, DW_AT_external
	.global	_mms_dict_Index2106
	.sect	".econst:_mms_dict_Index2106"
	.clink
	.align	2
_mms_dict_Index2106:
	.bits	2,16			; _mms_dict_Index2106[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2106[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2106[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2106,32		; _mms_dict_Index2106[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2106[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index2106[1]._bAccessType @ 128
	.bits	5,16			; _mms_dict_Index2106[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index2106[1]._size @ 160
	.bits	_ODV_SOC_SOC1,32		; _mms_dict_Index2106[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2106[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index2106[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index2106[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index2106[2]._size @ 288
	.bits	_ODV_SOC_SOC2,32		; _mms_dict_Index2106[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2106[2]._bProcessor @ 352
	.space	16

$C$DW$436	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2106")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_mms_dict_Index2106")
	.dwattr $C$DW$436, DW_AT_location[DW_OP_addr _mms_dict_Index2106]
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$436, DW_AT_external
	.global	_mms_dict_Index6100
	.sect	".econst:_mms_dict_Index6100"
	.clink
	.align	2
_mms_dict_Index6100:
	.bits	2,16			; _mms_dict_Index6100[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index6100[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index6100[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj6100,32		; _mms_dict_Index6100[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index6100[0]._bProcessor @ 96
	.space	16
	.bits	2,16			; _mms_dict_Index6100[1]._bAccessType @ 128
	.bits	6,16			; _mms_dict_Index6100[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index6100[1]._size @ 160
	.bits	_ODV_Read_Inputs_16_Bit,32		; _mms_dict_Index6100[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index6100[1]._bProcessor @ 224
	.space	16
	.bits	2,16			; _mms_dict_Index6100[2]._bAccessType @ 256
	.bits	6,16			; _mms_dict_Index6100[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index6100[2]._size @ 288
	.bits	_ODV_Read_Inputs_16_Bit+1,32		; _mms_dict_Index6100[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index6100[2]._bProcessor @ 352
	.space	16

$C$DW$437	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index6100")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_mms_dict_Index6100")
	.dwattr $C$DW$437, DW_AT_location[DW_OP_addr _mms_dict_Index6100]
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$437, DW_AT_external
	.global	_mms_dict_Index6300
	.sect	".econst:_mms_dict_Index6300"
	.clink
	.align	2
_mms_dict_Index6300:
	.bits	2,16			; _mms_dict_Index6300[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index6300[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index6300[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj6300,32		; _mms_dict_Index6300[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index6300[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index6300[1]._bAccessType @ 128
	.bits	6,16			; _mms_dict_Index6300[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index6300[1]._size @ 160
	.bits	_ODV_Write_Outputs_16_Bit,32		; _mms_dict_Index6300[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index6300[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index6300[2]._bAccessType @ 256
	.bits	6,16			; _mms_dict_Index6300[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index6300[2]._size @ 288
	.bits	_ODV_Write_Outputs_16_Bit+1,32		; _mms_dict_Index6300[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index6300[2]._bProcessor @ 352
	.space	16

$C$DW$438	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index6300")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_mms_dict_Index6300")
	.dwattr $C$DW$438, DW_AT_location[DW_OP_addr _mms_dict_Index6300]
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$438, DW_AT_external
	.global	_ODC_Recorder_callbacks
	.sect	".econst:_ODC_Recorder_callbacks"
	.clink
	.align	2
_ODC_Recorder_callbacks:
	.bits	0,32			; _ODC_Recorder_callbacks[0] @ 0
	.bits	0,32			; _ODC_Recorder_callbacks[1] @ 32
	.bits	0,32			; _ODC_Recorder_callbacks[2] @ 64
	.bits	0,32			; _ODC_Recorder_callbacks[3] @ 96
	.bits	_MultiunitsCallback,32		; _ODC_Recorder_callbacks[4] @ 128
	.bits	_VariablesCallback,32		; _ODC_Recorder_callbacks[5] @ 160
	.bits	_StartRecorderCallBack,32		; _ODC_Recorder_callbacks[6] @ 192
	.bits	0,32			; _ODC_Recorder_callbacks[7] @ 224
	.bits	0,32			; _ODC_Recorder_callbacks[8] @ 256
	.bits	0,32			; _ODC_Recorder_callbacks[9] @ 288
	.bits	0,32			; _ODC_Recorder_callbacks[10] @ 320
	.bits	0,32			; _ODC_Recorder_callbacks[11] @ 352
	.bits	0,32			; _ODC_Recorder_callbacks[12] @ 384

$C$DW$439	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Recorder_callbacks")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_ODC_Recorder_callbacks")
	.dwattr $C$DW$439, DW_AT_location[DW_OP_addr _ODC_Recorder_callbacks]
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$439, DW_AT_external
	.global	_mms_dict_Index200F
	.sect	".econst:_mms_dict_Index200F"
	.clink
	.align	2
_mms_dict_Index200F:
	.bits	2,16			; _mms_dict_Index200F[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index200F[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index200F[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj200F,32		; _mms_dict_Index200F[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index200F[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index200F[1]._bAccessType @ 128
	.bits	2,16			; _mms_dict_Index200F[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index200F[1]._size @ 160
	.bits	_ODV_Temperature_Min,32		; _mms_dict_Index200F[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index200F[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index200F[2]._bAccessType @ 256
	.bits	2,16			; _mms_dict_Index200F[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index200F[2]._size @ 288
	.bits	_ODV_Temperature_Max,32		; _mms_dict_Index200F[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index200F[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index200F[3]._bAccessType @ 384
	.bits	2,16			; _mms_dict_Index200F[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index200F[3]._size @ 416
	.bits	_ODV_Temperature_Average_data,32		; _mms_dict_Index200F[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index200F[3]._bProcessor @ 480
	.space	16

$C$DW$440	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index200F")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_mms_dict_Index200F")
	.dwattr $C$DW$440, DW_AT_location[DW_OP_addr _mms_dict_Index200F]
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$440, DW_AT_external
	.global	_mms_dict_Index1280
	.sect	".econst:_mms_dict_Index1280"
	.clink
	.align	2
_mms_dict_Index1280:
	.bits	2,16			; _mms_dict_Index1280[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1280[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1280[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1280,32		; _mms_dict_Index1280[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1280[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1280[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1280[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1280[1]._size @ 160
	.bits	_mms_dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO,32		; _mms_dict_Index1280[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1280[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1280[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index1280[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index1280[2]._size @ 288
	.bits	_mms_dict_obj1280_COB_ID_Server_to_Client_Receive_SDO,32		; _mms_dict_Index1280[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1280[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1280[3]._bAccessType @ 384
	.bits	5,16			; _mms_dict_Index1280[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index1280[3]._size @ 416
	.bits	_mms_dict_obj1280_Node_ID_of_the_SDO_Server,32		; _mms_dict_Index1280[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1280[3]._bProcessor @ 480
	.space	16

$C$DW$441	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1280")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_mms_dict_Index1280")
	.dwattr $C$DW$441, DW_AT_location[DW_OP_addr _mms_dict_Index1280]
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$441, DW_AT_external
	.global	_ODP_Analogue_Output_Offset_Integer
_ODP_Analogue_Output_Offset_Integer:	.usect	".ebss",32,1,1
$C$DW$442	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Output_Offset_Integer")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_ODP_Analogue_Output_Offset_Integer")
	.dwattr $C$DW$442, DW_AT_location[DW_OP_addr _ODP_Analogue_Output_Offset_Integer]
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$442, DW_AT_external
	.global	_ODC_Gateway_callbacks
	.sect	".econst:_ODC_Gateway_callbacks"
	.clink
	.align	2
_ODC_Gateway_callbacks:
	.bits	0,32			; _ODC_Gateway_callbacks[0] @ 0
	.bits	0,32			; _ODC_Gateway_callbacks[1] @ 32
	.bits	0,32			; _ODC_Gateway_callbacks[2] @ 64
	.bits	0,32			; _ODC_Gateway_callbacks[3] @ 96
	.bits	0,32			; _ODC_Gateway_callbacks[4] @ 128
	.bits	0,32			; _ODC_Gateway_callbacks[5] @ 160
	.bits	0,32			; _ODC_Gateway_callbacks[6] @ 192
	.bits	0,32			; _ODC_Gateway_callbacks[7] @ 224
	.bits	0,32			; _ODC_Gateway_callbacks[8] @ 256
	.bits	0,32			; _ODC_Gateway_callbacks[9] @ 288
	.bits	_LogNBCallback,32		; _ODC_Gateway_callbacks[10] @ 320
	.bits	0,32			; _ODC_Gateway_callbacks[11] @ 352
	.bits	0,32			; _ODC_Gateway_callbacks[12] @ 384
	.bits	0,32			; _ODC_Gateway_callbacks[13] @ 416
	.bits	0,32			; _ODC_Gateway_callbacks[14] @ 448
	.bits	0,32			; _ODC_Gateway_callbacks[15] @ 480

$C$DW$443	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Gateway_callbacks")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_ODC_Gateway_callbacks")
	.dwattr $C$DW$443, DW_AT_location[DW_OP_addr _ODC_Gateway_callbacks]
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$443, DW_AT_external
	.global	_mms_dict_Index2006
	.sect	".econst:_mms_dict_Index2006"
	.clink
	.align	2
_mms_dict_Index2006:
	.bits	2,16			; _mms_dict_Index2006[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2006[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2006[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2006,32		; _mms_dict_Index2006[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2006[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index2006[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index2006[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index2006[1]._size @ 160
	.bits	_ODV_ErrorDsp_ErrorNumber,32		; _mms_dict_Index2006[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2006[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index2006[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index2006[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index2006[2]._size @ 288
	.bits	_ODV_ErrorDsp_WarningNumber,32		; _mms_dict_Index2006[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2006[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index2006[3]._bAccessType @ 384
	.bits	5,16			; _mms_dict_Index2006[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index2006[3]._size @ 416
	.bits	_ODV_ErrorDsp_ErrorLevel,32		; _mms_dict_Index2006[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2006[3]._bProcessor @ 480
	.space	16

$C$DW$444	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2006")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_mms_dict_Index2006")
	.dwattr $C$DW$444, DW_AT_location[DW_OP_addr _mms_dict_Index2006]
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$444, DW_AT_external
	.global	_ODP_Analogue_Output_SI_Unit
_ODP_Analogue_Output_SI_Unit:	.usect	".ebss",32,1,1
$C$DW$445	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Output_SI_Unit")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_ODP_Analogue_Output_SI_Unit")
	.dwattr $C$DW$445, DW_AT_location[DW_OP_addr _ODP_Analogue_Output_SI_Unit]
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$445, DW_AT_external
	.global	_ODP_Analogue_Output_Scaling_Float
_ODP_Analogue_Output_Scaling_Float:	.usect	".ebss",32,1,1
$C$DW$446	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Output_Scaling_Float")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_ODP_Analogue_Output_Scaling_Float")
	.dwattr $C$DW$446, DW_AT_location[DW_OP_addr _ODP_Analogue_Output_Scaling_Float]
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$164)
	.dwattr $C$DW$446, DW_AT_external
	.global	_mms_dict_Index2100
	.sect	".econst:_mms_dict_Index2100"
	.clink
	.align	2
_mms_dict_Index2100:
	.bits	2,16			; _mms_dict_Index2100[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2100[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2100[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2100,32		; _mms_dict_Index2100[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2100[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index2100[1]._bAccessType @ 128
	.bits	5,16			; _mms_dict_Index2100[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index2100[1]._size @ 160
	.bits	_ODV_RTC_Pos,32		; _mms_dict_Index2100[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2100[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index2100[2]._bAccessType @ 256
	.bits	27,16			; _mms_dict_Index2100[2]._bDataType @ 272
	.bits	8,32			; _mms_dict_Index2100[2]._size @ 288
	.bits	_ODV_RTC_Text,32		; _mms_dict_Index2100[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2100[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index2100[3]._bAccessType @ 384
	.bits	5,16			; _mms_dict_Index2100[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index2100[3]._size @ 416
	.bits	_ODV_RTC_Len,32		; _mms_dict_Index2100[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2100[3]._bProcessor @ 480
	.space	16

$C$DW$447	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2100")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_mms_dict_Index2100")
	.dwattr $C$DW$447, DW_AT_location[DW_OP_addr _mms_dict_Index2100]
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$447, DW_AT_external
	.global	_mms_dict_Index1600
	.sect	".econst:_mms_dict_Index1600"
	.clink
	.align	2
_mms_dict_Index1600:
	.bits	0,16			; _mms_dict_Index1600[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1600[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1600[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1600,32		; _mms_dict_Index1600[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1600[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1600[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1600[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1600[1]._size @ 160
	.bits	_mms_dict_obj1600,32		; _mms_dict_Index1600[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1600[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1600[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index1600[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index1600[2]._size @ 288
	.bits	_mms_dict_obj1600+2,32		; _mms_dict_Index1600[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1600[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1600[3]._bAccessType @ 384
	.bits	7,16			; _mms_dict_Index1600[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index1600[3]._size @ 416
	.bits	_mms_dict_obj1600+4,32		; _mms_dict_Index1600[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1600[3]._bProcessor @ 480
	.space	16

$C$DW$448	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1600")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_mms_dict_Index1600")
	.dwattr $C$DW$448, DW_AT_location[DW_OP_addr _mms_dict_Index1600]
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$448, DW_AT_external
	.global	_ODC_Write_Analogue_Output_16_Bit_callbacks
	.sect	".econst:_ODC_Write_Analogue_Output_16_Bit_callbacks"
	.clink
	.align	2
_ODC_Write_Analogue_Output_16_Bit_callbacks:
	.bits	0,32			; _ODC_Write_Analogue_Output_16_Bit_callbacks[0] @ 0
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[1] @ 32
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[2] @ 64
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[3] @ 96
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[4] @ 128
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[5] @ 160
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[6] @ 192
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[7] @ 224
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[8] @ 256
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[9] @ 288
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[10] @ 320
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[11] @ 352
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[12] @ 384
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[13] @ 416
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[14] @ 448
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[15] @ 480
	.bits	_WriteAnalogueOutputsCallback,32		; _ODC_Write_Analogue_Output_16_Bit_callbacks[16] @ 512

$C$DW$449	.dwtag  DW_TAG_variable, DW_AT_name("ODC_Write_Analogue_Output_16_Bit_callbacks")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_ODC_Write_Analogue_Output_16_Bit_callbacks")
	.dwattr $C$DW$449, DW_AT_location[DW_OP_addr _ODC_Write_Analogue_Output_16_Bit_callbacks]
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$449, DW_AT_external
	.global	_mms_dict_Index2013
	.sect	".econst:_mms_dict_Index2013"
	.clink
	.align	2
_mms_dict_Index2013:
	.bits	2,16			; _mms_dict_Index2013[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2013[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2013[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2013,32		; _mms_dict_Index2013[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2013[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index2013[1]._bAccessType @ 128
	.bits	2,16			; _mms_dict_Index2013[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index2013[1]._size @ 160
	.bits	_ODP_Heater_OnTemp,32		; _mms_dict_Index2013[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2013[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index2013[2]._bAccessType @ 256
	.bits	2,16			; _mms_dict_Index2013[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index2013[2]._size @ 288
	.bits	_ODP_Heater_OffTemp,32		; _mms_dict_Index2013[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2013[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index2013[3]._bAccessType @ 384
	.bits	3,16			; _mms_dict_Index2013[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index2013[3]._size @ 416
	.bits	_ODP_Heater_Off_Cell_Voltage,32		; _mms_dict_Index2013[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2013[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index2013[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index2013[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index2013[4]._size @ 544
	.bits	_ODV_Heater_Heater_Status,32		; _mms_dict_Index2013[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2013[4]._bProcessor @ 608
	.space	16

$C$DW$450	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2013")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_mms_dict_Index2013")
	.dwattr $C$DW$450, DW_AT_location[DW_OP_addr _mms_dict_Index2013]
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$171)
	.dwattr $C$DW$450, DW_AT_external
	.global	_mms_dict_Index1018
	.sect	".econst:_mms_dict_Index1018"
	.clink
	.align	2
_mms_dict_Index1018:
	.bits	2,16			; _mms_dict_Index1018[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1018[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1018[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1018,32		; _mms_dict_Index1018[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1018[0]._bProcessor @ 96
	.space	16
	.bits	2,16			; _mms_dict_Index1018[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1018[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1018[1]._size @ 160
	.bits	_mms_dict_obj1018_Vendor_ID,32		; _mms_dict_Index1018[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1018[1]._bProcessor @ 224
	.space	16
	.bits	2,16			; _mms_dict_Index1018[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index1018[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index1018[2]._size @ 288
	.bits	_mms_dict_obj1018_Product_Code,32		; _mms_dict_Index1018[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1018[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1018[3]._bAccessType @ 384
	.bits	7,16			; _mms_dict_Index1018[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index1018[3]._size @ 416
	.bits	_mms_dict_obj1018_Revision_Number,32		; _mms_dict_Index1018[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1018[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1018[4]._bAccessType @ 512
	.bits	7,16			; _mms_dict_Index1018[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index1018[4]._size @ 544
	.bits	_mms_dict_obj1018_Serial_Number,32		; _mms_dict_Index1018[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1018[4]._bProcessor @ 608
	.space	16

$C$DW$451	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1018")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_mms_dict_Index1018")
	.dwattr $C$DW$451, DW_AT_location[DW_OP_addr _mms_dict_Index1018]
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$171)
	.dwattr $C$DW$451, DW_AT_external
	.global	_ODP_Analogue_Input_Scaling_Float
_ODP_Analogue_Input_Scaling_Float:	.usect	".ebss",46,1,1
$C$DW$452	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Input_Scaling_Float")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_ODP_Analogue_Input_Scaling_Float")
	.dwattr $C$DW$452, DW_AT_location[DW_OP_addr _ODP_Analogue_Input_Scaling_Float]
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$452, DW_AT_external
	.global	_ODP_Analogue_Input_SI_unit
_ODP_Analogue_Input_SI_unit:	.usect	".ebss",46,1,1
$C$DW$453	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Input_SI_unit")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_ODP_Analogue_Input_SI_unit")
	.dwattr $C$DW$453, DW_AT_location[DW_OP_addr _ODP_Analogue_Input_SI_unit]
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$453, DW_AT_external
	.global	_ODP_Analogue_Input_Offset_Integer
_ODP_Analogue_Input_Offset_Integer:	.usect	".ebss",46,1,1
$C$DW$454	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Input_Offset_Integer")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_ODP_Analogue_Input_Offset_Integer")
	.dwattr $C$DW$454, DW_AT_location[DW_OP_addr _ODP_Analogue_Input_Offset_Integer]
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$454, DW_AT_external
	.global	_mms_dict_Index1A02
	.sect	".econst:_mms_dict_Index1A02"
	.clink
	.align	2
_mms_dict_Index1A02:
	.bits	0,16			; _mms_dict_Index1A02[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1A02[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1A02[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1A02,32		; _mms_dict_Index1A02[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1A02[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1A02[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1A02[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1A02[1]._size @ 160
	.bits	_mms_dict_obj1A02,32		; _mms_dict_Index1A02[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1A02[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1A02[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index1A02[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index1A02[2]._size @ 288
	.bits	_mms_dict_obj1A02+2,32		; _mms_dict_Index1A02[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1A02[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1A02[3]._bAccessType @ 384
	.bits	7,16			; _mms_dict_Index1A02[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index1A02[3]._size @ 416
	.bits	_mms_dict_obj1A02+4,32		; _mms_dict_Index1A02[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1A02[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1A02[4]._bAccessType @ 512
	.bits	7,16			; _mms_dict_Index1A02[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index1A02[4]._size @ 544
	.bits	_mms_dict_obj1A02+6,32		; _mms_dict_Index1A02[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1A02[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index1A02[5]._bAccessType @ 640
	.bits	7,16			; _mms_dict_Index1A02[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index1A02[5]._size @ 672
	.bits	_mms_dict_obj1A02+8,32		; _mms_dict_Index1A02[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index1A02[5]._bProcessor @ 736
	.space	16

$C$DW$455	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1A02")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_mms_dict_Index1A02")
	.dwattr $C$DW$455, DW_AT_location[DW_OP_addr _mms_dict_Index1A02]
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$455, DW_AT_external
	.global	_mms_dict_Index1803
	.sect	".econst:_mms_dict_Index1803"
	.clink
	.align	2
_mms_dict_Index1803:
	.bits	2,16			; _mms_dict_Index1803[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1803[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1803[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1803,32		; _mms_dict_Index1803[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1803[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1803[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1803[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1803[1]._size @ 160
	.bits	_mms_dict_obj1803_COB_ID_used_by_PDO,32		; _mms_dict_Index1803[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1803[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1803[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index1803[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index1803[2]._size @ 288
	.bits	_mms_dict_obj1803_Transmission_Type,32		; _mms_dict_Index1803[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1803[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1803[3]._bAccessType @ 384
	.bits	6,16			; _mms_dict_Index1803[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index1803[3]._size @ 416
	.bits	_mms_dict_obj1803_Inhibit_Time,32		; _mms_dict_Index1803[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1803[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1803[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index1803[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index1803[4]._size @ 544
	.bits	_mms_dict_obj1803_Compatibility_Entry,32		; _mms_dict_Index1803[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1803[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index1803[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index1803[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index1803[5]._size @ 672
	.bits	_mms_dict_obj1803_Event_Timer,32		; _mms_dict_Index1803[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index1803[5]._bProcessor @ 736
	.space	16

$C$DW$456	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1803")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_mms_dict_Index1803")
	.dwattr $C$DW$456, DW_AT_location[DW_OP_addr _mms_dict_Index1803]
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$456, DW_AT_external
	.global	_mms_dict_Index1800
	.sect	".econst:_mms_dict_Index1800"
	.clink
	.align	2
_mms_dict_Index1800:
	.bits	2,16			; _mms_dict_Index1800[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1800[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1800[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1800,32		; _mms_dict_Index1800[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1800[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1800[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1800[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1800[1]._size @ 160
	.bits	_mms_dict_obj1800_COB_ID_used_by_PDO,32		; _mms_dict_Index1800[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1800[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1800[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index1800[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index1800[2]._size @ 288
	.bits	_mms_dict_obj1800_Transmission_Type,32		; _mms_dict_Index1800[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1800[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1800[3]._bAccessType @ 384
	.bits	6,16			; _mms_dict_Index1800[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index1800[3]._size @ 416
	.bits	_mms_dict_obj1800_Inhibit_Time,32		; _mms_dict_Index1800[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1800[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1800[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index1800[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index1800[4]._size @ 544
	.bits	_mms_dict_obj1800_Compatibility_Entry,32		; _mms_dict_Index1800[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1800[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index1800[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index1800[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index1800[5]._size @ 672
	.bits	_mms_dict_obj1800_Event_Timer,32		; _mms_dict_Index1800[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index1800[5]._bProcessor @ 736
	.space	16

$C$DW$457	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1800")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_mms_dict_Index1800")
	.dwattr $C$DW$457, DW_AT_location[DW_OP_addr _mms_dict_Index1800]
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$457, DW_AT_external
	.global	_mms_dict_Index1400
	.sect	".econst:_mms_dict_Index1400"
	.clink
	.align	2
_mms_dict_Index1400:
	.bits	2,16			; _mms_dict_Index1400[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1400[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1400[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1400,32		; _mms_dict_Index1400[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1400[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1400[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1400[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1400[1]._size @ 160
	.bits	_mms_dict_obj1400_COB_ID_used_by_PDO,32		; _mms_dict_Index1400[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1400[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1400[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index1400[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index1400[2]._size @ 288
	.bits	_mms_dict_obj1400_Transmission_Type,32		; _mms_dict_Index1400[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1400[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1400[3]._bAccessType @ 384
	.bits	6,16			; _mms_dict_Index1400[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index1400[3]._size @ 416
	.bits	_mms_dict_obj1400_Inhibit_Time,32		; _mms_dict_Index1400[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1400[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1400[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index1400[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index1400[4]._size @ 544
	.bits	_mms_dict_obj1400_Compatibility_Entry,32		; _mms_dict_Index1400[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1400[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index1400[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index1400[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index1400[5]._size @ 672
	.bits	_mms_dict_obj1400_Event_Timer,32		; _mms_dict_Index1400[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index1400[5]._bProcessor @ 736
	.space	16

$C$DW$458	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1400")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_mms_dict_Index1400")
	.dwattr $C$DW$458, DW_AT_location[DW_OP_addr _mms_dict_Index1400]
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$458, DW_AT_external
	.global	_mms_dict_Index1802
	.sect	".econst:_mms_dict_Index1802"
	.clink
	.align	2
_mms_dict_Index1802:
	.bits	2,16			; _mms_dict_Index1802[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1802[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1802[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1802,32		; _mms_dict_Index1802[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1802[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1802[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1802[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1802[1]._size @ 160
	.bits	_mms_dict_obj1802_COB_ID_used_by_PDO,32		; _mms_dict_Index1802[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1802[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1802[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index1802[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index1802[2]._size @ 288
	.bits	_mms_dict_obj1802_Transmission_Type,32		; _mms_dict_Index1802[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1802[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1802[3]._bAccessType @ 384
	.bits	6,16			; _mms_dict_Index1802[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index1802[3]._size @ 416
	.bits	_mms_dict_obj1802_Inhibit_Time,32		; _mms_dict_Index1802[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1802[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1802[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index1802[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index1802[4]._size @ 544
	.bits	_mms_dict_obj1802_Compatibility_Entry,32		; _mms_dict_Index1802[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1802[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index1802[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index1802[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index1802[5]._size @ 672
	.bits	_mms_dict_obj1802_Event_Timer,32		; _mms_dict_Index1802[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index1802[5]._bProcessor @ 736
	.space	16

$C$DW$459	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1802")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_mms_dict_Index1802")
	.dwattr $C$DW$459, DW_AT_location[DW_OP_addr _mms_dict_Index1802]
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$459, DW_AT_external
	.global	_mms_dict_Index1801
	.sect	".econst:_mms_dict_Index1801"
	.clink
	.align	2
_mms_dict_Index1801:
	.bits	2,16			; _mms_dict_Index1801[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1801[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1801[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1801,32		; _mms_dict_Index1801[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1801[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1801[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1801[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1801[1]._size @ 160
	.bits	_mms_dict_obj1801_COB_ID_used_by_PDO,32		; _mms_dict_Index1801[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1801[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1801[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index1801[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index1801[2]._size @ 288
	.bits	_mms_dict_obj1801_Transmission_Type,32		; _mms_dict_Index1801[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1801[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1801[3]._bAccessType @ 384
	.bits	6,16			; _mms_dict_Index1801[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index1801[3]._size @ 416
	.bits	_mms_dict_obj1801_Inhibit_Time,32		; _mms_dict_Index1801[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1801[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1801[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index1801[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index1801[4]._size @ 544
	.bits	_mms_dict_obj1801_Compatibility_Entry,32		; _mms_dict_Index1801[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1801[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index1801[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index1801[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index1801[5]._size @ 672
	.bits	_mms_dict_obj1801_Event_Timer,32		; _mms_dict_Index1801[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index1801[5]._bProcessor @ 736
	.space	16

$C$DW$460	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1801")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_mms_dict_Index1801")
	.dwattr $C$DW$460, DW_AT_location[DW_OP_addr _mms_dict_Index1801]
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$460, DW_AT_external
	.global	_mms_dict_Index2011
	.sect	".econst:_mms_dict_Index2011"
	.clink
	.align	2
_mms_dict_Index2011:
	.bits	2,16			; _mms_dict_Index2011[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2011[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2011[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2011,32		; _mms_dict_Index2011[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2011[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index2011[1]._bAccessType @ 128
	.bits	3,16			; _mms_dict_Index2011[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index2011[1]._size @ 160
	.bits	_ODV_Current_Min,32		; _mms_dict_Index2011[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2011[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index2011[2]._bAccessType @ 256
	.bits	3,16			; _mms_dict_Index2011[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index2011[2]._size @ 288
	.bits	_ODV_Current_Max,32		; _mms_dict_Index2011[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2011[2]._bProcessor @ 352
	.space	16
	.bits	2,16			; _mms_dict_Index2011[3]._bAccessType @ 384
	.bits	6,16			; _mms_dict_Index2011[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index2011[3]._size @ 416
	.bits	_ODV_Current_ChargeAllowed,32		; _mms_dict_Index2011[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2011[3]._bProcessor @ 480
	.space	16
	.bits	2,16			; _mms_dict_Index2011[4]._bAccessType @ 512
	.bits	6,16			; _mms_dict_Index2011[4]._bDataType @ 528
	.bits	2,32			; _mms_dict_Index2011[4]._size @ 544
	.bits	_ODV_Current_DischargeAllowed,32		; _mms_dict_Index2011[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2011[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index2011[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index2011[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index2011[5]._size @ 672
	.bits	_ODP_Current_C_D_Mode,32		; _mms_dict_Index2011[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2011[5]._bProcessor @ 736
	.space	16

$C$DW$461	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2011")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_mms_dict_Index2011")
	.dwattr $C$DW$461, DW_AT_location[DW_OP_addr _mms_dict_Index2011]
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$461, DW_AT_external
	.global	_mms_dict_Index1A03
	.sect	".econst:_mms_dict_Index1A03"
	.clink
	.align	2
_mms_dict_Index1A03:
	.bits	0,16			; _mms_dict_Index1A03[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1A03[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1A03[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1A03,32		; _mms_dict_Index1A03[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1A03[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1A03[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1A03[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1A03[1]._size @ 160
	.bits	_mms_dict_obj1A03,32		; _mms_dict_Index1A03[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1A03[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1A03[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index1A03[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index1A03[2]._size @ 288
	.bits	_mms_dict_obj1A03+2,32		; _mms_dict_Index1A03[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1A03[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1A03[3]._bAccessType @ 384
	.bits	7,16			; _mms_dict_Index1A03[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index1A03[3]._size @ 416
	.bits	_mms_dict_obj1A03+4,32		; _mms_dict_Index1A03[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1A03[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1A03[4]._bAccessType @ 512
	.bits	7,16			; _mms_dict_Index1A03[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index1A03[4]._size @ 544
	.bits	_mms_dict_obj1A03+6,32		; _mms_dict_Index1A03[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1A03[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index1A03[5]._bAccessType @ 640
	.bits	7,16			; _mms_dict_Index1A03[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index1A03[5]._size @ 672
	.bits	_mms_dict_obj1A03+8,32		; _mms_dict_Index1A03[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index1A03[5]._bProcessor @ 736
	.space	16

$C$DW$462	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1A03")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_mms_dict_Index1A03")
	.dwattr $C$DW$462, DW_AT_location[DW_OP_addr _mms_dict_Index1A03]
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$462, DW_AT_external
	.global	_mms_dict_Index2023
	.sect	".econst:_mms_dict_Index2023"
	.clink
	.align	2
_mms_dict_Index2023:
	.bits	2,16			; _mms_dict_Index2023[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2023[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2023[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2023,32		; _mms_dict_Index2023[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2023[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index2023[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index2023[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index2023[1]._size @ 160
	.bits	_ODV_CommError_Counter,32		; _mms_dict_Index2023[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2023[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index2023[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index2023[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index2023[2]._size @ 288
	.bits	_ODV_CommError_StateCounter,32		; _mms_dict_Index2023[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2023[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index2023[3]._bAccessType @ 384
	.bits	1,16			; _mms_dict_Index2023[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index2023[3]._size @ 416
	.bits	_ODV_CommError_Set,32		; _mms_dict_Index2023[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2023[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index2023[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index2023[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index2023[4]._size @ 544
	.bits	_ODP_CommError_TimeOut,32		; _mms_dict_Index2023[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2023[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index2023[5]._bAccessType @ 640
	.bits	5,16			; _mms_dict_Index2023[5]._bDataType @ 656
	.bits	1,32			; _mms_dict_Index2023[5]._size @ 672
	.bits	_ODP_CommError_Delay,32		; _mms_dict_Index2023[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2023[5]._bProcessor @ 736
	.space	16

$C$DW$463	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2023")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_mms_dict_Index2023")
	.dwattr $C$DW$463, DW_AT_location[DW_OP_addr _mms_dict_Index2023]
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$463, DW_AT_external
	.global	_mms_dict_PDO_status
_mms_dict_PDO_status:	.usect	".ebss",56,1,0
$C$DW$464	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_PDO_status")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_mms_dict_PDO_status")
	.dwattr $C$DW$464, DW_AT_location[DW_OP_addr _mms_dict_PDO_status]
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$464, DW_AT_external
	.global	_mms_dict_Index2105
	.sect	".econst:_mms_dict_Index2105"
	.clink
	.align	2
_mms_dict_Index2105:
	.bits	2,16			; _mms_dict_Index2105[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2105[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2105[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2105,32		; _mms_dict_Index2105[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2105[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index2105[1]._bAccessType @ 128
	.bits	8,16			; _mms_dict_Index2105[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index2105[1]._size @ 160
	.bits	_ODP_Battery_Capacity,32		; _mms_dict_Index2105[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2105[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index2105[2]._bAccessType @ 256
	.bits	6,16			; _mms_dict_Index2105[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index2105[2]._size @ 288
	.bits	_ODP_Battery_kWh,32		; _mms_dict_Index2105[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2105[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index2105[3]._bAccessType @ 384
	.bits	6,16			; _mms_dict_Index2105[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index2105[3]._size @ 416
	.bits	_ODP_Battery_kWh_Life,32		; _mms_dict_Index2105[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2105[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index2105[4]._bAccessType @ 512
	.bits	6,16			; _mms_dict_Index2105[4]._bDataType @ 528
	.bits	2,32			; _mms_dict_Index2105[4]._size @ 544
	.bits	_ODP_Battery_Cycles,32		; _mms_dict_Index2105[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2105[4]._bProcessor @ 608
	.space	16
	.bits	2,16			; _mms_dict_Index2105[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index2105[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index2105[5]._size @ 672
	.bits	_ODV_Battery_Total_Cycles,32		; _mms_dict_Index2105[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2105[5]._bProcessor @ 736
	.space	16
	.bits	2,16			; _mms_dict_Index2105[6]._bAccessType @ 768
	.bits	6,16			; _mms_dict_Index2105[6]._bDataType @ 784
	.bits	2,32			; _mms_dict_Index2105[6]._size @ 800
	.bits	_ODV_Battery_Total_ChargedkWh,32		; _mms_dict_Index2105[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index2105[6]._bProcessor @ 864
	.space	16

$C$DW$465	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2105")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_mms_dict_Index2105")
	.dwattr $C$DW$465, DW_AT_location[DW_OP_addr _mms_dict_Index2105]
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$465, DW_AT_external
	.global	_mms_dict_Index2015
	.sect	".econst:_mms_dict_Index2015"
	.clink
	.align	2
_mms_dict_Index2015:
	.bits	2,16			; _mms_dict_Index2015[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2015[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2015[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2015,32		; _mms_dict_Index2015[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2015[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index2015[1]._bAccessType @ 128
	.bits	6,16			; _mms_dict_Index2015[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index2015[1]._size @ 160
	.bits	_ODV_Master_Module1_Voltage,32		; _mms_dict_Index2015[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2015[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index2015[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index2015[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index2015[2]._size @ 288
	.bits	_ODV_Master_Module1_Error,32		; _mms_dict_Index2015[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2015[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index2015[3]._bAccessType @ 384
	.bits	2,16			; _mms_dict_Index2015[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index2015[3]._size @ 416
	.bits	_ODV_Master_Module1_Temp,32		; _mms_dict_Index2015[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2015[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index2015[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index2015[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index2015[4]._size @ 544
	.bits	_ODV_Master_Module1_Undefined1,32		; _mms_dict_Index2015[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2015[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index2015[5]._bAccessType @ 640
	.bits	5,16			; _mms_dict_Index2015[5]._bDataType @ 656
	.bits	1,32			; _mms_dict_Index2015[5]._size @ 672
	.bits	_ODV_Master_Module1_Undefined2,32		; _mms_dict_Index2015[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2015[5]._bProcessor @ 736
	.space	16
	.bits	0,16			; _mms_dict_Index2015[6]._bAccessType @ 768
	.bits	5,16			; _mms_dict_Index2015[6]._bDataType @ 784
	.bits	1,32			; _mms_dict_Index2015[6]._size @ 800
	.bits	_ODV_Master_Module1_Undefined3,32		; _mms_dict_Index2015[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index2015[6]._bProcessor @ 864
	.space	16

$C$DW$466	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2015")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_mms_dict_Index2015")
	.dwattr $C$DW$466, DW_AT_location[DW_OP_addr _mms_dict_Index2015]
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$466, DW_AT_external
	.global	_mms_dict_Index1A00
	.sect	".econst:_mms_dict_Index1A00"
	.clink
	.align	2
_mms_dict_Index1A00:
	.bits	0,16			; _mms_dict_Index1A00[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1A00[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1A00[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1A00,32		; _mms_dict_Index1A00[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1A00[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1A00[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1A00[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1A00[1]._size @ 160
	.bits	_mms_dict_obj1A00,32		; _mms_dict_Index1A00[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1A00[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1A00[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index1A00[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index1A00[2]._size @ 288
	.bits	_mms_dict_obj1A00+2,32		; _mms_dict_Index1A00[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1A00[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1A00[3]._bAccessType @ 384
	.bits	7,16			; _mms_dict_Index1A00[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index1A00[3]._size @ 416
	.bits	_mms_dict_obj1A00+4,32		; _mms_dict_Index1A00[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1A00[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1A00[4]._bAccessType @ 512
	.bits	7,16			; _mms_dict_Index1A00[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index1A00[4]._size @ 544
	.bits	_mms_dict_obj1A00+6,32		; _mms_dict_Index1A00[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1A00[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index1A00[5]._bAccessType @ 640
	.bits	7,16			; _mms_dict_Index1A00[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index1A00[5]._size @ 672
	.bits	_mms_dict_obj1A00+8,32		; _mms_dict_Index1A00[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index1A00[5]._bProcessor @ 736
	.space	16
	.bits	0,16			; _mms_dict_Index1A00[6]._bAccessType @ 768
	.bits	7,16			; _mms_dict_Index1A00[6]._bDataType @ 784
	.bits	4,32			; _mms_dict_Index1A00[6]._size @ 800
	.bits	_mms_dict_obj1A00+10,32		; _mms_dict_Index1A00[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index1A00[6]._bProcessor @ 864
	.space	16
	.bits	0,16			; _mms_dict_Index1A00[7]._bAccessType @ 896
	.bits	7,16			; _mms_dict_Index1A00[7]._bDataType @ 912
	.bits	4,32			; _mms_dict_Index1A00[7]._size @ 928
	.bits	_mms_dict_obj1A00+12,32		; _mms_dict_Index1A00[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index1A00[7]._bProcessor @ 992
	.space	16

$C$DW$467	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1A00")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_mms_dict_Index1A00")
	.dwattr $C$DW$467, DW_AT_location[DW_OP_addr _mms_dict_Index1A00]
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$467, DW_AT_external
	.global	_mms_dict_Index2012
	.sect	".econst:_mms_dict_Index2012"
	.clink
	.align	2
_mms_dict_Index2012:
	.bits	2,16			; _mms_dict_Index2012[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2012[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2012[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2012,32		; _mms_dict_Index2012[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2012[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index2012[1]._bAccessType @ 128
	.bits	6,16			; _mms_dict_Index2012[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index2012[1]._size @ 160
	.bits	_ODP_Contactor_Setup,32		; _mms_dict_Index2012[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2012[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index2012[2]._bAccessType @ 256
	.bits	6,16			; _mms_dict_Index2012[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index2012[2]._size @ 288
	.bits	_ODP_Contactor_Hold,32		; _mms_dict_Index2012[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2012[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index2012[3]._bAccessType @ 384
	.bits	6,16			; _mms_dict_Index2012[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index2012[3]._size @ 416
	.bits	_ODP_Contactor_Overcurrent,32		; _mms_dict_Index2012[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2012[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index2012[4]._bAccessType @ 512
	.bits	2,16			; _mms_dict_Index2012[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index2012[4]._size @ 544
	.bits	_ODP_Contactor_Slope,32		; _mms_dict_Index2012[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2012[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index2012[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index2012[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index2012[5]._size @ 672
	.bits	_ODP_Contactor_Undercurrent,32		; _mms_dict_Index2012[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2012[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index2012[6]._bAccessType @ 768
	.bits	5,16			; _mms_dict_Index2012[6]._bDataType @ 784
	.bits	1,32			; _mms_dict_Index2012[6]._size @ 800
	.bits	_ODP_Contactor_Delay,32		; _mms_dict_Index2012[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index2012[6]._bProcessor @ 864
	.space	16
	.bits	0,16			; _mms_dict_Index2012[7]._bAccessType @ 896
	.bits	5,16			; _mms_dict_Index2012[7]._bDataType @ 912
	.bits	1,32			; _mms_dict_Index2012[7]._size @ 928
	.bits	_ODV_Contactor_Undefined,32		; _mms_dict_Index2012[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index2012[7]._bProcessor @ 992
	.space	16

$C$DW$468	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2012")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_mms_dict_Index2012")
	.dwattr $C$DW$468, DW_AT_location[DW_OP_addr _mms_dict_Index2012]
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$468, DW_AT_external
	.global	_mms_dict_Index200C
	.sect	".econst:_mms_dict_Index200C"
	.clink
	.align	2
_mms_dict_Index200C:
	.bits	2,16			; _mms_dict_Index200C[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index200C[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index200C[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj200C,32		; _mms_dict_Index200C[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index200C[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index200C[1]._bAccessType @ 128
	.bits	5,16			; _mms_dict_Index200C[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index200C[1]._size @ 160
	.bits	_ODP_Power_DischargeLimits,32		; _mms_dict_Index200C[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index200C[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index200C[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index200C[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index200C[2]._size @ 288
	.bits	_ODP_Power_DischargeLimits+1,32		; _mms_dict_Index200C[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index200C[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index200C[3]._bAccessType @ 384
	.bits	5,16			; _mms_dict_Index200C[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index200C[3]._size @ 416
	.bits	_ODP_Power_DischargeLimits+2,32		; _mms_dict_Index200C[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index200C[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index200C[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index200C[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index200C[4]._size @ 544
	.bits	_ODP_Power_DischargeLimits+3,32		; _mms_dict_Index200C[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index200C[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index200C[5]._bAccessType @ 640
	.bits	5,16			; _mms_dict_Index200C[5]._bDataType @ 656
	.bits	1,32			; _mms_dict_Index200C[5]._size @ 672
	.bits	_ODP_Power_DischargeLimits+4,32		; _mms_dict_Index200C[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index200C[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index200C[6]._bAccessType @ 768
	.bits	5,16			; _mms_dict_Index200C[6]._bDataType @ 784
	.bits	1,32			; _mms_dict_Index200C[6]._size @ 800
	.bits	_ODP_Power_DischargeLimits+5,32		; _mms_dict_Index200C[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index200C[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index200C[7]._bAccessType @ 896
	.bits	5,16			; _mms_dict_Index200C[7]._bDataType @ 912
	.bits	1,32			; _mms_dict_Index200C[7]._size @ 928
	.bits	_ODP_Power_DischargeLimits+6,32		; _mms_dict_Index200C[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index200C[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index200C[8]._bAccessType @ 1024
	.bits	5,16			; _mms_dict_Index200C[8]._bDataType @ 1040
	.bits	1,32			; _mms_dict_Index200C[8]._size @ 1056
	.bits	_ODP_Power_DischargeLimits+7,32		; _mms_dict_Index200C[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index200C[8]._bProcessor @ 1120
	.space	16

$C$DW$469	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index200C")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_mms_dict_Index200C")
	.dwattr $C$DW$469, DW_AT_location[DW_OP_addr _mms_dict_Index200C]
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$469, DW_AT_external
	.global	_mms_dict_Index2014
	.sect	".econst:_mms_dict_Index2014"
	.clink
	.align	2
_mms_dict_Index2014:
	.bits	2,16			; _mms_dict_Index2014[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2014[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2014[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2014,32		; _mms_dict_Index2014[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2014[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index2014[1]._bAccessType @ 128
	.bits	5,16			; _mms_dict_Index2014[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index2014[1]._size @ 160
	.bits	_ODP_Unused_Undefined1,32		; _mms_dict_Index2014[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2014[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index2014[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index2014[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index2014[2]._size @ 288
	.bits	_ODP_Unused_Undefined2,32		; _mms_dict_Index2014[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2014[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index2014[3]._bAccessType @ 384
	.bits	5,16			; _mms_dict_Index2014[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index2014[3]._size @ 416
	.bits	_ODP_Unused_Undefined3,32		; _mms_dict_Index2014[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2014[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index2014[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index2014[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index2014[4]._size @ 544
	.bits	_ODP_Unused_Undefined4,32		; _mms_dict_Index2014[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2014[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index2014[5]._bAccessType @ 640
	.bits	5,16			; _mms_dict_Index2014[5]._bDataType @ 656
	.bits	1,32			; _mms_dict_Index2014[5]._size @ 672
	.bits	_ODP_Unused_Undefined5,32		; _mms_dict_Index2014[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2014[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index2014[6]._bAccessType @ 768
	.bits	5,16			; _mms_dict_Index2014[6]._bDataType @ 784
	.bits	1,32			; _mms_dict_Index2014[6]._size @ 800
	.bits	_ODP_Unused_Undefined6,32		; _mms_dict_Index2014[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index2014[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index2014[7]._bAccessType @ 896
	.bits	5,16			; _mms_dict_Index2014[7]._bDataType @ 912
	.bits	1,32			; _mms_dict_Index2014[7]._size @ 928
	.bits	_ODP_Unused_Undefined7,32		; _mms_dict_Index2014[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index2014[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index2014[8]._bAccessType @ 1024
	.bits	5,16			; _mms_dict_Index2014[8]._bDataType @ 1040
	.bits	1,32			; _mms_dict_Index2014[8]._size @ 1056
	.bits	_ODP_Unused_Undefined8,32		; _mms_dict_Index2014[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index2014[8]._bProcessor @ 1120
	.space	16

$C$DW$470	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2014")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_mms_dict_Index2014")
	.dwattr $C$DW$470, DW_AT_location[DW_OP_addr _mms_dict_Index2014]
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$470, DW_AT_external
	.global	_mms_dict_Index200D
	.sect	".econst:_mms_dict_Index200D"
	.clink
	.align	2
_mms_dict_Index200D:
	.bits	2,16			; _mms_dict_Index200D[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index200D[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index200D[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj200D,32		; _mms_dict_Index200D[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index200D[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index200D[1]._bAccessType @ 128
	.bits	2,16			; _mms_dict_Index200D[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index200D[1]._size @ 160
	.bits	_ODP_Temperature_DischargeLimits,32		; _mms_dict_Index200D[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index200D[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index200D[2]._bAccessType @ 256
	.bits	2,16			; _mms_dict_Index200D[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index200D[2]._size @ 288
	.bits	_ODP_Temperature_DischargeLimits+1,32		; _mms_dict_Index200D[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index200D[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index200D[3]._bAccessType @ 384
	.bits	2,16			; _mms_dict_Index200D[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index200D[3]._size @ 416
	.bits	_ODP_Temperature_DischargeLimits+2,32		; _mms_dict_Index200D[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index200D[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index200D[4]._bAccessType @ 512
	.bits	2,16			; _mms_dict_Index200D[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index200D[4]._size @ 544
	.bits	_ODP_Temperature_DischargeLimits+3,32		; _mms_dict_Index200D[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index200D[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index200D[5]._bAccessType @ 640
	.bits	2,16			; _mms_dict_Index200D[5]._bDataType @ 656
	.bits	1,32			; _mms_dict_Index200D[5]._size @ 672
	.bits	_ODP_Temperature_DischargeLimits+4,32		; _mms_dict_Index200D[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index200D[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index200D[6]._bAccessType @ 768
	.bits	2,16			; _mms_dict_Index200D[6]._bDataType @ 784
	.bits	1,32			; _mms_dict_Index200D[6]._size @ 800
	.bits	_ODP_Temperature_DischargeLimits+5,32		; _mms_dict_Index200D[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index200D[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index200D[7]._bAccessType @ 896
	.bits	2,16			; _mms_dict_Index200D[7]._bDataType @ 912
	.bits	1,32			; _mms_dict_Index200D[7]._size @ 928
	.bits	_ODP_Temperature_DischargeLimits+6,32		; _mms_dict_Index200D[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index200D[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index200D[8]._bAccessType @ 1024
	.bits	2,16			; _mms_dict_Index200D[8]._bDataType @ 1040
	.bits	1,32			; _mms_dict_Index200D[8]._size @ 1056
	.bits	_ODP_Temperature_DischargeLimits+7,32		; _mms_dict_Index200D[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index200D[8]._bProcessor @ 1120
	.space	16

$C$DW$471	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index200D")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_mms_dict_Index200D")
	.dwattr $C$DW$471, DW_AT_location[DW_OP_addr _mms_dict_Index200D]
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$471, DW_AT_external
	.global	_mms_dict_Index200A
	.sect	".econst:_mms_dict_Index200A"
	.clink
	.align	2
_mms_dict_Index200A:
	.bits	2,16			; _mms_dict_Index200A[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index200A[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index200A[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj200A,32		; _mms_dict_Index200A[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index200A[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index200A[1]._bAccessType @ 128
	.bits	5,16			; _mms_dict_Index200A[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index200A[1]._size @ 160
	.bits	_ODP_Power_ChargeLimits,32		; _mms_dict_Index200A[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index200A[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index200A[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index200A[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index200A[2]._size @ 288
	.bits	_ODP_Power_ChargeLimits+1,32		; _mms_dict_Index200A[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index200A[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index200A[3]._bAccessType @ 384
	.bits	5,16			; _mms_dict_Index200A[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index200A[3]._size @ 416
	.bits	_ODP_Power_ChargeLimits+2,32		; _mms_dict_Index200A[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index200A[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index200A[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index200A[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index200A[4]._size @ 544
	.bits	_ODP_Power_ChargeLimits+3,32		; _mms_dict_Index200A[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index200A[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index200A[5]._bAccessType @ 640
	.bits	5,16			; _mms_dict_Index200A[5]._bDataType @ 656
	.bits	1,32			; _mms_dict_Index200A[5]._size @ 672
	.bits	_ODP_Power_ChargeLimits+4,32		; _mms_dict_Index200A[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index200A[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index200A[6]._bAccessType @ 768
	.bits	5,16			; _mms_dict_Index200A[6]._bDataType @ 784
	.bits	1,32			; _mms_dict_Index200A[6]._size @ 800
	.bits	_ODP_Power_ChargeLimits+5,32		; _mms_dict_Index200A[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index200A[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index200A[7]._bAccessType @ 896
	.bits	5,16			; _mms_dict_Index200A[7]._bDataType @ 912
	.bits	1,32			; _mms_dict_Index200A[7]._size @ 928
	.bits	_ODP_Power_ChargeLimits+6,32		; _mms_dict_Index200A[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index200A[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index200A[8]._bAccessType @ 1024
	.bits	5,16			; _mms_dict_Index200A[8]._bDataType @ 1040
	.bits	1,32			; _mms_dict_Index200A[8]._size @ 1056
	.bits	_ODP_Power_ChargeLimits+7,32		; _mms_dict_Index200A[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index200A[8]._bProcessor @ 1120
	.space	16

$C$DW$472	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index200A")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_mms_dict_Index200A")
	.dwattr $C$DW$472, DW_AT_location[DW_OP_addr _mms_dict_Index200A]
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$472, DW_AT_external
	.global	_mms_dict_Index200B
	.sect	".econst:_mms_dict_Index200B"
	.clink
	.align	2
_mms_dict_Index200B:
	.bits	2,16			; _mms_dict_Index200B[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index200B[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index200B[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj200B,32		; _mms_dict_Index200B[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index200B[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index200B[1]._bAccessType @ 128
	.bits	2,16			; _mms_dict_Index200B[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index200B[1]._size @ 160
	.bits	_ODP_Temperature_ChargeLimits,32		; _mms_dict_Index200B[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index200B[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index200B[2]._bAccessType @ 256
	.bits	2,16			; _mms_dict_Index200B[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index200B[2]._size @ 288
	.bits	_ODP_Temperature_ChargeLimits+1,32		; _mms_dict_Index200B[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index200B[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index200B[3]._bAccessType @ 384
	.bits	2,16			; _mms_dict_Index200B[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index200B[3]._size @ 416
	.bits	_ODP_Temperature_ChargeLimits+2,32		; _mms_dict_Index200B[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index200B[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index200B[4]._bAccessType @ 512
	.bits	2,16			; _mms_dict_Index200B[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index200B[4]._size @ 544
	.bits	_ODP_Temperature_ChargeLimits+3,32		; _mms_dict_Index200B[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index200B[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index200B[5]._bAccessType @ 640
	.bits	2,16			; _mms_dict_Index200B[5]._bDataType @ 656
	.bits	1,32			; _mms_dict_Index200B[5]._size @ 672
	.bits	_ODP_Temperature_ChargeLimits+4,32		; _mms_dict_Index200B[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index200B[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index200B[6]._bAccessType @ 768
	.bits	2,16			; _mms_dict_Index200B[6]._bDataType @ 784
	.bits	1,32			; _mms_dict_Index200B[6]._size @ 800
	.bits	_ODP_Temperature_ChargeLimits+5,32		; _mms_dict_Index200B[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index200B[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index200B[7]._bAccessType @ 896
	.bits	2,16			; _mms_dict_Index200B[7]._bDataType @ 912
	.bits	1,32			; _mms_dict_Index200B[7]._size @ 928
	.bits	_ODP_Temperature_ChargeLimits+6,32		; _mms_dict_Index200B[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index200B[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index200B[8]._bAccessType @ 1024
	.bits	2,16			; _mms_dict_Index200B[8]._bDataType @ 1040
	.bits	1,32			; _mms_dict_Index200B[8]._size @ 1056
	.bits	_ODP_Temperature_ChargeLimits+7,32		; _mms_dict_Index200B[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index200B[8]._bProcessor @ 1120
	.space	16

$C$DW$473	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index200B")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_mms_dict_Index200B")
	.dwattr $C$DW$473, DW_AT_location[DW_OP_addr _mms_dict_Index200B]
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$473, DW_AT_external
	.global	_mms_dict_Index1A01
	.sect	".econst:_mms_dict_Index1A01"
	.clink
	.align	2
_mms_dict_Index1A01:
	.bits	0,16			; _mms_dict_Index1A01[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index1A01[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index1A01[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj1A01,32		; _mms_dict_Index1A01[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index1A01[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index1A01[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index1A01[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index1A01[1]._size @ 160
	.bits	_mms_dict_obj1A01,32		; _mms_dict_Index1A01[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index1A01[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index1A01[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index1A01[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index1A01[2]._size @ 288
	.bits	_mms_dict_obj1A01+2,32		; _mms_dict_Index1A01[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index1A01[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index1A01[3]._bAccessType @ 384
	.bits	7,16			; _mms_dict_Index1A01[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index1A01[3]._size @ 416
	.bits	_mms_dict_obj1A01+4,32		; _mms_dict_Index1A01[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index1A01[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index1A01[4]._bAccessType @ 512
	.bits	7,16			; _mms_dict_Index1A01[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index1A01[4]._size @ 544
	.bits	_mms_dict_obj1A01+6,32		; _mms_dict_Index1A01[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index1A01[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index1A01[5]._bAccessType @ 640
	.bits	7,16			; _mms_dict_Index1A01[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index1A01[5]._size @ 672
	.bits	_mms_dict_obj1A01+8,32		; _mms_dict_Index1A01[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index1A01[5]._bProcessor @ 736
	.space	16
	.bits	0,16			; _mms_dict_Index1A01[6]._bAccessType @ 768
	.bits	7,16			; _mms_dict_Index1A01[6]._bDataType @ 784
	.bits	4,32			; _mms_dict_Index1A01[6]._size @ 800
	.bits	_mms_dict_obj1A01+10,32		; _mms_dict_Index1A01[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index1A01[6]._bProcessor @ 864
	.space	16
	.bits	0,16			; _mms_dict_Index1A01[7]._bAccessType @ 896
	.bits	7,16			; _mms_dict_Index1A01[7]._bDataType @ 912
	.bits	4,32			; _mms_dict_Index1A01[7]._size @ 928
	.bits	_mms_dict_obj1A01+12,32		; _mms_dict_Index1A01[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index1A01[7]._bProcessor @ 992
	.space	16
	.bits	0,16			; _mms_dict_Index1A01[8]._bAccessType @ 1024
	.bits	7,16			; _mms_dict_Index1A01[8]._bDataType @ 1040
	.bits	4,32			; _mms_dict_Index1A01[8]._size @ 1056
	.bits	_mms_dict_obj1A01+14,32		; _mms_dict_Index1A01[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index1A01[8]._bProcessor @ 1120
	.space	16

$C$DW$474	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index1A01")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_mms_dict_Index1A01")
	.dwattr $C$DW$474, DW_AT_location[DW_OP_addr _mms_dict_Index1A01]
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$474, DW_AT_external
	.global	_mms_dict_Index2040
	.sect	".econst:_mms_dict_Index2040"
	.clink
	.align	2
_mms_dict_Index2040:
	.bits	2,16			; _mms_dict_Index2040[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2040[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2040[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2040,32		; _mms_dict_Index2040[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2040[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index2040[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index2040[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index2040[1]._size @ 160
	.bits	_ODP_Board_RevisionNumber,32		; _mms_dict_Index2040[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2040[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index2040[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index2040[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index2040[2]._size @ 288
	.bits	_ODP_Board_SerialNumber,32		; _mms_dict_Index2040[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2040[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index2040[3]._bAccessType @ 384
	.bits	6,16			; _mms_dict_Index2040[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index2040[3]._size @ 416
	.bits	_ODP_Board_Config,32		; _mms_dict_Index2040[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2040[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index2040[4]._bAccessType @ 512
	.bits	6,16			; _mms_dict_Index2040[4]._bDataType @ 528
	.bits	2,32			; _mms_dict_Index2040[4]._size @ 544
	.bits	_ODP_Board_BaudRate,32		; _mms_dict_Index2040[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2040[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index2040[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index2040[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index2040[5]._size @ 672
	.bits	_ODP_Board_Bootloader,32		; _mms_dict_Index2040[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2040[5]._bProcessor @ 736
	.space	16
	.bits	0,16			; _mms_dict_Index2040[6]._bAccessType @ 768
	.bits	6,16			; _mms_dict_Index2040[6]._bDataType @ 784
	.bits	2,32			; _mms_dict_Index2040[6]._size @ 800
	.bits	_ODV_Board_HW_Version,32		; _mms_dict_Index2040[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index2040[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index2040[7]._bAccessType @ 896
	.bits	5,16			; _mms_dict_Index2040[7]._bDataType @ 912
	.bits	1,32			; _mms_dict_Index2040[7]._size @ 928
	.bits	_ODP_Board_Deactive_FF,32		; _mms_dict_Index2040[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index2040[7]._bProcessor @ 992
	.space	16
	.bits	0,16			; _mms_dict_Index2040[8]._bAccessType @ 1024
	.bits	5,16			; _mms_dict_Index2040[8]._bDataType @ 1040
	.bits	1,32			; _mms_dict_Index2040[8]._size @ 1056
	.bits	_ODV_Board_Chip,32		; _mms_dict_Index2040[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index2040[8]._bProcessor @ 1120
	.space	16
	.bits	0,16			; _mms_dict_Index2040[9]._bAccessType @ 1152
	.bits	7,16			; _mms_dict_Index2040[9]._bDataType @ 1168
	.bits	4,32			; _mms_dict_Index2040[9]._size @ 1184
	.bits	_ODV_Board_Manufactuer,32		; _mms_dict_Index2040[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index2040[9]._bProcessor @ 1248
	.space	16
	.bits	0,16			; _mms_dict_Index2040[10]._bAccessType @ 1280
	.bits	5,16			; _mms_dict_Index2040[10]._bDataType @ 1296
	.bits	1,32			; _mms_dict_Index2040[10]._size @ 1312
	.bits	_ODV_Board_Type,32		; _mms_dict_Index2040[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index2040[10]._bProcessor @ 1376
	.space	16

$C$DW$475	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2040")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_mms_dict_Index2040")
	.dwattr $C$DW$475, DW_AT_location[DW_OP_addr _mms_dict_Index2040]
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$475, DW_AT_external
	.global	_mms_dict_Index2004
	.sect	".econst:_mms_dict_Index2004"
	.clink
	.align	2
_mms_dict_Index2004:
	.bits	2,16			; _mms_dict_Index2004[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2004[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2004[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2004,32		; _mms_dict_Index2004[0]._pObject @ 64
	.bits	2,16			; _mms_dict_Index2004[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index2004[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index2004[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index2004[1]._size @ 160
	.bits	_ODV_Recorder_Period,32		; _mms_dict_Index2004[1]._pObject @ 192
	.bits	2,16			; _mms_dict_Index2004[1]._bProcessor @ 224
	.space	16
	.bits	2,16			; _mms_dict_Index2004[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index2004[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index2004[2]._size @ 288
	.bits	_ODV_Recorder_NbOfSamples,32		; _mms_dict_Index2004[2]._pObject @ 320
	.bits	2,16			; _mms_dict_Index2004[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index2004[3]._bAccessType @ 384
	.bits	27,16			; _mms_dict_Index2004[3]._bDataType @ 400
	.bits	8,32			; _mms_dict_Index2004[3]._size @ 416
	.bits	_ODV_Recorder_Vectors,32		; _mms_dict_Index2004[3]._pObject @ 448
	.bits	2,16			; _mms_dict_Index2004[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index2004[4]._bAccessType @ 512
	.bits	6,16			; _mms_dict_Index2004[4]._bDataType @ 528
	.bits	2,32			; _mms_dict_Index2004[4]._size @ 544
	.bits	_ODV_Recorder_Multiunits,32		; _mms_dict_Index2004[4]._pObject @ 576
	.bits	2,16			; _mms_dict_Index2004[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index2004[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index2004[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index2004[5]._size @ 672
	.bits	_ODV_Recorder_Variables,32		; _mms_dict_Index2004[5]._pObject @ 704
	.bits	2,16			; _mms_dict_Index2004[5]._bProcessor @ 736
	.space	16
	.bits	0,16			; _mms_dict_Index2004[6]._bAccessType @ 768
	.bits	6,16			; _mms_dict_Index2004[6]._bDataType @ 784
	.bits	2,32			; _mms_dict_Index2004[6]._size @ 800
	.bits	_ODV_Recorder_Start,32		; _mms_dict_Index2004[6]._pObject @ 832
	.bits	2,16			; _mms_dict_Index2004[6]._bProcessor @ 864
	.space	16
	.bits	0,16			; _mms_dict_Index2004[7]._bAccessType @ 896
	.bits	6,16			; _mms_dict_Index2004[7]._bDataType @ 912
	.bits	2,32			; _mms_dict_Index2004[7]._size @ 928
	.bits	_ODV_Recorder_ReadIndex,32		; _mms_dict_Index2004[7]._pObject @ 960
	.bits	2,16			; _mms_dict_Index2004[7]._bProcessor @ 992
	.space	16
	.bits	0,16			; _mms_dict_Index2004[8]._bAccessType @ 1024
	.bits	6,16			; _mms_dict_Index2004[8]._bDataType @ 1040
	.bits	2,32			; _mms_dict_Index2004[8]._size @ 1056
	.bits	_ODV_Recorder_TriggerIndex,32		; _mms_dict_Index2004[8]._pObject @ 1088
	.bits	2,16			; _mms_dict_Index2004[8]._bProcessor @ 1120
	.space	16
	.bits	0,16			; _mms_dict_Index2004[9]._bAccessType @ 1152
	.bits	6,16			; _mms_dict_Index2004[9]._bDataType @ 1168
	.bits	2,32			; _mms_dict_Index2004[9]._size @ 1184
	.bits	_ODV_Recorder_PreTrigger,32		; _mms_dict_Index2004[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index2004[9]._bProcessor @ 1248
	.space	16
	.bits	0,16			; _mms_dict_Index2004[10]._bAccessType @ 1280
	.bits	4,16			; _mms_dict_Index2004[10]._bDataType @ 1296
	.bits	4,32			; _mms_dict_Index2004[10]._size @ 1312
	.bits	_ODV_Recorder_TriggerLevel,32		; _mms_dict_Index2004[10]._pObject @ 1344
	.bits	2,16			; _mms_dict_Index2004[10]._bProcessor @ 1376
	.space	16
	.bits	0,16			; _mms_dict_Index2004[11]._bAccessType @ 1408
	.bits	6,16			; _mms_dict_Index2004[11]._bDataType @ 1424
	.bits	2,32			; _mms_dict_Index2004[11]._size @ 1440
	.bits	_ODV_Recorder_Control,32		; _mms_dict_Index2004[11]._pObject @ 1472
	.bits	2,16			; _mms_dict_Index2004[11]._bProcessor @ 1504
	.space	16
	.bits	0,16			; _mms_dict_Index2004[12]._bAccessType @ 1536
	.bits	6,16			; _mms_dict_Index2004[12]._bDataType @ 1552
	.bits	2,32			; _mms_dict_Index2004[12]._size @ 1568
	.bits	_ODV_Recorder_Size,32		; _mms_dict_Index2004[12]._pObject @ 1600
	.bits	2,16			; _mms_dict_Index2004[12]._bProcessor @ 1632
	.space	16

$C$DW$476	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2004")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_mms_dict_Index2004")
	.dwattr $C$DW$476, DW_AT_location[DW_OP_addr _mms_dict_Index2004]
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$476, DW_AT_external
	.global	_mms_dict_Index2008
	.sect	".econst:_mms_dict_Index2008"
	.clink
	.align	2
_mms_dict_Index2008:
	.bits	2,16			; _mms_dict_Index2008[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2008[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2008[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2008,32		; _mms_dict_Index2008[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2008[0]._bProcessor @ 96
	.space	16
	.bits	2,16			; _mms_dict_Index2008[1]._bAccessType @ 128
	.bits	6,16			; _mms_dict_Index2008[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index2008[1]._size @ 160
	.bits	_ODV_Gateway_Voltage,32		; _mms_dict_Index2008[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2008[1]._bProcessor @ 224
	.space	16
	.bits	2,16			; _mms_dict_Index2008[2]._bAccessType @ 256
	.bits	3,16			; _mms_dict_Index2008[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index2008[2]._size @ 288
	.bits	_ODV_Gateway_Current,32		; _mms_dict_Index2008[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2008[2]._bProcessor @ 352
	.space	16
	.bits	2,16			; _mms_dict_Index2008[3]._bAccessType @ 384
	.bits	2,16			; _mms_dict_Index2008[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index2008[3]._size @ 416
	.bits	_ODV_Gateway_Temperature,32		; _mms_dict_Index2008[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2008[3]._bProcessor @ 480
	.space	16
	.bits	2,16			; _mms_dict_Index2008[4]._bAccessType @ 512
	.bits	5,16			; _mms_dict_Index2008[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index2008[4]._size @ 544
	.bits	_ODV_Gateway_SOC,32		; _mms_dict_Index2008[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2008[4]._bProcessor @ 608
	.space	16
	.bits	2,16			; _mms_dict_Index2008[5]._bAccessType @ 640
	.bits	5,16			; _mms_dict_Index2008[5]._bDataType @ 656
	.bits	1,32			; _mms_dict_Index2008[5]._size @ 672
	.bits	_ODV_Gateway_SOH,32		; _mms_dict_Index2008[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2008[5]._bProcessor @ 736
	.space	16
	.bits	0,16			; _mms_dict_Index2008[6]._bAccessType @ 768
	.bits	5,16			; _mms_dict_Index2008[6]._bDataType @ 784
	.bits	1,32			; _mms_dict_Index2008[6]._size @ 800
	.bits	_ODV_Gateway_State,32		; _mms_dict_Index2008[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index2008[6]._bProcessor @ 864
	.space	16
	.bits	0,16			; _mms_dict_Index2008[7]._bAccessType @ 896
	.bits	7,16			; _mms_dict_Index2008[7]._bDataType @ 912
	.bits	4,32			; _mms_dict_Index2008[7]._size @ 928
	.bits	_ODV_Gateway_Errorcode,32		; _mms_dict_Index2008[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index2008[7]._bProcessor @ 992
	.space	16
	.bits	2,16			; _mms_dict_Index2008[8]._bAccessType @ 1024
	.bits	5,16			; _mms_dict_Index2008[8]._bDataType @ 1040
	.bits	1,32			; _mms_dict_Index2008[8]._size @ 1056
	.bits	_ODV_Gateway_Status,32		; _mms_dict_Index2008[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index2008[8]._bProcessor @ 1120
	.space	16
	.bits	0,16			; _mms_dict_Index2008[9]._bAccessType @ 1152
	.bits	27,16			; _mms_dict_Index2008[9]._bDataType @ 1168
	.bits	8,32			; _mms_dict_Index2008[9]._size @ 1184
	.bits	_ODV_Gateway_Date_Time,32		; _mms_dict_Index2008[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index2008[9]._bProcessor @ 1248
	.space	16
	.bits	0,16			; _mms_dict_Index2008[10]._bAccessType @ 1280
	.bits	5,16			; _mms_dict_Index2008[10]._bDataType @ 1296
	.bits	1,32			; _mms_dict_Index2008[10]._size @ 1312
	.bits	_ODV_Gateway_LogNB,32		; _mms_dict_Index2008[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index2008[10]._bProcessor @ 1376
	.space	16
	.bits	2,16			; _mms_dict_Index2008[11]._bAccessType @ 1408
	.bits	7,16			; _mms_dict_Index2008[11]._bDataType @ 1424
	.bits	4,32			; _mms_dict_Index2008[11]._size @ 1440
	.bits	_ODV_Gateway_WhCounter,32		; _mms_dict_Index2008[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index2008[11]._bProcessor @ 1504
	.space	16
	.bits	2,16			; _mms_dict_Index2008[12]._bAccessType @ 1536
	.bits	4,16			; _mms_dict_Index2008[12]._bDataType @ 1552
	.bits	4,32			; _mms_dict_Index2008[12]._size @ 1568
	.bits	_ODV_Gateway_Power,32		; _mms_dict_Index2008[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index2008[12]._bProcessor @ 1632
	.space	16
	.bits	2,16			; _mms_dict_Index2008[13]._bAccessType @ 1664
	.bits	5,16			; _mms_dict_Index2008[13]._bDataType @ 1680
	.bits	1,32			; _mms_dict_Index2008[13]._size @ 1696
	.bits	_ODV_Gateway_MinCellVoltage,32		; _mms_dict_Index2008[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index2008[13]._bProcessor @ 1760
	.space	16
	.bits	2,16			; _mms_dict_Index2008[14]._bAccessType @ 1792
	.bits	5,16			; _mms_dict_Index2008[14]._bDataType @ 1808
	.bits	1,32			; _mms_dict_Index2008[14]._size @ 1824
	.bits	_ODV_Gateway_MaxCellVoltage,32		; _mms_dict_Index2008[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index2008[14]._bProcessor @ 1888
	.space	16
	.bits	2,16			; _mms_dict_Index2008[15]._bAccessType @ 1920
	.bits	5,16			; _mms_dict_Index2008[15]._bDataType @ 1936
	.bits	1,32			; _mms_dict_Index2008[15]._size @ 1952
	.bits	_ODV_Gateway_MaxDeltaCellVoltage,32		; _mms_dict_Index2008[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index2008[15]._bProcessor @ 2016
	.space	16

$C$DW$477	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2008")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_mms_dict_Index2008")
	.dwattr $C$DW$477, DW_AT_location[DW_OP_addr _mms_dict_Index2008]
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$178)
	.dwattr $C$DW$477, DW_AT_external
	.global	_mms_dict_Index6450
	.sect	".econst:_mms_dict_Index6450"
	.clink
	.align	2
_mms_dict_Index6450:
	.bits	2,16			; _mms_dict_Index6450[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index6450[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index6450[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj6450,32		; _mms_dict_Index6450[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index6450[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index6450[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index6450[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index6450[1]._size @ 160
	.bits	_ODP_Analogue_Output_SI_Unit,32		; _mms_dict_Index6450[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index6450[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index6450[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index6450[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index6450[2]._size @ 288
	.bits	_ODP_Analogue_Output_SI_Unit+2,32		; _mms_dict_Index6450[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index6450[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index6450[3]._bAccessType @ 384
	.bits	7,16			; _mms_dict_Index6450[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index6450[3]._size @ 416
	.bits	_ODP_Analogue_Output_SI_Unit+4,32		; _mms_dict_Index6450[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index6450[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index6450[4]._bAccessType @ 512
	.bits	7,16			; _mms_dict_Index6450[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index6450[4]._size @ 544
	.bits	_ODP_Analogue_Output_SI_Unit+6,32		; _mms_dict_Index6450[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index6450[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index6450[5]._bAccessType @ 640
	.bits	7,16			; _mms_dict_Index6450[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index6450[5]._size @ 672
	.bits	_ODP_Analogue_Output_SI_Unit+8,32		; _mms_dict_Index6450[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index6450[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index6450[6]._bAccessType @ 768
	.bits	7,16			; _mms_dict_Index6450[6]._bDataType @ 784
	.bits	4,32			; _mms_dict_Index6450[6]._size @ 800
	.bits	_ODP_Analogue_Output_SI_Unit+10,32		; _mms_dict_Index6450[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index6450[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index6450[7]._bAccessType @ 896
	.bits	7,16			; _mms_dict_Index6450[7]._bDataType @ 912
	.bits	4,32			; _mms_dict_Index6450[7]._size @ 928
	.bits	_ODP_Analogue_Output_SI_Unit+12,32		; _mms_dict_Index6450[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index6450[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index6450[8]._bAccessType @ 1024
	.bits	7,16			; _mms_dict_Index6450[8]._bDataType @ 1040
	.bits	4,32			; _mms_dict_Index6450[8]._size @ 1056
	.bits	_ODP_Analogue_Output_SI_Unit+14,32		; _mms_dict_Index6450[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index6450[8]._bProcessor @ 1120
	.space	16
	.bits	4,16			; _mms_dict_Index6450[9]._bAccessType @ 1152
	.bits	7,16			; _mms_dict_Index6450[9]._bDataType @ 1168
	.bits	4,32			; _mms_dict_Index6450[9]._size @ 1184
	.bits	_ODP_Analogue_Output_SI_Unit+16,32		; _mms_dict_Index6450[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index6450[9]._bProcessor @ 1248
	.space	16
	.bits	4,16			; _mms_dict_Index6450[10]._bAccessType @ 1280
	.bits	7,16			; _mms_dict_Index6450[10]._bDataType @ 1296
	.bits	4,32			; _mms_dict_Index6450[10]._size @ 1312
	.bits	_ODP_Analogue_Output_SI_Unit+18,32		; _mms_dict_Index6450[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index6450[10]._bProcessor @ 1376
	.space	16
	.bits	4,16			; _mms_dict_Index6450[11]._bAccessType @ 1408
	.bits	7,16			; _mms_dict_Index6450[11]._bDataType @ 1424
	.bits	4,32			; _mms_dict_Index6450[11]._size @ 1440
	.bits	_ODP_Analogue_Output_SI_Unit+20,32		; _mms_dict_Index6450[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index6450[11]._bProcessor @ 1504
	.space	16
	.bits	4,16			; _mms_dict_Index6450[12]._bAccessType @ 1536
	.bits	7,16			; _mms_dict_Index6450[12]._bDataType @ 1552
	.bits	4,32			; _mms_dict_Index6450[12]._size @ 1568
	.bits	_ODP_Analogue_Output_SI_Unit+22,32		; _mms_dict_Index6450[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index6450[12]._bProcessor @ 1632
	.space	16
	.bits	4,16			; _mms_dict_Index6450[13]._bAccessType @ 1664
	.bits	7,16			; _mms_dict_Index6450[13]._bDataType @ 1680
	.bits	4,32			; _mms_dict_Index6450[13]._size @ 1696
	.bits	_ODP_Analogue_Output_SI_Unit+24,32		; _mms_dict_Index6450[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index6450[13]._bProcessor @ 1760
	.space	16
	.bits	4,16			; _mms_dict_Index6450[14]._bAccessType @ 1792
	.bits	7,16			; _mms_dict_Index6450[14]._bDataType @ 1808
	.bits	4,32			; _mms_dict_Index6450[14]._size @ 1824
	.bits	_ODP_Analogue_Output_SI_Unit+26,32		; _mms_dict_Index6450[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index6450[14]._bProcessor @ 1888
	.space	16
	.bits	4,16			; _mms_dict_Index6450[15]._bAccessType @ 1920
	.bits	7,16			; _mms_dict_Index6450[15]._bDataType @ 1936
	.bits	4,32			; _mms_dict_Index6450[15]._size @ 1952
	.bits	_ODP_Analogue_Output_SI_Unit+28,32		; _mms_dict_Index6450[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index6450[15]._bProcessor @ 2016
	.space	16
	.bits	4,16			; _mms_dict_Index6450[16]._bAccessType @ 2048
	.bits	7,16			; _mms_dict_Index6450[16]._bDataType @ 2064
	.bits	4,32			; _mms_dict_Index6450[16]._size @ 2080
	.bits	_ODP_Analogue_Output_SI_Unit+30,32		; _mms_dict_Index6450[16]._pObject @ 2112
	.bits	1,16			; _mms_dict_Index6450[16]._bProcessor @ 2144
	.space	16

$C$DW$478	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index6450")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_mms_dict_Index6450")
	.dwattr $C$DW$478, DW_AT_location[DW_OP_addr _mms_dict_Index6450]
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$478, DW_AT_external
	.global	_mms_dict_Index6442
	.sect	".econst:_mms_dict_Index6442"
	.clink
	.align	2
_mms_dict_Index6442:
	.bits	2,16			; _mms_dict_Index6442[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index6442[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index6442[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj6442,32		; _mms_dict_Index6442[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index6442[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index6442[1]._bAccessType @ 128
	.bits	8,16			; _mms_dict_Index6442[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index6442[1]._size @ 160
	.bits	_ODP_Analogue_Output_Scaling_Float,32		; _mms_dict_Index6442[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index6442[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index6442[2]._bAccessType @ 256
	.bits	8,16			; _mms_dict_Index6442[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index6442[2]._size @ 288
	.bits	_ODP_Analogue_Output_Scaling_Float+2,32		; _mms_dict_Index6442[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index6442[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index6442[3]._bAccessType @ 384
	.bits	8,16			; _mms_dict_Index6442[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index6442[3]._size @ 416
	.bits	_ODP_Analogue_Output_Scaling_Float+4,32		; _mms_dict_Index6442[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index6442[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index6442[4]._bAccessType @ 512
	.bits	8,16			; _mms_dict_Index6442[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index6442[4]._size @ 544
	.bits	_ODP_Analogue_Output_Scaling_Float+6,32		; _mms_dict_Index6442[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index6442[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index6442[5]._bAccessType @ 640
	.bits	8,16			; _mms_dict_Index6442[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index6442[5]._size @ 672
	.bits	_ODP_Analogue_Output_Scaling_Float+8,32		; _mms_dict_Index6442[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index6442[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index6442[6]._bAccessType @ 768
	.bits	8,16			; _mms_dict_Index6442[6]._bDataType @ 784
	.bits	4,32			; _mms_dict_Index6442[6]._size @ 800
	.bits	_ODP_Analogue_Output_Scaling_Float+10,32		; _mms_dict_Index6442[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index6442[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index6442[7]._bAccessType @ 896
	.bits	8,16			; _mms_dict_Index6442[7]._bDataType @ 912
	.bits	4,32			; _mms_dict_Index6442[7]._size @ 928
	.bits	_ODP_Analogue_Output_Scaling_Float+12,32		; _mms_dict_Index6442[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index6442[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index6442[8]._bAccessType @ 1024
	.bits	8,16			; _mms_dict_Index6442[8]._bDataType @ 1040
	.bits	4,32			; _mms_dict_Index6442[8]._size @ 1056
	.bits	_ODP_Analogue_Output_Scaling_Float+14,32		; _mms_dict_Index6442[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index6442[8]._bProcessor @ 1120
	.space	16
	.bits	4,16			; _mms_dict_Index6442[9]._bAccessType @ 1152
	.bits	8,16			; _mms_dict_Index6442[9]._bDataType @ 1168
	.bits	4,32			; _mms_dict_Index6442[9]._size @ 1184
	.bits	_ODP_Analogue_Output_Scaling_Float+16,32		; _mms_dict_Index6442[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index6442[9]._bProcessor @ 1248
	.space	16
	.bits	4,16			; _mms_dict_Index6442[10]._bAccessType @ 1280
	.bits	8,16			; _mms_dict_Index6442[10]._bDataType @ 1296
	.bits	4,32			; _mms_dict_Index6442[10]._size @ 1312
	.bits	_ODP_Analogue_Output_Scaling_Float+18,32		; _mms_dict_Index6442[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index6442[10]._bProcessor @ 1376
	.space	16
	.bits	4,16			; _mms_dict_Index6442[11]._bAccessType @ 1408
	.bits	8,16			; _mms_dict_Index6442[11]._bDataType @ 1424
	.bits	4,32			; _mms_dict_Index6442[11]._size @ 1440
	.bits	_ODP_Analogue_Output_Scaling_Float+20,32		; _mms_dict_Index6442[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index6442[11]._bProcessor @ 1504
	.space	16
	.bits	4,16			; _mms_dict_Index6442[12]._bAccessType @ 1536
	.bits	8,16			; _mms_dict_Index6442[12]._bDataType @ 1552
	.bits	4,32			; _mms_dict_Index6442[12]._size @ 1568
	.bits	_ODP_Analogue_Output_Scaling_Float+22,32		; _mms_dict_Index6442[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index6442[12]._bProcessor @ 1632
	.space	16
	.bits	4,16			; _mms_dict_Index6442[13]._bAccessType @ 1664
	.bits	8,16			; _mms_dict_Index6442[13]._bDataType @ 1680
	.bits	4,32			; _mms_dict_Index6442[13]._size @ 1696
	.bits	_ODP_Analogue_Output_Scaling_Float+24,32		; _mms_dict_Index6442[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index6442[13]._bProcessor @ 1760
	.space	16
	.bits	4,16			; _mms_dict_Index6442[14]._bAccessType @ 1792
	.bits	8,16			; _mms_dict_Index6442[14]._bDataType @ 1808
	.bits	4,32			; _mms_dict_Index6442[14]._size @ 1824
	.bits	_ODP_Analogue_Output_Scaling_Float+26,32		; _mms_dict_Index6442[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index6442[14]._bProcessor @ 1888
	.space	16
	.bits	4,16			; _mms_dict_Index6442[15]._bAccessType @ 1920
	.bits	8,16			; _mms_dict_Index6442[15]._bDataType @ 1936
	.bits	4,32			; _mms_dict_Index6442[15]._size @ 1952
	.bits	_ODP_Analogue_Output_Scaling_Float+28,32		; _mms_dict_Index6442[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index6442[15]._bProcessor @ 2016
	.space	16
	.bits	4,16			; _mms_dict_Index6442[16]._bAccessType @ 2048
	.bits	8,16			; _mms_dict_Index6442[16]._bDataType @ 2064
	.bits	4,32			; _mms_dict_Index6442[16]._size @ 2080
	.bits	_ODP_Analogue_Output_Scaling_Float+30,32		; _mms_dict_Index6442[16]._pObject @ 2112
	.bits	1,16			; _mms_dict_Index6442[16]._bProcessor @ 2144
	.space	16

$C$DW$479	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index6442")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_mms_dict_Index6442")
	.dwattr $C$DW$479, DW_AT_location[DW_OP_addr _mms_dict_Index6442]
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$479, DW_AT_external
	.global	_mms_dict_Index6411
	.sect	".econst:_mms_dict_Index6411"
	.clink
	.align	2
_mms_dict_Index6411:
	.bits	2,16			; _mms_dict_Index6411[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index6411[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index6411[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj6411,32		; _mms_dict_Index6411[0]._pObject @ 64
	.bits	2,16			; _mms_dict_Index6411[0]._bProcessor @ 96
	.space	16
	.bits	0,16			; _mms_dict_Index6411[1]._bAccessType @ 128
	.bits	3,16			; _mms_dict_Index6411[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index6411[1]._size @ 160
	.bits	_ODV_Write_Analogue_Output_16_Bit,32		; _mms_dict_Index6411[1]._pObject @ 192
	.bits	2,16			; _mms_dict_Index6411[1]._bProcessor @ 224
	.space	16
	.bits	0,16			; _mms_dict_Index6411[2]._bAccessType @ 256
	.bits	3,16			; _mms_dict_Index6411[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index6411[2]._size @ 288
	.bits	_ODV_Write_Analogue_Output_16_Bit+1,32		; _mms_dict_Index6411[2]._pObject @ 320
	.bits	2,16			; _mms_dict_Index6411[2]._bProcessor @ 352
	.space	16
	.bits	0,16			; _mms_dict_Index6411[3]._bAccessType @ 384
	.bits	3,16			; _mms_dict_Index6411[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index6411[3]._size @ 416
	.bits	_ODV_Write_Analogue_Output_16_Bit+2,32		; _mms_dict_Index6411[3]._pObject @ 448
	.bits	2,16			; _mms_dict_Index6411[3]._bProcessor @ 480
	.space	16
	.bits	0,16			; _mms_dict_Index6411[4]._bAccessType @ 512
	.bits	3,16			; _mms_dict_Index6411[4]._bDataType @ 528
	.bits	2,32			; _mms_dict_Index6411[4]._size @ 544
	.bits	_ODV_Write_Analogue_Output_16_Bit+3,32		; _mms_dict_Index6411[4]._pObject @ 576
	.bits	2,16			; _mms_dict_Index6411[4]._bProcessor @ 608
	.space	16
	.bits	0,16			; _mms_dict_Index6411[5]._bAccessType @ 640
	.bits	3,16			; _mms_dict_Index6411[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index6411[5]._size @ 672
	.bits	_ODV_Write_Analogue_Output_16_Bit+4,32		; _mms_dict_Index6411[5]._pObject @ 704
	.bits	2,16			; _mms_dict_Index6411[5]._bProcessor @ 736
	.space	16
	.bits	0,16			; _mms_dict_Index6411[6]._bAccessType @ 768
	.bits	3,16			; _mms_dict_Index6411[6]._bDataType @ 784
	.bits	2,32			; _mms_dict_Index6411[6]._size @ 800
	.bits	_ODV_Write_Analogue_Output_16_Bit+5,32		; _mms_dict_Index6411[6]._pObject @ 832
	.bits	2,16			; _mms_dict_Index6411[6]._bProcessor @ 864
	.space	16
	.bits	0,16			; _mms_dict_Index6411[7]._bAccessType @ 896
	.bits	3,16			; _mms_dict_Index6411[7]._bDataType @ 912
	.bits	2,32			; _mms_dict_Index6411[7]._size @ 928
	.bits	_ODV_Write_Analogue_Output_16_Bit+6,32		; _mms_dict_Index6411[7]._pObject @ 960
	.bits	2,16			; _mms_dict_Index6411[7]._bProcessor @ 992
	.space	16
	.bits	0,16			; _mms_dict_Index6411[8]._bAccessType @ 1024
	.bits	3,16			; _mms_dict_Index6411[8]._bDataType @ 1040
	.bits	2,32			; _mms_dict_Index6411[8]._size @ 1056
	.bits	_ODV_Write_Analogue_Output_16_Bit+7,32		; _mms_dict_Index6411[8]._pObject @ 1088
	.bits	2,16			; _mms_dict_Index6411[8]._bProcessor @ 1120
	.space	16
	.bits	0,16			; _mms_dict_Index6411[9]._bAccessType @ 1152
	.bits	3,16			; _mms_dict_Index6411[9]._bDataType @ 1168
	.bits	2,32			; _mms_dict_Index6411[9]._size @ 1184
	.bits	_ODV_Write_Analogue_Output_16_Bit+8,32		; _mms_dict_Index6411[9]._pObject @ 1216
	.bits	2,16			; _mms_dict_Index6411[9]._bProcessor @ 1248
	.space	16
	.bits	0,16			; _mms_dict_Index6411[10]._bAccessType @ 1280
	.bits	3,16			; _mms_dict_Index6411[10]._bDataType @ 1296
	.bits	2,32			; _mms_dict_Index6411[10]._size @ 1312
	.bits	_ODV_Write_Analogue_Output_16_Bit+9,32		; _mms_dict_Index6411[10]._pObject @ 1344
	.bits	2,16			; _mms_dict_Index6411[10]._bProcessor @ 1376
	.space	16
	.bits	0,16			; _mms_dict_Index6411[11]._bAccessType @ 1408
	.bits	3,16			; _mms_dict_Index6411[11]._bDataType @ 1424
	.bits	2,32			; _mms_dict_Index6411[11]._size @ 1440
	.bits	_ODV_Write_Analogue_Output_16_Bit+10,32		; _mms_dict_Index6411[11]._pObject @ 1472
	.bits	2,16			; _mms_dict_Index6411[11]._bProcessor @ 1504
	.space	16
	.bits	0,16			; _mms_dict_Index6411[12]._bAccessType @ 1536
	.bits	3,16			; _mms_dict_Index6411[12]._bDataType @ 1552
	.bits	2,32			; _mms_dict_Index6411[12]._size @ 1568
	.bits	_ODV_Write_Analogue_Output_16_Bit+11,32		; _mms_dict_Index6411[12]._pObject @ 1600
	.bits	2,16			; _mms_dict_Index6411[12]._bProcessor @ 1632
	.space	16
	.bits	0,16			; _mms_dict_Index6411[13]._bAccessType @ 1664
	.bits	3,16			; _mms_dict_Index6411[13]._bDataType @ 1680
	.bits	2,32			; _mms_dict_Index6411[13]._size @ 1696
	.bits	_ODV_Write_Analogue_Output_16_Bit+12,32		; _mms_dict_Index6411[13]._pObject @ 1728
	.bits	2,16			; _mms_dict_Index6411[13]._bProcessor @ 1760
	.space	16
	.bits	0,16			; _mms_dict_Index6411[14]._bAccessType @ 1792
	.bits	3,16			; _mms_dict_Index6411[14]._bDataType @ 1808
	.bits	2,32			; _mms_dict_Index6411[14]._size @ 1824
	.bits	_ODV_Write_Analogue_Output_16_Bit+13,32		; _mms_dict_Index6411[14]._pObject @ 1856
	.bits	2,16			; _mms_dict_Index6411[14]._bProcessor @ 1888
	.space	16
	.bits	0,16			; _mms_dict_Index6411[15]._bAccessType @ 1920
	.bits	3,16			; _mms_dict_Index6411[15]._bDataType @ 1936
	.bits	2,32			; _mms_dict_Index6411[15]._size @ 1952
	.bits	_ODV_Write_Analogue_Output_16_Bit+14,32		; _mms_dict_Index6411[15]._pObject @ 1984
	.bits	2,16			; _mms_dict_Index6411[15]._bProcessor @ 2016
	.space	16
	.bits	0,16			; _mms_dict_Index6411[16]._bAccessType @ 2048
	.bits	3,16			; _mms_dict_Index6411[16]._bDataType @ 2064
	.bits	2,32			; _mms_dict_Index6411[16]._size @ 2080
	.bits	_ODV_Write_Analogue_Output_16_Bit+15,32		; _mms_dict_Index6411[16]._pObject @ 2112
	.bits	2,16			; _mms_dict_Index6411[16]._bProcessor @ 2144
	.space	16

$C$DW$480	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index6411")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_mms_dict_Index6411")
	.dwattr $C$DW$480, DW_AT_location[DW_OP_addr _mms_dict_Index6411]
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$480, DW_AT_external
	.global	_mms_dict_Index6446
	.sect	".econst:_mms_dict_Index6446"
	.clink
	.align	2
_mms_dict_Index6446:
	.bits	2,16			; _mms_dict_Index6446[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index6446[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index6446[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj6446,32		; _mms_dict_Index6446[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index6446[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index6446[1]._bAccessType @ 128
	.bits	4,16			; _mms_dict_Index6446[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index6446[1]._size @ 160
	.bits	_ODP_Analogue_Output_Offset_Integer,32		; _mms_dict_Index6446[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index6446[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index6446[2]._bAccessType @ 256
	.bits	4,16			; _mms_dict_Index6446[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index6446[2]._size @ 288
	.bits	_ODP_Analogue_Output_Offset_Integer+2,32		; _mms_dict_Index6446[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index6446[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index6446[3]._bAccessType @ 384
	.bits	4,16			; _mms_dict_Index6446[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index6446[3]._size @ 416
	.bits	_ODP_Analogue_Output_Offset_Integer+4,32		; _mms_dict_Index6446[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index6446[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index6446[4]._bAccessType @ 512
	.bits	4,16			; _mms_dict_Index6446[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index6446[4]._size @ 544
	.bits	_ODP_Analogue_Output_Offset_Integer+6,32		; _mms_dict_Index6446[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index6446[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index6446[5]._bAccessType @ 640
	.bits	4,16			; _mms_dict_Index6446[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index6446[5]._size @ 672
	.bits	_ODP_Analogue_Output_Offset_Integer+8,32		; _mms_dict_Index6446[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index6446[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index6446[6]._bAccessType @ 768
	.bits	4,16			; _mms_dict_Index6446[6]._bDataType @ 784
	.bits	4,32			; _mms_dict_Index6446[6]._size @ 800
	.bits	_ODP_Analogue_Output_Offset_Integer+10,32		; _mms_dict_Index6446[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index6446[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index6446[7]._bAccessType @ 896
	.bits	4,16			; _mms_dict_Index6446[7]._bDataType @ 912
	.bits	4,32			; _mms_dict_Index6446[7]._size @ 928
	.bits	_ODP_Analogue_Output_Offset_Integer+12,32		; _mms_dict_Index6446[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index6446[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index6446[8]._bAccessType @ 1024
	.bits	4,16			; _mms_dict_Index6446[8]._bDataType @ 1040
	.bits	4,32			; _mms_dict_Index6446[8]._size @ 1056
	.bits	_ODP_Analogue_Output_Offset_Integer+14,32		; _mms_dict_Index6446[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index6446[8]._bProcessor @ 1120
	.space	16
	.bits	4,16			; _mms_dict_Index6446[9]._bAccessType @ 1152
	.bits	4,16			; _mms_dict_Index6446[9]._bDataType @ 1168
	.bits	4,32			; _mms_dict_Index6446[9]._size @ 1184
	.bits	_ODP_Analogue_Output_Offset_Integer+16,32		; _mms_dict_Index6446[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index6446[9]._bProcessor @ 1248
	.space	16
	.bits	4,16			; _mms_dict_Index6446[10]._bAccessType @ 1280
	.bits	4,16			; _mms_dict_Index6446[10]._bDataType @ 1296
	.bits	4,32			; _mms_dict_Index6446[10]._size @ 1312
	.bits	_ODP_Analogue_Output_Offset_Integer+18,32		; _mms_dict_Index6446[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index6446[10]._bProcessor @ 1376
	.space	16
	.bits	4,16			; _mms_dict_Index6446[11]._bAccessType @ 1408
	.bits	4,16			; _mms_dict_Index6446[11]._bDataType @ 1424
	.bits	4,32			; _mms_dict_Index6446[11]._size @ 1440
	.bits	_ODP_Analogue_Output_Offset_Integer+20,32		; _mms_dict_Index6446[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index6446[11]._bProcessor @ 1504
	.space	16
	.bits	4,16			; _mms_dict_Index6446[12]._bAccessType @ 1536
	.bits	4,16			; _mms_dict_Index6446[12]._bDataType @ 1552
	.bits	4,32			; _mms_dict_Index6446[12]._size @ 1568
	.bits	_ODP_Analogue_Output_Offset_Integer+22,32		; _mms_dict_Index6446[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index6446[12]._bProcessor @ 1632
	.space	16
	.bits	4,16			; _mms_dict_Index6446[13]._bAccessType @ 1664
	.bits	4,16			; _mms_dict_Index6446[13]._bDataType @ 1680
	.bits	4,32			; _mms_dict_Index6446[13]._size @ 1696
	.bits	_ODP_Analogue_Output_Offset_Integer+24,32		; _mms_dict_Index6446[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index6446[13]._bProcessor @ 1760
	.space	16
	.bits	4,16			; _mms_dict_Index6446[14]._bAccessType @ 1792
	.bits	4,16			; _mms_dict_Index6446[14]._bDataType @ 1808
	.bits	4,32			; _mms_dict_Index6446[14]._size @ 1824
	.bits	_ODP_Analogue_Output_Offset_Integer+26,32		; _mms_dict_Index6446[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index6446[14]._bProcessor @ 1888
	.space	16
	.bits	4,16			; _mms_dict_Index6446[15]._bAccessType @ 1920
	.bits	4,16			; _mms_dict_Index6446[15]._bDataType @ 1936
	.bits	4,32			; _mms_dict_Index6446[15]._size @ 1952
	.bits	_ODP_Analogue_Output_Offset_Integer+28,32		; _mms_dict_Index6446[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index6446[15]._bProcessor @ 2016
	.space	16
	.bits	4,16			; _mms_dict_Index6446[16]._bAccessType @ 2048
	.bits	4,16			; _mms_dict_Index6446[16]._bDataType @ 2064
	.bits	4,32			; _mms_dict_Index6446[16]._size @ 2080
	.bits	_ODP_Analogue_Output_Offset_Integer+30,32		; _mms_dict_Index6446[16]._pObject @ 2112
	.bits	1,16			; _mms_dict_Index6446[16]._bProcessor @ 2144
	.space	16

$C$DW$481	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index6446")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_mms_dict_Index6446")
	.dwattr $C$DW$481, DW_AT_location[DW_OP_addr _mms_dict_Index6446]
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$481, DW_AT_external
	.global	_mms_dict_Index2200
	.sect	".econst:_mms_dict_Index2200"
	.clink
	.align	2
_mms_dict_Index2200:
	.bits	2,16			; _mms_dict_Index2200[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2200[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2200[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2200,32		; _mms_dict_Index2200[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2200[0]._bProcessor @ 96
	.space	16
	.bits	2,16			; _mms_dict_Index2200[1]._bAccessType @ 128
	.bits	5,16			; _mms_dict_Index2200[1]._bDataType @ 144
	.bits	1,32			; _mms_dict_Index2200[1]._size @ 160
	.bits	_ODV_Module1_Status,32		; _mms_dict_Index2200[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2200[1]._bProcessor @ 224
	.space	16
	.bits	2,16			; _mms_dict_Index2200[2]._bAccessType @ 256
	.bits	5,16			; _mms_dict_Index2200[2]._bDataType @ 272
	.bits	1,32			; _mms_dict_Index2200[2]._size @ 288
	.bits	_ODV_Module1_SOC,32		; _mms_dict_Index2200[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2200[2]._bProcessor @ 352
	.space	16
	.bits	2,16			; _mms_dict_Index2200[3]._bAccessType @ 384
	.bits	5,16			; _mms_dict_Index2200[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index2200[3]._size @ 416
	.bits	_ODV_Module1_Alarms,32		; _mms_dict_Index2200[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2200[3]._bProcessor @ 480
	.space	16
	.bits	2,16			; _mms_dict_Index2200[4]._bAccessType @ 512
	.bits	6,16			; _mms_dict_Index2200[4]._bDataType @ 528
	.bits	2,32			; _mms_dict_Index2200[4]._size @ 544
	.bits	_ODV_Module1_Voltage,32		; _mms_dict_Index2200[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2200[4]._bProcessor @ 608
	.space	16
	.bits	2,16			; _mms_dict_Index2200[5]._bAccessType @ 640
	.bits	3,16			; _mms_dict_Index2200[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index2200[5]._size @ 672
	.bits	_ODV_Module1_Current,32		; _mms_dict_Index2200[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2200[5]._bProcessor @ 736
	.space	16
	.bits	2,16			; _mms_dict_Index2200[6]._bAccessType @ 768
	.bits	2,16			; _mms_dict_Index2200[6]._bDataType @ 784
	.bits	1,32			; _mms_dict_Index2200[6]._size @ 800
	.bits	_ODV_Module1_Temperature,32		; _mms_dict_Index2200[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index2200[6]._bProcessor @ 864
	.space	16
	.bits	2,16			; _mms_dict_Index2200[7]._bAccessType @ 896
	.bits	6,16			; _mms_dict_Index2200[7]._bDataType @ 912
	.bits	2,32			; _mms_dict_Index2200[7]._size @ 928
	.bits	_ODV_Module1_Throughput,32		; _mms_dict_Index2200[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index2200[7]._bProcessor @ 992
	.space	16
	.bits	2,16			; _mms_dict_Index2200[8]._bAccessType @ 1024
	.bits	5,16			; _mms_dict_Index2200[8]._bDataType @ 1040
	.bits	1,32			; _mms_dict_Index2200[8]._size @ 1056
	.bits	_ODV_Module1_MinCellVoltage,32		; _mms_dict_Index2200[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index2200[8]._bProcessor @ 1120
	.space	16
	.bits	2,16			; _mms_dict_Index2200[9]._bAccessType @ 1152
	.bits	5,16			; _mms_dict_Index2200[9]._bDataType @ 1168
	.bits	1,32			; _mms_dict_Index2200[9]._size @ 1184
	.bits	_ODV_Module1_MaxCellVoltage,32		; _mms_dict_Index2200[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index2200[9]._bProcessor @ 1248
	.space	16
	.bits	2,16			; _mms_dict_Index2200[10]._bAccessType @ 1280
	.bits	5,16			; _mms_dict_Index2200[10]._bDataType @ 1296
	.bits	1,32			; _mms_dict_Index2200[10]._size @ 1312
	.bits	_ODV_Module1_Warnings,32		; _mms_dict_Index2200[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index2200[10]._bProcessor @ 1376
	.space	16
	.bits	2,16			; _mms_dict_Index2200[11]._bAccessType @ 1408
	.bits	2,16			; _mms_dict_Index2200[11]._bDataType @ 1424
	.bits	1,32			; _mms_dict_Index2200[11]._size @ 1440
	.bits	_ODV_Module1_Temperature2,32		; _mms_dict_Index2200[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index2200[11]._bProcessor @ 1504
	.space	16
	.bits	2,16			; _mms_dict_Index2200[12]._bAccessType @ 1536
	.bits	6,16			; _mms_dict_Index2200[12]._bDataType @ 1552
	.bits	2,32			; _mms_dict_Index2200[12]._size @ 1568
	.bits	_ODV_Module1_Capacity_set,32		; _mms_dict_Index2200[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index2200[12]._bProcessor @ 1632
	.space	16
	.bits	2,16			; _mms_dict_Index2200[13]._bAccessType @ 1664
	.bits	5,16			; _mms_dict_Index2200[13]._bDataType @ 1680
	.bits	1,32			; _mms_dict_Index2200[13]._size @ 1696
	.bits	_ODV_Module1_MaxDeltaVoltage,32		; _mms_dict_Index2200[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index2200[13]._bProcessor @ 1760
	.space	16
	.bits	2,16			; _mms_dict_Index2200[14]._bAccessType @ 1792
	.bits	5,16			; _mms_dict_Index2200[14]._bDataType @ 1808
	.bits	1,32			; _mms_dict_Index2200[14]._size @ 1824
	.bits	_ODV_Module1_SOH,32		; _mms_dict_Index2200[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index2200[14]._bProcessor @ 1888
	.space	16
	.bits	2,16			; _mms_dict_Index2200[15]._bAccessType @ 1920
	.bits	6,16			; _mms_dict_Index2200[15]._bDataType @ 1936
	.bits	2,32			; _mms_dict_Index2200[15]._size @ 1952
	.bits	_ODV_Module1_Capacity_Used,32		; _mms_dict_Index2200[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index2200[15]._bProcessor @ 2016
	.space	16
	.bits	2,16			; _mms_dict_Index2200[16]._bAccessType @ 2048
	.bits	6,16			; _mms_dict_Index2200[16]._bDataType @ 2064
	.bits	2,32			; _mms_dict_Index2200[16]._size @ 2080
	.bits	_ODV_Module1_Time_Remaining,32		; _mms_dict_Index2200[16]._pObject @ 2112
	.bits	1,16			; _mms_dict_Index2200[16]._bProcessor @ 2144
	.space	16
	.bits	0,16			; _mms_dict_Index2200[17]._bAccessType @ 2176
	.bits	5,16			; _mms_dict_Index2200[17]._bDataType @ 2192
	.bits	1,32			; _mms_dict_Index2200[17]._size @ 2208
	.bits	_ODV_Module1_Alive_Counter,32		; _mms_dict_Index2200[17]._pObject @ 2240
	.bits	1,16			; _mms_dict_Index2200[17]._bProcessor @ 2272
	.space	16
	.bits	0,16			; _mms_dict_Index2200[18]._bAccessType @ 2304
	.bits	5,16			; _mms_dict_Index2200[18]._bDataType @ 2320
	.bits	1,32			; _mms_dict_Index2200[18]._size @ 2336
	.bits	_ODV_Module1_Module_SOC_Current,32		; _mms_dict_Index2200[18]._pObject @ 2368
	.bits	1,16			; _mms_dict_Index2200[18]._bProcessor @ 2400
	.space	16
	.bits	0,16			; _mms_dict_Index2200[19]._bAccessType @ 2432
	.bits	5,16			; _mms_dict_Index2200[19]._bDataType @ 2448
	.bits	1,32			; _mms_dict_Index2200[19]._size @ 2464
	.bits	_ODV_Module1_Module_SOC_Time,32		; _mms_dict_Index2200[19]._pObject @ 2496
	.bits	1,16			; _mms_dict_Index2200[19]._bProcessor @ 2528
	.space	16

$C$DW$482	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2200")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_mms_dict_Index2200")
	.dwattr $C$DW$482, DW_AT_location[DW_OP_addr _mms_dict_Index2200]
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$182)
	.dwattr $C$DW$482, DW_AT_external
	.global	_mms_dict_Index642F
	.sect	".econst:_mms_dict_Index642F"
	.clink
	.align	2
_mms_dict_Index642F:
	.bits	2,16			; _mms_dict_Index642F[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index642F[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index642F[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj642F,32		; _mms_dict_Index642F[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index642F[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index642F[1]._bAccessType @ 128
	.bits	8,16			; _mms_dict_Index642F[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index642F[1]._size @ 160
	.bits	_ODP_Analogue_Input_Scaling_Float,32		; _mms_dict_Index642F[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index642F[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index642F[2]._bAccessType @ 256
	.bits	8,16			; _mms_dict_Index642F[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index642F[2]._size @ 288
	.bits	_ODP_Analogue_Input_Scaling_Float+2,32		; _mms_dict_Index642F[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index642F[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index642F[3]._bAccessType @ 384
	.bits	8,16			; _mms_dict_Index642F[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index642F[3]._size @ 416
	.bits	_ODP_Analogue_Input_Scaling_Float+4,32		; _mms_dict_Index642F[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index642F[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index642F[4]._bAccessType @ 512
	.bits	8,16			; _mms_dict_Index642F[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index642F[4]._size @ 544
	.bits	_ODP_Analogue_Input_Scaling_Float+6,32		; _mms_dict_Index642F[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index642F[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index642F[5]._bAccessType @ 640
	.bits	8,16			; _mms_dict_Index642F[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index642F[5]._size @ 672
	.bits	_ODP_Analogue_Input_Scaling_Float+8,32		; _mms_dict_Index642F[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index642F[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index642F[6]._bAccessType @ 768
	.bits	8,16			; _mms_dict_Index642F[6]._bDataType @ 784
	.bits	4,32			; _mms_dict_Index642F[6]._size @ 800
	.bits	_ODP_Analogue_Input_Scaling_Float+10,32		; _mms_dict_Index642F[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index642F[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index642F[7]._bAccessType @ 896
	.bits	8,16			; _mms_dict_Index642F[7]._bDataType @ 912
	.bits	4,32			; _mms_dict_Index642F[7]._size @ 928
	.bits	_ODP_Analogue_Input_Scaling_Float+12,32		; _mms_dict_Index642F[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index642F[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index642F[8]._bAccessType @ 1024
	.bits	8,16			; _mms_dict_Index642F[8]._bDataType @ 1040
	.bits	4,32			; _mms_dict_Index642F[8]._size @ 1056
	.bits	_ODP_Analogue_Input_Scaling_Float+14,32		; _mms_dict_Index642F[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index642F[8]._bProcessor @ 1120
	.space	16
	.bits	4,16			; _mms_dict_Index642F[9]._bAccessType @ 1152
	.bits	8,16			; _mms_dict_Index642F[9]._bDataType @ 1168
	.bits	4,32			; _mms_dict_Index642F[9]._size @ 1184
	.bits	_ODP_Analogue_Input_Scaling_Float+16,32		; _mms_dict_Index642F[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index642F[9]._bProcessor @ 1248
	.space	16
	.bits	4,16			; _mms_dict_Index642F[10]._bAccessType @ 1280
	.bits	8,16			; _mms_dict_Index642F[10]._bDataType @ 1296
	.bits	4,32			; _mms_dict_Index642F[10]._size @ 1312
	.bits	_ODP_Analogue_Input_Scaling_Float+18,32		; _mms_dict_Index642F[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index642F[10]._bProcessor @ 1376
	.space	16
	.bits	4,16			; _mms_dict_Index642F[11]._bAccessType @ 1408
	.bits	8,16			; _mms_dict_Index642F[11]._bDataType @ 1424
	.bits	4,32			; _mms_dict_Index642F[11]._size @ 1440
	.bits	_ODP_Analogue_Input_Scaling_Float+20,32		; _mms_dict_Index642F[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index642F[11]._bProcessor @ 1504
	.space	16
	.bits	4,16			; _mms_dict_Index642F[12]._bAccessType @ 1536
	.bits	8,16			; _mms_dict_Index642F[12]._bDataType @ 1552
	.bits	4,32			; _mms_dict_Index642F[12]._size @ 1568
	.bits	_ODP_Analogue_Input_Scaling_Float+22,32		; _mms_dict_Index642F[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index642F[12]._bProcessor @ 1632
	.space	16
	.bits	4,16			; _mms_dict_Index642F[13]._bAccessType @ 1664
	.bits	8,16			; _mms_dict_Index642F[13]._bDataType @ 1680
	.bits	4,32			; _mms_dict_Index642F[13]._size @ 1696
	.bits	_ODP_Analogue_Input_Scaling_Float+24,32		; _mms_dict_Index642F[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index642F[13]._bProcessor @ 1760
	.space	16
	.bits	4,16			; _mms_dict_Index642F[14]._bAccessType @ 1792
	.bits	8,16			; _mms_dict_Index642F[14]._bDataType @ 1808
	.bits	4,32			; _mms_dict_Index642F[14]._size @ 1824
	.bits	_ODP_Analogue_Input_Scaling_Float+26,32		; _mms_dict_Index642F[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index642F[14]._bProcessor @ 1888
	.space	16
	.bits	4,16			; _mms_dict_Index642F[15]._bAccessType @ 1920
	.bits	8,16			; _mms_dict_Index642F[15]._bDataType @ 1936
	.bits	4,32			; _mms_dict_Index642F[15]._size @ 1952
	.bits	_ODP_Analogue_Input_Scaling_Float+28,32		; _mms_dict_Index642F[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index642F[15]._bProcessor @ 2016
	.space	16
	.bits	4,16			; _mms_dict_Index642F[16]._bAccessType @ 2048
	.bits	8,16			; _mms_dict_Index642F[16]._bDataType @ 2064
	.bits	4,32			; _mms_dict_Index642F[16]._size @ 2080
	.bits	_ODP_Analogue_Input_Scaling_Float+30,32		; _mms_dict_Index642F[16]._pObject @ 2112
	.bits	1,16			; _mms_dict_Index642F[16]._bProcessor @ 2144
	.space	16
	.bits	4,16			; _mms_dict_Index642F[17]._bAccessType @ 2176
	.bits	8,16			; _mms_dict_Index642F[17]._bDataType @ 2192
	.bits	4,32			; _mms_dict_Index642F[17]._size @ 2208
	.bits	_ODP_Analogue_Input_Scaling_Float+32,32		; _mms_dict_Index642F[17]._pObject @ 2240
	.bits	1,16			; _mms_dict_Index642F[17]._bProcessor @ 2272
	.space	16
	.bits	4,16			; _mms_dict_Index642F[18]._bAccessType @ 2304
	.bits	8,16			; _mms_dict_Index642F[18]._bDataType @ 2320
	.bits	4,32			; _mms_dict_Index642F[18]._size @ 2336
	.bits	_ODP_Analogue_Input_Scaling_Float+34,32		; _mms_dict_Index642F[18]._pObject @ 2368
	.bits	1,16			; _mms_dict_Index642F[18]._bProcessor @ 2400
	.space	16
	.bits	4,16			; _mms_dict_Index642F[19]._bAccessType @ 2432
	.bits	8,16			; _mms_dict_Index642F[19]._bDataType @ 2448
	.bits	4,32			; _mms_dict_Index642F[19]._size @ 2464
	.bits	_ODP_Analogue_Input_Scaling_Float+36,32		; _mms_dict_Index642F[19]._pObject @ 2496
	.bits	1,16			; _mms_dict_Index642F[19]._bProcessor @ 2528
	.space	16
	.bits	4,16			; _mms_dict_Index642F[20]._bAccessType @ 2560
	.bits	8,16			; _mms_dict_Index642F[20]._bDataType @ 2576
	.bits	4,32			; _mms_dict_Index642F[20]._size @ 2592
	.bits	_ODP_Analogue_Input_Scaling_Float+38,32		; _mms_dict_Index642F[20]._pObject @ 2624
	.bits	1,16			; _mms_dict_Index642F[20]._bProcessor @ 2656
	.space	16
	.bits	4,16			; _mms_dict_Index642F[21]._bAccessType @ 2688
	.bits	8,16			; _mms_dict_Index642F[21]._bDataType @ 2704
	.bits	4,32			; _mms_dict_Index642F[21]._size @ 2720
	.bits	_ODP_Analogue_Input_Scaling_Float+40,32		; _mms_dict_Index642F[21]._pObject @ 2752
	.bits	1,16			; _mms_dict_Index642F[21]._bProcessor @ 2784
	.space	16
	.bits	4,16			; _mms_dict_Index642F[22]._bAccessType @ 2816
	.bits	8,16			; _mms_dict_Index642F[22]._bDataType @ 2832
	.bits	4,32			; _mms_dict_Index642F[22]._size @ 2848
	.bits	_ODP_Analogue_Input_Scaling_Float+42,32		; _mms_dict_Index642F[22]._pObject @ 2880
	.bits	1,16			; _mms_dict_Index642F[22]._bProcessor @ 2912
	.space	16
	.bits	4,16			; _mms_dict_Index642F[23]._bAccessType @ 2944
	.bits	8,16			; _mms_dict_Index642F[23]._bDataType @ 2960
	.bits	4,32			; _mms_dict_Index642F[23]._size @ 2976
	.bits	_ODP_Analogue_Input_Scaling_Float+44,32		; _mms_dict_Index642F[23]._pObject @ 3008
	.bits	1,16			; _mms_dict_Index642F[23]._bProcessor @ 3040
	.space	16

$C$DW$483	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index642F")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_mms_dict_Index642F")
	.dwattr $C$DW$483, DW_AT_location[DW_OP_addr _mms_dict_Index642F]
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$483, DW_AT_external
	.global	_mms_dict_Index6430
	.sect	".econst:_mms_dict_Index6430"
	.clink
	.align	2
_mms_dict_Index6430:
	.bits	2,16			; _mms_dict_Index6430[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index6430[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index6430[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj6430,32		; _mms_dict_Index6430[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index6430[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index6430[1]._bAccessType @ 128
	.bits	7,16			; _mms_dict_Index6430[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index6430[1]._size @ 160
	.bits	_ODP_Analogue_Input_SI_unit,32		; _mms_dict_Index6430[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index6430[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index6430[2]._bAccessType @ 256
	.bits	7,16			; _mms_dict_Index6430[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index6430[2]._size @ 288
	.bits	_ODP_Analogue_Input_SI_unit+2,32		; _mms_dict_Index6430[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index6430[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index6430[3]._bAccessType @ 384
	.bits	7,16			; _mms_dict_Index6430[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index6430[3]._size @ 416
	.bits	_ODP_Analogue_Input_SI_unit+4,32		; _mms_dict_Index6430[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index6430[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index6430[4]._bAccessType @ 512
	.bits	7,16			; _mms_dict_Index6430[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index6430[4]._size @ 544
	.bits	_ODP_Analogue_Input_SI_unit+6,32		; _mms_dict_Index6430[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index6430[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index6430[5]._bAccessType @ 640
	.bits	7,16			; _mms_dict_Index6430[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index6430[5]._size @ 672
	.bits	_ODP_Analogue_Input_SI_unit+8,32		; _mms_dict_Index6430[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index6430[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index6430[6]._bAccessType @ 768
	.bits	7,16			; _mms_dict_Index6430[6]._bDataType @ 784
	.bits	4,32			; _mms_dict_Index6430[6]._size @ 800
	.bits	_ODP_Analogue_Input_SI_unit+10,32		; _mms_dict_Index6430[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index6430[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index6430[7]._bAccessType @ 896
	.bits	7,16			; _mms_dict_Index6430[7]._bDataType @ 912
	.bits	4,32			; _mms_dict_Index6430[7]._size @ 928
	.bits	_ODP_Analogue_Input_SI_unit+12,32		; _mms_dict_Index6430[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index6430[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index6430[8]._bAccessType @ 1024
	.bits	7,16			; _mms_dict_Index6430[8]._bDataType @ 1040
	.bits	4,32			; _mms_dict_Index6430[8]._size @ 1056
	.bits	_ODP_Analogue_Input_SI_unit+14,32		; _mms_dict_Index6430[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index6430[8]._bProcessor @ 1120
	.space	16
	.bits	4,16			; _mms_dict_Index6430[9]._bAccessType @ 1152
	.bits	7,16			; _mms_dict_Index6430[9]._bDataType @ 1168
	.bits	4,32			; _mms_dict_Index6430[9]._size @ 1184
	.bits	_ODP_Analogue_Input_SI_unit+16,32		; _mms_dict_Index6430[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index6430[9]._bProcessor @ 1248
	.space	16
	.bits	4,16			; _mms_dict_Index6430[10]._bAccessType @ 1280
	.bits	7,16			; _mms_dict_Index6430[10]._bDataType @ 1296
	.bits	4,32			; _mms_dict_Index6430[10]._size @ 1312
	.bits	_ODP_Analogue_Input_SI_unit+18,32		; _mms_dict_Index6430[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index6430[10]._bProcessor @ 1376
	.space	16
	.bits	4,16			; _mms_dict_Index6430[11]._bAccessType @ 1408
	.bits	7,16			; _mms_dict_Index6430[11]._bDataType @ 1424
	.bits	4,32			; _mms_dict_Index6430[11]._size @ 1440
	.bits	_ODP_Analogue_Input_SI_unit+20,32		; _mms_dict_Index6430[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index6430[11]._bProcessor @ 1504
	.space	16
	.bits	4,16			; _mms_dict_Index6430[12]._bAccessType @ 1536
	.bits	7,16			; _mms_dict_Index6430[12]._bDataType @ 1552
	.bits	4,32			; _mms_dict_Index6430[12]._size @ 1568
	.bits	_ODP_Analogue_Input_SI_unit+22,32		; _mms_dict_Index6430[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index6430[12]._bProcessor @ 1632
	.space	16
	.bits	4,16			; _mms_dict_Index6430[13]._bAccessType @ 1664
	.bits	7,16			; _mms_dict_Index6430[13]._bDataType @ 1680
	.bits	4,32			; _mms_dict_Index6430[13]._size @ 1696
	.bits	_ODP_Analogue_Input_SI_unit+24,32		; _mms_dict_Index6430[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index6430[13]._bProcessor @ 1760
	.space	16
	.bits	4,16			; _mms_dict_Index6430[14]._bAccessType @ 1792
	.bits	7,16			; _mms_dict_Index6430[14]._bDataType @ 1808
	.bits	4,32			; _mms_dict_Index6430[14]._size @ 1824
	.bits	_ODP_Analogue_Input_SI_unit+26,32		; _mms_dict_Index6430[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index6430[14]._bProcessor @ 1888
	.space	16
	.bits	4,16			; _mms_dict_Index6430[15]._bAccessType @ 1920
	.bits	7,16			; _mms_dict_Index6430[15]._bDataType @ 1936
	.bits	4,32			; _mms_dict_Index6430[15]._size @ 1952
	.bits	_ODP_Analogue_Input_SI_unit+28,32		; _mms_dict_Index6430[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index6430[15]._bProcessor @ 2016
	.space	16
	.bits	4,16			; _mms_dict_Index6430[16]._bAccessType @ 2048
	.bits	7,16			; _mms_dict_Index6430[16]._bDataType @ 2064
	.bits	4,32			; _mms_dict_Index6430[16]._size @ 2080
	.bits	_ODP_Analogue_Input_SI_unit+30,32		; _mms_dict_Index6430[16]._pObject @ 2112
	.bits	1,16			; _mms_dict_Index6430[16]._bProcessor @ 2144
	.space	16
	.bits	4,16			; _mms_dict_Index6430[17]._bAccessType @ 2176
	.bits	7,16			; _mms_dict_Index6430[17]._bDataType @ 2192
	.bits	4,32			; _mms_dict_Index6430[17]._size @ 2208
	.bits	_ODP_Analogue_Input_SI_unit+32,32		; _mms_dict_Index6430[17]._pObject @ 2240
	.bits	1,16			; _mms_dict_Index6430[17]._bProcessor @ 2272
	.space	16
	.bits	4,16			; _mms_dict_Index6430[18]._bAccessType @ 2304
	.bits	7,16			; _mms_dict_Index6430[18]._bDataType @ 2320
	.bits	4,32			; _mms_dict_Index6430[18]._size @ 2336
	.bits	_ODP_Analogue_Input_SI_unit+34,32		; _mms_dict_Index6430[18]._pObject @ 2368
	.bits	1,16			; _mms_dict_Index6430[18]._bProcessor @ 2400
	.space	16
	.bits	4,16			; _mms_dict_Index6430[19]._bAccessType @ 2432
	.bits	7,16			; _mms_dict_Index6430[19]._bDataType @ 2448
	.bits	4,32			; _mms_dict_Index6430[19]._size @ 2464
	.bits	_ODP_Analogue_Input_SI_unit+36,32		; _mms_dict_Index6430[19]._pObject @ 2496
	.bits	1,16			; _mms_dict_Index6430[19]._bProcessor @ 2528
	.space	16
	.bits	4,16			; _mms_dict_Index6430[20]._bAccessType @ 2560
	.bits	7,16			; _mms_dict_Index6430[20]._bDataType @ 2576
	.bits	4,32			; _mms_dict_Index6430[20]._size @ 2592
	.bits	_ODP_Analogue_Input_SI_unit+38,32		; _mms_dict_Index6430[20]._pObject @ 2624
	.bits	1,16			; _mms_dict_Index6430[20]._bProcessor @ 2656
	.space	16
	.bits	4,16			; _mms_dict_Index6430[21]._bAccessType @ 2688
	.bits	7,16			; _mms_dict_Index6430[21]._bDataType @ 2704
	.bits	4,32			; _mms_dict_Index6430[21]._size @ 2720
	.bits	_ODP_Analogue_Input_SI_unit+40,32		; _mms_dict_Index6430[21]._pObject @ 2752
	.bits	1,16			; _mms_dict_Index6430[21]._bProcessor @ 2784
	.space	16
	.bits	4,16			; _mms_dict_Index6430[22]._bAccessType @ 2816
	.bits	7,16			; _mms_dict_Index6430[22]._bDataType @ 2832
	.bits	4,32			; _mms_dict_Index6430[22]._size @ 2848
	.bits	_ODP_Analogue_Input_SI_unit+42,32		; _mms_dict_Index6430[22]._pObject @ 2880
	.bits	1,16			; _mms_dict_Index6430[22]._bProcessor @ 2912
	.space	16
	.bits	4,16			; _mms_dict_Index6430[23]._bAccessType @ 2944
	.bits	7,16			; _mms_dict_Index6430[23]._bDataType @ 2960
	.bits	4,32			; _mms_dict_Index6430[23]._size @ 2976
	.bits	_ODP_Analogue_Input_SI_unit+44,32		; _mms_dict_Index6430[23]._pObject @ 3008
	.bits	1,16			; _mms_dict_Index6430[23]._bProcessor @ 3040
	.space	16

$C$DW$484	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index6430")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_mms_dict_Index6430")
	.dwattr $C$DW$484, DW_AT_location[DW_OP_addr _mms_dict_Index6430]
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$484, DW_AT_external
	.global	_mms_dict_Index6401
	.sect	".econst:_mms_dict_Index6401"
	.clink
	.align	2
_mms_dict_Index6401:
	.bits	2,16			; _mms_dict_Index6401[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index6401[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index6401[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj6401,32		; _mms_dict_Index6401[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index6401[0]._bProcessor @ 96
	.space	16
	.bits	2,16			; _mms_dict_Index6401[1]._bAccessType @ 128
	.bits	3,16			; _mms_dict_Index6401[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index6401[1]._size @ 160
	.bits	_ODV_Read_Analogue_Input_16_Bit,32		; _mms_dict_Index6401[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index6401[1]._bProcessor @ 224
	.space	16
	.bits	2,16			; _mms_dict_Index6401[2]._bAccessType @ 256
	.bits	3,16			; _mms_dict_Index6401[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index6401[2]._size @ 288
	.bits	_ODV_Read_Analogue_Input_16_Bit+1,32		; _mms_dict_Index6401[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index6401[2]._bProcessor @ 352
	.space	16
	.bits	2,16			; _mms_dict_Index6401[3]._bAccessType @ 384
	.bits	3,16			; _mms_dict_Index6401[3]._bDataType @ 400
	.bits	2,32			; _mms_dict_Index6401[3]._size @ 416
	.bits	_ODV_Read_Analogue_Input_16_Bit+2,32		; _mms_dict_Index6401[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index6401[3]._bProcessor @ 480
	.space	16
	.bits	2,16			; _mms_dict_Index6401[4]._bAccessType @ 512
	.bits	3,16			; _mms_dict_Index6401[4]._bDataType @ 528
	.bits	2,32			; _mms_dict_Index6401[4]._size @ 544
	.bits	_ODV_Read_Analogue_Input_16_Bit+3,32		; _mms_dict_Index6401[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index6401[4]._bProcessor @ 608
	.space	16
	.bits	2,16			; _mms_dict_Index6401[5]._bAccessType @ 640
	.bits	3,16			; _mms_dict_Index6401[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index6401[5]._size @ 672
	.bits	_ODV_Read_Analogue_Input_16_Bit+4,32		; _mms_dict_Index6401[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index6401[5]._bProcessor @ 736
	.space	16
	.bits	2,16			; _mms_dict_Index6401[6]._bAccessType @ 768
	.bits	3,16			; _mms_dict_Index6401[6]._bDataType @ 784
	.bits	2,32			; _mms_dict_Index6401[6]._size @ 800
	.bits	_ODV_Read_Analogue_Input_16_Bit+5,32		; _mms_dict_Index6401[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index6401[6]._bProcessor @ 864
	.space	16
	.bits	2,16			; _mms_dict_Index6401[7]._bAccessType @ 896
	.bits	3,16			; _mms_dict_Index6401[7]._bDataType @ 912
	.bits	2,32			; _mms_dict_Index6401[7]._size @ 928
	.bits	_ODV_Read_Analogue_Input_16_Bit+6,32		; _mms_dict_Index6401[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index6401[7]._bProcessor @ 992
	.space	16
	.bits	2,16			; _mms_dict_Index6401[8]._bAccessType @ 1024
	.bits	3,16			; _mms_dict_Index6401[8]._bDataType @ 1040
	.bits	2,32			; _mms_dict_Index6401[8]._size @ 1056
	.bits	_ODV_Read_Analogue_Input_16_Bit+7,32		; _mms_dict_Index6401[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index6401[8]._bProcessor @ 1120
	.space	16
	.bits	2,16			; _mms_dict_Index6401[9]._bAccessType @ 1152
	.bits	3,16			; _mms_dict_Index6401[9]._bDataType @ 1168
	.bits	2,32			; _mms_dict_Index6401[9]._size @ 1184
	.bits	_ODV_Read_Analogue_Input_16_Bit+8,32		; _mms_dict_Index6401[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index6401[9]._bProcessor @ 1248
	.space	16
	.bits	2,16			; _mms_dict_Index6401[10]._bAccessType @ 1280
	.bits	3,16			; _mms_dict_Index6401[10]._bDataType @ 1296
	.bits	2,32			; _mms_dict_Index6401[10]._size @ 1312
	.bits	_ODV_Read_Analogue_Input_16_Bit+9,32		; _mms_dict_Index6401[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index6401[10]._bProcessor @ 1376
	.space	16
	.bits	2,16			; _mms_dict_Index6401[11]._bAccessType @ 1408
	.bits	3,16			; _mms_dict_Index6401[11]._bDataType @ 1424
	.bits	2,32			; _mms_dict_Index6401[11]._size @ 1440
	.bits	_ODV_Read_Analogue_Input_16_Bit+10,32		; _mms_dict_Index6401[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index6401[11]._bProcessor @ 1504
	.space	16
	.bits	2,16			; _mms_dict_Index6401[12]._bAccessType @ 1536
	.bits	3,16			; _mms_dict_Index6401[12]._bDataType @ 1552
	.bits	2,32			; _mms_dict_Index6401[12]._size @ 1568
	.bits	_ODV_Read_Analogue_Input_16_Bit+11,32		; _mms_dict_Index6401[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index6401[12]._bProcessor @ 1632
	.space	16
	.bits	2,16			; _mms_dict_Index6401[13]._bAccessType @ 1664
	.bits	3,16			; _mms_dict_Index6401[13]._bDataType @ 1680
	.bits	2,32			; _mms_dict_Index6401[13]._size @ 1696
	.bits	_ODV_Read_Analogue_Input_16_Bit+12,32		; _mms_dict_Index6401[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index6401[13]._bProcessor @ 1760
	.space	16
	.bits	2,16			; _mms_dict_Index6401[14]._bAccessType @ 1792
	.bits	3,16			; _mms_dict_Index6401[14]._bDataType @ 1808
	.bits	2,32			; _mms_dict_Index6401[14]._size @ 1824
	.bits	_ODV_Read_Analogue_Input_16_Bit+13,32		; _mms_dict_Index6401[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index6401[14]._bProcessor @ 1888
	.space	16
	.bits	2,16			; _mms_dict_Index6401[15]._bAccessType @ 1920
	.bits	3,16			; _mms_dict_Index6401[15]._bDataType @ 1936
	.bits	2,32			; _mms_dict_Index6401[15]._size @ 1952
	.bits	_ODV_Read_Analogue_Input_16_Bit+14,32		; _mms_dict_Index6401[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index6401[15]._bProcessor @ 2016
	.space	16
	.bits	2,16			; _mms_dict_Index6401[16]._bAccessType @ 2048
	.bits	3,16			; _mms_dict_Index6401[16]._bDataType @ 2064
	.bits	2,32			; _mms_dict_Index6401[16]._size @ 2080
	.bits	_ODV_Read_Analogue_Input_16_Bit+15,32		; _mms_dict_Index6401[16]._pObject @ 2112
	.bits	1,16			; _mms_dict_Index6401[16]._bProcessor @ 2144
	.space	16
	.bits	2,16			; _mms_dict_Index6401[17]._bAccessType @ 2176
	.bits	3,16			; _mms_dict_Index6401[17]._bDataType @ 2192
	.bits	2,32			; _mms_dict_Index6401[17]._size @ 2208
	.bits	_ODV_Read_Analogue_Input_16_Bit+16,32		; _mms_dict_Index6401[17]._pObject @ 2240
	.bits	1,16			; _mms_dict_Index6401[17]._bProcessor @ 2272
	.space	16
	.bits	2,16			; _mms_dict_Index6401[18]._bAccessType @ 2304
	.bits	3,16			; _mms_dict_Index6401[18]._bDataType @ 2320
	.bits	2,32			; _mms_dict_Index6401[18]._size @ 2336
	.bits	_ODV_Read_Analogue_Input_16_Bit+17,32		; _mms_dict_Index6401[18]._pObject @ 2368
	.bits	1,16			; _mms_dict_Index6401[18]._bProcessor @ 2400
	.space	16
	.bits	2,16			; _mms_dict_Index6401[19]._bAccessType @ 2432
	.bits	3,16			; _mms_dict_Index6401[19]._bDataType @ 2448
	.bits	2,32			; _mms_dict_Index6401[19]._size @ 2464
	.bits	_ODV_Read_Analogue_Input_16_Bit+18,32		; _mms_dict_Index6401[19]._pObject @ 2496
	.bits	1,16			; _mms_dict_Index6401[19]._bProcessor @ 2528
	.space	16
	.bits	2,16			; _mms_dict_Index6401[20]._bAccessType @ 2560
	.bits	3,16			; _mms_dict_Index6401[20]._bDataType @ 2576
	.bits	2,32			; _mms_dict_Index6401[20]._size @ 2592
	.bits	_ODV_Read_Analogue_Input_16_Bit+19,32		; _mms_dict_Index6401[20]._pObject @ 2624
	.bits	1,16			; _mms_dict_Index6401[20]._bProcessor @ 2656
	.space	16
	.bits	2,16			; _mms_dict_Index6401[21]._bAccessType @ 2688
	.bits	3,16			; _mms_dict_Index6401[21]._bDataType @ 2704
	.bits	2,32			; _mms_dict_Index6401[21]._size @ 2720
	.bits	_ODV_Read_Analogue_Input_16_Bit+20,32		; _mms_dict_Index6401[21]._pObject @ 2752
	.bits	1,16			; _mms_dict_Index6401[21]._bProcessor @ 2784
	.space	16
	.bits	2,16			; _mms_dict_Index6401[22]._bAccessType @ 2816
	.bits	3,16			; _mms_dict_Index6401[22]._bDataType @ 2832
	.bits	2,32			; _mms_dict_Index6401[22]._size @ 2848
	.bits	_ODV_Read_Analogue_Input_16_Bit+21,32		; _mms_dict_Index6401[22]._pObject @ 2880
	.bits	1,16			; _mms_dict_Index6401[22]._bProcessor @ 2912
	.space	16
	.bits	2,16			; _mms_dict_Index6401[23]._bAccessType @ 2944
	.bits	3,16			; _mms_dict_Index6401[23]._bDataType @ 2960
	.bits	2,32			; _mms_dict_Index6401[23]._size @ 2976
	.bits	_ODV_Read_Analogue_Input_16_Bit+22,32		; _mms_dict_Index6401[23]._pObject @ 3008
	.bits	1,16			; _mms_dict_Index6401[23]._bProcessor @ 3040
	.space	16

$C$DW$485	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index6401")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_mms_dict_Index6401")
	.dwattr $C$DW$485, DW_AT_location[DW_OP_addr _mms_dict_Index6401]
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$485, DW_AT_external
	.global	_mms_dict_Index6431
	.sect	".econst:_mms_dict_Index6431"
	.clink
	.align	2
_mms_dict_Index6431:
	.bits	2,16			; _mms_dict_Index6431[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index6431[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index6431[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj6431,32		; _mms_dict_Index6431[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index6431[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index6431[1]._bAccessType @ 128
	.bits	4,16			; _mms_dict_Index6431[1]._bDataType @ 144
	.bits	4,32			; _mms_dict_Index6431[1]._size @ 160
	.bits	_ODP_Analogue_Input_Offset_Integer,32		; _mms_dict_Index6431[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index6431[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index6431[2]._bAccessType @ 256
	.bits	4,16			; _mms_dict_Index6431[2]._bDataType @ 272
	.bits	4,32			; _mms_dict_Index6431[2]._size @ 288
	.bits	_ODP_Analogue_Input_Offset_Integer+2,32		; _mms_dict_Index6431[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index6431[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index6431[3]._bAccessType @ 384
	.bits	4,16			; _mms_dict_Index6431[3]._bDataType @ 400
	.bits	4,32			; _mms_dict_Index6431[3]._size @ 416
	.bits	_ODP_Analogue_Input_Offset_Integer+4,32		; _mms_dict_Index6431[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index6431[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index6431[4]._bAccessType @ 512
	.bits	4,16			; _mms_dict_Index6431[4]._bDataType @ 528
	.bits	4,32			; _mms_dict_Index6431[4]._size @ 544
	.bits	_ODP_Analogue_Input_Offset_Integer+6,32		; _mms_dict_Index6431[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index6431[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index6431[5]._bAccessType @ 640
	.bits	4,16			; _mms_dict_Index6431[5]._bDataType @ 656
	.bits	4,32			; _mms_dict_Index6431[5]._size @ 672
	.bits	_ODP_Analogue_Input_Offset_Integer+8,32		; _mms_dict_Index6431[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index6431[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index6431[6]._bAccessType @ 768
	.bits	4,16			; _mms_dict_Index6431[6]._bDataType @ 784
	.bits	4,32			; _mms_dict_Index6431[6]._size @ 800
	.bits	_ODP_Analogue_Input_Offset_Integer+10,32		; _mms_dict_Index6431[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index6431[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index6431[7]._bAccessType @ 896
	.bits	4,16			; _mms_dict_Index6431[7]._bDataType @ 912
	.bits	4,32			; _mms_dict_Index6431[7]._size @ 928
	.bits	_ODP_Analogue_Input_Offset_Integer+12,32		; _mms_dict_Index6431[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index6431[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index6431[8]._bAccessType @ 1024
	.bits	4,16			; _mms_dict_Index6431[8]._bDataType @ 1040
	.bits	4,32			; _mms_dict_Index6431[8]._size @ 1056
	.bits	_ODP_Analogue_Input_Offset_Integer+14,32		; _mms_dict_Index6431[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index6431[8]._bProcessor @ 1120
	.space	16
	.bits	4,16			; _mms_dict_Index6431[9]._bAccessType @ 1152
	.bits	4,16			; _mms_dict_Index6431[9]._bDataType @ 1168
	.bits	4,32			; _mms_dict_Index6431[9]._size @ 1184
	.bits	_ODP_Analogue_Input_Offset_Integer+16,32		; _mms_dict_Index6431[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index6431[9]._bProcessor @ 1248
	.space	16
	.bits	4,16			; _mms_dict_Index6431[10]._bAccessType @ 1280
	.bits	4,16			; _mms_dict_Index6431[10]._bDataType @ 1296
	.bits	4,32			; _mms_dict_Index6431[10]._size @ 1312
	.bits	_ODP_Analogue_Input_Offset_Integer+18,32		; _mms_dict_Index6431[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index6431[10]._bProcessor @ 1376
	.space	16
	.bits	4,16			; _mms_dict_Index6431[11]._bAccessType @ 1408
	.bits	4,16			; _mms_dict_Index6431[11]._bDataType @ 1424
	.bits	4,32			; _mms_dict_Index6431[11]._size @ 1440
	.bits	_ODP_Analogue_Input_Offset_Integer+20,32		; _mms_dict_Index6431[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index6431[11]._bProcessor @ 1504
	.space	16
	.bits	4,16			; _mms_dict_Index6431[12]._bAccessType @ 1536
	.bits	4,16			; _mms_dict_Index6431[12]._bDataType @ 1552
	.bits	4,32			; _mms_dict_Index6431[12]._size @ 1568
	.bits	_ODP_Analogue_Input_Offset_Integer+22,32		; _mms_dict_Index6431[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index6431[12]._bProcessor @ 1632
	.space	16
	.bits	4,16			; _mms_dict_Index6431[13]._bAccessType @ 1664
	.bits	4,16			; _mms_dict_Index6431[13]._bDataType @ 1680
	.bits	4,32			; _mms_dict_Index6431[13]._size @ 1696
	.bits	_ODP_Analogue_Input_Offset_Integer+24,32		; _mms_dict_Index6431[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index6431[13]._bProcessor @ 1760
	.space	16
	.bits	4,16			; _mms_dict_Index6431[14]._bAccessType @ 1792
	.bits	4,16			; _mms_dict_Index6431[14]._bDataType @ 1808
	.bits	4,32			; _mms_dict_Index6431[14]._size @ 1824
	.bits	_ODP_Analogue_Input_Offset_Integer+26,32		; _mms_dict_Index6431[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index6431[14]._bProcessor @ 1888
	.space	16
	.bits	4,16			; _mms_dict_Index6431[15]._bAccessType @ 1920
	.bits	4,16			; _mms_dict_Index6431[15]._bDataType @ 1936
	.bits	4,32			; _mms_dict_Index6431[15]._size @ 1952
	.bits	_ODP_Analogue_Input_Offset_Integer+28,32		; _mms_dict_Index6431[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index6431[15]._bProcessor @ 2016
	.space	16
	.bits	4,16			; _mms_dict_Index6431[16]._bAccessType @ 2048
	.bits	4,16			; _mms_dict_Index6431[16]._bDataType @ 2064
	.bits	4,32			; _mms_dict_Index6431[16]._size @ 2080
	.bits	_ODP_Analogue_Input_Offset_Integer+30,32		; _mms_dict_Index6431[16]._pObject @ 2112
	.bits	1,16			; _mms_dict_Index6431[16]._bProcessor @ 2144
	.space	16
	.bits	4,16			; _mms_dict_Index6431[17]._bAccessType @ 2176
	.bits	4,16			; _mms_dict_Index6431[17]._bDataType @ 2192
	.bits	4,32			; _mms_dict_Index6431[17]._size @ 2208
	.bits	_ODP_Analogue_Input_Offset_Integer+32,32		; _mms_dict_Index6431[17]._pObject @ 2240
	.bits	1,16			; _mms_dict_Index6431[17]._bProcessor @ 2272
	.space	16
	.bits	4,16			; _mms_dict_Index6431[18]._bAccessType @ 2304
	.bits	4,16			; _mms_dict_Index6431[18]._bDataType @ 2320
	.bits	4,32			; _mms_dict_Index6431[18]._size @ 2336
	.bits	_ODP_Analogue_Input_Offset_Integer+34,32		; _mms_dict_Index6431[18]._pObject @ 2368
	.bits	1,16			; _mms_dict_Index6431[18]._bProcessor @ 2400
	.space	16
	.bits	4,16			; _mms_dict_Index6431[19]._bAccessType @ 2432
	.bits	4,16			; _mms_dict_Index6431[19]._bDataType @ 2448
	.bits	4,32			; _mms_dict_Index6431[19]._size @ 2464
	.bits	_ODP_Analogue_Input_Offset_Integer+36,32		; _mms_dict_Index6431[19]._pObject @ 2496
	.bits	1,16			; _mms_dict_Index6431[19]._bProcessor @ 2528
	.space	16
	.bits	4,16			; _mms_dict_Index6431[20]._bAccessType @ 2560
	.bits	4,16			; _mms_dict_Index6431[20]._bDataType @ 2576
	.bits	4,32			; _mms_dict_Index6431[20]._size @ 2592
	.bits	_ODP_Analogue_Input_Offset_Integer+38,32		; _mms_dict_Index6431[20]._pObject @ 2624
	.bits	1,16			; _mms_dict_Index6431[20]._bProcessor @ 2656
	.space	16
	.bits	4,16			; _mms_dict_Index6431[21]._bAccessType @ 2688
	.bits	4,16			; _mms_dict_Index6431[21]._bDataType @ 2704
	.bits	4,32			; _mms_dict_Index6431[21]._size @ 2720
	.bits	_ODP_Analogue_Input_Offset_Integer+40,32		; _mms_dict_Index6431[21]._pObject @ 2752
	.bits	1,16			; _mms_dict_Index6431[21]._bProcessor @ 2784
	.space	16
	.bits	4,16			; _mms_dict_Index6431[22]._bAccessType @ 2816
	.bits	4,16			; _mms_dict_Index6431[22]._bDataType @ 2832
	.bits	4,32			; _mms_dict_Index6431[22]._size @ 2848
	.bits	_ODP_Analogue_Input_Offset_Integer+42,32		; _mms_dict_Index6431[22]._pObject @ 2880
	.bits	1,16			; _mms_dict_Index6431[22]._bProcessor @ 2912
	.space	16
	.bits	4,16			; _mms_dict_Index6431[23]._bAccessType @ 2944
	.bits	4,16			; _mms_dict_Index6431[23]._bDataType @ 2960
	.bits	4,32			; _mms_dict_Index6431[23]._size @ 2976
	.bits	_ODP_Analogue_Input_Offset_Integer+44,32		; _mms_dict_Index6431[23]._pObject @ 3008
	.bits	1,16			; _mms_dict_Index6431[23]._bProcessor @ 3040
	.space	16

$C$DW$486	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index6431")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_mms_dict_Index6431")
	.dwattr $C$DW$486, DW_AT_location[DW_OP_addr _mms_dict_Index6431]
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$486, DW_AT_external
	.global	_mms_dict_Index2009
	.sect	".econst:_mms_dict_Index2009"
	.clink
	.align	2
_mms_dict_Index2009:
	.bits	2,16			; _mms_dict_Index2009[0]._bAccessType @ 0
	.bits	5,16			; _mms_dict_Index2009[0]._bDataType @ 16
	.bits	1,32			; _mms_dict_Index2009[0]._size @ 32
	.bits	_mms_dict_highestSubIndex_obj2009,32		; _mms_dict_Index2009[0]._pObject @ 64
	.bits	1,16			; _mms_dict_Index2009[0]._bProcessor @ 96
	.space	16
	.bits	4,16			; _mms_dict_Index2009[1]._bAccessType @ 128
	.bits	3,16			; _mms_dict_Index2009[1]._bDataType @ 144
	.bits	2,32			; _mms_dict_Index2009[1]._size @ 160
	.bits	_ODP_SafetyLimits_Umax,32		; _mms_dict_Index2009[1]._pObject @ 192
	.bits	1,16			; _mms_dict_Index2009[1]._bProcessor @ 224
	.space	16
	.bits	4,16			; _mms_dict_Index2009[2]._bAccessType @ 256
	.bits	3,16			; _mms_dict_Index2009[2]._bDataType @ 272
	.bits	2,32			; _mms_dict_Index2009[2]._size @ 288
	.bits	_ODP_SafetyLimits_Umin,32		; _mms_dict_Index2009[2]._pObject @ 320
	.bits	1,16			; _mms_dict_Index2009[2]._bProcessor @ 352
	.space	16
	.bits	4,16			; _mms_dict_Index2009[3]._bAccessType @ 384
	.bits	2,16			; _mms_dict_Index2009[3]._bDataType @ 400
	.bits	1,32			; _mms_dict_Index2009[3]._size @ 416
	.bits	_ODP_SafetyLimits_Tmax,32		; _mms_dict_Index2009[3]._pObject @ 448
	.bits	1,16			; _mms_dict_Index2009[3]._bProcessor @ 480
	.space	16
	.bits	4,16			; _mms_dict_Index2009[4]._bAccessType @ 512
	.bits	2,16			; _mms_dict_Index2009[4]._bDataType @ 528
	.bits	1,32			; _mms_dict_Index2009[4]._size @ 544
	.bits	_ODP_SafetyLimits_Tmin,32		; _mms_dict_Index2009[4]._pObject @ 576
	.bits	1,16			; _mms_dict_Index2009[4]._bProcessor @ 608
	.space	16
	.bits	4,16			; _mms_dict_Index2009[5]._bAccessType @ 640
	.bits	6,16			; _mms_dict_Index2009[5]._bDataType @ 656
	.bits	2,32			; _mms_dict_Index2009[5]._size @ 672
	.bits	_ODP_SafetyLimits_Imax_charge,32		; _mms_dict_Index2009[5]._pObject @ 704
	.bits	1,16			; _mms_dict_Index2009[5]._bProcessor @ 736
	.space	16
	.bits	4,16			; _mms_dict_Index2009[6]._bAccessType @ 768
	.bits	6,16			; _mms_dict_Index2009[6]._bDataType @ 784
	.bits	2,32			; _mms_dict_Index2009[6]._size @ 800
	.bits	_ODP_SafetyLimits_Umax_bal_delta,32		; _mms_dict_Index2009[6]._pObject @ 832
	.bits	1,16			; _mms_dict_Index2009[6]._bProcessor @ 864
	.space	16
	.bits	4,16			; _mms_dict_Index2009[7]._bAccessType @ 896
	.bits	6,16			; _mms_dict_Index2009[7]._bDataType @ 912
	.bits	2,32			; _mms_dict_Index2009[7]._size @ 928
	.bits	_ODP_SafetyLimits_Imax_dis,32		; _mms_dict_Index2009[7]._pObject @ 960
	.bits	1,16			; _mms_dict_Index2009[7]._bProcessor @ 992
	.space	16
	.bits	4,16			; _mms_dict_Index2009[8]._bAccessType @ 1024
	.bits	6,16			; _mms_dict_Index2009[8]._bDataType @ 1040
	.bits	2,32			; _mms_dict_Index2009[8]._size @ 1056
	.bits	_ODP_SafetyLimits_Umin_bal_delta,32		; _mms_dict_Index2009[8]._pObject @ 1088
	.bits	1,16			; _mms_dict_Index2009[8]._bProcessor @ 1120
	.space	16
	.bits	4,16			; _mms_dict_Index2009[9]._bAccessType @ 1152
	.bits	5,16			; _mms_dict_Index2009[9]._bDataType @ 1168
	.bits	1,32			; _mms_dict_Index2009[9]._size @ 1184
	.bits	_ODP_SafetyLimits_Charge_In_Thres_Cur,32		; _mms_dict_Index2009[9]._pObject @ 1216
	.bits	1,16			; _mms_dict_Index2009[9]._bProcessor @ 1248
	.space	16
	.bits	4,16			; _mms_dict_Index2009[10]._bAccessType @ 1280
	.bits	3,16			; _mms_dict_Index2009[10]._bDataType @ 1296
	.bits	2,32			; _mms_dict_Index2009[10]._size @ 1312
	.bits	_ODP_SafetyLimits_Overcurrent,32		; _mms_dict_Index2009[10]._pObject @ 1344
	.bits	1,16			; _mms_dict_Index2009[10]._bProcessor @ 1376
	.space	16
	.bits	4,16			; _mms_dict_Index2009[11]._bAccessType @ 1408
	.bits	3,16			; _mms_dict_Index2009[11]._bDataType @ 1424
	.bits	2,32			; _mms_dict_Index2009[11]._size @ 1440
	.bits	_ODP_SafetyLimits_OverVoltage,32		; _mms_dict_Index2009[11]._pObject @ 1472
	.bits	1,16			; _mms_dict_Index2009[11]._bProcessor @ 1504
	.space	16
	.bits	4,16			; _mms_dict_Index2009[12]._bAccessType @ 1536
	.bits	3,16			; _mms_dict_Index2009[12]._bDataType @ 1552
	.bits	2,32			; _mms_dict_Index2009[12]._size @ 1568
	.bits	_ODP_SafetyLimits_UnderVoltage,32		; _mms_dict_Index2009[12]._pObject @ 1600
	.bits	1,16			; _mms_dict_Index2009[12]._bProcessor @ 1632
	.space	16
	.bits	4,16			; _mms_dict_Index2009[13]._bAccessType @ 1664
	.bits	2,16			; _mms_dict_Index2009[13]._bDataType @ 1680
	.bits	1,32			; _mms_dict_Index2009[13]._size @ 1696
	.bits	_ODP_SafetyLimits_Resistor_Tmax,32		; _mms_dict_Index2009[13]._pObject @ 1728
	.bits	1,16			; _mms_dict_Index2009[13]._bProcessor @ 1760
	.space	16
	.bits	4,16			; _mms_dict_Index2009[14]._bAccessType @ 1792
	.bits	2,16			; _mms_dict_Index2009[14]._bDataType @ 1808
	.bits	1,32			; _mms_dict_Index2009[14]._size @ 1824
	.bits	_ODP_SafetyLimits_Resistor_Tmin,32		; _mms_dict_Index2009[14]._pObject @ 1856
	.bits	1,16			; _mms_dict_Index2009[14]._bProcessor @ 1888
	.space	16
	.bits	4,16			; _mms_dict_Index2009[15]._bAccessType @ 1920
	.bits	5,16			; _mms_dict_Index2009[15]._bDataType @ 1936
	.bits	1,32			; _mms_dict_Index2009[15]._size @ 1952
	.bits	_ODP_SafetyLimits_Resistor_Delay,32		; _mms_dict_Index2009[15]._pObject @ 1984
	.bits	1,16			; _mms_dict_Index2009[15]._bProcessor @ 2016
	.space	16
	.bits	4,16			; _mms_dict_Index2009[16]._bAccessType @ 2048
	.bits	2,16			; _mms_dict_Index2009[16]._bDataType @ 2064
	.bits	1,32			; _mms_dict_Index2009[16]._size @ 2080
	.bits	_ODP_SafetyLimits_Mosfet_Tmax,32		; _mms_dict_Index2009[16]._pObject @ 2112
	.bits	1,16			; _mms_dict_Index2009[16]._bProcessor @ 2144
	.space	16
	.bits	4,16			; _mms_dict_Index2009[17]._bAccessType @ 2176
	.bits	2,16			; _mms_dict_Index2009[17]._bDataType @ 2192
	.bits	1,32			; _mms_dict_Index2009[17]._size @ 2208
	.bits	_ODP_SafetyLimits_Mosfet_Tmin,32		; _mms_dict_Index2009[17]._pObject @ 2240
	.bits	1,16			; _mms_dict_Index2009[17]._bProcessor @ 2272
	.space	16
	.bits	4,16			; _mms_dict_Index2009[18]._bAccessType @ 2304
	.bits	5,16			; _mms_dict_Index2009[18]._bDataType @ 2320
	.bits	1,32			; _mms_dict_Index2009[18]._size @ 2336
	.bits	_ODP_SafetyLimits_Cell_Nb,32		; _mms_dict_Index2009[18]._pObject @ 2368
	.bits	1,16			; _mms_dict_Index2009[18]._bProcessor @ 2400
	.space	16
	.bits	4,16			; _mms_dict_Index2009[19]._bAccessType @ 2432
	.bits	2,16			; _mms_dict_Index2009[19]._bDataType @ 2448
	.bits	1,32			; _mms_dict_Index2009[19]._size @ 2464
	.bits	_ODP_SafetyLimits_T3_max,32		; _mms_dict_Index2009[19]._pObject @ 2496
	.bits	1,16			; _mms_dict_Index2009[19]._bProcessor @ 2528
	.space	16
	.bits	4,16			; _mms_dict_Index2009[20]._bAccessType @ 2560
	.bits	2,16			; _mms_dict_Index2009[20]._bDataType @ 2576
	.bits	1,32			; _mms_dict_Index2009[20]._size @ 2592
	.bits	_ODP_SafetyLimits_T3_min,32		; _mms_dict_Index2009[20]._pObject @ 2624
	.bits	1,16			; _mms_dict_Index2009[20]._bProcessor @ 2656
	.space	16
	.bits	4,16			; _mms_dict_Index2009[21]._bAccessType @ 2688
	.bits	5,16			; _mms_dict_Index2009[21]._bDataType @ 2704
	.bits	1,32			; _mms_dict_Index2009[21]._size @ 2720
	.bits	_ODP_SafetyLimits_Voltage_delay,32		; _mms_dict_Index2009[21]._pObject @ 2752
	.bits	1,16			; _mms_dict_Index2009[21]._bProcessor @ 2784
	.space	16
	.bits	4,16			; _mms_dict_Index2009[22]._bAccessType @ 2816
	.bits	5,16			; _mms_dict_Index2009[22]._bDataType @ 2832
	.bits	1,32			; _mms_dict_Index2009[22]._size @ 2848
	.bits	_ODP_SafetyLimits_Current_delay,32		; _mms_dict_Index2009[22]._pObject @ 2880
	.bits	1,16			; _mms_dict_Index2009[22]._bProcessor @ 2912
	.space	16
	.bits	4,16			; _mms_dict_Index2009[23]._bAccessType @ 2944
	.bits	3,16			; _mms_dict_Index2009[23]._bDataType @ 2960
	.bits	2,32			; _mms_dict_Index2009[23]._size @ 2976
	.bits	_ODP_SafetyLimits_UnderCurrent,32		; _mms_dict_Index2009[23]._pObject @ 3008
	.bits	1,16			; _mms_dict_Index2009[23]._bProcessor @ 3040
	.space	16
	.bits	4,16			; _mms_dict_Index2009[24]._bAccessType @ 3072
	.bits	5,16			; _mms_dict_Index2009[24]._bDataType @ 3088
	.bits	1,32			; _mms_dict_Index2009[24]._size @ 3104
	.bits	_ODP_SafetyLimits_SleepTimeout,32		; _mms_dict_Index2009[24]._pObject @ 3136
	.bits	1,16			; _mms_dict_Index2009[24]._bProcessor @ 3168
	.space	16
	.bits	4,16			; _mms_dict_Index2009[25]._bAccessType @ 3200
	.bits	5,16			; _mms_dict_Index2009[25]._bDataType @ 3216
	.bits	1,32			; _mms_dict_Index2009[25]._size @ 3232
	.bits	_ODP_SafetyLimits_BalancingTimeout,32		; _mms_dict_Index2009[25]._pObject @ 3264
	.bits	1,16			; _mms_dict_Index2009[25]._bProcessor @ 3296
	.space	16
	.bits	4,16			; _mms_dict_Index2009[26]._bAccessType @ 3328
	.bits	5,16			; _mms_dict_Index2009[26]._bDataType @ 3344
	.bits	1,32			; _mms_dict_Index2009[26]._size @ 3360
	.bits	_ODP_SafetyLimits_DamagedVoltage,32		; _mms_dict_Index2009[26]._pObject @ 3392
	.bits	1,16			; _mms_dict_Index2009[26]._bProcessor @ 3424
	.space	16
	.bits	4,16			; _mms_dict_Index2009[27]._bAccessType @ 3456
	.bits	5,16			; _mms_dict_Index2009[27]._bDataType @ 3472
	.bits	1,32			; _mms_dict_Index2009[27]._size @ 3488
	.bits	_ODP_SafetyLimits_Temp_Delay,32		; _mms_dict_Index2009[27]._pObject @ 3520
	.bits	1,16			; _mms_dict_Index2009[27]._bProcessor @ 3552
	.space	16
	.bits	4,16			; _mms_dict_Index2009[28]._bAccessType @ 3584
	.bits	5,16			; _mms_dict_Index2009[28]._bDataType @ 3600
	.bits	1,32			; _mms_dict_Index2009[28]._size @ 3616
	.bits	_ODP_SafetyLimits_Low_Voltage_Current_Delay,32		; _mms_dict_Index2009[28]._pObject @ 3648
	.bits	1,16			; _mms_dict_Index2009[28]._bProcessor @ 3680
	.space	16
	.bits	4,16			; _mms_dict_Index2009[29]._bAccessType @ 3712
	.bits	3,16			; _mms_dict_Index2009[29]._bDataType @ 3728
	.bits	2,32			; _mms_dict_Index2009[29]._size @ 3744
	.bits	_ODP_SafetyLimits_Low_Voltage_Current_Allow,32		; _mms_dict_Index2009[29]._pObject @ 3776
	.bits	1,16			; _mms_dict_Index2009[29]._bProcessor @ 3808
	.space	16
	.bits	4,16			; _mms_dict_Index2009[30]._bAccessType @ 3840
	.bits	5,16			; _mms_dict_Index2009[30]._bDataType @ 3856
	.bits	1,32			; _mms_dict_Index2009[30]._size @ 3872
	.bits	_ODP_SafetyLimits_Low_Voltage_Charge_Delay,32		; _mms_dict_Index2009[30]._pObject @ 3904
	.bits	1,16			; _mms_dict_Index2009[30]._bProcessor @ 3936
	.space	16

$C$DW$487	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_Index2009")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_mms_dict_Index2009")
	.dwattr $C$DW$487, DW_AT_location[DW_OP_addr _mms_dict_Index2009]
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$179)
	.dwattr $C$DW$487, DW_AT_external
	.global	_mms_dict_objdict
	.sect	".econst:_mms_dict_objdict"
	.clink
	.align	2
_mms_dict_objdict:
	.bits	_mms_dict_Index1000,32		; _mms_dict_objdict[0]._pSubindex @ 0
	.bits	1,16			; _mms_dict_objdict[0]._bSubCount @ 32
	.bits	4096,16			; _mms_dict_objdict[0]._index @ 48
	.bits	_mms_dict_Index1001,32		; _mms_dict_objdict[1]._pSubindex @ 64
	.bits	1,16			; _mms_dict_objdict[1]._bSubCount @ 96
	.bits	4097,16			; _mms_dict_objdict[1]._index @ 112
	.bits	_mms_dict_Index1005,32		; _mms_dict_objdict[2]._pSubindex @ 128
	.bits	1,16			; _mms_dict_objdict[2]._bSubCount @ 160
	.bits	4101,16			; _mms_dict_objdict[2]._index @ 176
	.bits	_mms_dict_Index1006,32		; _mms_dict_objdict[3]._pSubindex @ 192
	.bits	1,16			; _mms_dict_objdict[3]._bSubCount @ 224
	.bits	4102,16			; _mms_dict_objdict[3]._index @ 240
	.bits	_mms_dict_Index100A,32		; _mms_dict_objdict[4]._pSubindex @ 256
	.bits	1,16			; _mms_dict_objdict[4]._bSubCount @ 288
	.bits	4106,16			; _mms_dict_objdict[4]._index @ 304
	.bits	_mms_dict_Index1014,32		; _mms_dict_objdict[5]._pSubindex @ 320
	.bits	1,16			; _mms_dict_objdict[5]._bSubCount @ 352
	.bits	4116,16			; _mms_dict_objdict[5]._index @ 368
	.bits	_mms_dict_Index1018,32		; _mms_dict_objdict[6]._pSubindex @ 384
	.bits	5,16			; _mms_dict_objdict[6]._bSubCount @ 416
	.bits	4120,16			; _mms_dict_objdict[6]._index @ 432
	.bits	_mms_dict_Index1200,32		; _mms_dict_objdict[7]._pSubindex @ 448
	.bits	3,16			; _mms_dict_objdict[7]._bSubCount @ 480
	.bits	4608,16			; _mms_dict_objdict[7]._index @ 496
	.bits	_mms_dict_Index1280,32		; _mms_dict_objdict[8]._pSubindex @ 512
	.bits	4,16			; _mms_dict_objdict[8]._bSubCount @ 544
	.bits	4736,16			; _mms_dict_objdict[8]._index @ 560
	.bits	_mms_dict_Index1400,32		; _mms_dict_objdict[9]._pSubindex @ 576
	.bits	6,16			; _mms_dict_objdict[9]._bSubCount @ 608
	.bits	5120,16			; _mms_dict_objdict[9]._index @ 624
	.bits	_mms_dict_Index1600,32		; _mms_dict_objdict[10]._pSubindex @ 640
	.bits	4,16			; _mms_dict_objdict[10]._bSubCount @ 672
	.bits	5632,16			; _mms_dict_objdict[10]._index @ 688
	.bits	_mms_dict_Index1800,32		; _mms_dict_objdict[11]._pSubindex @ 704
	.bits	6,16			; _mms_dict_objdict[11]._bSubCount @ 736
	.bits	6144,16			; _mms_dict_objdict[11]._index @ 752
	.bits	_mms_dict_Index1801,32		; _mms_dict_objdict[12]._pSubindex @ 768
	.bits	6,16			; _mms_dict_objdict[12]._bSubCount @ 800
	.bits	6145,16			; _mms_dict_objdict[12]._index @ 816
	.bits	_mms_dict_Index1802,32		; _mms_dict_objdict[13]._pSubindex @ 832
	.bits	6,16			; _mms_dict_objdict[13]._bSubCount @ 864
	.bits	6146,16			; _mms_dict_objdict[13]._index @ 880
	.bits	_mms_dict_Index1803,32		; _mms_dict_objdict[14]._pSubindex @ 896
	.bits	6,16			; _mms_dict_objdict[14]._bSubCount @ 928
	.bits	6147,16			; _mms_dict_objdict[14]._index @ 944
	.bits	_mms_dict_Index1A00,32		; _mms_dict_objdict[15]._pSubindex @ 960
	.bits	8,16			; _mms_dict_objdict[15]._bSubCount @ 992
	.bits	6656,16			; _mms_dict_objdict[15]._index @ 1008
	.bits	_mms_dict_Index1A01,32		; _mms_dict_objdict[16]._pSubindex @ 1024
	.bits	9,16			; _mms_dict_objdict[16]._bSubCount @ 1056
	.bits	6657,16			; _mms_dict_objdict[16]._index @ 1072
	.bits	_mms_dict_Index1A02,32		; _mms_dict_objdict[17]._pSubindex @ 1088
	.bits	6,16			; _mms_dict_objdict[17]._bSubCount @ 1120
	.bits	6658,16			; _mms_dict_objdict[17]._index @ 1136
	.bits	_mms_dict_Index1A03,32		; _mms_dict_objdict[18]._pSubindex @ 1152
	.bits	6,16			; _mms_dict_objdict[18]._bSubCount @ 1184
	.bits	6659,16			; _mms_dict_objdict[18]._index @ 1200
	.bits	_mms_dict_Index2000,32		; _mms_dict_objdict[19]._pSubindex @ 1216
	.bits	1,16			; _mms_dict_objdict[19]._bSubCount @ 1248
	.bits	8192,16			; _mms_dict_objdict[19]._index @ 1264
	.bits	_mms_dict_Index2001,32		; _mms_dict_objdict[20]._pSubindex @ 1280
	.bits	1,16			; _mms_dict_objdict[20]._bSubCount @ 1312
	.bits	8193,16			; _mms_dict_objdict[20]._index @ 1328
	.bits	_mms_dict_Index2002,32		; _mms_dict_objdict[21]._pSubindex @ 1344
	.bits	1,16			; _mms_dict_objdict[21]._bSubCount @ 1376
	.bits	8194,16			; _mms_dict_objdict[21]._index @ 1392
	.bits	_mms_dict_Index2003,32		; _mms_dict_objdict[22]._pSubindex @ 1408
	.bits	1,16			; _mms_dict_objdict[22]._bSubCount @ 1440
	.bits	8195,16			; _mms_dict_objdict[22]._index @ 1456
	.bits	_mms_dict_Index2004,32		; _mms_dict_objdict[23]._pSubindex @ 1472
	.bits	13,16			; _mms_dict_objdict[23]._bSubCount @ 1504
	.bits	8196,16			; _mms_dict_objdict[23]._index @ 1520
	.bits	_mms_dict_Index2005,32		; _mms_dict_objdict[24]._pSubindex @ 1536
	.bits	1,16			; _mms_dict_objdict[24]._bSubCount @ 1568
	.bits	8197,16			; _mms_dict_objdict[24]._index @ 1584
	.bits	_mms_dict_Index2006,32		; _mms_dict_objdict[25]._pSubindex @ 1600
	.bits	4,16			; _mms_dict_objdict[25]._bSubCount @ 1632
	.bits	8198,16			; _mms_dict_objdict[25]._index @ 1648
	.bits	_mms_dict_Index2007,32		; _mms_dict_objdict[26]._pSubindex @ 1664
	.bits	1,16			; _mms_dict_objdict[26]._bSubCount @ 1696
	.bits	8199,16			; _mms_dict_objdict[26]._index @ 1712
	.bits	_mms_dict_Index2008,32		; _mms_dict_objdict[27]._pSubindex @ 1728
	.bits	16,16			; _mms_dict_objdict[27]._bSubCount @ 1760
	.bits	8200,16			; _mms_dict_objdict[27]._index @ 1776
	.bits	_mms_dict_Index2009,32		; _mms_dict_objdict[28]._pSubindex @ 1792
	.bits	31,16			; _mms_dict_objdict[28]._bSubCount @ 1824
	.bits	8201,16			; _mms_dict_objdict[28]._index @ 1840
	.bits	_mms_dict_Index200A,32		; _mms_dict_objdict[29]._pSubindex @ 1856
	.bits	9,16			; _mms_dict_objdict[29]._bSubCount @ 1888
	.bits	8202,16			; _mms_dict_objdict[29]._index @ 1904
	.bits	_mms_dict_Index200B,32		; _mms_dict_objdict[30]._pSubindex @ 1920
	.bits	9,16			; _mms_dict_objdict[30]._bSubCount @ 1952
	.bits	8203,16			; _mms_dict_objdict[30]._index @ 1968
	.bits	_mms_dict_Index200C,32		; _mms_dict_objdict[31]._pSubindex @ 1984
	.bits	9,16			; _mms_dict_objdict[31]._bSubCount @ 2016
	.bits	8204,16			; _mms_dict_objdict[31]._index @ 2032
	.bits	_mms_dict_Index200D,32		; _mms_dict_objdict[32]._pSubindex @ 2048
	.bits	9,16			; _mms_dict_objdict[32]._bSubCount @ 2080
	.bits	8205,16			; _mms_dict_objdict[32]._index @ 2096
	.bits	_mms_dict_Index200E,32		; _mms_dict_objdict[33]._pSubindex @ 2112
	.bits	1,16			; _mms_dict_objdict[33]._bSubCount @ 2144
	.bits	8206,16			; _mms_dict_objdict[33]._index @ 2160
	.bits	_mms_dict_Index200F,32		; _mms_dict_objdict[34]._pSubindex @ 2176
	.bits	4,16			; _mms_dict_objdict[34]._bSubCount @ 2208
	.bits	8207,16			; _mms_dict_objdict[34]._index @ 2224
	.bits	_mms_dict_Index2010,32		; _mms_dict_objdict[35]._pSubindex @ 2240
	.bits	3,16			; _mms_dict_objdict[35]._bSubCount @ 2272
	.bits	8208,16			; _mms_dict_objdict[35]._index @ 2288
	.bits	_mms_dict_Index2011,32		; _mms_dict_objdict[36]._pSubindex @ 2304
	.bits	6,16			; _mms_dict_objdict[36]._bSubCount @ 2336
	.bits	8209,16			; _mms_dict_objdict[36]._index @ 2352
	.bits	_mms_dict_Index2012,32		; _mms_dict_objdict[37]._pSubindex @ 2368
	.bits	8,16			; _mms_dict_objdict[37]._bSubCount @ 2400
	.bits	8210,16			; _mms_dict_objdict[37]._index @ 2416
	.bits	_mms_dict_Index2013,32		; _mms_dict_objdict[38]._pSubindex @ 2432
	.bits	5,16			; _mms_dict_objdict[38]._bSubCount @ 2464
	.bits	8211,16			; _mms_dict_objdict[38]._index @ 2480
	.bits	_mms_dict_Index2014,32		; _mms_dict_objdict[39]._pSubindex @ 2496
	.bits	9,16			; _mms_dict_objdict[39]._bSubCount @ 2528
	.bits	8212,16			; _mms_dict_objdict[39]._index @ 2544
	.bits	_mms_dict_Index2015,32		; _mms_dict_objdict[40]._pSubindex @ 2560
	.bits	7,16			; _mms_dict_objdict[40]._bSubCount @ 2592
	.bits	8213,16			; _mms_dict_objdict[40]._index @ 2608
	.bits	_mms_dict_Index201C,32		; _mms_dict_objdict[41]._pSubindex @ 2624
	.bits	1,16			; _mms_dict_objdict[41]._bSubCount @ 2656
	.bits	8220,16			; _mms_dict_objdict[41]._index @ 2672
	.bits	_mms_dict_Index201E,32		; _mms_dict_objdict[42]._pSubindex @ 2688
	.bits	1,16			; _mms_dict_objdict[42]._bSubCount @ 2720
	.bits	8222,16			; _mms_dict_objdict[42]._index @ 2736
	.bits	_mms_dict_Index2023,32		; _mms_dict_objdict[43]._pSubindex @ 2752
	.bits	6,16			; _mms_dict_objdict[43]._bSubCount @ 2784
	.bits	8227,16			; _mms_dict_objdict[43]._index @ 2800
	.bits	_mms_dict_Index2040,32		; _mms_dict_objdict[44]._pSubindex @ 2816
	.bits	11,16			; _mms_dict_objdict[44]._bSubCount @ 2848
	.bits	8256,16			; _mms_dict_objdict[44]._index @ 2864
	.bits	_mms_dict_Index2041,32		; _mms_dict_objdict[45]._pSubindex @ 2880
	.bits	1,16			; _mms_dict_objdict[45]._bSubCount @ 2912
	.bits	8257,16			; _mms_dict_objdict[45]._index @ 2928
	.bits	_mms_dict_Index2100,32		; _mms_dict_objdict[46]._pSubindex @ 2944
	.bits	4,16			; _mms_dict_objdict[46]._bSubCount @ 2976
	.bits	8448,16			; _mms_dict_objdict[46]._index @ 2992
	.bits	_mms_dict_Index2101,32		; _mms_dict_objdict[47]._pSubindex @ 3008
	.bits	1,16			; _mms_dict_objdict[47]._bSubCount @ 3040
	.bits	8449,16			; _mms_dict_objdict[47]._index @ 3056
	.bits	_mms_dict_Index2102,32		; _mms_dict_objdict[48]._pSubindex @ 3072
	.bits	1,16			; _mms_dict_objdict[48]._bSubCount @ 3104
	.bits	8450,16			; _mms_dict_objdict[48]._index @ 3120
	.bits	_mms_dict_Index2103,32		; _mms_dict_objdict[49]._pSubindex @ 3136
	.bits	1,16			; _mms_dict_objdict[49]._bSubCount @ 3168
	.bits	8451,16			; _mms_dict_objdict[49]._index @ 3184
	.bits	_mms_dict_Index2104,32		; _mms_dict_objdict[50]._pSubindex @ 3200
	.bits	1,16			; _mms_dict_objdict[50]._bSubCount @ 3232
	.bits	8452,16			; _mms_dict_objdict[50]._index @ 3248
	.bits	_mms_dict_Index2105,32		; _mms_dict_objdict[51]._pSubindex @ 3264
	.bits	7,16			; _mms_dict_objdict[51]._bSubCount @ 3296
	.bits	8453,16			; _mms_dict_objdict[51]._index @ 3312
	.bits	_mms_dict_Index2106,32		; _mms_dict_objdict[52]._pSubindex @ 3328
	.bits	3,16			; _mms_dict_objdict[52]._bSubCount @ 3360
	.bits	8454,16			; _mms_dict_objdict[52]._index @ 3376
	.bits	_mms_dict_Index2200,32		; _mms_dict_objdict[53]._pSubindex @ 3392
	.bits	20,16			; _mms_dict_objdict[53]._bSubCount @ 3424
	.bits	8704,16			; _mms_dict_objdict[53]._index @ 3440
	.bits	_mms_dict_Index2F00,32		; _mms_dict_objdict[54]._pSubindex @ 3456
	.bits	1,16			; _mms_dict_objdict[54]._bSubCount @ 3488
	.bits	12032,16			; _mms_dict_objdict[54]._index @ 3504
	.bits	_mms_dict_Index2F01,32		; _mms_dict_objdict[55]._pSubindex @ 3520
	.bits	1,16			; _mms_dict_objdict[55]._bSubCount @ 3552
	.bits	12033,16			; _mms_dict_objdict[55]._index @ 3568
	.bits	_mms_dict_Index2F02,32		; _mms_dict_objdict[56]._pSubindex @ 3584
	.bits	1,16			; _mms_dict_objdict[56]._bSubCount @ 3616
	.bits	12034,16			; _mms_dict_objdict[56]._index @ 3632
	.bits	_mms_dict_Index2F03,32		; _mms_dict_objdict[57]._pSubindex @ 3648
	.bits	1,16			; _mms_dict_objdict[57]._bSubCount @ 3680
	.bits	12035,16			; _mms_dict_objdict[57]._index @ 3696
	.bits	_mms_dict_Index6100,32		; _mms_dict_objdict[58]._pSubindex @ 3712
	.bits	3,16			; _mms_dict_objdict[58]._bSubCount @ 3744
	.bits	24832,16			; _mms_dict_objdict[58]._index @ 3760
	.bits	_mms_dict_Index6300,32		; _mms_dict_objdict[59]._pSubindex @ 3776
	.bits	3,16			; _mms_dict_objdict[59]._bSubCount @ 3808
	.bits	25344,16			; _mms_dict_objdict[59]._index @ 3824
	.bits	_mms_dict_Index6401,32		; _mms_dict_objdict[60]._pSubindex @ 3840
	.bits	24,16			; _mms_dict_objdict[60]._bSubCount @ 3872
	.bits	25601,16			; _mms_dict_objdict[60]._index @ 3888
	.bits	_mms_dict_Index6411,32		; _mms_dict_objdict[61]._pSubindex @ 3904
	.bits	17,16			; _mms_dict_objdict[61]._bSubCount @ 3936
	.bits	25617,16			; _mms_dict_objdict[61]._index @ 3952
	.bits	_mms_dict_Index642F,32		; _mms_dict_objdict[62]._pSubindex @ 3968
	.bits	24,16			; _mms_dict_objdict[62]._bSubCount @ 4000
	.bits	25647,16			; _mms_dict_objdict[62]._index @ 4016
	.bits	_mms_dict_Index6430,32		; _mms_dict_objdict[63]._pSubindex @ 4032
	.bits	24,16			; _mms_dict_objdict[63]._bSubCount @ 4064
	.bits	25648,16			; _mms_dict_objdict[63]._index @ 4080
	.bits	_mms_dict_Index6431,32		; _mms_dict_objdict[64]._pSubindex @ 4096
	.bits	24,16			; _mms_dict_objdict[64]._bSubCount @ 4128
	.bits	25649,16			; _mms_dict_objdict[64]._index @ 4144
	.bits	_mms_dict_Index6442,32		; _mms_dict_objdict[65]._pSubindex @ 4160
	.bits	17,16			; _mms_dict_objdict[65]._bSubCount @ 4192
	.bits	25666,16			; _mms_dict_objdict[65]._index @ 4208
	.bits	_mms_dict_Index6446,32		; _mms_dict_objdict[66]._pSubindex @ 4224
	.bits	17,16			; _mms_dict_objdict[66]._bSubCount @ 4256
	.bits	25670,16			; _mms_dict_objdict[66]._index @ 4272
	.bits	_mms_dict_Index6450,32		; _mms_dict_objdict[67]._pSubindex @ 4288
	.bits	17,16			; _mms_dict_objdict[67]._bSubCount @ 4320
	.bits	25680,16			; _mms_dict_objdict[67]._index @ 4336
	.bits	_mms_dict_Index8040,32		; _mms_dict_objdict[68]._pSubindex @ 4352
	.bits	1,16			; _mms_dict_objdict[68]._bSubCount @ 4384
	.bits	32832,16			; _mms_dict_objdict[68]._index @ 4400
	.bits	_mms_dict_Index8041,32		; _mms_dict_objdict[69]._pSubindex @ 4416
	.bits	1,16			; _mms_dict_objdict[69]._bSubCount @ 4448
	.bits	32833,16			; _mms_dict_objdict[69]._index @ 4464
	.bits	_mms_dict_Index8502,32		; _mms_dict_objdict[70]._pSubindex @ 4480
	.bits	1,16			; _mms_dict_objdict[70]._bSubCount @ 4512
	.bits	34050,16			; _mms_dict_objdict[70]._index @ 4528

$C$DW$488	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_objdict")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_mms_dict_objdict")
	.dwattr $C$DW$488, DW_AT_location[DW_OP_addr _mms_dict_objdict]
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$488, DW_AT_external
	.global	_ODI_mms_dict_Data
_ODI_mms_dict_Data:	.usect	".ebss",306,1,1
$C$DW$489	.dwtag  DW_TAG_variable, DW_AT_name("ODI_mms_dict_Data")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_ODI_mms_dict_Data")
	.dwattr $C$DW$489, DW_AT_location[DW_OP_addr _ODI_mms_dict_Data]
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$489, DW_AT_external
	.global	_ODI_EEPROM_INDEXES
	.sect	".econst:_ODI_EEPROM_INDEXES"
	.clink
	.align	1
_ODI_EEPROM_INDEXES:
	.bits	8192,16			; _ODI_EEPROM_INDEXES[0]._index @ 0
	.bits	0,16			; _ODI_EEPROM_INDEXES[0]._subindex @ 16
	.bits	4,16			; _ODI_EEPROM_INDEXES[0]._size @ 32
	.bits	0,16			; _ODI_EEPROM_INDEXES[0]._address @ 48
	.bits	8193,16			; _ODI_EEPROM_INDEXES[1]._index @ 64
	.bits	0,16			; _ODI_EEPROM_INDEXES[1]._subindex @ 80
	.bits	4,16			; _ODI_EEPROM_INDEXES[1]._size @ 96
	.bits	4,16			; _ODI_EEPROM_INDEXES[1]._address @ 112
	.bits	8194,16			; _ODI_EEPROM_INDEXES[2]._index @ 128
	.bits	0,16			; _ODI_EEPROM_INDEXES[2]._subindex @ 144
	.bits	2,16			; _ODI_EEPROM_INDEXES[2]._size @ 160
	.bits	8,16			; _ODI_EEPROM_INDEXES[2]._address @ 176
	.bits	8195,16			; _ODI_EEPROM_INDEXES[3]._index @ 192
	.bits	0,16			; _ODI_EEPROM_INDEXES[3]._subindex @ 208
	.bits	4,16			; _ODI_EEPROM_INDEXES[3]._size @ 224
	.bits	10,16			; _ODI_EEPROM_INDEXES[3]._address @ 240
	.bits	8201,16			; _ODI_EEPROM_INDEXES[4]._index @ 256
	.bits	1,16			; _ODI_EEPROM_INDEXES[4]._subindex @ 272
	.bits	2,16			; _ODI_EEPROM_INDEXES[4]._size @ 288
	.bits	14,16			; _ODI_EEPROM_INDEXES[4]._address @ 304
	.bits	8201,16			; _ODI_EEPROM_INDEXES[5]._index @ 320
	.bits	2,16			; _ODI_EEPROM_INDEXES[5]._subindex @ 336
	.bits	2,16			; _ODI_EEPROM_INDEXES[5]._size @ 352
	.bits	16,16			; _ODI_EEPROM_INDEXES[5]._address @ 368
	.bits	8201,16			; _ODI_EEPROM_INDEXES[6]._index @ 384
	.bits	3,16			; _ODI_EEPROM_INDEXES[6]._subindex @ 400
	.bits	1,16			; _ODI_EEPROM_INDEXES[6]._size @ 416
	.bits	18,16			; _ODI_EEPROM_INDEXES[6]._address @ 432
	.bits	8201,16			; _ODI_EEPROM_INDEXES[7]._index @ 448
	.bits	4,16			; _ODI_EEPROM_INDEXES[7]._subindex @ 464
	.bits	1,16			; _ODI_EEPROM_INDEXES[7]._size @ 480
	.bits	19,16			; _ODI_EEPROM_INDEXES[7]._address @ 496
	.bits	8201,16			; _ODI_EEPROM_INDEXES[8]._index @ 512
	.bits	5,16			; _ODI_EEPROM_INDEXES[8]._subindex @ 528
	.bits	2,16			; _ODI_EEPROM_INDEXES[8]._size @ 544
	.bits	20,16			; _ODI_EEPROM_INDEXES[8]._address @ 560
	.bits	8201,16			; _ODI_EEPROM_INDEXES[9]._index @ 576
	.bits	6,16			; _ODI_EEPROM_INDEXES[9]._subindex @ 592
	.bits	2,16			; _ODI_EEPROM_INDEXES[9]._size @ 608
	.bits	22,16			; _ODI_EEPROM_INDEXES[9]._address @ 624
	.bits	8201,16			; _ODI_EEPROM_INDEXES[10]._index @ 640
	.bits	7,16			; _ODI_EEPROM_INDEXES[10]._subindex @ 656
	.bits	2,16			; _ODI_EEPROM_INDEXES[10]._size @ 672
	.bits	24,16			; _ODI_EEPROM_INDEXES[10]._address @ 688
	.bits	8201,16			; _ODI_EEPROM_INDEXES[11]._index @ 704
	.bits	8,16			; _ODI_EEPROM_INDEXES[11]._subindex @ 720
	.bits	2,16			; _ODI_EEPROM_INDEXES[11]._size @ 736
	.bits	26,16			; _ODI_EEPROM_INDEXES[11]._address @ 752
	.bits	8201,16			; _ODI_EEPROM_INDEXES[12]._index @ 768
	.bits	9,16			; _ODI_EEPROM_INDEXES[12]._subindex @ 784
	.bits	1,16			; _ODI_EEPROM_INDEXES[12]._size @ 800
	.bits	28,16			; _ODI_EEPROM_INDEXES[12]._address @ 816
	.bits	8201,16			; _ODI_EEPROM_INDEXES[13]._index @ 832
	.bits	10,16			; _ODI_EEPROM_INDEXES[13]._subindex @ 848
	.bits	2,16			; _ODI_EEPROM_INDEXES[13]._size @ 864
	.bits	29,16			; _ODI_EEPROM_INDEXES[13]._address @ 880
	.bits	8201,16			; _ODI_EEPROM_INDEXES[14]._index @ 896
	.bits	11,16			; _ODI_EEPROM_INDEXES[14]._subindex @ 912
	.bits	2,16			; _ODI_EEPROM_INDEXES[14]._size @ 928
	.bits	31,16			; _ODI_EEPROM_INDEXES[14]._address @ 944
	.bits	8201,16			; _ODI_EEPROM_INDEXES[15]._index @ 960
	.bits	12,16			; _ODI_EEPROM_INDEXES[15]._subindex @ 976
	.bits	2,16			; _ODI_EEPROM_INDEXES[15]._size @ 992
	.bits	33,16			; _ODI_EEPROM_INDEXES[15]._address @ 1008
	.bits	8201,16			; _ODI_EEPROM_INDEXES[16]._index @ 1024
	.bits	13,16			; _ODI_EEPROM_INDEXES[16]._subindex @ 1040
	.bits	1,16			; _ODI_EEPROM_INDEXES[16]._size @ 1056
	.bits	35,16			; _ODI_EEPROM_INDEXES[16]._address @ 1072
	.bits	8201,16			; _ODI_EEPROM_INDEXES[17]._index @ 1088
	.bits	14,16			; _ODI_EEPROM_INDEXES[17]._subindex @ 1104
	.bits	1,16			; _ODI_EEPROM_INDEXES[17]._size @ 1120
	.bits	36,16			; _ODI_EEPROM_INDEXES[17]._address @ 1136
	.bits	8201,16			; _ODI_EEPROM_INDEXES[18]._index @ 1152
	.bits	15,16			; _ODI_EEPROM_INDEXES[18]._subindex @ 1168
	.bits	1,16			; _ODI_EEPROM_INDEXES[18]._size @ 1184
	.bits	37,16			; _ODI_EEPROM_INDEXES[18]._address @ 1200
	.bits	8201,16			; _ODI_EEPROM_INDEXES[19]._index @ 1216
	.bits	16,16			; _ODI_EEPROM_INDEXES[19]._subindex @ 1232
	.bits	1,16			; _ODI_EEPROM_INDEXES[19]._size @ 1248
	.bits	38,16			; _ODI_EEPROM_INDEXES[19]._address @ 1264
	.bits	8201,16			; _ODI_EEPROM_INDEXES[20]._index @ 1280
	.bits	17,16			; _ODI_EEPROM_INDEXES[20]._subindex @ 1296
	.bits	1,16			; _ODI_EEPROM_INDEXES[20]._size @ 1312
	.bits	39,16			; _ODI_EEPROM_INDEXES[20]._address @ 1328
	.bits	8201,16			; _ODI_EEPROM_INDEXES[21]._index @ 1344
	.bits	18,16			; _ODI_EEPROM_INDEXES[21]._subindex @ 1360
	.bits	1,16			; _ODI_EEPROM_INDEXES[21]._size @ 1376
	.bits	40,16			; _ODI_EEPROM_INDEXES[21]._address @ 1392
	.bits	8201,16			; _ODI_EEPROM_INDEXES[22]._index @ 1408
	.bits	19,16			; _ODI_EEPROM_INDEXES[22]._subindex @ 1424
	.bits	1,16			; _ODI_EEPROM_INDEXES[22]._size @ 1440
	.bits	41,16			; _ODI_EEPROM_INDEXES[22]._address @ 1456
	.bits	8201,16			; _ODI_EEPROM_INDEXES[23]._index @ 1472
	.bits	20,16			; _ODI_EEPROM_INDEXES[23]._subindex @ 1488
	.bits	1,16			; _ODI_EEPROM_INDEXES[23]._size @ 1504
	.bits	42,16			; _ODI_EEPROM_INDEXES[23]._address @ 1520
	.bits	8201,16			; _ODI_EEPROM_INDEXES[24]._index @ 1536
	.bits	21,16			; _ODI_EEPROM_INDEXES[24]._subindex @ 1552
	.bits	1,16			; _ODI_EEPROM_INDEXES[24]._size @ 1568
	.bits	43,16			; _ODI_EEPROM_INDEXES[24]._address @ 1584
	.bits	8201,16			; _ODI_EEPROM_INDEXES[25]._index @ 1600
	.bits	22,16			; _ODI_EEPROM_INDEXES[25]._subindex @ 1616
	.bits	1,16			; _ODI_EEPROM_INDEXES[25]._size @ 1632
	.bits	44,16			; _ODI_EEPROM_INDEXES[25]._address @ 1648
	.bits	8201,16			; _ODI_EEPROM_INDEXES[26]._index @ 1664
	.bits	23,16			; _ODI_EEPROM_INDEXES[26]._subindex @ 1680
	.bits	2,16			; _ODI_EEPROM_INDEXES[26]._size @ 1696
	.bits	45,16			; _ODI_EEPROM_INDEXES[26]._address @ 1712
	.bits	8201,16			; _ODI_EEPROM_INDEXES[27]._index @ 1728
	.bits	24,16			; _ODI_EEPROM_INDEXES[27]._subindex @ 1744
	.bits	1,16			; _ODI_EEPROM_INDEXES[27]._size @ 1760
	.bits	47,16			; _ODI_EEPROM_INDEXES[27]._address @ 1776
	.bits	8201,16			; _ODI_EEPROM_INDEXES[28]._index @ 1792
	.bits	25,16			; _ODI_EEPROM_INDEXES[28]._subindex @ 1808
	.bits	1,16			; _ODI_EEPROM_INDEXES[28]._size @ 1824
	.bits	48,16			; _ODI_EEPROM_INDEXES[28]._address @ 1840
	.bits	8201,16			; _ODI_EEPROM_INDEXES[29]._index @ 1856
	.bits	26,16			; _ODI_EEPROM_INDEXES[29]._subindex @ 1872
	.bits	1,16			; _ODI_EEPROM_INDEXES[29]._size @ 1888
	.bits	49,16			; _ODI_EEPROM_INDEXES[29]._address @ 1904
	.bits	8201,16			; _ODI_EEPROM_INDEXES[30]._index @ 1920
	.bits	27,16			; _ODI_EEPROM_INDEXES[30]._subindex @ 1936
	.bits	1,16			; _ODI_EEPROM_INDEXES[30]._size @ 1952
	.bits	50,16			; _ODI_EEPROM_INDEXES[30]._address @ 1968
	.bits	8201,16			; _ODI_EEPROM_INDEXES[31]._index @ 1984
	.bits	28,16			; _ODI_EEPROM_INDEXES[31]._subindex @ 2000
	.bits	1,16			; _ODI_EEPROM_INDEXES[31]._size @ 2016
	.bits	51,16			; _ODI_EEPROM_INDEXES[31]._address @ 2032
	.bits	8201,16			; _ODI_EEPROM_INDEXES[32]._index @ 2048
	.bits	29,16			; _ODI_EEPROM_INDEXES[32]._subindex @ 2064
	.bits	2,16			; _ODI_EEPROM_INDEXES[32]._size @ 2080
	.bits	52,16			; _ODI_EEPROM_INDEXES[32]._address @ 2096
	.bits	8201,16			; _ODI_EEPROM_INDEXES[33]._index @ 2112
	.bits	30,16			; _ODI_EEPROM_INDEXES[33]._subindex @ 2128
	.bits	1,16			; _ODI_EEPROM_INDEXES[33]._size @ 2144
	.bits	54,16			; _ODI_EEPROM_INDEXES[33]._address @ 2160
	.bits	8202,16			; _ODI_EEPROM_INDEXES[34]._index @ 2176
	.bits	1,16			; _ODI_EEPROM_INDEXES[34]._subindex @ 2192
	.bits	1,16			; _ODI_EEPROM_INDEXES[34]._size @ 2208
	.bits	55,16			; _ODI_EEPROM_INDEXES[34]._address @ 2224
	.bits	8202,16			; _ODI_EEPROM_INDEXES[35]._index @ 2240
	.bits	2,16			; _ODI_EEPROM_INDEXES[35]._subindex @ 2256
	.bits	1,16			; _ODI_EEPROM_INDEXES[35]._size @ 2272
	.bits	56,16			; _ODI_EEPROM_INDEXES[35]._address @ 2288
	.bits	8202,16			; _ODI_EEPROM_INDEXES[36]._index @ 2304
	.bits	3,16			; _ODI_EEPROM_INDEXES[36]._subindex @ 2320
	.bits	1,16			; _ODI_EEPROM_INDEXES[36]._size @ 2336
	.bits	57,16			; _ODI_EEPROM_INDEXES[36]._address @ 2352
	.bits	8202,16			; _ODI_EEPROM_INDEXES[37]._index @ 2368
	.bits	4,16			; _ODI_EEPROM_INDEXES[37]._subindex @ 2384
	.bits	1,16			; _ODI_EEPROM_INDEXES[37]._size @ 2400
	.bits	58,16			; _ODI_EEPROM_INDEXES[37]._address @ 2416
	.bits	8202,16			; _ODI_EEPROM_INDEXES[38]._index @ 2432
	.bits	5,16			; _ODI_EEPROM_INDEXES[38]._subindex @ 2448
	.bits	1,16			; _ODI_EEPROM_INDEXES[38]._size @ 2464
	.bits	59,16			; _ODI_EEPROM_INDEXES[38]._address @ 2480
	.bits	8202,16			; _ODI_EEPROM_INDEXES[39]._index @ 2496
	.bits	6,16			; _ODI_EEPROM_INDEXES[39]._subindex @ 2512
	.bits	1,16			; _ODI_EEPROM_INDEXES[39]._size @ 2528
	.bits	60,16			; _ODI_EEPROM_INDEXES[39]._address @ 2544
	.bits	8202,16			; _ODI_EEPROM_INDEXES[40]._index @ 2560
	.bits	7,16			; _ODI_EEPROM_INDEXES[40]._subindex @ 2576
	.bits	1,16			; _ODI_EEPROM_INDEXES[40]._size @ 2592
	.bits	61,16			; _ODI_EEPROM_INDEXES[40]._address @ 2608
	.bits	8202,16			; _ODI_EEPROM_INDEXES[41]._index @ 2624
	.bits	8,16			; _ODI_EEPROM_INDEXES[41]._subindex @ 2640
	.bits	1,16			; _ODI_EEPROM_INDEXES[41]._size @ 2656
	.bits	62,16			; _ODI_EEPROM_INDEXES[41]._address @ 2672
	.bits	8203,16			; _ODI_EEPROM_INDEXES[42]._index @ 2688
	.bits	1,16			; _ODI_EEPROM_INDEXES[42]._subindex @ 2704
	.bits	1,16			; _ODI_EEPROM_INDEXES[42]._size @ 2720
	.bits	63,16			; _ODI_EEPROM_INDEXES[42]._address @ 2736
	.bits	8203,16			; _ODI_EEPROM_INDEXES[43]._index @ 2752
	.bits	2,16			; _ODI_EEPROM_INDEXES[43]._subindex @ 2768
	.bits	1,16			; _ODI_EEPROM_INDEXES[43]._size @ 2784
	.bits	64,16			; _ODI_EEPROM_INDEXES[43]._address @ 2800
	.bits	8203,16			; _ODI_EEPROM_INDEXES[44]._index @ 2816
	.bits	3,16			; _ODI_EEPROM_INDEXES[44]._subindex @ 2832
	.bits	1,16			; _ODI_EEPROM_INDEXES[44]._size @ 2848
	.bits	65,16			; _ODI_EEPROM_INDEXES[44]._address @ 2864
	.bits	8203,16			; _ODI_EEPROM_INDEXES[45]._index @ 2880
	.bits	4,16			; _ODI_EEPROM_INDEXES[45]._subindex @ 2896
	.bits	1,16			; _ODI_EEPROM_INDEXES[45]._size @ 2912
	.bits	66,16			; _ODI_EEPROM_INDEXES[45]._address @ 2928
	.bits	8203,16			; _ODI_EEPROM_INDEXES[46]._index @ 2944
	.bits	5,16			; _ODI_EEPROM_INDEXES[46]._subindex @ 2960
	.bits	1,16			; _ODI_EEPROM_INDEXES[46]._size @ 2976
	.bits	67,16			; _ODI_EEPROM_INDEXES[46]._address @ 2992
	.bits	8203,16			; _ODI_EEPROM_INDEXES[47]._index @ 3008
	.bits	6,16			; _ODI_EEPROM_INDEXES[47]._subindex @ 3024
	.bits	1,16			; _ODI_EEPROM_INDEXES[47]._size @ 3040
	.bits	68,16			; _ODI_EEPROM_INDEXES[47]._address @ 3056
	.bits	8203,16			; _ODI_EEPROM_INDEXES[48]._index @ 3072
	.bits	7,16			; _ODI_EEPROM_INDEXES[48]._subindex @ 3088
	.bits	1,16			; _ODI_EEPROM_INDEXES[48]._size @ 3104
	.bits	69,16			; _ODI_EEPROM_INDEXES[48]._address @ 3120
	.bits	8203,16			; _ODI_EEPROM_INDEXES[49]._index @ 3136
	.bits	8,16			; _ODI_EEPROM_INDEXES[49]._subindex @ 3152
	.bits	1,16			; _ODI_EEPROM_INDEXES[49]._size @ 3168
	.bits	70,16			; _ODI_EEPROM_INDEXES[49]._address @ 3184
	.bits	8204,16			; _ODI_EEPROM_INDEXES[50]._index @ 3200
	.bits	1,16			; _ODI_EEPROM_INDEXES[50]._subindex @ 3216
	.bits	1,16			; _ODI_EEPROM_INDEXES[50]._size @ 3232
	.bits	71,16			; _ODI_EEPROM_INDEXES[50]._address @ 3248
	.bits	8204,16			; _ODI_EEPROM_INDEXES[51]._index @ 3264
	.bits	2,16			; _ODI_EEPROM_INDEXES[51]._subindex @ 3280
	.bits	1,16			; _ODI_EEPROM_INDEXES[51]._size @ 3296
	.bits	72,16			; _ODI_EEPROM_INDEXES[51]._address @ 3312
	.bits	8204,16			; _ODI_EEPROM_INDEXES[52]._index @ 3328
	.bits	3,16			; _ODI_EEPROM_INDEXES[52]._subindex @ 3344
	.bits	1,16			; _ODI_EEPROM_INDEXES[52]._size @ 3360
	.bits	73,16			; _ODI_EEPROM_INDEXES[52]._address @ 3376
	.bits	8204,16			; _ODI_EEPROM_INDEXES[53]._index @ 3392
	.bits	4,16			; _ODI_EEPROM_INDEXES[53]._subindex @ 3408
	.bits	1,16			; _ODI_EEPROM_INDEXES[53]._size @ 3424
	.bits	74,16			; _ODI_EEPROM_INDEXES[53]._address @ 3440
	.bits	8204,16			; _ODI_EEPROM_INDEXES[54]._index @ 3456
	.bits	5,16			; _ODI_EEPROM_INDEXES[54]._subindex @ 3472
	.bits	1,16			; _ODI_EEPROM_INDEXES[54]._size @ 3488
	.bits	75,16			; _ODI_EEPROM_INDEXES[54]._address @ 3504
	.bits	8204,16			; _ODI_EEPROM_INDEXES[55]._index @ 3520
	.bits	6,16			; _ODI_EEPROM_INDEXES[55]._subindex @ 3536
	.bits	1,16			; _ODI_EEPROM_INDEXES[55]._size @ 3552
	.bits	76,16			; _ODI_EEPROM_INDEXES[55]._address @ 3568
	.bits	8204,16			; _ODI_EEPROM_INDEXES[56]._index @ 3584
	.bits	7,16			; _ODI_EEPROM_INDEXES[56]._subindex @ 3600
	.bits	1,16			; _ODI_EEPROM_INDEXES[56]._size @ 3616
	.bits	77,16			; _ODI_EEPROM_INDEXES[56]._address @ 3632
	.bits	8204,16			; _ODI_EEPROM_INDEXES[57]._index @ 3648
	.bits	8,16			; _ODI_EEPROM_INDEXES[57]._subindex @ 3664
	.bits	1,16			; _ODI_EEPROM_INDEXES[57]._size @ 3680
	.bits	78,16			; _ODI_EEPROM_INDEXES[57]._address @ 3696
	.bits	8205,16			; _ODI_EEPROM_INDEXES[58]._index @ 3712
	.bits	1,16			; _ODI_EEPROM_INDEXES[58]._subindex @ 3728
	.bits	1,16			; _ODI_EEPROM_INDEXES[58]._size @ 3744
	.bits	79,16			; _ODI_EEPROM_INDEXES[58]._address @ 3760
	.bits	8205,16			; _ODI_EEPROM_INDEXES[59]._index @ 3776
	.bits	2,16			; _ODI_EEPROM_INDEXES[59]._subindex @ 3792
	.bits	1,16			; _ODI_EEPROM_INDEXES[59]._size @ 3808
	.bits	80,16			; _ODI_EEPROM_INDEXES[59]._address @ 3824
	.bits	8205,16			; _ODI_EEPROM_INDEXES[60]._index @ 3840
	.bits	3,16			; _ODI_EEPROM_INDEXES[60]._subindex @ 3856
	.bits	1,16			; _ODI_EEPROM_INDEXES[60]._size @ 3872
	.bits	81,16			; _ODI_EEPROM_INDEXES[60]._address @ 3888
	.bits	8205,16			; _ODI_EEPROM_INDEXES[61]._index @ 3904
	.bits	4,16			; _ODI_EEPROM_INDEXES[61]._subindex @ 3920
	.bits	1,16			; _ODI_EEPROM_INDEXES[61]._size @ 3936
	.bits	82,16			; _ODI_EEPROM_INDEXES[61]._address @ 3952
	.bits	8205,16			; _ODI_EEPROM_INDEXES[62]._index @ 3968
	.bits	5,16			; _ODI_EEPROM_INDEXES[62]._subindex @ 3984
	.bits	1,16			; _ODI_EEPROM_INDEXES[62]._size @ 4000
	.bits	83,16			; _ODI_EEPROM_INDEXES[62]._address @ 4016
	.bits	8205,16			; _ODI_EEPROM_INDEXES[63]._index @ 4032
	.bits	6,16			; _ODI_EEPROM_INDEXES[63]._subindex @ 4048
	.bits	1,16			; _ODI_EEPROM_INDEXES[63]._size @ 4064
	.bits	84,16			; _ODI_EEPROM_INDEXES[63]._address @ 4080
	.bits	8205,16			; _ODI_EEPROM_INDEXES[64]._index @ 4096
	.bits	7,16			; _ODI_EEPROM_INDEXES[64]._subindex @ 4112
	.bits	1,16			; _ODI_EEPROM_INDEXES[64]._size @ 4128
	.bits	85,16			; _ODI_EEPROM_INDEXES[64]._address @ 4144
	.bits	8205,16			; _ODI_EEPROM_INDEXES[65]._index @ 4160
	.bits	8,16			; _ODI_EEPROM_INDEXES[65]._subindex @ 4176
	.bits	1,16			; _ODI_EEPROM_INDEXES[65]._size @ 4192
	.bits	86,16			; _ODI_EEPROM_INDEXES[65]._address @ 4208
	.bits	8209,16			; _ODI_EEPROM_INDEXES[66]._index @ 4224
	.bits	5,16			; _ODI_EEPROM_INDEXES[66]._subindex @ 4240
	.bits	2,16			; _ODI_EEPROM_INDEXES[66]._size @ 4256
	.bits	87,16			; _ODI_EEPROM_INDEXES[66]._address @ 4272
	.bits	8210,16			; _ODI_EEPROM_INDEXES[67]._index @ 4288
	.bits	1,16			; _ODI_EEPROM_INDEXES[67]._subindex @ 4304
	.bits	2,16			; _ODI_EEPROM_INDEXES[67]._size @ 4320
	.bits	89,16			; _ODI_EEPROM_INDEXES[67]._address @ 4336
	.bits	8210,16			; _ODI_EEPROM_INDEXES[68]._index @ 4352
	.bits	2,16			; _ODI_EEPROM_INDEXES[68]._subindex @ 4368
	.bits	2,16			; _ODI_EEPROM_INDEXES[68]._size @ 4384
	.bits	91,16			; _ODI_EEPROM_INDEXES[68]._address @ 4400
	.bits	8210,16			; _ODI_EEPROM_INDEXES[69]._index @ 4416
	.bits	3,16			; _ODI_EEPROM_INDEXES[69]._subindex @ 4432
	.bits	2,16			; _ODI_EEPROM_INDEXES[69]._size @ 4448
	.bits	93,16			; _ODI_EEPROM_INDEXES[69]._address @ 4464
	.bits	8210,16			; _ODI_EEPROM_INDEXES[70]._index @ 4480
	.bits	4,16			; _ODI_EEPROM_INDEXES[70]._subindex @ 4496
	.bits	1,16			; _ODI_EEPROM_INDEXES[70]._size @ 4512
	.bits	95,16			; _ODI_EEPROM_INDEXES[70]._address @ 4528
	.bits	8210,16			; _ODI_EEPROM_INDEXES[71]._index @ 4544
	.bits	5,16			; _ODI_EEPROM_INDEXES[71]._subindex @ 4560
	.bits	2,16			; _ODI_EEPROM_INDEXES[71]._size @ 4576
	.bits	96,16			; _ODI_EEPROM_INDEXES[71]._address @ 4592
	.bits	8210,16			; _ODI_EEPROM_INDEXES[72]._index @ 4608
	.bits	6,16			; _ODI_EEPROM_INDEXES[72]._subindex @ 4624
	.bits	1,16			; _ODI_EEPROM_INDEXES[72]._size @ 4640
	.bits	98,16			; _ODI_EEPROM_INDEXES[72]._address @ 4656
	.bits	8211,16			; _ODI_EEPROM_INDEXES[73]._index @ 4672
	.bits	1,16			; _ODI_EEPROM_INDEXES[73]._subindex @ 4688
	.bits	1,16			; _ODI_EEPROM_INDEXES[73]._size @ 4704
	.bits	99,16			; _ODI_EEPROM_INDEXES[73]._address @ 4720
	.bits	8211,16			; _ODI_EEPROM_INDEXES[74]._index @ 4736
	.bits	2,16			; _ODI_EEPROM_INDEXES[74]._subindex @ 4752
	.bits	1,16			; _ODI_EEPROM_INDEXES[74]._size @ 4768
	.bits	100,16			; _ODI_EEPROM_INDEXES[74]._address @ 4784
	.bits	8211,16			; _ODI_EEPROM_INDEXES[75]._index @ 4800
	.bits	3,16			; _ODI_EEPROM_INDEXES[75]._subindex @ 4816
	.bits	2,16			; _ODI_EEPROM_INDEXES[75]._size @ 4832
	.bits	101,16			; _ODI_EEPROM_INDEXES[75]._address @ 4848
	.bits	8212,16			; _ODI_EEPROM_INDEXES[76]._index @ 4864
	.bits	1,16			; _ODI_EEPROM_INDEXES[76]._subindex @ 4880
	.bits	1,16			; _ODI_EEPROM_INDEXES[76]._size @ 4896
	.bits	103,16			; _ODI_EEPROM_INDEXES[76]._address @ 4912
	.bits	8212,16			; _ODI_EEPROM_INDEXES[77]._index @ 4928
	.bits	2,16			; _ODI_EEPROM_INDEXES[77]._subindex @ 4944
	.bits	1,16			; _ODI_EEPROM_INDEXES[77]._size @ 4960
	.bits	104,16			; _ODI_EEPROM_INDEXES[77]._address @ 4976
	.bits	8212,16			; _ODI_EEPROM_INDEXES[78]._index @ 4992
	.bits	3,16			; _ODI_EEPROM_INDEXES[78]._subindex @ 5008
	.bits	1,16			; _ODI_EEPROM_INDEXES[78]._size @ 5024
	.bits	105,16			; _ODI_EEPROM_INDEXES[78]._address @ 5040
	.bits	8212,16			; _ODI_EEPROM_INDEXES[79]._index @ 5056
	.bits	4,16			; _ODI_EEPROM_INDEXES[79]._subindex @ 5072
	.bits	1,16			; _ODI_EEPROM_INDEXES[79]._size @ 5088
	.bits	106,16			; _ODI_EEPROM_INDEXES[79]._address @ 5104
	.bits	8212,16			; _ODI_EEPROM_INDEXES[80]._index @ 5120
	.bits	5,16			; _ODI_EEPROM_INDEXES[80]._subindex @ 5136
	.bits	1,16			; _ODI_EEPROM_INDEXES[80]._size @ 5152
	.bits	107,16			; _ODI_EEPROM_INDEXES[80]._address @ 5168
	.bits	8212,16			; _ODI_EEPROM_INDEXES[81]._index @ 5184
	.bits	6,16			; _ODI_EEPROM_INDEXES[81]._subindex @ 5200
	.bits	1,16			; _ODI_EEPROM_INDEXES[81]._size @ 5216
	.bits	108,16			; _ODI_EEPROM_INDEXES[81]._address @ 5232
	.bits	8212,16			; _ODI_EEPROM_INDEXES[82]._index @ 5248
	.bits	7,16			; _ODI_EEPROM_INDEXES[82]._subindex @ 5264
	.bits	1,16			; _ODI_EEPROM_INDEXES[82]._size @ 5280
	.bits	109,16			; _ODI_EEPROM_INDEXES[82]._address @ 5296
	.bits	8212,16			; _ODI_EEPROM_INDEXES[83]._index @ 5312
	.bits	8,16			; _ODI_EEPROM_INDEXES[83]._subindex @ 5328
	.bits	1,16			; _ODI_EEPROM_INDEXES[83]._size @ 5344
	.bits	110,16			; _ODI_EEPROM_INDEXES[83]._address @ 5360
	.bits	8227,16			; _ODI_EEPROM_INDEXES[84]._index @ 5376
	.bits	4,16			; _ODI_EEPROM_INDEXES[84]._subindex @ 5392
	.bits	1,16			; _ODI_EEPROM_INDEXES[84]._size @ 5408
	.bits	111,16			; _ODI_EEPROM_INDEXES[84]._address @ 5424
	.bits	8227,16			; _ODI_EEPROM_INDEXES[85]._index @ 5440
	.bits	5,16			; _ODI_EEPROM_INDEXES[85]._subindex @ 5456
	.bits	1,16			; _ODI_EEPROM_INDEXES[85]._size @ 5472
	.bits	112,16			; _ODI_EEPROM_INDEXES[85]._address @ 5488
	.bits	8256,16			; _ODI_EEPROM_INDEXES[86]._index @ 5504
	.bits	1,16			; _ODI_EEPROM_INDEXES[86]._subindex @ 5520
	.bits	4,16			; _ODI_EEPROM_INDEXES[86]._size @ 5536
	.bits	113,16			; _ODI_EEPROM_INDEXES[86]._address @ 5552
	.bits	8256,16			; _ODI_EEPROM_INDEXES[87]._index @ 5568
	.bits	2,16			; _ODI_EEPROM_INDEXES[87]._subindex @ 5584
	.bits	4,16			; _ODI_EEPROM_INDEXES[87]._size @ 5600
	.bits	117,16			; _ODI_EEPROM_INDEXES[87]._address @ 5616
	.bits	8256,16			; _ODI_EEPROM_INDEXES[88]._index @ 5632
	.bits	3,16			; _ODI_EEPROM_INDEXES[88]._subindex @ 5648
	.bits	2,16			; _ODI_EEPROM_INDEXES[88]._size @ 5664
	.bits	121,16			; _ODI_EEPROM_INDEXES[88]._address @ 5680
	.bits	8256,16			; _ODI_EEPROM_INDEXES[89]._index @ 5696
	.bits	4,16			; _ODI_EEPROM_INDEXES[89]._subindex @ 5712
	.bits	2,16			; _ODI_EEPROM_INDEXES[89]._size @ 5728
	.bits	123,16			; _ODI_EEPROM_INDEXES[89]._address @ 5744
	.bits	8256,16			; _ODI_EEPROM_INDEXES[90]._index @ 5760
	.bits	5,16			; _ODI_EEPROM_INDEXES[90]._subindex @ 5776
	.bits	2,16			; _ODI_EEPROM_INDEXES[90]._size @ 5792
	.bits	125,16			; _ODI_EEPROM_INDEXES[90]._address @ 5808
	.bits	8256,16			; _ODI_EEPROM_INDEXES[91]._index @ 5824
	.bits	7,16			; _ODI_EEPROM_INDEXES[91]._subindex @ 5840
	.bits	1,16			; _ODI_EEPROM_INDEXES[91]._size @ 5856
	.bits	127,16			; _ODI_EEPROM_INDEXES[91]._address @ 5872
	.bits	8257,16			; _ODI_EEPROM_INDEXES[92]._index @ 5888
	.bits	0,16			; _ODI_EEPROM_INDEXES[92]._subindex @ 5904
	.bits	2,16			; _ODI_EEPROM_INDEXES[92]._size @ 5920
	.bits	128,16			; _ODI_EEPROM_INDEXES[92]._address @ 5936
	.bits	8451,16			; _ODI_EEPROM_INDEXES[93]._index @ 5952
	.bits	0,16			; _ODI_EEPROM_INDEXES[93]._subindex @ 5968
	.bits	4,16			; _ODI_EEPROM_INDEXES[93]._size @ 5984
	.bits	130,16			; _ODI_EEPROM_INDEXES[93]._address @ 6000
	.bits	8453,16			; _ODI_EEPROM_INDEXES[94]._index @ 6016
	.bits	1,16			; _ODI_EEPROM_INDEXES[94]._subindex @ 6032
	.bits	4,16			; _ODI_EEPROM_INDEXES[94]._size @ 6048
	.bits	134,16			; _ODI_EEPROM_INDEXES[94]._address @ 6064
	.bits	8453,16			; _ODI_EEPROM_INDEXES[95]._index @ 6080
	.bits	2,16			; _ODI_EEPROM_INDEXES[95]._subindex @ 6096
	.bits	2,16			; _ODI_EEPROM_INDEXES[95]._size @ 6112
	.bits	138,16			; _ODI_EEPROM_INDEXES[95]._address @ 6128
	.bits	8453,16			; _ODI_EEPROM_INDEXES[96]._index @ 6144
	.bits	3,16			; _ODI_EEPROM_INDEXES[96]._subindex @ 6160
	.bits	2,16			; _ODI_EEPROM_INDEXES[96]._size @ 6176
	.bits	140,16			; _ODI_EEPROM_INDEXES[96]._address @ 6192
	.bits	8453,16			; _ODI_EEPROM_INDEXES[97]._index @ 6208
	.bits	4,16			; _ODI_EEPROM_INDEXES[97]._subindex @ 6224
	.bits	2,16			; _ODI_EEPROM_INDEXES[97]._size @ 6240
	.bits	142,16			; _ODI_EEPROM_INDEXES[97]._address @ 6256
	.bits	25647,16			; _ODI_EEPROM_INDEXES[98]._index @ 6272
	.bits	1,16			; _ODI_EEPROM_INDEXES[98]._subindex @ 6288
	.bits	4,16			; _ODI_EEPROM_INDEXES[98]._size @ 6304
	.bits	144,16			; _ODI_EEPROM_INDEXES[98]._address @ 6320
	.bits	25647,16			; _ODI_EEPROM_INDEXES[99]._index @ 6336
	.bits	2,16			; _ODI_EEPROM_INDEXES[99]._subindex @ 6352
	.bits	4,16			; _ODI_EEPROM_INDEXES[99]._size @ 6368
	.bits	148,16			; _ODI_EEPROM_INDEXES[99]._address @ 6384
	.bits	25647,16			; _ODI_EEPROM_INDEXES[100]._index @ 6400
	.bits	3,16			; _ODI_EEPROM_INDEXES[100]._subindex @ 6416
	.bits	4,16			; _ODI_EEPROM_INDEXES[100]._size @ 6432
	.bits	152,16			; _ODI_EEPROM_INDEXES[100]._address @ 6448
	.bits	25647,16			; _ODI_EEPROM_INDEXES[101]._index @ 6464
	.bits	4,16			; _ODI_EEPROM_INDEXES[101]._subindex @ 6480
	.bits	4,16			; _ODI_EEPROM_INDEXES[101]._size @ 6496
	.bits	156,16			; _ODI_EEPROM_INDEXES[101]._address @ 6512
	.bits	25647,16			; _ODI_EEPROM_INDEXES[102]._index @ 6528
	.bits	5,16			; _ODI_EEPROM_INDEXES[102]._subindex @ 6544
	.bits	4,16			; _ODI_EEPROM_INDEXES[102]._size @ 6560
	.bits	160,16			; _ODI_EEPROM_INDEXES[102]._address @ 6576
	.bits	25647,16			; _ODI_EEPROM_INDEXES[103]._index @ 6592
	.bits	6,16			; _ODI_EEPROM_INDEXES[103]._subindex @ 6608
	.bits	4,16			; _ODI_EEPROM_INDEXES[103]._size @ 6624
	.bits	164,16			; _ODI_EEPROM_INDEXES[103]._address @ 6640
	.bits	25647,16			; _ODI_EEPROM_INDEXES[104]._index @ 6656
	.bits	7,16			; _ODI_EEPROM_INDEXES[104]._subindex @ 6672
	.bits	4,16			; _ODI_EEPROM_INDEXES[104]._size @ 6688
	.bits	168,16			; _ODI_EEPROM_INDEXES[104]._address @ 6704
	.bits	25647,16			; _ODI_EEPROM_INDEXES[105]._index @ 6720
	.bits	8,16			; _ODI_EEPROM_INDEXES[105]._subindex @ 6736
	.bits	4,16			; _ODI_EEPROM_INDEXES[105]._size @ 6752
	.bits	172,16			; _ODI_EEPROM_INDEXES[105]._address @ 6768
	.bits	25647,16			; _ODI_EEPROM_INDEXES[106]._index @ 6784
	.bits	9,16			; _ODI_EEPROM_INDEXES[106]._subindex @ 6800
	.bits	4,16			; _ODI_EEPROM_INDEXES[106]._size @ 6816
	.bits	176,16			; _ODI_EEPROM_INDEXES[106]._address @ 6832
	.bits	25647,16			; _ODI_EEPROM_INDEXES[107]._index @ 6848
	.bits	10,16			; _ODI_EEPROM_INDEXES[107]._subindex @ 6864
	.bits	4,16			; _ODI_EEPROM_INDEXES[107]._size @ 6880
	.bits	180,16			; _ODI_EEPROM_INDEXES[107]._address @ 6896
	.bits	25647,16			; _ODI_EEPROM_INDEXES[108]._index @ 6912
	.bits	11,16			; _ODI_EEPROM_INDEXES[108]._subindex @ 6928
	.bits	4,16			; _ODI_EEPROM_INDEXES[108]._size @ 6944
	.bits	184,16			; _ODI_EEPROM_INDEXES[108]._address @ 6960
	.bits	25647,16			; _ODI_EEPROM_INDEXES[109]._index @ 6976
	.bits	12,16			; _ODI_EEPROM_INDEXES[109]._subindex @ 6992
	.bits	4,16			; _ODI_EEPROM_INDEXES[109]._size @ 7008
	.bits	188,16			; _ODI_EEPROM_INDEXES[109]._address @ 7024
	.bits	25647,16			; _ODI_EEPROM_INDEXES[110]._index @ 7040
	.bits	13,16			; _ODI_EEPROM_INDEXES[110]._subindex @ 7056
	.bits	4,16			; _ODI_EEPROM_INDEXES[110]._size @ 7072
	.bits	192,16			; _ODI_EEPROM_INDEXES[110]._address @ 7088
	.bits	25647,16			; _ODI_EEPROM_INDEXES[111]._index @ 7104
	.bits	14,16			; _ODI_EEPROM_INDEXES[111]._subindex @ 7120
	.bits	4,16			; _ODI_EEPROM_INDEXES[111]._size @ 7136
	.bits	196,16			; _ODI_EEPROM_INDEXES[111]._address @ 7152
	.bits	25647,16			; _ODI_EEPROM_INDEXES[112]._index @ 7168
	.bits	15,16			; _ODI_EEPROM_INDEXES[112]._subindex @ 7184
	.bits	4,16			; _ODI_EEPROM_INDEXES[112]._size @ 7200
	.bits	200,16			; _ODI_EEPROM_INDEXES[112]._address @ 7216
	.bits	25647,16			; _ODI_EEPROM_INDEXES[113]._index @ 7232
	.bits	16,16			; _ODI_EEPROM_INDEXES[113]._subindex @ 7248
	.bits	4,16			; _ODI_EEPROM_INDEXES[113]._size @ 7264
	.bits	204,16			; _ODI_EEPROM_INDEXES[113]._address @ 7280
	.bits	25647,16			; _ODI_EEPROM_INDEXES[114]._index @ 7296
	.bits	17,16			; _ODI_EEPROM_INDEXES[114]._subindex @ 7312
	.bits	4,16			; _ODI_EEPROM_INDEXES[114]._size @ 7328
	.bits	208,16			; _ODI_EEPROM_INDEXES[114]._address @ 7344
	.bits	25647,16			; _ODI_EEPROM_INDEXES[115]._index @ 7360
	.bits	18,16			; _ODI_EEPROM_INDEXES[115]._subindex @ 7376
	.bits	4,16			; _ODI_EEPROM_INDEXES[115]._size @ 7392
	.bits	212,16			; _ODI_EEPROM_INDEXES[115]._address @ 7408
	.bits	25647,16			; _ODI_EEPROM_INDEXES[116]._index @ 7424
	.bits	19,16			; _ODI_EEPROM_INDEXES[116]._subindex @ 7440
	.bits	4,16			; _ODI_EEPROM_INDEXES[116]._size @ 7456
	.bits	216,16			; _ODI_EEPROM_INDEXES[116]._address @ 7472
	.bits	25647,16			; _ODI_EEPROM_INDEXES[117]._index @ 7488
	.bits	20,16			; _ODI_EEPROM_INDEXES[117]._subindex @ 7504
	.bits	4,16			; _ODI_EEPROM_INDEXES[117]._size @ 7520
	.bits	220,16			; _ODI_EEPROM_INDEXES[117]._address @ 7536
	.bits	25647,16			; _ODI_EEPROM_INDEXES[118]._index @ 7552
	.bits	21,16			; _ODI_EEPROM_INDEXES[118]._subindex @ 7568
	.bits	4,16			; _ODI_EEPROM_INDEXES[118]._size @ 7584
	.bits	224,16			; _ODI_EEPROM_INDEXES[118]._address @ 7600
	.bits	25647,16			; _ODI_EEPROM_INDEXES[119]._index @ 7616
	.bits	22,16			; _ODI_EEPROM_INDEXES[119]._subindex @ 7632
	.bits	4,16			; _ODI_EEPROM_INDEXES[119]._size @ 7648
	.bits	228,16			; _ODI_EEPROM_INDEXES[119]._address @ 7664
	.bits	25647,16			; _ODI_EEPROM_INDEXES[120]._index @ 7680
	.bits	23,16			; _ODI_EEPROM_INDEXES[120]._subindex @ 7696
	.bits	4,16			; _ODI_EEPROM_INDEXES[120]._size @ 7712
	.bits	232,16			; _ODI_EEPROM_INDEXES[120]._address @ 7728
	.bits	25648,16			; _ODI_EEPROM_INDEXES[121]._index @ 7744
	.bits	1,16			; _ODI_EEPROM_INDEXES[121]._subindex @ 7760
	.bits	4,16			; _ODI_EEPROM_INDEXES[121]._size @ 7776
	.bits	236,16			; _ODI_EEPROM_INDEXES[121]._address @ 7792
	.bits	25648,16			; _ODI_EEPROM_INDEXES[122]._index @ 7808
	.bits	2,16			; _ODI_EEPROM_INDEXES[122]._subindex @ 7824
	.bits	4,16			; _ODI_EEPROM_INDEXES[122]._size @ 7840
	.bits	240,16			; _ODI_EEPROM_INDEXES[122]._address @ 7856
	.bits	25648,16			; _ODI_EEPROM_INDEXES[123]._index @ 7872
	.bits	3,16			; _ODI_EEPROM_INDEXES[123]._subindex @ 7888
	.bits	4,16			; _ODI_EEPROM_INDEXES[123]._size @ 7904
	.bits	244,16			; _ODI_EEPROM_INDEXES[123]._address @ 7920
	.bits	25648,16			; _ODI_EEPROM_INDEXES[124]._index @ 7936
	.bits	4,16			; _ODI_EEPROM_INDEXES[124]._subindex @ 7952
	.bits	4,16			; _ODI_EEPROM_INDEXES[124]._size @ 7968
	.bits	248,16			; _ODI_EEPROM_INDEXES[124]._address @ 7984
	.bits	25648,16			; _ODI_EEPROM_INDEXES[125]._index @ 8000
	.bits	5,16			; _ODI_EEPROM_INDEXES[125]._subindex @ 8016
	.bits	4,16			; _ODI_EEPROM_INDEXES[125]._size @ 8032
	.bits	252,16			; _ODI_EEPROM_INDEXES[125]._address @ 8048
	.bits	25648,16			; _ODI_EEPROM_INDEXES[126]._index @ 8064
	.bits	6,16			; _ODI_EEPROM_INDEXES[126]._subindex @ 8080
	.bits	4,16			; _ODI_EEPROM_INDEXES[126]._size @ 8096
	.bits	256,16			; _ODI_EEPROM_INDEXES[126]._address @ 8112
	.bits	25648,16			; _ODI_EEPROM_INDEXES[127]._index @ 8128
	.bits	7,16			; _ODI_EEPROM_INDEXES[127]._subindex @ 8144
	.bits	4,16			; _ODI_EEPROM_INDEXES[127]._size @ 8160
	.bits	260,16			; _ODI_EEPROM_INDEXES[127]._address @ 8176
	.bits	25648,16			; _ODI_EEPROM_INDEXES[128]._index @ 8192
	.bits	8,16			; _ODI_EEPROM_INDEXES[128]._subindex @ 8208
	.bits	4,16			; _ODI_EEPROM_INDEXES[128]._size @ 8224
	.bits	264,16			; _ODI_EEPROM_INDEXES[128]._address @ 8240
	.bits	25648,16			; _ODI_EEPROM_INDEXES[129]._index @ 8256
	.bits	9,16			; _ODI_EEPROM_INDEXES[129]._subindex @ 8272
	.bits	4,16			; _ODI_EEPROM_INDEXES[129]._size @ 8288
	.bits	268,16			; _ODI_EEPROM_INDEXES[129]._address @ 8304
	.bits	25648,16			; _ODI_EEPROM_INDEXES[130]._index @ 8320
	.bits	10,16			; _ODI_EEPROM_INDEXES[130]._subindex @ 8336
	.bits	4,16			; _ODI_EEPROM_INDEXES[130]._size @ 8352
	.bits	272,16			; _ODI_EEPROM_INDEXES[130]._address @ 8368
	.bits	25648,16			; _ODI_EEPROM_INDEXES[131]._index @ 8384
	.bits	11,16			; _ODI_EEPROM_INDEXES[131]._subindex @ 8400
	.bits	4,16			; _ODI_EEPROM_INDEXES[131]._size @ 8416
	.bits	276,16			; _ODI_EEPROM_INDEXES[131]._address @ 8432
	.bits	25648,16			; _ODI_EEPROM_INDEXES[132]._index @ 8448
	.bits	12,16			; _ODI_EEPROM_INDEXES[132]._subindex @ 8464
	.bits	4,16			; _ODI_EEPROM_INDEXES[132]._size @ 8480
	.bits	280,16			; _ODI_EEPROM_INDEXES[132]._address @ 8496
	.bits	25648,16			; _ODI_EEPROM_INDEXES[133]._index @ 8512
	.bits	13,16			; _ODI_EEPROM_INDEXES[133]._subindex @ 8528
	.bits	4,16			; _ODI_EEPROM_INDEXES[133]._size @ 8544
	.bits	284,16			; _ODI_EEPROM_INDEXES[133]._address @ 8560
	.bits	25648,16			; _ODI_EEPROM_INDEXES[134]._index @ 8576
	.bits	14,16			; _ODI_EEPROM_INDEXES[134]._subindex @ 8592
	.bits	4,16			; _ODI_EEPROM_INDEXES[134]._size @ 8608
	.bits	288,16			; _ODI_EEPROM_INDEXES[134]._address @ 8624
	.bits	25648,16			; _ODI_EEPROM_INDEXES[135]._index @ 8640
	.bits	15,16			; _ODI_EEPROM_INDEXES[135]._subindex @ 8656
	.bits	4,16			; _ODI_EEPROM_INDEXES[135]._size @ 8672
	.bits	292,16			; _ODI_EEPROM_INDEXES[135]._address @ 8688
	.bits	25648,16			; _ODI_EEPROM_INDEXES[136]._index @ 8704
	.bits	16,16			; _ODI_EEPROM_INDEXES[136]._subindex @ 8720
	.bits	4,16			; _ODI_EEPROM_INDEXES[136]._size @ 8736
	.bits	296,16			; _ODI_EEPROM_INDEXES[136]._address @ 8752
	.bits	25648,16			; _ODI_EEPROM_INDEXES[137]._index @ 8768
	.bits	17,16			; _ODI_EEPROM_INDEXES[137]._subindex @ 8784
	.bits	4,16			; _ODI_EEPROM_INDEXES[137]._size @ 8800
	.bits	300,16			; _ODI_EEPROM_INDEXES[137]._address @ 8816
	.bits	25648,16			; _ODI_EEPROM_INDEXES[138]._index @ 8832
	.bits	18,16			; _ODI_EEPROM_INDEXES[138]._subindex @ 8848
	.bits	4,16			; _ODI_EEPROM_INDEXES[138]._size @ 8864
	.bits	304,16			; _ODI_EEPROM_INDEXES[138]._address @ 8880
	.bits	25648,16			; _ODI_EEPROM_INDEXES[139]._index @ 8896
	.bits	19,16			; _ODI_EEPROM_INDEXES[139]._subindex @ 8912
	.bits	4,16			; _ODI_EEPROM_INDEXES[139]._size @ 8928
	.bits	308,16			; _ODI_EEPROM_INDEXES[139]._address @ 8944
	.bits	25648,16			; _ODI_EEPROM_INDEXES[140]._index @ 8960
	.bits	20,16			; _ODI_EEPROM_INDEXES[140]._subindex @ 8976
	.bits	4,16			; _ODI_EEPROM_INDEXES[140]._size @ 8992
	.bits	312,16			; _ODI_EEPROM_INDEXES[140]._address @ 9008
	.bits	25648,16			; _ODI_EEPROM_INDEXES[141]._index @ 9024
	.bits	21,16			; _ODI_EEPROM_INDEXES[141]._subindex @ 9040
	.bits	4,16			; _ODI_EEPROM_INDEXES[141]._size @ 9056
	.bits	316,16			; _ODI_EEPROM_INDEXES[141]._address @ 9072
	.bits	25648,16			; _ODI_EEPROM_INDEXES[142]._index @ 9088
	.bits	22,16			; _ODI_EEPROM_INDEXES[142]._subindex @ 9104
	.bits	4,16			; _ODI_EEPROM_INDEXES[142]._size @ 9120
	.bits	320,16			; _ODI_EEPROM_INDEXES[142]._address @ 9136
	.bits	25648,16			; _ODI_EEPROM_INDEXES[143]._index @ 9152
	.bits	23,16			; _ODI_EEPROM_INDEXES[143]._subindex @ 9168
	.bits	4,16			; _ODI_EEPROM_INDEXES[143]._size @ 9184
	.bits	324,16			; _ODI_EEPROM_INDEXES[143]._address @ 9200
	.bits	25649,16			; _ODI_EEPROM_INDEXES[144]._index @ 9216
	.bits	1,16			; _ODI_EEPROM_INDEXES[144]._subindex @ 9232
	.bits	4,16			; _ODI_EEPROM_INDEXES[144]._size @ 9248
	.bits	328,16			; _ODI_EEPROM_INDEXES[144]._address @ 9264
	.bits	25649,16			; _ODI_EEPROM_INDEXES[145]._index @ 9280
	.bits	2,16			; _ODI_EEPROM_INDEXES[145]._subindex @ 9296
	.bits	4,16			; _ODI_EEPROM_INDEXES[145]._size @ 9312
	.bits	332,16			; _ODI_EEPROM_INDEXES[145]._address @ 9328
	.bits	25649,16			; _ODI_EEPROM_INDEXES[146]._index @ 9344
	.bits	3,16			; _ODI_EEPROM_INDEXES[146]._subindex @ 9360
	.bits	4,16			; _ODI_EEPROM_INDEXES[146]._size @ 9376
	.bits	336,16			; _ODI_EEPROM_INDEXES[146]._address @ 9392
	.bits	25649,16			; _ODI_EEPROM_INDEXES[147]._index @ 9408
	.bits	4,16			; _ODI_EEPROM_INDEXES[147]._subindex @ 9424
	.bits	4,16			; _ODI_EEPROM_INDEXES[147]._size @ 9440
	.bits	340,16			; _ODI_EEPROM_INDEXES[147]._address @ 9456
	.bits	25649,16			; _ODI_EEPROM_INDEXES[148]._index @ 9472
	.bits	5,16			; _ODI_EEPROM_INDEXES[148]._subindex @ 9488
	.bits	4,16			; _ODI_EEPROM_INDEXES[148]._size @ 9504
	.bits	344,16			; _ODI_EEPROM_INDEXES[148]._address @ 9520
	.bits	25649,16			; _ODI_EEPROM_INDEXES[149]._index @ 9536
	.bits	6,16			; _ODI_EEPROM_INDEXES[149]._subindex @ 9552
	.bits	4,16			; _ODI_EEPROM_INDEXES[149]._size @ 9568
	.bits	348,16			; _ODI_EEPROM_INDEXES[149]._address @ 9584
	.bits	25649,16			; _ODI_EEPROM_INDEXES[150]._index @ 9600
	.bits	7,16			; _ODI_EEPROM_INDEXES[150]._subindex @ 9616
	.bits	4,16			; _ODI_EEPROM_INDEXES[150]._size @ 9632
	.bits	352,16			; _ODI_EEPROM_INDEXES[150]._address @ 9648
	.bits	25649,16			; _ODI_EEPROM_INDEXES[151]._index @ 9664
	.bits	8,16			; _ODI_EEPROM_INDEXES[151]._subindex @ 9680
	.bits	4,16			; _ODI_EEPROM_INDEXES[151]._size @ 9696
	.bits	356,16			; _ODI_EEPROM_INDEXES[151]._address @ 9712
	.bits	25649,16			; _ODI_EEPROM_INDEXES[152]._index @ 9728
	.bits	9,16			; _ODI_EEPROM_INDEXES[152]._subindex @ 9744
	.bits	4,16			; _ODI_EEPROM_INDEXES[152]._size @ 9760
	.bits	360,16			; _ODI_EEPROM_INDEXES[152]._address @ 9776
	.bits	25649,16			; _ODI_EEPROM_INDEXES[153]._index @ 9792
	.bits	10,16			; _ODI_EEPROM_INDEXES[153]._subindex @ 9808
	.bits	4,16			; _ODI_EEPROM_INDEXES[153]._size @ 9824
	.bits	364,16			; _ODI_EEPROM_INDEXES[153]._address @ 9840
	.bits	25649,16			; _ODI_EEPROM_INDEXES[154]._index @ 9856
	.bits	11,16			; _ODI_EEPROM_INDEXES[154]._subindex @ 9872
	.bits	4,16			; _ODI_EEPROM_INDEXES[154]._size @ 9888
	.bits	368,16			; _ODI_EEPROM_INDEXES[154]._address @ 9904
	.bits	25649,16			; _ODI_EEPROM_INDEXES[155]._index @ 9920
	.bits	12,16			; _ODI_EEPROM_INDEXES[155]._subindex @ 9936
	.bits	4,16			; _ODI_EEPROM_INDEXES[155]._size @ 9952
	.bits	372,16			; _ODI_EEPROM_INDEXES[155]._address @ 9968
	.bits	25649,16			; _ODI_EEPROM_INDEXES[156]._index @ 9984
	.bits	13,16			; _ODI_EEPROM_INDEXES[156]._subindex @ 10000
	.bits	4,16			; _ODI_EEPROM_INDEXES[156]._size @ 10016
	.bits	376,16			; _ODI_EEPROM_INDEXES[156]._address @ 10032
	.bits	25649,16			; _ODI_EEPROM_INDEXES[157]._index @ 10048
	.bits	14,16			; _ODI_EEPROM_INDEXES[157]._subindex @ 10064
	.bits	4,16			; _ODI_EEPROM_INDEXES[157]._size @ 10080
	.bits	380,16			; _ODI_EEPROM_INDEXES[157]._address @ 10096
	.bits	25649,16			; _ODI_EEPROM_INDEXES[158]._index @ 10112
	.bits	15,16			; _ODI_EEPROM_INDEXES[158]._subindex @ 10128
	.bits	4,16			; _ODI_EEPROM_INDEXES[158]._size @ 10144
	.bits	384,16			; _ODI_EEPROM_INDEXES[158]._address @ 10160
	.bits	25649,16			; _ODI_EEPROM_INDEXES[159]._index @ 10176
	.bits	16,16			; _ODI_EEPROM_INDEXES[159]._subindex @ 10192
	.bits	4,16			; _ODI_EEPROM_INDEXES[159]._size @ 10208
	.bits	388,16			; _ODI_EEPROM_INDEXES[159]._address @ 10224
	.bits	25649,16			; _ODI_EEPROM_INDEXES[160]._index @ 10240
	.bits	17,16			; _ODI_EEPROM_INDEXES[160]._subindex @ 10256
	.bits	4,16			; _ODI_EEPROM_INDEXES[160]._size @ 10272
	.bits	392,16			; _ODI_EEPROM_INDEXES[160]._address @ 10288
	.bits	25649,16			; _ODI_EEPROM_INDEXES[161]._index @ 10304
	.bits	18,16			; _ODI_EEPROM_INDEXES[161]._subindex @ 10320
	.bits	4,16			; _ODI_EEPROM_INDEXES[161]._size @ 10336
	.bits	396,16			; _ODI_EEPROM_INDEXES[161]._address @ 10352
	.bits	25649,16			; _ODI_EEPROM_INDEXES[162]._index @ 10368
	.bits	19,16			; _ODI_EEPROM_INDEXES[162]._subindex @ 10384
	.bits	4,16			; _ODI_EEPROM_INDEXES[162]._size @ 10400
	.bits	400,16			; _ODI_EEPROM_INDEXES[162]._address @ 10416
	.bits	25649,16			; _ODI_EEPROM_INDEXES[163]._index @ 10432
	.bits	20,16			; _ODI_EEPROM_INDEXES[163]._subindex @ 10448
	.bits	4,16			; _ODI_EEPROM_INDEXES[163]._size @ 10464
	.bits	404,16			; _ODI_EEPROM_INDEXES[163]._address @ 10480
	.bits	25649,16			; _ODI_EEPROM_INDEXES[164]._index @ 10496
	.bits	21,16			; _ODI_EEPROM_INDEXES[164]._subindex @ 10512
	.bits	4,16			; _ODI_EEPROM_INDEXES[164]._size @ 10528
	.bits	408,16			; _ODI_EEPROM_INDEXES[164]._address @ 10544
	.bits	25649,16			; _ODI_EEPROM_INDEXES[165]._index @ 10560
	.bits	22,16			; _ODI_EEPROM_INDEXES[165]._subindex @ 10576
	.bits	4,16			; _ODI_EEPROM_INDEXES[165]._size @ 10592
	.bits	412,16			; _ODI_EEPROM_INDEXES[165]._address @ 10608
	.bits	25649,16			; _ODI_EEPROM_INDEXES[166]._index @ 10624
	.bits	23,16			; _ODI_EEPROM_INDEXES[166]._subindex @ 10640
	.bits	4,16			; _ODI_EEPROM_INDEXES[166]._size @ 10656
	.bits	416,16			; _ODI_EEPROM_INDEXES[166]._address @ 10672
	.bits	25666,16			; _ODI_EEPROM_INDEXES[167]._index @ 10688
	.bits	1,16			; _ODI_EEPROM_INDEXES[167]._subindex @ 10704
	.bits	4,16			; _ODI_EEPROM_INDEXES[167]._size @ 10720
	.bits	420,16			; _ODI_EEPROM_INDEXES[167]._address @ 10736
	.bits	25666,16			; _ODI_EEPROM_INDEXES[168]._index @ 10752
	.bits	2,16			; _ODI_EEPROM_INDEXES[168]._subindex @ 10768
	.bits	4,16			; _ODI_EEPROM_INDEXES[168]._size @ 10784
	.bits	424,16			; _ODI_EEPROM_INDEXES[168]._address @ 10800
	.bits	25666,16			; _ODI_EEPROM_INDEXES[169]._index @ 10816
	.bits	3,16			; _ODI_EEPROM_INDEXES[169]._subindex @ 10832
	.bits	4,16			; _ODI_EEPROM_INDEXES[169]._size @ 10848
	.bits	428,16			; _ODI_EEPROM_INDEXES[169]._address @ 10864
	.bits	25666,16			; _ODI_EEPROM_INDEXES[170]._index @ 10880
	.bits	4,16			; _ODI_EEPROM_INDEXES[170]._subindex @ 10896
	.bits	4,16			; _ODI_EEPROM_INDEXES[170]._size @ 10912
	.bits	432,16			; _ODI_EEPROM_INDEXES[170]._address @ 10928
	.bits	25666,16			; _ODI_EEPROM_INDEXES[171]._index @ 10944
	.bits	5,16			; _ODI_EEPROM_INDEXES[171]._subindex @ 10960
	.bits	4,16			; _ODI_EEPROM_INDEXES[171]._size @ 10976
	.bits	436,16			; _ODI_EEPROM_INDEXES[171]._address @ 10992
	.bits	25666,16			; _ODI_EEPROM_INDEXES[172]._index @ 11008
	.bits	6,16			; _ODI_EEPROM_INDEXES[172]._subindex @ 11024
	.bits	4,16			; _ODI_EEPROM_INDEXES[172]._size @ 11040
	.bits	440,16			; _ODI_EEPROM_INDEXES[172]._address @ 11056
	.bits	25666,16			; _ODI_EEPROM_INDEXES[173]._index @ 11072
	.bits	7,16			; _ODI_EEPROM_INDEXES[173]._subindex @ 11088
	.bits	4,16			; _ODI_EEPROM_INDEXES[173]._size @ 11104
	.bits	444,16			; _ODI_EEPROM_INDEXES[173]._address @ 11120
	.bits	25666,16			; _ODI_EEPROM_INDEXES[174]._index @ 11136
	.bits	8,16			; _ODI_EEPROM_INDEXES[174]._subindex @ 11152
	.bits	4,16			; _ODI_EEPROM_INDEXES[174]._size @ 11168
	.bits	448,16			; _ODI_EEPROM_INDEXES[174]._address @ 11184
	.bits	25666,16			; _ODI_EEPROM_INDEXES[175]._index @ 11200
	.bits	9,16			; _ODI_EEPROM_INDEXES[175]._subindex @ 11216
	.bits	4,16			; _ODI_EEPROM_INDEXES[175]._size @ 11232
	.bits	452,16			; _ODI_EEPROM_INDEXES[175]._address @ 11248
	.bits	25666,16			; _ODI_EEPROM_INDEXES[176]._index @ 11264
	.bits	10,16			; _ODI_EEPROM_INDEXES[176]._subindex @ 11280
	.bits	4,16			; _ODI_EEPROM_INDEXES[176]._size @ 11296
	.bits	456,16			; _ODI_EEPROM_INDEXES[176]._address @ 11312
	.bits	25666,16			; _ODI_EEPROM_INDEXES[177]._index @ 11328
	.bits	11,16			; _ODI_EEPROM_INDEXES[177]._subindex @ 11344
	.bits	4,16			; _ODI_EEPROM_INDEXES[177]._size @ 11360
	.bits	460,16			; _ODI_EEPROM_INDEXES[177]._address @ 11376
	.bits	25666,16			; _ODI_EEPROM_INDEXES[178]._index @ 11392
	.bits	12,16			; _ODI_EEPROM_INDEXES[178]._subindex @ 11408
	.bits	4,16			; _ODI_EEPROM_INDEXES[178]._size @ 11424
	.bits	464,16			; _ODI_EEPROM_INDEXES[178]._address @ 11440
	.bits	25666,16			; _ODI_EEPROM_INDEXES[179]._index @ 11456
	.bits	13,16			; _ODI_EEPROM_INDEXES[179]._subindex @ 11472
	.bits	4,16			; _ODI_EEPROM_INDEXES[179]._size @ 11488
	.bits	468,16			; _ODI_EEPROM_INDEXES[179]._address @ 11504
	.bits	25666,16			; _ODI_EEPROM_INDEXES[180]._index @ 11520
	.bits	14,16			; _ODI_EEPROM_INDEXES[180]._subindex @ 11536
	.bits	4,16			; _ODI_EEPROM_INDEXES[180]._size @ 11552
	.bits	472,16			; _ODI_EEPROM_INDEXES[180]._address @ 11568
	.bits	25666,16			; _ODI_EEPROM_INDEXES[181]._index @ 11584
	.bits	15,16			; _ODI_EEPROM_INDEXES[181]._subindex @ 11600
	.bits	4,16			; _ODI_EEPROM_INDEXES[181]._size @ 11616
	.bits	476,16			; _ODI_EEPROM_INDEXES[181]._address @ 11632
	.bits	25666,16			; _ODI_EEPROM_INDEXES[182]._index @ 11648
	.bits	16,16			; _ODI_EEPROM_INDEXES[182]._subindex @ 11664
	.bits	4,16			; _ODI_EEPROM_INDEXES[182]._size @ 11680
	.bits	480,16			; _ODI_EEPROM_INDEXES[182]._address @ 11696
	.bits	25670,16			; _ODI_EEPROM_INDEXES[183]._index @ 11712
	.bits	1,16			; _ODI_EEPROM_INDEXES[183]._subindex @ 11728
	.bits	4,16			; _ODI_EEPROM_INDEXES[183]._size @ 11744
	.bits	484,16			; _ODI_EEPROM_INDEXES[183]._address @ 11760
	.bits	25670,16			; _ODI_EEPROM_INDEXES[184]._index @ 11776
	.bits	2,16			; _ODI_EEPROM_INDEXES[184]._subindex @ 11792
	.bits	4,16			; _ODI_EEPROM_INDEXES[184]._size @ 11808
	.bits	488,16			; _ODI_EEPROM_INDEXES[184]._address @ 11824
	.bits	25670,16			; _ODI_EEPROM_INDEXES[185]._index @ 11840
	.bits	3,16			; _ODI_EEPROM_INDEXES[185]._subindex @ 11856
	.bits	4,16			; _ODI_EEPROM_INDEXES[185]._size @ 11872
	.bits	492,16			; _ODI_EEPROM_INDEXES[185]._address @ 11888
	.bits	25670,16			; _ODI_EEPROM_INDEXES[186]._index @ 11904
	.bits	4,16			; _ODI_EEPROM_INDEXES[186]._subindex @ 11920
	.bits	4,16			; _ODI_EEPROM_INDEXES[186]._size @ 11936
	.bits	496,16			; _ODI_EEPROM_INDEXES[186]._address @ 11952
	.bits	25670,16			; _ODI_EEPROM_INDEXES[187]._index @ 11968
	.bits	5,16			; _ODI_EEPROM_INDEXES[187]._subindex @ 11984
	.bits	4,16			; _ODI_EEPROM_INDEXES[187]._size @ 12000
	.bits	500,16			; _ODI_EEPROM_INDEXES[187]._address @ 12016
	.bits	25670,16			; _ODI_EEPROM_INDEXES[188]._index @ 12032
	.bits	6,16			; _ODI_EEPROM_INDEXES[188]._subindex @ 12048
	.bits	4,16			; _ODI_EEPROM_INDEXES[188]._size @ 12064
	.bits	504,16			; _ODI_EEPROM_INDEXES[188]._address @ 12080
	.bits	25670,16			; _ODI_EEPROM_INDEXES[189]._index @ 12096
	.bits	7,16			; _ODI_EEPROM_INDEXES[189]._subindex @ 12112
	.bits	4,16			; _ODI_EEPROM_INDEXES[189]._size @ 12128
	.bits	508,16			; _ODI_EEPROM_INDEXES[189]._address @ 12144
	.bits	25670,16			; _ODI_EEPROM_INDEXES[190]._index @ 12160
	.bits	8,16			; _ODI_EEPROM_INDEXES[190]._subindex @ 12176
	.bits	4,16			; _ODI_EEPROM_INDEXES[190]._size @ 12192
	.bits	512,16			; _ODI_EEPROM_INDEXES[190]._address @ 12208
	.bits	25670,16			; _ODI_EEPROM_INDEXES[191]._index @ 12224
	.bits	9,16			; _ODI_EEPROM_INDEXES[191]._subindex @ 12240
	.bits	4,16			; _ODI_EEPROM_INDEXES[191]._size @ 12256
	.bits	516,16			; _ODI_EEPROM_INDEXES[191]._address @ 12272
	.bits	25670,16			; _ODI_EEPROM_INDEXES[192]._index @ 12288
	.bits	10,16			; _ODI_EEPROM_INDEXES[192]._subindex @ 12304
	.bits	4,16			; _ODI_EEPROM_INDEXES[192]._size @ 12320
	.bits	520,16			; _ODI_EEPROM_INDEXES[192]._address @ 12336
	.bits	25670,16			; _ODI_EEPROM_INDEXES[193]._index @ 12352
	.bits	11,16			; _ODI_EEPROM_INDEXES[193]._subindex @ 12368
	.bits	4,16			; _ODI_EEPROM_INDEXES[193]._size @ 12384
	.bits	524,16			; _ODI_EEPROM_INDEXES[193]._address @ 12400
	.bits	25670,16			; _ODI_EEPROM_INDEXES[194]._index @ 12416
	.bits	12,16			; _ODI_EEPROM_INDEXES[194]._subindex @ 12432
	.bits	4,16			; _ODI_EEPROM_INDEXES[194]._size @ 12448
	.bits	528,16			; _ODI_EEPROM_INDEXES[194]._address @ 12464
	.bits	25670,16			; _ODI_EEPROM_INDEXES[195]._index @ 12480
	.bits	13,16			; _ODI_EEPROM_INDEXES[195]._subindex @ 12496
	.bits	4,16			; _ODI_EEPROM_INDEXES[195]._size @ 12512
	.bits	532,16			; _ODI_EEPROM_INDEXES[195]._address @ 12528
	.bits	25670,16			; _ODI_EEPROM_INDEXES[196]._index @ 12544
	.bits	14,16			; _ODI_EEPROM_INDEXES[196]._subindex @ 12560
	.bits	4,16			; _ODI_EEPROM_INDEXES[196]._size @ 12576
	.bits	536,16			; _ODI_EEPROM_INDEXES[196]._address @ 12592
	.bits	25670,16			; _ODI_EEPROM_INDEXES[197]._index @ 12608
	.bits	15,16			; _ODI_EEPROM_INDEXES[197]._subindex @ 12624
	.bits	4,16			; _ODI_EEPROM_INDEXES[197]._size @ 12640
	.bits	540,16			; _ODI_EEPROM_INDEXES[197]._address @ 12656
	.bits	25670,16			; _ODI_EEPROM_INDEXES[198]._index @ 12672
	.bits	16,16			; _ODI_EEPROM_INDEXES[198]._subindex @ 12688
	.bits	4,16			; _ODI_EEPROM_INDEXES[198]._size @ 12704
	.bits	544,16			; _ODI_EEPROM_INDEXES[198]._address @ 12720
	.bits	25680,16			; _ODI_EEPROM_INDEXES[199]._index @ 12736
	.bits	1,16			; _ODI_EEPROM_INDEXES[199]._subindex @ 12752
	.bits	4,16			; _ODI_EEPROM_INDEXES[199]._size @ 12768
	.bits	548,16			; _ODI_EEPROM_INDEXES[199]._address @ 12784
	.bits	25680,16			; _ODI_EEPROM_INDEXES[200]._index @ 12800
	.bits	2,16			; _ODI_EEPROM_INDEXES[200]._subindex @ 12816
	.bits	4,16			; _ODI_EEPROM_INDEXES[200]._size @ 12832
	.bits	552,16			; _ODI_EEPROM_INDEXES[200]._address @ 12848
	.bits	25680,16			; _ODI_EEPROM_INDEXES[201]._index @ 12864
	.bits	3,16			; _ODI_EEPROM_INDEXES[201]._subindex @ 12880
	.bits	4,16			; _ODI_EEPROM_INDEXES[201]._size @ 12896
	.bits	556,16			; _ODI_EEPROM_INDEXES[201]._address @ 12912
	.bits	25680,16			; _ODI_EEPROM_INDEXES[202]._index @ 12928
	.bits	4,16			; _ODI_EEPROM_INDEXES[202]._subindex @ 12944
	.bits	4,16			; _ODI_EEPROM_INDEXES[202]._size @ 12960
	.bits	560,16			; _ODI_EEPROM_INDEXES[202]._address @ 12976
	.bits	25680,16			; _ODI_EEPROM_INDEXES[203]._index @ 12992
	.bits	5,16			; _ODI_EEPROM_INDEXES[203]._subindex @ 13008
	.bits	4,16			; _ODI_EEPROM_INDEXES[203]._size @ 13024
	.bits	564,16			; _ODI_EEPROM_INDEXES[203]._address @ 13040
	.bits	25680,16			; _ODI_EEPROM_INDEXES[204]._index @ 13056
	.bits	6,16			; _ODI_EEPROM_INDEXES[204]._subindex @ 13072
	.bits	4,16			; _ODI_EEPROM_INDEXES[204]._size @ 13088
	.bits	568,16			; _ODI_EEPROM_INDEXES[204]._address @ 13104
	.bits	25680,16			; _ODI_EEPROM_INDEXES[205]._index @ 13120
	.bits	7,16			; _ODI_EEPROM_INDEXES[205]._subindex @ 13136
	.bits	4,16			; _ODI_EEPROM_INDEXES[205]._size @ 13152
	.bits	572,16			; _ODI_EEPROM_INDEXES[205]._address @ 13168
	.bits	25680,16			; _ODI_EEPROM_INDEXES[206]._index @ 13184
	.bits	8,16			; _ODI_EEPROM_INDEXES[206]._subindex @ 13200
	.bits	4,16			; _ODI_EEPROM_INDEXES[206]._size @ 13216
	.bits	576,16			; _ODI_EEPROM_INDEXES[206]._address @ 13232
	.bits	25680,16			; _ODI_EEPROM_INDEXES[207]._index @ 13248
	.bits	9,16			; _ODI_EEPROM_INDEXES[207]._subindex @ 13264
	.bits	4,16			; _ODI_EEPROM_INDEXES[207]._size @ 13280
	.bits	580,16			; _ODI_EEPROM_INDEXES[207]._address @ 13296
	.bits	25680,16			; _ODI_EEPROM_INDEXES[208]._index @ 13312
	.bits	10,16			; _ODI_EEPROM_INDEXES[208]._subindex @ 13328
	.bits	4,16			; _ODI_EEPROM_INDEXES[208]._size @ 13344
	.bits	584,16			; _ODI_EEPROM_INDEXES[208]._address @ 13360
	.bits	25680,16			; _ODI_EEPROM_INDEXES[209]._index @ 13376
	.bits	11,16			; _ODI_EEPROM_INDEXES[209]._subindex @ 13392
	.bits	4,16			; _ODI_EEPROM_INDEXES[209]._size @ 13408
	.bits	588,16			; _ODI_EEPROM_INDEXES[209]._address @ 13424
	.bits	25680,16			; _ODI_EEPROM_INDEXES[210]._index @ 13440
	.bits	12,16			; _ODI_EEPROM_INDEXES[210]._subindex @ 13456
	.bits	4,16			; _ODI_EEPROM_INDEXES[210]._size @ 13472
	.bits	592,16			; _ODI_EEPROM_INDEXES[210]._address @ 13488
	.bits	25680,16			; _ODI_EEPROM_INDEXES[211]._index @ 13504
	.bits	13,16			; _ODI_EEPROM_INDEXES[211]._subindex @ 13520
	.bits	4,16			; _ODI_EEPROM_INDEXES[211]._size @ 13536
	.bits	596,16			; _ODI_EEPROM_INDEXES[211]._address @ 13552
	.bits	25680,16			; _ODI_EEPROM_INDEXES[212]._index @ 13568
	.bits	14,16			; _ODI_EEPROM_INDEXES[212]._subindex @ 13584
	.bits	4,16			; _ODI_EEPROM_INDEXES[212]._size @ 13600
	.bits	600,16			; _ODI_EEPROM_INDEXES[212]._address @ 13616
	.bits	25680,16			; _ODI_EEPROM_INDEXES[213]._index @ 13632
	.bits	15,16			; _ODI_EEPROM_INDEXES[213]._subindex @ 13648
	.bits	4,16			; _ODI_EEPROM_INDEXES[213]._size @ 13664
	.bits	604,16			; _ODI_EEPROM_INDEXES[213]._address @ 13680
	.bits	25680,16			; _ODI_EEPROM_INDEXES[214]._index @ 13696
	.bits	16,16			; _ODI_EEPROM_INDEXES[214]._subindex @ 13712
	.bits	4,16			; _ODI_EEPROM_INDEXES[214]._size @ 13728
	.bits	608,16			; _ODI_EEPROM_INDEXES[214]._address @ 13744

$C$DW$490	.dwtag  DW_TAG_variable, DW_AT_name("ODI_EEPROM_INDEXES")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_ODI_EEPROM_INDEXES")
	.dwattr $C$DW$490, DW_AT_location[DW_OP_addr _ODI_EEPROM_INDEXES]
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$490, DW_AT_external
	.global	_ODV_RecorderData1
_ODV_RecorderData1:	.usect	"MeasureData",4096,1,0
$C$DW$491	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RecorderData1")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_ODV_RecorderData1")
	.dwattr $C$DW$491, DW_AT_location[DW_OP_addr _ODV_RecorderData1]
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$491, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1768012 
	.sect	".text"
	.global	_ODI_mms_dict_valueRangeTest

$C$DW$492	.dwtag  DW_TAG_subprogram, DW_AT_name("ODI_mms_dict_valueRangeTest")
	.dwattr $C$DW$492, DW_AT_low_pc(_ODI_mms_dict_valueRangeTest)
	.dwattr $C$DW$492, DW_AT_high_pc(0x00)
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_ODI_mms_dict_valueRangeTest")
	.dwattr $C$DW$492, DW_AT_external
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$492, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c")
	.dwattr $C$DW$492, DW_AT_TI_begin_line(0x1a0)
	.dwattr $C$DW$492, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$492, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 417,column 1,is_stmt,address _ODI_mms_dict_valueRangeTest

	.dwfde $C$DW$CIE, _ODI_mms_dict_valueRangeTest
$C$DW$493	.dwtag  DW_TAG_formal_parameter, DW_AT_name("typeValue")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_typeValue")
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$493, DW_AT_location[DW_OP_reg0]
$C$DW$494	.dwtag  DW_TAG_formal_parameter, DW_AT_name("value")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$494, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _ODI_mms_dict_valueRangeTest  FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_ODI_mms_dict_valueRangeTest:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$495	.dwtag  DW_TAG_variable, DW_AT_name("typeValue")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_typeValue")
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$495, DW_AT_location[DW_OP_breg20 -1]
$C$DW$496	.dwtag  DW_TAG_variable, DW_AT_name("value")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$496, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[1],AL            ; [CPU_] |417| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |417| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 418,column 3,is_stmt
        B         $C$L2,UNC             ; [CPU_] |418| 
        ; branch occurs ; [] |418| 
$C$L1:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 420,column 7,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |420| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |420| 
        BF        $C$L3,EQ              ; [CPU_] |420| 
        ; branchcc occurs ; [] |420| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 420,column 37,is_stmt
        MOV       AL,#48                ; [CPU_] |420| 
        MOV       AH,#1545              ; [CPU_] |420| 
        B         $C$L4,UNC             ; [CPU_] |420| 
        ; branch occurs ; [] |420| 
$C$L2:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 418,column 3,is_stmt
        CMPB      AL,#159               ; [CPU_] |418| 
        BF        $C$L1,EQ              ; [CPU_] |418| 
        ; branchcc occurs ; [] |418| 
$C$L3:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 423,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |423| 
$C$L4:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 424,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$497	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$497, DW_AT_low_pc(0x00)
	.dwattr $C$DW$497, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$492, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c")
	.dwattr $C$DW$492, DW_AT_TI_end_line(0x1a8)
	.dwattr $C$DW$492, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$492

	.sect	".text"
	.global	_ODI_mms_dict_scanIndexOD

$C$DW$498	.dwtag  DW_TAG_subprogram, DW_AT_name("ODI_mms_dict_scanIndexOD")
	.dwattr $C$DW$498, DW_AT_low_pc(_ODI_mms_dict_scanIndexOD)
	.dwattr $C$DW$498, DW_AT_high_pc(0x00)
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_ODI_mms_dict_scanIndexOD")
	.dwattr $C$DW$498, DW_AT_external
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$498, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c")
	.dwattr $C$DW$498, DW_AT_TI_begin_line(0x71b)
	.dwattr $C$DW$498, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$498, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1820,column 1,is_stmt,address _ODI_mms_dict_scanIndexOD

	.dwfde $C$DW$CIE, _ODI_mms_dict_scanIndexOD
$C$DW$499	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$499, DW_AT_location[DW_OP_reg0]
$C$DW$500	.dwtag  DW_TAG_formal_parameter, DW_AT_name("errorCode")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$500, DW_AT_location[DW_OP_reg12]
$C$DW$501	.dwtag  DW_TAG_formal_parameter, DW_AT_name("callbacks")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_callbacks")
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$501, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _ODI_mms_dict_scanIndexOD     FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_ODI_mms_dict_scanIndexOD:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$502	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$502, DW_AT_location[DW_OP_breg20 -1]
$C$DW$503	.dwtag  DW_TAG_variable, DW_AT_name("errorCode")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$503, DW_AT_location[DW_OP_breg20 -4]
$C$DW$504	.dwtag  DW_TAG_variable, DW_AT_name("callbacks")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_callbacks")
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$504, DW_AT_location[DW_OP_breg20 -6]
$C$DW$505	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$505, DW_AT_location[DW_OP_breg20 -7]
        MOV       *-SP[1],AL            ; [CPU_] |1820| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |1820| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |1820| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1822,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1822| 
        MOVB      ACC,#0                ; [CPU_] |1822| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |1822| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1823,column 5,is_stmt
        B         $C$L77,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L5:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1824,column 22,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |1824| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1824,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1824| 
        ; branch occurs ; [] |1824| 
$C$L6:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1825,column 22,is_stmt
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |1825| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1825,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1825| 
        ; branch occurs ; [] |1825| 
$C$L7:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1826,column 22,is_stmt
        MOVB      *-SP[7],#2,UNC        ; [CPU_] |1826| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1826,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1826| 
        ; branch occurs ; [] |1826| 
$C$L8:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1827,column 22,is_stmt
        MOVB      *-SP[7],#3,UNC        ; [CPU_] |1827| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1827,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1827| 
        ; branch occurs ; [] |1827| 
$C$L9:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1828,column 22,is_stmt
        MOVB      *-SP[7],#4,UNC        ; [CPU_] |1828| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1828,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1828| 
        ; branch occurs ; [] |1828| 
$C$L10:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1829,column 22,is_stmt
        MOVB      *-SP[7],#5,UNC        ; [CPU_] |1829| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1829,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1829| 
        ; branch occurs ; [] |1829| 
$C$L11:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1830,column 22,is_stmt
        MOVB      *-SP[7],#6,UNC        ; [CPU_] |1830| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1830,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1830| 
        ; branch occurs ; [] |1830| 
$C$L12:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1831,column 22,is_stmt
        MOVB      *-SP[7],#7,UNC        ; [CPU_] |1831| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1831,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1831| 
        ; branch occurs ; [] |1831| 
$C$L13:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1832,column 22,is_stmt
        MOVB      *-SP[7],#8,UNC        ; [CPU_] |1832| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1832,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1832| 
        ; branch occurs ; [] |1832| 
$C$L14:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1833,column 22,is_stmt
        MOVB      *-SP[7],#9,UNC        ; [CPU_] |1833| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1833,column 28,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1833| 
        ; branch occurs ; [] |1833| 
$C$L15:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1834,column 22,is_stmt
        MOVB      *-SP[7],#10,UNC       ; [CPU_] |1834| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1834,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1834| 
        ; branch occurs ; [] |1834| 
$C$L16:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1835,column 22,is_stmt
        MOVB      *-SP[7],#11,UNC       ; [CPU_] |1835| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1835,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1835| 
        ; branch occurs ; [] |1835| 
$C$L17:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1836,column 22,is_stmt
        MOVB      *-SP[7],#12,UNC       ; [CPU_] |1836| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1836,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1836| 
        ; branch occurs ; [] |1836| 
$C$L18:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1837,column 22,is_stmt
        MOVB      *-SP[7],#13,UNC       ; [CPU_] |1837| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1837,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1837| 
        ; branch occurs ; [] |1837| 
$C$L19:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1838,column 22,is_stmt
        MOVB      *-SP[7],#14,UNC       ; [CPU_] |1838| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1838,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1838| 
        ; branch occurs ; [] |1838| 
$C$L20:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1839,column 22,is_stmt
        MOVB      *-SP[7],#15,UNC       ; [CPU_] |1839| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1839,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1839| 
        ; branch occurs ; [] |1839| 
$C$L21:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1840,column 22,is_stmt
        MOVB      *-SP[7],#16,UNC       ; [CPU_] |1840| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1840,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1840| 
        ; branch occurs ; [] |1840| 
$C$L22:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1841,column 22,is_stmt
        MOVB      *-SP[7],#17,UNC       ; [CPU_] |1841| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1841,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1841| 
        ; branch occurs ; [] |1841| 
$C$L23:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1842,column 22,is_stmt
        MOVB      *-SP[7],#18,UNC       ; [CPU_] |1842| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1842,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1842| 
        ; branch occurs ; [] |1842| 
$C$L24:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1843,column 22,is_stmt
        MOVB      *-SP[7],#19,UNC       ; [CPU_] |1843| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1843,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1843| 
        ; branch occurs ; [] |1843| 
$C$L25:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1844,column 22,is_stmt
        MOVB      *-SP[7],#20,UNC       ; [CPU_] |1844| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1844,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1844| 
        ; branch occurs ; [] |1844| 
$C$L26:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1845,column 22,is_stmt
        MOVB      *-SP[7],#21,UNC       ; [CPU_] |1845| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1845,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1845| 
        ; branch occurs ; [] |1845| 
$C$L27:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1846,column 22,is_stmt
        MOVB      *-SP[7],#22,UNC       ; [CPU_] |1846| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1846,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1846| 
        ; branch occurs ; [] |1846| 
$C$L28:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1847,column 22,is_stmt
        MOVB      *-SP[7],#23,UNC       ; [CPU_] |1847| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1847,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1847| 
        MOVL      XAR4,#_ODC_Recorder_callbacks ; [CPU_U] |1847| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1847| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1847,column 66,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1847| 
        ; branch occurs ; [] |1847| 
$C$L29:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1848,column 22,is_stmt
        MOVB      *-SP[7],#24,UNC       ; [CPU_] |1848| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1848,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1848| 
        ; branch occurs ; [] |1848| 
$C$L30:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1849,column 22,is_stmt
        MOVB      *-SP[7],#25,UNC       ; [CPU_] |1849| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1849,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1849| 
        MOVL      XAR4,#_ODC_ErrorDsp_callbacks ; [CPU_U] |1849| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1849| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1849,column 66,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1849| 
        ; branch occurs ; [] |1849| 
$C$L31:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1850,column 22,is_stmt
        MOVB      *-SP[7],#26,UNC       ; [CPU_] |1850| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1850,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1850| 
        MOVL      XAR4,#_ODC_SciSend_callbacks ; [CPU_U] |1850| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1850| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1850,column 65,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1850| 
        ; branch occurs ; [] |1850| 
$C$L32:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1851,column 22,is_stmt
        MOVB      *-SP[7],#27,UNC       ; [CPU_] |1851| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1851,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1851| 
        MOVL      XAR4,#_ODC_Gateway_callbacks ; [CPU_U] |1851| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1851| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1851,column 65,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1851| 
        ; branch occurs ; [] |1851| 
$C$L33:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1852,column 22,is_stmt
        MOVB      *-SP[7],#28,UNC       ; [CPU_] |1852| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1852,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1852| 
        ; branch occurs ; [] |1852| 
$C$L34:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1853,column 22,is_stmt
        MOVB      *-SP[7],#29,UNC       ; [CPU_] |1853| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1853,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1853| 
        ; branch occurs ; [] |1853| 
$C$L35:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1854,column 22,is_stmt
        MOVB      *-SP[7],#30,UNC       ; [CPU_] |1854| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1854,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1854| 
        ; branch occurs ; [] |1854| 
$C$L36:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1855,column 22,is_stmt
        MOVB      *-SP[7],#31,UNC       ; [CPU_] |1855| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1855,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1855| 
        ; branch occurs ; [] |1855| 
$C$L37:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1856,column 22,is_stmt
        MOVB      *-SP[7],#32,UNC       ; [CPU_] |1856| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1856,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1856| 
        ; branch occurs ; [] |1856| 
$C$L38:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1857,column 22,is_stmt
        MOVB      *-SP[7],#33,UNC       ; [CPU_] |1857| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1857,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1857| 
        ; branch occurs ; [] |1857| 
$C$L39:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1858,column 22,is_stmt
        MOVB      *-SP[7],#34,UNC       ; [CPU_] |1858| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1858,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1858| 
        ; branch occurs ; [] |1858| 
$C$L40:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1859,column 22,is_stmt
        MOVB      *-SP[7],#35,UNC       ; [CPU_] |1859| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1859,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1859| 
        ; branch occurs ; [] |1859| 
$C$L41:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1860,column 22,is_stmt
        MOVB      *-SP[7],#36,UNC       ; [CPU_] |1860| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1860,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1860| 
        ; branch occurs ; [] |1860| 
$C$L42:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1861,column 22,is_stmt
        MOVB      *-SP[7],#37,UNC       ; [CPU_] |1861| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1861,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1861| 
        ; branch occurs ; [] |1861| 
$C$L43:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1862,column 22,is_stmt
        MOVB      *-SP[7],#38,UNC       ; [CPU_] |1862| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1862,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1862| 
        ; branch occurs ; [] |1862| 
$C$L44:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1863,column 22,is_stmt
        MOVB      *-SP[7],#39,UNC       ; [CPU_] |1863| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1863,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1863| 
        ; branch occurs ; [] |1863| 
$C$L45:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1864,column 22,is_stmt
        MOVB      *-SP[7],#40,UNC       ; [CPU_] |1864| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1864,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1864| 
        ; branch occurs ; [] |1864| 
$C$L46:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1865,column 22,is_stmt
        MOVB      *-SP[7],#41,UNC       ; [CPU_] |1865| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1865,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1865| 
        ; branch occurs ; [] |1865| 
$C$L47:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1866,column 22,is_stmt
        MOVB      *-SP[7],#42,UNC       ; [CPU_] |1866| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1866,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1866| 
        ; branch occurs ; [] |1866| 
$C$L48:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1867,column 22,is_stmt
        MOVB      *-SP[7],#43,UNC       ; [CPU_] |1867| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1867,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1867| 
        MOVL      XAR4,#_ODC_CommError_callbacks ; [CPU_U] |1867| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1867| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1867,column 67,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1867| 
        ; branch occurs ; [] |1867| 
$C$L49:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1868,column 22,is_stmt
        MOVB      *-SP[7],#44,UNC       ; [CPU_] |1868| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1868,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1868| 
        MOVL      XAR4,#_ODC_Board_callbacks ; [CPU_U] |1868| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1868| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1868,column 63,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1868| 
        ; branch occurs ; [] |1868| 
$C$L50:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1869,column 22,is_stmt
        MOVB      *-SP[7],#45,UNC       ; [CPU_] |1869| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1869,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1869| 
        ; branch occurs ; [] |1869| 
$C$L51:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1870,column 22,is_stmt
        MOVB      *-SP[7],#46,UNC       ; [CPU_] |1870| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1870,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1870| 
        MOVL      XAR4,#_ODC_RTC_callbacks ; [CPU_U] |1870| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1870| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1870,column 61,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1870| 
        ; branch occurs ; [] |1870| 
$C$L52:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1871,column 22,is_stmt
        MOVB      *-SP[7],#47,UNC       ; [CPU_] |1871| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1871,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1871| 
        MOVL      XAR4,#_ODC_Version_callbacks ; [CPU_U] |1871| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1871| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1871,column 65,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1871| 
        ; branch occurs ; [] |1871| 
$C$L53:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1872,column 22,is_stmt
        MOVB      *-SP[7],#48,UNC       ; [CPU_] |1872| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1872,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1872| 
        MOVL      XAR4,#_ODC_Security_callbacks ; [CPU_U] |1872| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1872| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1872,column 66,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1872| 
        ; branch occurs ; [] |1872| 
$C$L54:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1873,column 22,is_stmt
        MOVB      *-SP[7],#49,UNC       ; [CPU_] |1873| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1873,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1873| 
        ; branch occurs ; [] |1873| 
$C$L55:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1874,column 22,is_stmt
        MOVB      *-SP[7],#50,UNC       ; [CPU_] |1874| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1874,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1874| 
        ; branch occurs ; [] |1874| 
$C$L56:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1875,column 22,is_stmt
        MOVB      *-SP[7],#51,UNC       ; [CPU_] |1875| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1875,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1875| 
        MOVL      XAR4,#_ODC_Battery_callbacks ; [CPU_U] |1875| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1875| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1875,column 65,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1875| 
        ; branch occurs ; [] |1875| 
$C$L57:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1876,column 22,is_stmt
        MOVB      *-SP[7],#52,UNC       ; [CPU_] |1876| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1876,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1876| 
        ; branch occurs ; [] |1876| 
$C$L58:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1877,column 22,is_stmt
        MOVB      *-SP[7],#53,UNC       ; [CPU_] |1877| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1877,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1877| 
        ; branch occurs ; [] |1877| 
$C$L59:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1878,column 22,is_stmt
        MOVB      *-SP[7],#54,UNC       ; [CPU_] |1878| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1878,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1878| 
        MOVL      XAR4,#_ODC_StoreParameters_callbacks ; [CPU_U] |1878| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1878| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1878,column 73,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1878| 
        ; branch occurs ; [] |1878| 
$C$L60:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1879,column 22,is_stmt
        MOVB      *-SP[7],#55,UNC       ; [CPU_] |1879| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1879,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1879| 
        MOVL      XAR4,#_ODC_RestoreDefaultParameters_callbacks ; [CPU_U] |1879| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1879| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1879,column 82,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1879| 
        ; branch occurs ; [] |1879| 
$C$L61:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1880,column 22,is_stmt
        MOVB      *-SP[7],#56,UNC       ; [CPU_] |1880| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1880,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1880| 
        MOVL      XAR4,#_ODC_ResetHW_callbacks ; [CPU_U] |1880| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1880| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1880,column 65,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1880| 
        ; branch occurs ; [] |1880| 
$C$L62:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1881,column 22,is_stmt
        MOVB      *-SP[7],#57,UNC       ; [CPU_] |1881| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1881,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1881| 
        MOVL      XAR4,#_ODC_Debug_callbacks ; [CPU_U] |1881| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1881| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1881,column 63,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1881| 
        ; branch occurs ; [] |1881| 
$C$L63:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1882,column 22,is_stmt
        MOVB      *-SP[7],#58,UNC       ; [CPU_] |1882| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1882,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1882| 
        ; branch occurs ; [] |1882| 
$C$L64:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1883,column 22,is_stmt
        MOVB      *-SP[7],#59,UNC       ; [CPU_] |1883| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1883,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1883| 
        MOVL      XAR4,#_ODC_Write_Outputs_16_Bit_callbacks ; [CPU_U] |1883| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1883| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1883,column 78,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1883| 
        ; branch occurs ; [] |1883| 
$C$L65:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1884,column 22,is_stmt
        MOVB      *-SP[7],#60,UNC       ; [CPU_] |1884| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1884,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1884| 
        ; branch occurs ; [] |1884| 
$C$L66:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1885,column 22,is_stmt
        MOVB      *-SP[7],#61,UNC       ; [CPU_] |1885| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1885,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1885| 
        MOVL      XAR4,#_ODC_Write_Analogue_Output_16_Bit_callbacks ; [CPU_U] |1885| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1885| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1885,column 86,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1885| 
        ; branch occurs ; [] |1885| 
$C$L67:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1886,column 22,is_stmt
        MOVB      *-SP[7],#62,UNC       ; [CPU_] |1886| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1886,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1886| 
        ; branch occurs ; [] |1886| 
$C$L68:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1887,column 22,is_stmt
        MOVB      *-SP[7],#63,UNC       ; [CPU_] |1887| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1887,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1887| 
        ; branch occurs ; [] |1887| 
$C$L69:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1888,column 22,is_stmt
        MOVB      *-SP[7],#64,UNC       ; [CPU_] |1888| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1888,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1888| 
        ; branch occurs ; [] |1888| 
$C$L70:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1889,column 22,is_stmt
        MOVB      *-SP[7],#65,UNC       ; [CPU_] |1889| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1889,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1889| 
        ; branch occurs ; [] |1889| 
$C$L71:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1890,column 22,is_stmt
        MOVB      *-SP[7],#66,UNC       ; [CPU_] |1890| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1890,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1890| 
        ; branch occurs ; [] |1890| 
$C$L72:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1891,column 22,is_stmt
        MOVB      *-SP[7],#67,UNC       ; [CPU_] |1891| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1891,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1891| 
        ; branch occurs ; [] |1891| 
$C$L73:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1892,column 22,is_stmt
        MOVB      *-SP[7],#68,UNC       ; [CPU_] |1892| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1892,column 29,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |1892| 
        MOVL      XAR4,#_ODC_Controlword_callbacks ; [CPU_U] |1892| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |1892| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1892,column 69,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1892| 
        ; branch occurs ; [] |1892| 
$C$L74:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1893,column 22,is_stmt
        MOVB      *-SP[7],#69,UNC       ; [CPU_] |1893| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1893,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1893| 
        ; branch occurs ; [] |1893| 
$C$L75:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1894,column 22,is_stmt
        MOVB      *-SP[7],#70,UNC       ; [CPU_] |1894| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1894,column 29,is_stmt
        B         $C$L94,UNC            ; [CPU_] |1894| 
        ; branch occurs ; [] |1894| 
$C$L76:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1896,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1896| 
        MOV       ACC,#3076 << 15       ; [CPU_] |1896| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |1896| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1897,column 13,is_stmt
        MOVB      XAR4,#0               ; [CPU_] |1897| 
        B         $C$L95,UNC            ; [CPU_] |1897| 
        ; branch occurs ; [] |1897| 
$C$L77:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1823,column 5,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |1823| 
        CMP       AL,#8227              ; [CPU_] |1823| 
        B         $C$L85,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8227              ; [CPU_] |1823| 
        BF        $C$L48,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#5120              ; [CPU_] |1823| 
        B         $C$L81,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#5120              ; [CPU_] |1823| 
        BF        $C$L14,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#4102              ; [CPU_] |1823| 
        B         $C$L79,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#4102              ; [CPU_] |1823| 
        BF        $C$L8,EQ              ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#4096              ; [CPU_] |1823| 
        B         $C$L78,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#4096              ; [CPU_] |1823| 
        BF        $C$L5,EQ              ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#32832             ; [CPU_] |1823| 
        BF        $C$L73,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#32833             ; [CPU_] |1823| 
        BF        $C$L74,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#34050             ; [CPU_] |1823| 
        BF        $C$L75,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L78:    
        CMP       AL,#4097              ; [CPU_] |1823| 
        BF        $C$L6,EQ              ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#4101              ; [CPU_] |1823| 
        BF        $C$L7,EQ              ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L79:    
        CMP       AL,#4120              ; [CPU_] |1823| 
        B         $C$L80,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#4120              ; [CPU_] |1823| 
        BF        $C$L11,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#4106              ; [CPU_] |1823| 
        BF        $C$L9,EQ              ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#4116              ; [CPU_] |1823| 
        BF        $C$L10,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L80:    
        CMP       AL,#4608              ; [CPU_] |1823| 
        BF        $C$L12,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#4736              ; [CPU_] |1823| 
        BF        $C$L13,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L81:    
        CMP       AL,#6657              ; [CPU_] |1823| 
        B         $C$L83,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#6657              ; [CPU_] |1823| 
        BF        $C$L21,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#6146              ; [CPU_] |1823| 
        B         $C$L82,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#6146              ; [CPU_] |1823| 
        BF        $C$L18,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#5632              ; [CPU_] |1823| 
        BF        $C$L15,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#6144              ; [CPU_] |1823| 
        BF        $C$L16,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#6145              ; [CPU_] |1823| 
        BF        $C$L17,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L82:    
        CMP       AL,#6147              ; [CPU_] |1823| 
        BF        $C$L19,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#6656              ; [CPU_] |1823| 
        BF        $C$L20,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L83:    
        CMP       AL,#8213              ; [CPU_] |1823| 
        B         $C$L84,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8192              ; [CPU_] |1823| 
        B         $C$L93,GEQ            ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#6658              ; [CPU_] |1823| 
        BF        $C$L22,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#6659              ; [CPU_] |1823| 
        BF        $C$L23,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L84:    
        CMP       AL,#8220              ; [CPU_] |1823| 
        BF        $C$L46,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8222              ; [CPU_] |1823| 
        BF        $C$L47,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L85:    
        CMP       AL,#12034             ; [CPU_] |1823| 
        B         $C$L89,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#12034             ; [CPU_] |1823| 
        BF        $C$L61,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8452              ; [CPU_] |1823| 
        B         $C$L87,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8452              ; [CPU_] |1823| 
        BF        $C$L55,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8449              ; [CPU_] |1823| 
        B         $C$L86,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8449              ; [CPU_] |1823| 
        BF        $C$L52,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8256              ; [CPU_] |1823| 
        BF        $C$L49,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8257              ; [CPU_] |1823| 
        BF        $C$L50,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8448              ; [CPU_] |1823| 
        BF        $C$L51,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L86:    
        CMP       AL,#8450              ; [CPU_] |1823| 
        BF        $C$L53,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8451              ; [CPU_] |1823| 
        BF        $C$L54,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L87:    
        CMP       AL,#8704              ; [CPU_] |1823| 
        B         $C$L88,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8704              ; [CPU_] |1823| 
        BF        $C$L58,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8453              ; [CPU_] |1823| 
        BF        $C$L56,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#8454              ; [CPU_] |1823| 
        BF        $C$L57,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L88:    
        CMP       AL,#12032             ; [CPU_] |1823| 
        BF        $C$L59,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#12033             ; [CPU_] |1823| 
        BF        $C$L60,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L89:    
        CMP       AL,#25647             ; [CPU_] |1823| 
        B         $C$L91,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#25647             ; [CPU_] |1823| 
        BF        $C$L67,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#25344             ; [CPU_] |1823| 
        B         $C$L90,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#25344             ; [CPU_] |1823| 
        BF        $C$L64,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#12035             ; [CPU_] |1823| 
        BF        $C$L62,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#24832             ; [CPU_] |1823| 
        BF        $C$L63,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L90:    
        CMP       AL,#25601             ; [CPU_] |1823| 
        BF        $C$L65,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#25617             ; [CPU_] |1823| 
        BF        $C$L66,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L91:    
        CMP       AL,#25666             ; [CPU_] |1823| 
        B         $C$L92,GT             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#25666             ; [CPU_] |1823| 
        BF        $C$L70,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#25648             ; [CPU_] |1823| 
        BF        $C$L68,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#25649             ; [CPU_] |1823| 
        BF        $C$L69,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L92:    
        CMP       AL,#25670             ; [CPU_] |1823| 
        BF        $C$L71,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        CMP       AL,#25680             ; [CPU_] |1823| 
        BF        $C$L72,EQ             ; [CPU_] |1823| 
        ; branchcc occurs ; [] |1823| 
        B         $C$L76,UNC            ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
$C$L93:    
        SUB       AL,#8192              ; [CPU_] |1823| 
        MOVL      XAR7,#$C$SW1          ; [CPU_U] |1823| 
        MOV       ACC,AL << #1          ; [CPU_] |1823| 
        MOVZ      AR6,AL                ; [CPU_] |1823| 
        MOVL      ACC,XAR7              ; [CPU_] |1823| 
        ADDU      ACC,AR6               ; [CPU_] |1823| 
        MOVL      XAR7,ACC              ; [CPU_] |1823| 
        MOVL      XAR7,*XAR7            ; [CPU_] |1823| 
        LB        *XAR7                 ; [CPU_] |1823| 
        ; branch occurs ; [] |1823| 
	.sect	".switch:_ODI_mms_dict_scanIndexOD"
	.clink
$C$SW1:	.long	$C$L24	; 8192
	.long	$C$L25	; 8193
	.long	$C$L26	; 8194
	.long	$C$L27	; 8195
	.long	$C$L28	; 8196
	.long	$C$L29	; 8197
	.long	$C$L30	; 8198
	.long	$C$L31	; 8199
	.long	$C$L32	; 8200
	.long	$C$L33	; 8201
	.long	$C$L34	; 8202
	.long	$C$L35	; 8203
	.long	$C$L36	; 8204
	.long	$C$L37	; 8205
	.long	$C$L38	; 8206
	.long	$C$L39	; 8207
	.long	$C$L40	; 8208
	.long	$C$L41	; 8209
	.long	$C$L42	; 8210
	.long	$C$L43	; 8211
	.long	$C$L44	; 8212
	.long	$C$L45	; 8213
	.sect	".text"
$C$L94:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1899,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1899| 
        MOVB      ACC,#0                ; [CPU_] |1899| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |1899| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1900,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[7] << 2      ; [CPU_] |1900| 
        MOVL      XAR4,#_mms_dict_objdict ; [CPU_U] |1900| 
        ADDL      XAR4,ACC              ; [CPU_] |1900| 
$C$L95:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c",line 1901,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$506	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$506, DW_AT_low_pc(0x00)
	.dwattr $C$DW$506, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$498, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Dictionary/standard/mms_dict.c")
	.dwattr $C$DW$498, DW_AT_TI_end_line(0x76d)
	.dwattr $C$DW$498, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$498

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	__post_TPDO
	.global	__post_sync
	.global	__heartbeatError
	.global	__post_emcy
	.global	__post_SlaveBootup
	.global	__preOperational
	.global	__initialisation
	.global	__stopped
	.global	__operational
	.global	_ConfigCallback
	.global	_WriteTextCallback
	.global	_LogNBCallback
	.global	_CommErrorSetCallback
	.global	_BatteryCallBack
	.global	_VersionCallback
	.global	_SecurityCallBack
	.global	_MultiunitsCallback
	.global	__storeODSubIndex
	.global	_ClearErrorCallback
	.global	_SciSendCallback
	.global	_VariablesCallback
	.global	_StartRecorderCallBack
	.global	_WriteAnalogueOutputsCallback
	.global	_ControlWordCallBack
	.global	_ResetCallBack
	.global	_LoadDefaultParameters
	.global	_WriteOutputs16BitCallback
	.global	_DebugCallBack
	.global	_SaveAllParameters

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$507, DW_AT_name("cob_id")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$508, DW_AT_name("rtr")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$509, DW_AT_name("len")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$510, DW_AT_name("data")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$99	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$511, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$512, DW_AT_name("csSDO")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$513, DW_AT_name("csEmergency")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$514, DW_AT_name("csSYNC")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$515, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$516, DW_AT_name("csPDO")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$517, DW_AT_name("csLSS")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$56	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$56, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$518, DW_AT_name("errCode")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$519, DW_AT_name("errRegMask")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$520, DW_AT_name("active")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$90	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)

$C$DW$T$91	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$91, DW_AT_byte_size(0x18)
$C$DW$521	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$521, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$91


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$522, DW_AT_name("index")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$523, DW_AT_name("subindex")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$524, DW_AT_name("size")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$525, DW_AT_name("address")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$96	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$96, DW_AT_language(DW_LANG_C)
$C$DW$526	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$96)
$C$DW$T$108	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$526)

$C$DW$T$109	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$109, DW_AT_byte_size(0x35c)
$C$DW$527	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$527, DW_AT_upper_bound(0xd6)
	.dwendtag $C$DW$T$109

$C$DW$T$97	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$528, DW_AT_name("Pos_Record")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_Pos_Record")
	.dwattr $C$DW$528, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$529, DW_AT_name("Cur_Record")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_Cur_Record")
	.dwattr $C$DW$529, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$530, DW_AT_name("AutoTrigg")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_AutoTrigg")
	.dwattr $C$DW$530, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$531, DW_AT_name("AutoRecord")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_AutoRecord")
	.dwattr $C$DW$531, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$532, DW_AT_name("ContinuousRecord")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_ContinuousRecord")
	.dwattr $C$DW$532, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$533, DW_AT_name("Trig_Record")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_Trig_Record")
	.dwattr $C$DW$533, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$534, DW_AT_name("unused")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_unused")
	.dwattr $C$DW$534, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0a)
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24

$C$DW$T$111	.dwtag  DW_TAG_typedef, DW_AT_name("TControl")
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$111, DW_AT_language(DW_LANG_C)

$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$535, DW_AT_name("SwitchOn")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$535, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$536, DW_AT_name("EnableVolt")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$536, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$537, DW_AT_name("QuickStop")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$537, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$538, DW_AT_name("EnableOperation")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$538, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$539, DW_AT_name("OpModeSpecific")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_OpModeSpecific")
	.dwattr $C$DW$539, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$540, DW_AT_name("ResetFault")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$540, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$541, DW_AT_name("Halt")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$541, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$542, DW_AT_name("Oms")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$542, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$543, DW_AT_name("Rsvd")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$543, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$544, DW_AT_name("Manufacturer")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$544, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x01)
$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$545, DW_AT_name("SwitchOn")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$545, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$546, DW_AT_name("EnableVolt")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$546, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$547, DW_AT_name("QuickStop")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$547, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$548, DW_AT_name("EnableOperation")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$548, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$549, DW_AT_name("Rsvd0")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_Rsvd0")
	.dwattr $C$DW$549, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$550, DW_AT_name("ResetFault")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$550, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$551, DW_AT_name("Halt")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$551, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$552, DW_AT_name("Oms")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$552, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$553, DW_AT_name("Rsvd")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$553, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$554, DW_AT_name("Manufacturer")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$554, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$555, DW_AT_name("SwitchOn")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$555, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$556, DW_AT_name("EnableVolt")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$556, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$557, DW_AT_name("QuickStop")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$557, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$558, DW_AT_name("EnableOperation")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$558, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$559, DW_AT_name("NewSetPoint")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_NewSetPoint")
	.dwattr $C$DW$559, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$560, DW_AT_name("ChangeSetImm")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_ChangeSetImm")
	.dwattr $C$DW$560, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$561, DW_AT_name("Abs_Rel")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_Abs_Rel")
	.dwattr $C$DW$561, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$562, DW_AT_name("ResetFault")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$562, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$563, DW_AT_name("Halt")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$563, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$564, DW_AT_name("ChangeSetPos")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_ChangeSetPos")
	.dwattr $C$DW$564, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$565, DW_AT_name("Rsvd")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$565, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$566, DW_AT_name("Manufacturer")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$566, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27


$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x01)
$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$567, DW_AT_name("ReadyToSwitchOn")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_ReadyToSwitchOn")
	.dwattr $C$DW$567, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$568, DW_AT_name("SwitchedOn")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_SwitchedOn")
	.dwattr $C$DW$568, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$569, DW_AT_name("OperationEnabled")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_OperationEnabled")
	.dwattr $C$DW$569, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$570, DW_AT_name("Fault")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_Fault")
	.dwattr $C$DW$570, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$571, DW_AT_name("VoltageDisabled")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_VoltageDisabled")
	.dwattr $C$DW$571, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$572, DW_AT_name("NotQuickStop")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_NotQuickStop")
	.dwattr $C$DW$572, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$573, DW_AT_name("SwitchOnDisabled")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_SwitchOnDisabled")
	.dwattr $C$DW$573, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$574, DW_AT_name("Warning")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_Warning")
	.dwattr $C$DW$574, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$575, DW_AT_name("Manufacturer")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$575, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$576, DW_AT_name("Remote")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_Remote")
	.dwattr $C$DW$576, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$577, DW_AT_name("TargetReached")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_TargetReached")
	.dwattr $C$DW$577, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$578, DW_AT_name("InternalLimit")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_InternalLimit")
	.dwattr $C$DW$578, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$579, DW_AT_name("OpModeSpecific")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_OpModeSpecific")
	.dwattr $C$DW$579, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$580, DW_AT_name("Manufacturers")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_Manufacturers")
	.dwattr $C$DW$580, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28


$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x01)
$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$581, DW_AT_name("ReadyToSwitchOn")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_ReadyToSwitchOn")
	.dwattr $C$DW$581, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$582, DW_AT_name("SwitchedOn")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_SwitchedOn")
	.dwattr $C$DW$582, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$583, DW_AT_name("OperationEnabled")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_OperationEnabled")
	.dwattr $C$DW$583, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$584, DW_AT_name("Fault")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_Fault")
	.dwattr $C$DW$584, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$585, DW_AT_name("VoltageDisabled")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_VoltageDisabled")
	.dwattr $C$DW$585, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$586, DW_AT_name("NotQuickStop")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_NotQuickStop")
	.dwattr $C$DW$586, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$587, DW_AT_name("SwitchOnDisabled")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_SwitchOnDisabled")
	.dwattr $C$DW$587, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$588, DW_AT_name("Warning")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_Warning")
	.dwattr $C$DW$588, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$589, DW_AT_name("Manufacturer")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$589, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$590, DW_AT_name("Remote")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_Remote")
	.dwattr $C$DW$590, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$591, DW_AT_name("TargetReached")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_TargetReached")
	.dwattr $C$DW$591, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$592, DW_AT_name("InternalLimit")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_InternalLimit")
	.dwattr $C$DW$592, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$593, DW_AT_name("Speed")
	.dwattr $C$DW$593, DW_AT_TI_symbol_name("_Speed")
	.dwattr $C$DW$593, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$594, DW_AT_name("MaxSlipError")
	.dwattr $C$DW$594, DW_AT_TI_symbol_name("_MaxSlipError")
	.dwattr $C$DW$594, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$595, DW_AT_name("Manufacturers")
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_Manufacturers")
	.dwattr $C$DW$595, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x01)
$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$596, DW_AT_name("ReadyToSwitchOn")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_ReadyToSwitchOn")
	.dwattr $C$DW$596, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$597, DW_AT_name("SwitchedOn")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_SwitchedOn")
	.dwattr $C$DW$597, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$598, DW_AT_name("OperationEnabled")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_OperationEnabled")
	.dwattr $C$DW$598, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$599, DW_AT_name("Fault")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_Fault")
	.dwattr $C$DW$599, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$600, DW_AT_name("VoltageDisabled")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_VoltageDisabled")
	.dwattr $C$DW$600, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$601, DW_AT_name("NotQuickStop")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_NotQuickStop")
	.dwattr $C$DW$601, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$602, DW_AT_name("SwitchOnDisabled")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_SwitchOnDisabled")
	.dwattr $C$DW$602, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$603, DW_AT_name("Warning")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_Warning")
	.dwattr $C$DW$603, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$604, DW_AT_name("Manufacturer")
	.dwattr $C$DW$604, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$604, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$605, DW_AT_name("Remote")
	.dwattr $C$DW$605, DW_AT_TI_symbol_name("_Remote")
	.dwattr $C$DW$605, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$606, DW_AT_name("TargetReached")
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_TargetReached")
	.dwattr $C$DW$606, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$607, DW_AT_name("InternalLimit")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_InternalLimit")
	.dwattr $C$DW$607, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$608, DW_AT_name("SetPointAck")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_SetPointAck")
	.dwattr $C$DW$608, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$609, DW_AT_name("FollowError")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_FollowError")
	.dwattr $C$DW$609, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$610, DW_AT_name("Manufacturers")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_Manufacturers")
	.dwattr $C$DW$610, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$611, DW_AT_name("ReadyToSwitchOn")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_ReadyToSwitchOn")
	.dwattr $C$DW$611, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$612, DW_AT_name("SwitchedOn")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_SwitchedOn")
	.dwattr $C$DW$612, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$613, DW_AT_name("OperationEnabled")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_OperationEnabled")
	.dwattr $C$DW$613, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$614, DW_AT_name("Fault")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_Fault")
	.dwattr $C$DW$614, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$615, DW_AT_name("VoltageDisabled")
	.dwattr $C$DW$615, DW_AT_TI_symbol_name("_VoltageDisabled")
	.dwattr $C$DW$615, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$616, DW_AT_name("NotQuickStop")
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_NotQuickStop")
	.dwattr $C$DW$616, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$617, DW_AT_name("SwitchOnDisabled")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_SwitchOnDisabled")
	.dwattr $C$DW$617, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$618, DW_AT_name("Warning")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_Warning")
	.dwattr $C$DW$618, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$619, DW_AT_name("Manufacturer")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$619, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$620, DW_AT_name("Remote")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_Remote")
	.dwattr $C$DW$620, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$621, DW_AT_name("TargetReached")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_TargetReached")
	.dwattr $C$DW$621, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$622, DW_AT_name("InternalLimit")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_InternalLimit")
	.dwattr $C$DW$622, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$623, DW_AT_name("HomingAttained")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_HomingAttained")
	.dwattr $C$DW$623, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$624, DW_AT_name("HomingError")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_HomingError")
	.dwattr $C$DW$624, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$625, DW_AT_name("Manufacturers")
	.dwattr $C$DW$625, DW_AT_TI_symbol_name("_Manufacturers")
	.dwattr $C$DW$625, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x02)
$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$626, DW_AT_name("tototo")
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_tototo")
	.dwattr $C$DW$626, DW_AT_bit_offset(0x10), DW_AT_bit_size(0x10)
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$627	.dwtag  DW_TAG_member
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$627, DW_AT_name("tatata")
	.dwattr $C$DW$627, DW_AT_TI_symbol_name("_tatata")
	.dwattr $C$DW$627, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$32

$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("TSupported_drive_modes")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)

$C$DW$T$33	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x01)
$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$628, DW_AT_name("ControlWord")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_ControlWord")
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$629, DW_AT_name("AnyMode")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_AnyMode")
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$630, DW_AT_name("VelocityMode")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_VelocityMode")
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$631, DW_AT_name("PositionMode")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_PositionMode")
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33

$C$DW$T$115	.dwtag  DW_TAG_typedef, DW_AT_name("TControlword")
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$115, DW_AT_language(DW_LANG_C)

$C$DW$T$34	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x01)
$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$632, DW_AT_name("StatusWord")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_StatusWord")
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$633, DW_AT_name("AnyMode")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_AnyMode")
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$634, DW_AT_name("VelocityMode")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_VelocityMode")
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$635, DW_AT_name("PositionMode")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_PositionMode")
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$636, DW_AT_name("HomingMode")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_HomingMode")
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34

$C$DW$T$117	.dwtag  DW_TAG_typedef, DW_AT_name("TStatusword")
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)

$C$DW$T$59	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$637	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$58)
	.dwendtag $C$DW$T$59

$C$DW$T$60	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$T$60, DW_AT_address_class(0x16)
$C$DW$T$61	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
$C$DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)
$C$DW$T$73	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$73, DW_AT_language(DW_LANG_C)
$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("post_sync_t")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)
$C$DW$T$62	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$62, DW_AT_language(DW_LANG_C)
$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$68	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$68, DW_AT_language(DW_LANG_C)
$C$DW$638	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$58)
$C$DW$639	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$68

$C$DW$T$69	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_address_class(0x16)
$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
$C$DW$T$74	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$74, DW_AT_language(DW_LANG_C)

$C$DW$T$92	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)
$C$DW$640	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$58)
$C$DW$641	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$6)
$C$DW$642	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$9)
$C$DW$643	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$92

$C$DW$T$93	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_address_class(0x16)
$C$DW$T$94	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$119	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$119, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$119, DW_AT_byte_size(0x08)
$C$DW$644	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$644, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$119

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$75	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$75, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$645	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$645, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x16)
$C$DW$646	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$6)
$C$DW$T$47	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$646)
$C$DW$T$48	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$48, DW_AT_address_class(0x16)
$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x16)

$C$DW$T$123	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$123, DW_AT_byte_size(0x17)
$C$DW$647	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$647, DW_AT_upper_bound(0x16)
	.dwendtag $C$DW$T$123


$C$DW$T$125	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$125, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$125, DW_AT_byte_size(0x10)
$C$DW$648	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$648, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$125


$C$DW$T$127	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$127, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$127, DW_AT_byte_size(0x01)
$C$DW$649	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$649, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$127

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$650	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$9)
$C$DW$T$45	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$650)
$C$DW$T$46	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$46, DW_AT_address_class(0x16)
$C$DW$T$67	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$67, DW_AT_address_class(0x16)

$C$DW$T$128	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$128, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$128, DW_AT_byte_size(0x1000)
$C$DW$651	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$651, DW_AT_upper_bound(0xfff)
	.dwendtag $C$DW$T$128


$C$DW$T$130	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$130, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x02)
$C$DW$652	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$652, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$130

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$132	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$132, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$132, DW_AT_byte_size(0x2e)
$C$DW$653	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$653, DW_AT_upper_bound(0x16)
	.dwendtag $C$DW$T$132


$C$DW$T$134	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$134, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$134, DW_AT_byte_size(0x20)
$C$DW$654	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$654, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$134

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$49	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$655	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$6)
$C$DW$656	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$49

$C$DW$T$50	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_address_class(0x16)
$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$T$65	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$65, DW_AT_address_class(0x16)

$C$DW$T$76	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$657	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$58)
$C$DW$658	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$39)
$C$DW$659	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$6)
$C$DW$660	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$76

$C$DW$T$77	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x16)
$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$661	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$78)
$C$DW$T$79	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$661)
$C$DW$T$80	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$80, DW_AT_address_class(0x16)
$C$DW$T$81	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$T$81, DW_AT_address_class(0x16)

$C$DW$T$137	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$137, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$137, DW_AT_byte_size(0x1a)
$C$DW$662	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$662, DW_AT_upper_bound(0x0c)
	.dwendtag $C$DW$T$137


$C$DW$T$138	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$138, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$138, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$138, DW_AT_byte_size(0x08)
$C$DW$663	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$663, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$138


$C$DW$T$139	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$139, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$139, DW_AT_byte_size(0x02)
$C$DW$664	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$664, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$139


$C$DW$T$140	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$140, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$140, DW_AT_byte_size(0x20)
$C$DW$665	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$665, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$140


$C$DW$T$141	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$141, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$141, DW_AT_byte_size(0x0c)
$C$DW$666	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$666, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$141


$C$DW$T$142	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$142, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$142, DW_AT_byte_size(0x16)
$C$DW$667	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$667, DW_AT_upper_bound(0x0a)
	.dwendtag $C$DW$T$142


$C$DW$T$143	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$143, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$143, DW_AT_byte_size(0x0e)
$C$DW$668	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$668, DW_AT_upper_bound(0x06)
	.dwendtag $C$DW$T$143


$C$DW$T$144	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$144, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$144, DW_AT_byte_size(0x06)
$C$DW$669	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$669, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$144


$C$DW$T$145	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$145, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$145, DW_AT_byte_size(0x22)
$C$DW$670	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$670, DW_AT_upper_bound(0x10)
	.dwendtag $C$DW$T$145


$C$DW$T$146	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$T$146, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$146, DW_AT_byte_size(0x04)
$C$DW$671	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$671, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$146


$C$DW$T$85	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
$C$DW$672	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$58)
$C$DW$673	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$9)
$C$DW$674	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$85

$C$DW$T$86	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_address_class(0x16)
$C$DW$T$87	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$T$87, DW_AT_language(DW_LANG_C)

$C$DW$T$147	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$147, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$147, DW_AT_byte_size(0x2e)
$C$DW$675	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$675, DW_AT_upper_bound(0x16)
	.dwendtag $C$DW$T$147


$C$DW$T$149	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$149, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$149, DW_AT_byte_size(0x20)
$C$DW$676	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$676, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$149


$C$DW$T$151	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$151, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$151, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$151, DW_AT_byte_size(0x02)
$C$DW$677	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$677, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$151


$C$DW$T$153	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$153, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$153, DW_AT_byte_size(0x06)
$C$DW$678	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$678, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$153


$C$DW$T$155	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$155, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$155, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$155, DW_AT_byte_size(0x0e)
$C$DW$679	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$679, DW_AT_upper_bound(0x06)
	.dwendtag $C$DW$T$155


$C$DW$T$157	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$157, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$157, DW_AT_byte_size(0x10)
$C$DW$680	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$680, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$157


$C$DW$T$159	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$159, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$159, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$159, DW_AT_byte_size(0x0a)
$C$DW$681	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$681, DW_AT_upper_bound(0x04)
	.dwendtag $C$DW$T$159

$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$162	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$162, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$162, DW_AT_byte_size(0x2e)
$C$DW$682	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$682, DW_AT_upper_bound(0x16)
	.dwendtag $C$DW$T$162


$C$DW$T$164	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$164, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$164, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$164, DW_AT_byte_size(0x20)
$C$DW$683	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$683, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$164

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

$C$DW$T$88	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$88, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$88, DW_AT_byte_size(0x01)
$C$DW$684	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$685	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$88

$C$DW$T$89	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_language(DW_LANG_C)

$C$DW$T$54	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$54, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x01)
$C$DW$686	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$687	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$688	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$689	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$690	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$691	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$692	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$693	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$54

$C$DW$T$55	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)

$C$DW$T$71	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$71, DW_AT_byte_size(0x80)
$C$DW$694	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$694, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$71


$C$DW$T$35	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$35, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x06)
$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$695, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$696, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$697, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$697, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$698, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$698, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$699, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$699, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$700	.dwtag  DW_TAG_member
	.dwattr $C$DW$700, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$700, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$700, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$700, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$700, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35

$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$701	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$42)
$C$DW$T$43	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$701)
$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)

$C$DW$T$98	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$98, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$98, DW_AT_byte_size(0x132)
$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$702, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$703, DW_AT_name("objdict")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$704, DW_AT_name("PDO_status")
	.dwattr $C$DW$704, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$705, DW_AT_name("firstIndex")
	.dwattr $C$DW$705, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$706	.dwtag  DW_TAG_member
	.dwattr $C$DW$706, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$706, DW_AT_name("lastIndex")
	.dwattr $C$DW$706, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$706, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$707, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$707, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$708	.dwtag  DW_TAG_member
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$708, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$708, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$708, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$709, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$709, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$710	.dwtag  DW_TAG_member
	.dwattr $C$DW$710, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$710, DW_AT_name("transfers")
	.dwattr $C$DW$710, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$710, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$711	.dwtag  DW_TAG_member
	.dwattr $C$DW$711, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$711, DW_AT_name("nodeState")
	.dwattr $C$DW$711, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$711, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$712	.dwtag  DW_TAG_member
	.dwattr $C$DW$712, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$712, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$712, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$712, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$713	.dwtag  DW_TAG_member
	.dwattr $C$DW$713, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$713, DW_AT_name("initialisation")
	.dwattr $C$DW$713, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$713, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$714	.dwtag  DW_TAG_member
	.dwattr $C$DW$714, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$714, DW_AT_name("preOperational")
	.dwattr $C$DW$714, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$714, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$715, DW_AT_name("operational")
	.dwattr $C$DW$715, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$716	.dwtag  DW_TAG_member
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$716, DW_AT_name("stopped")
	.dwattr $C$DW$716, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$717	.dwtag  DW_TAG_member
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$717, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$717, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$717, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$717, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$718	.dwtag  DW_TAG_member
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$718, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$718, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$719	.dwtag  DW_TAG_member
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$719, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$719, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$719, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$720	.dwtag  DW_TAG_member
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$720, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$720, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$720, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$721	.dwtag  DW_TAG_member
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$721, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$721, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$721, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$722, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$723, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$724, DW_AT_name("heartbeatError")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$725, DW_AT_name("NMTable")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$726, DW_AT_name("syncTimer")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$727, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$727, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$728, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$729, DW_AT_name("post_sync")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_post_sync")
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$730, DW_AT_name("post_TPDO")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$731, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$732, DW_AT_name("toggle")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$733, DW_AT_name("canHandle")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$734, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$735, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$736, DW_AT_name("globalCallback")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$737, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$738, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$739, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$740, DW_AT_name("dcf_request")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$741, DW_AT_name("error_state")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$742, DW_AT_name("error_history_size")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$743, DW_AT_name("error_number")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$744, DW_AT_name("error_first_element")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$745, DW_AT_name("error_register")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$746, DW_AT_name("error_cobid")
	.dwattr $C$DW$746, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$747, DW_AT_name("error_data")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$748, DW_AT_name("post_emcy")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$749, DW_AT_name("lss_transfer")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$750, DW_AT_name("eeprom_index")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$751, DW_AT_name("eeprom_size")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$98

$C$DW$T$57	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)

$C$DW$T$100	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$100, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$100, DW_AT_byte_size(0x0e)
$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$752, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$753, DW_AT_name("event_timer")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$754, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$755, DW_AT_name("last_message")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$100

$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$41	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$41, DW_AT_address_class(0x16)

$C$DW$T$167	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$167, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$167, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$167, DW_AT_byte_size(0x38)
$C$DW$756	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$756, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$167


$C$DW$T$102	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$102, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$102, DW_AT_byte_size(0x14)
$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$757, DW_AT_name("nodeId")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$758, DW_AT_name("whoami")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$759, DW_AT_name("state")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$760, DW_AT_name("toggle")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$761, DW_AT_name("abortCode")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$762, DW_AT_name("index")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$763, DW_AT_name("subIndex")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$764, DW_AT_name("port")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$765, DW_AT_name("count")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$766, DW_AT_name("offset")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$767	.dwtag  DW_TAG_member
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$767, DW_AT_name("datap")
	.dwattr $C$DW$767, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$767, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$768, DW_AT_name("dataType")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$769, DW_AT_name("timer")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$770, DW_AT_name("Callback")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$102

$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)

$C$DW$T$53	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x3c)
$C$DW$771	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$771, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$53


$C$DW$T$106	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$106, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x04)
$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$772, DW_AT_name("pSubindex")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$773, DW_AT_name("bSubCount")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$774, DW_AT_name("index")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$106

$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$775	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$37)
$C$DW$T$38	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$775)
$C$DW$T$39	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x16)

$C$DW$T$82	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)
$C$DW$776	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$9)
$C$DW$777	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$65)
$C$DW$778	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$81)
	.dwendtag $C$DW$T$82

$C$DW$T$83	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_address_class(0x16)
$C$DW$T$84	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)

$C$DW$T$168	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$168, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$168, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$168, DW_AT_byte_size(0x11c)
$C$DW$779	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$779, DW_AT_upper_bound(0x46)
	.dwendtag $C$DW$T$168


$C$DW$T$107	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$107, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$107, DW_AT_byte_size(0x08)
$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$780, DW_AT_name("bAccessType")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$781, DW_AT_name("bDataType")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$782, DW_AT_name("size")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$783, DW_AT_name("pObject")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$784, DW_AT_name("bProcessor")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$107

$C$DW$785	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$107)
$C$DW$T$103	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$785)
$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)

$C$DW$T$169	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$169, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$169, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$169, DW_AT_byte_size(0x08)
$C$DW$786	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$786, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$169


$C$DW$T$170	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$170, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$170, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$170, DW_AT_byte_size(0x10)
$C$DW$787	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$787, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$170


$C$DW$T$171	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$171, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$171, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$171, DW_AT_byte_size(0x28)
$C$DW$788	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$788, DW_AT_upper_bound(0x04)
	.dwendtag $C$DW$T$171


$C$DW$T$172	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$172, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$172, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$172, DW_AT_byte_size(0x18)
$C$DW$789	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$789, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$172


$C$DW$T$173	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$173, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$173, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$173, DW_AT_byte_size(0x20)
$C$DW$790	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$790, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$173


$C$DW$T$174	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$174, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$174, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$174, DW_AT_byte_size(0x30)
$C$DW$791	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$791, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$174


$C$DW$T$175	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$175, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$175, DW_AT_byte_size(0x40)
$C$DW$792	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$792, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$175


$C$DW$T$176	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$176, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$176, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$176, DW_AT_byte_size(0x48)
$C$DW$793	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$793, DW_AT_upper_bound(0x08)
	.dwendtag $C$DW$T$176


$C$DW$T$177	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$177, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$177, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$177, DW_AT_byte_size(0x68)
$C$DW$794	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$794, DW_AT_upper_bound(0x0c)
	.dwendtag $C$DW$T$177


$C$DW$T$178	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$178, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$178, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$178, DW_AT_byte_size(0x80)
$C$DW$795	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$795, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$178


$C$DW$T$179	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$179, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$179, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$179, DW_AT_byte_size(0xf8)
$C$DW$796	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$796, DW_AT_upper_bound(0x1e)
	.dwendtag $C$DW$T$179


$C$DW$T$180	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$180, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$180, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$180, DW_AT_byte_size(0x38)
$C$DW$797	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$797, DW_AT_upper_bound(0x06)
	.dwendtag $C$DW$T$180


$C$DW$T$181	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$181, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$181, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$181, DW_AT_byte_size(0x58)
$C$DW$798	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$798, DW_AT_upper_bound(0x0a)
	.dwendtag $C$DW$T$181


$C$DW$T$182	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$182, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$182, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$182, DW_AT_byte_size(0xa0)
$C$DW$799	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$799, DW_AT_upper_bound(0x13)
	.dwendtag $C$DW$T$182


$C$DW$T$183	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$183, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$183, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$183, DW_AT_byte_size(0xc0)
$C$DW$800	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$800, DW_AT_upper_bound(0x17)
	.dwendtag $C$DW$T$183


$C$DW$T$184	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$184, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$184, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$184, DW_AT_byte_size(0x88)
$C$DW$801	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$801, DW_AT_upper_bound(0x10)
	.dwendtag $C$DW$T$184

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$802	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$802, DW_AT_location[DW_OP_reg0]
$C$DW$803	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$803, DW_AT_location[DW_OP_reg1]
$C$DW$804	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$804, DW_AT_location[DW_OP_reg2]
$C$DW$805	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$805, DW_AT_location[DW_OP_reg3]
$C$DW$806	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$806, DW_AT_location[DW_OP_reg20]
$C$DW$807	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$807, DW_AT_location[DW_OP_reg21]
$C$DW$808	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$808, DW_AT_location[DW_OP_reg22]
$C$DW$809	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$809, DW_AT_location[DW_OP_reg23]
$C$DW$810	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$810, DW_AT_location[DW_OP_reg24]
$C$DW$811	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$811, DW_AT_location[DW_OP_reg25]
$C$DW$812	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$812, DW_AT_location[DW_OP_reg26]
$C$DW$813	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$813, DW_AT_location[DW_OP_reg28]
$C$DW$814	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$814, DW_AT_location[DW_OP_reg29]
$C$DW$815	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$815, DW_AT_location[DW_OP_reg30]
$C$DW$816	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$816, DW_AT_location[DW_OP_reg31]
$C$DW$817	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$817, DW_AT_location[DW_OP_regx 0x20]
$C$DW$818	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$818, DW_AT_location[DW_OP_regx 0x21]
$C$DW$819	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$819, DW_AT_location[DW_OP_regx 0x22]
$C$DW$820	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$820, DW_AT_location[DW_OP_regx 0x23]
$C$DW$821	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$821, DW_AT_location[DW_OP_regx 0x24]
$C$DW$822	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$822, DW_AT_location[DW_OP_regx 0x25]
$C$DW$823	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$823, DW_AT_location[DW_OP_regx 0x26]
$C$DW$824	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$824, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$825	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$825, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$826	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$826, DW_AT_location[DW_OP_reg4]
$C$DW$827	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$827, DW_AT_location[DW_OP_reg6]
$C$DW$828	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$828, DW_AT_location[DW_OP_reg8]
$C$DW$829	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$829, DW_AT_location[DW_OP_reg10]
$C$DW$830	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$830, DW_AT_location[DW_OP_reg12]
$C$DW$831	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$831, DW_AT_location[DW_OP_reg14]
$C$DW$832	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$832, DW_AT_location[DW_OP_reg16]
$C$DW$833	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$833, DW_AT_location[DW_OP_reg17]
$C$DW$834	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$834, DW_AT_location[DW_OP_reg18]
$C$DW$835	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$835, DW_AT_location[DW_OP_reg19]
$C$DW$836	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$836, DW_AT_location[DW_OP_reg5]
$C$DW$837	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$837, DW_AT_location[DW_OP_reg7]
$C$DW$838	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$838, DW_AT_location[DW_OP_reg9]
$C$DW$839	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$839, DW_AT_location[DW_OP_reg11]
$C$DW$840	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$840, DW_AT_location[DW_OP_reg13]
$C$DW$841	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$841, DW_AT_location[DW_OP_reg15]
$C$DW$842	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$842, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$843	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$843, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$844	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$844, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$845	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$845, DW_AT_location[DW_OP_regx 0x30]
$C$DW$846	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$846, DW_AT_location[DW_OP_regx 0x33]
$C$DW$847	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$847, DW_AT_location[DW_OP_regx 0x34]
$C$DW$848	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$848, DW_AT_location[DW_OP_regx 0x37]
$C$DW$849	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$849, DW_AT_location[DW_OP_regx 0x38]
$C$DW$850	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$850, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$851	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$851, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$852	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$852, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$853	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$853, DW_AT_location[DW_OP_regx 0x40]
$C$DW$854	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$854, DW_AT_location[DW_OP_regx 0x43]
$C$DW$855	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$855, DW_AT_location[DW_OP_regx 0x44]
$C$DW$856	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$856, DW_AT_location[DW_OP_regx 0x47]
$C$DW$857	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$857, DW_AT_location[DW_OP_regx 0x48]
$C$DW$858	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$858, DW_AT_location[DW_OP_regx 0x49]
$C$DW$859	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$859, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$860	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$860, DW_AT_location[DW_OP_regx 0x27]
$C$DW$861	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$861, DW_AT_location[DW_OP_regx 0x28]
$C$DW$862	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$862, DW_AT_location[DW_OP_reg27]
$C$DW$863	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$863, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

