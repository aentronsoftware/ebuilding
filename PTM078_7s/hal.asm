;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue Feb 02 11:43:29 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../hal.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\R_Module_48V_Standard\PTM078_7s")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ErrorPinOn+0,32
	.bits	0,16			; _ErrorPinOn @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ErrorCounter+0,32
	.bits	0,16			; _ErrorCounter @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_BlinkCounter+0,32
	.bits	0,16			; _BlinkCounter @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_HAL_Enable+0,32
	.bits	0,16			; _HAL_Enable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Temp_Module_Voltage+0,32
	.bits	0,16			; _Temp_Module_Voltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_HAL_RelayState+0,32
	.bits	15,16			; _HAL_RelayState @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_MeasureCounter+0,32
	.bits	16,16			; _MeasureCounter @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Temparature_Delay+0,32
	.bits	0,16			; _Temparature_Delay @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_RelayTimeOut+0,32
	.bits	0,16			; _RelayTimeOut @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_HAL_TimeSOC+0,32
	.bits	0,16			; _HAL_TimeSOC @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_RelayTimer+0,32
	.bits	0,16			; _RelayTimer @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_HAL_CellOK+0,32
	.bits	2,16			; _HAL_CellOK @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_HAL_DoorClosed+0,32
	.bits	0,16			; _HAL_DoorClosed @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_PositionCounter+0,32
	.bits	200,16			; _PositionCounter @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_UpdateTimer+0,32
	.bits	0,16			; _UpdateTimer @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_HAL_NewCurPoint+0,32
	.bits	0,16			; _HAL_NewCurPoint @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_HAL_Current_Sum+0,32
	.bits	0,32			; _HAL_Current_Sum @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_HAL_LastPosition+0,32
	.bits	0,32			; _HAL_LastPosition @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_voltage_counter1+0,32
	.bits	0,32			; _GV_voltage_counter1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_HAL_Position+0,32
	.bits	0,32			; _HAL_Position @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_voltage_counter+0,32
	.bits	0,32			; _GV_voltage_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_Temp_counter+0,32
	.bits	0,32			; _GV_Temp_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_Blinking_counter+0,32
	.bits	0,32			; _GV_Blinking_counter @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("srand")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_srand")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$11)
	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_SetError")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_ERR_SetError")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$3


$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_Update")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_SCI1_Update")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external

$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverVoltage")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external

$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_HandleWarning")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_ERR_HandleWarning")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$7


$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("REC_Record")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_REC_Record")
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverTemp")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_ERR_ErrorOverTemp")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external

$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorComm")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_ERR_ErrorComm")
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external

$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorDigOvld")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ERR_ErrorDigOvld")
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_Delay")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_ODP_CommError_Delay")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external
$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_Deactive_FF")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_ODP_Board_Deactive_FF")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_TimeOut")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_ODP_CommError_TimeOut")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Cycles")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_ODP_Battery_Cycles")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Voltage")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_ODV_Module1_Voltage")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_SOC")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_ODV_Module1_SOC")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Temperature")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_ODV_Module1_Temperature")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Current")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_ODV_Module1_Current")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Battery_Total_ChargedkWh")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_ODV_Battery_Total_ChargedkWh")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Battery_Total_Cycles")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_ODV_Battery_Total_Cycles")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SOC_SOC2")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_ODV_SOC_SOC2")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$23, DW_AT_declaration
	.dwattr $C$DW$23, DW_AT_external
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SOC_SOC1")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_ODV_SOC_SOC1")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$24, DW_AT_declaration
	.dwattr $C$DW$24, DW_AT_external
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Slope")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_ODP_Contactor_Slope")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Undercurrent")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_ODP_Contactor_Undercurrent")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$26, DW_AT_declaration
	.dwattr $C$DW$26, DW_AT_external
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Overcurrent")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_ODP_Contactor_Overcurrent")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Setup")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ODP_Contactor_Setup")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Hold")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_ODP_Contactor_Hold")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_DischargeAllowed")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_ODV_Current_DischargeAllowed")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$30, DW_AT_declaration
	.dwattr $C$DW$30, DW_AT_external
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Heater_Heater_Status")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_ODV_Heater_Heater_Status")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$31, DW_AT_declaration
	.dwattr $C$DW$31, DW_AT_external
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineMode")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_ODV_MachineMode")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$32, DW_AT_declaration
	.dwattr $C$DW$32, DW_AT_external
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Heater_Off_Cell_Voltage")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_ODP_Heater_Off_Cell_Voltage")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Heater_OnTemp")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ODP_Heater_OnTemp")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Heater_OffTemp")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_ODP_Heater_OffTemp")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Contactor_Delay")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_ODP_Contactor_Delay")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
	.global	_ErrorPinOn
_ErrorPinOn:	.usect	".ebss",1,1,0
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("ErrorPinOn")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ErrorPinOn")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_addr _ErrorPinOn]
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$37, DW_AT_external
	.global	_ErrorCounter
_ErrorCounter:	.usect	".ebss",1,1,0
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("ErrorCounter")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_ErrorCounter")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_addr _ErrorCounter]
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$38, DW_AT_external
	.global	_BlinkCounter
_BlinkCounter:	.usect	".ebss",1,1,0
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("BlinkCounter")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_BlinkCounter")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_addr _BlinkCounter]
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$39, DW_AT_external
	.global	_HAL_Enable
_HAL_Enable:	.usect	".ebss",1,1,0
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("HAL_Enable")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_HAL_Enable")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_addr _HAL_Enable]
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$40, DW_AT_external
	.global	_Temp_Module_Voltage
_Temp_Module_Voltage:	.usect	".ebss",1,1,0
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("Temp_Module_Voltage")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_Temp_Module_Voltage")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_addr _Temp_Module_Voltage]
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$41, DW_AT_external
	.global	_HAL_RelayState
_HAL_RelayState:	.usect	".ebss",1,1,0
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("HAL_RelayState")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_HAL_RelayState")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_addr _HAL_RelayState]
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$42, DW_AT_external
	.global	_OldVal
_OldVal:	.usect	".ebss",1,1,0
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("OldVal")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_OldVal")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_addr _OldVal]
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$43, DW_AT_external
	.global	_MeasureCounter
_MeasureCounter:	.usect	".ebss",1,1,0
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("MeasureCounter")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_MeasureCounter")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_addr _MeasureCounter]
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$44, DW_AT_external
	.global	_Temparature_Delay
_Temparature_Delay:	.usect	".ebss",1,1,0
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("Temparature_Delay")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_Temparature_Delay")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_addr _Temparature_Delay]
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$45, DW_AT_external
	.global	_RelayTimeOut
_RelayTimeOut:	.usect	".ebss",1,1,0
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("RelayTimeOut")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_RelayTimeOut")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_addr _RelayTimeOut]
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$46, DW_AT_external
	.global	_HAL_TimeSOC
_HAL_TimeSOC:	.usect	".ebss",1,1,0
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("HAL_TimeSOC")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_HAL_TimeSOC")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_addr _HAL_TimeSOC]
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$47, DW_AT_external
	.global	_RelayTimer
_RelayTimer:	.usect	".ebss",1,1,0
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("RelayTimer")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_RelayTimer")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_addr _RelayTimer]
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$48, DW_AT_external
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Time_Remaining")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_ODV_Module1_Time_Remaining")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Module_SOC_Current")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_ODV_Module1_Module_SOC_Current")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Capacity_Used")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_ODV_Module1_Capacity_Used")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$51, DW_AT_declaration
	.dwattr $C$DW$51, DW_AT_external
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Capacity_set")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_ODV_Module1_Capacity_set")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_SOH")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_ODV_Module1_SOH")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Throughput")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_ODV_Module1_Throughput")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$54, DW_AT_declaration
	.dwattr $C$DW$54, DW_AT_external
	.global	_HAL_CellOK
_HAL_CellOK:	.usect	".ebss",1,1,0
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("HAL_CellOK")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_HAL_CellOK")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_addr _HAL_CellOK]
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$55, DW_AT_external
	.global	_HAL_DoorClosed
_HAL_DoorClosed:	.usect	".ebss",1,1,0
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("HAL_DoorClosed")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_HAL_DoorClosed")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_addr _HAL_DoorClosed]
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$56, DW_AT_external
	.global	_PositionCounter
_PositionCounter:	.usect	".ebss",1,1,0
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("PositionCounter")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_PositionCounter")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_addr _PositionCounter]
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$57, DW_AT_external
	.global	_UpdateTimer
_UpdateTimer:	.usect	".ebss",1,1,0
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("UpdateTimer")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_UpdateTimer")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_addr _UpdateTimer]
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$58, DW_AT_external
	.global	_HAL_NewCurPoint
_HAL_NewCurPoint:	.usect	".ebss",1,1,0
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("HAL_NewCurPoint")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_HAL_NewCurPoint")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_addr _HAL_NewCurPoint]
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$59, DW_AT_external
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$60, DW_AT_declaration
	.dwattr $C$DW$60, DW_AT_external

$C$DW$61	.dwtag  DW_TAG_subprogram, DW_AT_name("rand")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_rand")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$61, DW_AT_declaration
	.dwattr $C$DW$61, DW_AT_external
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$62, DW_AT_declaration
	.dwattr $C$DW$62, DW_AT_external
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$63, DW_AT_declaration
	.dwattr $C$DW$63, DW_AT_external
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Imax_charge")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Imax_charge")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$64, DW_AT_declaration
	.dwattr $C$DW$64, DW_AT_external
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$65, DW_AT_declaration
	.dwattr $C$DW$65, DW_AT_external
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Start")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ODV_Recorder_Start")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$66, DW_AT_declaration
	.dwattr $C$DW$66, DW_AT_external
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Status")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_ODV_Gateway_Status")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$67, DW_AT_declaration
	.dwattr $C$DW$67, DW_AT_external

$C$DW$68	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarning")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_ERR_ClearWarning")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$68, DW_AT_declaration
	.dwattr $C$DW$68, DW_AT_external
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Control")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ODV_Recorder_Control")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$260)
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_external

$C$DW$70	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$311)
	.dwattr $C$DW$70, DW_AT_declaration
	.dwattr $C$DW$70, DW_AT_external
$C$DW$71	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$285)
$C$DW$72	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$298)
$C$DW$73	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$123)
	.dwendtag $C$DW$70


$C$DW$74	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$311)
	.dwattr $C$DW$74, DW_AT_declaration
	.dwattr $C$DW$74, DW_AT_external
$C$DW$75	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$297)
$C$DW$76	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$123)
	.dwendtag $C$DW$74

$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Temp_Delay")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Temp_Delay")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$77, DW_AT_declaration
	.dwattr $C$DW$77, DW_AT_external
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_ChargeAllowed")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_ODV_Current_ChargeAllowed")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$78, DW_AT_declaration
	.dwattr $C$DW$78, DW_AT_external
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("SCI_GpioState")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_SCI_GpioState")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$79, DW_AT_declaration
	.dwattr $C$DW$79, DW_AT_external

$C$DW$80	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearError")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_ERR_ClearError")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$80, DW_AT_declaration
	.dwattr $C$DW$80, DW_AT_external
$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Temperature_Average_data")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_ODV_Temperature_Average_data")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$81, DW_AT_declaration
	.dwattr $C$DW$81, DW_AT_external
$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$82, DW_AT_declaration
	.dwattr $C$DW$82, DW_AT_external
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Cell_Nb")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Cell_Nb")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$83, DW_AT_declaration
	.dwattr $C$DW$83, DW_AT_external
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$84, DW_AT_declaration
	.dwattr $C$DW$84, DW_AT_external
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Imax_dis")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Imax_dis")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$85, DW_AT_declaration
	.dwattr $C$DW$85, DW_AT_external
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$86, DW_AT_declaration
	.dwattr $C$DW$86, DW_AT_external
	.global	_HAL_Current_Sum
_HAL_Current_Sum:	.usect	".ebss",2,1,1
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("HAL_Current_Sum")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_HAL_Current_Sum")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_addr _HAL_Current_Sum]
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$87, DW_AT_external
	.global	_HAL_LastPosition
_HAL_LastPosition:	.usect	".ebss",2,1,1
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("HAL_LastPosition")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_HAL_LastPosition")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_addr _HAL_LastPosition]
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$88, DW_AT_external
	.global	_GV_voltage_counter1
_GV_voltage_counter1:	.usect	".ebss",2,1,1
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("GV_voltage_counter1")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_GV_voltage_counter1")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_addr _GV_voltage_counter1]
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$89, DW_AT_external
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Total")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_PAR_Capacity_Total")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$90, DW_AT_declaration
	.dwattr $C$DW$90, DW_AT_external
	.global	_HAL_Position
_HAL_Position:	.usect	".ebss",2,1,1
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("HAL_Position")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_HAL_Position")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_addr _HAL_Position]
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$91, DW_AT_external
	.global	_GV_voltage_counter
_GV_voltage_counter:	.usect	".ebss",2,1,1
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("GV_voltage_counter")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_GV_voltage_counter")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_addr _GV_voltage_counter]
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$92, DW_AT_external
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("CommTimeout")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_CommTimeout")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$93, DW_AT_declaration
	.dwattr $C$DW$93, DW_AT_external

$C$DW$94	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_StoreODSubIndex")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$94, DW_AT_declaration
	.dwattr $C$DW$94, DW_AT_external
$C$DW$95	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$210)
$C$DW$96	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$9)
$C$DW$97	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$94


$C$DW$98	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteStatisticParam")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$98, DW_AT_declaration
	.dwattr $C$DW$98, DW_AT_external
$C$DW$99	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$8)
	.dwendtag $C$DW$98


$C$DW$100	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_Round")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_CNV_Round")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$100, DW_AT_declaration
	.dwattr $C$DW$100, DW_AT_external
$C$DW$101	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$100

	.global	_GV_Temp_counter
_GV_Temp_counter:	.usect	".ebss",2,1,1
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("GV_Temp_counter")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_GV_Temp_counter")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_addr _GV_Temp_counter]
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$102, DW_AT_external
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Left")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_PAR_Capacity_Left")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$103, DW_AT_declaration
	.dwattr $C$DW$103, DW_AT_external
	.global	_MMSConfig
_MMSConfig:	.usect	".ebss",2,1,1
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("MMSConfig")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_MMSConfig")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_addr _MMSConfig]
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$266)
	.dwattr $C$DW$104, DW_AT_external
	.global	_GV_Blinking_counter
_GV_Blinking_counter:	.usect	".ebss",2,1,1
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("GV_Blinking_counter")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_GV_Blinking_counter")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_addr _GV_Blinking_counter]
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$105, DW_AT_external
$C$DW$106	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SysTick_ms")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_ODV_SysTick_ms")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$106, DW_AT_declaration
	.dwattr $C$DW$106, DW_AT_external
$C$DW$107	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Capacity")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_ODP_Battery_Capacity")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$107, DW_AT_declaration
	.dwattr $C$DW$107, DW_AT_external
$C$DW$108	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RandomNB")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_ODP_RandomNB")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$108, DW_AT_declaration
	.dwattr $C$DW$108, DW_AT_external
$C$DW$109	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Read_Inputs_16_Bit")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_ODV_Read_Inputs_16_Bit")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$109, DW_AT_declaration
	.dwattr $C$DW$109, DW_AT_external
$C$DW$110	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$110, DW_AT_declaration
	.dwattr $C$DW$110, DW_AT_external
$C$DW$111	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Password")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_ODP_Password")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$111, DW_AT_declaration
	.dwattr $C$DW$111, DW_AT_external
$C$DW$112	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Period")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_ODV_Recorder_Period")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$112, DW_AT_declaration
	.dwattr $C$DW$112, DW_AT_external
$C$DW$113	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$210)
	.dwattr $C$DW$113, DW_AT_declaration
	.dwattr $C$DW$113, DW_AT_external
$C$DW$114	.dwtag  DW_TAG_variable, DW_AT_name("ODP_OnTime")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_ODP_OnTime")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$114, DW_AT_declaration
	.dwattr $C$DW$114, DW_AT_external
$C$DW$115	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$115, DW_AT_declaration
	.dwattr $C$DW$115, DW_AT_external
$C$DW$116	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Security")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_ODV_Security")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$116, DW_AT_declaration
	.dwattr $C$DW$116, DW_AT_external
$C$DW$117	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$117, DW_AT_declaration
	.dwattr $C$DW$117, DW_AT_external
$C$DW$118	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Vectors")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_ODV_Recorder_Vectors")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$118, DW_AT_declaration
	.dwattr $C$DW$118, DW_AT_external
$C$DW$119	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_DischargeLimits")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_ODP_Temperature_DischargeLimits")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$303)
	.dwattr $C$DW$119, DW_AT_declaration
	.dwattr $C$DW$119, DW_AT_external
$C$DW$120	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Power_DischargeLimits")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_ODP_Power_DischargeLimits")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$120, DW_AT_declaration
	.dwattr $C$DW$120, DW_AT_external
$C$DW$121	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_ChargeLimits")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_ODP_Temperature_ChargeLimits")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$303)
	.dwattr $C$DW$121, DW_AT_declaration
	.dwattr $C$DW$121, DW_AT_external
$C$DW$122	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Power_ChargeLimits")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_ODP_Power_ChargeLimits")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$122, DW_AT_declaration
	.dwattr $C$DW$122, DW_AT_external
	.global	_permtable
	.sect	".econst:_permtable"
	.clink
	.align	1
_permtable:
	.bits	1,16			; _permtable[0] @ 0
	.bits	3,16			; _permtable[1] @ 16
	.bits	5,16			; _permtable[2] @ 32
	.bits	7,16			; _permtable[3] @ 48
	.bits	11,16			; _permtable[4] @ 64
	.bits	13,16			; _permtable[5] @ 80
	.bits	17,16			; _permtable[6] @ 96
	.bits	19,16			; _permtable[7] @ 112
	.bits	23,16			; _permtable[8] @ 128
	.bits	29,16			; _permtable[9] @ 144
	.bits	31,16			; _permtable[10] @ 160

$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("permtable")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_permtable")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_addr _permtable]
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$316)
	.dwattr $C$DW$123, DW_AT_external
$C$DW$124	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$124, DW_AT_declaration
	.dwattr $C$DW$124, DW_AT_external
$C$DW$125	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Analogue_Output_16_Bit")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_ODV_Write_Analogue_Output_16_Bit")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$309)
	.dwattr $C$DW$125, DW_AT_declaration
	.dwattr $C$DW$125, DW_AT_external
$C$DW$126	.dwtag  DW_TAG_variable, DW_AT_name("AdcResult")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_AdcResult")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$270)
	.dwattr $C$DW$126, DW_AT_declaration
	.dwattr $C$DW$126, DW_AT_external
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$308)
	.dwattr $C$DW$127, DW_AT_declaration
	.dwattr $C$DW$127, DW_AT_external
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$290)
	.dwattr $C$DW$128, DW_AT_declaration
	.dwattr $C$DW$128, DW_AT_external
$C$DW$129	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Output_Scaling_Float")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_ODP_Analogue_Output_Scaling_Float")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$327)
	.dwattr $C$DW$129, DW_AT_declaration
	.dwattr $C$DW$129, DW_AT_external
$C$DW$130	.dwtag  DW_TAG_variable, DW_AT_name("GpioDataRegs")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_GpioDataRegs")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$277)
	.dwattr $C$DW$130, DW_AT_declaration
	.dwattr $C$DW$130, DW_AT_external
$C$DW$131	.dwtag  DW_TAG_variable, DW_AT_name("EQep1Regs")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_EQep1Regs")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$274)
	.dwattr $C$DW$131, DW_AT_declaration
	.dwattr $C$DW$131, DW_AT_external
$C$DW$132	.dwtag  DW_TAG_variable, DW_AT_name("sci_rx_mbox")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_sci_rx_mbox")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$282)
	.dwattr $C$DW$132, DW_AT_declaration
	.dwattr $C$DW$132, DW_AT_external
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("EPwm2Regs")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_EPwm2Regs")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$273)
	.dwattr $C$DW$133, DW_AT_declaration
	.dwattr $C$DW$133, DW_AT_external
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("EPwm3Regs")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_EPwm3Regs")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$273)
	.dwattr $C$DW$134, DW_AT_declaration
	.dwattr $C$DW$134, DW_AT_external
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("AdcRegs")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_AdcRegs")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$269)
	.dwattr $C$DW$135, DW_AT_declaration
	.dwattr $C$DW$135, DW_AT_external
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("TempTable")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_TempTable")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$263)
	.dwattr $C$DW$136, DW_AT_declaration
	.dwattr $C$DW$136, DW_AT_external
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("PieVectTable")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_PieVectTable")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$137, DW_AT_declaration
	.dwattr $C$DW$137, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0222812 
	.sect	".text"
	.global	_HAL_Init

$C$DW$138	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Init")
	.dwattr $C$DW$138, DW_AT_low_pc(_HAL_Init)
	.dwattr $C$DW$138, DW_AT_high_pc(0x00)
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_HAL_Init")
	.dwattr $C$DW$138, DW_AT_external
	.dwattr $C$DW$138, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$138, DW_AT_TI_begin_line(0x35)
	.dwattr $C$DW$138, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$138, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../hal.c",line 53,column 20,is_stmt,address _HAL_Init

	.dwfde $C$DW$CIE, _HAL_Init

;***************************************************************
;* FNAME: _HAL_Init                     FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_HAL_Init:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../hal.c",line 55,column 1,is_stmt
$C$DW$139	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$139, DW_AT_low_pc(0x00)
	.dwattr $C$DW$139, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$138, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$138, DW_AT_TI_end_line(0x37)
	.dwattr $C$DW$138, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$138

	.sect	".text:retain"
	.global	_millisecint

$C$DW$140	.dwtag  DW_TAG_subprogram, DW_AT_name("millisecint")
	.dwattr $C$DW$140, DW_AT_low_pc(_millisecint)
	.dwattr $C$DW$140, DW_AT_high_pc(0x00)
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_millisecint")
	.dwattr $C$DW$140, DW_AT_external
	.dwattr $C$DW$140, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$140, DW_AT_TI_begin_line(0x50)
	.dwattr $C$DW$140, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$140, DW_AT_TI_interrupt
	.dwattr $C$DW$140, DW_AT_TI_max_frame_size(-30)
	.dwpsn	file "../hal.c",line 81,column 1,is_stmt,address _millisecint

	.dwfde $C$DW$CIE, _millisecint

;***************************************************************
;* FNAME: _millisecint                  FR SIZE:  28           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto, 24 SOE     *
;***************************************************************

_millisecint:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        PUSH      AR1H:AR0H             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 5, 4
	.dwcfi	save_reg_to_mem, 7, 5
	.dwcfi	cfa_offset, -6
        MOVL      *SP++,XT              ; [CPU_] 
	.dwcfi	save_reg_to_mem, 21, 6
	.dwcfi	save_reg_to_mem, 22, 7
	.dwcfi	cfa_offset, -8
        MOVL      *SP++,XAR4            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 12, 8
	.dwcfi	save_reg_to_mem, 13, 9
	.dwcfi	cfa_offset, -10
        MOVL      *SP++,XAR5            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 14, 10
	.dwcfi	save_reg_to_mem, 15, 11
	.dwcfi	cfa_offset, -12
        MOVL      *SP++,XAR6            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 16, 12
	.dwcfi	save_reg_to_mem, 17, 13
	.dwcfi	cfa_offset, -14
        MOVL      *SP++,XAR7            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 18, 14
	.dwcfi	save_reg_to_mem, 19, 15
	.dwcfi	cfa_offset, -16
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 16
	.dwcfi	save_reg_to_mem, 40, 17
	.dwcfi	cfa_offset, -18
        MOV32     *SP++,R0H             ; [CPU_] 
        MOV32     *SP++,R1H             ; [CPU_] 
        MOV32     *SP++,R2H             ; [CPU_] 
        MOV32     *SP++,R3H             ; [CPU_] 
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -30
        SPM       0                     ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
$C$DW$141	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$141, DW_AT_location[DW_OP_breg20 -1]
$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("newval")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_newval")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$142, DW_AT_location[DW_OP_breg20 -2]
$C$DW$143	.dwtag  DW_TAG_variable, DW_AT_name("voltsoc")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_voltsoc")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_breg20 -3]
	.dwpsn	file "../hal.c",line 84,column 3,is_stmt
 clrc INTM
	.dwpsn	file "../hal.c",line 85,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        MOVB      @_PieCtrlRegs+1,#16,UNC ; [CPU_] |85| 
	.dwpsn	file "../hal.c",line 86,column 3,is_stmt
        MOV       AL,#1025              ; [CPU_] |86| 
        MOV       IER,AL                ; [CPU_] |86| 
 clrc INTM
	.dwpsn	file "../hal.c",line 89,column 18,is_stmt
        MOVW      DP,#_ODV_Module1_Voltage ; [CPU_U] 
        MOVIZ     R1H,#16672            ; [CPU_] |89| 
        SPM       #0                    ; [CPU_] 
        UI16TOF32 R0H,@_ODV_Module1_Voltage ; [CPU_] |89| 
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$144, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |89| 
        ; call occurs [#FS$$DIV] ; [] |89| 
        MOVW      DP,#_ODP_SafetyLimits_Cell_Nb ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_SafetyLimits_Cell_Nb ; [CPU_] |89| 
$C$DW$145	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$145, DW_AT_low_pc(0x00)
	.dwattr $C$DW$145, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$145, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |89| 
        ; call occurs [#FS$$DIV] ; [] |89| 
$C$DW$146	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$146, DW_AT_low_pc(0x00)
	.dwattr $C$DW$146, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$146, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |89| 
        ; call occurs [#_CNV_Round] ; [] |89| 
        MOV       *-SP[3],AL            ; [CPU_] |89| 
	.dwpsn	file "../hal.c",line 91,column 4,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       @_GpioDataRegs+1,#0xfffe ; [CPU_] |91| 
	.dwpsn	file "../hal.c",line 92,column 4,is_stmt
        AND       @_GpioDataRegs+1,#0xfffd ; [CPU_] |92| 
	.dwpsn	file "../hal.c",line 93,column 5,is_stmt
        AND       @_GpioDataRegs+1,#0xfffb ; [CPU_] |93| 
	.dwpsn	file "../hal.c",line 95,column 5,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |95| 
        BF        $C$L2,EQ              ; [CPU_] |95| 
        ; branchcc occurs ; [] |95| 
	.dwpsn	file "../hal.c",line 98,column 9,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       @_GpioDataRegs+1,#0xfff7 ; [CPU_] |98| 
	.dwpsn	file "../hal.c",line 100,column 10,is_stmt
        MOVB      ACC,#1                ; [CPU_] |100| 
        MOVW      DP,#_GV_Blinking_counter ; [CPU_U] 
        ADDL      @_GV_Blinking_counter,ACC ; [CPU_] |100| 
	.dwpsn	file "../hal.c",line 101,column 10,is_stmt
        MOVB      ACC,#200              ; [CPU_] |101| 
        CMPL      ACC,@_GV_Blinking_counter ; [CPU_] |101| 
        B         $C$L7,HIS             ; [CPU_] |101| 
        ; branchcc occurs ; [] |101| 
	.dwpsn	file "../hal.c",line 103,column 10,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       AL,@_GpioDataRegs+1,#0x1000 ; [CPU_] |103| 
        LSR       AL,12                 ; [CPU_] |103| 
        CMPB      AL,#1                 ; [CPU_] |103| 
        BF        $C$L1,NEQ             ; [CPU_] |103| 
        ; branchcc occurs ; [] |103| 
	.dwpsn	file "../hal.c",line 105,column 12,is_stmt
        AND       @_GpioDataRegs+1,#0xefff ; [CPU_] |105| 
	.dwpsn	file "../hal.c",line 106,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |106| 
        MOVW      DP,#_GV_Blinking_counter ; [CPU_U] 
        MOVL      @_GV_Blinking_counter,ACC ; [CPU_] |106| 
	.dwpsn	file "../hal.c",line 107,column 13,is_stmt
        B         $C$L7,UNC             ; [CPU_] |107| 
        ; branch occurs ; [] |107| 
$C$L1:    
	.dwpsn	file "../hal.c",line 110,column 12,is_stmt
        OR        @_GpioDataRegs+1,#0x1000 ; [CPU_] |110| 
	.dwpsn	file "../hal.c",line 111,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |111| 
        MOVW      DP,#_GV_Blinking_counter ; [CPU_U] 
        MOVL      @_GV_Blinking_counter,ACC ; [CPU_] |111| 
	.dwpsn	file "../hal.c",line 114,column 9,is_stmt
        B         $C$L7,UNC             ; [CPU_] |114| 
        ; branch occurs ; [] |114| 
$C$L2:    
	.dwpsn	file "../hal.c",line 117,column 6,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |117| 
        CMP       AL,#440               ; [CPU_] |117| 
        B         $C$L4,GEQ             ; [CPU_] |117| 
        ; branchcc occurs ; [] |117| 
        CMP       AL,#400               ; [CPU_] |117| 
        B         $C$L4,LEQ             ; [CPU_] |117| 
        ; branchcc occurs ; [] |117| 
	.dwpsn	file "../hal.c",line 120,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       @_GpioDataRegs+1,#0xefff ; [CPU_] |120| 
	.dwpsn	file "../hal.c",line 122,column 12,is_stmt
        MOVB      ACC,#1                ; [CPU_] |122| 
        MOVW      DP,#_GV_Blinking_counter ; [CPU_U] 
        ADDL      @_GV_Blinking_counter,ACC ; [CPU_] |122| 
	.dwpsn	file "../hal.c",line 123,column 12,is_stmt
        MOVB      ACC,#200              ; [CPU_] |123| 
        CMPL      ACC,@_GV_Blinking_counter ; [CPU_] |123| 
        B         $C$L4,HIS             ; [CPU_] |123| 
        ; branchcc occurs ; [] |123| 
	.dwpsn	file "../hal.c",line 125,column 13,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       AL,@_GpioDataRegs+1,#0x0008 ; [CPU_] |125| 
        LSR       AL,3                  ; [CPU_] |125| 
        CMPB      AL,#1                 ; [CPU_] |125| 
        BF        $C$L3,NEQ             ; [CPU_] |125| 
        ; branchcc occurs ; [] |125| 
	.dwpsn	file "../hal.c",line 127,column 14,is_stmt
        AND       @_GpioDataRegs+1,#0xfff7 ; [CPU_] |127| 
	.dwpsn	file "../hal.c",line 128,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |128| 
        MOVW      DP,#_GV_Blinking_counter ; [CPU_U] 
        MOVL      @_GV_Blinking_counter,ACC ; [CPU_] |128| 
	.dwpsn	file "../hal.c",line 129,column 13,is_stmt
        B         $C$L4,UNC             ; [CPU_] |129| 
        ; branch occurs ; [] |129| 
$C$L3:    
	.dwpsn	file "../hal.c",line 132,column 14,is_stmt
        OR        @_GpioDataRegs+1,#0x0008 ; [CPU_] |132| 
	.dwpsn	file "../hal.c",line 133,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |133| 
        MOVW      DP,#_GV_Blinking_counter ; [CPU_U] 
        MOVL      @_GV_Blinking_counter,ACC ; [CPU_] |133| 
$C$L4:    
	.dwpsn	file "../hal.c",line 137,column 6,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |137| 
        CMP       AL,#330               ; [CPU_] |137| 
        B         $C$L5,GEQ             ; [CPU_] |137| 
        ; branchcc occurs ; [] |137| 
        CMP       AL,#270               ; [CPU_] |137| 
        B         $C$L5,LEQ             ; [CPU_] |137| 
        ; branchcc occurs ; [] |137| 
	.dwpsn	file "../hal.c",line 140,column 12,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x1000 ; [CPU_] |140| 
	.dwpsn	file "../hal.c",line 141,column 12,is_stmt
        AND       @_GpioDataRegs+1,#0xfff7 ; [CPU_] |141| 
$C$L5:    
	.dwpsn	file "../hal.c",line 143,column 7,is_stmt
        CMP       AL,#350               ; [CPU_] |143| 
        B         $C$L6,GEQ             ; [CPU_] |143| 
        ; branchcc occurs ; [] |143| 
        CMP       AL,#330               ; [CPU_] |143| 
        B         $C$L6,LEQ             ; [CPU_] |143| 
        ; branchcc occurs ; [] |143| 
	.dwpsn	file "../hal.c",line 146,column 8,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0008 ; [CPU_] |146| 
	.dwpsn	file "../hal.c",line 147,column 8,is_stmt
        OR        @_GpioDataRegs+1,#0x1000 ; [CPU_] |147| 
$C$L6:    
	.dwpsn	file "../hal.c",line 150,column 7,is_stmt
        CMP       AL,#400               ; [CPU_] |150| 
        B         $C$L7,GEQ             ; [CPU_] |150| 
        ; branchcc occurs ; [] |150| 
        CMP       AL,#350               ; [CPU_] |150| 
        B         $C$L7,LEQ             ; [CPU_] |150| 
        ; branchcc occurs ; [] |150| 
	.dwpsn	file "../hal.c",line 153,column 12,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0008 ; [CPU_] |153| 
	.dwpsn	file "../hal.c",line 154,column 12,is_stmt
        AND       @_GpioDataRegs+1,#0xefff ; [CPU_] |154| 
$C$L7:    
	.dwpsn	file "../hal.c",line 160,column 3,is_stmt
        MOVB      ACC,#1                ; [CPU_] |160| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        ADDL      @_ODV_SysTick_ms,ACC  ; [CPU_] |160| 
	.dwpsn	file "../hal.c",line 162,column 3,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |162| 
        MOVB      ACC,#0                ; [CPU_] |162| 
        MOVL      P,@_ODV_SysTick_ms    ; [CPU_] |162| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |162| 
        MOVL      XAR4,#999             ; [CPU_U] |162| 
        CMPL      ACC,XAR4              ; [CPU_] |162| 
        BF        $C$L8,NEQ             ; [CPU_] |162| 
        ; branchcc occurs ; [] |162| 
	.dwpsn	file "../hal.c",line 163,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |163| 
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        ADDL      @_ODP_OnTime,ACC      ; [CPU_] |163| 
	.dwpsn	file "../hal.c",line 164,column 5,is_stmt
        MOVW      DP,#_HAL_TimeSOC      ; [CPU_U] 
        INC       @_HAL_TimeSOC         ; [CPU_] |164| 
	.dwpsn	file "../hal.c",line 165,column 5,is_stmt
        INC       @_Temparature_Delay   ; [CPU_] |165| 
$C$L8:    
	.dwpsn	file "../hal.c",line 168,column 3,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Temp_Delay ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Temp_Delay ; [CPU_] |168| 
        MOVW      DP,#_Temparature_Delay ; [CPU_U] 
        LSR       AL,1                  ; [CPU_] |168| 
        CMP       AL,@_Temparature_Delay ; [CPU_] |168| 
        BF        $C$L9,NEQ             ; [CPU_] |168| 
        ; branchcc occurs ; [] |168| 
	.dwpsn	file "../hal.c",line 170,column 8,is_stmt
        MOVW      DP,#_ODV_Temperature_Average_data ; [CPU_U] 
        MOV       AL,@_ODV_Temperature_Average_data ; [CPU_] |170| 
        MOVW      DP,#_ODV_Module1_Temperature ; [CPU_U] 
        MOV       @_ODV_Module1_Temperature,AL ; [CPU_] |170| 
	.dwpsn	file "../hal.c",line 171,column 8,is_stmt
        MOVW      DP,#_Temparature_Delay ; [CPU_U] 
        MOV       @_Temparature_Delay,#0 ; [CPU_] |171| 
$C$L9:    
	.dwpsn	file "../hal.c",line 174,column 3,is_stmt
        MOVW      DP,#_ODP_CommError_TimeOut ; [CPU_U] 
        MOV       AL,@_ODP_CommError_TimeOut ; [CPU_] |174| 
        BF        $C$L10,EQ             ; [CPU_] |174| 
        ; branchcc occurs ; [] |174| 
	.dwpsn	file "../hal.c",line 175,column 5,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |175| 
        MOVW      DP,#_ODP_CommError_TimeOut ; [CPU_U] 
        MOVZ      AR6,@_ODP_CommError_TimeOut ; [CPU_] |175| 
        MOVW      DP,#_CommTimeout      ; [CPU_U] 
        SUBL      ACC,@_CommTimeout     ; [CPU_] |175| 
        CMPL      ACC,XAR6              ; [CPU_] |175| 
        B         $C$L10,LOS            ; [CPU_] |175| 
        ; branchcc occurs ; [] |175| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |175| 
        B         $C$L10,LEQ            ; [CPU_] |175| 
        ; branchcc occurs ; [] |175| 
        CMPB      AL,#6                 ; [CPU_] |175| 
        B         $C$L10,GEQ            ; [CPU_] |175| 
        ; branchcc occurs ; [] |175| 
	.dwpsn	file "../hal.c",line 177,column 7,is_stmt
$C$DW$147	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$147, DW_AT_low_pc(0x00)
	.dwattr $C$DW$147, DW_AT_name("_ERR_ErrorComm")
	.dwattr $C$DW$147, DW_AT_TI_call
        LCR       #_ERR_ErrorComm       ; [CPU_] |177| 
        ; call occurs [#_ERR_ErrorComm] ; [] |177| 
	.dwpsn	file "../hal.c",line 178,column 7,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#64,UNC ; [CPU_] |178| 
$C$L10:    
	.dwpsn	file "../hal.c",line 182,column 3,is_stmt
        MOVW      DP,#_EQep1Regs+26     ; [CPU_U] 
        OR        @_EQep1Regs+26,#0x0800 ; [CPU_] |182| 
	.dwpsn	file "../hal.c",line 183,column 3,is_stmt
        OR        @_EQep1Regs+26,#0x0001 ; [CPU_] |183| 
	.dwpsn	file "../hal.c",line 194,column 3,is_stmt
        MOVW      DP,#_RelayTimer       ; [CPU_U] 
        MOV       AL,@_RelayTimer       ; [CPU_] |194| 
        BF        $C$L13,EQ             ; [CPU_] |194| 
        ; branchcc occurs ; [] |194| 
	.dwpsn	file "../hal.c",line 196,column 4,is_stmt
        MOVW      DP,#_ODP_Board_Deactive_FF ; [CPU_U] 
        MOV       AL,@_ODP_Board_Deactive_FF ; [CPU_] |196| 
        CMPB      AL,#1                 ; [CPU_] |196| 
        BF        $C$L11,EQ             ; [CPU_] |196| 
        ; branchcc occurs ; [] |196| 
	.dwpsn	file "../hal.c",line 198,column 5,is_stmt
        MOVW      DP,#_RelayTimer       ; [CPU_U] 
        DEC       @_RelayTimer          ; [CPU_] |198| 
$C$L11:    
	.dwpsn	file "../hal.c",line 200,column 5,is_stmt
        MOVW      DP,#_RelayTimer       ; [CPU_U] 
        MOV       AL,@_RelayTimer       ; [CPU_] |200| 
        BF        $C$L12,NEQ            ; [CPU_] |200| 
        ; branchcc occurs ; [] |200| 
	.dwpsn	file "../hal.c",line 201,column 7,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |201| 
	.dwpsn	file "../hal.c",line 202,column 7,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        AND       @_ODV_Read_Inputs_16_Bit,#0xfffe ; [CPU_] |202| 
	.dwpsn	file "../hal.c",line 203,column 7,is_stmt
        MOVW      DP,#_EPwm2Regs+9      ; [CPU_U] 
        MOV       @_EPwm2Regs+9,#0      ; [CPU_] |203| 
	.dwpsn	file "../hal.c",line 204,column 7,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |204| 
	.dwpsn	file "../hal.c",line 205,column 7,is_stmt
        AND       @_GpioDataRegs,#0xffef ; [CPU_] |205| 
$C$L12:    
	.dwpsn	file "../hal.c",line 207,column 5,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       AL,@_GpioDataRegs+1,#0x0010 ; [CPU_] |207| 
        LSR       AL,4                  ; [CPU_] |207| 
        CMPB      AL,#1                 ; [CPU_] |207| 
        BF        $C$L13,NEQ            ; [CPU_] |207| 
        ; branchcc occurs ; [] |207| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |207| 
        BF        $C$L13,NEQ            ; [CPU_] |207| 
        ; branchcc occurs ; [] |207| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |207| 
        CMPB      AL,#128               ; [CPU_] |207| 
        BF        $C$L13,EQ             ; [CPU_] |207| 
        ; branchcc occurs ; [] |207| 
	.dwpsn	file "../hal.c",line 208,column 7,is_stmt
        MOVW      DP,#_ErrorCounter     ; [CPU_U] 
        MOVB      @_ErrorCounter,#1,UNC ; [CPU_] |208| 
	.dwpsn	file "../hal.c",line 209,column 7,is_stmt
        MOV       @_RelayTimer,#0       ; [CPU_] |209| 
	.dwpsn	file "../hal.c",line 210,column 7,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |210| 
	.dwpsn	file "../hal.c",line 211,column 7,is_stmt
$C$DW$148	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$148, DW_AT_low_pc(0x00)
	.dwattr $C$DW$148, DW_AT_name("_ERR_ClearError")
	.dwattr $C$DW$148, DW_AT_TI_call
        LCR       #_ERR_ClearError      ; [CPU_] |211| 
        ; call occurs [#_ERR_ClearError] ; [] |211| 
$C$L13:    
	.dwpsn	file "../hal.c",line 214,column 3,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        TBIT      @_GpioDataRegs+1,#4   ; [CPU_] |214| 
        BF        $C$L18,TC             ; [CPU_] |214| 
        ; branchcc occurs ; [] |214| 
        MOVW      DP,#_HAL_Enable       ; [CPU_U] 
        MOV       AL,@_HAL_Enable       ; [CPU_] |214| 
        BF        $C$L18,EQ             ; [CPU_] |214| 
        ; branchcc occurs ; [] |214| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |214| 
        BF        $C$L18,NEQ            ; [CPU_] |214| 
        ; branchcc occurs ; [] |214| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |214| 
        CMPB      AL,#128               ; [CPU_] |214| 
        BF        $C$L18,EQ             ; [CPU_] |214| 
        ; branchcc occurs ; [] |214| 
	.dwpsn	file "../hal.c",line 215,column 5,is_stmt
        MOVW      DP,#_ErrorPinOn       ; [CPU_U] 
        MOV       AL,@_ErrorPinOn       ; [CPU_] |215| 
        BF        $C$L14,NEQ            ; [CPU_] |215| 
        ; branchcc occurs ; [] |215| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |215| 
        BF        $C$L14,NEQ            ; [CPU_] |215| 
        ; branchcc occurs ; [] |215| 
	.dwpsn	file "../hal.c",line 216,column 7,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        OR        @_ODV_Read_Inputs_16_Bit,#0x0008 ; [CPU_] |216| 
	.dwpsn	file "../hal.c",line 219,column 7,is_stmt
        MOVW      DP,#_ODP_CommError_Delay ; [CPU_U] 
        MOV       T,@_ODP_CommError_Delay ; [CPU_] |219| 
        MPYB      ACC,T,#200            ; [CPU_] |219| 
        MOVW      DP,#_RelayTimer       ; [CPU_U] 
        MOV       @_RelayTimer,AL       ; [CPU_] |219| 
	.dwpsn	file "../hal.c",line 220,column 7,is_stmt
        MOVB      *-SP[1],#69,UNC       ; [CPU_] |220| 
	.dwpsn	file "../hal.c",line 221,column 7,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |221| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |221| 
        MOVB      AL,#0                 ; [CPU_] |221| 
        SUBB      XAR5,#1               ; [CPU_U] |221| 
$C$DW$149	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$149, DW_AT_low_pc(0x00)
	.dwattr $C$DW$149, DW_AT_name("_MBX_post")
	.dwattr $C$DW$149, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |221| 
        ; call occurs [#_MBX_post] ; [] |221| 
	.dwpsn	file "../hal.c",line 222,column 7,is_stmt
        MOVW      DP,#_ErrorCounter     ; [CPU_U] 
        MOVB      @_ErrorCounter,#5,UNC ; [CPU_] |222| 
	.dwpsn	file "../hal.c",line 223,column 7,is_stmt
        MOV       @_HAL_CellOK,#0       ; [CPU_] |223| 
	.dwpsn	file "../hal.c",line 224,column 7,is_stmt
        MOVB      @_ErrorPinOn,#1,UNC   ; [CPU_] |224| 
$C$L14:    
	.dwpsn	file "../hal.c",line 226,column 5,is_stmt
        MOVW      DP,#_ErrorCounter     ; [CPU_U] 
        MOV       AL,@_ErrorCounter     ; [CPU_] |226| 
        BF        $C$L15,EQ             ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
	.dwpsn	file "../hal.c",line 226,column 27,is_stmt
        DEC       @_ErrorCounter        ; [CPU_] |226| 
$C$L15:    
	.dwpsn	file "../hal.c",line 227,column 5,is_stmt
        MOV       AL,@_ErrorCounter     ; [CPU_] |227| 
        BF        $C$L20,NEQ            ; [CPU_] |227| 
        ; branchcc occurs ; [] |227| 
	.dwpsn	file "../hal.c",line 228,column 7,is_stmt
        MOV       AL,@_RelayTimer       ; [CPU_] |228| 
        CMPB      AL,#20                ; [CPU_] |228| 
        B         $C$L16,LOS            ; [CPU_] |228| 
        ; branchcc occurs ; [] |228| 
	.dwpsn	file "../hal.c",line 228,column 28,is_stmt
        MOVB      @_ErrorCounter,#20,UNC ; [CPU_] |228| 
        B         $C$L17,UNC            ; [CPU_] |228| 
        ; branch occurs ; [] |228| 
$C$L16:    
	.dwpsn	file "../hal.c",line 230,column 9,is_stmt
        MOV       @_ErrorCounter,#1000  ; [CPU_] |230| 
	.dwpsn	file "../hal.c",line 231,column 9,is_stmt
        MOVW      DP,#_ODP_Board_Deactive_FF ; [CPU_U] 
        MOV       AL,@_ODP_Board_Deactive_FF ; [CPU_] |231| 
        CMPB      AL,#1                 ; [CPU_] |231| 
        BF        $C$L17,EQ             ; [CPU_] |231| 
        ; branchcc occurs ; [] |231| 
	.dwpsn	file "../hal.c",line 233,column 10,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |233| 
        BF        $C$L17,NEQ            ; [CPU_] |233| 
        ; branchcc occurs ; [] |233| 
	.dwpsn	file "../hal.c",line 233,column 45,is_stmt
        MOVB      ACC,#255              ; [CPU_] |233| 
$C$DW$150	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$150, DW_AT_low_pc(0x00)
	.dwattr $C$DW$150, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$150, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |233| 
        ; call occurs [#_ERR_SetError] ; [] |233| 
$C$L17:    
	.dwpsn	file "../hal.c",line 236,column 7,is_stmt
        MOVB      *-SP[1],#69,UNC       ; [CPU_] |236| 
	.dwpsn	file "../hal.c",line 237,column 7,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |237| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |237| 
        MOVB      AL,#0                 ; [CPU_] |237| 
        SUBB      XAR5,#1               ; [CPU_U] |237| 
$C$DW$151	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$151, DW_AT_low_pc(0x00)
	.dwattr $C$DW$151, DW_AT_name("_MBX_post")
	.dwattr $C$DW$151, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |237| 
        ; call occurs [#_MBX_post] ; [] |237| 
	.dwpsn	file "../hal.c",line 238,column 7,is_stmt
        MOVB      *-SP[1],#72,UNC       ; [CPU_] |238| 
	.dwpsn	file "../hal.c",line 239,column 7,is_stmt
        MOVB      AL,#0                 ; [CPU_] |239| 
        MOVZ      AR5,SP                ; [CPU_U] |239| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |239| 
        SUBB      XAR5,#1               ; [CPU_U] |239| 
$C$DW$152	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$152, DW_AT_low_pc(0x00)
	.dwattr $C$DW$152, DW_AT_name("_MBX_post")
	.dwattr $C$DW$152, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |239| 
        ; call occurs [#_MBX_post] ; [] |239| 
	.dwpsn	file "../hal.c",line 241,column 3,is_stmt
        B         $C$L20,UNC            ; [CPU_] |241| 
        ; branch occurs ; [] |241| 
$C$L18:    
	.dwpsn	file "../hal.c",line 243,column 5,is_stmt
        MOVW      DP,#_ErrorCounter     ; [CPU_U] 
        MOV       AL,@_ErrorCounter     ; [CPU_] |243| 
        BF        $C$L19,EQ             ; [CPU_] |243| 
        ; branchcc occurs ; [] |243| 
	.dwpsn	file "../hal.c",line 243,column 27,is_stmt
        DEC       @_ErrorCounter        ; [CPU_] |243| 
        B         $C$L20,UNC            ; [CPU_] |243| 
        ; branch occurs ; [] |243| 
$C$L19:    
	.dwpsn	file "../hal.c",line 244,column 10,is_stmt
        MOV       AL,@_ErrorPinOn       ; [CPU_] |244| 
        BF        $C$L20,EQ             ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
	.dwpsn	file "../hal.c",line 245,column 7,is_stmt
        MOVB      @_HAL_CellOK,#2,UNC   ; [CPU_] |245| 
	.dwpsn	file "../hal.c",line 246,column 7,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        AND       @_ODV_Read_Inputs_16_Bit,#0xfff7 ; [CPU_] |246| 
	.dwpsn	file "../hal.c",line 249,column 7,is_stmt
        MOVW      DP,#_ErrorPinOn       ; [CPU_U] 
        MOV       @_ErrorPinOn,#0       ; [CPU_] |249| 
$C$L20:    
	.dwpsn	file "../hal.c",line 253,column 3,is_stmt
$C$DW$153	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$153, DW_AT_low_pc(0x00)
	.dwattr $C$DW$153, DW_AT_name("_SCI1_Update")
	.dwattr $C$DW$153, DW_AT_TI_call
        LCR       #_SCI1_Update         ; [CPU_] |253| 
        ; call occurs [#_SCI1_Update] ; [] |253| 
	.dwpsn	file "../hal.c",line 254,column 3,is_stmt
        B         $C$L30,UNC            ; [CPU_] |254| 
        ; branch occurs ; [] |254| 
$C$L21:    
	.dwpsn	file "../hal.c",line 257,column 7,is_stmt
        MOVW      DP,#_ODP_Contactor_Delay ; [CPU_U] 
        MOV       AL,@_ODP_Contactor_Delay ; [CPU_] |257| 
        MOVW      DP,#_RelayTimeOut     ; [CPU_U] 
        MOV       @_RelayTimeOut,AL     ; [CPU_] |257| 
	.dwpsn	file "../hal.c",line 258,column 7,is_stmt
        MOVW      DP,#_ODP_Contactor_Setup ; [CPU_U] 
        MOV       AL,@_ODP_Contactor_Setup ; [CPU_] |258| 
        MOVW      DP,#_EPwm2Regs+9      ; [CPU_U] 
        MOV       @_EPwm2Regs+9,AL      ; [CPU_] |258| 
	.dwpsn	file "../hal.c",line 259,column 7,is_stmt
        MOVW      DP,#_HAL_RelayState   ; [CPU_U] 
        MOVB      @_HAL_RelayState,#1,UNC ; [CPU_] |259| 
	.dwpsn	file "../hal.c",line 260,column 7,is_stmt
        MOVW      DP,#_AdcResult+1      ; [CPU_U] 
        MOV       AL,@_AdcResult+1      ; [CPU_] |260| 
        MOVW      DP,#_OldVal           ; [CPU_U] 
        MOV       @_OldVal,AL           ; [CPU_] |260| 
	.dwpsn	file "../hal.c",line 261,column 7,is_stmt
        B         $C$L32,UNC            ; [CPU_] |261| 
        ; branch occurs ; [] |261| 
$C$L22:    
	.dwpsn	file "../hal.c",line 263,column 7,is_stmt
        MOVW      DP,#_AdcResult+1      ; [CPU_U] 
        MOV       AL,@_AdcResult+1      ; [CPU_] |263| 
        MOV       *-SP[2],AL            ; [CPU_] |263| 
	.dwpsn	file "../hal.c",line 264,column 7,is_stmt
        MOVW      DP,#_RelayTimeOut     ; [CPU_U] 
        DEC       @_RelayTimeOut        ; [CPU_] |264| 
        BF        $C$L23,NEQ            ; [CPU_] |264| 
        ; branchcc occurs ; [] |264| 
	.dwpsn	file "../hal.c",line 264,column 32,is_stmt
        MOVB      @_HAL_RelayState,#20,UNC ; [CPU_] |264| 
$C$L23:    
	.dwpsn	file "../hal.c",line 265,column 7,is_stmt
        MOV       AL,@_OldVal           ; [CPU_] |265| 
        MOVW      DP,#_ODP_Contactor_Slope ; [CPU_U] 
        SUB       AL,*-SP[2]            ; [CPU_] |265| 
        CMP       AL,@_ODP_Contactor_Slope ; [CPU_] |265| 
        B         $C$L24,LT             ; [CPU_] |265| 
        ; branchcc occurs ; [] |265| 
	.dwpsn	file "../hal.c",line 265,column 49,is_stmt
        MOVW      DP,#_HAL_RelayState   ; [CPU_U] 
        MOVB      @_HAL_RelayState,#2,UNC ; [CPU_] |265| 
$C$L24:    
	.dwpsn	file "../hal.c",line 266,column 7,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |266| 
        MOVW      DP,#_OldVal           ; [CPU_U] 
        MOV       @_OldVal,AL           ; [CPU_] |266| 
	.dwpsn	file "../hal.c",line 267,column 7,is_stmt
        B         $C$L32,UNC            ; [CPU_] |267| 
        ; branch occurs ; [] |267| 
$C$L25:    
	.dwpsn	file "../hal.c",line 269,column 7,is_stmt
        MOVW      DP,#_ODP_Contactor_Hold ; [CPU_U] 
        MOV       AL,@_ODP_Contactor_Hold ; [CPU_] |269| 
        MOVW      DP,#_EPwm2Regs+9      ; [CPU_U] 
        MOV       @_EPwm2Regs+9,AL      ; [CPU_] |269| 
	.dwpsn	file "../hal.c",line 270,column 7,is_stmt
        MOVW      DP,#_HAL_RelayState   ; [CPU_U] 
        MOVB      @_HAL_RelayState,#15,UNC ; [CPU_] |270| 
	.dwpsn	file "../hal.c",line 271,column 7,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        OR        @_ODV_Read_Inputs_16_Bit,#0x0001 ; [CPU_] |271| 
	.dwpsn	file "../hal.c",line 272,column 7,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       @_GpioDataRegs+1,#0xfeff ; [CPU_] |272| 
	.dwpsn	file "../hal.c",line 273,column 7,is_stmt
        OR        @_GpioDataRegs,#0x0010 ; [CPU_] |273| 
	.dwpsn	file "../hal.c",line 274,column 7,is_stmt
        B         $C$L32,UNC            ; [CPU_] |274| 
        ; branch occurs ; [] |274| 
$C$L26:    
	.dwpsn	file "../hal.c",line 277,column 7,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        TBIT      @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |277| 
        BF        $C$L27,NTC            ; [CPU_] |277| 
        ; branchcc occurs ; [] |277| 
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        TBIT      @_ODV_Read_Inputs_16_Bit,#0 ; [CPU_] |277| 
        BF        $C$L27,TC             ; [CPU_] |277| 
        ; branchcc occurs ; [] |277| 
	.dwpsn	file "../hal.c",line 278,column 9,is_stmt
        MOVW      DP,#_HAL_RelayState   ; [CPU_U] 
        MOV       @_HAL_RelayState,#0   ; [CPU_] |278| 
	.dwpsn	file "../hal.c",line 279,column 7,is_stmt
        B         $C$L28,UNC            ; [CPU_] |279| 
        ; branch occurs ; [] |279| 
$C$L27:    
	.dwpsn	file "../hal.c",line 281,column 9,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        TBIT      @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |281| 
        BF        $C$L28,TC             ; [CPU_] |281| 
        ; branchcc occurs ; [] |281| 
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        TBIT      @_ODV_Read_Inputs_16_Bit,#0 ; [CPU_] |281| 
        BF        $C$L28,NTC            ; [CPU_] |281| 
        ; branchcc occurs ; [] |281| 
	.dwpsn	file "../hal.c",line 282,column 11,is_stmt
        MOVW      DP,#_EPwm2Regs+9      ; [CPU_U] 
        MOV       @_EPwm2Regs+9,#0      ; [CPU_] |282| 
	.dwpsn	file "../hal.c",line 283,column 11,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        AND       @_ODV_Read_Inputs_16_Bit,#0xfffe ; [CPU_] |283| 
	.dwpsn	file "../hal.c",line 284,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |284| 
	.dwpsn	file "../hal.c",line 285,column 11,is_stmt
        AND       @_GpioDataRegs,#0xffef ; [CPU_] |285| 
$C$L28:    
	.dwpsn	file "../hal.c",line 288,column 7,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        MOV       AL,@_ODV_Read_Inputs_16_Bit ; [CPU_] |288| 
        MOVW      DP,#_ODV_Gateway_Status ; [CPU_U] 
        ANDB      AL,#0x01              ; [CPU_] |288| 
        MOV       @_ODV_Gateway_Status,AL ; [CPU_] |288| 
	.dwpsn	file "../hal.c",line 289,column 7,is_stmt
        B         $C$L32,UNC            ; [CPU_] |289| 
        ; branch occurs ; [] |289| 
$C$L29:    
	.dwpsn	file "../hal.c",line 293,column 7,is_stmt
        MOVW      DP,#_EPwm2Regs+9      ; [CPU_U] 
        MOV       @_EPwm2Regs+9,#0      ; [CPU_] |293| 
	.dwpsn	file "../hal.c",line 294,column 7,is_stmt
        MOVW      DP,#_EPwm3Regs+9      ; [CPU_U] 
        MOV       @_EPwm3Regs+9,#0      ; [CPU_] |294| 
	.dwpsn	file "../hal.c",line 295,column 7,is_stmt
        MOV       @_EPwm3Regs+10,#0     ; [CPU_] |295| 
	.dwpsn	file "../hal.c",line 296,column 7,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |296| 
	.dwpsn	file "../hal.c",line 297,column 7,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Read_Inputs_16_Bit,#0 ; [CPU_] |297| 
	.dwpsn	file "../hal.c",line 298,column 7,is_stmt
$C$DW$154	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$154, DW_AT_low_pc(0x00)
	.dwattr $C$DW$154, DW_AT_name("_ERR_ErrorDigOvld")
	.dwattr $C$DW$154, DW_AT_TI_call
        LCR       #_ERR_ErrorDigOvld    ; [CPU_] |298| 
        ; call occurs [#_ERR_ErrorDigOvld] ; [] |298| 
	.dwpsn	file "../hal.c",line 299,column 7,is_stmt
        MOVW      DP,#_HAL_RelayState   ; [CPU_U] 
        MOVB      @_HAL_RelayState,#15,UNC ; [CPU_] |299| 
	.dwpsn	file "../hal.c",line 300,column 7,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |300| 
	.dwpsn	file "../hal.c",line 301,column 7,is_stmt
        AND       @_GpioDataRegs,#0xffef ; [CPU_] |301| 
	.dwpsn	file "../hal.c",line 302,column 7,is_stmt
        B         $C$L32,UNC            ; [CPU_] |302| 
        ; branch occurs ; [] |302| 
$C$L30:    
	.dwpsn	file "../hal.c",line 254,column 3,is_stmt
        MOVW      DP,#_HAL_RelayState   ; [CPU_U] 
        MOV       AL,@_HAL_RelayState   ; [CPU_] |254| 
        CMPB      AL,#2                 ; [CPU_] |254| 
        B         $C$L31,GT             ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
        CMPB      AL,#2                 ; [CPU_] |254| 
        BF        $C$L25,EQ             ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
        CMPB      AL,#0                 ; [CPU_] |254| 
        BF        $C$L21,EQ             ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
        CMPB      AL,#1                 ; [CPU_] |254| 
        BF        $C$L22,EQ             ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
        B         $C$L32,UNC            ; [CPU_] |254| 
        ; branch occurs ; [] |254| 
$C$L31:    
        CMPB      AL,#15                ; [CPU_] |254| 
        BF        $C$L26,EQ             ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
        CMPB      AL,#20                ; [CPU_] |254| 
        BF        $C$L29,EQ             ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
$C$L32:    
	.dwpsn	file "../hal.c",line 307,column 3,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        TBIT      @_ODV_Recorder_Control,#0 ; [CPU_] |307| 
        BF        $C$L33,NTC            ; [CPU_] |307| 
        ; branchcc occurs ; [] |307| 
	.dwpsn	file "../hal.c",line 309,column 5,is_stmt
$C$DW$155	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$155, DW_AT_low_pc(0x00)
	.dwattr $C$DW$155, DW_AT_name("_REC_Record")
	.dwattr $C$DW$155, DW_AT_TI_call
        LCR       #_REC_Record          ; [CPU_] |309| 
        ; call occurs [#_REC_Record] ; [] |309| 
	.dwpsn	file "../hal.c",line 311,column 1,is_stmt
$C$L33:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -26
        MOV32     R3H,*--SP             ; [CPU_] 
        MOV32     R2H,*--SP             ; [CPU_] 
        MOV32     R1H,*--SP             ; [CPU_] 
        MOV32     R0H,*--SP             ; [CPU_] 
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -16
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        MOVL      XAR7,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -14
	.dwcfi	restore_reg, 18
        MOVL      XAR6,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -12
	.dwcfi	restore_reg, 16
        MOVL      XAR5,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -10
	.dwcfi	restore_reg, 14
        MOVL      XAR4,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -8
	.dwcfi	restore_reg, 12
        MOVL      XT,*--SP              ; [CPU_] 
	.dwcfi	cfa_offset, -6
	.dwcfi	restore_reg, 22
	.dwcfi	restore_reg, 21
        POP       AR1H:AR0H             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 5
	.dwcfi	restore_reg, 7
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$156	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$156, DW_AT_low_pc(0x00)
	.dwattr $C$DW$156, DW_AT_TI_return
        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$140, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$140, DW_AT_TI_end_line(0x137)
	.dwattr $C$DW$140, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$140

	.sect	"ramfuncs:retain"
	.global	_HAL_ReadAnalogueInputs

$C$DW$157	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_ReadAnalogueInputs")
	.dwattr $C$DW$157, DW_AT_low_pc(_HAL_ReadAnalogueInputs)
	.dwattr $C$DW$157, DW_AT_high_pc(0x00)
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_HAL_ReadAnalogueInputs")
	.dwattr $C$DW$157, DW_AT_external
	.dwattr $C$DW$157, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$157, DW_AT_TI_begin_line(0x145)
	.dwattr $C$DW$157, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$157, DW_AT_TI_interrupt
	.dwattr $C$DW$157, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "../hal.c",line 325,column 44,is_stmt,address _HAL_ReadAnalogueInputs

	.dwfde $C$DW$CIE, _HAL_ReadAnalogueInputs

;***************************************************************
;* FNAME: _HAL_ReadAnalogueInputs       FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto, 24 SOE     *
;***************************************************************

_HAL_ReadAnalogueInputs:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        PUSH      AR1H:AR0H             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 5, 4
	.dwcfi	save_reg_to_mem, 7, 5
	.dwcfi	cfa_offset, -6
        MOVL      *SP++,XT              ; [CPU_] 
	.dwcfi	save_reg_to_mem, 21, 6
	.dwcfi	save_reg_to_mem, 22, 7
	.dwcfi	cfa_offset, -8
        MOVL      *SP++,XAR4            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 12, 8
	.dwcfi	save_reg_to_mem, 13, 9
	.dwcfi	cfa_offset, -10
        MOVL      *SP++,XAR5            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 14, 10
	.dwcfi	save_reg_to_mem, 15, 11
	.dwcfi	cfa_offset, -12
        MOVL      *SP++,XAR6            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 16, 12
	.dwcfi	save_reg_to_mem, 17, 13
	.dwcfi	cfa_offset, -14
        MOVL      *SP++,XAR7            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 18, 14
	.dwcfi	save_reg_to_mem, 19, 15
	.dwcfi	cfa_offset, -16
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 16
	.dwcfi	save_reg_to_mem, 40, 17
	.dwcfi	cfa_offset, -18
        MOV32     *SP++,R0H             ; [CPU_] 
        MOV32     *SP++,R1H             ; [CPU_] 
        MOV32     *SP++,R2H             ; [CPU_] 
        MOV32     *SP++,R3H             ; [CPU_] 
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        SPM       0                     ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
	.dwpsn	file "../hal.c",line 327,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        MOVB      @_PieCtrlRegs+1,#1,UNC ; [CPU_] |327| 
	.dwpsn	file "../hal.c",line 328,column 3,is_stmt
        MOVW      DP,#_ODP_Contactor_Overcurrent ; [CPU_U] 
        MOV       AL,@_ODP_Contactor_Overcurrent ; [CPU_] |328| 
        MOVW      DP,#_AdcResult+1      ; [CPU_U] 
        CMP       AL,@_AdcResult+1      ; [CPU_] |328| 
        B         $C$L34,HI             ; [CPU_] |328| 
        ; branchcc occurs ; [] |328| 
	.dwpsn	file "../hal.c",line 328,column 50,is_stmt
        MOVW      DP,#_HAL_RelayState   ; [CPU_U] 
        MOVB      @_HAL_RelayState,#20,UNC ; [CPU_] |328| 
$C$L34:    
	.dwpsn	file "../hal.c",line 329,column 3,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        TBIT      @_ODV_Read_Inputs_16_Bit,#0 ; [CPU_] |329| 
        BF        $C$L35,NTC            ; [CPU_] |329| 
        ; branchcc occurs ; [] |329| 
        MOVW      DP,#_ODP_Contactor_Undercurrent ; [CPU_U] 
        MOV       AL,@_ODP_Contactor_Undercurrent ; [CPU_] |329| 
        MOVW      DP,#_AdcResult+1      ; [CPU_U] 
        CMP       AL,@_AdcResult+1      ; [CPU_] |329| 
        B         $C$L35,LO             ; [CPU_] |329| 
        ; branchcc occurs ; [] |329| 
	.dwpsn	file "../hal.c",line 329,column 95,is_stmt
        MOVW      DP,#_HAL_RelayState   ; [CPU_U] 
        MOVB      @_HAL_RelayState,#20,UNC ; [CPU_] |329| 
$C$L35:    
	.dwpsn	file "../hal.c",line 330,column 3,is_stmt
        MOVW      DP,#_AdcResult+1      ; [CPU_U] 
        MOV       AL,@_AdcResult+1      ; [CPU_] |330| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+22 ; [CPU_U] 
        MOV       @_ODV_Read_Analogue_Input_16_Bit+22,AL ; [CPU_] |330| 
	.dwpsn	file "../hal.c",line 331,column 3,is_stmt
        MOVW      DP,#_AdcResult        ; [CPU_U] 
        UI16TOF32 R0H,@_AdcResult       ; [CPU_] |331| 
        MOVW      DP,#_ODP_Analogue_Output_Scaling_Float ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Output_Scaling_Float ; [CPU_] |331| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |331| 
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$158, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |331| 
        ; call occurs [#_CNV_Round] ; [] |331| 
        MOVW      DP,#_ODV_Write_Analogue_Output_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Analogue_Output_16_Bit,AL ; [CPU_] |331| 
	.dwpsn	file "../hal.c",line 333,column 3,is_stmt
        MOVW      DP,#_AdcRegs+5        ; [CPU_U] 
        OR        @_AdcRegs+5,#0x0100   ; [CPU_] |333| 
	.dwpsn	file "../hal.c",line 334,column 1,is_stmt
        MOV32     R3H,*--SP             ; [CPU_] 
        MOV32     R2H,*--SP             ; [CPU_] 
        MOV32     R1H,*--SP             ; [CPU_] 
        MOV32     R0H,*--SP             ; [CPU_] 
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -16
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        MOVL      XAR7,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -14
	.dwcfi	restore_reg, 18
        MOVL      XAR6,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -12
	.dwcfi	restore_reg, 16
        MOVL      XAR5,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -10
	.dwcfi	restore_reg, 14
        MOVL      XAR4,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -8
	.dwcfi	restore_reg, 12
        MOVL      XT,*--SP              ; [CPU_] 
	.dwcfi	cfa_offset, -6
	.dwcfi	restore_reg, 22
	.dwcfi	restore_reg, 21
        POP       AR1H:AR0H             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 5
	.dwcfi	restore_reg, 7
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_TI_return
        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$157, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$157, DW_AT_TI_end_line(0x14e)
	.dwattr $C$DW$157, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$157

	.sect	".text:retain"
	.global	_HAL_ReadAnalogueInputs2

$C$DW$160	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_ReadAnalogueInputs2")
	.dwattr $C$DW$160, DW_AT_low_pc(_HAL_ReadAnalogueInputs2)
	.dwattr $C$DW$160, DW_AT_high_pc(0x00)
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_HAL_ReadAnalogueInputs2")
	.dwattr $C$DW$160, DW_AT_external
	.dwattr $C$DW$160, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$160, DW_AT_TI_begin_line(0x157)
	.dwattr $C$DW$160, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$160, DW_AT_TI_interrupt
	.dwattr $C$DW$160, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../hal.c",line 343,column 45,is_stmt,address _HAL_ReadAnalogueInputs2

	.dwfde $C$DW$CIE, _HAL_ReadAnalogueInputs2

;***************************************************************
;* FNAME: _HAL_ReadAnalogueInputs2      FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  4 SOE     *
;***************************************************************

_HAL_ReadAnalogueInputs2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 4
	.dwcfi	save_reg_to_mem, 40, 5
	.dwcfi	cfa_offset, -6
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
	.dwpsn	file "../hal.c",line 346,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        MOVB      @_PieCtrlRegs+1,#1,UNC ; [CPU_] |346| 
	.dwpsn	file "../hal.c",line 350,column 3,is_stmt
        MOVW      DP,#_AdcRegs+5        ; [CPU_U] 
        OR        @_AdcRegs+5,#0x0002   ; [CPU_] |350| 
	.dwpsn	file "../hal.c",line 351,column 1,is_stmt
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_TI_return
        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$160, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$160, DW_AT_TI_end_line(0x15f)
	.dwattr $C$DW$160, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$160

	.sect	".text"
	.global	_HAL_LectureTemperature

$C$DW$162	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_LectureTemperature")
	.dwattr $C$DW$162, DW_AT_low_pc(_HAL_LectureTemperature)
	.dwattr $C$DW$162, DW_AT_high_pc(0x00)
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_HAL_LectureTemperature")
	.dwattr $C$DW$162, DW_AT_external
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$162, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$162, DW_AT_TI_begin_line(0x169)
	.dwattr $C$DW$162, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$162, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../hal.c",line 361,column 52,is_stmt,address _HAL_LectureTemperature

	.dwfde $C$DW$CIE, _HAL_LectureTemperature
$C$DW$163	.dwtag  DW_TAG_formal_parameter, DW_AT_name("val")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_val")
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$164	.dwtag  DW_TAG_formal_parameter, DW_AT_name("type")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_type")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$164, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _HAL_LectureTemperature       FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_HAL_LectureTemperature:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$165	.dwtag  DW_TAG_variable, DW_AT_name("val")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_val")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_breg20 -4]
$C$DW$166	.dwtag  DW_TAG_variable, DW_AT_name("type")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_type")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$166, DW_AT_location[DW_OP_breg20 -5]
$C$DW$167	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$167, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[5],AL            ; [CPU_] |361| 
        MOV32     *-SP[4],R0H           ; [CPU_] |361| 
	.dwpsn	file "../hal.c",line 366,column 3,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |366| 
	.dwpsn	file "../hal.c",line 367,column 3,is_stmt
        B         $C$L37,UNC            ; [CPU_] |367| 
        ; branch occurs ; [] |367| 
$C$L36:    
	.dwpsn	file "../hal.c",line 368,column 5,is_stmt
        INC       *-SP[6]               ; [CPU_] |368| 
$C$L37:    
	.dwpsn	file "../hal.c",line 367,column 9,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |367| 
        CMPB      AL,#36                ; [CPU_] |367| 
        B         $C$L38,GEQ            ; [CPU_] |367| 
        ; branchcc occurs ; [] |367| 
        MOV       T,*-SP[6]             ; [CPU_] |367| 
        MOVL      XAR4,#_TempTable+2    ; [CPU_U] |367| 
        MPYB      ACC,T,#6              ; [CPU_] |367| 
        MOV32     R1H,*-SP[4]           ; [CPU_] |367| 
        ADDL      XAR4,ACC              ; [CPU_] |367| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |367| 
        LSL       ACC,1                 ; [CPU_] |367| 
        ADDL      XAR4,ACC              ; [CPU_] |367| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |367| 
        CMPF32    R1H,R0H               ; [CPU_] |367| 
        MOVST0    ZF, NF                ; [CPU_] |367| 
        B         $C$L36,LT             ; [CPU_] |367| 
        ; branchcc occurs ; [] |367| 
$C$L38:    
	.dwpsn	file "../hal.c",line 370,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |370| 
        B         $C$L39,LEQ            ; [CPU_] |370| 
        ; branchcc occurs ; [] |370| 
        CMPB      AL,#37                ; [CPU_] |370| 
        B         $C$L39,GEQ            ; [CPU_] |370| 
        ; branchcc occurs ; [] |370| 
	.dwpsn	file "../hal.c",line 371,column 5,is_stmt
        ADDB      AL,#-1                ; [CPU_] |371| 
        MOV       T,AL                  ; [CPU_] |371| 
        MOVL      XAR5,#_TempTable+2    ; [CPU_U] |371| 
        MOVL      XAR4,#_TempTable+2    ; [CPU_U] |371| 
        MPYB      ACC,T,#6              ; [CPU_] |371| 
        MOV       T,*-SP[6]             ; [CPU_] |371| 
        ADDL      XAR5,ACC              ; [CPU_] |371| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |371| 
        LSL       ACC,1                 ; [CPU_] |371| 
        ADDL      XAR5,ACC              ; [CPU_] |371| 
        MPYB      ACC,T,#6              ; [CPU_] |371| 
        ADDL      XAR4,ACC              ; [CPU_] |371| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |371| 
        LSL       ACC,1                 ; [CPU_] |371| 
        ADDL      XAR4,ACC              ; [CPU_] |371| 
        MOV       AL,T                  ; [CPU_] |371| 
        ADDB      AL,#-1                ; [CPU_] |371| 
        MOV32     R1H,*+XAR4[0]         ; [CPU_] |371| 
        MOV       T,AL                  ; [CPU_] |371| 
        MOV32     R2H,*+XAR5[0]         ; [CPU_] |371| 
        MOVL      XAR5,#_TempTable+2    ; [CPU_U] |371| 
        MOV32     R0H,*-SP[4]           ; [CPU_] |371| 
        MPYB      ACC,T,#6              ; [CPU_] |371| 
        ADDL      XAR5,ACC              ; [CPU_] |371| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |371| 
        LSL       ACC,1                 ; [CPU_] |371| 
        ADDL      XAR5,ACC              ; [CPU_] |371| 

        MOV32     R2H,*+XAR5[0]         ; [CPU_] |371| 
||      SUBF32    R0H,R2H,R0H           ; [CPU_] |371| 

        SUBF32    R1H,R2H,R1H           ; [CPU_] |371| 
$C$DW$168	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$168, DW_AT_low_pc(0x00)
	.dwattr $C$DW$168, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$168, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |371| 
        ; call occurs [#FS$$DIV] ; [] |371| 
        MOV32     *-SP[4],R0H           ; [CPU_] |371| 
	.dwpsn	file "../hal.c",line 372,column 5,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |372| 
        ADDB      AL,#-1                ; [CPU_] |372| 
        MOV       T,AL                  ; [CPU_] |372| 
        MOVL      XAR4,#_TempTable      ; [CPU_U] |372| 
        MPYB      ACC,T,#6              ; [CPU_] |372| 
        ADDL      XAR4,ACC              ; [CPU_] |372| 
;       MOV       T,*+XAR4[0] Implicitly done by - MPY ACC,OP,#IMM
        MPY      ACC,*+XAR4[0],#10     ; [CPU_] |372| 
        MOV32     R0H,ACC               ; [CPU_] |372| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     R3H,*-SP[4]           ; [CPU_] |372| 
        I16TOF32  R1H,R0H               ; [CPU_] |372| 
        MPYF32    R0H,R3H,#16968        ; [CPU_] |372| 
        NOP       ; [CPU_] 
        ADDF32    R0H,R0H,R1H           ; [CPU_] |372| 
        NOP       ; [CPU_] 
        F32TOI16  R0H,R0H               ; [CPU_] |372| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |372| 
        MOV       *-SP[6],AL            ; [CPU_] |372| 
	.dwpsn	file "../hal.c",line 373,column 3,is_stmt
        B         $C$L40,UNC            ; [CPU_] |373| 
        ; branch occurs ; [] |373| 
$C$L39:    
	.dwpsn	file "../hal.c",line 375,column 5,is_stmt
        MOV       T,*-SP[6]             ; [CPU_] |375| 
        MOVL      XAR4,#_TempTable      ; [CPU_U] |375| 
        MPYB      ACC,T,#6              ; [CPU_] |375| 
        ADDL      XAR4,ACC              ; [CPU_] |375| 
        MOV       T,*+XAR4[0]           ; [CPU_] |375| 
        MPYB      ACC,T,#10             ; [CPU_] |375| 
        MOV       *-SP[6],AL            ; [CPU_] |375| 
$C$L40:    
	.dwpsn	file "../hal.c",line 377,column 3,is_stmt
	.dwpsn	file "../hal.c",line 378,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$169	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$169, DW_AT_low_pc(0x00)
	.dwattr $C$DW$169, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$162, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$162, DW_AT_TI_end_line(0x17a)
	.dwattr $C$DW$162, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$162

	.sect	".text"
	.global	_HAL_LectureADTemperature

$C$DW$170	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_LectureADTemperature")
	.dwattr $C$DW$170, DW_AT_low_pc(_HAL_LectureADTemperature)
	.dwattr $C$DW$170, DW_AT_high_pc(0x00)
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$170, DW_AT_external
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$170, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$170, DW_AT_TI_begin_line(0x17c)
	.dwattr $C$DW$170, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$170, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../hal.c",line 380,column 56,is_stmt,address _HAL_LectureADTemperature

	.dwfde $C$DW$CIE, _HAL_LectureADTemperature
$C$DW$171	.dwtag  DW_TAG_formal_parameter, DW_AT_name("vali")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_vali")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg0]
$C$DW$172	.dwtag  DW_TAG_formal_parameter, DW_AT_name("type")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_type")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _HAL_LectureADTemperature     FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_HAL_LectureADTemperature:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$173	.dwtag  DW_TAG_variable, DW_AT_name("vali")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_vali")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$173, DW_AT_location[DW_OP_breg20 -1]
$C$DW$174	.dwtag  DW_TAG_variable, DW_AT_name("type")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_type")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$174, DW_AT_location[DW_OP_breg20 -2]
$C$DW$175	.dwtag  DW_TAG_variable, DW_AT_name("val")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_val")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$175, DW_AT_location[DW_OP_breg20 -4]
$C$DW$176	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[2],AH            ; [CPU_] |380| 
        MOV       *-SP[1],AL            ; [CPU_] |380| 
	.dwpsn	file "../hal.c",line 385,column 3,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |385| 
	.dwpsn	file "../hal.c",line 386,column 3,is_stmt
        MOV       T,#6                  ; [CPU_] |386| 
        B         $C$L42,UNC            ; [CPU_] |386| 
        ; branch occurs ; [] |386| 
$C$L41:    
	.dwpsn	file "../hal.c",line 387,column 5,is_stmt
        INC       *-SP[5]               ; [CPU_] |387| 
$C$L42:    
	.dwpsn	file "../hal.c",line 386,column 9,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |386| 
        CMPB      AL,#36                ; [CPU_] |386| 
        B         $C$L43,HIS            ; [CPU_] |386| 
        ; branchcc occurs ; [] |386| 
        MPYXU     ACC,T,*-SP[5]         ; [CPU_] |386| 
        MOVL      XAR4,#_TempTable      ; [CPU_U] |386| 
        ADDL      XAR4,ACC              ; [CPU_] |386| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |386| 
        CMP       AL,*-SP[1]            ; [CPU_] |386| 
        B         $C$L41,LT             ; [CPU_] |386| 
        ; branchcc occurs ; [] |386| 
$C$L43:    
	.dwpsn	file "../hal.c",line 389,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |389| 
        BF        $C$L44,EQ             ; [CPU_] |389| 
        ; branchcc occurs ; [] |389| 
        CMPB      AL,#37                ; [CPU_] |389| 
        B         $C$L44,HIS            ; [CPU_] |389| 
        ; branchcc occurs ; [] |389| 
	.dwpsn	file "../hal.c",line 390,column 5,is_stmt
        MOV       T,#6                  ; [CPU_] |390| 
        MOVL      XAR6,#_TempTable      ; [CPU_U] |390| 
        MPYXU     ACC,T,*-SP[5]         ; [CPU_] |390| 
        I16TOF32  R0H,*-SP[1]           ; [CPU_] |390| 
        MOVL      XAR4,#_TempTable      ; [CPU_U] |390| 
        MOVL      XAR5,#_TempTable      ; [CPU_U] |390| 
        SETC      SXM                   ; [CPU_] 
        ADDL      XAR6,ACC              ; [CPU_] |390| 
        MOV       AL,*-SP[5]            ; [CPU_] |390| 
        I16TOF32  R1H,*+XAR6[0]         ; [CPU_] |390| 
        ADDB      AL,#-1                ; [CPU_] |390| 
        MPYXU     ACC,T,AL              ; [CPU_] |390| 
        SUBF32    R0H,R1H,R0H           ; [CPU_] |390| 
        ADDL      XAR4,ACC              ; [CPU_] |390| 
        MPYXU     ACC,T,*-SP[5]         ; [CPU_] |390| 
        ADDL      XAR5,ACC              ; [CPU_] |390| 
        MOV       AL,*+XAR5[0]          ; [CPU_] |390| 
        SUB       AL,*+XAR4[0]          ; [CPU_] |390| 
        MOV       ACC,AL                ; [CPU_] |390| 
        MOV32     R1H,ACC               ; [CPU_] |390| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        I32TOF32  R1H,R1H               ; [CPU_] |390| 
$C$DW$177	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$177, DW_AT_low_pc(0x00)
	.dwattr $C$DW$177, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$177, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |390| 
        ; call occurs [#FS$$DIV] ; [] |390| 
        MOV32     *-SP[4],R0H           ; [CPU_] |390| 
	.dwpsn	file "../hal.c",line 391,column 5,is_stmt
        MOV       T,#6                  ; [CPU_] |391| 
        MOVL      XAR4,#_TempTable+2    ; [CPU_U] |391| 
        MPYXU     ACC,T,*-SP[5]         ; [CPU_] |391| 
        MOVL      XAR5,#_TempTable+2    ; [CPU_U] |391| 
        ADDL      XAR4,ACC              ; [CPU_] |391| 
        MOVU      ACC,*-SP[2]           ; [CPU_] |391| 
        LSL       ACC,1                 ; [CPU_] |391| 
        ADDL      XAR4,ACC              ; [CPU_] |391| 
        MOV       AL,*-SP[5]            ; [CPU_] |391| 
        ADDB      AL,#-1                ; [CPU_] |391| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |391| 
        MPYXU     ACC,T,AL              ; [CPU_] |391| 
        MOVL      XAR4,#_TempTable+2    ; [CPU_U] |391| 
        ADDL      XAR5,ACC              ; [CPU_] |391| 
        MOVU      ACC,*-SP[2]           ; [CPU_] |391| 
        LSL       ACC,1                 ; [CPU_] |391| 
        ADDL      XAR5,ACC              ; [CPU_] |391| 
        MPYXU     ACC,T,*-SP[5]         ; [CPU_] |391| 
        MOV32     R1H,*+XAR5[0]         ; [CPU_] |391| 
        ADDL      XAR4,ACC              ; [CPU_] |391| 

        SUBF32    R1H,R1H,R0H           ; [CPU_] |391| 
||      MOV32     R3H,*-SP[4]           ; [CPU_] |391| 

        MOVU      ACC,*-SP[2]           ; [CPU_] |391| 
        MPYF32    R0H,R1H,R3H           ; [CPU_] |391| 
        LSL       ACC,1                 ; [CPU_] |391| 
        ADDL      XAR4,ACC              ; [CPU_] |391| 
        MOV32     R1H,*+XAR4[0]         ; [CPU_] |391| 
        ADDF32    R0H,R0H,R1H           ; [CPU_] |391| 
        NOP       ; [CPU_] 
        MOV32     *-SP[4],R0H           ; [CPU_] |391| 
	.dwpsn	file "../hal.c",line 392,column 3,is_stmt
        B         $C$L45,UNC            ; [CPU_] |392| 
        ; branch occurs ; [] |392| 
$C$L44:    
	.dwpsn	file "../hal.c",line 394,column 5,is_stmt
        MOV       T,#6                  ; [CPU_] |394| 
        MOVL      XAR4,#_TempTable+2    ; [CPU_U] |394| 
        MPYXU     ACC,T,*-SP[5]         ; [CPU_] |394| 
        ADDL      XAR4,ACC              ; [CPU_] |394| 
        MOVU      ACC,*-SP[2]           ; [CPU_] |394| 
        LSL       ACC,1                 ; [CPU_] |394| 
        ADDL      XAR4,ACC              ; [CPU_] |394| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |394| 
        MOVL      *-SP[4],ACC           ; [CPU_] |394| 
$C$L45:    
	.dwpsn	file "../hal.c",line 396,column 3,is_stmt
        MOV32     R0H,*-SP[4]           ; [CPU_] |396| 
        MPYF32    R0H,R0H,#16768        ; [CPU_] |396| 
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$178, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |396| 
        ; call occurs [#_CNV_Round] ; [] |396| 
	.dwpsn	file "../hal.c",line 397,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$170, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$170, DW_AT_TI_end_line(0x18d)
	.dwattr $C$DW$170, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$170

	.sect	".text"
	.global	_TskFnComputeMeasures

$C$DW$180	.dwtag  DW_TAG_subprogram, DW_AT_name("TskFnComputeMeasures")
	.dwattr $C$DW$180, DW_AT_low_pc(_TskFnComputeMeasures)
	.dwattr $C$DW$180, DW_AT_high_pc(0x00)
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_TskFnComputeMeasures")
	.dwattr $C$DW$180, DW_AT_external
	.dwattr $C$DW$180, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$180, DW_AT_TI_begin_line(0x18f)
	.dwattr $C$DW$180, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$180, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "../hal.c",line 399,column 32,is_stmt,address _TskFnComputeMeasures

	.dwfde $C$DW$CIE, _TskFnComputeMeasures

;***************************************************************
;* FNAME: _TskFnComputeMeasures         FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_TskFnComputeMeasures:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$181	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$181, DW_AT_location[DW_OP_breg20 -5]
$C$DW$182	.dwtag  DW_TAG_variable, DW_AT_name("volt")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_volt")
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$182, DW_AT_location[DW_OP_breg20 -6]
$C$DW$183	.dwtag  DW_TAG_variable, DW_AT_name("oldstate")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_oldstate")
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$183, DW_AT_location[DW_OP_breg20 -7]
$C$DW$184	.dwtag  DW_TAG_variable, DW_AT_name("SOC_Temp")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_SOC_Temp")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$184, DW_AT_location[DW_OP_breg20 -8]
$C$DW$185	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$185, DW_AT_location[DW_OP_breg20 -9]
$C$DW$186	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$186, DW_AT_location[DW_OP_breg20 -12]
$C$DW$187	.dwtag  DW_TAG_variable, DW_AT_name("temp1")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_temp1")
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$187, DW_AT_location[DW_OP_breg20 -14]
$C$DW$188	.dwtag  DW_TAG_variable, DW_AT_name("count")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$188, DW_AT_location[DW_OP_breg20 -16]
	.dwpsn	file "../hal.c",line 401,column 27,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |401| 
	.dwpsn	file "../hal.c",line 402,column 13,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |402| 
	.dwpsn	file "../hal.c",line 403,column 28,is_stmt
        ZERO      R0H                   ; [CPU_] |403| 
        MOV32     *-SP[16],R0H          ; [CPU_] |403| 
	.dwpsn	file "../hal.c",line 405,column 3,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |405| 
        MOVW      DP,#_ODV_Recorder_Period ; [CPU_U] 
        MOVL      @_ODV_Recorder_Period,XAR4 ; [CPU_] |405| 
	.dwpsn	file "../hal.c",line 406,column 3,is_stmt
        MOV       PL,#1798              ; [CPU_] |406| 
        MOV       PH,#0                 ; [CPU_] |406| 
        MOV       AL,#0                 ; [CPU_] |406| 
        MOV       AH,#8                 ; [CPU_] |406| 
        MOVW      DP,#_ODV_Recorder_Vectors ; [CPU_U] 
        MOVL      @_ODV_Recorder_Vectors,P ; [CPU_] |406| 
        MOVL      @_ODV_Recorder_Vectors+2,ACC ; [CPU_] |406| 
	.dwpsn	file "../hal.c",line 407,column 3,is_stmt
        MOVW      DP,#_ODV_Recorder_Start ; [CPU_U] 
        MOV       @_ODV_Recorder_Start,#0 ; [CPU_] |407| 
	.dwpsn	file "../hal.c",line 408,column 3,is_stmt
        MOVB      AL,#200               ; [CPU_] |408| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |408| 
$C$DW$189	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$189, DW_AT_low_pc(0x00)
	.dwattr $C$DW$189, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$189, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |408| 
        ; call occurs [#_SEM_pend] ; [] |408| 
	.dwpsn	file "../hal.c",line 410,column 9,is_stmt
$C$L46:    
	.dwpsn	file "../hal.c",line 416,column 6,is_stmt
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+14 ; [CPU_U] 
        MOVB      AH,#10                ; [CPU_] |416| 
        MOV       AL,@_ODV_Read_Analogue_Input_16_Bit+14 ; [CPU_] |416| 
$C$DW$190	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$190, DW_AT_low_pc(0x00)
	.dwattr $C$DW$190, DW_AT_name("I$$DIV")
	.dwattr $C$DW$190, DW_AT_TI_call
        FFC       XAR7,#I$$DIV          ; [CPU_] |416| 
        ; call occurs [#I$$DIV] ; [] |416| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,AL                ; [CPU_] |416| 
        MOV32     R0H,ACC               ; [CPU_] |416| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        I32TOF32  R0H,R0H               ; [CPU_] |416| 
        NOP       ; [CPU_] 
        MOV32     *-SP[12],R0H          ; [CPU_] |416| 
	.dwpsn	file "../hal.c",line 417,column 6,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Tmin ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_Tmin ; [CPU_] |417| 
        MOV32     R1H,*-SP[12]          ; [CPU_] |417| 
        CMPF32    R1H,R0H               ; [CPU_] |417| 
        MOVST0    ZF, NF                ; [CPU_] |417| 
        B         $C$L47,GEQ            ; [CPU_] |417| 
        ; branchcc occurs ; [] |417| 
	.dwpsn	file "../hal.c",line 417,column 40,is_stmt
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+15 ; [CPU_U] 
        MOVB      AH,#10                ; [CPU_] |417| 
        MOV       AL,@_ODV_Read_Analogue_Input_16_Bit+15 ; [CPU_] |417| 
$C$DW$191	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$191, DW_AT_low_pc(0x00)
	.dwattr $C$DW$191, DW_AT_name("I$$DIV")
	.dwattr $C$DW$191, DW_AT_TI_call
        FFC       XAR7,#I$$DIV          ; [CPU_] |417| 
        ; call occurs [#I$$DIV] ; [] |417| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,AL                ; [CPU_] |417| 
        MOV32     R0H,ACC               ; [CPU_] |417| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        I32TOF32  R0H,R0H               ; [CPU_] |417| 
        NOP       ; [CPU_] 
        MOV32     *-SP[12],R0H          ; [CPU_] |417| 
$C$L47:    
	.dwpsn	file "../hal.c",line 418,column 6,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Tmin ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_Tmin ; [CPU_] |418| 
        MOV32     R1H,*-SP[12]          ; [CPU_] |418| 
        CMPF32    R1H,R0H               ; [CPU_] |418| 
        MOVST0    ZF, NF                ; [CPU_] |418| 
        B         $C$L48,GEQ            ; [CPU_] |418| 
        ; branchcc occurs ; [] |418| 
	.dwpsn	file "../hal.c",line 418,column 40,is_stmt
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+20 ; [CPU_U] 
        MOVB      AH,#10                ; [CPU_] |418| 
        MOV       AL,@_ODV_Read_Analogue_Input_16_Bit+20 ; [CPU_] |418| 
$C$DW$192	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$192, DW_AT_low_pc(0x00)
	.dwattr $C$DW$192, DW_AT_name("I$$DIV")
	.dwattr $C$DW$192, DW_AT_TI_call
        FFC       XAR7,#I$$DIV          ; [CPU_] |418| 
        ; call occurs [#I$$DIV] ; [] |418| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,AL                ; [CPU_] |418| 
        MOV32     R0H,ACC               ; [CPU_] |418| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        I32TOF32  R0H,R0H               ; [CPU_] |418| 
        NOP       ; [CPU_] 
        MOV32     *-SP[12],R0H          ; [CPU_] |418| 
$C$L48:    
	.dwpsn	file "../hal.c",line 419,column 6,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Tmin ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_Tmin ; [CPU_] |419| 
        MOV32     R1H,*-SP[12]          ; [CPU_] |419| 
        CMPF32    R1H,R0H               ; [CPU_] |419| 
        MOVST0    ZF, NF                ; [CPU_] |419| 
        B         $C$L49,LT             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        MOVW      DP,#_ODP_SafetyLimits_Tmax ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_Tmax ; [CPU_] |419| 
        NOP       ; [CPU_] 
        CMPF32    R1H,R0H               ; [CPU_] |419| 
        MOVST0    ZF, NF                ; [CPU_] |419| 
        B         $C$L51,LEQ            ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
$C$L49:    
	.dwpsn	file "../hal.c",line 421,column 7,is_stmt
        MOVB      ACC,#1                ; [CPU_] |421| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        ADDL      @_GV_Temp_counter,ACC ; [CPU_] |421| 
	.dwpsn	file "../hal.c",line 422,column 7,is_stmt
        MOV       T,#10                 ; [CPU_] |422| 
        MOVW      DP,#_ODP_SafetyLimits_Temp_Delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Temp_Delay ; [CPU_] |422| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        CMPL      ACC,@_GV_Temp_counter ; [CPU_] |422| 
        B         $C$L56,HIS            ; [CPU_] |422| 
        ; branchcc occurs ; [] |422| 
	.dwpsn	file "../hal.c",line 424,column 8,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |424| 
        BF        $C$L50,NEQ            ; [CPU_] |424| 
        ; branchcc occurs ; [] |424| 
	.dwpsn	file "../hal.c",line 424,column 43,is_stmt
$C$DW$193	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$193, DW_AT_low_pc(0x00)
	.dwattr $C$DW$193, DW_AT_name("_ERR_ErrorOverTemp")
	.dwattr $C$DW$193, DW_AT_TI_call
        LCR       #_ERR_ErrorOverTemp   ; [CPU_] |424| 
        ; call occurs [#_ERR_ErrorOverTemp] ; [] |424| 
$C$L50:    
	.dwpsn	file "../hal.c",line 425,column 13,is_stmt
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_ChargeAllowed,#0 ; [CPU_] |425| 
	.dwpsn	file "../hal.c",line 426,column 13,is_stmt
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_DischargeAllowed,#0 ; [CPU_] |426| 
	.dwpsn	file "../hal.c",line 428,column 5,is_stmt
        B         $C$L56,UNC            ; [CPU_] |428| 
        ; branch occurs ; [] |428| 
$C$L51:    
	.dwpsn	file "../hal.c",line 430,column 7,is_stmt
        MOVW      DP,#_ODP_Temperature_ChargeLimits ; [CPU_U] 
        I16TOF32  R0H,@_ODP_Temperature_ChargeLimits ; [CPU_] |430| 
        NOP       ; [CPU_] 
        CMPF32    R1H,R0H               ; [CPU_] |430| 
        MOVST0    ZF, NF                ; [CPU_] |430| 
        B         $C$L52,GEQ            ; [CPU_] |430| 
        ; branchcc occurs ; [] |430| 
	.dwpsn	file "../hal.c",line 431,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Imax_charge ; [CPU_U] 
        MOV       T,@_ODP_SafetyLimits_Imax_charge ; [CPU_] |431| 
        MOVW      DP,#_ODP_Power_ChargeLimits ; [CPU_U] 
        MPY       ACC,T,@_ODP_Power_ChargeLimits ; [CPU_] |431| 
        MOVU      ACC,AL                ; [CPU_] |431| 
        MOV32     R0H,ACC               ; [CPU_] |431| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#17096            ; [CPU_] |431| 
        UI32TOF32 R0H,R0H               ; [CPU_] |431| 
$C$DW$194	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$194, DW_AT_low_pc(0x00)
	.dwattr $C$DW$194, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$194, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |431| 
        ; call occurs [#FS$$DIV] ; [] |431| 
$C$DW$195	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$195, DW_AT_low_pc(0x00)
	.dwattr $C$DW$195, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$195, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |431| 
        ; call occurs [#_CNV_Round] ; [] |431| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_ChargeAllowed,AL ; [CPU_] |431| 
	.dwpsn	file "../hal.c",line 432,column 7,is_stmt
        B         $C$L56,UNC            ; [CPU_] |432| 
        ; branch occurs ; [] |432| 
$C$L52:    
	.dwpsn	file "../hal.c",line 433,column 17,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |433| 
	.dwpsn	file "../hal.c",line 433,column 22,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |433| 
        CMPB      AL,#7                 ; [CPU_] |433| 
        B         $C$L56,GEQ            ; [CPU_] |433| 
        ; branchcc occurs ; [] |433| 
$C$L53:    
	.dwpsn	file "../hal.c",line 434,column 9,is_stmt
        ADDB      AL,#1                 ; [CPU_] |434| 
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ODP_Temperature_ChargeLimits ; [CPU_U] |434| 
        MOV       ACC,AL                ; [CPU_] |434| 
        ADDL      XAR4,ACC              ; [CPU_] |434| 
        MOVZ      AR6,*+XAR4[0]         ; [CPU_] |434| 
        MOVL      XAR4,#_ODP_Temperature_ChargeLimits ; [CPU_U] |434| 
        MOV       ACC,*-SP[5]           ; [CPU_] |434| 
        ADDL      XAR4,ACC              ; [CPU_] |434| 
        MOV       AL,AR6                ; [CPU_] |434| 
        CMP       AL,*+XAR4[0]          ; [CPU_] |434| 
        B         $C$L54,GT             ; [CPU_] |434| 
        ; branchcc occurs ; [] |434| 
	.dwpsn	file "../hal.c",line 435,column 11,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Imax_charge ; [CPU_U] 
        MOV       ACC,*-SP[5]           ; [CPU_] |435| 
        MOVL      XAR4,#_ODP_Power_ChargeLimits ; [CPU_U] |435| 
        MOV       T,@_ODP_SafetyLimits_Imax_charge ; [CPU_] |435| 
        ADDL      XAR4,ACC              ; [CPU_] |435| 
        MPY       ACC,T,*+XAR4[0]       ; [CPU_] |435| 
        MOVU      ACC,AL                ; [CPU_] |435| 
        MOV32     R0H,ACC               ; [CPU_] |435| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#17096            ; [CPU_] |435| 
        UI32TOF32 R0H,R0H               ; [CPU_] |435| 
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$196, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |435| 
        ; call occurs [#FS$$DIV] ; [] |435| 
$C$DW$197	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$197, DW_AT_low_pc(0x00)
	.dwattr $C$DW$197, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$197, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |435| 
        ; call occurs [#_CNV_Round] ; [] |435| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_ChargeAllowed,AL ; [CPU_] |435| 
	.dwpsn	file "../hal.c",line 436,column 11,is_stmt
        B         $C$L56,UNC            ; [CPU_] |436| 
        ; branch occurs ; [] |436| 
$C$L54:    
	.dwpsn	file "../hal.c",line 438,column 9,is_stmt
        MOV       ACC,*-SP[5]           ; [CPU_] |438| 
        MOVL      XAR4,#_ODP_Temperature_ChargeLimits ; [CPU_U] |438| 
        ADDL      XAR4,ACC              ; [CPU_] |438| 
        I16TOF32  R0H,*+XAR4[0]         ; [CPU_] |438| 
        NOP       ; [CPU_] 
        CMPF32    R1H,R0H               ; [CPU_] |438| 
        MOVST0    ZF, NF                ; [CPU_] |438| 
        B         $C$L55,LEQ            ; [CPU_] |438| 
        ; branchcc occurs ; [] |438| 
        MOV       AL,*-SP[5]            ; [CPU_] |438| 
        MOVL      XAR4,#_ODP_Temperature_ChargeLimits ; [CPU_U] |438| 
        ADDB      AL,#1                 ; [CPU_] |438| 
        MOV       ACC,AL                ; [CPU_] |438| 
        ADDL      XAR4,ACC              ; [CPU_] |438| 
        I16TOF32  R0H,*+XAR4[0]         ; [CPU_] |438| 
        NOP       ; [CPU_] 
        CMPF32    R1H,R0H               ; [CPU_] |438| 
        MOVST0    ZF, NF                ; [CPU_] |438| 
        B         $C$L55,GT             ; [CPU_] |438| 
        ; branchcc occurs ; [] |438| 
	.dwpsn	file "../hal.c",line 439,column 11,is_stmt
        MOV       ACC,*-SP[5]           ; [CPU_] |439| 
        MOVL      XAR4,#_ODP_Temperature_ChargeLimits ; [CPU_U] |439| 
        ADDL      XAR4,ACC              ; [CPU_] |439| 
        I16TOF32  R0H,*+XAR4[0]         ; [CPU_] |439| 
        MOV       ACC,*-SP[5]           ; [CPU_] |439| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |439| 
        MOVL      XAR5,#_ODP_Temperature_ChargeLimits ; [CPU_U] |439| 
        SUBF32    R0H,R1H,R0H           ; [CPU_] |439| 
        ADDL      XAR5,ACC              ; [CPU_] |439| 
        ADDB      XAR4,#1               ; [CPU_] |439| 
        MOV       ACC,AR4               ; [CPU_] |439| 
        MOVL      XAR4,#_ODP_Temperature_ChargeLimits ; [CPU_U] |439| 
        ADDL      XAR4,ACC              ; [CPU_] |439| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |439| 
        SUB       AL,*+XAR5[0]          ; [CPU_] |439| 
        MOV       ACC,AL                ; [CPU_] |439| 
        MOV32     R1H,ACC               ; [CPU_] |439| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        I32TOF32  R1H,R1H               ; [CPU_] |439| 
$C$DW$198	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$198, DW_AT_low_pc(0x00)
	.dwattr $C$DW$198, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$198, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |439| 
        ; call occurs [#FS$$DIV] ; [] |439| 
        MOV32     *-SP[14],R0H          ; [CPU_] |439| 
	.dwpsn	file "../hal.c",line 440,column 11,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[5]           ; [CPU_] |440| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |440| 
        MOVL      XAR5,#_ODP_Power_ChargeLimits ; [CPU_U] |440| 
        ADDL      XAR5,ACC              ; [CPU_] |440| 
        ADDB      XAR4,#1               ; [CPU_] |440| 
        MOV       ACC,AR4               ; [CPU_] |440| 
        MOVL      XAR4,#_ODP_Power_ChargeLimits ; [CPU_U] |440| 
        ADDL      XAR4,ACC              ; [CPU_] |440| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |440| 
        SUB       AL,*+XAR5[0]          ; [CPU_] |440| 
        MOV       ACC,AL                ; [CPU_] |440| 
        MOV32     R0H,ACC               ; [CPU_] |440| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     R3H,*-SP[14]          ; [CPU_] |440| 
        MOVL      XAR4,#_ODP_Power_ChargeLimits ; [CPU_U] |440| 
        I32TOF32  R1H,R0H               ; [CPU_] |440| 
        MOV       ACC,*-SP[5]           ; [CPU_] |440| 
        ADDL      XAR4,ACC              ; [CPU_] |440| 
        MPYF32    R0H,R1H,R3H           ; [CPU_] |440| 
        UI16TOF32 R2H,*+XAR4[0]         ; [CPU_] |440| 
        NOP       ; [CPU_] 
        ADDF32    R0H,R0H,R2H           ; [CPU_] |440| 
        NOP       ; [CPU_] 
        MOV32     *-SP[14],R0H          ; [CPU_] |440| 
	.dwpsn	file "../hal.c",line 441,column 11,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Imax_charge ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_SafetyLimits_Imax_charge ; [CPU_] |441| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |441| 
        MOVIZ     R1H,#17096            ; [CPU_] |441| 
$C$DW$199	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$199, DW_AT_low_pc(0x00)
	.dwattr $C$DW$199, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$199, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |441| 
        ; call occurs [#FS$$DIV] ; [] |441| 
$C$DW$200	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$200, DW_AT_low_pc(0x00)
	.dwattr $C$DW$200, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$200, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |441| 
        ; call occurs [#_CNV_Round] ; [] |441| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_ChargeAllowed,AL ; [CPU_] |441| 
	.dwpsn	file "../hal.c",line 442,column 11,is_stmt
        B         $C$L56,UNC            ; [CPU_] |442| 
        ; branch occurs ; [] |442| 
$C$L55:    
	.dwpsn	file "../hal.c",line 433,column 27,is_stmt
        INC       *-SP[5]               ; [CPU_] |433| 
	.dwpsn	file "../hal.c",line 433,column 22,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |433| 
        CMPB      AL,#7                 ; [CPU_] |433| 
        B         $C$L53,LT             ; [CPU_] |433| 
        ; branchcc occurs ; [] |433| 
$C$L56:    
	.dwpsn	file "../hal.c",line 446,column 5,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Tmin ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_Tmin ; [CPU_] |446| 
        MOV32     R1H,*-SP[12]          ; [CPU_] |446| 
        CMPF32    R1H,R0H               ; [CPU_] |446| 
        MOVST0    ZF, NF                ; [CPU_] |446| 
        B         $C$L62,LT             ; [CPU_] |446| 
        ; branchcc occurs ; [] |446| 
        MOVW      DP,#_ODP_SafetyLimits_Tmax ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_Tmax ; [CPU_] |446| 
        NOP       ; [CPU_] 
        CMPF32    R1H,R0H               ; [CPU_] |446| 
        MOVST0    ZF, NF                ; [CPU_] |446| 
        B         $C$L57,LEQ            ; [CPU_] |446| 
        ; branchcc occurs ; [] |446| 
        B         $C$L62,UNC            ; [CPU_] |446| 
        ; branch occurs ; [] |446| 
$C$L57:    
	.dwpsn	file "../hal.c",line 449,column 7,is_stmt
        MOVW      DP,#_ODP_Temperature_DischargeLimits ; [CPU_U] 
        I16TOF32  R0H,@_ODP_Temperature_DischargeLimits ; [CPU_] |449| 
        NOP       ; [CPU_] 
        CMPF32    R1H,R0H               ; [CPU_] |449| 
        MOVST0    ZF, NF                ; [CPU_] |449| 
        B         $C$L58,GEQ            ; [CPU_] |449| 
        ; branchcc occurs ; [] |449| 
	.dwpsn	file "../hal.c",line 450,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Imax_dis ; [CPU_U] 
        MOV       T,@_ODP_SafetyLimits_Imax_dis ; [CPU_] |450| 
        MOVW      DP,#_ODP_Power_DischargeLimits ; [CPU_U] 
        MPY       ACC,T,@_ODP_Power_DischargeLimits ; [CPU_] |450| 
        MOVU      ACC,AL                ; [CPU_] |450| 
        MOV32     R0H,ACC               ; [CPU_] |450| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#17096            ; [CPU_] |450| 
        UI32TOF32 R0H,R0H               ; [CPU_] |450| 
$C$DW$201	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$201, DW_AT_low_pc(0x00)
	.dwattr $C$DW$201, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$201, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |450| 
        ; call occurs [#FS$$DIV] ; [] |450| 
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$202, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |450| 
        ; call occurs [#_CNV_Round] ; [] |450| 
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_DischargeAllowed,AL ; [CPU_] |450| 
	.dwpsn	file "../hal.c",line 451,column 7,is_stmt
        B         $C$L62,UNC            ; [CPU_] |451| 
        ; branch occurs ; [] |451| 
$C$L58:    
	.dwpsn	file "../hal.c",line 452,column 17,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |452| 
	.dwpsn	file "../hal.c",line 452,column 22,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |452| 
        CMPB      AL,#7                 ; [CPU_] |452| 
        B         $C$L62,GEQ            ; [CPU_] |452| 
        ; branchcc occurs ; [] |452| 
$C$L59:    
	.dwpsn	file "../hal.c",line 453,column 9,is_stmt
        ADDB      AL,#1                 ; [CPU_] |453| 
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ODP_Temperature_DischargeLimits ; [CPU_U] |453| 
        MOV       ACC,AL                ; [CPU_] |453| 
        ADDL      XAR4,ACC              ; [CPU_] |453| 
        MOVZ      AR6,*+XAR4[0]         ; [CPU_] |453| 
        MOVL      XAR4,#_ODP_Temperature_DischargeLimits ; [CPU_U] |453| 
        MOV       ACC,*-SP[5]           ; [CPU_] |453| 
        ADDL      XAR4,ACC              ; [CPU_] |453| 
        MOV       AL,AR6                ; [CPU_] |453| 
        CMP       AL,*+XAR4[0]          ; [CPU_] |453| 
        B         $C$L60,GT             ; [CPU_] |453| 
        ; branchcc occurs ; [] |453| 
	.dwpsn	file "../hal.c",line 454,column 11,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Imax_dis ; [CPU_U] 
        MOV       ACC,*-SP[5]           ; [CPU_] |454| 
        MOVL      XAR4,#_ODP_Power_DischargeLimits ; [CPU_U] |454| 
        MOV       T,@_ODP_SafetyLimits_Imax_dis ; [CPU_] |454| 
        ADDL      XAR4,ACC              ; [CPU_] |454| 
        MPY       ACC,T,*+XAR4[0]       ; [CPU_] |454| 
        MOVU      ACC,AL                ; [CPU_] |454| 
        MOV32     R0H,ACC               ; [CPU_] |454| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#17096            ; [CPU_] |454| 
        UI32TOF32 R0H,R0H               ; [CPU_] |454| 
$C$DW$203	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$203, DW_AT_low_pc(0x00)
	.dwattr $C$DW$203, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$203, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |454| 
        ; call occurs [#FS$$DIV] ; [] |454| 
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$204, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |454| 
        ; call occurs [#_CNV_Round] ; [] |454| 
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_DischargeAllowed,AL ; [CPU_] |454| 
	.dwpsn	file "../hal.c",line 455,column 11,is_stmt
        B         $C$L62,UNC            ; [CPU_] |455| 
        ; branch occurs ; [] |455| 
$C$L60:    
	.dwpsn	file "../hal.c",line 457,column 9,is_stmt
        MOV       ACC,*-SP[5]           ; [CPU_] |457| 
        MOVL      XAR4,#_ODP_Temperature_DischargeLimits ; [CPU_U] |457| 
        ADDL      XAR4,ACC              ; [CPU_] |457| 
        I16TOF32  R0H,*+XAR4[0]         ; [CPU_] |457| 
        NOP       ; [CPU_] 
        CMPF32    R1H,R0H               ; [CPU_] |457| 
        MOVST0    ZF, NF                ; [CPU_] |457| 
        B         $C$L61,LEQ            ; [CPU_] |457| 
        ; branchcc occurs ; [] |457| 
        MOV       AL,*-SP[5]            ; [CPU_] |457| 
        MOVL      XAR4,#_ODP_Temperature_DischargeLimits ; [CPU_U] |457| 
        ADDB      AL,#1                 ; [CPU_] |457| 
        MOV       ACC,AL                ; [CPU_] |457| 
        ADDL      XAR4,ACC              ; [CPU_] |457| 
        I16TOF32  R0H,*+XAR4[0]         ; [CPU_] |457| 
        NOP       ; [CPU_] 
        CMPF32    R1H,R0H               ; [CPU_] |457| 
        MOVST0    ZF, NF                ; [CPU_] |457| 
        B         $C$L61,GT             ; [CPU_] |457| 
        ; branchcc occurs ; [] |457| 
	.dwpsn	file "../hal.c",line 458,column 11,is_stmt
        MOV       ACC,*-SP[5]           ; [CPU_] |458| 
        MOVL      XAR4,#_ODP_Temperature_DischargeLimits ; [CPU_U] |458| 
        ADDL      XAR4,ACC              ; [CPU_] |458| 
        I16TOF32  R0H,*+XAR4[0]         ; [CPU_] |458| 
        MOV       ACC,*-SP[5]           ; [CPU_] |458| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |458| 
        MOVL      XAR5,#_ODP_Temperature_DischargeLimits ; [CPU_U] |458| 
        SUBF32    R0H,R1H,R0H           ; [CPU_] |458| 
        ADDL      XAR5,ACC              ; [CPU_] |458| 
        ADDB      XAR4,#1               ; [CPU_] |458| 
        MOV       ACC,AR4               ; [CPU_] |458| 
        MOVL      XAR4,#_ODP_Temperature_DischargeLimits ; [CPU_U] |458| 
        ADDL      XAR4,ACC              ; [CPU_] |458| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |458| 
        SUB       AL,*+XAR5[0]          ; [CPU_] |458| 
        MOV       ACC,AL                ; [CPU_] |458| 
        MOV32     R1H,ACC               ; [CPU_] |458| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        I32TOF32  R1H,R1H               ; [CPU_] |458| 
$C$DW$205	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$205, DW_AT_low_pc(0x00)
	.dwattr $C$DW$205, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$205, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |458| 
        ; call occurs [#FS$$DIV] ; [] |458| 
        MOV32     *-SP[14],R0H          ; [CPU_] |458| 
	.dwpsn	file "../hal.c",line 459,column 11,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[5]           ; [CPU_] |459| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |459| 
        MOVL      XAR5,#_ODP_Power_DischargeLimits ; [CPU_U] |459| 
        ADDL      XAR5,ACC              ; [CPU_] |459| 
        ADDB      XAR4,#1               ; [CPU_] |459| 
        MOV       ACC,AR4               ; [CPU_] |459| 
        MOVL      XAR4,#_ODP_Power_DischargeLimits ; [CPU_U] |459| 
        ADDL      XAR4,ACC              ; [CPU_] |459| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |459| 
        SUB       AL,*+XAR5[0]          ; [CPU_] |459| 
        MOV       ACC,AL                ; [CPU_] |459| 
        MOV32     R0H,ACC               ; [CPU_] |459| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     R3H,*-SP[14]          ; [CPU_] |459| 
        MOVL      XAR4,#_ODP_Power_DischargeLimits ; [CPU_U] |459| 
        I32TOF32  R1H,R0H               ; [CPU_] |459| 
        MOV       ACC,*-SP[5]           ; [CPU_] |459| 
        ADDL      XAR4,ACC              ; [CPU_] |459| 
        MPYF32    R0H,R1H,R3H           ; [CPU_] |459| 
        UI16TOF32 R2H,*+XAR4[0]         ; [CPU_] |459| 
        NOP       ; [CPU_] 
        ADDF32    R0H,R0H,R2H           ; [CPU_] |459| 
        NOP       ; [CPU_] 
        MOV32     *-SP[14],R0H          ; [CPU_] |459| 
	.dwpsn	file "../hal.c",line 460,column 11,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Imax_dis ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_SafetyLimits_Imax_dis ; [CPU_] |460| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |460| 
        MOVIZ     R1H,#17096            ; [CPU_] |460| 
$C$DW$206	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$206, DW_AT_low_pc(0x00)
	.dwattr $C$DW$206, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$206, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |460| 
        ; call occurs [#FS$$DIV] ; [] |460| 
$C$DW$207	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$207, DW_AT_low_pc(0x00)
	.dwattr $C$DW$207, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$207, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |460| 
        ; call occurs [#_CNV_Round] ; [] |460| 
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_DischargeAllowed,AL ; [CPU_] |460| 
	.dwpsn	file "../hal.c",line 461,column 11,is_stmt
        B         $C$L62,UNC            ; [CPU_] |461| 
        ; branch occurs ; [] |461| 
$C$L61:    
	.dwpsn	file "../hal.c",line 452,column 27,is_stmt
        INC       *-SP[5]               ; [CPU_] |452| 
	.dwpsn	file "../hal.c",line 452,column 22,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |452| 
        CMPB      AL,#7                 ; [CPU_] |452| 
        B         $C$L59,LT             ; [CPU_] |452| 
        ; branchcc occurs ; [] |452| 
$C$L62:    
	.dwpsn	file "../hal.c",line 465,column 5,is_stmt
        MOVW      DP,#_ODV_Module1_Voltage ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Voltage ; [CPU_] |465| 
        BF        $C$L63,NEQ            ; [CPU_] |465| 
        ; branchcc occurs ; [] |465| 
	.dwpsn	file "../hal.c",line 467,column 6,is_stmt
        MOVW      DP,#_Temp_Module_Voltage ; [CPU_U] 
        MOVIZ     R1H,#16672            ; [CPU_] |467| 
        UI16TOF32 R0H,@_Temp_Module_Voltage ; [CPU_] |467| 
$C$DW$208	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$208, DW_AT_low_pc(0x00)
	.dwattr $C$DW$208, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$208, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |467| 
        ; call occurs [#FS$$DIV] ; [] |467| 
        MOVW      DP,#_ODP_SafetyLimits_Cell_Nb ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_SafetyLimits_Cell_Nb ; [CPU_] |467| 
$C$DW$209	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$209, DW_AT_low_pc(0x00)
	.dwattr $C$DW$209, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$209, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |467| 
        ; call occurs [#FS$$DIV] ; [] |467| 
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$210, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |467| 
        ; call occurs [#_CNV_Round] ; [] |467| 
        MOV       *-SP[6],AL            ; [CPU_] |467| 
	.dwpsn	file "../hal.c",line 468,column 5,is_stmt
        B         $C$L64,UNC            ; [CPU_] |468| 
        ; branch occurs ; [] |468| 
$C$L63:    
	.dwpsn	file "../hal.c",line 471,column 9,is_stmt
        UI16TOF32 R0H,@_ODV_Module1_Voltage ; [CPU_] |471| 
        MOVIZ     R1H,#16672            ; [CPU_] |471| 
$C$DW$211	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$211, DW_AT_low_pc(0x00)
	.dwattr $C$DW$211, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$211, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |471| 
        ; call occurs [#FS$$DIV] ; [] |471| 
        MOVW      DP,#_ODP_SafetyLimits_Cell_Nb ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_SafetyLimits_Cell_Nb ; [CPU_] |471| 
$C$DW$212	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$212, DW_AT_low_pc(0x00)
	.dwattr $C$DW$212, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$212, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |471| 
        ; call occurs [#FS$$DIV] ; [] |471| 
$C$DW$213	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$213, DW_AT_low_pc(0x00)
	.dwattr $C$DW$213, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$213, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |471| 
        ; call occurs [#_CNV_Round] ; [] |471| 
        MOV       *-SP[6],AL            ; [CPU_] |471| 
	.dwpsn	file "../hal.c",line 472,column 9,is_stmt
        MOVW      DP,#_ODV_Module1_Voltage ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Voltage ; [CPU_] |472| 
        MOVW      DP,#_Temp_Module_Voltage ; [CPU_U] 
        MOV       @_Temp_Module_Voltage,AL ; [CPU_] |472| 
$C$L64:    
	.dwpsn	file "../hal.c",line 475,column 5,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |475| 
        AND       AL,*+XAR4[0],#0x0200  ; [CPU_] |475| 
        LSR       AL,9                  ; [CPU_] |475| 
        CMPB      AL,#1                 ; [CPU_] |475| 
        BF        $C$L65,NEQ            ; [CPU_] |475| 
        ; branchcc occurs ; [] |475| 
	.dwpsn	file "../hal.c",line 476,column 6,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |476| 
        MOVW      DP,#_ODP_SafetyLimits_Umin ; [CPU_U] 
        SUB       AL,@_ODP_SafetyLimits_Umin ; [CPU_] |476| 
        MOV       T,AL                  ; [CPU_] |476| 
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MPYB      ACC,T,#100            ; [CPU_] |476| 
        MOV       AH,@_ODP_SafetyLimits_Umax ; [CPU_] |476| 
        MOVW      DP,#_ODP_SafetyLimits_Umin ; [CPU_U] 
        SUB       AH,@_ODP_SafetyLimits_Umin ; [CPU_] |476| 
$C$DW$214	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$214, DW_AT_low_pc(0x00)
	.dwattr $C$DW$214, DW_AT_name("I$$DIV")
	.dwattr $C$DW$214, DW_AT_TI_call
        FFC       XAR7,#I$$DIV          ; [CPU_] |476| 
        ; call occurs [#I$$DIV] ; [] |476| 
        MOV       *-SP[8],AL            ; [CPU_] |476| 
	.dwpsn	file "../hal.c",line 477,column 5,is_stmt
        B         $C$L66,UNC            ; [CPU_] |477| 
        ; branch occurs ; [] |477| 
$C$L65:    
	.dwpsn	file "../hal.c",line 478,column 6,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |478| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        SUB       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |478| 
        MOV       T,AL                  ; [CPU_] |478| 
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MPYB      ACC,T,#100            ; [CPU_] |478| 
        MOV       AH,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |478| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        SUB       AH,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |478| 
$C$DW$215	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$215, DW_AT_low_pc(0x00)
	.dwattr $C$DW$215, DW_AT_name("I$$DIV")
	.dwattr $C$DW$215, DW_AT_TI_call
        FFC       XAR7,#I$$DIV          ; [CPU_] |478| 
        ; call occurs [#I$$DIV] ; [] |478| 
        MOV       *-SP[8],AL            ; [CPU_] |478| 
$C$L66:    
	.dwpsn	file "../hal.c",line 482,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |482| 
        CMP       AL,*-SP[6]            ; [CPU_] |482| 
        B         $C$L67,GEQ            ; [CPU_] |482| 
        ; branchcc occurs ; [] |482| 
	.dwpsn	file "../hal.c",line 484,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |484| 
        MOVW      DP,#_GV_voltage_counter1 ; [CPU_U] 
        ADDL      @_GV_voltage_counter1,ACC ; [CPU_] |484| 
	.dwpsn	file "../hal.c",line 485,column 5,is_stmt
        MOV       T,#10                 ; [CPU_] |485| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |485| 
        MOVW      DP,#_GV_voltage_counter1 ; [CPU_U] 
        CMPL      ACC,@_GV_voltage_counter1 ; [CPU_] |485| 
        B         $C$L67,HIS            ; [CPU_] |485| 
        ; branchcc occurs ; [] |485| 
	.dwpsn	file "../hal.c",line 487,column 12,is_stmt
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$216, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |487| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |487| 
	.dwpsn	file "../hal.c",line 488,column 12,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |488| 
	.dwpsn	file "../hal.c",line 489,column 13,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit ; [CPU_U] 
        AND       @_ODV_Read_Inputs_16_Bit,#0xfffe ; [CPU_] |489| 
	.dwpsn	file "../hal.c",line 490,column 13,is_stmt
        MOVW      DP,#_EPwm2Regs+9      ; [CPU_U] 
        MOV       @_EPwm2Regs+9,#0      ; [CPU_] |490| 
	.dwpsn	file "../hal.c",line 491,column 13,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |491| 
	.dwpsn	file "../hal.c",line 492,column 13,is_stmt
        AND       @_GpioDataRegs,#0xffef ; [CPU_] |492| 
$C$L67:    
	.dwpsn	file "../hal.c",line 496,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |496| 
        CMP       AL,*-SP[6]            ; [CPU_] |496| 
        B         $C$L68,LEQ            ; [CPU_] |496| 
        ; branchcc occurs ; [] |496| 
	.dwpsn	file "../hal.c",line 498,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |498| 
        MOVW      DP,#_GV_voltage_counter ; [CPU_U] 
        ADDL      @_GV_voltage_counter,ACC ; [CPU_] |498| 
	.dwpsn	file "../hal.c",line 499,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |499| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |499| 
        MOVW      DP,#_GV_voltage_counter ; [CPU_U] 
        CMPL      ACC,@_GV_voltage_counter ; [CPU_] |499| 
        B         $C$L68,HIS            ; [CPU_] |499| 
        ; branchcc occurs ; [] |499| 
	.dwpsn	file "../hal.c",line 500,column 12,is_stmt
$C$DW$217	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$217, DW_AT_low_pc(0x00)
	.dwattr $C$DW$217, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$217, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |500| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |500| 
$C$L68:    
	.dwpsn	file "../hal.c",line 505,column 3,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |505| 
        CMPB      AL,#100               ; [CPU_] |505| 
        B         $C$L69,LEQ            ; [CPU_] |505| 
        ; branchcc occurs ; [] |505| 
	.dwpsn	file "../hal.c",line 505,column 21,is_stmt
        MOVB      *-SP[8],#100,UNC      ; [CPU_] |505| 
$C$L69:    
	.dwpsn	file "../hal.c",line 506,column 3,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |506| 
        B         $C$L70,GEQ            ; [CPU_] |506| 
        ; branchcc occurs ; [] |506| 
	.dwpsn	file "../hal.c",line 506,column 19,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |506| 
$C$L70:    
	.dwpsn	file "../hal.c",line 507,column 3,is_stmt
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Current ; [CPU_] |507| 
        B         $C$L71,GEQ            ; [CPU_] |507| 
        ; branchcc occurs ; [] |507| 
        NEG       AL                    ; [CPU_] |507| 
$C$L71:    
        MOVW      DP,#_ODV_Module1_Module_SOC_Current ; [CPU_U] 
        CMP       AL,@_ODV_Module1_Module_SOC_Current ; [CPU_] |507| 
        B         $C$L72,LO             ; [CPU_] |507| 
        ; branchcc occurs ; [] |507| 
        MOVW      DP,#_ODV_SOC_SOC2     ; [CPU_U] 
        MOV       AL,@_ODV_SOC_SOC2     ; [CPU_] |507| 
        BF        $C$L73,NEQ            ; [CPU_] |507| 
        ; branchcc occurs ; [] |507| 
        MOV       AL,*-SP[8]            ; [CPU_] |507| 
        B         $C$L73,LEQ            ; [CPU_] |507| 
        ; branchcc occurs ; [] |507| 
$C$L72:    
	.dwpsn	file "../hal.c",line 508,column 8,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |508| 
        MOVW      DP,#_ODV_SOC_SOC2     ; [CPU_U] 
        MOV       @_ODV_SOC_SOC2,AL     ; [CPU_] |508| 
	.dwpsn	file "../hal.c",line 509,column 8,is_stmt
        MOVB      ACC,#0                ; [CPU_] |509| 
        MOVW      DP,#_HAL_Current_Sum  ; [CPU_U] 
        MOVL      @_HAL_Current_Sum,ACC ; [CPU_] |509| 
$C$L73:    
	.dwpsn	file "../hal.c",line 515,column 5,is_stmt
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      ACC,@_PAR_Capacity_Total ; [CPU_] |515| 
        BF        $C$L74,NEQ            ; [CPU_] |515| 
        ; branchcc occurs ; [] |515| 
	.dwpsn	file "../hal.c",line 515,column 34,is_stmt
        MOV       ACC,#28125 << 12      ; [CPU_] |515| 
        MOVL      @_PAR_Capacity_Total,ACC ; [CPU_] |515| 
$C$L74:    
	.dwpsn	file "../hal.c",line 516,column 5,is_stmt
        UI32TOF32 R1H,@_PAR_Capacity_Total ; [CPU_] |516| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        UI32TOF32 R0H,@_PAR_Capacity_Left ; [CPU_] |516| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R0H,#17096        ; [CPU_] |516| 
$C$DW$218	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$218, DW_AT_low_pc(0x00)
	.dwattr $C$DW$218, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$218, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |516| 
        ; call occurs [#FS$$DIV] ; [] |516| 
$C$DW$219	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$219, DW_AT_low_pc(0x00)
	.dwattr $C$DW$219, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$219, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |516| 
        ; call occurs [#_CNV_Round] ; [] |516| 
        MOVW      DP,#_ODV_SOC_SOC1     ; [CPU_U] 
        MOV       @_ODV_SOC_SOC1,AL     ; [CPU_] |516| 
	.dwpsn	file "../hal.c",line 517,column 5,is_stmt
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        UI32TOF32 R1H,@_PAR_Capacity_Total ; [CPU_] |517| 
        MOVW      DP,#_HAL_Current_Sum  ; [CPU_U] 
        I32TOF32  R0H,@_HAL_Current_Sum ; [CPU_] |517| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R0H,#17096        ; [CPU_] |517| 
$C$DW$220	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$220, DW_AT_low_pc(0x00)
	.dwattr $C$DW$220, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$220, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |517| 
        ; call occurs [#FS$$DIV] ; [] |517| 
$C$DW$221	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$221, DW_AT_low_pc(0x00)
	.dwattr $C$DW$221, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$221, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |517| 
        ; call occurs [#_CNV_Round] ; [] |517| 
        MOVW      DP,#_ODV_SOC_SOC2     ; [CPU_U] 
        ADD       AL,@_ODV_SOC_SOC2     ; [CPU_] |517| 
        MOV       *-SP[8],AL            ; [CPU_] |517| 
	.dwpsn	file "../hal.c",line 518,column 5,is_stmt
        CMPB      AL,#100               ; [CPU_] |518| 
        B         $C$L75,LEQ            ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
	.dwpsn	file "../hal.c",line 518,column 23,is_stmt
        MOVB      *-SP[8],#100,UNC      ; [CPU_] |518| 
$C$L75:    
	.dwpsn	file "../hal.c",line 519,column 5,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |519| 
        B         $C$L76,GEQ            ; [CPU_] |519| 
        ; branchcc occurs ; [] |519| 
	.dwpsn	file "../hal.c",line 519,column 21,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |519| 
$C$L76:    
	.dwpsn	file "../hal.c",line 520,column 5,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |520| 
        MOVW      DP,#_ODV_Module1_SOC  ; [CPU_U] 
        MOV       @_ODV_Module1_SOC,AL  ; [CPU_] |520| 
	.dwpsn	file "../hal.c",line 521,column 5,is_stmt
        MOVW      DP,#_HAL_TimeSOC      ; [CPU_U] 
        CMP       @_HAL_TimeSOC,#600    ; [CPU_] |521| 
        B         $C$L77,LOS            ; [CPU_] |521| 
        ; branchcc occurs ; [] |521| 
	.dwpsn	file "../hal.c",line 522,column 7,is_stmt
        MOV       @_HAL_TimeSOC,#0      ; [CPU_] |522| 
	.dwpsn	file "../hal.c",line 523,column 7,is_stmt
        MOV       AL,#-5                ; [CPU_] |523| 
$C$DW$222	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$222, DW_AT_low_pc(0x00)
	.dwattr $C$DW$222, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$222, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |523| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |523| 
$C$L77:    
	.dwpsn	file "../hal.c",line 526,column 5,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |526| 
        AND       AL,*+XAR4[0],#0x0400  ; [CPU_] |526| 
        LSR       AL,10                 ; [CPU_] |526| 
        CMPB      AL,#1                 ; [CPU_] |526| 
        BF        $C$L80,NEQ            ; [CPU_] |526| 
        ; branchcc occurs ; [] |526| 
	.dwpsn	file "../hal.c",line 527,column 6,is_stmt
        MOVW      DP,#_ODP_Heater_Off_Cell_Voltage ; [CPU_U] 
        MOV       AL,@_ODP_Heater_Off_Cell_Voltage ; [CPU_] |527| 
        CMP       AL,*-SP[6]            ; [CPU_] |527| 
        B         $C$L78,GEQ            ; [CPU_] |527| 
        ; branchcc occurs ; [] |527| 
        MOVW      DP,#_ODP_Heater_OffTemp ; [CPU_U] 
        I16TOF32  R0H,@_ODP_Heater_OffTemp ; [CPU_] |527| 
        MOV32     R1H,*-SP[12]          ; [CPU_] |527| 
        CMPF32    R1H,R0H               ; [CPU_] |527| 
        MOVST0    ZF, NF                ; [CPU_] |527| 
        B         $C$L79,LT             ; [CPU_] |527| 
        ; branchcc occurs ; [] |527| 
$C$L78:    
	.dwpsn	file "../hal.c",line 529,column 8,is_stmt
        MOVW      DP,#_SCI_GpioState    ; [CPU_U] 
        MOV       @_SCI_GpioState,#0    ; [CPU_] |529| 
	.dwpsn	file "../hal.c",line 530,column 8,is_stmt
        MOVW      DP,#_ODV_Heater_Heater_Status ; [CPU_U] 
        MOV       @_ODV_Heater_Heater_Status,#0 ; [CPU_] |530| 
	.dwpsn	file "../hal.c",line 531,column 7,is_stmt
        B         $C$L81,UNC            ; [CPU_] |531| 
        ; branch occurs ; [] |531| 
$C$L79:    
	.dwpsn	file "../hal.c",line 532,column 14,is_stmt
        MOVW      DP,#_ODP_Heater_OnTemp ; [CPU_U] 
        I16TOF32  R0H,@_ODP_Heater_OnTemp ; [CPU_] |532| 
        NOP       ; [CPU_] 
        CMPF32    R1H,R0H               ; [CPU_] |532| 
        MOVST0    ZF, NF                ; [CPU_] |532| 
        B         $C$L81,GEQ            ; [CPU_] |532| 
        ; branchcc occurs ; [] |532| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |532| 
        BF        $C$L81,NEQ            ; [CPU_] |532| 
        ; branchcc occurs ; [] |532| 
	.dwpsn	file "../hal.c",line 534,column 9,is_stmt
        MOVW      DP,#_SCI_GpioState    ; [CPU_U] 
        MOV       @_SCI_GpioState,#8192 ; [CPU_] |534| 
	.dwpsn	file "../hal.c",line 535,column 9,is_stmt
        MOVW      DP,#_ODV_Heater_Heater_Status ; [CPU_U] 
        MOVB      @_ODV_Heater_Heater_Status,#1,UNC ; [CPU_] |535| 
	.dwpsn	file "../hal.c",line 537,column 9,is_stmt
        B         $C$L81,UNC            ; [CPU_] |537| 
        ; branch occurs ; [] |537| 
$C$L80:    
	.dwpsn	file "../hal.c",line 540,column 7,is_stmt
        MOVW      DP,#_SCI_GpioState    ; [CPU_U] 
        MOV       @_SCI_GpioState,#0    ; [CPU_] |540| 
	.dwpsn	file "../hal.c",line 541,column 7,is_stmt
        MOVW      DP,#_ODV_Heater_Heater_Status ; [CPU_U] 
        MOV       @_ODV_Heater_Heater_Status,#0 ; [CPU_] |541| 
$C$L81:    
	.dwpsn	file "../hal.c",line 543,column 9,is_stmt
        MOVW      DP,#_SCI_GpioState    ; [CPU_U] 
        MOV       AL,@_SCI_GpioState    ; [CPU_] |543| 
        CMP       AL,*-SP[7]            ; [CPU_] |543| 
        BF        $C$L82,EQ             ; [CPU_] |543| 
        ; branchcc occurs ; [] |543| 
	.dwpsn	file "../hal.c",line 544,column 11,is_stmt
        MOV       AL,@_SCI_GpioState    ; [CPU_] |544| 
        MOV       *-SP[7],AL            ; [CPU_] |544| 
	.dwpsn	file "../hal.c",line 545,column 11,is_stmt
        MOVB      *-SP[9],#73,UNC       ; [CPU_] |545| 
	.dwpsn	file "../hal.c",line 546,column 11,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |546| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |546| 
        MOVB      AL,#0                 ; [CPU_] |546| 
        SUBB      XAR5,#9               ; [CPU_U] |546| 
$C$DW$223	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$223, DW_AT_low_pc(0x00)
	.dwattr $C$DW$223, DW_AT_name("_MBX_post")
	.dwattr $C$DW$223, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |546| 
        ; call occurs [#_MBX_post] ; [] |546| 
$C$L82:    
	.dwpsn	file "../hal.c",line 550,column 5,is_stmt
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOVL      XAR4,#2000            ; [CPU_U] |550| 
        MOV       T,@_ODV_Module1_Current ; [CPU_] |550| 
        MOVW      DP,#_ODV_Module1_Voltage ; [CPU_U] 
        MOVL      *-SP[2],XAR4          ; [CPU_] |550| 
        MPYXU     ACC,T,@_ODV_Module1_Voltage ; [CPU_] |550| 
$C$DW$224	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$224, DW_AT_low_pc(0x00)
	.dwattr $C$DW$224, DW_AT_name("L$$DIV")
	.dwattr $C$DW$224, DW_AT_TI_call
        FFC       XAR7,#L$$DIV          ; [CPU_] |550| 
        ; call occurs [#L$$DIV] ; [] |550| 
        B         $C$L83,LT             ; [CPU_] |550| 
        ; branchcc occurs ; [] |550| 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       T,@_ODV_Module1_Current ; [CPU_] |550| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |550| 
        MOVW      DP,#_ODV_Module1_Voltage ; [CPU_U] 
        MPYXU     ACC,T,@_ODV_Module1_Voltage ; [CPU_] |550| 
$C$DW$225	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$225, DW_AT_low_pc(0x00)
	.dwattr $C$DW$225, DW_AT_name("L$$DIV")
	.dwattr $C$DW$225, DW_AT_TI_call
        FFC       XAR7,#L$$DIV          ; [CPU_] |550| 
        ; call occurs [#L$$DIV] ; [] |550| 
        B         $C$L84,UNC            ; [CPU_] |550| 
        ; branch occurs ; [] |550| 
$C$L83:    
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       T,@_ODV_Module1_Current ; [CPU_] |550| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |550| 
        MOVW      DP,#_ODV_Module1_Voltage ; [CPU_U] 
        MPYXU     ACC,T,@_ODV_Module1_Voltage ; [CPU_] |550| 
$C$DW$226	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$226, DW_AT_low_pc(0x00)
	.dwattr $C$DW$226, DW_AT_name("L$$DIV")
	.dwattr $C$DW$226, DW_AT_TI_call
        FFC       XAR7,#L$$DIV          ; [CPU_] |550| 
        ; call occurs [#L$$DIV] ; [] |550| 
        NEG       ACC                   ; [CPU_] |550| 
$C$L84:    
        MOVW      DP,#_ODV_Module1_Throughput ; [CPU_U] 
        MOV       @_ODV_Module1_Throughput,AL ; [CPU_] |550| 
	.dwpsn	file "../hal.c",line 551,column 5,is_stmt
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      ACC,@_PAR_Capacity_Total ; [CPU_] |551| 
        MOVL      *-SP[4],ACC           ; [CPU_] |551| 
        MOVW      DP,#_PAR_Capacity_TotalLife_Used ; [CPU_U] 
        MOV       *-SP[2],#0            ; [CPU_] |551| 
        MOVL      P,@_PAR_Capacity_TotalLife_Used ; [CPU_] |551| 
        MOV       *-SP[1],#0            ; [CPU_] |551| 
        MOVL      ACC,@_PAR_Capacity_TotalLife_Used+2 ; [CPU_] |551| 
$C$DW$227	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$227, DW_AT_low_pc(0x00)
	.dwattr $C$DW$227, DW_AT_name("ULL$$DIV")
	.dwattr $C$DW$227, DW_AT_TI_call
        LCR       #ULL$$DIV             ; [CPU_] |551| 
        ; call occurs [#ULL$$DIV] ; [] |551| 
        MOVW      DP,#_ODV_Battery_Total_Cycles ; [CPU_U] 
        MOV       @_ODV_Battery_Total_Cycles,P ; [CPU_] |551| 
	.dwpsn	file "../hal.c",line 552,column 5,is_stmt
        MOVL      XAR4,#360000          ; [CPU_U] |552| 
        MOVW      DP,#_PAR_Capacity_TotalLife_Used ; [CPU_U] 
        MOVL      *-SP[4],XAR4          ; [CPU_] |552| 
        MOVL      ACC,@_PAR_Capacity_TotalLife_Used+2 ; [CPU_] |552| 
        MOVL      P,@_PAR_Capacity_TotalLife_Used ; [CPU_] |552| 
        MOV       *-SP[2],#0            ; [CPU_] |552| 
        MOV       *-SP[1],#0            ; [CPU_] |552| 
$C$DW$228	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$228, DW_AT_low_pc(0x00)
	.dwattr $C$DW$228, DW_AT_name("ULL$$DIV")
	.dwattr $C$DW$228, DW_AT_TI_call
        LCR       #ULL$$DIV             ; [CPU_] |552| 
        ; call occurs [#ULL$$DIV] ; [] |552| 
        MOVW      DP,#_ODP_SafetyLimits_Cell_Nb ; [CPU_U] 
        MOVZ      AR4,@_ODP_SafetyLimits_Cell_Nb ; [CPU_] |552| 
        LSR64     ACC:P,5               ; [CPU_] |552| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |552| 
        MOV       *-SP[2],#0            ; [CPU_] |552| 
        MOV       *-SP[1],#0            ; [CPU_] |552| 
$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_name("LL$$MPY")
	.dwattr $C$DW$229, DW_AT_TI_call
        FFC       XAR7,#LL$$MPY         ; [CPU_] |552| 
        ; call occurs [#LL$$MPY] ; [] |552| 
$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_name("ULL$$TOFS")
	.dwattr $C$DW$230, DW_AT_TI_call
        LCR       #ULL$$TOFS            ; [CPU_] |552| 
        ; call occurs [#ULL$$TOFS] ; [] |552| 
        MOVIZ     R1H,#15200            ; [CPU_] |552| 
        MOVXI     R1H,#51674            ; [CPU_] |552| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |552| 
        NOP       ; [CPU_] 
        F32TOUI16 R0H,R0H               ; [CPU_] |552| 
        NOP       ; [CPU_] 
        MOVW      DP,#_ODV_Battery_Total_ChargedkWh ; [CPU_U] 
        MOV32     ACC,R0H               ; [CPU_] |552| 
        MOV       @_ODV_Battery_Total_ChargedkWh,AL ; [CPU_] |552| 
	.dwpsn	file "../hal.c",line 557,column 5,is_stmt
        MOVW      DP,#_ODP_Battery_Cycles ; [CPU_U] 
        MOV       AL,@_ODP_Battery_Cycles ; [CPU_] |557| 
        BF        $C$L85,EQ             ; [CPU_] |557| 
        ; branchcc occurs ; [] |557| 
	.dwpsn	file "../hal.c",line 557,column 34,is_stmt
        MOVW      DP,#_ODV_Battery_Total_Cycles ; [CPU_U] 
        MOV       T,@_ODV_Battery_Total_Cycles ; [CPU_] |557| 
        MPYB      ACC,T,#100            ; [CPU_] |557| 
        MOVW      DP,#_ODP_Battery_Cycles ; [CPU_U] 
        MOVU      ACC,AL                ; [CPU_] |557| 
        RPT       #15
||     SUBCU     ACC,@_ODP_Battery_Cycles ; [CPU_] |557| 
        MOVB      AH,#100               ; [CPU_] |557| 
        MOVW      DP,#_ODV_Module1_SOH  ; [CPU_U] 
        SUB       AH,AL                 ; [CPU_] |557| 
        MOV       @_ODV_Module1_SOH,AH  ; [CPU_] |557| 
$C$L85:    
	.dwpsn	file "../hal.c",line 559,column 5,is_stmt
        MOVW      DP,#_ODV_SOC_SOC1     ; [CPU_U] 
        UI16TOF32 R0H,@_ODV_SOC_SOC1    ; [CPU_] |559| 
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOV32     R1H,@_ODP_Battery_Capacity ; [CPU_] |559| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |559| 
        MOVIZ     R1H,#16672            ; [CPU_] |559| 
$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$231, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |559| 
        ; call occurs [#FS$$DIV] ; [] |559| 
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$232, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |559| 
        ; call occurs [#_CNV_Round] ; [] |559| 
        MOVW      DP,#_ODV_Module1_Capacity_Used ; [CPU_U] 
        MOV       @_ODV_Module1_Capacity_Used,AL ; [CPU_] |559| 
	.dwpsn	file "../hal.c",line 561,column 5,is_stmt
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        CMP       @_ODV_Module1_Current,#-1 ; [CPU_] |561| 
        B         $C$L87,GEQ            ; [CPU_] |561| 
        ; branchcc occurs ; [] |561| 
	.dwpsn	file "../hal.c",line 561,column 35,is_stmt
        MOV       AL,@_ODV_Module1_Current ; [CPU_] |561| 
        B         $C$L86,GEQ            ; [CPU_] |561| 
        ; branchcc occurs ; [] |561| 
        NEG       AL                    ; [CPU_] |561| 
$C$L86:    
        SETC      SXM                   ; [CPU_] 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOV       ACC,AL                ; [CPU_] |561| 
        MOVL      XAR4,#6000            ; [CPU_U] |561| 
        MOVL      P,@_PAR_Capacity_Left ; [CPU_] |561| 
        MOVL      XAR6,ACC              ; [CPU_] |561| 
        MOVW      DP,#_ODV_Module1_Time_Remaining ; [CPU_U] 
        MOVB      ACC,#0                ; [CPU_] |561| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |561| 
        MOVB      ACC,#0                ; [CPU_] |561| 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |561| 
        MOV       @_ODV_Module1_Time_Remaining,P ; [CPU_] |561| 
$C$L87:    
	.dwpsn	file "../hal.c",line 563,column 5,is_stmt
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Current ; [CPU_] |563| 
        CMPB      AL,#15                ; [CPU_] |563| 
        B         $C$L88,LEQ            ; [CPU_] |563| 
        ; branchcc occurs ; [] |563| 
	.dwpsn	file "../hal.c",line 563,column 35,is_stmt
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      ACC,@_PAR_Capacity_Total ; [CPU_] |563| 
        MOVL      XAR4,#60000           ; [CPU_U] |563| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        SETC      SXM                   ; [CPU_] 
        SUBL      ACC,@_PAR_Capacity_Left ; [CPU_] |563| 
        MOVL      P,ACC                 ; [CPU_] |563| 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOVB      ACC,#0                ; [CPU_] |563| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |563| 
        MOV       ACC,@_ODV_Module1_Current ; [CPU_] |563| 
        MOVL      XAR6,ACC              ; [CPU_] |563| 
        MOVW      DP,#_ODV_Module1_Time_Remaining ; [CPU_U] 
        MOVB      ACC,#0                ; [CPU_] |563| 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |563| 
        MOV       @_ODV_Module1_Time_Remaining,P ; [CPU_] |563| 
$C$L88:    
	.dwpsn	file "../hal.c",line 565,column 5,is_stmt
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOV32     R0H,@_ODP_Battery_Capacity ; [CPU_] |565| 
        MPYF32    R0H,R0H,#16672        ; [CPU_] |565| 
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$233, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |565| 
        ; call occurs [#_CNV_Round] ; [] |565| 
        MOVW      DP,#_ODV_Module1_Capacity_set ; [CPU_U] 
        MOV       @_ODV_Module1_Capacity_set,AL ; [CPU_] |565| 
	.dwpsn	file "../hal.c",line 566,column 5,is_stmt
        MOVW      DP,#_ODV_Battery_Total_Cycles ; [CPU_U] 
        CMP       @_ODV_Battery_Total_Cycles,#2500 ; [CPU_] |566| 
        B         $C$L89,LOS            ; [CPU_] |566| 
        ; branchcc occurs ; [] |566| 
	.dwpsn	file "../hal.c",line 566,column 42,is_stmt
        MOVB      ACC,#128              ; [CPU_] |566| 
$C$DW$234	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$234, DW_AT_low_pc(0x00)
	.dwattr $C$DW$234, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$234, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |566| 
        ; call occurs [#_ERR_HandleWarning] ; [] |566| 
$C$L89:    
	.dwpsn	file "../hal.c",line 567,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |567| 
        MOVB      AL,#100               ; [CPU_] |567| 
$C$DW$235	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$235, DW_AT_low_pc(0x00)
	.dwattr $C$DW$235, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$235, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |567| 
        ; call occurs [#_SEM_pend] ; [] |567| 
	.dwpsn	file "../hal.c",line 410,column 9,is_stmt
        B         $C$L46,UNC            ; [CPU_] |410| 
        ; branch occurs ; [] |410| 
	.dwattr $C$DW$180, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$180, DW_AT_TI_end_line(0x239)
	.dwattr $C$DW$180, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$180

	.sect	".text"
	.global	_HAL_Unlock

$C$DW$236	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Unlock")
	.dwattr $C$DW$236, DW_AT_low_pc(_HAL_Unlock)
	.dwattr $C$DW$236, DW_AT_high_pc(0x00)
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_HAL_Unlock")
	.dwattr $C$DW$236, DW_AT_external
	.dwattr $C$DW$236, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$236, DW_AT_TI_begin_line(0x240)
	.dwattr $C$DW$236, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$236, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../hal.c",line 577,column 1,is_stmt,address _HAL_Unlock

	.dwfde $C$DW$CIE, _HAL_Unlock

;***************************************************************
;* FNAME: _HAL_Unlock                   FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_HAL_Unlock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$237	.dwtag  DW_TAG_variable, DW_AT_name("num")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_num")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_breg20 -2]
$C$DW$238	.dwtag  DW_TAG_variable, DW_AT_name("pass")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_pass")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_breg20 -4]
$C$DW$239	.dwtag  DW_TAG_variable, DW_AT_name("num2")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_num2")
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$239, DW_AT_location[DW_OP_breg20 -6]
$C$DW$240	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$240, DW_AT_location[DW_OP_breg20 -7]
	.dwpsn	file "../hal.c",line 580,column 3,is_stmt
        MOVW      DP,#_ODV_Security     ; [CPU_U] 
        MOVL      ACC,@_ODV_Security    ; [CPU_] |580| 
        MOVL      *-SP[2],ACC           ; [CPU_] |580| 
	.dwpsn	file "../hal.c",line 581,column 3,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |581| 
        MOVL      *-SP[6],ACC           ; [CPU_] |581| 
	.dwpsn	file "../hal.c",line 582,column 3,is_stmt
        MOV       T,#32                 ; [CPU_] |582| 
        MOVL      P,@_ODV_Security      ; [CPU_] |582| 
        MOVL      ACC,@_ODV_Security+2  ; [CPU_] |582| 
        LSR64     ACC:P,T               ; [CPU_] |582| 
        MOVL      *-SP[4],P             ; [CPU_] |582| 
	.dwpsn	file "../hal.c",line 583,column 8,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |583| 
	.dwpsn	file "../hal.c",line 583,column 13,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |583| 
        CMPB      AL,#11                ; [CPU_] |583| 
        B         $C$L95,HIS            ; [CPU_] |583| 
        ; branchcc occurs ; [] |583| 
$C$L90:    
	.dwpsn	file "../hal.c",line 584,column 5,is_stmt
        MOVZ      AR0,*-SP[7]           ; [CPU_] |584| 
        MOVL      XAR4,#_permtable      ; [CPU_U] |584| 
        MOVB      ACC,#1                ; [CPU_] |584| 
        MOV       T,*+XAR4[AR0]         ; [CPU_] |584| 
        LSLL      ACC,T                 ; [CPU_] |584| 
        AND       AL,*-SP[4]            ; [CPU_] |584| 
        AND       AH,*-SP[3]            ; [CPU_] |584| 
        TEST      ACC                   ; [CPU_] |584| 
        BF        $C$L91,EQ             ; [CPU_] |584| 
        ; branchcc occurs ; [] |584| 
        MOV       T,*+XAR4[AR0]         ; [CPU_] |584| 
        MOVB      ACC,#1                ; [CPU_] |584| 
        LSLL      ACC,T                 ; [CPU_] |584| 
        OR        *-SP[2],AL            ; [CPU_] |584| 
        OR        *-SP[1],AH            ; [CPU_] |584| 
        B         $C$L92,UNC            ; [CPU_] |584| 
        ; branch occurs ; [] |584| 
$C$L91:    
        MOV       T,*+XAR4[AR0]         ; [CPU_] |584| 
        MOVB      ACC,#1                ; [CPU_] |584| 
        LSLL      ACC,T                 ; [CPU_] |584| 
        NOT       ACC                   ; [CPU_] |584| 
        AND       *-SP[2],AL            ; [CPU_] |584| 
        AND       *-SP[1],AH            ; [CPU_] |584| 
$C$L92:    
	.dwpsn	file "../hal.c",line 585,column 5,is_stmt
        MOV       T,*+XAR4[AR0]         ; [CPU_] |585| 
        MOVB      ACC,#1                ; [CPU_] |585| 
        LSLL      ACC,T                 ; [CPU_] |585| 
        AND       AL,*-SP[6]            ; [CPU_] |585| 
        AND       AH,*-SP[5]            ; [CPU_] |585| 
        TEST      ACC                   ; [CPU_] |585| 
        BF        $C$L93,EQ             ; [CPU_] |585| 
        ; branchcc occurs ; [] |585| 
        MOV       T,*+XAR4[AR0]         ; [CPU_] |585| 
        MOVB      ACC,#1                ; [CPU_] |585| 
        LSLL      ACC,T                 ; [CPU_] |585| 
        OR        *-SP[4],AL            ; [CPU_] |585| 
        OR        *-SP[3],AH            ; [CPU_] |585| 
        B         $C$L94,UNC            ; [CPU_] |585| 
        ; branch occurs ; [] |585| 
$C$L93:    
        MOV       T,*+XAR4[AR0]         ; [CPU_] |585| 
        MOVB      ACC,#1                ; [CPU_] |585| 
        LSLL      ACC,T                 ; [CPU_] |585| 
        NOT       ACC                   ; [CPU_] |585| 
        AND       *-SP[4],AL            ; [CPU_] |585| 
        AND       *-SP[3],AH            ; [CPU_] |585| 
$C$L94:    
	.dwpsn	file "../hal.c",line 583,column 19,is_stmt
        INC       *-SP[7]               ; [CPU_] |583| 
	.dwpsn	file "../hal.c",line 583,column 13,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |583| 
        CMPB      AL,#11                ; [CPU_] |583| 
        B         $C$L90,LO             ; [CPU_] |583| 
        ; branchcc occurs ; [] |583| 
$C$L95:    
	.dwpsn	file "../hal.c",line 587,column 3,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |587| 
        CMPL      ACC,*-SP[2]           ; [CPU_] |587| 
        BF        $C$L97,NEQ            ; [CPU_] |587| 
        ; branchcc occurs ; [] |587| 
	.dwpsn	file "../hal.c",line 588,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |588| 
        MOVW      DP,#_ODP_Password     ; [CPU_U] 
        ADDL      @_ODP_Password,ACC    ; [CPU_] |588| 
	.dwpsn	file "../hal.c",line 589,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |589| 
        BF        $C$L96,NEQ            ; [CPU_] |589| 
        ; branchcc occurs ; [] |589| 
	.dwpsn	file "../hal.c",line 590,column 7,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |590| 
        MOVW      DP,#_ODP_Password     ; [CPU_U] 
        MOVL      @_ODP_Password,ACC    ; [CPU_] |590| 
$C$L96:    
	.dwpsn	file "../hal.c",line 592,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |592| 
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      @_ODP_RandomNB,ACC    ; [CPU_] |592| 
	.dwpsn	file "../hal.c",line 593,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |593| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |593| 
        MOV       AL,#8193              ; [CPU_] |593| 
$C$DW$241	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$241, DW_AT_low_pc(0x00)
	.dwattr $C$DW$241, DW_AT_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$241, DW_AT_TI_call
        LCR       #_PAR_StoreODSubIndex ; [CPU_] |593| 
        ; call occurs [#_PAR_StoreODSubIndex] ; [] |593| 
	.dwpsn	file "../hal.c",line 594,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |594| 
        MOV       AL,#8451              ; [CPU_] |594| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |594| 
$C$DW$242	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$242, DW_AT_low_pc(0x00)
	.dwattr $C$DW$242, DW_AT_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$242, DW_AT_TI_call
        LCR       #_PAR_StoreODSubIndex ; [CPU_] |594| 
        ; call occurs [#_PAR_StoreODSubIndex] ; [] |594| 
	.dwpsn	file "../hal.c",line 595,column 5,is_stmt
$C$DW$243	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$243, DW_AT_low_pc(0x00)
	.dwattr $C$DW$243, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$243, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |595| 
        ; call occurs [#_ERR_ClearWarning] ; [] |595| 
$C$L97:    
	.dwpsn	file "../hal.c",line 597,column 3,is_stmt
        ZAPA      ; [CPU_] |597| 
        MOVW      DP,#_ODV_Security     ; [CPU_U] 
        MOVL      @_ODV_Security,P      ; [CPU_] |597| 
        MOVL      @_ODV_Security+2,ACC  ; [CPU_] |597| 
	.dwpsn	file "../hal.c",line 598,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$244	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$244, DW_AT_low_pc(0x00)
	.dwattr $C$DW$244, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$236, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$236, DW_AT_TI_end_line(0x256)
	.dwattr $C$DW$236, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$236

	.sect	".text"
	.global	_HAL_Random

$C$DW$245	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Random")
	.dwattr $C$DW$245, DW_AT_low_pc(_HAL_Random)
	.dwattr $C$DW$245, DW_AT_high_pc(0x00)
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_HAL_Random")
	.dwattr $C$DW$245, DW_AT_external
	.dwattr $C$DW$245, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$245, DW_AT_TI_begin_line(0x258)
	.dwattr $C$DW$245, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$245, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../hal.c",line 601,column 1,is_stmt,address _HAL_Random

	.dwfde $C$DW$CIE, _HAL_Random

;***************************************************************
;* FNAME: _HAL_Random                   FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_HAL_Random:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../hal.c",line 603,column 3,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOV       AL,@_ODP_OnTime       ; [CPU_] |603| 
$C$DW$246	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$246, DW_AT_low_pc(0x00)
	.dwattr $C$DW$246, DW_AT_name("_srand")
	.dwattr $C$DW$246, DW_AT_TI_call
        LCR       #_srand               ; [CPU_] |603| 
        ; call occurs [#_srand] ; [] |603| 
	.dwpsn	file "../hal.c",line 604,column 3,is_stmt
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_name("_rand")
	.dwattr $C$DW$247, DW_AT_TI_call
        LCR       #_rand                ; [CPU_] |604| 
        ; call occurs [#_rand] ; [] |604| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,AL                ; [CPU_] |604| 
        MOV32     R0H,ACC               ; [CPU_] |604| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#18303            ; [CPU_] |604| 
        I32TOF32  R0H,R0H               ; [CPU_] |604| 
        MOVXI     R1H,#65280            ; [CPU_] |604| 
        MPYF32    R0H,R0H,R1H           ; [CPU_] |604| 
        MOVIZ     R1H,#18176            ; [CPU_] |604| 
$C$DW$248	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$248, DW_AT_low_pc(0x00)
	.dwattr $C$DW$248, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$248, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |604| 
        ; call occurs [#FS$$DIV] ; [] |604| 
        F32TOUI32 R0H,R0H               ; [CPU_] |604| 
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOV32     @_ODP_RandomNB,R0H    ; [CPU_] |604| 
	.dwpsn	file "../hal.c",line 605,column 3,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |605| 
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        SFR       ACC,8                 ; [CPU_] |605| 
        MOVL      P,@_ODP_OnTime        ; [CPU_] |605| 
        ADD       AL,PH                 ; [CPU_] |605| 
$C$DW$249	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$249, DW_AT_low_pc(0x00)
	.dwattr $C$DW$249, DW_AT_name("_srand")
	.dwattr $C$DW$249, DW_AT_TI_call
        LCR       #_srand               ; [CPU_] |605| 
        ; call occurs [#_srand] ; [] |605| 
	.dwpsn	file "../hal.c",line 606,column 3,is_stmt
$C$DW$250	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$250, DW_AT_low_pc(0x00)
	.dwattr $C$DW$250, DW_AT_name("_rand")
	.dwattr $C$DW$250, DW_AT_TI_call
        LCR       #_rand                ; [CPU_] |606| 
        ; call occurs [#_rand] ; [] |606| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,AL                ; [CPU_] |606| 
        MOV32     R0H,ACC               ; [CPU_] |606| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#18303            ; [CPU_] |606| 
        I32TOF32  R0H,R0H               ; [CPU_] |606| 
        MOVXI     R1H,#65280            ; [CPU_] |606| 
        MPYF32    R0H,R0H,R1H           ; [CPU_] |606| 
        MOVIZ     R1H,#18176            ; [CPU_] |606| 
$C$DW$251	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$251, DW_AT_low_pc(0x00)
	.dwattr $C$DW$251, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$251, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |606| 
        ; call occurs [#FS$$DIV] ; [] |606| 
        F32TOUI32 R0H,R0H               ; [CPU_] |606| 
        NOP       ; [CPU_] 
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOV32     ACC,R0H               ; [CPU_] |606| 
        LSL       ACC,16                ; [CPU_] |606| 
        ADDL      @_ODP_RandomNB,ACC    ; [CPU_] |606| 
	.dwpsn	file "../hal.c",line 607,column 3,is_stmt
        MOVB      AH,#0                 ; [CPU_] |607| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOV       AL,#8451              ; [CPU_] |607| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |607| 
$C$DW$252	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$252, DW_AT_low_pc(0x00)
	.dwattr $C$DW$252, DW_AT_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$252, DW_AT_TI_call
        LCR       #_PAR_StoreODSubIndex ; [CPU_] |607| 
        ; call occurs [#_PAR_StoreODSubIndex] ; [] |607| 
	.dwpsn	file "../hal.c",line 608,column 1,is_stmt
$C$DW$253	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$253, DW_AT_low_pc(0x00)
	.dwattr $C$DW$253, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$245, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$245, DW_AT_TI_end_line(0x260)
	.dwattr $C$DW$245, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$245

	.sect	".text:retain"
	.global	_HAL_Dummy

$C$DW$254	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Dummy")
	.dwattr $C$DW$254, DW_AT_low_pc(_HAL_Dummy)
	.dwattr $C$DW$254, DW_AT_high_pc(0x00)
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_HAL_Dummy")
	.dwattr $C$DW$254, DW_AT_external
	.dwattr $C$DW$254, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$254, DW_AT_TI_begin_line(0x262)
	.dwattr $C$DW$254, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$254, DW_AT_TI_interrupt
	.dwattr $C$DW$254, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../hal.c",line 611,column 1,is_stmt,address _HAL_Dummy

	.dwfde $C$DW$CIE, _HAL_Dummy

;***************************************************************
;* FNAME: _HAL_Dummy                    FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  4 SOE     *
;***************************************************************

_HAL_Dummy:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 4
	.dwcfi	save_reg_to_mem, 40, 5
	.dwcfi	cfa_offset, -6
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
	.dwpsn	file "../hal.c",line 612,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        MOV       @_PieCtrlRegs+1,#4095 ; [CPU_] |612| 
	.dwpsn	file "../hal.c",line 613,column 1,is_stmt
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$255	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$255, DW_AT_low_pc(0x00)
	.dwattr $C$DW$255, DW_AT_TI_return
        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$254, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$254, DW_AT_TI_end_line(0x265)
	.dwattr $C$DW$254, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$254

	.sect	".text"
	.global	_HAL_Reset

$C$DW$256	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Reset")
	.dwattr $C$DW$256, DW_AT_low_pc(_HAL_Reset)
	.dwattr $C$DW$256, DW_AT_high_pc(0x00)
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_HAL_Reset")
	.dwattr $C$DW$256, DW_AT_external
	.dwattr $C$DW$256, DW_AT_TI_begin_file("../hal.c")
	.dwattr $C$DW$256, DW_AT_TI_begin_line(0x267)
	.dwattr $C$DW$256, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$256, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../hal.c",line 615,column 21,is_stmt,address _HAL_Reset

	.dwfde $C$DW$CIE, _HAL_Reset

;***************************************************************
;* FNAME: _HAL_Reset                    FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_HAL_Reset:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../hal.c",line 616,column 3,is_stmt
 setc INTM
	.dwpsn	file "../hal.c",line 617,column 3,is_stmt
 EALLOW
	.dwpsn	file "../hal.c",line 618,column 3,is_stmt
        MOVW      DP,#_AdcRegs+12       ; [CPU_U] 
        AND       @_AdcRegs+12,#0xffdf  ; [CPU_] |618| 
	.dwpsn	file "../hal.c",line 619,column 3,is_stmt
        AND       @_AdcRegs+8,#0xdfff   ; [CPU_] |619| 
	.dwpsn	file "../hal.c",line 621,column 3,is_stmt
        MOVL      XAR4,#_HAL_Dummy      ; [CPU_U] |621| 
        MOVW      DP,#_PieVectTable+176 ; [CPU_U] 
        MOVL      @_PieVectTable+176,XAR4 ; [CPU_] |621| 
	.dwpsn	file "../hal.c",line 622,column 3,is_stmt
        MOVW      DP,#_PieVectTable+66  ; [CPU_U] 
        MOVL      @_PieVectTable+66,XAR4 ; [CPU_] |622| 
	.dwpsn	file "../hal.c",line 623,column 3,is_stmt
        MOVL      @_PieVectTable+74,XAR4 ; [CPU_] |623| 
	.dwpsn	file "../hal.c",line 624,column 3,is_stmt
        MOVW      DP,#_PieVectTable+128 ; [CPU_U] 
        MOVL      @_PieVectTable+128,XAR4 ; [CPU_] |624| 
	.dwpsn	file "../hal.c",line 625,column 3,is_stmt
        MOVW      DP,#_PieVectTable+202 ; [CPU_U] 
        MOVL      @_PieVectTable+202,XAR4 ; [CPU_] |625| 
	.dwpsn	file "../hal.c",line 626,column 3,is_stmt
        MOVL      @_PieVectTable+200,XAR4 ; [CPU_] |626| 
	.dwpsn	file "../hal.c",line 627,column 3,is_stmt
        MOVW      DP,#_PieVectTable+142 ; [CPU_U] 
        MOVL      @_PieVectTable+142,XAR4 ; [CPU_] |627| 
	.dwpsn	file "../hal.c",line 629,column 3,is_stmt
        MOVW      DP,#_EQep1Regs+24     ; [CPU_U] 
        AND       @_EQep1Regs+24,#0xf7ff ; [CPU_] |629| 
	.dwpsn	file "../hal.c",line 630,column 3,is_stmt
 EDIS
	.dwpsn	file "../hal.c",line 631,column 3,is_stmt
 clrc INTM
	.dwpsn	file "../hal.c",line 632,column 3,is_stmt
$C$L98:    
	.dwpsn	file "../hal.c",line 632,column 9,is_stmt
        MOVW      DP,#_PieCtrlRegs+3    ; [CPU_U] 
        MOV       AL,@_PieCtrlRegs+3    ; [CPU_] |632| 
        BF        $C$L98,NEQ            ; [CPU_] |632| 
        ; branchcc occurs ; [] |632| 
	.dwpsn	file "../hal.c",line 633,column 3,is_stmt
        MOV       @_PieCtrlRegs+2,#0    ; [CPU_] |633| 
	.dwpsn	file "../hal.c",line 634,column 3,is_stmt
$C$L99:    
	.dwpsn	file "../hal.c",line 634,column 9,is_stmt
        MOV       AL,@_PieCtrlRegs+11   ; [CPU_] |634| 
        BF        $C$L99,NEQ            ; [CPU_] |634| 
        ; branchcc occurs ; [] |634| 
	.dwpsn	file "../hal.c",line 635,column 3,is_stmt
        MOV       @_PieCtrlRegs+10,#0   ; [CPU_] |635| 
	.dwpsn	file "../hal.c",line 636,column 3,is_stmt
$C$L100:    
	.dwpsn	file "../hal.c",line 636,column 9,is_stmt
        MOV       AL,@_PieCtrlRegs+17   ; [CPU_] |636| 
        BF        $C$L100,NEQ           ; [CPU_] |636| 
        ; branchcc occurs ; [] |636| 
	.dwpsn	file "../hal.c",line 637,column 3,is_stmt
        MOV       @_PieCtrlRegs+16,#0   ; [CPU_] |637| 
	.dwpsn	file "../hal.c",line 638,column 3,is_stmt
$C$L101:    
	.dwpsn	file "../hal.c",line 638,column 9,is_stmt
        MOV       AL,@_PieCtrlRegs+19   ; [CPU_] |638| 
        BF        $C$L101,NEQ           ; [CPU_] |638| 
        ; branchcc occurs ; [] |638| 
	.dwpsn	file "../hal.c",line 639,column 3,is_stmt
        MOV       @_PieCtrlRegs+18,#0   ; [CPU_] |639| 
	.dwpsn	file "../hal.c",line 641,column 3,is_stmt
        MOV       @_PieCtrlRegs+1,#65535 ; [CPU_] |641| 
	.dwpsn	file "../hal.c",line 642,column 3,is_stmt
        AND       @_PieCtrlRegs+2,#0xffdf ; [CPU_] |642| 
	.dwpsn	file "../hal.c",line 644,column 3,is_stmt
        AND       IER,#0                ; [CPU_] |644| 
	.dwpsn	file "../hal.c",line 645,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
$C$DW$257	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$257, DW_AT_low_pc(0x00)
	.dwattr $C$DW$257, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$256, DW_AT_TI_end_file("../hal.c")
	.dwattr $C$DW$256, DW_AT_TI_end_line(0x285)
	.dwattr $C$DW$256, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$256

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_srand
	.global	_ERR_SetError
	.global	_SCI1_Update
	.global	_ERR_ErrorOverVoltage
	.global	_ERR_HandleWarning
	.global	_REC_Record
	.global	_ERR_ErrorOverTemp
	.global	_ERR_ErrorComm
	.global	_ERR_ErrorDigOvld
	.global	_ODP_CommError_Delay
	.global	_ODP_Board_Deactive_FF
	.global	_ODP_CommError_TimeOut
	.global	_ODP_Battery_Cycles
	.global	_ODV_Module1_Voltage
	.global	_ODV_Module1_SOC
	.global	_ODV_Module1_Temperature
	.global	_ODV_Module1_Current
	.global	_ODV_Battery_Total_ChargedkWh
	.global	_ODV_Battery_Total_Cycles
	.global	_ODV_SOC_SOC2
	.global	_ODV_SOC_SOC1
	.global	_ODP_Contactor_Slope
	.global	_ODP_Contactor_Undercurrent
	.global	_ODP_Contactor_Overcurrent
	.global	_ODP_Contactor_Setup
	.global	_ODP_Contactor_Hold
	.global	_ODV_Current_DischargeAllowed
	.global	_ODV_Heater_Heater_Status
	.global	_ODV_MachineMode
	.global	_ODP_Heater_Off_Cell_Voltage
	.global	_ODP_Heater_OnTemp
	.global	_ODP_Heater_OffTemp
	.global	_ODP_Contactor_Delay
	.global	_ODV_Module1_Time_Remaining
	.global	_ODV_Module1_Module_SOC_Current
	.global	_ODV_Module1_Capacity_Used
	.global	_ODV_Module1_Capacity_set
	.global	_ODV_Module1_SOH
	.global	_ODV_Module1_Throughput
	.global	_ODP_SafetyLimits_Tmax
	.global	_rand
	.global	_ODP_SafetyLimits_Umax
	.global	_ODP_SafetyLimits_Tmin
	.global	_ODP_SafetyLimits_Imax_charge
	.global	_ODP_SafetyLimits_Umin
	.global	_ODV_Recorder_Start
	.global	_ODV_Gateway_Status
	.global	_ERR_ClearWarning
	.global	_ODV_Recorder_Control
	.global	_MBX_post
	.global	_SEM_pend
	.global	_ODP_SafetyLimits_Temp_Delay
	.global	_ODV_Current_ChargeAllowed
	.global	_SCI_GpioState
	.global	_ERR_ClearError
	.global	_ODV_Temperature_Average_data
	.global	_ODP_SafetyLimits_UnderVoltage
	.global	_ODP_SafetyLimits_Cell_Nb
	.global	_ODP_SafetyLimits_OverVoltage
	.global	_ODP_SafetyLimits_Imax_dis
	.global	_ODP_SafetyLimits_Voltage_delay
	.global	_PAR_Capacity_Total
	.global	_CommTimeout
	.global	_PAR_StoreODSubIndex
	.global	_PAR_WriteStatisticParam
	.global	_CNV_Round
	.global	_PAR_Capacity_Left
	.global	_ODV_SysTick_ms
	.global	_ODP_Battery_Capacity
	.global	_ODP_RandomNB
	.global	_ODV_Read_Inputs_16_Bit
	.global	_ODV_Write_Outputs_16_Bit
	.global	_ODP_Password
	.global	_ODV_Recorder_Period
	.global	_BoardODdata
	.global	_ODP_OnTime
	.global	_ODV_ErrorDsp_ErrorNumber
	.global	_ODV_Security
	.global	_PAR_Capacity_TotalLife_Used
	.global	_ODV_Recorder_Vectors
	.global	_ODP_Temperature_DischargeLimits
	.global	_ODP_Power_DischargeLimits
	.global	_ODP_Temperature_ChargeLimits
	.global	_ODP_Power_ChargeLimits
	.global	_TSK_timerSem
	.global	_ODV_Write_Analogue_Output_16_Bit
	.global	_AdcResult
	.global	_ODV_Read_Analogue_Input_16_Bit
	.global	_PieCtrlRegs
	.global	_ODP_Analogue_Output_Scaling_Float
	.global	_GpioDataRegs
	.global	_EQep1Regs
	.global	_sci_rx_mbox
	.global	_EPwm2Regs
	.global	_EPwm3Regs
	.global	_AdcRegs
	.global	_TempTable
	.global	_PieVectTable
	.global	FS$$DIV
	.global	I$$DIV
	.global	L$$DIV
	.global	ULL$$DIV
	.global	LL$$MPY
	.global	ULL$$TOFS

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$258, DW_AT_name("cob_id")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$259, DW_AT_name("rtr")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$260, DW_AT_name("len")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$261, DW_AT_name("data")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$251	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$251, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$251, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$262	.dwtag  DW_TAG_member
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$262, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$262, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$262, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$263, DW_AT_name("csSDO")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$264, DW_AT_name("csEmergency")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$264, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$265, DW_AT_name("csSYNC")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$266	.dwtag  DW_TAG_member
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$266, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$266, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$266, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$267	.dwtag  DW_TAG_member
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$267, DW_AT_name("csPDO")
	.dwattr $C$DW$267, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$267, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$267, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$268	.dwtag  DW_TAG_member
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$268, DW_AT_name("csLSS")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$268, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$268, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$208	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$208, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$208, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$269	.dwtag  DW_TAG_member
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$269, DW_AT_name("errCode")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$269, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$269, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$270, DW_AT_name("errRegMask")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$270, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$271	.dwtag  DW_TAG_member
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$271, DW_AT_name("active")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$271, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$242	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$242, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$242, DW_AT_language(DW_LANG_C)

$C$DW$T$243	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$243, DW_AT_type(*$C$DW$T$242)
	.dwattr $C$DW$T$243, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$243, DW_AT_byte_size(0x18)
$C$DW$272	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$272, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$243


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$273	.dwtag  DW_TAG_member
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$273, DW_AT_name("index")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$273, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$274	.dwtag  DW_TAG_member
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$274, DW_AT_name("subindex")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$274, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$274, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$275	.dwtag  DW_TAG_member
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$275, DW_AT_name("size")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$275, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$275, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$276	.dwtag  DW_TAG_member
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$276, DW_AT_name("address")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$276, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$276, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$248	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$248, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$248, DW_AT_language(DW_LANG_C)
$C$DW$T$249	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$249, DW_AT_type(*$C$DW$T$248)
	.dwattr $C$DW$T$249, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$277	.dwtag  DW_TAG_member
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$277, DW_AT_name("Pos_Record")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_Pos_Record")
	.dwattr $C$DW$277, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$277, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$277, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$278	.dwtag  DW_TAG_member
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$278, DW_AT_name("Cur_Record")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_Cur_Record")
	.dwattr $C$DW$278, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$278, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$278, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$279	.dwtag  DW_TAG_member
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$279, DW_AT_name("AutoTrigg")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_AutoTrigg")
	.dwattr $C$DW$279, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$279, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$279, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$280	.dwtag  DW_TAG_member
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$280, DW_AT_name("AutoRecord")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_AutoRecord")
	.dwattr $C$DW$280, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$280, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$280, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$281	.dwtag  DW_TAG_member
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$281, DW_AT_name("ContinuousRecord")
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_ContinuousRecord")
	.dwattr $C$DW$281, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$281, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$281, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$282	.dwtag  DW_TAG_member
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$282, DW_AT_name("Trig_Record")
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_Trig_Record")
	.dwattr $C$DW$282, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$282, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$282, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$283	.dwtag  DW_TAG_member
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$283, DW_AT_name("unused")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_unused")
	.dwattr $C$DW$283, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0a)
	.dwattr $C$DW$283, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$283, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24

$C$DW$T$260	.dwtag  DW_TAG_typedef, DW_AT_name("TControl")
	.dwattr $C$DW$T$260, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$260, DW_AT_language(DW_LANG_C)

$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x06)
$C$DW$284	.dwtag  DW_TAG_member
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$284, DW_AT_name("temp")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$284, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$284, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$285	.dwtag  DW_TAG_member
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$285, DW_AT_name("adval")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_adval")
	.dwattr $C$DW$285, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$285, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26

$C$DW$T$261	.dwtag  DW_TAG_typedef, DW_AT_name("TTempTable")
	.dwattr $C$DW$T$261, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$261, DW_AT_language(DW_LANG_C)
$C$DW$286	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$261)
$C$DW$T$262	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$262, DW_AT_type(*$C$DW$286)

$C$DW$T$263	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$263, DW_AT_type(*$C$DW$T$262)
	.dwattr $C$DW$T$263, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$263, DW_AT_byte_size(0xde)
$C$DW$287	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$287, DW_AT_upper_bound(0x24)
	.dwendtag $C$DW$T$263


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$288	.dwtag  DW_TAG_member
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$288, DW_AT_name("can_wk")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_can_wk")
	.dwattr $C$DW$288, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$288, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$288, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$289, DW_AT_name("sw_wk")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_sw_wk")
	.dwattr $C$DW$289, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$290, DW_AT_name("bal_dic")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_bal_dic")
	.dwattr $C$DW$290, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$291	.dwtag  DW_TAG_member
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$291, DW_AT_name("bal_ch")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_bal_ch")
	.dwattr $C$DW$291, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$291, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$292	.dwtag  DW_TAG_member
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$292, DW_AT_name("bal_ocv")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_bal_ocv")
	.dwattr $C$DW$292, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$292, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$292, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$293	.dwtag  DW_TAG_member
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$293, DW_AT_name("lem")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_lem")
	.dwattr $C$DW$293, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$293, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$294, DW_AT_name("shunt")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_shunt")
	.dwattr $C$DW$294, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$295	.dwtag  DW_TAG_member
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$295, DW_AT_name("relay_on")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_relay_on")
	.dwattr $C$DW$295, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$295, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$295, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$296	.dwtag  DW_TAG_member
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$296, DW_AT_name("ch_wk")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_ch_wk")
	.dwattr $C$DW$296, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$296, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$296, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$297, DW_AT_name("SOC2")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_SOC2")
	.dwattr $C$DW$297, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$297, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$298	.dwtag  DW_TAG_member
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$298, DW_AT_name("foil")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_foil")
	.dwattr $C$DW$298, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$298, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$299	.dwtag  DW_TAG_member
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$299, DW_AT_name("rel_sta")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_rel_sta")
	.dwattr $C$DW$299, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$299, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$299, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$300	.dwtag  DW_TAG_member
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$300, DW_AT_name("b12")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_b12")
	.dwattr $C$DW$300, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$300, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$301	.dwtag  DW_TAG_member
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$301, DW_AT_name("b13")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_b13")
	.dwattr $C$DW$301, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$301, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$301, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$302	.dwtag  DW_TAG_member
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$302, DW_AT_name("b14")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_b14")
	.dwattr $C$DW$302, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$302, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$302, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$303	.dwtag  DW_TAG_member
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$303, DW_AT_name("b15")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_b15")
	.dwattr $C$DW$303, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$303, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$303, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27

$C$DW$T$265	.dwtag  DW_TAG_typedef, DW_AT_name("TMMSConfig")
	.dwattr $C$DW$T$265, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$265, DW_AT_language(DW_LANG_C)
$C$DW$T$266	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$266, DW_AT_type(*$C$DW$T$265)
	.dwattr $C$DW$T$266, DW_AT_address_class(0x16)

$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_name("ADCCTL1_BITS")
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x01)
$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$304, DW_AT_name("TEMPCONV")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_TEMPCONV")
	.dwattr $C$DW$304, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$304, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$305, DW_AT_name("VREFLOCONV")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_VREFLOCONV")
	.dwattr $C$DW$305, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$305, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$306, DW_AT_name("INTPULSEPOS")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_INTPULSEPOS")
	.dwattr $C$DW$306, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$307, DW_AT_name("ADCREFSEL")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_ADCREFSEL")
	.dwattr $C$DW$307, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$308, DW_AT_name("rsvd1")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$308, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$309, DW_AT_name("ADCREFPWD")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_ADCREFPWD")
	.dwattr $C$DW$309, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$310, DW_AT_name("ADCBGPWD")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_ADCBGPWD")
	.dwattr $C$DW$310, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$311, DW_AT_name("ADCPWDN")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_ADCPWDN")
	.dwattr $C$DW$311, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$312, DW_AT_name("ADCBSYCHN")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_ADCBSYCHN")
	.dwattr $C$DW$312, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$312, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$313, DW_AT_name("ADCBSY")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_ADCBSY")
	.dwattr $C$DW$313, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$314, DW_AT_name("ADCENABLE")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_ADCENABLE")
	.dwattr $C$DW$314, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$315, DW_AT_name("RESET")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_RESET")
	.dwattr $C$DW$315, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$30, DW_AT_name("ADCCTL1_REG")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x01)
$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$316, DW_AT_name("all")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$317, DW_AT_name("bit")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("ADCCTL2_BITS")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$318, DW_AT_name("CLKDIV2EN")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_CLKDIV2EN")
	.dwattr $C$DW$318, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$319, DW_AT_name("ADCNONOVERLAP")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_ADCNONOVERLAP")
	.dwattr $C$DW$319, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$320, DW_AT_name("CLKDIV4EN")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_CLKDIV4EN")
	.dwattr $C$DW$320, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$321, DW_AT_name("rsvd1")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$321, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0d)
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$32, DW_AT_name("ADCCTL2_REG")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x01)
$C$DW$322	.dwtag  DW_TAG_member
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$322, DW_AT_name("all")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$322, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$323, DW_AT_name("bit")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("ADCINTSOCSEL1_BITS")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x01)
$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$324, DW_AT_name("SOC0")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_SOC0")
	.dwattr $C$DW$324, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$324, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$325, DW_AT_name("SOC1")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_SOC1")
	.dwattr $C$DW$325, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$326, DW_AT_name("SOC2")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_SOC2")
	.dwattr $C$DW$326, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$327, DW_AT_name("SOC3")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_SOC3")
	.dwattr $C$DW$327, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$328, DW_AT_name("SOC4")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_SOC4")
	.dwattr $C$DW$328, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$329, DW_AT_name("SOC5")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_SOC5")
	.dwattr $C$DW$329, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$330, DW_AT_name("SOC6")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_SOC6")
	.dwattr $C$DW$330, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$331, DW_AT_name("SOC7")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_SOC7")
	.dwattr $C$DW$331, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$34, DW_AT_name("ADCINTSOCSEL1_REG")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x01)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$332, DW_AT_name("all")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$333, DW_AT_name("bit")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$35, DW_AT_name("ADCINTSOCSEL2_BITS")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x01)
$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$334, DW_AT_name("SOC8")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_SOC8")
	.dwattr $C$DW$334, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$335, DW_AT_name("SOC9")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_SOC9")
	.dwattr $C$DW$335, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$336, DW_AT_name("SOC10")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_SOC10")
	.dwattr $C$DW$336, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$337, DW_AT_name("SOC11")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_SOC11")
	.dwattr $C$DW$337, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$338, DW_AT_name("SOC12")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_SOC12")
	.dwattr $C$DW$338, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$339, DW_AT_name("SOC13")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_SOC13")
	.dwattr $C$DW$339, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$340, DW_AT_name("SOC14")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_SOC14")
	.dwattr $C$DW$340, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$341, DW_AT_name("SOC15")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_SOC15")
	.dwattr $C$DW$341, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35


$C$DW$T$36	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$36, DW_AT_name("ADCINTSOCSEL2_REG")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x01)
$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$342, DW_AT_name("all")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$343, DW_AT_name("bit")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$36


$C$DW$T$37	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$37, DW_AT_name("ADCINT_BITS")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x01)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$344, DW_AT_name("ADCINT1")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_ADCINT1")
	.dwattr $C$DW$344, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$345, DW_AT_name("ADCINT2")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_ADCINT2")
	.dwattr $C$DW$345, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$346, DW_AT_name("ADCINT3")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_ADCINT3")
	.dwattr $C$DW$346, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$347, DW_AT_name("ADCINT4")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_ADCINT4")
	.dwattr $C$DW$347, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$348, DW_AT_name("ADCINT5")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_ADCINT5")
	.dwattr $C$DW$348, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$349, DW_AT_name("ADCINT6")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_ADCINT6")
	.dwattr $C$DW$349, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$350, DW_AT_name("ADCINT7")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_ADCINT7")
	.dwattr $C$DW$350, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$351, DW_AT_name("ADCINT8")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_ADCINT8")
	.dwattr $C$DW$351, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$352, DW_AT_name("ADCINT9")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_ADCINT9")
	.dwattr $C$DW$352, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$353, DW_AT_name("rsvd1")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$353, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x07)
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$37


$C$DW$T$38	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$38, DW_AT_name("ADCINT_REG")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x01)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$354, DW_AT_name("all")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$355, DW_AT_name("bit")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38


$C$DW$T$39	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$39, DW_AT_name("ADCOFFTRIM_BITS")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x01)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$356, DW_AT_name("OFFTRIM")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_OFFTRIM")
	.dwattr $C$DW$356, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x09)
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$357, DW_AT_name("rsvd1")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$357, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x07)
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$39


$C$DW$T$40	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$40, DW_AT_name("ADCOFFTRIM_REG")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x01)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$358, DW_AT_name("all")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$359, DW_AT_name("bit")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40


$C$DW$T$41	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$41, DW_AT_name("ADCREFTRIM_BITS")
	.dwattr $C$DW$T$41, DW_AT_byte_size(0x01)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$360, DW_AT_name("BG_FINE_TRIM")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_BG_FINE_TRIM")
	.dwattr $C$DW$360, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$361, DW_AT_name("BG_COARSE_TRIM")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_BG_COARSE_TRIM")
	.dwattr $C$DW$361, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x04)
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$362, DW_AT_name("EXTREF_FINE_TRIM")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_EXTREF_FINE_TRIM")
	.dwattr $C$DW$362, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x05)
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$363, DW_AT_name("rsvd1")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$363, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$41


$C$DW$T$42	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$42, DW_AT_name("ADCREFTRIM_REG")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x01)
$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$364, DW_AT_name("all")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$365, DW_AT_name("bit")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42


$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("ADCSAMPLEMODE_BITS")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x01)
$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$366, DW_AT_name("SIMULEN0")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_SIMULEN0")
	.dwattr $C$DW$366, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$367, DW_AT_name("SIMULEN2")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_SIMULEN2")
	.dwattr $C$DW$367, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$368, DW_AT_name("SIMULEN4")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_SIMULEN4")
	.dwattr $C$DW$368, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$369, DW_AT_name("SIMULEN6")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_SIMULEN6")
	.dwattr $C$DW$369, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$370, DW_AT_name("SIMULEN8")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_SIMULEN8")
	.dwattr $C$DW$370, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$371, DW_AT_name("SIMULEN10")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_SIMULEN10")
	.dwattr $C$DW$371, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$372, DW_AT_name("SIMULEN12")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_SIMULEN12")
	.dwattr $C$DW$372, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$373, DW_AT_name("SIMULEN14")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_SIMULEN14")
	.dwattr $C$DW$373, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$374, DW_AT_name("rsvd1")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$374, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$43


$C$DW$T$44	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$44, DW_AT_name("ADCSAMPLEMODE_REG")
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x01)
$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$375, DW_AT_name("all")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$376	.dwtag  DW_TAG_member
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$376, DW_AT_name("bit")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$376, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$44


$C$DW$T$45	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$45, DW_AT_name("ADCSOC_BITS")
	.dwattr $C$DW$T$45, DW_AT_byte_size(0x01)
$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$377, DW_AT_name("SOC0")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_SOC0")
	.dwattr $C$DW$377, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$377, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$378, DW_AT_name("SOC1")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_SOC1")
	.dwattr $C$DW$378, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$379, DW_AT_name("SOC2")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_SOC2")
	.dwattr $C$DW$379, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$380, DW_AT_name("SOC3")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_SOC3")
	.dwattr $C$DW$380, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$381, DW_AT_name("SOC4")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_SOC4")
	.dwattr $C$DW$381, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$382, DW_AT_name("SOC5")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_SOC5")
	.dwattr $C$DW$382, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$383, DW_AT_name("SOC6")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_SOC6")
	.dwattr $C$DW$383, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$384, DW_AT_name("SOC7")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_SOC7")
	.dwattr $C$DW$384, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$385	.dwtag  DW_TAG_member
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$385, DW_AT_name("SOC8")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_SOC8")
	.dwattr $C$DW$385, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$385, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$385, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$386	.dwtag  DW_TAG_member
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$386, DW_AT_name("SOC9")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_SOC9")
	.dwattr $C$DW$386, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$386, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$386, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$387	.dwtag  DW_TAG_member
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$387, DW_AT_name("SOC10")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_SOC10")
	.dwattr $C$DW$387, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$387, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$387, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$388, DW_AT_name("SOC11")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_SOC11")
	.dwattr $C$DW$388, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$388, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$389, DW_AT_name("SOC12")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_SOC12")
	.dwattr $C$DW$389, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$390, DW_AT_name("SOC13")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_SOC13")
	.dwattr $C$DW$390, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$391, DW_AT_name("SOC14")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_SOC14")
	.dwattr $C$DW$391, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$392, DW_AT_name("SOC15")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_SOC15")
	.dwattr $C$DW$392, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$45


$C$DW$T$46	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$46, DW_AT_name("ADCSOC_REG")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x01)
$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$393, DW_AT_name("all")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$394, DW_AT_name("bit")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$46


$C$DW$T$47	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$47, DW_AT_name("ADCSOCxCTL_BITS")
	.dwattr $C$DW$T$47, DW_AT_byte_size(0x01)
$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$395, DW_AT_name("ACQPS")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_ACQPS")
	.dwattr $C$DW$395, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x06)
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$396, DW_AT_name("CHSEL")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_CHSEL")
	.dwattr $C$DW$396, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x04)
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$397, DW_AT_name("rsvd1")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$397, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$398, DW_AT_name("TRIGSEL")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_TRIGSEL")
	.dwattr $C$DW$398, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$47


$C$DW$T$48	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$48, DW_AT_name("ADCSOCxCTL_REG")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x01)
$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$399, DW_AT_name("all")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$400, DW_AT_name("bit")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$48


$C$DW$T$52	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$52, DW_AT_name("ADC_REGS")
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x50)
$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$401, DW_AT_name("ADCCTL1")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_ADCCTL1")
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$402, DW_AT_name("ADCCTL2")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_ADCCTL2")
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$403, DW_AT_name("rsvd1")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$404, DW_AT_name("rsvd2")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$405, DW_AT_name("ADCINTFLG")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_ADCINTFLG")
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$406, DW_AT_name("ADCINTFLGCLR")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_ADCINTFLGCLR")
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$407, DW_AT_name("ADCINTOVF")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_ADCINTOVF")
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$408, DW_AT_name("ADCINTOVFCLR")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_ADCINTOVFCLR")
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$409	.dwtag  DW_TAG_member
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$409, DW_AT_name("INTSEL1N2")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_INTSEL1N2")
	.dwattr $C$DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$409, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$410, DW_AT_name("INTSEL3N4")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_INTSEL3N4")
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$411, DW_AT_name("INTSEL5N6")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_INTSEL5N6")
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$412, DW_AT_name("INTSEL7N8")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_INTSEL7N8")
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$413, DW_AT_name("INTSEL9N10")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_INTSEL9N10")
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$414, DW_AT_name("rsvd3")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$415, DW_AT_name("rsvd4")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$416, DW_AT_name("rsvd5")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$164)
	.dwattr $C$DW$417, DW_AT_name("SOCPRICTL")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_SOCPRICTL")
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$418, DW_AT_name("rsvd6")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$419, DW_AT_name("ADCSAMPLEMODE")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_ADCSAMPLEMODE")
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$420	.dwtag  DW_TAG_member
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$420, DW_AT_name("rsvd7")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$421, DW_AT_name("ADCINTSOCSEL1")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_ADCINTSOCSEL1")
	.dwattr $C$DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$422, DW_AT_name("ADCINTSOCSEL2")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_ADCINTSOCSEL2")
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$423, DW_AT_name("rsvd8")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$423, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$424, DW_AT_name("rsvd9")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$425, DW_AT_name("ADCSOCFLG1")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_ADCSOCFLG1")
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$426, DW_AT_name("rsvd10")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$427, DW_AT_name("ADCSOCFRC1")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_ADCSOCFRC1")
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$428, DW_AT_name("rsvd11")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_rsvd11")
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$429, DW_AT_name("ADCSOCOVF1")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_ADCSOCOVF1")
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$430, DW_AT_name("rsvd12")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_rsvd12")
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x1d]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$431	.dwtag  DW_TAG_member
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$431, DW_AT_name("ADCSOCOVFCLR1")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_ADCSOCOVFCLR1")
	.dwattr $C$DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$432, DW_AT_name("rsvd13")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_rsvd13")
	.dwattr $C$DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0x1f]
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$433, DW_AT_name("ADCSOC0CTL")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_ADCSOC0CTL")
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$434, DW_AT_name("ADCSOC1CTL")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_ADCSOC1CTL")
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0x21]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$435	.dwtag  DW_TAG_member
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$435, DW_AT_name("ADCSOC2CTL")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_ADCSOC2CTL")
	.dwattr $C$DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$436, DW_AT_name("ADCSOC3CTL")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_ADCSOC3CTL")
	.dwattr $C$DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0x23]
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$437, DW_AT_name("ADCSOC4CTL")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_ADCSOC4CTL")
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$438, DW_AT_name("ADCSOC5CTL")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_ADCSOC5CTL")
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x25]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$439, DW_AT_name("ADCSOC6CTL")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_ADCSOC6CTL")
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$440, DW_AT_name("ADCSOC7CTL")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_ADCSOC7CTL")
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x27]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$441, DW_AT_name("ADCSOC8CTL")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_ADCSOC8CTL")
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$442, DW_AT_name("ADCSOC9CTL")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_ADCSOC9CTL")
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x29]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$443, DW_AT_name("ADCSOC10CTL")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_ADCSOC10CTL")
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$444, DW_AT_name("ADCSOC11CTL")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_ADCSOC11CTL")
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$445, DW_AT_name("ADCSOC12CTL")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_ADCSOC12CTL")
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$446, DW_AT_name("ADCSOC13CTL")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_ADCSOC13CTL")
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$447, DW_AT_name("ADCSOC14CTL")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_ADCSOC14CTL")
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$448, DW_AT_name("ADCSOC15CTL")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_ADCSOC15CTL")
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x2f]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$449, DW_AT_name("rsvd14")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_rsvd14")
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$450, DW_AT_name("ADCREFTRIM")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_ADCREFTRIM")
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$451, DW_AT_name("ADCOFFTRIM")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_ADCOFFTRIM")
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x41]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$452, DW_AT_name("rsvd15")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_rsvd15")
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x42]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$453, DW_AT_name("COMPHYSTCTL")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_COMPHYSTCTL")
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$454, DW_AT_name("rsvd16")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_rsvd16")
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$52

$C$DW$455	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$52)
$C$DW$T$269	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$269, DW_AT_type(*$C$DW$455)

$C$DW$T$53	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$53, DW_AT_name("ADC_RESULT_REGS")
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x10)
$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$456, DW_AT_name("ADCRESULT0")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_ADCRESULT0")
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$457, DW_AT_name("ADCRESULT1")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_ADCRESULT1")
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$458, DW_AT_name("ADCRESULT2")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_ADCRESULT2")
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$459, DW_AT_name("ADCRESULT3")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_ADCRESULT3")
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$460, DW_AT_name("ADCRESULT4")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_ADCRESULT4")
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$461, DW_AT_name("ADCRESULT5")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_ADCRESULT5")
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$462, DW_AT_name("ADCRESULT6")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_ADCRESULT6")
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$463, DW_AT_name("ADCRESULT7")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_ADCRESULT7")
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$464, DW_AT_name("ADCRESULT8")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_ADCRESULT8")
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$465, DW_AT_name("ADCRESULT9")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_ADCRESULT9")
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$466, DW_AT_name("ADCRESULT10")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_ADCRESULT10")
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$467, DW_AT_name("ADCRESULT11")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_ADCRESULT11")
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$468, DW_AT_name("ADCRESULT12")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_ADCRESULT12")
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$469, DW_AT_name("ADCRESULT13")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_ADCRESULT13")
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$470, DW_AT_name("ADCRESULT14")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_ADCRESULT14")
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$471, DW_AT_name("ADCRESULT15")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_ADCRESULT15")
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$53

$C$DW$472	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$53)
$C$DW$T$270	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$270, DW_AT_type(*$C$DW$472)

$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("AIODAT_BITS")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x02)
$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$473, DW_AT_name("rsvd1")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$473, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$474, DW_AT_name("rsvd2")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$474, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$475, DW_AT_name("AIO2")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_AIO2")
	.dwattr $C$DW$475, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$476, DW_AT_name("rsvd3")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$476, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$477, DW_AT_name("AIO4")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_AIO4")
	.dwattr $C$DW$477, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$478	.dwtag  DW_TAG_member
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$478, DW_AT_name("rsvd4")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$478, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$478, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$479, DW_AT_name("AIO6")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_AIO6")
	.dwattr $C$DW$479, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$480, DW_AT_name("rsvd5")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$480, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$481, DW_AT_name("rsvd6")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$481, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$482, DW_AT_name("rsvd7")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$482, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$483, DW_AT_name("AIO10")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_AIO10")
	.dwattr $C$DW$483, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$484, DW_AT_name("rsvd8")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$484, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$485, DW_AT_name("AIO12")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_AIO12")
	.dwattr $C$DW$485, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$486, DW_AT_name("rsvd9")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$486, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$487, DW_AT_name("AIO14")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_AIO14")
	.dwattr $C$DW$487, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$488	.dwtag  DW_TAG_member
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$488, DW_AT_name("rsvd10")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$488, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$489	.dwtag  DW_TAG_member
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$489, DW_AT_name("rsvd11")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_rsvd11")
	.dwattr $C$DW$489, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$489, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$54


$C$DW$T$56	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$56, DW_AT_name("AIODAT_REG")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x02)
$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$490, DW_AT_name("all")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$491	.dwtag  DW_TAG_member
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$491, DW_AT_name("bit")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$491, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56


$C$DW$T$57	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$57, DW_AT_name("AQCSFRC_BITS")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x01)
$C$DW$492	.dwtag  DW_TAG_member
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$492, DW_AT_name("CSFA")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_CSFA")
	.dwattr $C$DW$492, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$492, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$493	.dwtag  DW_TAG_member
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$493, DW_AT_name("CSFB")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_CSFB")
	.dwattr $C$DW$493, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$493, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$494	.dwtag  DW_TAG_member
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$494, DW_AT_name("rsvd1")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$494, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0c)
	.dwattr $C$DW$494, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$494, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$57


$C$DW$T$58	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$58, DW_AT_name("AQCSFRC_REG")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x01)
$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$495, DW_AT_name("all")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$496, DW_AT_name("bit")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$58


$C$DW$T$59	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$59, DW_AT_name("AQCTL_BITS")
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x01)
$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$497, DW_AT_name("ZRO")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_ZRO")
	.dwattr $C$DW$497, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$498, DW_AT_name("PRD")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_PRD")
	.dwattr $C$DW$498, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$499, DW_AT_name("CAU")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_CAU")
	.dwattr $C$DW$499, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$500, DW_AT_name("CAD")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_CAD")
	.dwattr $C$DW$500, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$501, DW_AT_name("CBU")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_CBU")
	.dwattr $C$DW$501, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$502, DW_AT_name("CBD")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_CBD")
	.dwattr $C$DW$502, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$503, DW_AT_name("rsvd1")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$503, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$59


$C$DW$T$60	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$60, DW_AT_name("AQCTL_REG")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x01)
$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$504, DW_AT_name("all")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$505, DW_AT_name("bit")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$60


$C$DW$T$61	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$61, DW_AT_name("AQSFRC_BITS")
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x01)
$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$506, DW_AT_name("ACTSFA")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_ACTSFA")
	.dwattr $C$DW$506, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$507, DW_AT_name("OTSFA")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_OTSFA")
	.dwattr $C$DW$507, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$508, DW_AT_name("ACTSFB")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_ACTSFB")
	.dwattr $C$DW$508, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x02)
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$509, DW_AT_name("OTSFB")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_OTSFB")
	.dwattr $C$DW$509, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$510, DW_AT_name("RLDCSF")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_RLDCSF")
	.dwattr $C$DW$510, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$511, DW_AT_name("rsvd1")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$511, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$61


$C$DW$T$62	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$62, DW_AT_name("AQSFRC_REG")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)
$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$512, DW_AT_name("all")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$513, DW_AT_name("bit")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$62


$C$DW$T$63	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$63, DW_AT_name("CMPA_HRPWM_GROUP")
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x02)
$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$514, DW_AT_name("all")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$515, DW_AT_name("half")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_half")
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$63


$C$DW$T$64	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$64, DW_AT_name("CMPA_HRPWM_REG")
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x02)
$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$516, DW_AT_name("CMPAHR")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_CMPAHR")
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$517, DW_AT_name("CMPA")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_CMPA")
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$64


$C$DW$T$65	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$65, DW_AT_name("CMPCTL_BITS")
	.dwattr $C$DW$T$65, DW_AT_byte_size(0x01)
$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$518, DW_AT_name("LOADAMODE")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_LOADAMODE")
	.dwattr $C$DW$518, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$519, DW_AT_name("LOADBMODE")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_LOADBMODE")
	.dwattr $C$DW$519, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$520, DW_AT_name("SHDWAMODE")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_SHDWAMODE")
	.dwattr $C$DW$520, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$521, DW_AT_name("rsvd1")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$521, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$522, DW_AT_name("SHDWBMODE")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_SHDWBMODE")
	.dwattr $C$DW$522, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$523, DW_AT_name("rsvd2")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$523, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$524, DW_AT_name("SHDWAFULL")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_SHDWAFULL")
	.dwattr $C$DW$524, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$525, DW_AT_name("SHDWBFULL")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_SHDWBFULL")
	.dwattr $C$DW$525, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$526, DW_AT_name("rsvd3")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$526, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x06)
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$65


$C$DW$T$66	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$66, DW_AT_name("CMPCTL_REG")
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x01)
$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$527, DW_AT_name("all")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$528, DW_AT_name("bit")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$66


$C$DW$T$67	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$67, DW_AT_name("COMPHYSTCTL_BITS")
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x01)
$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$529, DW_AT_name("rsvd1")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$529, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$530, DW_AT_name("COMP1_HYST_DISABLE")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_COMP1_HYST_DISABLE")
	.dwattr $C$DW$530, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$531, DW_AT_name("rsvd2")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$531, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x04)
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$532, DW_AT_name("COMP2_HYST_DISABLE")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_COMP2_HYST_DISABLE")
	.dwattr $C$DW$532, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$533, DW_AT_name("rsvd3")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$533, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x04)
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$534, DW_AT_name("COMP3_HYST_DISABLE")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_COMP3_HYST_DISABLE")
	.dwattr $C$DW$534, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$535, DW_AT_name("rsvd4")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$535, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$67


$C$DW$T$68	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$68, DW_AT_name("COMPHYSTCTL_REG")
	.dwattr $C$DW$T$68, DW_AT_byte_size(0x01)
$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$536, DW_AT_name("all")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$537, DW_AT_name("bit")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$68


$C$DW$T$69	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$69, DW_AT_name("DBCTL_BITS")
	.dwattr $C$DW$T$69, DW_AT_byte_size(0x01)
$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$538, DW_AT_name("OUT_MODE")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_OUT_MODE")
	.dwattr $C$DW$538, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$539, DW_AT_name("POLSEL")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_POLSEL")
	.dwattr $C$DW$539, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$540, DW_AT_name("IN_MODE")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_IN_MODE")
	.dwattr $C$DW$540, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$541, DW_AT_name("rsvd1")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$541, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x09)
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$542, DW_AT_name("HALFCYCLE")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_HALFCYCLE")
	.dwattr $C$DW$542, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$69


$C$DW$T$70	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$70, DW_AT_name("DBCTL_REG")
	.dwattr $C$DW$T$70, DW_AT_byte_size(0x01)
$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$543, DW_AT_name("all")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$544, DW_AT_name("bit")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$70


$C$DW$T$71	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$71, DW_AT_name("DCCAPCTL_BITS")
	.dwattr $C$DW$T$71, DW_AT_byte_size(0x01)
$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$545, DW_AT_name("CAPE")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_CAPE")
	.dwattr $C$DW$545, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$546, DW_AT_name("SHDWMODE")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_SHDWMODE")
	.dwattr $C$DW$546, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$547, DW_AT_name("rsvd1")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$547, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0e)
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$71


$C$DW$T$72	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$72, DW_AT_name("DCCAPCTL_REG")
	.dwattr $C$DW$T$72, DW_AT_byte_size(0x01)
$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$548, DW_AT_name("all")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$549, DW_AT_name("bit")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$72


$C$DW$T$73	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$73, DW_AT_name("DCCTL_BITS")
	.dwattr $C$DW$T$73, DW_AT_byte_size(0x01)
$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$550, DW_AT_name("EVT1SRCSEL")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_EVT1SRCSEL")
	.dwattr $C$DW$550, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$551, DW_AT_name("EVT1FRCSYNCSEL")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_EVT1FRCSYNCSEL")
	.dwattr $C$DW$551, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$552, DW_AT_name("EVT1SOCE")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_EVT1SOCE")
	.dwattr $C$DW$552, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$553, DW_AT_name("EVT1SYNCE")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_EVT1SYNCE")
	.dwattr $C$DW$553, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$554, DW_AT_name("rsvd1")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$554, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x04)
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$555, DW_AT_name("EVT2SRCSEL")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_EVT2SRCSEL")
	.dwattr $C$DW$555, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$556, DW_AT_name("EVT2FRCSYNCSEL")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_EVT2FRCSYNCSEL")
	.dwattr $C$DW$556, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$557, DW_AT_name("rsvd2")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$557, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x06)
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$73


$C$DW$T$74	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$74, DW_AT_name("DCCTL_REG")
	.dwattr $C$DW$T$74, DW_AT_byte_size(0x01)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$558, DW_AT_name("all")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$559, DW_AT_name("bit")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$74


$C$DW$T$75	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$75, DW_AT_name("DCFCTL_BITS")
	.dwattr $C$DW$T$75, DW_AT_byte_size(0x01)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$560, DW_AT_name("SRCSEL")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_SRCSEL")
	.dwattr $C$DW$560, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$561, DW_AT_name("BLANKE")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_BLANKE")
	.dwattr $C$DW$561, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$562, DW_AT_name("BLANKINV")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_BLANKINV")
	.dwattr $C$DW$562, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$563, DW_AT_name("PULSESEL")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_PULSESEL")
	.dwattr $C$DW$563, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$564, DW_AT_name("rsvd1")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$564, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0a)
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$75


$C$DW$T$76	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$76, DW_AT_name("DCFCTL_REG")
	.dwattr $C$DW$T$76, DW_AT_byte_size(0x01)
$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$565, DW_AT_name("all")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$566, DW_AT_name("bit")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$76


$C$DW$T$77	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$77, DW_AT_name("DCTRIPSEL_BITS")
	.dwattr $C$DW$T$77, DW_AT_byte_size(0x01)
$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$567, DW_AT_name("DCAHCOMPSEL")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_DCAHCOMPSEL")
	.dwattr $C$DW$567, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x04)
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$568, DW_AT_name("DCALCOMPSEL")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_DCALCOMPSEL")
	.dwattr $C$DW$568, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x04)
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$569, DW_AT_name("DCBHCOMPSEL")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_DCBHCOMPSEL")
	.dwattr $C$DW$569, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x04)
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$570, DW_AT_name("DCBLCOMPSEL")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_DCBLCOMPSEL")
	.dwattr $C$DW$570, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$77


$C$DW$T$78	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$78, DW_AT_name("DCTRIPSEL_REG")
	.dwattr $C$DW$T$78, DW_AT_byte_size(0x01)
$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$571, DW_AT_name("all")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$572, DW_AT_name("bit")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$78


$C$DW$T$81	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$81, DW_AT_name("EPWM_REGS")
	.dwattr $C$DW$T$81, DW_AT_byte_size(0x40)
$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$166)
	.dwattr $C$DW$573, DW_AT_name("TBCTL")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_TBCTL")
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$574, DW_AT_name("TBSTS")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_TBSTS")
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$575, DW_AT_name("TBPHS")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_TBPHS")
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$576, DW_AT_name("TBCTR")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_TBCTR")
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$577, DW_AT_name("TBPRD")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_TBPRD")
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$578, DW_AT_name("TBPRDHR")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_TBPRDHR")
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$579, DW_AT_name("CMPCTL")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_CMPCTL")
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$580, DW_AT_name("CMPA")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_CMPA")
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$581, DW_AT_name("CMPB")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_CMPB")
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$582, DW_AT_name("AQCTLA")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_AQCTLA")
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$583, DW_AT_name("AQCTLB")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_AQCTLB")
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$584, DW_AT_name("AQSFRC")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_AQSFRC")
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$585, DW_AT_name("AQCSFRC")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_AQCSFRC")
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$586, DW_AT_name("DBCTL")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_DBCTL")
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$587, DW_AT_name("DBRED")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_DBRED")
	.dwattr $C$DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$588, DW_AT_name("DBFED")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_DBFED")
	.dwattr $C$DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$589, DW_AT_name("TZSEL")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_TZSEL")
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$178)
	.dwattr $C$DW$590, DW_AT_name("TZDCSEL")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_TZDCSEL")
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$591, DW_AT_name("TZCTL")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_TZCTL")
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$592, DW_AT_name("TZEINT")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_TZEINT")
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$182)
	.dwattr $C$DW$593, DW_AT_name("TZFLG")
	.dwattr $C$DW$593, DW_AT_TI_symbol_name("_TZFLG")
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$594, DW_AT_name("TZCLR")
	.dwattr $C$DW$594, DW_AT_TI_symbol_name("_TZCLR")
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$595, DW_AT_name("TZFRC")
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_TZFRC")
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$596, DW_AT_name("ETSEL")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_ETSEL")
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$597, DW_AT_name("ETPS")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_ETPS")
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$598, DW_AT_name("ETFLG")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_ETFLG")
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$599, DW_AT_name("ETCLR")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_ETCLR")
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$600, DW_AT_name("ETFRC")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_ETFRC")
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x1d]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$601, DW_AT_name("PCCTL")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_PCCTL")
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$602, DW_AT_name("rsvd1")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x1f]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$603, DW_AT_name("HRCNFG")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_HRCNFG")
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$604, DW_AT_name("rsvd2")
	.dwattr $C$DW$604, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x21]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$605, DW_AT_name("rsvd3")
	.dwattr $C$DW$605, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$606, DW_AT_name("rsvd4")
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x23]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$607, DW_AT_name("rsvd5")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$608, DW_AT_name("rsvd6")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x25]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$609, DW_AT_name("HRMSTEP")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_HRMSTEP")
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$610, DW_AT_name("rsvd7")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x27]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$611, DW_AT_name("HRPCTL")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_HRPCTL")
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$612, DW_AT_name("rsvd8")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x29]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$613, DW_AT_name("TBPRDM")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_TBPRDM")
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$614, DW_AT_name("CMPAM")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_CMPAM")
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$615, DW_AT_name("rsvd9")
	.dwattr $C$DW$615, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$616, DW_AT_name("DCTRIPSEL")
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_DCTRIPSEL")
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$617, DW_AT_name("DCACTL")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_DCACTL")
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x31]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$618, DW_AT_name("DCBCTL")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_DCBCTL")
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x32]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$619, DW_AT_name("DCFCTL")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_DCFCTL")
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x33]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$620, DW_AT_name("DCCAPCTL")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_DCCAPCTL")
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$621, DW_AT_name("DCFOFFSET")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_DCFOFFSET")
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x35]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$622, DW_AT_name("DCFOFFSETCNT")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_DCFOFFSETCNT")
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$623, DW_AT_name("DCFWINDOW")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_DCFWINDOW")
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x37]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$624, DW_AT_name("DCFWINDOWCNT")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_DCFWINDOWCNT")
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$625, DW_AT_name("DCCAP")
	.dwattr $C$DW$625, DW_AT_TI_symbol_name("_DCCAP")
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x39]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$626, DW_AT_name("rsvd10")
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$81

$C$DW$627	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$81)
$C$DW$T$273	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$273, DW_AT_type(*$C$DW$627)

$C$DW$T$82	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$82, DW_AT_name("EQEP_REGS")
	.dwattr $C$DW$T$82, DW_AT_byte_size(0x22)
$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$628, DW_AT_name("QPOSCNT")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_QPOSCNT")
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$629, DW_AT_name("QPOSINIT")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_QPOSINIT")
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$630, DW_AT_name("QPOSMAX")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_QPOSMAX")
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$631, DW_AT_name("QPOSCMP")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_QPOSCMP")
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$632, DW_AT_name("QPOSILAT")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_QPOSILAT")
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$633, DW_AT_name("QPOSSLAT")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_QPOSSLAT")
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$634, DW_AT_name("QPOSLAT")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_QPOSLAT")
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$635, DW_AT_name("QUTMR")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_QUTMR")
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$636, DW_AT_name("QUPRD")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_QUPRD")
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$637	.dwtag  DW_TAG_member
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$637, DW_AT_name("QWDTMR")
	.dwattr $C$DW$637, DW_AT_TI_symbol_name("_QWDTMR")
	.dwattr $C$DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$637, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$638, DW_AT_name("QWDPRD")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_QWDPRD")
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$639, DW_AT_name("QDECCTL")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_QDECCTL")
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$640, DW_AT_name("QEPCTL")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_QEPCTL")
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$641, DW_AT_name("QCAPCTL")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_QCAPCTL")
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$642, DW_AT_name("QPOSCTL")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_QPOSCTL")
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$643, DW_AT_name("QEINT")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_QEINT")
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$644, DW_AT_name("QFLG")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_QFLG")
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$645, DW_AT_name("QCLR")
	.dwattr $C$DW$645, DW_AT_TI_symbol_name("_QCLR")
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$646, DW_AT_name("QFRC")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_QFRC")
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$647, DW_AT_name("QEPSTS")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_QEPSTS")
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$648, DW_AT_name("QCTMR")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_QCTMR")
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x1d]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$649, DW_AT_name("QCPRD")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_QCPRD")
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$650, DW_AT_name("QCTMRLAT")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_QCTMRLAT")
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x1f]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$651, DW_AT_name("QCPRDLAT")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_QCPRDLAT")
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$652	.dwtag  DW_TAG_member
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$652, DW_AT_name("rsvd1")
	.dwattr $C$DW$652, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0x21]
	.dwattr $C$DW$652, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$82

$C$DW$653	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$82)
$C$DW$T$274	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$274, DW_AT_type(*$C$DW$653)

$C$DW$T$83	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$83, DW_AT_name("ETCLR_BITS")
	.dwattr $C$DW$T$83, DW_AT_byte_size(0x01)
$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$654, DW_AT_name("INT")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_INT")
	.dwattr $C$DW$654, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$655, DW_AT_name("rsvd1")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$655, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$656, DW_AT_name("SOCA")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_SOCA")
	.dwattr $C$DW$656, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$657, DW_AT_name("SOCB")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_SOCB")
	.dwattr $C$DW$657, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$658, DW_AT_name("rsvd2")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$658, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0c)
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$83


$C$DW$T$84	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$84, DW_AT_name("ETCLR_REG")
	.dwattr $C$DW$T$84, DW_AT_byte_size(0x01)
$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$659, DW_AT_name("all")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$660, DW_AT_name("bit")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$84


$C$DW$T$85	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$85, DW_AT_name("ETFLG_BITS")
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x01)
$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$661, DW_AT_name("INT")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_INT")
	.dwattr $C$DW$661, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$662, DW_AT_name("rsvd1")
	.dwattr $C$DW$662, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$662, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$663, DW_AT_name("SOCA")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_SOCA")
	.dwattr $C$DW$663, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$664, DW_AT_name("SOCB")
	.dwattr $C$DW$664, DW_AT_TI_symbol_name("_SOCB")
	.dwattr $C$DW$664, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$665, DW_AT_name("rsvd2")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$665, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0c)
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$85


$C$DW$T$86	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$86, DW_AT_name("ETFLG_REG")
	.dwattr $C$DW$T$86, DW_AT_byte_size(0x01)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$666, DW_AT_name("all")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$667, DW_AT_name("bit")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$86


$C$DW$T$87	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$87, DW_AT_name("ETFRC_BITS")
	.dwattr $C$DW$T$87, DW_AT_byte_size(0x01)
$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$668, DW_AT_name("INT")
	.dwattr $C$DW$668, DW_AT_TI_symbol_name("_INT")
	.dwattr $C$DW$668, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$669, DW_AT_name("rsvd1")
	.dwattr $C$DW$669, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$669, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$670, DW_AT_name("SOCA")
	.dwattr $C$DW$670, DW_AT_TI_symbol_name("_SOCA")
	.dwattr $C$DW$670, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$671, DW_AT_name("SOCB")
	.dwattr $C$DW$671, DW_AT_TI_symbol_name("_SOCB")
	.dwattr $C$DW$671, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$672, DW_AT_name("rsvd2")
	.dwattr $C$DW$672, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$672, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0c)
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$87


$C$DW$T$88	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$88, DW_AT_name("ETFRC_REG")
	.dwattr $C$DW$T$88, DW_AT_byte_size(0x01)
$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$673, DW_AT_name("all")
	.dwattr $C$DW$673, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$674, DW_AT_name("bit")
	.dwattr $C$DW$674, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$88


$C$DW$T$89	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$89, DW_AT_name("ETPS_BITS")
	.dwattr $C$DW$T$89, DW_AT_byte_size(0x01)
$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$675, DW_AT_name("INTPRD")
	.dwattr $C$DW$675, DW_AT_TI_symbol_name("_INTPRD")
	.dwattr $C$DW$675, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$676	.dwtag  DW_TAG_member
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$676, DW_AT_name("INTCNT")
	.dwattr $C$DW$676, DW_AT_TI_symbol_name("_INTCNT")
	.dwattr $C$DW$676, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$676, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$677, DW_AT_name("rsvd1")
	.dwattr $C$DW$677, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$677, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x04)
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$678	.dwtag  DW_TAG_member
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$678, DW_AT_name("SOCAPRD")
	.dwattr $C$DW$678, DW_AT_TI_symbol_name("_SOCAPRD")
	.dwattr $C$DW$678, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$679	.dwtag  DW_TAG_member
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$679, DW_AT_name("SOCACNT")
	.dwattr $C$DW$679, DW_AT_TI_symbol_name("_SOCACNT")
	.dwattr $C$DW$679, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$679, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$680	.dwtag  DW_TAG_member
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$680, DW_AT_name("SOCBPRD")
	.dwattr $C$DW$680, DW_AT_TI_symbol_name("_SOCBPRD")
	.dwattr $C$DW$680, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$681	.dwtag  DW_TAG_member
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$681, DW_AT_name("SOCBCNT")
	.dwattr $C$DW$681, DW_AT_TI_symbol_name("_SOCBCNT")
	.dwattr $C$DW$681, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$681, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$681, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$89


$C$DW$T$90	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$90, DW_AT_name("ETPS_REG")
	.dwattr $C$DW$T$90, DW_AT_byte_size(0x01)
$C$DW$682	.dwtag  DW_TAG_member
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$682, DW_AT_name("all")
	.dwattr $C$DW$682, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$682, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$683	.dwtag  DW_TAG_member
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$683, DW_AT_name("bit")
	.dwattr $C$DW$683, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$683, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$90


$C$DW$T$91	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$91, DW_AT_name("ETSEL_BITS")
	.dwattr $C$DW$T$91, DW_AT_byte_size(0x01)
$C$DW$684	.dwtag  DW_TAG_member
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$684, DW_AT_name("INTSEL")
	.dwattr $C$DW$684, DW_AT_TI_symbol_name("_INTSEL")
	.dwattr $C$DW$684, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$684, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$684, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$685, DW_AT_name("INTEN")
	.dwattr $C$DW$685, DW_AT_TI_symbol_name("_INTEN")
	.dwattr $C$DW$685, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$686	.dwtag  DW_TAG_member
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$686, DW_AT_name("rsvd1")
	.dwattr $C$DW$686, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$686, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x04)
	.dwattr $C$DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$686, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$687	.dwtag  DW_TAG_member
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$687, DW_AT_name("SOCASEL")
	.dwattr $C$DW$687, DW_AT_TI_symbol_name("_SOCASEL")
	.dwattr $C$DW$687, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x03)
	.dwattr $C$DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$688	.dwtag  DW_TAG_member
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$688, DW_AT_name("SOCAEN")
	.dwattr $C$DW$688, DW_AT_TI_symbol_name("_SOCAEN")
	.dwattr $C$DW$688, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$688, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$689	.dwtag  DW_TAG_member
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$689, DW_AT_name("SOCBSEL")
	.dwattr $C$DW$689, DW_AT_TI_symbol_name("_SOCBSEL")
	.dwattr $C$DW$689, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x03)
	.dwattr $C$DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$690	.dwtag  DW_TAG_member
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$690, DW_AT_name("SOCBEN")
	.dwattr $C$DW$690, DW_AT_TI_symbol_name("_SOCBEN")
	.dwattr $C$DW$690, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$690, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$91


$C$DW$T$92	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$92, DW_AT_name("ETSEL_REG")
	.dwattr $C$DW$T$92, DW_AT_byte_size(0x01)
$C$DW$691	.dwtag  DW_TAG_member
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$691, DW_AT_name("all")
	.dwattr $C$DW$691, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$691, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$692	.dwtag  DW_TAG_member
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$692, DW_AT_name("bit")
	.dwattr $C$DW$692, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$692, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$692, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$92


$C$DW$T$93	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$93, DW_AT_name("GPADAT_BITS")
	.dwattr $C$DW$T$93, DW_AT_byte_size(0x02)
$C$DW$693	.dwtag  DW_TAG_member
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$693, DW_AT_name("GPIO0")
	.dwattr $C$DW$693, DW_AT_TI_symbol_name("_GPIO0")
	.dwattr $C$DW$693, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$693, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$693, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$694	.dwtag  DW_TAG_member
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$694, DW_AT_name("GPIO1")
	.dwattr $C$DW$694, DW_AT_TI_symbol_name("_GPIO1")
	.dwattr $C$DW$694, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$694, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$694, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$695, DW_AT_name("GPIO2")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_GPIO2")
	.dwattr $C$DW$695, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$696, DW_AT_name("GPIO3")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_GPIO3")
	.dwattr $C$DW$696, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$697, DW_AT_name("GPIO4")
	.dwattr $C$DW$697, DW_AT_TI_symbol_name("_GPIO4")
	.dwattr $C$DW$697, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$698, DW_AT_name("GPIO5")
	.dwattr $C$DW$698, DW_AT_TI_symbol_name("_GPIO5")
	.dwattr $C$DW$698, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$699, DW_AT_name("GPIO6")
	.dwattr $C$DW$699, DW_AT_TI_symbol_name("_GPIO6")
	.dwattr $C$DW$699, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$700	.dwtag  DW_TAG_member
	.dwattr $C$DW$700, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$700, DW_AT_name("GPIO7")
	.dwattr $C$DW$700, DW_AT_TI_symbol_name("_GPIO7")
	.dwattr $C$DW$700, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$700, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$700, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$701, DW_AT_name("GPIO8")
	.dwattr $C$DW$701, DW_AT_TI_symbol_name("_GPIO8")
	.dwattr $C$DW$701, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$702, DW_AT_name("GPIO9")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_GPIO9")
	.dwattr $C$DW$702, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$703, DW_AT_name("GPIO10")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_GPIO10")
	.dwattr $C$DW$703, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$704, DW_AT_name("GPIO11")
	.dwattr $C$DW$704, DW_AT_TI_symbol_name("_GPIO11")
	.dwattr $C$DW$704, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$705, DW_AT_name("GPIO12")
	.dwattr $C$DW$705, DW_AT_TI_symbol_name("_GPIO12")
	.dwattr $C$DW$705, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$706	.dwtag  DW_TAG_member
	.dwattr $C$DW$706, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$706, DW_AT_name("GPIO13")
	.dwattr $C$DW$706, DW_AT_TI_symbol_name("_GPIO13")
	.dwattr $C$DW$706, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$706, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$707, DW_AT_name("GPIO14")
	.dwattr $C$DW$707, DW_AT_TI_symbol_name("_GPIO14")
	.dwattr $C$DW$707, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$708	.dwtag  DW_TAG_member
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$708, DW_AT_name("GPIO15")
	.dwattr $C$DW$708, DW_AT_TI_symbol_name("_GPIO15")
	.dwattr $C$DW$708, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$708, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$709, DW_AT_name("GPIO16")
	.dwattr $C$DW$709, DW_AT_TI_symbol_name("_GPIO16")
	.dwattr $C$DW$709, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$710	.dwtag  DW_TAG_member
	.dwattr $C$DW$710, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$710, DW_AT_name("GPIO17")
	.dwattr $C$DW$710, DW_AT_TI_symbol_name("_GPIO17")
	.dwattr $C$DW$710, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$710, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$711	.dwtag  DW_TAG_member
	.dwattr $C$DW$711, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$711, DW_AT_name("GPIO18")
	.dwattr $C$DW$711, DW_AT_TI_symbol_name("_GPIO18")
	.dwattr $C$DW$711, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$711, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$712	.dwtag  DW_TAG_member
	.dwattr $C$DW$712, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$712, DW_AT_name("GPIO19")
	.dwattr $C$DW$712, DW_AT_TI_symbol_name("_GPIO19")
	.dwattr $C$DW$712, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$712, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$713	.dwtag  DW_TAG_member
	.dwattr $C$DW$713, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$713, DW_AT_name("GPIO20")
	.dwattr $C$DW$713, DW_AT_TI_symbol_name("_GPIO20")
	.dwattr $C$DW$713, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$713, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$714	.dwtag  DW_TAG_member
	.dwattr $C$DW$714, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$714, DW_AT_name("GPIO21")
	.dwattr $C$DW$714, DW_AT_TI_symbol_name("_GPIO21")
	.dwattr $C$DW$714, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$714, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$715, DW_AT_name("GPIO22")
	.dwattr $C$DW$715, DW_AT_TI_symbol_name("_GPIO22")
	.dwattr $C$DW$715, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$716	.dwtag  DW_TAG_member
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$716, DW_AT_name("GPIO23")
	.dwattr $C$DW$716, DW_AT_TI_symbol_name("_GPIO23")
	.dwattr $C$DW$716, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$717	.dwtag  DW_TAG_member
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$717, DW_AT_name("GPIO24")
	.dwattr $C$DW$717, DW_AT_TI_symbol_name("_GPIO24")
	.dwattr $C$DW$717, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$717, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$717, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$718	.dwtag  DW_TAG_member
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$718, DW_AT_name("GPIO25")
	.dwattr $C$DW$718, DW_AT_TI_symbol_name("_GPIO25")
	.dwattr $C$DW$718, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$719	.dwtag  DW_TAG_member
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$719, DW_AT_name("GPIO26")
	.dwattr $C$DW$719, DW_AT_TI_symbol_name("_GPIO26")
	.dwattr $C$DW$719, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$719, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$720	.dwtag  DW_TAG_member
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$720, DW_AT_name("GPIO27")
	.dwattr $C$DW$720, DW_AT_TI_symbol_name("_GPIO27")
	.dwattr $C$DW$720, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$720, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$721	.dwtag  DW_TAG_member
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$721, DW_AT_name("GPIO28")
	.dwattr $C$DW$721, DW_AT_TI_symbol_name("_GPIO28")
	.dwattr $C$DW$721, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$721, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$722, DW_AT_name("GPIO29")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_GPIO29")
	.dwattr $C$DW$722, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$723, DW_AT_name("GPIO30")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_GPIO30")
	.dwattr $C$DW$723, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$724, DW_AT_name("GPIO31")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_GPIO31")
	.dwattr $C$DW$724, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$93


$C$DW$T$94	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$94, DW_AT_name("GPADAT_REG")
	.dwattr $C$DW$T$94, DW_AT_byte_size(0x02)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$725, DW_AT_name("all")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$726, DW_AT_name("bit")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$94


$C$DW$T$95	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$95, DW_AT_name("GPBDAT_BITS")
	.dwattr $C$DW$T$95, DW_AT_byte_size(0x02)
$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$727, DW_AT_name("GPIO32")
	.dwattr $C$DW$727, DW_AT_TI_symbol_name("_GPIO32")
	.dwattr $C$DW$727, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$728, DW_AT_name("GPIO33")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_GPIO33")
	.dwattr $C$DW$728, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$729, DW_AT_name("GPIO34")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_GPIO34")
	.dwattr $C$DW$729, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$730, DW_AT_name("GPIO35")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_GPIO35")
	.dwattr $C$DW$730, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$731, DW_AT_name("GPIO36")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_GPIO36")
	.dwattr $C$DW$731, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$732, DW_AT_name("GPIO37")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_GPIO37")
	.dwattr $C$DW$732, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$733, DW_AT_name("GPIO38")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_GPIO38")
	.dwattr $C$DW$733, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$734, DW_AT_name("GPIO39")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_GPIO39")
	.dwattr $C$DW$734, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$735, DW_AT_name("GPIO40")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_GPIO40")
	.dwattr $C$DW$735, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$736, DW_AT_name("GPIO41")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_GPIO41")
	.dwattr $C$DW$736, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$737, DW_AT_name("GPIO42")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_GPIO42")
	.dwattr $C$DW$737, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$738, DW_AT_name("GPIO43")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_GPIO43")
	.dwattr $C$DW$738, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$739, DW_AT_name("GPIO44")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_GPIO44")
	.dwattr $C$DW$739, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$740, DW_AT_name("rsvd1")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$740, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x03)
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$741, DW_AT_name("rsvd2")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$741, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$742, DW_AT_name("GPIO50")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_GPIO50")
	.dwattr $C$DW$742, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$743, DW_AT_name("GPIO51")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_GPIO51")
	.dwattr $C$DW$743, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$744, DW_AT_name("GPIO52")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_GPIO52")
	.dwattr $C$DW$744, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$745, DW_AT_name("GPIO53")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_GPIO53")
	.dwattr $C$DW$745, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$746, DW_AT_name("GPIO54")
	.dwattr $C$DW$746, DW_AT_TI_symbol_name("_GPIO54")
	.dwattr $C$DW$746, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$747, DW_AT_name("GPIO55")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_GPIO55")
	.dwattr $C$DW$747, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$748, DW_AT_name("GPIO56")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_GPIO56")
	.dwattr $C$DW$748, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$749, DW_AT_name("GPIO57")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_GPIO57")
	.dwattr $C$DW$749, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$750, DW_AT_name("GPIO58")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_GPIO58")
	.dwattr $C$DW$750, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$751, DW_AT_name("rsvd3")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$751, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$95


$C$DW$T$96	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$96, DW_AT_name("GPBDAT_REG")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x02)
$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$752, DW_AT_name("all")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$753, DW_AT_name("bit")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$96


$C$DW$T$98	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$98, DW_AT_name("GPIO_DATA_REGS")
	.dwattr $C$DW$T$98, DW_AT_byte_size(0x20)
$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$754, DW_AT_name("GPADAT")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_GPADAT")
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$755, DW_AT_name("GPASET")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_GPASET")
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$756, DW_AT_name("GPACLEAR")
	.dwattr $C$DW$756, DW_AT_TI_symbol_name("_GPACLEAR")
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$757, DW_AT_name("GPATOGGLE")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_GPATOGGLE")
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$758, DW_AT_name("GPBDAT")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_GPBDAT")
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$759, DW_AT_name("GPBSET")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_GPBSET")
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$760, DW_AT_name("GPBCLEAR")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_GPBCLEAR")
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$761, DW_AT_name("GPBTOGGLE")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_GPBTOGGLE")
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$762, DW_AT_name("rsvd1")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$763, DW_AT_name("AIODAT")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_AIODAT")
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$764, DW_AT_name("AIOSET")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_AIOSET")
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$765, DW_AT_name("AIOCLEAR")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_AIOCLEAR")
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$766, DW_AT_name("AIOTOGGLE")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_AIOTOGGLE")
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$98

$C$DW$767	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$98)
$C$DW$T$277	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$277, DW_AT_type(*$C$DW$767)

$C$DW$T$99	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$99, DW_AT_name("HRCNFG_BITS")
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x01)
$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$768, DW_AT_name("EDGMODE")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_EDGMODE")
	.dwattr $C$DW$768, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$769, DW_AT_name("CTLMODE")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_CTLMODE")
	.dwattr $C$DW$769, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$770, DW_AT_name("HRLOAD")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_HRLOAD")
	.dwattr $C$DW$770, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x02)
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$771, DW_AT_name("SELOUTB")
	.dwattr $C$DW$771, DW_AT_TI_symbol_name("_SELOUTB")
	.dwattr $C$DW$771, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$772, DW_AT_name("AUTOCONV")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_AUTOCONV")
	.dwattr $C$DW$772, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$773, DW_AT_name("SWAPAB")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_SWAPAB")
	.dwattr $C$DW$773, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$774, DW_AT_name("rsvd1")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$774, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$99


$C$DW$T$100	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$100, DW_AT_name("HRCNFG_REG")
	.dwattr $C$DW$T$100, DW_AT_byte_size(0x01)
$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$775, DW_AT_name("all")
	.dwattr $C$DW$775, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$776, DW_AT_name("bit")
	.dwattr $C$DW$776, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$100


$C$DW$T$101	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$101, DW_AT_name("HRPCTL_BITS")
	.dwattr $C$DW$T$101, DW_AT_byte_size(0x01)
$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$777, DW_AT_name("HRPE")
	.dwattr $C$DW$777, DW_AT_TI_symbol_name("_HRPE")
	.dwattr $C$DW$777, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$778, DW_AT_name("rsvd1")
	.dwattr $C$DW$778, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$778, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$779, DW_AT_name("TBPHSHRLOADE")
	.dwattr $C$DW$779, DW_AT_TI_symbol_name("_TBPHSHRLOADE")
	.dwattr $C$DW$779, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$780, DW_AT_name("rsvd2")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$780, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0d)
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$101


$C$DW$T$102	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$102, DW_AT_name("HRPCTL_REG")
	.dwattr $C$DW$T$102, DW_AT_byte_size(0x01)
$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$781, DW_AT_name("all")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$782, DW_AT_name("bit")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$102


$C$DW$T$103	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$103, DW_AT_name("INTSEL1N2_BITS")
	.dwattr $C$DW$T$103, DW_AT_byte_size(0x01)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$783, DW_AT_name("INT1SEL")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_INT1SEL")
	.dwattr $C$DW$783, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$784, DW_AT_name("INT1E")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_INT1E")
	.dwattr $C$DW$784, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$785, DW_AT_name("INT1CONT")
	.dwattr $C$DW$785, DW_AT_TI_symbol_name("_INT1CONT")
	.dwattr $C$DW$785, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$786, DW_AT_name("rsvd1")
	.dwattr $C$DW$786, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$786, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$787	.dwtag  DW_TAG_member
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$787, DW_AT_name("INT2SEL")
	.dwattr $C$DW$787, DW_AT_TI_symbol_name("_INT2SEL")
	.dwattr $C$DW$787, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$787, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$788, DW_AT_name("INT2E")
	.dwattr $C$DW$788, DW_AT_TI_symbol_name("_INT2E")
	.dwattr $C$DW$788, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$789, DW_AT_name("INT2CONT")
	.dwattr $C$DW$789, DW_AT_TI_symbol_name("_INT2CONT")
	.dwattr $C$DW$789, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$790	.dwtag  DW_TAG_member
	.dwattr $C$DW$790, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$790, DW_AT_name("rsvd2")
	.dwattr $C$DW$790, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$790, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$790, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$103


$C$DW$T$104	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$104, DW_AT_name("INTSEL1N2_REG")
	.dwattr $C$DW$T$104, DW_AT_byte_size(0x01)
$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$791, DW_AT_name("all")
	.dwattr $C$DW$791, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$792, DW_AT_name("bit")
	.dwattr $C$DW$792, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$104


$C$DW$T$105	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$105, DW_AT_name("INTSEL3N4_BITS")
	.dwattr $C$DW$T$105, DW_AT_byte_size(0x01)
$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$793, DW_AT_name("INT3SEL")
	.dwattr $C$DW$793, DW_AT_TI_symbol_name("_INT3SEL")
	.dwattr $C$DW$793, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$794	.dwtag  DW_TAG_member
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$794, DW_AT_name("INT3E")
	.dwattr $C$DW$794, DW_AT_TI_symbol_name("_INT3E")
	.dwattr $C$DW$794, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$794, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$794, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$795	.dwtag  DW_TAG_member
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$795, DW_AT_name("INT3CONT")
	.dwattr $C$DW$795, DW_AT_TI_symbol_name("_INT3CONT")
	.dwattr $C$DW$795, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$795, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$795, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$796	.dwtag  DW_TAG_member
	.dwattr $C$DW$796, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$796, DW_AT_name("rsvd1")
	.dwattr $C$DW$796, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$796, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$796, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$796, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$797	.dwtag  DW_TAG_member
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$797, DW_AT_name("INT4SEL")
	.dwattr $C$DW$797, DW_AT_TI_symbol_name("_INT4SEL")
	.dwattr $C$DW$797, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$797, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$797, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$798	.dwtag  DW_TAG_member
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$798, DW_AT_name("INT4E")
	.dwattr $C$DW$798, DW_AT_TI_symbol_name("_INT4E")
	.dwattr $C$DW$798, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$798, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$799	.dwtag  DW_TAG_member
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$799, DW_AT_name("INT4CONT")
	.dwattr $C$DW$799, DW_AT_TI_symbol_name("_INT4CONT")
	.dwattr $C$DW$799, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$799, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$800	.dwtag  DW_TAG_member
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$800, DW_AT_name("rsvd2")
	.dwattr $C$DW$800, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$800, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$800, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$800, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$105


$C$DW$T$106	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$106, DW_AT_name("INTSEL3N4_REG")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x01)
$C$DW$801	.dwtag  DW_TAG_member
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$801, DW_AT_name("all")
	.dwattr $C$DW$801, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$801, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$801, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$802	.dwtag  DW_TAG_member
	.dwattr $C$DW$802, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$802, DW_AT_name("bit")
	.dwattr $C$DW$802, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$802, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$802, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$106


$C$DW$T$107	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$107, DW_AT_name("INTSEL5N6_BITS")
	.dwattr $C$DW$T$107, DW_AT_byte_size(0x01)
$C$DW$803	.dwtag  DW_TAG_member
	.dwattr $C$DW$803, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$803, DW_AT_name("INT5SEL")
	.dwattr $C$DW$803, DW_AT_TI_symbol_name("_INT5SEL")
	.dwattr $C$DW$803, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$803, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$803, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$804	.dwtag  DW_TAG_member
	.dwattr $C$DW$804, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$804, DW_AT_name("INT5E")
	.dwattr $C$DW$804, DW_AT_TI_symbol_name("_INT5E")
	.dwattr $C$DW$804, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$804, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$804, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$805	.dwtag  DW_TAG_member
	.dwattr $C$DW$805, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$805, DW_AT_name("INT5CONT")
	.dwattr $C$DW$805, DW_AT_TI_symbol_name("_INT5CONT")
	.dwattr $C$DW$805, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$805, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$805, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$806	.dwtag  DW_TAG_member
	.dwattr $C$DW$806, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$806, DW_AT_name("rsvd1")
	.dwattr $C$DW$806, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$806, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$806, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$806, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$807	.dwtag  DW_TAG_member
	.dwattr $C$DW$807, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$807, DW_AT_name("INT6SEL")
	.dwattr $C$DW$807, DW_AT_TI_symbol_name("_INT6SEL")
	.dwattr $C$DW$807, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$807, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$807, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$808	.dwtag  DW_TAG_member
	.dwattr $C$DW$808, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$808, DW_AT_name("INT6E")
	.dwattr $C$DW$808, DW_AT_TI_symbol_name("_INT6E")
	.dwattr $C$DW$808, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$808, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$808, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$809	.dwtag  DW_TAG_member
	.dwattr $C$DW$809, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$809, DW_AT_name("INT6CONT")
	.dwattr $C$DW$809, DW_AT_TI_symbol_name("_INT6CONT")
	.dwattr $C$DW$809, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$809, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$809, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$810	.dwtag  DW_TAG_member
	.dwattr $C$DW$810, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$810, DW_AT_name("rsvd2")
	.dwattr $C$DW$810, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$810, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$810, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$810, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$107


$C$DW$T$108	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$108, DW_AT_name("INTSEL5N6_REG")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x01)
$C$DW$811	.dwtag  DW_TAG_member
	.dwattr $C$DW$811, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$811, DW_AT_name("all")
	.dwattr $C$DW$811, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$811, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$811, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$812	.dwtag  DW_TAG_member
	.dwattr $C$DW$812, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$812, DW_AT_name("bit")
	.dwattr $C$DW$812, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$812, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$812, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$108


$C$DW$T$109	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$109, DW_AT_name("INTSEL7N8_BITS")
	.dwattr $C$DW$T$109, DW_AT_byte_size(0x01)
$C$DW$813	.dwtag  DW_TAG_member
	.dwattr $C$DW$813, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$813, DW_AT_name("INT7SEL")
	.dwattr $C$DW$813, DW_AT_TI_symbol_name("_INT7SEL")
	.dwattr $C$DW$813, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$813, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$813, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$814	.dwtag  DW_TAG_member
	.dwattr $C$DW$814, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$814, DW_AT_name("INT7E")
	.dwattr $C$DW$814, DW_AT_TI_symbol_name("_INT7E")
	.dwattr $C$DW$814, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$814, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$814, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$815	.dwtag  DW_TAG_member
	.dwattr $C$DW$815, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$815, DW_AT_name("INT7CONT")
	.dwattr $C$DW$815, DW_AT_TI_symbol_name("_INT7CONT")
	.dwattr $C$DW$815, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$815, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$815, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$816	.dwtag  DW_TAG_member
	.dwattr $C$DW$816, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$816, DW_AT_name("rsvd1")
	.dwattr $C$DW$816, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$816, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$816, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$816, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$817	.dwtag  DW_TAG_member
	.dwattr $C$DW$817, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$817, DW_AT_name("INT8SEL")
	.dwattr $C$DW$817, DW_AT_TI_symbol_name("_INT8SEL")
	.dwattr $C$DW$817, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$817, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$817, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$818	.dwtag  DW_TAG_member
	.dwattr $C$DW$818, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$818, DW_AT_name("INT8E")
	.dwattr $C$DW$818, DW_AT_TI_symbol_name("_INT8E")
	.dwattr $C$DW$818, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$818, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$818, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$819	.dwtag  DW_TAG_member
	.dwattr $C$DW$819, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$819, DW_AT_name("INT8CONT")
	.dwattr $C$DW$819, DW_AT_TI_symbol_name("_INT8CONT")
	.dwattr $C$DW$819, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$819, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$819, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$820	.dwtag  DW_TAG_member
	.dwattr $C$DW$820, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$820, DW_AT_name("rsvd2")
	.dwattr $C$DW$820, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$820, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$820, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$820, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$109


$C$DW$T$110	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$110, DW_AT_name("INTSEL7N8_REG")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x01)
$C$DW$821	.dwtag  DW_TAG_member
	.dwattr $C$DW$821, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$821, DW_AT_name("all")
	.dwattr $C$DW$821, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$821, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$821, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$822	.dwtag  DW_TAG_member
	.dwattr $C$DW$822, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$822, DW_AT_name("bit")
	.dwattr $C$DW$822, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$822, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$822, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$110


$C$DW$T$111	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$111, DW_AT_name("INTSEL9N10_BITS")
	.dwattr $C$DW$T$111, DW_AT_byte_size(0x01)
$C$DW$823	.dwtag  DW_TAG_member
	.dwattr $C$DW$823, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$823, DW_AT_name("INT9SEL")
	.dwattr $C$DW$823, DW_AT_TI_symbol_name("_INT9SEL")
	.dwattr $C$DW$823, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$823, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$823, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$824	.dwtag  DW_TAG_member
	.dwattr $C$DW$824, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$824, DW_AT_name("INT9E")
	.dwattr $C$DW$824, DW_AT_TI_symbol_name("_INT9E")
	.dwattr $C$DW$824, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$824, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$824, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$825	.dwtag  DW_TAG_member
	.dwattr $C$DW$825, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$825, DW_AT_name("INT9CONT")
	.dwattr $C$DW$825, DW_AT_TI_symbol_name("_INT9CONT")
	.dwattr $C$DW$825, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$825, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$825, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$826	.dwtag  DW_TAG_member
	.dwattr $C$DW$826, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$826, DW_AT_name("rsvd1")
	.dwattr $C$DW$826, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$826, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$826, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$826, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$827	.dwtag  DW_TAG_member
	.dwattr $C$DW$827, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$827, DW_AT_name("INT10SEL")
	.dwattr $C$DW$827, DW_AT_TI_symbol_name("_INT10SEL")
	.dwattr $C$DW$827, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$827, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$827, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$828	.dwtag  DW_TAG_member
	.dwattr $C$DW$828, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$828, DW_AT_name("INT10E")
	.dwattr $C$DW$828, DW_AT_TI_symbol_name("_INT10E")
	.dwattr $C$DW$828, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$828, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$828, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$829	.dwtag  DW_TAG_member
	.dwattr $C$DW$829, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$829, DW_AT_name("INT10CONT")
	.dwattr $C$DW$829, DW_AT_TI_symbol_name("_INT10CONT")
	.dwattr $C$DW$829, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$829, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$829, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$830	.dwtag  DW_TAG_member
	.dwattr $C$DW$830, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$830, DW_AT_name("rsvd2")
	.dwattr $C$DW$830, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$830, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$830, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$830, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$111


$C$DW$T$112	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$112, DW_AT_name("INTSEL9N10_REG")
	.dwattr $C$DW$T$112, DW_AT_byte_size(0x01)
$C$DW$831	.dwtag  DW_TAG_member
	.dwattr $C$DW$831, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$831, DW_AT_name("all")
	.dwattr $C$DW$831, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$831, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$831, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$832	.dwtag  DW_TAG_member
	.dwattr $C$DW$832, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$832, DW_AT_name("bit")
	.dwattr $C$DW$832, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$832, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$832, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$112


$C$DW$T$120	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$120, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$120, DW_AT_byte_size(0x08)
$C$DW$833	.dwtag  DW_TAG_member
	.dwattr $C$DW$833, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$833, DW_AT_name("wListElem")
	.dwattr $C$DW$833, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$833, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$833, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$834	.dwtag  DW_TAG_member
	.dwattr $C$DW$834, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$834, DW_AT_name("wCount")
	.dwattr $C$DW$834, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$834, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$834, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$835	.dwtag  DW_TAG_member
	.dwattr $C$DW$835, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$835, DW_AT_name("fxn")
	.dwattr $C$DW$835, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$835, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$835, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$120

$C$DW$T$161	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$161, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$161, DW_AT_language(DW_LANG_C)
$C$DW$T$115	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$115, DW_AT_address_class(0x16)
$C$DW$T$116	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$116, DW_AT_language(DW_LANG_C)

$C$DW$T$127	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$127, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$127, DW_AT_byte_size(0x30)
$C$DW$836	.dwtag  DW_TAG_member
	.dwattr $C$DW$836, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$836, DW_AT_name("dataQue")
	.dwattr $C$DW$836, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$836, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$836, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$837	.dwtag  DW_TAG_member
	.dwattr $C$DW$837, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$837, DW_AT_name("freeQue")
	.dwattr $C$DW$837, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$837, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$837, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$838	.dwtag  DW_TAG_member
	.dwattr $C$DW$838, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$838, DW_AT_name("dataSem")
	.dwattr $C$DW$838, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$838, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$838, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$839	.dwtag  DW_TAG_member
	.dwattr $C$DW$839, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$839, DW_AT_name("freeSem")
	.dwattr $C$DW$839, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$839, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$839, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$840	.dwtag  DW_TAG_member
	.dwattr $C$DW$840, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$840, DW_AT_name("segid")
	.dwattr $C$DW$840, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$840, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$840, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$841	.dwtag  DW_TAG_member
	.dwattr $C$DW$841, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$841, DW_AT_name("size")
	.dwattr $C$DW$841, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$841, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$841, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$842	.dwtag  DW_TAG_member
	.dwattr $C$DW$842, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$842, DW_AT_name("length")
	.dwattr $C$DW$842, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$842, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$842, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$843	.dwtag  DW_TAG_member
	.dwattr $C$DW$843, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$843, DW_AT_name("name")
	.dwattr $C$DW$843, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$843, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$843, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$127

$C$DW$T$282	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$282, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$T$282, DW_AT_language(DW_LANG_C)
$C$DW$T$284	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$284, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$T$284, DW_AT_address_class(0x16)
$C$DW$T$285	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$285, DW_AT_type(*$C$DW$T$284)
	.dwattr $C$DW$T$285, DW_AT_language(DW_LANG_C)

$C$DW$T$128	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$128, DW_AT_name("PCCTL_BITS")
	.dwattr $C$DW$T$128, DW_AT_byte_size(0x01)
$C$DW$844	.dwtag  DW_TAG_member
	.dwattr $C$DW$844, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$844, DW_AT_name("CHPEN")
	.dwattr $C$DW$844, DW_AT_TI_symbol_name("_CHPEN")
	.dwattr $C$DW$844, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$844, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$844, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$845	.dwtag  DW_TAG_member
	.dwattr $C$DW$845, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$845, DW_AT_name("OSHTWTH")
	.dwattr $C$DW$845, DW_AT_TI_symbol_name("_OSHTWTH")
	.dwattr $C$DW$845, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x04)
	.dwattr $C$DW$845, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$845, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$846	.dwtag  DW_TAG_member
	.dwattr $C$DW$846, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$846, DW_AT_name("CHPFREQ")
	.dwattr $C$DW$846, DW_AT_TI_symbol_name("_CHPFREQ")
	.dwattr $C$DW$846, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x03)
	.dwattr $C$DW$846, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$846, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$847	.dwtag  DW_TAG_member
	.dwattr $C$DW$847, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$847, DW_AT_name("CHPDUTY")
	.dwattr $C$DW$847, DW_AT_TI_symbol_name("_CHPDUTY")
	.dwattr $C$DW$847, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x03)
	.dwattr $C$DW$847, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$847, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$848	.dwtag  DW_TAG_member
	.dwattr $C$DW$848, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$848, DW_AT_name("rsvd1")
	.dwattr $C$DW$848, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$848, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$848, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$848, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$128


$C$DW$T$129	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$129, DW_AT_name("PCCTL_REG")
	.dwattr $C$DW$T$129, DW_AT_byte_size(0x01)
$C$DW$849	.dwtag  DW_TAG_member
	.dwattr $C$DW$849, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$849, DW_AT_name("all")
	.dwattr $C$DW$849, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$849, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$849, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$850	.dwtag  DW_TAG_member
	.dwattr $C$DW$850, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$850, DW_AT_name("bit")
	.dwattr $C$DW$850, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$850, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$850, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$129


$C$DW$T$130	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$130, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x01)
$C$DW$851	.dwtag  DW_TAG_member
	.dwattr $C$DW$851, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$851, DW_AT_name("ACK1")
	.dwattr $C$DW$851, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$851, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$851, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$851, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$852	.dwtag  DW_TAG_member
	.dwattr $C$DW$852, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$852, DW_AT_name("ACK2")
	.dwattr $C$DW$852, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$852, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$852, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$852, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$853	.dwtag  DW_TAG_member
	.dwattr $C$DW$853, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$853, DW_AT_name("ACK3")
	.dwattr $C$DW$853, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$853, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$853, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$853, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$854	.dwtag  DW_TAG_member
	.dwattr $C$DW$854, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$854, DW_AT_name("ACK4")
	.dwattr $C$DW$854, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$854, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$854, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$854, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$855	.dwtag  DW_TAG_member
	.dwattr $C$DW$855, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$855, DW_AT_name("ACK5")
	.dwattr $C$DW$855, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$855, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$855, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$855, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$856	.dwtag  DW_TAG_member
	.dwattr $C$DW$856, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$856, DW_AT_name("ACK6")
	.dwattr $C$DW$856, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$856, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$856, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$856, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$857	.dwtag  DW_TAG_member
	.dwattr $C$DW$857, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$857, DW_AT_name("ACK7")
	.dwattr $C$DW$857, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$857, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$857, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$857, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$858	.dwtag  DW_TAG_member
	.dwattr $C$DW$858, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$858, DW_AT_name("ACK8")
	.dwattr $C$DW$858, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$858, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$858, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$858, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$859	.dwtag  DW_TAG_member
	.dwattr $C$DW$859, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$859, DW_AT_name("ACK9")
	.dwattr $C$DW$859, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$859, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$859, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$859, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$860	.dwtag  DW_TAG_member
	.dwattr $C$DW$860, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$860, DW_AT_name("ACK10")
	.dwattr $C$DW$860, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$860, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$860, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$860, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$861	.dwtag  DW_TAG_member
	.dwattr $C$DW$861, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$861, DW_AT_name("ACK11")
	.dwattr $C$DW$861, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$861, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$861, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$861, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$862	.dwtag  DW_TAG_member
	.dwattr $C$DW$862, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$862, DW_AT_name("ACK12")
	.dwattr $C$DW$862, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$862, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$862, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$862, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$863	.dwtag  DW_TAG_member
	.dwattr $C$DW$863, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$863, DW_AT_name("rsvd1")
	.dwattr $C$DW$863, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$863, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$863, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$863, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$130


$C$DW$T$131	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$131, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$131, DW_AT_byte_size(0x01)
$C$DW$864	.dwtag  DW_TAG_member
	.dwattr $C$DW$864, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$864, DW_AT_name("all")
	.dwattr $C$DW$864, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$864, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$864, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$865	.dwtag  DW_TAG_member
	.dwattr $C$DW$865, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$865, DW_AT_name("bit")
	.dwattr $C$DW$865, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$865, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$865, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$131


$C$DW$T$132	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$132, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$132, DW_AT_byte_size(0x01)
$C$DW$866	.dwtag  DW_TAG_member
	.dwattr $C$DW$866, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$866, DW_AT_name("ENPIE")
	.dwattr $C$DW$866, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$866, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$866, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$866, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$867	.dwtag  DW_TAG_member
	.dwattr $C$DW$867, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$867, DW_AT_name("PIEVECT")
	.dwattr $C$DW$867, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$867, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$867, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$867, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$132


$C$DW$T$133	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$133, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$133, DW_AT_byte_size(0x01)
$C$DW$868	.dwtag  DW_TAG_member
	.dwattr $C$DW$868, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$868, DW_AT_name("all")
	.dwattr $C$DW$868, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$868, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$868, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$869	.dwtag  DW_TAG_member
	.dwattr $C$DW$869, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$869, DW_AT_name("bit")
	.dwattr $C$DW$869, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$869, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$869, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$133


$C$DW$T$134	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$134, DW_AT_name("PIEIER_BITS")
	.dwattr $C$DW$T$134, DW_AT_byte_size(0x01)
$C$DW$870	.dwtag  DW_TAG_member
	.dwattr $C$DW$870, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$870, DW_AT_name("INTx1")
	.dwattr $C$DW$870, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$870, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$870, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$870, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$871	.dwtag  DW_TAG_member
	.dwattr $C$DW$871, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$871, DW_AT_name("INTx2")
	.dwattr $C$DW$871, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$871, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$871, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$871, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$872	.dwtag  DW_TAG_member
	.dwattr $C$DW$872, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$872, DW_AT_name("INTx3")
	.dwattr $C$DW$872, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$872, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$872, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$872, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$873	.dwtag  DW_TAG_member
	.dwattr $C$DW$873, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$873, DW_AT_name("INTx4")
	.dwattr $C$DW$873, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$873, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$873, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$873, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$874	.dwtag  DW_TAG_member
	.dwattr $C$DW$874, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$874, DW_AT_name("INTx5")
	.dwattr $C$DW$874, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$874, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$874, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$874, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$875	.dwtag  DW_TAG_member
	.dwattr $C$DW$875, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$875, DW_AT_name("INTx6")
	.dwattr $C$DW$875, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$875, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$875, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$875, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$876	.dwtag  DW_TAG_member
	.dwattr $C$DW$876, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$876, DW_AT_name("INTx7")
	.dwattr $C$DW$876, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$876, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$876, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$876, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$877	.dwtag  DW_TAG_member
	.dwattr $C$DW$877, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$877, DW_AT_name("INTx8")
	.dwattr $C$DW$877, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$877, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$877, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$877, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$878	.dwtag  DW_TAG_member
	.dwattr $C$DW$878, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$878, DW_AT_name("rsvd1")
	.dwattr $C$DW$878, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$878, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$878, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$878, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$134


$C$DW$T$135	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$135, DW_AT_name("PIEIER_REG")
	.dwattr $C$DW$T$135, DW_AT_byte_size(0x01)
$C$DW$879	.dwtag  DW_TAG_member
	.dwattr $C$DW$879, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$879, DW_AT_name("all")
	.dwattr $C$DW$879, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$879, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$879, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$880	.dwtag  DW_TAG_member
	.dwattr $C$DW$880, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$880, DW_AT_name("bit")
	.dwattr $C$DW$880, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$880, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$880, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$135


$C$DW$T$136	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$136, DW_AT_name("PIEIFR_BITS")
	.dwattr $C$DW$T$136, DW_AT_byte_size(0x01)
$C$DW$881	.dwtag  DW_TAG_member
	.dwattr $C$DW$881, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$881, DW_AT_name("INTx1")
	.dwattr $C$DW$881, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$881, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$881, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$881, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$882	.dwtag  DW_TAG_member
	.dwattr $C$DW$882, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$882, DW_AT_name("INTx2")
	.dwattr $C$DW$882, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$882, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$882, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$882, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$883	.dwtag  DW_TAG_member
	.dwattr $C$DW$883, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$883, DW_AT_name("INTx3")
	.dwattr $C$DW$883, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$883, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$883, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$883, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$884	.dwtag  DW_TAG_member
	.dwattr $C$DW$884, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$884, DW_AT_name("INTx4")
	.dwattr $C$DW$884, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$884, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$884, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$884, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$885	.dwtag  DW_TAG_member
	.dwattr $C$DW$885, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$885, DW_AT_name("INTx5")
	.dwattr $C$DW$885, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$885, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$885, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$885, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$886	.dwtag  DW_TAG_member
	.dwattr $C$DW$886, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$886, DW_AT_name("INTx6")
	.dwattr $C$DW$886, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$886, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$886, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$886, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$887	.dwtag  DW_TAG_member
	.dwattr $C$DW$887, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$887, DW_AT_name("INTx7")
	.dwattr $C$DW$887, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$887, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$887, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$887, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$888	.dwtag  DW_TAG_member
	.dwattr $C$DW$888, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$888, DW_AT_name("INTx8")
	.dwattr $C$DW$888, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$888, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$888, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$888, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$889	.dwtag  DW_TAG_member
	.dwattr $C$DW$889, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$889, DW_AT_name("rsvd1")
	.dwattr $C$DW$889, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$889, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$889, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$889, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$136


$C$DW$T$137	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$137, DW_AT_name("PIEIFR_REG")
	.dwattr $C$DW$T$137, DW_AT_byte_size(0x01)
$C$DW$890	.dwtag  DW_TAG_member
	.dwattr $C$DW$890, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$890, DW_AT_name("all")
	.dwattr $C$DW$890, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$890, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$890, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$891	.dwtag  DW_TAG_member
	.dwattr $C$DW$891, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$891, DW_AT_name("bit")
	.dwattr $C$DW$891, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$891, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$891, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$137


$C$DW$T$138	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$138, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$138, DW_AT_byte_size(0x1a)
$C$DW$892	.dwtag  DW_TAG_member
	.dwattr $C$DW$892, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$892, DW_AT_name("PIECTRL")
	.dwattr $C$DW$892, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$892, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$892, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$893	.dwtag  DW_TAG_member
	.dwattr $C$DW$893, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$893, DW_AT_name("PIEACK")
	.dwattr $C$DW$893, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$893, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$893, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$894	.dwtag  DW_TAG_member
	.dwattr $C$DW$894, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$894, DW_AT_name("PIEIER1")
	.dwattr $C$DW$894, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$894, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$894, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$895	.dwtag  DW_TAG_member
	.dwattr $C$DW$895, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$895, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$895, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$895, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$895, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$896	.dwtag  DW_TAG_member
	.dwattr $C$DW$896, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$896, DW_AT_name("PIEIER2")
	.dwattr $C$DW$896, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$896, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$896, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$897	.dwtag  DW_TAG_member
	.dwattr $C$DW$897, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$897, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$897, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$897, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$897, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$898	.dwtag  DW_TAG_member
	.dwattr $C$DW$898, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$898, DW_AT_name("PIEIER3")
	.dwattr $C$DW$898, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$898, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$898, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$899	.dwtag  DW_TAG_member
	.dwattr $C$DW$899, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$899, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$899, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$899, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$899, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$900	.dwtag  DW_TAG_member
	.dwattr $C$DW$900, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$900, DW_AT_name("PIEIER4")
	.dwattr $C$DW$900, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$900, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$900, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$901	.dwtag  DW_TAG_member
	.dwattr $C$DW$901, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$901, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$901, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$901, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$901, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$902	.dwtag  DW_TAG_member
	.dwattr $C$DW$902, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$902, DW_AT_name("PIEIER5")
	.dwattr $C$DW$902, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$902, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$902, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$903	.dwtag  DW_TAG_member
	.dwattr $C$DW$903, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$903, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$903, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$903, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$903, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$904	.dwtag  DW_TAG_member
	.dwattr $C$DW$904, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$904, DW_AT_name("PIEIER6")
	.dwattr $C$DW$904, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$904, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$904, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$905	.dwtag  DW_TAG_member
	.dwattr $C$DW$905, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$905, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$905, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$905, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$905, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$906	.dwtag  DW_TAG_member
	.dwattr $C$DW$906, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$906, DW_AT_name("PIEIER7")
	.dwattr $C$DW$906, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$906, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$906, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$907	.dwtag  DW_TAG_member
	.dwattr $C$DW$907, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$907, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$907, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$907, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$907, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$908	.dwtag  DW_TAG_member
	.dwattr $C$DW$908, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$908, DW_AT_name("PIEIER8")
	.dwattr $C$DW$908, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$908, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$908, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$909	.dwtag  DW_TAG_member
	.dwattr $C$DW$909, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$909, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$909, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$909, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$909, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$910	.dwtag  DW_TAG_member
	.dwattr $C$DW$910, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$910, DW_AT_name("PIEIER9")
	.dwattr $C$DW$910, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$910, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$910, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$911	.dwtag  DW_TAG_member
	.dwattr $C$DW$911, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$911, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$911, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$911, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$911, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$912	.dwtag  DW_TAG_member
	.dwattr $C$DW$912, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$912, DW_AT_name("PIEIER10")
	.dwattr $C$DW$912, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$912, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$912, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$913	.dwtag  DW_TAG_member
	.dwattr $C$DW$913, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$913, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$913, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$913, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$913, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$914	.dwtag  DW_TAG_member
	.dwattr $C$DW$914, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$914, DW_AT_name("PIEIER11")
	.dwattr $C$DW$914, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$914, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$914, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$915	.dwtag  DW_TAG_member
	.dwattr $C$DW$915, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$915, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$915, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$915, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$915, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$916	.dwtag  DW_TAG_member
	.dwattr $C$DW$916, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$916, DW_AT_name("PIEIER12")
	.dwattr $C$DW$916, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$916, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$916, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$917	.dwtag  DW_TAG_member
	.dwattr $C$DW$917, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$917, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$917, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$917, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$917, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$138

$C$DW$918	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$918, DW_AT_type(*$C$DW$T$138)
$C$DW$T$290	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$290, DW_AT_type(*$C$DW$918)

$C$DW$T$142	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$142, DW_AT_name("PIE_VECT_TABLE")
	.dwattr $C$DW$T$142, DW_AT_byte_size(0x100)
$C$DW$919	.dwtag  DW_TAG_member
	.dwattr $C$DW$919, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$919, DW_AT_name("PIE1_RESERVED")
	.dwattr $C$DW$919, DW_AT_TI_symbol_name("_PIE1_RESERVED")
	.dwattr $C$DW$919, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$919, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$920	.dwtag  DW_TAG_member
	.dwattr $C$DW$920, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$920, DW_AT_name("PIE2_RESERVED")
	.dwattr $C$DW$920, DW_AT_TI_symbol_name("_PIE2_RESERVED")
	.dwattr $C$DW$920, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$920, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$921	.dwtag  DW_TAG_member
	.dwattr $C$DW$921, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$921, DW_AT_name("PIE3_RESERVED")
	.dwattr $C$DW$921, DW_AT_TI_symbol_name("_PIE3_RESERVED")
	.dwattr $C$DW$921, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$921, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$922	.dwtag  DW_TAG_member
	.dwattr $C$DW$922, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$922, DW_AT_name("PIE4_RESERVED")
	.dwattr $C$DW$922, DW_AT_TI_symbol_name("_PIE4_RESERVED")
	.dwattr $C$DW$922, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$922, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$923	.dwtag  DW_TAG_member
	.dwattr $C$DW$923, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$923, DW_AT_name("PIE5_RESERVED")
	.dwattr $C$DW$923, DW_AT_TI_symbol_name("_PIE5_RESERVED")
	.dwattr $C$DW$923, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$923, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$924	.dwtag  DW_TAG_member
	.dwattr $C$DW$924, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$924, DW_AT_name("PIE6_RESERVED")
	.dwattr $C$DW$924, DW_AT_TI_symbol_name("_PIE6_RESERVED")
	.dwattr $C$DW$924, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$924, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$925	.dwtag  DW_TAG_member
	.dwattr $C$DW$925, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$925, DW_AT_name("PIE7_RESERVED")
	.dwattr $C$DW$925, DW_AT_TI_symbol_name("_PIE7_RESERVED")
	.dwattr $C$DW$925, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$925, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$926	.dwtag  DW_TAG_member
	.dwattr $C$DW$926, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$926, DW_AT_name("PIE8_RESERVED")
	.dwattr $C$DW$926, DW_AT_TI_symbol_name("_PIE8_RESERVED")
	.dwattr $C$DW$926, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$926, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$927	.dwtag  DW_TAG_member
	.dwattr $C$DW$927, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$927, DW_AT_name("PIE9_RESERVED")
	.dwattr $C$DW$927, DW_AT_TI_symbol_name("_PIE9_RESERVED")
	.dwattr $C$DW$927, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$927, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$928	.dwtag  DW_TAG_member
	.dwattr $C$DW$928, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$928, DW_AT_name("PIE10_RESERVED")
	.dwattr $C$DW$928, DW_AT_TI_symbol_name("_PIE10_RESERVED")
	.dwattr $C$DW$928, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$928, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$929	.dwtag  DW_TAG_member
	.dwattr $C$DW$929, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$929, DW_AT_name("PIE11_RESERVED")
	.dwattr $C$DW$929, DW_AT_TI_symbol_name("_PIE11_RESERVED")
	.dwattr $C$DW$929, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$929, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$930	.dwtag  DW_TAG_member
	.dwattr $C$DW$930, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$930, DW_AT_name("PIE12_RESERVED")
	.dwattr $C$DW$930, DW_AT_TI_symbol_name("_PIE12_RESERVED")
	.dwattr $C$DW$930, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$930, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$931	.dwtag  DW_TAG_member
	.dwattr $C$DW$931, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$931, DW_AT_name("PIE13_RESERVED")
	.dwattr $C$DW$931, DW_AT_TI_symbol_name("_PIE13_RESERVED")
	.dwattr $C$DW$931, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$931, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$932	.dwtag  DW_TAG_member
	.dwattr $C$DW$932, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$932, DW_AT_name("TINT1")
	.dwattr $C$DW$932, DW_AT_TI_symbol_name("_TINT1")
	.dwattr $C$DW$932, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$932, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$933	.dwtag  DW_TAG_member
	.dwattr $C$DW$933, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$933, DW_AT_name("TINT2")
	.dwattr $C$DW$933, DW_AT_TI_symbol_name("_TINT2")
	.dwattr $C$DW$933, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$933, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$934	.dwtag  DW_TAG_member
	.dwattr $C$DW$934, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$934, DW_AT_name("DATALOG")
	.dwattr $C$DW$934, DW_AT_TI_symbol_name("_DATALOG")
	.dwattr $C$DW$934, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$934, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$935	.dwtag  DW_TAG_member
	.dwattr $C$DW$935, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$935, DW_AT_name("RTOSINT")
	.dwattr $C$DW$935, DW_AT_TI_symbol_name("_RTOSINT")
	.dwattr $C$DW$935, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$935, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$936	.dwtag  DW_TAG_member
	.dwattr $C$DW$936, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$936, DW_AT_name("EMUINT")
	.dwattr $C$DW$936, DW_AT_TI_symbol_name("_EMUINT")
	.dwattr $C$DW$936, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$936, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$937	.dwtag  DW_TAG_member
	.dwattr $C$DW$937, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$937, DW_AT_name("NMI")
	.dwattr $C$DW$937, DW_AT_TI_symbol_name("_NMI")
	.dwattr $C$DW$937, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$937, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$938	.dwtag  DW_TAG_member
	.dwattr $C$DW$938, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$938, DW_AT_name("ILLEGAL")
	.dwattr $C$DW$938, DW_AT_TI_symbol_name("_ILLEGAL")
	.dwattr $C$DW$938, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$938, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$939	.dwtag  DW_TAG_member
	.dwattr $C$DW$939, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$939, DW_AT_name("USER1")
	.dwattr $C$DW$939, DW_AT_TI_symbol_name("_USER1")
	.dwattr $C$DW$939, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$939, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$940	.dwtag  DW_TAG_member
	.dwattr $C$DW$940, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$940, DW_AT_name("USER2")
	.dwattr $C$DW$940, DW_AT_TI_symbol_name("_USER2")
	.dwattr $C$DW$940, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$940, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$941	.dwtag  DW_TAG_member
	.dwattr $C$DW$941, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$941, DW_AT_name("USER3")
	.dwattr $C$DW$941, DW_AT_TI_symbol_name("_USER3")
	.dwattr $C$DW$941, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$941, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$942	.dwtag  DW_TAG_member
	.dwattr $C$DW$942, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$942, DW_AT_name("USER4")
	.dwattr $C$DW$942, DW_AT_TI_symbol_name("_USER4")
	.dwattr $C$DW$942, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$942, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$943	.dwtag  DW_TAG_member
	.dwattr $C$DW$943, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$943, DW_AT_name("USER5")
	.dwattr $C$DW$943, DW_AT_TI_symbol_name("_USER5")
	.dwattr $C$DW$943, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$943, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$944	.dwtag  DW_TAG_member
	.dwattr $C$DW$944, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$944, DW_AT_name("USER6")
	.dwattr $C$DW$944, DW_AT_TI_symbol_name("_USER6")
	.dwattr $C$DW$944, DW_AT_data_member_location[DW_OP_plus_uconst 0x32]
	.dwattr $C$DW$944, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$945	.dwtag  DW_TAG_member
	.dwattr $C$DW$945, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$945, DW_AT_name("USER7")
	.dwattr $C$DW$945, DW_AT_TI_symbol_name("_USER7")
	.dwattr $C$DW$945, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$945, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$946	.dwtag  DW_TAG_member
	.dwattr $C$DW$946, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$946, DW_AT_name("USER8")
	.dwattr $C$DW$946, DW_AT_TI_symbol_name("_USER8")
	.dwattr $C$DW$946, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$946, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$947	.dwtag  DW_TAG_member
	.dwattr $C$DW$947, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$947, DW_AT_name("USER9")
	.dwattr $C$DW$947, DW_AT_TI_symbol_name("_USER9")
	.dwattr $C$DW$947, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$947, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$948	.dwtag  DW_TAG_member
	.dwattr $C$DW$948, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$948, DW_AT_name("USER10")
	.dwattr $C$DW$948, DW_AT_TI_symbol_name("_USER10")
	.dwattr $C$DW$948, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$948, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$949	.dwtag  DW_TAG_member
	.dwattr $C$DW$949, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$949, DW_AT_name("USER11")
	.dwattr $C$DW$949, DW_AT_TI_symbol_name("_USER11")
	.dwattr $C$DW$949, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$949, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$950	.dwtag  DW_TAG_member
	.dwattr $C$DW$950, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$950, DW_AT_name("USER12")
	.dwattr $C$DW$950, DW_AT_TI_symbol_name("_USER12")
	.dwattr $C$DW$950, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$950, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$951	.dwtag  DW_TAG_member
	.dwattr $C$DW$951, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$951, DW_AT_name("ADCINT1")
	.dwattr $C$DW$951, DW_AT_TI_symbol_name("_ADCINT1")
	.dwattr $C$DW$951, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$951, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$952	.dwtag  DW_TAG_member
	.dwattr $C$DW$952, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$952, DW_AT_name("ADCINT2")
	.dwattr $C$DW$952, DW_AT_TI_symbol_name("_ADCINT2")
	.dwattr $C$DW$952, DW_AT_data_member_location[DW_OP_plus_uconst 0x42]
	.dwattr $C$DW$952, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$953	.dwtag  DW_TAG_member
	.dwattr $C$DW$953, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$953, DW_AT_name("rsvd1_3")
	.dwattr $C$DW$953, DW_AT_TI_symbol_name("_rsvd1_3")
	.dwattr $C$DW$953, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$953, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$954	.dwtag  DW_TAG_member
	.dwattr $C$DW$954, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$954, DW_AT_name("XINT1")
	.dwattr $C$DW$954, DW_AT_TI_symbol_name("_XINT1")
	.dwattr $C$DW$954, DW_AT_data_member_location[DW_OP_plus_uconst 0x46]
	.dwattr $C$DW$954, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$955	.dwtag  DW_TAG_member
	.dwattr $C$DW$955, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$955, DW_AT_name("XINT2")
	.dwattr $C$DW$955, DW_AT_TI_symbol_name("_XINT2")
	.dwattr $C$DW$955, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$955, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$956	.dwtag  DW_TAG_member
	.dwattr $C$DW$956, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$956, DW_AT_name("ADCINT9")
	.dwattr $C$DW$956, DW_AT_TI_symbol_name("_ADCINT9")
	.dwattr $C$DW$956, DW_AT_data_member_location[DW_OP_plus_uconst 0x4a]
	.dwattr $C$DW$956, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$957	.dwtag  DW_TAG_member
	.dwattr $C$DW$957, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$957, DW_AT_name("TINT0")
	.dwattr $C$DW$957, DW_AT_TI_symbol_name("_TINT0")
	.dwattr $C$DW$957, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$957, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$958	.dwtag  DW_TAG_member
	.dwattr $C$DW$958, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$958, DW_AT_name("WAKEINT")
	.dwattr $C$DW$958, DW_AT_TI_symbol_name("_WAKEINT")
	.dwattr $C$DW$958, DW_AT_data_member_location[DW_OP_plus_uconst 0x4e]
	.dwattr $C$DW$958, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$959	.dwtag  DW_TAG_member
	.dwattr $C$DW$959, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$959, DW_AT_name("EPWM1_TZINT")
	.dwattr $C$DW$959, DW_AT_TI_symbol_name("_EPWM1_TZINT")
	.dwattr $C$DW$959, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$959, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$960	.dwtag  DW_TAG_member
	.dwattr $C$DW$960, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$960, DW_AT_name("EPWM2_TZINT")
	.dwattr $C$DW$960, DW_AT_TI_symbol_name("_EPWM2_TZINT")
	.dwattr $C$DW$960, DW_AT_data_member_location[DW_OP_plus_uconst 0x52]
	.dwattr $C$DW$960, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$961	.dwtag  DW_TAG_member
	.dwattr $C$DW$961, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$961, DW_AT_name("EPWM3_TZINT")
	.dwattr $C$DW$961, DW_AT_TI_symbol_name("_EPWM3_TZINT")
	.dwattr $C$DW$961, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$961, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$962	.dwtag  DW_TAG_member
	.dwattr $C$DW$962, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$962, DW_AT_name("EPWM4_TZINT")
	.dwattr $C$DW$962, DW_AT_TI_symbol_name("_EPWM4_TZINT")
	.dwattr $C$DW$962, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$962, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$963	.dwtag  DW_TAG_member
	.dwattr $C$DW$963, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$963, DW_AT_name("EPWM5_TZINT")
	.dwattr $C$DW$963, DW_AT_TI_symbol_name("_EPWM5_TZINT")
	.dwattr $C$DW$963, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$963, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$964	.dwtag  DW_TAG_member
	.dwattr $C$DW$964, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$964, DW_AT_name("EPWM6_TZINT")
	.dwattr $C$DW$964, DW_AT_TI_symbol_name("_EPWM6_TZINT")
	.dwattr $C$DW$964, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$964, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$965	.dwtag  DW_TAG_member
	.dwattr $C$DW$965, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$965, DW_AT_name("EPWM7_TZINT")
	.dwattr $C$DW$965, DW_AT_TI_symbol_name("_EPWM7_TZINT")
	.dwattr $C$DW$965, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$965, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$966	.dwtag  DW_TAG_member
	.dwattr $C$DW$966, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$966, DW_AT_name("EPWM8_TZINT")
	.dwattr $C$DW$966, DW_AT_TI_symbol_name("_EPWM8_TZINT")
	.dwattr $C$DW$966, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$966, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$967	.dwtag  DW_TAG_member
	.dwattr $C$DW$967, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$967, DW_AT_name("EPWM1_INT")
	.dwattr $C$DW$967, DW_AT_TI_symbol_name("_EPWM1_INT")
	.dwattr $C$DW$967, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$967, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$968	.dwtag  DW_TAG_member
	.dwattr $C$DW$968, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$968, DW_AT_name("EPWM2_INT")
	.dwattr $C$DW$968, DW_AT_TI_symbol_name("_EPWM2_INT")
	.dwattr $C$DW$968, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$968, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$969	.dwtag  DW_TAG_member
	.dwattr $C$DW$969, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$969, DW_AT_name("EPWM3_INT")
	.dwattr $C$DW$969, DW_AT_TI_symbol_name("_EPWM3_INT")
	.dwattr $C$DW$969, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$969, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$970	.dwtag  DW_TAG_member
	.dwattr $C$DW$970, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$970, DW_AT_name("EPWM4_INT")
	.dwattr $C$DW$970, DW_AT_TI_symbol_name("_EPWM4_INT")
	.dwattr $C$DW$970, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$970, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$971	.dwtag  DW_TAG_member
	.dwattr $C$DW$971, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$971, DW_AT_name("EPWM5_INT")
	.dwattr $C$DW$971, DW_AT_TI_symbol_name("_EPWM5_INT")
	.dwattr $C$DW$971, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$971, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$972	.dwtag  DW_TAG_member
	.dwattr $C$DW$972, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$972, DW_AT_name("EPWM6_INT")
	.dwattr $C$DW$972, DW_AT_TI_symbol_name("_EPWM6_INT")
	.dwattr $C$DW$972, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$972, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$973	.dwtag  DW_TAG_member
	.dwattr $C$DW$973, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$973, DW_AT_name("EPWM7_INT")
	.dwattr $C$DW$973, DW_AT_TI_symbol_name("_EPWM7_INT")
	.dwattr $C$DW$973, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$973, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$974	.dwtag  DW_TAG_member
	.dwattr $C$DW$974, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$974, DW_AT_name("EPWM8_INT")
	.dwattr $C$DW$974, DW_AT_TI_symbol_name("_EPWM8_INT")
	.dwattr $C$DW$974, DW_AT_data_member_location[DW_OP_plus_uconst 0x6e]
	.dwattr $C$DW$974, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$975	.dwtag  DW_TAG_member
	.dwattr $C$DW$975, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$975, DW_AT_name("ECAP1_INT")
	.dwattr $C$DW$975, DW_AT_TI_symbol_name("_ECAP1_INT")
	.dwattr $C$DW$975, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$975, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$976	.dwtag  DW_TAG_member
	.dwattr $C$DW$976, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$976, DW_AT_name("ECAP2_INT")
	.dwattr $C$DW$976, DW_AT_TI_symbol_name("_ECAP2_INT")
	.dwattr $C$DW$976, DW_AT_data_member_location[DW_OP_plus_uconst 0x72]
	.dwattr $C$DW$976, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$977	.dwtag  DW_TAG_member
	.dwattr $C$DW$977, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$977, DW_AT_name("ECAP3_INT")
	.dwattr $C$DW$977, DW_AT_TI_symbol_name("_ECAP3_INT")
	.dwattr $C$DW$977, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$977, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$978	.dwtag  DW_TAG_member
	.dwattr $C$DW$978, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$978, DW_AT_name("rsvd4_4")
	.dwattr $C$DW$978, DW_AT_TI_symbol_name("_rsvd4_4")
	.dwattr $C$DW$978, DW_AT_data_member_location[DW_OP_plus_uconst 0x76]
	.dwattr $C$DW$978, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$979	.dwtag  DW_TAG_member
	.dwattr $C$DW$979, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$979, DW_AT_name("rsvd4_5")
	.dwattr $C$DW$979, DW_AT_TI_symbol_name("_rsvd4_5")
	.dwattr $C$DW$979, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$979, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$980	.dwtag  DW_TAG_member
	.dwattr $C$DW$980, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$980, DW_AT_name("rsvd4_6")
	.dwattr $C$DW$980, DW_AT_TI_symbol_name("_rsvd4_6")
	.dwattr $C$DW$980, DW_AT_data_member_location[DW_OP_plus_uconst 0x7a]
	.dwattr $C$DW$980, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$981	.dwtag  DW_TAG_member
	.dwattr $C$DW$981, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$981, DW_AT_name("HRCAP1_INT")
	.dwattr $C$DW$981, DW_AT_TI_symbol_name("_HRCAP1_INT")
	.dwattr $C$DW$981, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$981, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$982	.dwtag  DW_TAG_member
	.dwattr $C$DW$982, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$982, DW_AT_name("HRCAP2_INT")
	.dwattr $C$DW$982, DW_AT_TI_symbol_name("_HRCAP2_INT")
	.dwattr $C$DW$982, DW_AT_data_member_location[DW_OP_plus_uconst 0x7e]
	.dwattr $C$DW$982, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$983	.dwtag  DW_TAG_member
	.dwattr $C$DW$983, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$983, DW_AT_name("EQEP1_INT")
	.dwattr $C$DW$983, DW_AT_TI_symbol_name("_EQEP1_INT")
	.dwattr $C$DW$983, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$983, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$984	.dwtag  DW_TAG_member
	.dwattr $C$DW$984, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$984, DW_AT_name("EQEP2_INT")
	.dwattr $C$DW$984, DW_AT_TI_symbol_name("_EQEP2_INT")
	.dwattr $C$DW$984, DW_AT_data_member_location[DW_OP_plus_uconst 0x82]
	.dwattr $C$DW$984, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$985	.dwtag  DW_TAG_member
	.dwattr $C$DW$985, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$985, DW_AT_name("rsvd5_3")
	.dwattr $C$DW$985, DW_AT_TI_symbol_name("_rsvd5_3")
	.dwattr $C$DW$985, DW_AT_data_member_location[DW_OP_plus_uconst 0x84]
	.dwattr $C$DW$985, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$986	.dwtag  DW_TAG_member
	.dwattr $C$DW$986, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$986, DW_AT_name("HRCAP3_INT")
	.dwattr $C$DW$986, DW_AT_TI_symbol_name("_HRCAP3_INT")
	.dwattr $C$DW$986, DW_AT_data_member_location[DW_OP_plus_uconst 0x86]
	.dwattr $C$DW$986, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$987	.dwtag  DW_TAG_member
	.dwattr $C$DW$987, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$987, DW_AT_name("HRCAP4_INT")
	.dwattr $C$DW$987, DW_AT_TI_symbol_name("_HRCAP4_INT")
	.dwattr $C$DW$987, DW_AT_data_member_location[DW_OP_plus_uconst 0x88]
	.dwattr $C$DW$987, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$988	.dwtag  DW_TAG_member
	.dwattr $C$DW$988, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$988, DW_AT_name("rsvd5_6")
	.dwattr $C$DW$988, DW_AT_TI_symbol_name("_rsvd5_6")
	.dwattr $C$DW$988, DW_AT_data_member_location[DW_OP_plus_uconst 0x8a]
	.dwattr $C$DW$988, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$989	.dwtag  DW_TAG_member
	.dwattr $C$DW$989, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$989, DW_AT_name("rsvd5_7")
	.dwattr $C$DW$989, DW_AT_TI_symbol_name("_rsvd5_7")
	.dwattr $C$DW$989, DW_AT_data_member_location[DW_OP_plus_uconst 0x8c]
	.dwattr $C$DW$989, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$990	.dwtag  DW_TAG_member
	.dwattr $C$DW$990, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$990, DW_AT_name("USB0_INT")
	.dwattr $C$DW$990, DW_AT_TI_symbol_name("_USB0_INT")
	.dwattr $C$DW$990, DW_AT_data_member_location[DW_OP_plus_uconst 0x8e]
	.dwattr $C$DW$990, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$991	.dwtag  DW_TAG_member
	.dwattr $C$DW$991, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$991, DW_AT_name("SPIRXINTA")
	.dwattr $C$DW$991, DW_AT_TI_symbol_name("_SPIRXINTA")
	.dwattr $C$DW$991, DW_AT_data_member_location[DW_OP_plus_uconst 0x90]
	.dwattr $C$DW$991, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$992	.dwtag  DW_TAG_member
	.dwattr $C$DW$992, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$992, DW_AT_name("SPITXINTA")
	.dwattr $C$DW$992, DW_AT_TI_symbol_name("_SPITXINTA")
	.dwattr $C$DW$992, DW_AT_data_member_location[DW_OP_plus_uconst 0x92]
	.dwattr $C$DW$992, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$993	.dwtag  DW_TAG_member
	.dwattr $C$DW$993, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$993, DW_AT_name("SPIRXINTB")
	.dwattr $C$DW$993, DW_AT_TI_symbol_name("_SPIRXINTB")
	.dwattr $C$DW$993, DW_AT_data_member_location[DW_OP_plus_uconst 0x94]
	.dwattr $C$DW$993, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$994	.dwtag  DW_TAG_member
	.dwattr $C$DW$994, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$994, DW_AT_name("SPITXINTB")
	.dwattr $C$DW$994, DW_AT_TI_symbol_name("_SPITXINTB")
	.dwattr $C$DW$994, DW_AT_data_member_location[DW_OP_plus_uconst 0x96]
	.dwattr $C$DW$994, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$995	.dwtag  DW_TAG_member
	.dwattr $C$DW$995, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$995, DW_AT_name("MRINTA")
	.dwattr $C$DW$995, DW_AT_TI_symbol_name("_MRINTA")
	.dwattr $C$DW$995, DW_AT_data_member_location[DW_OP_plus_uconst 0x98]
	.dwattr $C$DW$995, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$996	.dwtag  DW_TAG_member
	.dwattr $C$DW$996, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$996, DW_AT_name("MXINTA")
	.dwattr $C$DW$996, DW_AT_TI_symbol_name("_MXINTA")
	.dwattr $C$DW$996, DW_AT_data_member_location[DW_OP_plus_uconst 0x9a]
	.dwattr $C$DW$996, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$997	.dwtag  DW_TAG_member
	.dwattr $C$DW$997, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$997, DW_AT_name("rsvd6_7")
	.dwattr $C$DW$997, DW_AT_TI_symbol_name("_rsvd6_7")
	.dwattr $C$DW$997, DW_AT_data_member_location[DW_OP_plus_uconst 0x9c]
	.dwattr $C$DW$997, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$998	.dwtag  DW_TAG_member
	.dwattr $C$DW$998, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$998, DW_AT_name("rsvd6_8")
	.dwattr $C$DW$998, DW_AT_TI_symbol_name("_rsvd6_8")
	.dwattr $C$DW$998, DW_AT_data_member_location[DW_OP_plus_uconst 0x9e]
	.dwattr $C$DW$998, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$999	.dwtag  DW_TAG_member
	.dwattr $C$DW$999, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$999, DW_AT_name("DINTCH1")
	.dwattr $C$DW$999, DW_AT_TI_symbol_name("_DINTCH1")
	.dwattr $C$DW$999, DW_AT_data_member_location[DW_OP_plus_uconst 0xa0]
	.dwattr $C$DW$999, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1000	.dwtag  DW_TAG_member
	.dwattr $C$DW$1000, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1000, DW_AT_name("DINTCH2")
	.dwattr $C$DW$1000, DW_AT_TI_symbol_name("_DINTCH2")
	.dwattr $C$DW$1000, DW_AT_data_member_location[DW_OP_plus_uconst 0xa2]
	.dwattr $C$DW$1000, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1001	.dwtag  DW_TAG_member
	.dwattr $C$DW$1001, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1001, DW_AT_name("DINTCH3")
	.dwattr $C$DW$1001, DW_AT_TI_symbol_name("_DINTCH3")
	.dwattr $C$DW$1001, DW_AT_data_member_location[DW_OP_plus_uconst 0xa4]
	.dwattr $C$DW$1001, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1002	.dwtag  DW_TAG_member
	.dwattr $C$DW$1002, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1002, DW_AT_name("DINTCH4")
	.dwattr $C$DW$1002, DW_AT_TI_symbol_name("_DINTCH4")
	.dwattr $C$DW$1002, DW_AT_data_member_location[DW_OP_plus_uconst 0xa6]
	.dwattr $C$DW$1002, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1003	.dwtag  DW_TAG_member
	.dwattr $C$DW$1003, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1003, DW_AT_name("DINTCH5")
	.dwattr $C$DW$1003, DW_AT_TI_symbol_name("_DINTCH5")
	.dwattr $C$DW$1003, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$1003, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1004	.dwtag  DW_TAG_member
	.dwattr $C$DW$1004, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1004, DW_AT_name("DINTCH6")
	.dwattr $C$DW$1004, DW_AT_TI_symbol_name("_DINTCH6")
	.dwattr $C$DW$1004, DW_AT_data_member_location[DW_OP_plus_uconst 0xaa]
	.dwattr $C$DW$1004, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1005	.dwtag  DW_TAG_member
	.dwattr $C$DW$1005, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1005, DW_AT_name("rsvd7_7")
	.dwattr $C$DW$1005, DW_AT_TI_symbol_name("_rsvd7_7")
	.dwattr $C$DW$1005, DW_AT_data_member_location[DW_OP_plus_uconst 0xac]
	.dwattr $C$DW$1005, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1006	.dwtag  DW_TAG_member
	.dwattr $C$DW$1006, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1006, DW_AT_name("rsvd7_8")
	.dwattr $C$DW$1006, DW_AT_TI_symbol_name("_rsvd7_8")
	.dwattr $C$DW$1006, DW_AT_data_member_location[DW_OP_plus_uconst 0xae]
	.dwattr $C$DW$1006, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1007	.dwtag  DW_TAG_member
	.dwattr $C$DW$1007, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1007, DW_AT_name("I2CINT1A")
	.dwattr $C$DW$1007, DW_AT_TI_symbol_name("_I2CINT1A")
	.dwattr $C$DW$1007, DW_AT_data_member_location[DW_OP_plus_uconst 0xb0]
	.dwattr $C$DW$1007, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1008	.dwtag  DW_TAG_member
	.dwattr $C$DW$1008, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1008, DW_AT_name("I2CINT2A")
	.dwattr $C$DW$1008, DW_AT_TI_symbol_name("_I2CINT2A")
	.dwattr $C$DW$1008, DW_AT_data_member_location[DW_OP_plus_uconst 0xb2]
	.dwattr $C$DW$1008, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1009	.dwtag  DW_TAG_member
	.dwattr $C$DW$1009, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1009, DW_AT_name("rsvd8_3")
	.dwattr $C$DW$1009, DW_AT_TI_symbol_name("_rsvd8_3")
	.dwattr $C$DW$1009, DW_AT_data_member_location[DW_OP_plus_uconst 0xb4]
	.dwattr $C$DW$1009, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1010	.dwtag  DW_TAG_member
	.dwattr $C$DW$1010, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1010, DW_AT_name("rsvd8_4")
	.dwattr $C$DW$1010, DW_AT_TI_symbol_name("_rsvd8_4")
	.dwattr $C$DW$1010, DW_AT_data_member_location[DW_OP_plus_uconst 0xb6]
	.dwattr $C$DW$1010, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1011	.dwtag  DW_TAG_member
	.dwattr $C$DW$1011, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1011, DW_AT_name("rsvd8_5")
	.dwattr $C$DW$1011, DW_AT_TI_symbol_name("_rsvd8_5")
	.dwattr $C$DW$1011, DW_AT_data_member_location[DW_OP_plus_uconst 0xb8]
	.dwattr $C$DW$1011, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1012	.dwtag  DW_TAG_member
	.dwattr $C$DW$1012, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1012, DW_AT_name("rsvd8_6")
	.dwattr $C$DW$1012, DW_AT_TI_symbol_name("_rsvd8_6")
	.dwattr $C$DW$1012, DW_AT_data_member_location[DW_OP_plus_uconst 0xba]
	.dwattr $C$DW$1012, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1013	.dwtag  DW_TAG_member
	.dwattr $C$DW$1013, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1013, DW_AT_name("rsvd8_7")
	.dwattr $C$DW$1013, DW_AT_TI_symbol_name("_rsvd8_7")
	.dwattr $C$DW$1013, DW_AT_data_member_location[DW_OP_plus_uconst 0xbc]
	.dwattr $C$DW$1013, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1014	.dwtag  DW_TAG_member
	.dwattr $C$DW$1014, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1014, DW_AT_name("rsvd8_8")
	.dwattr $C$DW$1014, DW_AT_TI_symbol_name("_rsvd8_8")
	.dwattr $C$DW$1014, DW_AT_data_member_location[DW_OP_plus_uconst 0xbe]
	.dwattr $C$DW$1014, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1015	.dwtag  DW_TAG_member
	.dwattr $C$DW$1015, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1015, DW_AT_name("SCIRXINTA")
	.dwattr $C$DW$1015, DW_AT_TI_symbol_name("_SCIRXINTA")
	.dwattr $C$DW$1015, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$1015, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1016	.dwtag  DW_TAG_member
	.dwattr $C$DW$1016, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1016, DW_AT_name("SCITXINTA")
	.dwattr $C$DW$1016, DW_AT_TI_symbol_name("_SCITXINTA")
	.dwattr $C$DW$1016, DW_AT_data_member_location[DW_OP_plus_uconst 0xc2]
	.dwattr $C$DW$1016, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1017	.dwtag  DW_TAG_member
	.dwattr $C$DW$1017, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1017, DW_AT_name("SCIRXINTB")
	.dwattr $C$DW$1017, DW_AT_TI_symbol_name("_SCIRXINTB")
	.dwattr $C$DW$1017, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$1017, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1018	.dwtag  DW_TAG_member
	.dwattr $C$DW$1018, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1018, DW_AT_name("SCITXINTB")
	.dwattr $C$DW$1018, DW_AT_TI_symbol_name("_SCITXINTB")
	.dwattr $C$DW$1018, DW_AT_data_member_location[DW_OP_plus_uconst 0xc6]
	.dwattr $C$DW$1018, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1019	.dwtag  DW_TAG_member
	.dwattr $C$DW$1019, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1019, DW_AT_name("ECAN0INTA")
	.dwattr $C$DW$1019, DW_AT_TI_symbol_name("_ECAN0INTA")
	.dwattr $C$DW$1019, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$1019, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1020	.dwtag  DW_TAG_member
	.dwattr $C$DW$1020, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1020, DW_AT_name("ECAN1INTA")
	.dwattr $C$DW$1020, DW_AT_TI_symbol_name("_ECAN1INTA")
	.dwattr $C$DW$1020, DW_AT_data_member_location[DW_OP_plus_uconst 0xca]
	.dwattr $C$DW$1020, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1021	.dwtag  DW_TAG_member
	.dwattr $C$DW$1021, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1021, DW_AT_name("rsvd9_7")
	.dwattr $C$DW$1021, DW_AT_TI_symbol_name("_rsvd9_7")
	.dwattr $C$DW$1021, DW_AT_data_member_location[DW_OP_plus_uconst 0xcc]
	.dwattr $C$DW$1021, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1022	.dwtag  DW_TAG_member
	.dwattr $C$DW$1022, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1022, DW_AT_name("rsvd9_8")
	.dwattr $C$DW$1022, DW_AT_TI_symbol_name("_rsvd9_8")
	.dwattr $C$DW$1022, DW_AT_data_member_location[DW_OP_plus_uconst 0xce]
	.dwattr $C$DW$1022, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1023	.dwtag  DW_TAG_member
	.dwattr $C$DW$1023, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1023, DW_AT_name("rsvd10_1")
	.dwattr $C$DW$1023, DW_AT_TI_symbol_name("_rsvd10_1")
	.dwattr $C$DW$1023, DW_AT_data_member_location[DW_OP_plus_uconst 0xd0]
	.dwattr $C$DW$1023, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1024	.dwtag  DW_TAG_member
	.dwattr $C$DW$1024, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1024, DW_AT_name("rsvd10_2")
	.dwattr $C$DW$1024, DW_AT_TI_symbol_name("_rsvd10_2")
	.dwattr $C$DW$1024, DW_AT_data_member_location[DW_OP_plus_uconst 0xd2]
	.dwattr $C$DW$1024, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1025	.dwtag  DW_TAG_member
	.dwattr $C$DW$1025, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1025, DW_AT_name("ADCINT3")
	.dwattr $C$DW$1025, DW_AT_TI_symbol_name("_ADCINT3")
	.dwattr $C$DW$1025, DW_AT_data_member_location[DW_OP_plus_uconst 0xd4]
	.dwattr $C$DW$1025, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1026	.dwtag  DW_TAG_member
	.dwattr $C$DW$1026, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1026, DW_AT_name("ADCINT4")
	.dwattr $C$DW$1026, DW_AT_TI_symbol_name("_ADCINT4")
	.dwattr $C$DW$1026, DW_AT_data_member_location[DW_OP_plus_uconst 0xd6]
	.dwattr $C$DW$1026, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1027	.dwtag  DW_TAG_member
	.dwattr $C$DW$1027, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1027, DW_AT_name("ADCINT5")
	.dwattr $C$DW$1027, DW_AT_TI_symbol_name("_ADCINT5")
	.dwattr $C$DW$1027, DW_AT_data_member_location[DW_OP_plus_uconst 0xd8]
	.dwattr $C$DW$1027, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1028	.dwtag  DW_TAG_member
	.dwattr $C$DW$1028, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1028, DW_AT_name("ADCINT6")
	.dwattr $C$DW$1028, DW_AT_TI_symbol_name("_ADCINT6")
	.dwattr $C$DW$1028, DW_AT_data_member_location[DW_OP_plus_uconst 0xda]
	.dwattr $C$DW$1028, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1029	.dwtag  DW_TAG_member
	.dwattr $C$DW$1029, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1029, DW_AT_name("ADCINT7")
	.dwattr $C$DW$1029, DW_AT_TI_symbol_name("_ADCINT7")
	.dwattr $C$DW$1029, DW_AT_data_member_location[DW_OP_plus_uconst 0xdc]
	.dwattr $C$DW$1029, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1030	.dwtag  DW_TAG_member
	.dwattr $C$DW$1030, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1030, DW_AT_name("ADCINT8")
	.dwattr $C$DW$1030, DW_AT_TI_symbol_name("_ADCINT8")
	.dwattr $C$DW$1030, DW_AT_data_member_location[DW_OP_plus_uconst 0xde]
	.dwattr $C$DW$1030, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1031	.dwtag  DW_TAG_member
	.dwattr $C$DW$1031, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1031, DW_AT_name("CLA1_INT1")
	.dwattr $C$DW$1031, DW_AT_TI_symbol_name("_CLA1_INT1")
	.dwattr $C$DW$1031, DW_AT_data_member_location[DW_OP_plus_uconst 0xe0]
	.dwattr $C$DW$1031, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1032	.dwtag  DW_TAG_member
	.dwattr $C$DW$1032, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1032, DW_AT_name("CLA1_INT2")
	.dwattr $C$DW$1032, DW_AT_TI_symbol_name("_CLA1_INT2")
	.dwattr $C$DW$1032, DW_AT_data_member_location[DW_OP_plus_uconst 0xe2]
	.dwattr $C$DW$1032, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1033	.dwtag  DW_TAG_member
	.dwattr $C$DW$1033, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1033, DW_AT_name("CLA1_INT3")
	.dwattr $C$DW$1033, DW_AT_TI_symbol_name("_CLA1_INT3")
	.dwattr $C$DW$1033, DW_AT_data_member_location[DW_OP_plus_uconst 0xe4]
	.dwattr $C$DW$1033, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1034	.dwtag  DW_TAG_member
	.dwattr $C$DW$1034, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1034, DW_AT_name("CLA1_INT4")
	.dwattr $C$DW$1034, DW_AT_TI_symbol_name("_CLA1_INT4")
	.dwattr $C$DW$1034, DW_AT_data_member_location[DW_OP_plus_uconst 0xe6]
	.dwattr $C$DW$1034, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1035	.dwtag  DW_TAG_member
	.dwattr $C$DW$1035, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1035, DW_AT_name("CLA1_INT5")
	.dwattr $C$DW$1035, DW_AT_TI_symbol_name("_CLA1_INT5")
	.dwattr $C$DW$1035, DW_AT_data_member_location[DW_OP_plus_uconst 0xe8]
	.dwattr $C$DW$1035, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1036	.dwtag  DW_TAG_member
	.dwattr $C$DW$1036, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1036, DW_AT_name("CLA1_INT6")
	.dwattr $C$DW$1036, DW_AT_TI_symbol_name("_CLA1_INT6")
	.dwattr $C$DW$1036, DW_AT_data_member_location[DW_OP_plus_uconst 0xea]
	.dwattr $C$DW$1036, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1037	.dwtag  DW_TAG_member
	.dwattr $C$DW$1037, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1037, DW_AT_name("CLA1_INT7")
	.dwattr $C$DW$1037, DW_AT_TI_symbol_name("_CLA1_INT7")
	.dwattr $C$DW$1037, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$1037, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1038	.dwtag  DW_TAG_member
	.dwattr $C$DW$1038, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1038, DW_AT_name("CLA1_INT8")
	.dwattr $C$DW$1038, DW_AT_TI_symbol_name("_CLA1_INT8")
	.dwattr $C$DW$1038, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$1038, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1039	.dwtag  DW_TAG_member
	.dwattr $C$DW$1039, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1039, DW_AT_name("XINT3")
	.dwattr $C$DW$1039, DW_AT_TI_symbol_name("_XINT3")
	.dwattr $C$DW$1039, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$1039, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1040	.dwtag  DW_TAG_member
	.dwattr $C$DW$1040, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1040, DW_AT_name("rsvd12_2")
	.dwattr $C$DW$1040, DW_AT_TI_symbol_name("_rsvd12_2")
	.dwattr $C$DW$1040, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$1040, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1041	.dwtag  DW_TAG_member
	.dwattr $C$DW$1041, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1041, DW_AT_name("rsvd12_3")
	.dwattr $C$DW$1041, DW_AT_TI_symbol_name("_rsvd12_3")
	.dwattr $C$DW$1041, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$1041, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1042	.dwtag  DW_TAG_member
	.dwattr $C$DW$1042, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1042, DW_AT_name("rsvd12_4")
	.dwattr $C$DW$1042, DW_AT_TI_symbol_name("_rsvd12_4")
	.dwattr $C$DW$1042, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$1042, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1043	.dwtag  DW_TAG_member
	.dwattr $C$DW$1043, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1043, DW_AT_name("rsvd12_5")
	.dwattr $C$DW$1043, DW_AT_TI_symbol_name("_rsvd12_5")
	.dwattr $C$DW$1043, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$1043, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1044	.dwtag  DW_TAG_member
	.dwattr $C$DW$1044, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1044, DW_AT_name("rsvd12_6")
	.dwattr $C$DW$1044, DW_AT_TI_symbol_name("_rsvd12_6")
	.dwattr $C$DW$1044, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$1044, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1045	.dwtag  DW_TAG_member
	.dwattr $C$DW$1045, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1045, DW_AT_name("LVF")
	.dwattr $C$DW$1045, DW_AT_TI_symbol_name("_LVF")
	.dwattr $C$DW$1045, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$1045, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1046	.dwtag  DW_TAG_member
	.dwattr $C$DW$1046, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1046, DW_AT_name("LUF")
	.dwattr $C$DW$1046, DW_AT_TI_symbol_name("_LUF")
	.dwattr $C$DW$1046, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$1046, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$142


$C$DW$T$143	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$143, DW_AT_name("QCAPCTL_BITS")
	.dwattr $C$DW$T$143, DW_AT_byte_size(0x01)
$C$DW$1047	.dwtag  DW_TAG_member
	.dwattr $C$DW$1047, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1047, DW_AT_name("UPPS")
	.dwattr $C$DW$1047, DW_AT_TI_symbol_name("_UPPS")
	.dwattr $C$DW$1047, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x04)
	.dwattr $C$DW$1047, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1047, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1048	.dwtag  DW_TAG_member
	.dwattr $C$DW$1048, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1048, DW_AT_name("CCPS")
	.dwattr $C$DW$1048, DW_AT_TI_symbol_name("_CCPS")
	.dwattr $C$DW$1048, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$1048, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1048, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1049	.dwtag  DW_TAG_member
	.dwattr $C$DW$1049, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1049, DW_AT_name("rsvd1")
	.dwattr $C$DW$1049, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1049, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x08)
	.dwattr $C$DW$1049, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1049, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1050	.dwtag  DW_TAG_member
	.dwattr $C$DW$1050, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1050, DW_AT_name("CEN")
	.dwattr $C$DW$1050, DW_AT_TI_symbol_name("_CEN")
	.dwattr $C$DW$1050, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1050, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1050, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$143


$C$DW$T$144	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$144, DW_AT_name("QCAPCTL_REG")
	.dwattr $C$DW$T$144, DW_AT_byte_size(0x01)
$C$DW$1051	.dwtag  DW_TAG_member
	.dwattr $C$DW$1051, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1051, DW_AT_name("all")
	.dwattr $C$DW$1051, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1051, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1051, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1052	.dwtag  DW_TAG_member
	.dwattr $C$DW$1052, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$1052, DW_AT_name("bit")
	.dwattr $C$DW$1052, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1052, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1052, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$144


$C$DW$T$145	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$145, DW_AT_name("QDECCTL_BITS")
	.dwattr $C$DW$T$145, DW_AT_byte_size(0x01)
$C$DW$1053	.dwtag  DW_TAG_member
	.dwattr $C$DW$1053, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1053, DW_AT_name("rsvd1")
	.dwattr $C$DW$1053, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1053, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$1053, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1053, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1054	.dwtag  DW_TAG_member
	.dwattr $C$DW$1054, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1054, DW_AT_name("QSP")
	.dwattr $C$DW$1054, DW_AT_TI_symbol_name("_QSP")
	.dwattr $C$DW$1054, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1054, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1054, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1055	.dwtag  DW_TAG_member
	.dwattr $C$DW$1055, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1055, DW_AT_name("QIP")
	.dwattr $C$DW$1055, DW_AT_TI_symbol_name("_QIP")
	.dwattr $C$DW$1055, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1055, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1055, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1056	.dwtag  DW_TAG_member
	.dwattr $C$DW$1056, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1056, DW_AT_name("QBP")
	.dwattr $C$DW$1056, DW_AT_TI_symbol_name("_QBP")
	.dwattr $C$DW$1056, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1056, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1056, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1057	.dwtag  DW_TAG_member
	.dwattr $C$DW$1057, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1057, DW_AT_name("QAP")
	.dwattr $C$DW$1057, DW_AT_TI_symbol_name("_QAP")
	.dwattr $C$DW$1057, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1057, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1057, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1058	.dwtag  DW_TAG_member
	.dwattr $C$DW$1058, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1058, DW_AT_name("IGATE")
	.dwattr $C$DW$1058, DW_AT_TI_symbol_name("_IGATE")
	.dwattr $C$DW$1058, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1058, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1058, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1059	.dwtag  DW_TAG_member
	.dwattr $C$DW$1059, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1059, DW_AT_name("SWAP")
	.dwattr $C$DW$1059, DW_AT_TI_symbol_name("_SWAP")
	.dwattr $C$DW$1059, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1059, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1059, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1060	.dwtag  DW_TAG_member
	.dwattr $C$DW$1060, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1060, DW_AT_name("XCR")
	.dwattr $C$DW$1060, DW_AT_TI_symbol_name("_XCR")
	.dwattr $C$DW$1060, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1060, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1060, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1061	.dwtag  DW_TAG_member
	.dwattr $C$DW$1061, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1061, DW_AT_name("SPSEL")
	.dwattr $C$DW$1061, DW_AT_TI_symbol_name("_SPSEL")
	.dwattr $C$DW$1061, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1061, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1061, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1062	.dwtag  DW_TAG_member
	.dwattr $C$DW$1062, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1062, DW_AT_name("SOEN")
	.dwattr $C$DW$1062, DW_AT_TI_symbol_name("_SOEN")
	.dwattr $C$DW$1062, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1062, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1062, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1063	.dwtag  DW_TAG_member
	.dwattr $C$DW$1063, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1063, DW_AT_name("QSRC")
	.dwattr $C$DW$1063, DW_AT_TI_symbol_name("_QSRC")
	.dwattr $C$DW$1063, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1063, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1063, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$145


$C$DW$T$146	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$146, DW_AT_name("QDECCTL_REG")
	.dwattr $C$DW$T$146, DW_AT_byte_size(0x01)
$C$DW$1064	.dwtag  DW_TAG_member
	.dwattr $C$DW$1064, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1064, DW_AT_name("all")
	.dwattr $C$DW$1064, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1064, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1064, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1065	.dwtag  DW_TAG_member
	.dwattr $C$DW$1065, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$1065, DW_AT_name("bit")
	.dwattr $C$DW$1065, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1065, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1065, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$146


$C$DW$T$147	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$147, DW_AT_name("QEINT_BITS")
	.dwattr $C$DW$T$147, DW_AT_byte_size(0x01)
$C$DW$1066	.dwtag  DW_TAG_member
	.dwattr $C$DW$1066, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1066, DW_AT_name("rsvd1")
	.dwattr $C$DW$1066, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1066, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1066, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1066, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1067	.dwtag  DW_TAG_member
	.dwattr $C$DW$1067, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1067, DW_AT_name("PCE")
	.dwattr $C$DW$1067, DW_AT_TI_symbol_name("_PCE")
	.dwattr $C$DW$1067, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1067, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1067, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1068	.dwtag  DW_TAG_member
	.dwattr $C$DW$1068, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1068, DW_AT_name("QPE")
	.dwattr $C$DW$1068, DW_AT_TI_symbol_name("_QPE")
	.dwattr $C$DW$1068, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1068, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1068, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1069	.dwtag  DW_TAG_member
	.dwattr $C$DW$1069, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1069, DW_AT_name("QDC")
	.dwattr $C$DW$1069, DW_AT_TI_symbol_name("_QDC")
	.dwattr $C$DW$1069, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1069, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1069, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1070	.dwtag  DW_TAG_member
	.dwattr $C$DW$1070, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1070, DW_AT_name("WTO")
	.dwattr $C$DW$1070, DW_AT_TI_symbol_name("_WTO")
	.dwattr $C$DW$1070, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1070, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1070, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1071	.dwtag  DW_TAG_member
	.dwattr $C$DW$1071, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1071, DW_AT_name("PCU")
	.dwattr $C$DW$1071, DW_AT_TI_symbol_name("_PCU")
	.dwattr $C$DW$1071, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1071, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1071, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1072	.dwtag  DW_TAG_member
	.dwattr $C$DW$1072, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1072, DW_AT_name("PCO")
	.dwattr $C$DW$1072, DW_AT_TI_symbol_name("_PCO")
	.dwattr $C$DW$1072, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1072, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1072, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1073	.dwtag  DW_TAG_member
	.dwattr $C$DW$1073, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1073, DW_AT_name("PCR")
	.dwattr $C$DW$1073, DW_AT_TI_symbol_name("_PCR")
	.dwattr $C$DW$1073, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1073, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1073, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1074	.dwtag  DW_TAG_member
	.dwattr $C$DW$1074, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1074, DW_AT_name("PCM")
	.dwattr $C$DW$1074, DW_AT_TI_symbol_name("_PCM")
	.dwattr $C$DW$1074, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1074, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1074, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1075	.dwtag  DW_TAG_member
	.dwattr $C$DW$1075, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1075, DW_AT_name("SEL")
	.dwattr $C$DW$1075, DW_AT_TI_symbol_name("_SEL")
	.dwattr $C$DW$1075, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1075, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1075, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1076	.dwtag  DW_TAG_member
	.dwattr $C$DW$1076, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1076, DW_AT_name("IEL")
	.dwattr $C$DW$1076, DW_AT_TI_symbol_name("_IEL")
	.dwattr $C$DW$1076, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1076, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1076, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1077	.dwtag  DW_TAG_member
	.dwattr $C$DW$1077, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1077, DW_AT_name("UTO")
	.dwattr $C$DW$1077, DW_AT_TI_symbol_name("_UTO")
	.dwattr $C$DW$1077, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1077, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1077, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1078	.dwtag  DW_TAG_member
	.dwattr $C$DW$1078, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1078, DW_AT_name("rsvd2")
	.dwattr $C$DW$1078, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$1078, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$1078, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1078, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$147


$C$DW$T$148	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$148, DW_AT_name("QEINT_REG")
	.dwattr $C$DW$T$148, DW_AT_byte_size(0x01)
$C$DW$1079	.dwtag  DW_TAG_member
	.dwattr $C$DW$1079, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1079, DW_AT_name("all")
	.dwattr $C$DW$1079, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1079, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1079, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1080	.dwtag  DW_TAG_member
	.dwattr $C$DW$1080, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$1080, DW_AT_name("bit")
	.dwattr $C$DW$1080, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1080, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1080, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$148


$C$DW$T$149	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$149, DW_AT_name("QEPCTL_BITS")
	.dwattr $C$DW$T$149, DW_AT_byte_size(0x01)
$C$DW$1081	.dwtag  DW_TAG_member
	.dwattr $C$DW$1081, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1081, DW_AT_name("WDE")
	.dwattr $C$DW$1081, DW_AT_TI_symbol_name("_WDE")
	.dwattr $C$DW$1081, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1081, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1081, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1082	.dwtag  DW_TAG_member
	.dwattr $C$DW$1082, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1082, DW_AT_name("UTE")
	.dwattr $C$DW$1082, DW_AT_TI_symbol_name("_UTE")
	.dwattr $C$DW$1082, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1082, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1082, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1083	.dwtag  DW_TAG_member
	.dwattr $C$DW$1083, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1083, DW_AT_name("QCLM")
	.dwattr $C$DW$1083, DW_AT_TI_symbol_name("_QCLM")
	.dwattr $C$DW$1083, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1083, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1083, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1084	.dwtag  DW_TAG_member
	.dwattr $C$DW$1084, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1084, DW_AT_name("QPEN")
	.dwattr $C$DW$1084, DW_AT_TI_symbol_name("_QPEN")
	.dwattr $C$DW$1084, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1084, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1084, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1085	.dwtag  DW_TAG_member
	.dwattr $C$DW$1085, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1085, DW_AT_name("IEL")
	.dwattr $C$DW$1085, DW_AT_TI_symbol_name("_IEL")
	.dwattr $C$DW$1085, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1085, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1085, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1086	.dwtag  DW_TAG_member
	.dwattr $C$DW$1086, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1086, DW_AT_name("SEL")
	.dwattr $C$DW$1086, DW_AT_TI_symbol_name("_SEL")
	.dwattr $C$DW$1086, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1086, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1086, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1087	.dwtag  DW_TAG_member
	.dwattr $C$DW$1087, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1087, DW_AT_name("SWI")
	.dwattr $C$DW$1087, DW_AT_TI_symbol_name("_SWI")
	.dwattr $C$DW$1087, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1087, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1087, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1088	.dwtag  DW_TAG_member
	.dwattr $C$DW$1088, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1088, DW_AT_name("IEI")
	.dwattr $C$DW$1088, DW_AT_TI_symbol_name("_IEI")
	.dwattr $C$DW$1088, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1088, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1088, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1089	.dwtag  DW_TAG_member
	.dwattr $C$DW$1089, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1089, DW_AT_name("SEI")
	.dwattr $C$DW$1089, DW_AT_TI_symbol_name("_SEI")
	.dwattr $C$DW$1089, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1089, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1089, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1090	.dwtag  DW_TAG_member
	.dwattr $C$DW$1090, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1090, DW_AT_name("PCRM")
	.dwattr $C$DW$1090, DW_AT_TI_symbol_name("_PCRM")
	.dwattr $C$DW$1090, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1090, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1090, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1091	.dwtag  DW_TAG_member
	.dwattr $C$DW$1091, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1091, DW_AT_name("FREE_SOFT")
	.dwattr $C$DW$1091, DW_AT_TI_symbol_name("_FREE_SOFT")
	.dwattr $C$DW$1091, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1091, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1091, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$149


$C$DW$T$150	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$150, DW_AT_name("QEPCTL_REG")
	.dwattr $C$DW$T$150, DW_AT_byte_size(0x01)
$C$DW$1092	.dwtag  DW_TAG_member
	.dwattr $C$DW$1092, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1092, DW_AT_name("all")
	.dwattr $C$DW$1092, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1092, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1092, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1093	.dwtag  DW_TAG_member
	.dwattr $C$DW$1093, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$1093, DW_AT_name("bit")
	.dwattr $C$DW$1093, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1093, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1093, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$150


$C$DW$T$151	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$151, DW_AT_name("QEPSTS_BITS")
	.dwattr $C$DW$T$151, DW_AT_byte_size(0x01)
$C$DW$1094	.dwtag  DW_TAG_member
	.dwattr $C$DW$1094, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1094, DW_AT_name("PCEF")
	.dwattr $C$DW$1094, DW_AT_TI_symbol_name("_PCEF")
	.dwattr $C$DW$1094, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1094, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1094, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1095	.dwtag  DW_TAG_member
	.dwattr $C$DW$1095, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1095, DW_AT_name("FIMF")
	.dwattr $C$DW$1095, DW_AT_TI_symbol_name("_FIMF")
	.dwattr $C$DW$1095, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1095, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1095, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1096	.dwtag  DW_TAG_member
	.dwattr $C$DW$1096, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1096, DW_AT_name("CDEF")
	.dwattr $C$DW$1096, DW_AT_TI_symbol_name("_CDEF")
	.dwattr $C$DW$1096, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1096, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1096, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1097	.dwtag  DW_TAG_member
	.dwattr $C$DW$1097, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1097, DW_AT_name("COEF")
	.dwattr $C$DW$1097, DW_AT_TI_symbol_name("_COEF")
	.dwattr $C$DW$1097, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1097, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1097, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1098	.dwtag  DW_TAG_member
	.dwattr $C$DW$1098, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1098, DW_AT_name("QDLF")
	.dwattr $C$DW$1098, DW_AT_TI_symbol_name("_QDLF")
	.dwattr $C$DW$1098, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1098, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1098, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1099	.dwtag  DW_TAG_member
	.dwattr $C$DW$1099, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1099, DW_AT_name("QDF")
	.dwattr $C$DW$1099, DW_AT_TI_symbol_name("_QDF")
	.dwattr $C$DW$1099, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1099, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1099, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1100	.dwtag  DW_TAG_member
	.dwattr $C$DW$1100, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1100, DW_AT_name("FIDF")
	.dwattr $C$DW$1100, DW_AT_TI_symbol_name("_FIDF")
	.dwattr $C$DW$1100, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1100, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1100, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1101	.dwtag  DW_TAG_member
	.dwattr $C$DW$1101, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1101, DW_AT_name("UPEVNT")
	.dwattr $C$DW$1101, DW_AT_TI_symbol_name("_UPEVNT")
	.dwattr $C$DW$1101, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1101, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1101, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1102	.dwtag  DW_TAG_member
	.dwattr $C$DW$1102, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1102, DW_AT_name("rsvd1")
	.dwattr $C$DW$1102, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1102, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$1102, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1102, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$151


$C$DW$T$152	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$152, DW_AT_name("QEPSTS_REG")
	.dwattr $C$DW$T$152, DW_AT_byte_size(0x01)
$C$DW$1103	.dwtag  DW_TAG_member
	.dwattr $C$DW$1103, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1103, DW_AT_name("all")
	.dwattr $C$DW$1103, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1103, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1103, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1104	.dwtag  DW_TAG_member
	.dwattr $C$DW$1104, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$1104, DW_AT_name("bit")
	.dwattr $C$DW$1104, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1104, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1104, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$152


$C$DW$T$153	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$153, DW_AT_name("QFLG_BITS")
	.dwattr $C$DW$T$153, DW_AT_byte_size(0x01)
$C$DW$1105	.dwtag  DW_TAG_member
	.dwattr $C$DW$1105, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1105, DW_AT_name("INT")
	.dwattr $C$DW$1105, DW_AT_TI_symbol_name("_INT")
	.dwattr $C$DW$1105, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1105, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1105, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1106	.dwtag  DW_TAG_member
	.dwattr $C$DW$1106, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1106, DW_AT_name("PCE")
	.dwattr $C$DW$1106, DW_AT_TI_symbol_name("_PCE")
	.dwattr $C$DW$1106, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1106, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1106, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1107	.dwtag  DW_TAG_member
	.dwattr $C$DW$1107, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1107, DW_AT_name("PHE")
	.dwattr $C$DW$1107, DW_AT_TI_symbol_name("_PHE")
	.dwattr $C$DW$1107, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1107, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1107, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1108	.dwtag  DW_TAG_member
	.dwattr $C$DW$1108, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1108, DW_AT_name("QDC")
	.dwattr $C$DW$1108, DW_AT_TI_symbol_name("_QDC")
	.dwattr $C$DW$1108, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1108, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1108, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1109	.dwtag  DW_TAG_member
	.dwattr $C$DW$1109, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1109, DW_AT_name("WTO")
	.dwattr $C$DW$1109, DW_AT_TI_symbol_name("_WTO")
	.dwattr $C$DW$1109, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1109, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1109, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1110	.dwtag  DW_TAG_member
	.dwattr $C$DW$1110, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1110, DW_AT_name("PCU")
	.dwattr $C$DW$1110, DW_AT_TI_symbol_name("_PCU")
	.dwattr $C$DW$1110, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1110, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1110, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1111	.dwtag  DW_TAG_member
	.dwattr $C$DW$1111, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1111, DW_AT_name("PCO")
	.dwattr $C$DW$1111, DW_AT_TI_symbol_name("_PCO")
	.dwattr $C$DW$1111, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1111, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1111, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1112	.dwtag  DW_TAG_member
	.dwattr $C$DW$1112, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1112, DW_AT_name("PCR")
	.dwattr $C$DW$1112, DW_AT_TI_symbol_name("_PCR")
	.dwattr $C$DW$1112, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1112, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1112, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1113	.dwtag  DW_TAG_member
	.dwattr $C$DW$1113, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1113, DW_AT_name("PCM")
	.dwattr $C$DW$1113, DW_AT_TI_symbol_name("_PCM")
	.dwattr $C$DW$1113, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1113, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1113, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1114	.dwtag  DW_TAG_member
	.dwattr $C$DW$1114, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1114, DW_AT_name("SEL")
	.dwattr $C$DW$1114, DW_AT_TI_symbol_name("_SEL")
	.dwattr $C$DW$1114, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1114, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1114, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1115	.dwtag  DW_TAG_member
	.dwattr $C$DW$1115, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1115, DW_AT_name("IEL")
	.dwattr $C$DW$1115, DW_AT_TI_symbol_name("_IEL")
	.dwattr $C$DW$1115, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1115, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1115, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1116	.dwtag  DW_TAG_member
	.dwattr $C$DW$1116, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1116, DW_AT_name("UTO")
	.dwattr $C$DW$1116, DW_AT_TI_symbol_name("_UTO")
	.dwattr $C$DW$1116, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1116, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1116, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1117	.dwtag  DW_TAG_member
	.dwattr $C$DW$1117, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1117, DW_AT_name("rsvd1")
	.dwattr $C$DW$1117, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1117, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$1117, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1117, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$153


$C$DW$T$154	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$154, DW_AT_name("QFLG_REG")
	.dwattr $C$DW$T$154, DW_AT_byte_size(0x01)
$C$DW$1118	.dwtag  DW_TAG_member
	.dwattr $C$DW$1118, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1118, DW_AT_name("all")
	.dwattr $C$DW$1118, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1118, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1118, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1119	.dwtag  DW_TAG_member
	.dwattr $C$DW$1119, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$1119, DW_AT_name("bit")
	.dwattr $C$DW$1119, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1119, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1119, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$154


$C$DW$T$155	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$155, DW_AT_name("QFRC_BITS")
	.dwattr $C$DW$T$155, DW_AT_byte_size(0x01)
$C$DW$1120	.dwtag  DW_TAG_member
	.dwattr $C$DW$1120, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1120, DW_AT_name("rsvd1")
	.dwattr $C$DW$1120, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1120, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1120, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1120, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1121	.dwtag  DW_TAG_member
	.dwattr $C$DW$1121, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1121, DW_AT_name("PCE")
	.dwattr $C$DW$1121, DW_AT_TI_symbol_name("_PCE")
	.dwattr $C$DW$1121, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1121, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1121, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1122	.dwtag  DW_TAG_member
	.dwattr $C$DW$1122, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1122, DW_AT_name("PHE")
	.dwattr $C$DW$1122, DW_AT_TI_symbol_name("_PHE")
	.dwattr $C$DW$1122, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1122, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1123	.dwtag  DW_TAG_member
	.dwattr $C$DW$1123, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1123, DW_AT_name("QDC")
	.dwattr $C$DW$1123, DW_AT_TI_symbol_name("_QDC")
	.dwattr $C$DW$1123, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1123, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1124	.dwtag  DW_TAG_member
	.dwattr $C$DW$1124, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1124, DW_AT_name("WTO")
	.dwattr $C$DW$1124, DW_AT_TI_symbol_name("_WTO")
	.dwattr $C$DW$1124, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1124, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1125	.dwtag  DW_TAG_member
	.dwattr $C$DW$1125, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1125, DW_AT_name("PCU")
	.dwattr $C$DW$1125, DW_AT_TI_symbol_name("_PCU")
	.dwattr $C$DW$1125, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1125, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1125, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1126	.dwtag  DW_TAG_member
	.dwattr $C$DW$1126, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1126, DW_AT_name("PCO")
	.dwattr $C$DW$1126, DW_AT_TI_symbol_name("_PCO")
	.dwattr $C$DW$1126, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1126, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1127	.dwtag  DW_TAG_member
	.dwattr $C$DW$1127, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1127, DW_AT_name("PCR")
	.dwattr $C$DW$1127, DW_AT_TI_symbol_name("_PCR")
	.dwattr $C$DW$1127, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1127, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1127, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1128	.dwtag  DW_TAG_member
	.dwattr $C$DW$1128, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1128, DW_AT_name("PCM")
	.dwattr $C$DW$1128, DW_AT_TI_symbol_name("_PCM")
	.dwattr $C$DW$1128, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1128, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1128, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1129	.dwtag  DW_TAG_member
	.dwattr $C$DW$1129, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1129, DW_AT_name("SEL")
	.dwattr $C$DW$1129, DW_AT_TI_symbol_name("_SEL")
	.dwattr $C$DW$1129, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1129, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1129, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1130	.dwtag  DW_TAG_member
	.dwattr $C$DW$1130, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1130, DW_AT_name("IEL")
	.dwattr $C$DW$1130, DW_AT_TI_symbol_name("_IEL")
	.dwattr $C$DW$1130, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1130, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1130, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1131	.dwtag  DW_TAG_member
	.dwattr $C$DW$1131, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1131, DW_AT_name("UTO")
	.dwattr $C$DW$1131, DW_AT_TI_symbol_name("_UTO")
	.dwattr $C$DW$1131, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1131, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1131, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1132	.dwtag  DW_TAG_member
	.dwattr $C$DW$1132, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1132, DW_AT_name("rsvd2")
	.dwattr $C$DW$1132, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$1132, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$1132, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1132, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$155


$C$DW$T$156	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$156, DW_AT_name("QFRC_REG")
	.dwattr $C$DW$T$156, DW_AT_byte_size(0x01)
$C$DW$1133	.dwtag  DW_TAG_member
	.dwattr $C$DW$1133, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1133, DW_AT_name("all")
	.dwattr $C$DW$1133, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1133, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1133, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1134	.dwtag  DW_TAG_member
	.dwattr $C$DW$1134, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$1134, DW_AT_name("bit")
	.dwattr $C$DW$1134, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1134, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1134, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$156


$C$DW$T$157	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$157, DW_AT_name("QPOSCTL_BITS")
	.dwattr $C$DW$T$157, DW_AT_byte_size(0x01)
$C$DW$1135	.dwtag  DW_TAG_member
	.dwattr $C$DW$1135, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1135, DW_AT_name("PCSPW")
	.dwattr $C$DW$1135, DW_AT_TI_symbol_name("_PCSPW")
	.dwattr $C$DW$1135, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x0c)
	.dwattr $C$DW$1135, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1136	.dwtag  DW_TAG_member
	.dwattr $C$DW$1136, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1136, DW_AT_name("PCE")
	.dwattr $C$DW$1136, DW_AT_TI_symbol_name("_PCE")
	.dwattr $C$DW$1136, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1136, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1137	.dwtag  DW_TAG_member
	.dwattr $C$DW$1137, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1137, DW_AT_name("PCPOL")
	.dwattr $C$DW$1137, DW_AT_TI_symbol_name("_PCPOL")
	.dwattr $C$DW$1137, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1137, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1137, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1138	.dwtag  DW_TAG_member
	.dwattr $C$DW$1138, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1138, DW_AT_name("PCLOAD")
	.dwattr $C$DW$1138, DW_AT_TI_symbol_name("_PCLOAD")
	.dwattr $C$DW$1138, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1138, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1138, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1139	.dwtag  DW_TAG_member
	.dwattr $C$DW$1139, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1139, DW_AT_name("PCSHDW")
	.dwattr $C$DW$1139, DW_AT_TI_symbol_name("_PCSHDW")
	.dwattr $C$DW$1139, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1139, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1139, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$157


$C$DW$T$158	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$158, DW_AT_name("QPOSCTL_REG")
	.dwattr $C$DW$T$158, DW_AT_byte_size(0x01)
$C$DW$1140	.dwtag  DW_TAG_member
	.dwattr $C$DW$1140, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1140, DW_AT_name("all")
	.dwattr $C$DW$1140, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1140, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1140, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1141	.dwtag  DW_TAG_member
	.dwattr $C$DW$1141, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$1141, DW_AT_name("bit")
	.dwattr $C$DW$1141, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1141, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1141, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$158


$C$DW$T$160	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$160, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$160, DW_AT_byte_size(0x04)
$C$DW$1142	.dwtag  DW_TAG_member
	.dwattr $C$DW$1142, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$1142, DW_AT_name("next")
	.dwattr $C$DW$1142, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$1142, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1142, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1143	.dwtag  DW_TAG_member
	.dwattr $C$DW$1143, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$1143, DW_AT_name("prev")
	.dwattr $C$DW$1143, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$1143, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1143, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$160

$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)
$C$DW$T$159	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$159, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$T$159, DW_AT_address_class(0x16)

$C$DW$T$162	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$162, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$162, DW_AT_byte_size(0x10)
$C$DW$1144	.dwtag  DW_TAG_member
	.dwattr $C$DW$1144, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$1144, DW_AT_name("job")
	.dwattr $C$DW$1144, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$1144, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1144, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1145	.dwtag  DW_TAG_member
	.dwattr $C$DW$1145, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$1145, DW_AT_name("count")
	.dwattr $C$DW$1145, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$1145, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1146	.dwtag  DW_TAG_member
	.dwattr $C$DW$1146, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$1146, DW_AT_name("pendQ")
	.dwattr $C$DW$1146, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$1146, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$1146, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1147	.dwtag  DW_TAG_member
	.dwattr $C$DW$1147, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$1147, DW_AT_name("name")
	.dwattr $C$DW$1147, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$1147, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$1147, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$162

$C$DW$T$121	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$T$121, DW_AT_language(DW_LANG_C)
$C$DW$T$296	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$296, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$T$296, DW_AT_address_class(0x16)
$C$DW$T$297	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$297, DW_AT_type(*$C$DW$T$296)
	.dwattr $C$DW$T$297, DW_AT_language(DW_LANG_C)

$C$DW$T$163	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$163, DW_AT_name("SOCPRICTL_BITS")
	.dwattr $C$DW$T$163, DW_AT_byte_size(0x01)
$C$DW$1148	.dwtag  DW_TAG_member
	.dwattr $C$DW$1148, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1148, DW_AT_name("SOCPRIORITY")
	.dwattr $C$DW$1148, DW_AT_TI_symbol_name("_SOCPRIORITY")
	.dwattr $C$DW$1148, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$1148, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1148, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1149	.dwtag  DW_TAG_member
	.dwattr $C$DW$1149, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1149, DW_AT_name("RRPOINTER")
	.dwattr $C$DW$1149, DW_AT_TI_symbol_name("_RRPOINTER")
	.dwattr $C$DW$1149, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x06)
	.dwattr $C$DW$1149, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1149, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1150	.dwtag  DW_TAG_member
	.dwattr $C$DW$1150, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1150, DW_AT_name("rsvd1")
	.dwattr $C$DW$1150, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1150, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x04)
	.dwattr $C$DW$1150, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1150, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1151	.dwtag  DW_TAG_member
	.dwattr $C$DW$1151, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1151, DW_AT_name("ONESHOT")
	.dwattr $C$DW$1151, DW_AT_TI_symbol_name("_ONESHOT")
	.dwattr $C$DW$1151, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1151, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1151, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$163


$C$DW$T$164	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$164, DW_AT_name("SOCPRICTL_REG")
	.dwattr $C$DW$T$164, DW_AT_byte_size(0x01)
$C$DW$1152	.dwtag  DW_TAG_member
	.dwattr $C$DW$1152, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1152, DW_AT_name("all")
	.dwattr $C$DW$1152, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1152, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1152, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1153	.dwtag  DW_TAG_member
	.dwattr $C$DW$1153, DW_AT_type(*$C$DW$T$163)
	.dwattr $C$DW$1153, DW_AT_name("bit")
	.dwattr $C$DW$1153, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1153, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1153, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$164


$C$DW$T$165	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$165, DW_AT_name("TBCTL_BITS")
	.dwattr $C$DW$T$165, DW_AT_byte_size(0x01)
$C$DW$1154	.dwtag  DW_TAG_member
	.dwattr $C$DW$1154, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1154, DW_AT_name("CTRMODE")
	.dwattr $C$DW$1154, DW_AT_TI_symbol_name("_CTRMODE")
	.dwattr $C$DW$1154, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1154, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1154, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1155	.dwtag  DW_TAG_member
	.dwattr $C$DW$1155, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1155, DW_AT_name("PHSEN")
	.dwattr $C$DW$1155, DW_AT_TI_symbol_name("_PHSEN")
	.dwattr $C$DW$1155, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1155, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1155, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1156	.dwtag  DW_TAG_member
	.dwattr $C$DW$1156, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1156, DW_AT_name("PRDLD")
	.dwattr $C$DW$1156, DW_AT_TI_symbol_name("_PRDLD")
	.dwattr $C$DW$1156, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1156, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1156, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1157	.dwtag  DW_TAG_member
	.dwattr $C$DW$1157, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1157, DW_AT_name("SYNCOSEL")
	.dwattr $C$DW$1157, DW_AT_TI_symbol_name("_SYNCOSEL")
	.dwattr $C$DW$1157, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1157, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1157, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1158	.dwtag  DW_TAG_member
	.dwattr $C$DW$1158, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1158, DW_AT_name("SWFSYNC")
	.dwattr $C$DW$1158, DW_AT_TI_symbol_name("_SWFSYNC")
	.dwattr $C$DW$1158, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1158, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1158, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1159	.dwtag  DW_TAG_member
	.dwattr $C$DW$1159, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1159, DW_AT_name("HSPCLKDIV")
	.dwattr $C$DW$1159, DW_AT_TI_symbol_name("_HSPCLKDIV")
	.dwattr $C$DW$1159, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x03)
	.dwattr $C$DW$1159, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1159, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1160	.dwtag  DW_TAG_member
	.dwattr $C$DW$1160, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1160, DW_AT_name("CLKDIV")
	.dwattr $C$DW$1160, DW_AT_TI_symbol_name("_CLKDIV")
	.dwattr $C$DW$1160, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x03)
	.dwattr $C$DW$1160, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1160, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1161	.dwtag  DW_TAG_member
	.dwattr $C$DW$1161, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1161, DW_AT_name("PHSDIR")
	.dwattr $C$DW$1161, DW_AT_TI_symbol_name("_PHSDIR")
	.dwattr $C$DW$1161, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1161, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1161, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1162	.dwtag  DW_TAG_member
	.dwattr $C$DW$1162, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1162, DW_AT_name("FREE_SOFT")
	.dwattr $C$DW$1162, DW_AT_TI_symbol_name("_FREE_SOFT")
	.dwattr $C$DW$1162, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1162, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1162, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$165


$C$DW$T$166	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$166, DW_AT_name("TBCTL_REG")
	.dwattr $C$DW$T$166, DW_AT_byte_size(0x01)
$C$DW$1163	.dwtag  DW_TAG_member
	.dwattr $C$DW$1163, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1163, DW_AT_name("all")
	.dwattr $C$DW$1163, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1163, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1163, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1164	.dwtag  DW_TAG_member
	.dwattr $C$DW$1164, DW_AT_type(*$C$DW$T$165)
	.dwattr $C$DW$1164, DW_AT_name("bit")
	.dwattr $C$DW$1164, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1164, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1164, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$166


$C$DW$T$167	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$167, DW_AT_name("TBPHS_HRPWM_GROUP")
	.dwattr $C$DW$T$167, DW_AT_byte_size(0x02)
$C$DW$1165	.dwtag  DW_TAG_member
	.dwattr $C$DW$1165, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$1165, DW_AT_name("all")
	.dwattr $C$DW$1165, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1165, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1165, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1166	.dwtag  DW_TAG_member
	.dwattr $C$DW$1166, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$1166, DW_AT_name("half")
	.dwattr $C$DW$1166, DW_AT_TI_symbol_name("_half")
	.dwattr $C$DW$1166, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1166, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$167


$C$DW$T$168	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$168, DW_AT_name("TBPHS_HRPWM_REG")
	.dwattr $C$DW$T$168, DW_AT_byte_size(0x02)
$C$DW$1167	.dwtag  DW_TAG_member
	.dwattr $C$DW$1167, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1167, DW_AT_name("TBPHSHR")
	.dwattr $C$DW$1167, DW_AT_TI_symbol_name("_TBPHSHR")
	.dwattr $C$DW$1167, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1167, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1168	.dwtag  DW_TAG_member
	.dwattr $C$DW$1168, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1168, DW_AT_name("TBPHS")
	.dwattr $C$DW$1168, DW_AT_TI_symbol_name("_TBPHS")
	.dwattr $C$DW$1168, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1168, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$168


$C$DW$T$169	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$169, DW_AT_name("TBPRD_HRPWM_GROUP")
	.dwattr $C$DW$T$169, DW_AT_byte_size(0x02)
$C$DW$1169	.dwtag  DW_TAG_member
	.dwattr $C$DW$1169, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$1169, DW_AT_name("all")
	.dwattr $C$DW$1169, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1169, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1169, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1170	.dwtag  DW_TAG_member
	.dwattr $C$DW$1170, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$1170, DW_AT_name("half")
	.dwattr $C$DW$1170, DW_AT_TI_symbol_name("_half")
	.dwattr $C$DW$1170, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1170, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$169


$C$DW$T$170	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$170, DW_AT_name("TBPRD_HRPWM_REG")
	.dwattr $C$DW$T$170, DW_AT_byte_size(0x02)
$C$DW$1171	.dwtag  DW_TAG_member
	.dwattr $C$DW$1171, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1171, DW_AT_name("TBPRDHR")
	.dwattr $C$DW$1171, DW_AT_TI_symbol_name("_TBPRDHR")
	.dwattr $C$DW$1171, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1171, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1172	.dwtag  DW_TAG_member
	.dwattr $C$DW$1172, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1172, DW_AT_name("TBPRD")
	.dwattr $C$DW$1172, DW_AT_TI_symbol_name("_TBPRD")
	.dwattr $C$DW$1172, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1172, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$170


$C$DW$T$171	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$171, DW_AT_name("TBSTS_BITS")
	.dwattr $C$DW$T$171, DW_AT_byte_size(0x01)
$C$DW$1173	.dwtag  DW_TAG_member
	.dwattr $C$DW$1173, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1173, DW_AT_name("CTRDIR")
	.dwattr $C$DW$1173, DW_AT_TI_symbol_name("_CTRDIR")
	.dwattr $C$DW$1173, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1173, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1173, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1174	.dwtag  DW_TAG_member
	.dwattr $C$DW$1174, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1174, DW_AT_name("SYNCI")
	.dwattr $C$DW$1174, DW_AT_TI_symbol_name("_SYNCI")
	.dwattr $C$DW$1174, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1174, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1174, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1175	.dwtag  DW_TAG_member
	.dwattr $C$DW$1175, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1175, DW_AT_name("CTRMAX")
	.dwattr $C$DW$1175, DW_AT_TI_symbol_name("_CTRMAX")
	.dwattr $C$DW$1175, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1175, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1175, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1176	.dwtag  DW_TAG_member
	.dwattr $C$DW$1176, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1176, DW_AT_name("rsvd1")
	.dwattr $C$DW$1176, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1176, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0d)
	.dwattr $C$DW$1176, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1176, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$171


$C$DW$T$172	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$172, DW_AT_name("TBSTS_REG")
	.dwattr $C$DW$T$172, DW_AT_byte_size(0x01)
$C$DW$1177	.dwtag  DW_TAG_member
	.dwattr $C$DW$1177, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1177, DW_AT_name("all")
	.dwattr $C$DW$1177, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1177, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1177, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1178	.dwtag  DW_TAG_member
	.dwattr $C$DW$1178, DW_AT_type(*$C$DW$T$171)
	.dwattr $C$DW$1178, DW_AT_name("bit")
	.dwattr $C$DW$1178, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1178, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1178, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$172


$C$DW$T$173	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$173, DW_AT_name("TZCLR_BITS")
	.dwattr $C$DW$T$173, DW_AT_byte_size(0x01)
$C$DW$1179	.dwtag  DW_TAG_member
	.dwattr $C$DW$1179, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1179, DW_AT_name("INT")
	.dwattr $C$DW$1179, DW_AT_TI_symbol_name("_INT")
	.dwattr $C$DW$1179, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1179, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1179, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1180	.dwtag  DW_TAG_member
	.dwattr $C$DW$1180, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1180, DW_AT_name("CBC")
	.dwattr $C$DW$1180, DW_AT_TI_symbol_name("_CBC")
	.dwattr $C$DW$1180, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1180, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1180, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1181	.dwtag  DW_TAG_member
	.dwattr $C$DW$1181, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1181, DW_AT_name("OST")
	.dwattr $C$DW$1181, DW_AT_TI_symbol_name("_OST")
	.dwattr $C$DW$1181, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1181, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1181, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1182	.dwtag  DW_TAG_member
	.dwattr $C$DW$1182, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1182, DW_AT_name("DCAEVT1")
	.dwattr $C$DW$1182, DW_AT_TI_symbol_name("_DCAEVT1")
	.dwattr $C$DW$1182, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1182, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1182, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1183	.dwtag  DW_TAG_member
	.dwattr $C$DW$1183, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1183, DW_AT_name("DCAEVT2")
	.dwattr $C$DW$1183, DW_AT_TI_symbol_name("_DCAEVT2")
	.dwattr $C$DW$1183, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1183, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1183, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1184	.dwtag  DW_TAG_member
	.dwattr $C$DW$1184, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1184, DW_AT_name("DCBEVT1")
	.dwattr $C$DW$1184, DW_AT_TI_symbol_name("_DCBEVT1")
	.dwattr $C$DW$1184, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1184, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1185	.dwtag  DW_TAG_member
	.dwattr $C$DW$1185, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1185, DW_AT_name("DCBEVT2")
	.dwattr $C$DW$1185, DW_AT_TI_symbol_name("_DCBEVT2")
	.dwattr $C$DW$1185, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1185, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1186	.dwtag  DW_TAG_member
	.dwattr $C$DW$1186, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1186, DW_AT_name("rsvd1")
	.dwattr $C$DW$1186, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1186, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x09)
	.dwattr $C$DW$1186, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1186, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$173


$C$DW$T$174	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$174, DW_AT_name("TZCLR_REG")
	.dwattr $C$DW$T$174, DW_AT_byte_size(0x01)
$C$DW$1187	.dwtag  DW_TAG_member
	.dwattr $C$DW$1187, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1187, DW_AT_name("all")
	.dwattr $C$DW$1187, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1187, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1188	.dwtag  DW_TAG_member
	.dwattr $C$DW$1188, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$1188, DW_AT_name("bit")
	.dwattr $C$DW$1188, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1188, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1188, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$174


$C$DW$T$175	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$175, DW_AT_name("TZCTL_BITS")
	.dwattr $C$DW$T$175, DW_AT_byte_size(0x01)
$C$DW$1189	.dwtag  DW_TAG_member
	.dwattr $C$DW$1189, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1189, DW_AT_name("TZA")
	.dwattr $C$DW$1189, DW_AT_TI_symbol_name("_TZA")
	.dwattr $C$DW$1189, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1189, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1190	.dwtag  DW_TAG_member
	.dwattr $C$DW$1190, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1190, DW_AT_name("TZB")
	.dwattr $C$DW$1190, DW_AT_TI_symbol_name("_TZB")
	.dwattr $C$DW$1190, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1190, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1190, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1191	.dwtag  DW_TAG_member
	.dwattr $C$DW$1191, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1191, DW_AT_name("DCAEVT1")
	.dwattr $C$DW$1191, DW_AT_TI_symbol_name("_DCAEVT1")
	.dwattr $C$DW$1191, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1191, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1192	.dwtag  DW_TAG_member
	.dwattr $C$DW$1192, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1192, DW_AT_name("DCAEVT2")
	.dwattr $C$DW$1192, DW_AT_TI_symbol_name("_DCAEVT2")
	.dwattr $C$DW$1192, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1192, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1192, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1193	.dwtag  DW_TAG_member
	.dwattr $C$DW$1193, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1193, DW_AT_name("DCBEVT1")
	.dwattr $C$DW$1193, DW_AT_TI_symbol_name("_DCBEVT1")
	.dwattr $C$DW$1193, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1193, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1193, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1194	.dwtag  DW_TAG_member
	.dwattr $C$DW$1194, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1194, DW_AT_name("DCBEVT2")
	.dwattr $C$DW$1194, DW_AT_TI_symbol_name("_DCBEVT2")
	.dwattr $C$DW$1194, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1194, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1194, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1195	.dwtag  DW_TAG_member
	.dwattr $C$DW$1195, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1195, DW_AT_name("rsvd1")
	.dwattr $C$DW$1195, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1195, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$1195, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1195, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$175


$C$DW$T$176	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$176, DW_AT_name("TZCTL_REG")
	.dwattr $C$DW$T$176, DW_AT_byte_size(0x01)
$C$DW$1196	.dwtag  DW_TAG_member
	.dwattr $C$DW$1196, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1196, DW_AT_name("all")
	.dwattr $C$DW$1196, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1196, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1196, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1197	.dwtag  DW_TAG_member
	.dwattr $C$DW$1197, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$1197, DW_AT_name("bit")
	.dwattr $C$DW$1197, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1197, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1197, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$176


$C$DW$T$177	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$177, DW_AT_name("TZDCSEL_BITS")
	.dwattr $C$DW$T$177, DW_AT_byte_size(0x01)
$C$DW$1198	.dwtag  DW_TAG_member
	.dwattr $C$DW$1198, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1198, DW_AT_name("DCAEVT1")
	.dwattr $C$DW$1198, DW_AT_TI_symbol_name("_DCAEVT1")
	.dwattr $C$DW$1198, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$1198, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1198, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1199	.dwtag  DW_TAG_member
	.dwattr $C$DW$1199, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1199, DW_AT_name("DCAEVT2")
	.dwattr $C$DW$1199, DW_AT_TI_symbol_name("_DCAEVT2")
	.dwattr $C$DW$1199, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x03)
	.dwattr $C$DW$1199, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1199, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1200	.dwtag  DW_TAG_member
	.dwattr $C$DW$1200, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1200, DW_AT_name("DCBEVT1")
	.dwattr $C$DW$1200, DW_AT_TI_symbol_name("_DCBEVT1")
	.dwattr $C$DW$1200, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x03)
	.dwattr $C$DW$1200, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1201	.dwtag  DW_TAG_member
	.dwattr $C$DW$1201, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1201, DW_AT_name("DCBEVT2")
	.dwattr $C$DW$1201, DW_AT_TI_symbol_name("_DCBEVT2")
	.dwattr $C$DW$1201, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x03)
	.dwattr $C$DW$1201, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1201, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1202	.dwtag  DW_TAG_member
	.dwattr $C$DW$1202, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1202, DW_AT_name("rsvd1")
	.dwattr $C$DW$1202, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1202, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$1202, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1202, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$177


$C$DW$T$178	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$178, DW_AT_name("TZDCSEL_REG")
	.dwattr $C$DW$T$178, DW_AT_byte_size(0x01)
$C$DW$1203	.dwtag  DW_TAG_member
	.dwattr $C$DW$1203, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1203, DW_AT_name("all")
	.dwattr $C$DW$1203, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1203, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1203, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1204	.dwtag  DW_TAG_member
	.dwattr $C$DW$1204, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$1204, DW_AT_name("bit")
	.dwattr $C$DW$1204, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1204, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1204, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$178


$C$DW$T$179	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$179, DW_AT_name("TZEINT_BITS")
	.dwattr $C$DW$T$179, DW_AT_byte_size(0x01)
$C$DW$1205	.dwtag  DW_TAG_member
	.dwattr $C$DW$1205, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1205, DW_AT_name("rsvd1")
	.dwattr $C$DW$1205, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1205, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1205, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1205, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1206	.dwtag  DW_TAG_member
	.dwattr $C$DW$1206, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1206, DW_AT_name("CBC")
	.dwattr $C$DW$1206, DW_AT_TI_symbol_name("_CBC")
	.dwattr $C$DW$1206, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1206, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1206, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1207	.dwtag  DW_TAG_member
	.dwattr $C$DW$1207, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1207, DW_AT_name("OST")
	.dwattr $C$DW$1207, DW_AT_TI_symbol_name("_OST")
	.dwattr $C$DW$1207, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1207, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1207, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1208	.dwtag  DW_TAG_member
	.dwattr $C$DW$1208, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1208, DW_AT_name("DCAEVT1")
	.dwattr $C$DW$1208, DW_AT_TI_symbol_name("_DCAEVT1")
	.dwattr $C$DW$1208, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1208, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1208, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1209	.dwtag  DW_TAG_member
	.dwattr $C$DW$1209, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1209, DW_AT_name("DCAEVT2")
	.dwattr $C$DW$1209, DW_AT_TI_symbol_name("_DCAEVT2")
	.dwattr $C$DW$1209, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1209, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1209, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1210	.dwtag  DW_TAG_member
	.dwattr $C$DW$1210, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1210, DW_AT_name("DCBEVT1")
	.dwattr $C$DW$1210, DW_AT_TI_symbol_name("_DCBEVT1")
	.dwattr $C$DW$1210, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1210, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1210, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1211	.dwtag  DW_TAG_member
	.dwattr $C$DW$1211, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1211, DW_AT_name("DCBEVT2")
	.dwattr $C$DW$1211, DW_AT_TI_symbol_name("_DCBEVT2")
	.dwattr $C$DW$1211, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1211, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1211, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1212	.dwtag  DW_TAG_member
	.dwattr $C$DW$1212, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1212, DW_AT_name("rsvd2")
	.dwattr $C$DW$1212, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$1212, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x09)
	.dwattr $C$DW$1212, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1212, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$179


$C$DW$T$180	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$180, DW_AT_name("TZEINT_REG")
	.dwattr $C$DW$T$180, DW_AT_byte_size(0x01)
$C$DW$1213	.dwtag  DW_TAG_member
	.dwattr $C$DW$1213, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1213, DW_AT_name("all")
	.dwattr $C$DW$1213, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1213, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1213, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1214	.dwtag  DW_TAG_member
	.dwattr $C$DW$1214, DW_AT_type(*$C$DW$T$179)
	.dwattr $C$DW$1214, DW_AT_name("bit")
	.dwattr $C$DW$1214, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1214, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1214, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$180


$C$DW$T$181	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$181, DW_AT_name("TZFLG_BITS")
	.dwattr $C$DW$T$181, DW_AT_byte_size(0x01)
$C$DW$1215	.dwtag  DW_TAG_member
	.dwattr $C$DW$1215, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1215, DW_AT_name("INT")
	.dwattr $C$DW$1215, DW_AT_TI_symbol_name("_INT")
	.dwattr $C$DW$1215, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1215, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1215, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1216	.dwtag  DW_TAG_member
	.dwattr $C$DW$1216, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1216, DW_AT_name("CBC")
	.dwattr $C$DW$1216, DW_AT_TI_symbol_name("_CBC")
	.dwattr $C$DW$1216, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1216, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1216, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1217	.dwtag  DW_TAG_member
	.dwattr $C$DW$1217, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1217, DW_AT_name("OST")
	.dwattr $C$DW$1217, DW_AT_TI_symbol_name("_OST")
	.dwattr $C$DW$1217, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1217, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1217, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1218	.dwtag  DW_TAG_member
	.dwattr $C$DW$1218, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1218, DW_AT_name("DCAEVT1")
	.dwattr $C$DW$1218, DW_AT_TI_symbol_name("_DCAEVT1")
	.dwattr $C$DW$1218, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1218, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1218, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1219	.dwtag  DW_TAG_member
	.dwattr $C$DW$1219, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1219, DW_AT_name("DCAEVT2")
	.dwattr $C$DW$1219, DW_AT_TI_symbol_name("_DCAEVT2")
	.dwattr $C$DW$1219, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1219, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1219, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1220	.dwtag  DW_TAG_member
	.dwattr $C$DW$1220, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1220, DW_AT_name("DCBEVT1")
	.dwattr $C$DW$1220, DW_AT_TI_symbol_name("_DCBEVT1")
	.dwattr $C$DW$1220, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1220, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1220, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1221	.dwtag  DW_TAG_member
	.dwattr $C$DW$1221, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1221, DW_AT_name("DCBEVT2")
	.dwattr $C$DW$1221, DW_AT_TI_symbol_name("_DCBEVT2")
	.dwattr $C$DW$1221, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1221, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1221, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1222	.dwtag  DW_TAG_member
	.dwattr $C$DW$1222, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1222, DW_AT_name("rsvd1")
	.dwattr $C$DW$1222, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1222, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x09)
	.dwattr $C$DW$1222, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1222, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$181


$C$DW$T$182	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$182, DW_AT_name("TZFLG_REG")
	.dwattr $C$DW$T$182, DW_AT_byte_size(0x01)
$C$DW$1223	.dwtag  DW_TAG_member
	.dwattr $C$DW$1223, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1223, DW_AT_name("all")
	.dwattr $C$DW$1223, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1223, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1223, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1224	.dwtag  DW_TAG_member
	.dwattr $C$DW$1224, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$1224, DW_AT_name("bit")
	.dwattr $C$DW$1224, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1224, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1224, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$182


$C$DW$T$183	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$183, DW_AT_name("TZFRC_BITS")
	.dwattr $C$DW$T$183, DW_AT_byte_size(0x01)
$C$DW$1225	.dwtag  DW_TAG_member
	.dwattr $C$DW$1225, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1225, DW_AT_name("rsvd1")
	.dwattr $C$DW$1225, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1225, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1225, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1225, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1226	.dwtag  DW_TAG_member
	.dwattr $C$DW$1226, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1226, DW_AT_name("CBC")
	.dwattr $C$DW$1226, DW_AT_TI_symbol_name("_CBC")
	.dwattr $C$DW$1226, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1226, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1226, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1227	.dwtag  DW_TAG_member
	.dwattr $C$DW$1227, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1227, DW_AT_name("OST")
	.dwattr $C$DW$1227, DW_AT_TI_symbol_name("_OST")
	.dwattr $C$DW$1227, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1227, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1227, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1228	.dwtag  DW_TAG_member
	.dwattr $C$DW$1228, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1228, DW_AT_name("DCAEVT1")
	.dwattr $C$DW$1228, DW_AT_TI_symbol_name("_DCAEVT1")
	.dwattr $C$DW$1228, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1228, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1228, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1229	.dwtag  DW_TAG_member
	.dwattr $C$DW$1229, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1229, DW_AT_name("DCAEVT2")
	.dwattr $C$DW$1229, DW_AT_TI_symbol_name("_DCAEVT2")
	.dwattr $C$DW$1229, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1229, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1229, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1230	.dwtag  DW_TAG_member
	.dwattr $C$DW$1230, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1230, DW_AT_name("DCBEVT1")
	.dwattr $C$DW$1230, DW_AT_TI_symbol_name("_DCBEVT1")
	.dwattr $C$DW$1230, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1230, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1231	.dwtag  DW_TAG_member
	.dwattr $C$DW$1231, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1231, DW_AT_name("DCBEVT2")
	.dwattr $C$DW$1231, DW_AT_TI_symbol_name("_DCBEVT2")
	.dwattr $C$DW$1231, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1231, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1231, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1232	.dwtag  DW_TAG_member
	.dwattr $C$DW$1232, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1232, DW_AT_name("rsvd2")
	.dwattr $C$DW$1232, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$1232, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x09)
	.dwattr $C$DW$1232, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1232, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$183


$C$DW$T$184	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$184, DW_AT_name("TZFRC_REG")
	.dwattr $C$DW$T$184, DW_AT_byte_size(0x01)
$C$DW$1233	.dwtag  DW_TAG_member
	.dwattr $C$DW$1233, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1233, DW_AT_name("all")
	.dwattr $C$DW$1233, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1233, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1233, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1234	.dwtag  DW_TAG_member
	.dwattr $C$DW$1234, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$1234, DW_AT_name("bit")
	.dwattr $C$DW$1234, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1234, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1234, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$184


$C$DW$T$185	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$185, DW_AT_name("TZSEL_BITS")
	.dwattr $C$DW$T$185, DW_AT_byte_size(0x01)
$C$DW$1235	.dwtag  DW_TAG_member
	.dwattr $C$DW$1235, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1235, DW_AT_name("CBC1")
	.dwattr $C$DW$1235, DW_AT_TI_symbol_name("_CBC1")
	.dwattr $C$DW$1235, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1235, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1235, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1236	.dwtag  DW_TAG_member
	.dwattr $C$DW$1236, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1236, DW_AT_name("CBC2")
	.dwattr $C$DW$1236, DW_AT_TI_symbol_name("_CBC2")
	.dwattr $C$DW$1236, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1236, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1236, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1237	.dwtag  DW_TAG_member
	.dwattr $C$DW$1237, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1237, DW_AT_name("CBC3")
	.dwattr $C$DW$1237, DW_AT_TI_symbol_name("_CBC3")
	.dwattr $C$DW$1237, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1237, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1238	.dwtag  DW_TAG_member
	.dwattr $C$DW$1238, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1238, DW_AT_name("CBC4")
	.dwattr $C$DW$1238, DW_AT_TI_symbol_name("_CBC4")
	.dwattr $C$DW$1238, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1238, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1238, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1239	.dwtag  DW_TAG_member
	.dwattr $C$DW$1239, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1239, DW_AT_name("CBC5")
	.dwattr $C$DW$1239, DW_AT_TI_symbol_name("_CBC5")
	.dwattr $C$DW$1239, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1239, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1239, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1240	.dwtag  DW_TAG_member
	.dwattr $C$DW$1240, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1240, DW_AT_name("CBC6")
	.dwattr $C$DW$1240, DW_AT_TI_symbol_name("_CBC6")
	.dwattr $C$DW$1240, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1240, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1240, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1241	.dwtag  DW_TAG_member
	.dwattr $C$DW$1241, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1241, DW_AT_name("DCAEVT2")
	.dwattr $C$DW$1241, DW_AT_TI_symbol_name("_DCAEVT2")
	.dwattr $C$DW$1241, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1241, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1241, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1242	.dwtag  DW_TAG_member
	.dwattr $C$DW$1242, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1242, DW_AT_name("DCBEVT2")
	.dwattr $C$DW$1242, DW_AT_TI_symbol_name("_DCBEVT2")
	.dwattr $C$DW$1242, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1242, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1243	.dwtag  DW_TAG_member
	.dwattr $C$DW$1243, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1243, DW_AT_name("OSHT1")
	.dwattr $C$DW$1243, DW_AT_TI_symbol_name("_OSHT1")
	.dwattr $C$DW$1243, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1243, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1243, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1244	.dwtag  DW_TAG_member
	.dwattr $C$DW$1244, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1244, DW_AT_name("OSHT2")
	.dwattr $C$DW$1244, DW_AT_TI_symbol_name("_OSHT2")
	.dwattr $C$DW$1244, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1244, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1244, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1245	.dwtag  DW_TAG_member
	.dwattr $C$DW$1245, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1245, DW_AT_name("OSHT3")
	.dwattr $C$DW$1245, DW_AT_TI_symbol_name("_OSHT3")
	.dwattr $C$DW$1245, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1245, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1245, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1246	.dwtag  DW_TAG_member
	.dwattr $C$DW$1246, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1246, DW_AT_name("OSHT4")
	.dwattr $C$DW$1246, DW_AT_TI_symbol_name("_OSHT4")
	.dwattr $C$DW$1246, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1246, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1247	.dwtag  DW_TAG_member
	.dwattr $C$DW$1247, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1247, DW_AT_name("OSHT5")
	.dwattr $C$DW$1247, DW_AT_TI_symbol_name("_OSHT5")
	.dwattr $C$DW$1247, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1247, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1247, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1248	.dwtag  DW_TAG_member
	.dwattr $C$DW$1248, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1248, DW_AT_name("OSHT6")
	.dwattr $C$DW$1248, DW_AT_TI_symbol_name("_OSHT6")
	.dwattr $C$DW$1248, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1248, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1248, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1249	.dwtag  DW_TAG_member
	.dwattr $C$DW$1249, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1249, DW_AT_name("DCAEVT1")
	.dwattr $C$DW$1249, DW_AT_TI_symbol_name("_DCAEVT1")
	.dwattr $C$DW$1249, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1249, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1249, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1250	.dwtag  DW_TAG_member
	.dwattr $C$DW$1250, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1250, DW_AT_name("DCBEVT1")
	.dwattr $C$DW$1250, DW_AT_TI_symbol_name("_DCBEVT1")
	.dwattr $C$DW$1250, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1250, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1250, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$185


$C$DW$T$186	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$186, DW_AT_name("TZSEL_REG")
	.dwattr $C$DW$T$186, DW_AT_byte_size(0x01)
$C$DW$1251	.dwtag  DW_TAG_member
	.dwattr $C$DW$1251, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$1251, DW_AT_name("all")
	.dwattr $C$DW$1251, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1251, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1251, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1252	.dwtag  DW_TAG_member
	.dwattr $C$DW$1252, DW_AT_type(*$C$DW$T$185)
	.dwattr $C$DW$1252, DW_AT_name("bit")
	.dwattr $C$DW$1252, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1252, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1252, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$186

$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$298	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$298, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$298, DW_AT_language(DW_LANG_C)

$C$DW$T$117	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$1253	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1253, DW_AT_type(*$C$DW$T$116)
	.dwendtag $C$DW$T$117

$C$DW$T$118	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$118, DW_AT_address_class(0x16)
$C$DW$T$119	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$119, DW_AT_language(DW_LANG_C)

$C$DW$T$139	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$139, DW_AT_language(DW_LANG_C)
$C$DW$T$140	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$T$140, DW_AT_address_class(0x16)
$C$DW$T$141	.dwtag  DW_TAG_typedef, DW_AT_name("PINT")
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$T$141, DW_AT_language(DW_LANG_C)

$C$DW$T$211	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$211, DW_AT_language(DW_LANG_C)
$C$DW$1254	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1254, DW_AT_type(*$C$DW$T$210)
	.dwendtag $C$DW$T$211

$C$DW$T$212	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$212, DW_AT_type(*$C$DW$T$211)
	.dwattr $C$DW$T$212, DW_AT_address_class(0x16)
$C$DW$T$213	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$213, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$T$213, DW_AT_language(DW_LANG_C)
$C$DW$T$215	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$215, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$T$215, DW_AT_language(DW_LANG_C)
$C$DW$T$225	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$225, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$T$225, DW_AT_language(DW_LANG_C)
$C$DW$T$224	.dwtag  DW_TAG_typedef, DW_AT_name("post_sync_t")
	.dwattr $C$DW$T$224, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$T$224, DW_AT_language(DW_LANG_C)
$C$DW$T$214	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$214, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$T$214, DW_AT_language(DW_LANG_C)
$C$DW$T$216	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$216, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$T$216, DW_AT_language(DW_LANG_C)

$C$DW$T$220	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$220, DW_AT_language(DW_LANG_C)
$C$DW$1255	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1255, DW_AT_type(*$C$DW$T$210)
$C$DW$1256	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1256, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$220

$C$DW$T$221	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$221, DW_AT_type(*$C$DW$T$220)
	.dwattr $C$DW$T$221, DW_AT_address_class(0x16)
$C$DW$T$253	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$253, DW_AT_type(*$C$DW$T$221)
	.dwattr $C$DW$T$253, DW_AT_language(DW_LANG_C)
$C$DW$T$222	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$222, DW_AT_type(*$C$DW$T$221)
	.dwattr $C$DW$T$222, DW_AT_language(DW_LANG_C)
$C$DW$T$226	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$226, DW_AT_type(*$C$DW$T$221)
	.dwattr $C$DW$T$226, DW_AT_language(DW_LANG_C)

$C$DW$T$244	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$244, DW_AT_language(DW_LANG_C)
$C$DW$1257	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1257, DW_AT_type(*$C$DW$T$210)
$C$DW$1258	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1258, DW_AT_type(*$C$DW$T$6)
$C$DW$1259	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1259, DW_AT_type(*$C$DW$T$9)
$C$DW$1260	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1260, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$244

$C$DW$T$245	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$245, DW_AT_type(*$C$DW$T$244)
	.dwattr $C$DW$T$245, DW_AT_address_class(0x16)
$C$DW$T$246	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$246, DW_AT_type(*$C$DW$T$245)
	.dwattr $C$DW$T$246, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$303	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$303, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$303, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$303, DW_AT_byte_size(0x08)
$C$DW$1261	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1261, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$303

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$227	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$227, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$227, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$1262	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1262, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$188	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$188, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$188, DW_AT_address_class(0x16)
$C$DW$1263	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1263, DW_AT_type(*$C$DW$T$6)
$C$DW$T$199	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$199, DW_AT_type(*$C$DW$1263)
$C$DW$T$200	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$200, DW_AT_type(*$C$DW$T$199)
	.dwattr $C$DW$T$200, DW_AT_address_class(0x16)
$C$DW$T$247	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$247, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$247, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$218	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$218, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$218, DW_AT_address_class(0x16)

$C$DW$T$308	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$308, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$308, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$308, DW_AT_byte_size(0x17)
$C$DW$1264	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1264, DW_AT_upper_bound(0x16)
	.dwendtag $C$DW$T$308


$C$DW$T$309	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$309, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$309, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$309, DW_AT_byte_size(0x10)
$C$DW$1265	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1265, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$309

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$311	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$311, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$311, DW_AT_language(DW_LANG_C)
$C$DW$1266	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1266, DW_AT_type(*$C$DW$T$9)
$C$DW$T$197	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$197, DW_AT_type(*$C$DW$1266)
$C$DW$T$198	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$198, DW_AT_type(*$C$DW$T$197)
	.dwattr $C$DW$T$198, DW_AT_address_class(0x16)

$C$DW$T$316	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$316, DW_AT_type(*$C$DW$T$197)
	.dwattr $C$DW$T$316, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$316, DW_AT_byte_size(0x0b)
$C$DW$1267	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1267, DW_AT_upper_bound(0x0a)
	.dwendtag $C$DW$T$316

$C$DW$T$219	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$219, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$219, DW_AT_address_class(0x16)

$C$DW$T$318	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$318, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$318, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$318, DW_AT_byte_size(0x02)
$C$DW$1268	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1268, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$318

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$114	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$114, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)

$C$DW$T$49	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x10)
$C$DW$1269	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1269, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$49


$C$DW$T$50	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x0a)
$C$DW$1270	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1270, DW_AT_upper_bound(0x09)
	.dwendtag $C$DW$T$50


$C$DW$T$51	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$51, DW_AT_byte_size(0x03)
$C$DW$1271	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1271, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$51


$C$DW$T$79	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$79, DW_AT_byte_size(0x02)
$C$DW$1272	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1272, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$79


$C$DW$T$80	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$80, DW_AT_byte_size(0x06)
$C$DW$1273	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1273, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$80


$C$DW$T$97	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$97, DW_AT_byte_size(0x08)
$C$DW$1274	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1274, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$97

$C$DW$T$123	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$55	.dwtag  DW_TAG_typedef, DW_AT_name("Uint32")
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)

$C$DW$T$201	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$201, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$201, DW_AT_language(DW_LANG_C)
$C$DW$1275	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1275, DW_AT_type(*$C$DW$T$6)
$C$DW$1276	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1276, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$201

$C$DW$T$202	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$202, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$T$202, DW_AT_address_class(0x16)
$C$DW$T$203	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$203, DW_AT_type(*$C$DW$T$202)
	.dwattr $C$DW$T$203, DW_AT_language(DW_LANG_C)
$C$DW$T$217	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$217, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$217, DW_AT_address_class(0x16)

$C$DW$T$228	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$228, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$228, DW_AT_language(DW_LANG_C)
$C$DW$1277	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1277, DW_AT_type(*$C$DW$T$210)
$C$DW$1278	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1278, DW_AT_type(*$C$DW$T$191)
$C$DW$1279	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1279, DW_AT_type(*$C$DW$T$6)
$C$DW$1280	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1280, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$228

$C$DW$T$229	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$229, DW_AT_type(*$C$DW$T$228)
	.dwattr $C$DW$T$229, DW_AT_address_class(0x16)
$C$DW$T$230	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$230, DW_AT_type(*$C$DW$T$229)
	.dwattr $C$DW$T$230, DW_AT_language(DW_LANG_C)
$C$DW$1281	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1281, DW_AT_type(*$C$DW$T$230)
$C$DW$T$231	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$231, DW_AT_type(*$C$DW$1281)
$C$DW$T$232	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$232, DW_AT_type(*$C$DW$T$231)
	.dwattr $C$DW$T$232, DW_AT_address_class(0x16)
$C$DW$T$233	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$233, DW_AT_type(*$C$DW$T$232)
	.dwattr $C$DW$T$233, DW_AT_address_class(0x16)

$C$DW$T$237	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$237, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$237, DW_AT_language(DW_LANG_C)
$C$DW$1282	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1282, DW_AT_type(*$C$DW$T$210)
$C$DW$1283	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1283, DW_AT_type(*$C$DW$T$9)
$C$DW$1284	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1284, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$237

$C$DW$T$238	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$238, DW_AT_type(*$C$DW$T$237)
	.dwattr $C$DW$T$238, DW_AT_address_class(0x16)
$C$DW$T$239	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$239, DW_AT_type(*$C$DW$T$238)
	.dwattr $C$DW$T$239, DW_AT_language(DW_LANG_C)
$C$DW$T$122	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$25	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x04)
$C$DW$1285	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1285, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$25


$C$DW$T$327	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$327, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$327, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$327, DW_AT_byte_size(0x20)
$C$DW$1286	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1286, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$327

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$125	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$125, DW_AT_address_class(0x16)
$C$DW$T$126	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)

$C$DW$T$240	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$240, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$240, DW_AT_byte_size(0x01)
$C$DW$1287	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$1288	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$240

$C$DW$T$241	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$241, DW_AT_type(*$C$DW$T$240)
	.dwattr $C$DW$T$241, DW_AT_language(DW_LANG_C)

$C$DW$T$206	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$206, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$206, DW_AT_byte_size(0x01)
$C$DW$1289	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$1290	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$1291	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$1292	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$1293	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$1294	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$1295	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$1296	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$206

$C$DW$T$207	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$207, DW_AT_type(*$C$DW$T$206)
	.dwattr $C$DW$T$207, DW_AT_language(DW_LANG_C)

$C$DW$T$223	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$223, DW_AT_type(*$C$DW$T$207)
	.dwattr $C$DW$T$223, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$223, DW_AT_byte_size(0x80)
$C$DW$1297	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1297, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$223


$C$DW$T$187	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$187, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$187, DW_AT_byte_size(0x06)
$C$DW$1298	.dwtag  DW_TAG_member
	.dwattr $C$DW$1298, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1298, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$1298, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$1298, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1299	.dwtag  DW_TAG_member
	.dwattr $C$DW$1299, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1299, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$1299, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$1299, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1299, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1300	.dwtag  DW_TAG_member
	.dwattr $C$DW$1300, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1300, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$1300, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$1300, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1300, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1301	.dwtag  DW_TAG_member
	.dwattr $C$DW$1301, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1301, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$1301, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$1301, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1301, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1302	.dwtag  DW_TAG_member
	.dwattr $C$DW$1302, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1302, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$1302, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$1302, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1302, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1303	.dwtag  DW_TAG_member
	.dwattr $C$DW$1303, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1303, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$1303, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$1303, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$1303, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$187

$C$DW$T$194	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$194, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$T$194, DW_AT_language(DW_LANG_C)
$C$DW$1304	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1304, DW_AT_type(*$C$DW$T$194)
$C$DW$T$195	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$195, DW_AT_type(*$C$DW$1304)
$C$DW$T$196	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$196, DW_AT_type(*$C$DW$T$195)
	.dwattr $C$DW$T$196, DW_AT_address_class(0x16)

$C$DW$T$250	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$250, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$250, DW_AT_byte_size(0x132)
$C$DW$1305	.dwtag  DW_TAG_member
	.dwattr $C$DW$1305, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$1305, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$1305, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$1305, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1305, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1306	.dwtag  DW_TAG_member
	.dwattr $C$DW$1306, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$1306, DW_AT_name("objdict")
	.dwattr $C$DW$1306, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$1306, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1306, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1307	.dwtag  DW_TAG_member
	.dwattr $C$DW$1307, DW_AT_type(*$C$DW$T$193)
	.dwattr $C$DW$1307, DW_AT_name("PDO_status")
	.dwattr $C$DW$1307, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$1307, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1307, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1308	.dwtag  DW_TAG_member
	.dwattr $C$DW$1308, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$1308, DW_AT_name("firstIndex")
	.dwattr $C$DW$1308, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$1308, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1308, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1309	.dwtag  DW_TAG_member
	.dwattr $C$DW$1309, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$1309, DW_AT_name("lastIndex")
	.dwattr $C$DW$1309, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$1309, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1309, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1310	.dwtag  DW_TAG_member
	.dwattr $C$DW$1310, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$1310, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$1310, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$1310, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$1310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1311	.dwtag  DW_TAG_member
	.dwattr $C$DW$1311, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$1311, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$1311, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$1311, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1311, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1312	.dwtag  DW_TAG_member
	.dwattr $C$DW$1312, DW_AT_type(*$C$DW$T$203)
	.dwattr $C$DW$1312, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$1312, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$1312, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$1312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1313	.dwtag  DW_TAG_member
	.dwattr $C$DW$1313, DW_AT_type(*$C$DW$T$205)
	.dwattr $C$DW$1313, DW_AT_name("transfers")
	.dwattr $C$DW$1313, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$1313, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1313, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1314	.dwtag  DW_TAG_member
	.dwattr $C$DW$1314, DW_AT_type(*$C$DW$T$207)
	.dwattr $C$DW$1314, DW_AT_name("nodeState")
	.dwattr $C$DW$1314, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$1314, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$1314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1315	.dwtag  DW_TAG_member
	.dwattr $C$DW$1315, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$1315, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$1315, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$1315, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$1315, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1316	.dwtag  DW_TAG_member
	.dwattr $C$DW$1316, DW_AT_type(*$C$DW$T$213)
	.dwattr $C$DW$1316, DW_AT_name("initialisation")
	.dwattr $C$DW$1316, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$1316, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$1316, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1317	.dwtag  DW_TAG_member
	.dwattr $C$DW$1317, DW_AT_type(*$C$DW$T$214)
	.dwattr $C$DW$1317, DW_AT_name("preOperational")
	.dwattr $C$DW$1317, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$1317, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$1317, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1318	.dwtag  DW_TAG_member
	.dwattr $C$DW$1318, DW_AT_type(*$C$DW$T$215)
	.dwattr $C$DW$1318, DW_AT_name("operational")
	.dwattr $C$DW$1318, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$1318, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$1318, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1319	.dwtag  DW_TAG_member
	.dwattr $C$DW$1319, DW_AT_type(*$C$DW$T$216)
	.dwattr $C$DW$1319, DW_AT_name("stopped")
	.dwattr $C$DW$1319, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$1319, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$1319, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1320	.dwtag  DW_TAG_member
	.dwattr $C$DW$1320, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$1320, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$1320, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$1320, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$1320, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1321	.dwtag  DW_TAG_member
	.dwattr $C$DW$1321, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$1321, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$1321, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$1321, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$1321, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1322	.dwtag  DW_TAG_member
	.dwattr $C$DW$1322, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$1322, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$1322, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$1322, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$1322, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1323	.dwtag  DW_TAG_member
	.dwattr $C$DW$1323, DW_AT_type(*$C$DW$T$217)
	.dwattr $C$DW$1323, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$1323, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$1323, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$1323, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1324	.dwtag  DW_TAG_member
	.dwattr $C$DW$1324, DW_AT_type(*$C$DW$T$218)
	.dwattr $C$DW$1324, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$1324, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$1324, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$1324, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1325	.dwtag  DW_TAG_member
	.dwattr $C$DW$1325, DW_AT_type(*$C$DW$T$219)
	.dwattr $C$DW$1325, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$1325, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$1325, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$1325, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1326	.dwtag  DW_TAG_member
	.dwattr $C$DW$1326, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1326, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$1326, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$1326, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$1326, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1327	.dwtag  DW_TAG_member
	.dwattr $C$DW$1327, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$1327, DW_AT_name("heartbeatError")
	.dwattr $C$DW$1327, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$1327, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$1327, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1328	.dwtag  DW_TAG_member
	.dwattr $C$DW$1328, DW_AT_type(*$C$DW$T$223)
	.dwattr $C$DW$1328, DW_AT_name("NMTable")
	.dwattr $C$DW$1328, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$1328, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$1328, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1329	.dwtag  DW_TAG_member
	.dwattr $C$DW$1329, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1329, DW_AT_name("syncTimer")
	.dwattr $C$DW$1329, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$1329, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$1329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1330	.dwtag  DW_TAG_member
	.dwattr $C$DW$1330, DW_AT_type(*$C$DW$T$217)
	.dwattr $C$DW$1330, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$1330, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$1330, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$1330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1331	.dwtag  DW_TAG_member
	.dwattr $C$DW$1331, DW_AT_type(*$C$DW$T$217)
	.dwattr $C$DW$1331, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$1331, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$1331, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$1331, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1332	.dwtag  DW_TAG_member
	.dwattr $C$DW$1332, DW_AT_type(*$C$DW$T$224)
	.dwattr $C$DW$1332, DW_AT_name("post_sync")
	.dwattr $C$DW$1332, DW_AT_TI_symbol_name("_post_sync")
	.dwattr $C$DW$1332, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$1332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1333	.dwtag  DW_TAG_member
	.dwattr $C$DW$1333, DW_AT_type(*$C$DW$T$225)
	.dwattr $C$DW$1333, DW_AT_name("post_TPDO")
	.dwattr $C$DW$1333, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$1333, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$1333, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1334	.dwtag  DW_TAG_member
	.dwattr $C$DW$1334, DW_AT_type(*$C$DW$T$226)
	.dwattr $C$DW$1334, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$1334, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$1334, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$1334, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1335	.dwtag  DW_TAG_member
	.dwattr $C$DW$1335, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1335, DW_AT_name("toggle")
	.dwattr $C$DW$1335, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$1335, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$1335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1336	.dwtag  DW_TAG_member
	.dwattr $C$DW$1336, DW_AT_type(*$C$DW$T$227)
	.dwattr $C$DW$1336, DW_AT_name("canHandle")
	.dwattr $C$DW$1336, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$1336, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$1336, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1337	.dwtag  DW_TAG_member
	.dwattr $C$DW$1337, DW_AT_type(*$C$DW$T$236)
	.dwattr $C$DW$1337, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$1337, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$1337, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$1337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1338	.dwtag  DW_TAG_member
	.dwattr $C$DW$1338, DW_AT_type(*$C$DW$T$239)
	.dwattr $C$DW$1338, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$1338, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$1338, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$1338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1339	.dwtag  DW_TAG_member
	.dwattr $C$DW$1339, DW_AT_type(*$C$DW$T$230)
	.dwattr $C$DW$1339, DW_AT_name("globalCallback")
	.dwattr $C$DW$1339, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$1339, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$1339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1340	.dwtag  DW_TAG_member
	.dwattr $C$DW$1340, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$1340, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$1340, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$1340, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$1340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1341	.dwtag  DW_TAG_member
	.dwattr $C$DW$1341, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$1341, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$1341, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$1341, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$1341, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1342	.dwtag  DW_TAG_member
	.dwattr $C$DW$1342, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1342, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$1342, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$1342, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$1342, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1343	.dwtag  DW_TAG_member
	.dwattr $C$DW$1343, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1343, DW_AT_name("dcf_request")
	.dwattr $C$DW$1343, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$1343, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$1343, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1344	.dwtag  DW_TAG_member
	.dwattr $C$DW$1344, DW_AT_type(*$C$DW$T$241)
	.dwattr $C$DW$1344, DW_AT_name("error_state")
	.dwattr $C$DW$1344, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$1344, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$1344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1345	.dwtag  DW_TAG_member
	.dwattr $C$DW$1345, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1345, DW_AT_name("error_history_size")
	.dwattr $C$DW$1345, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$1345, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$1345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1346	.dwtag  DW_TAG_member
	.dwattr $C$DW$1346, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$1346, DW_AT_name("error_number")
	.dwattr $C$DW$1346, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$1346, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$1346, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1347	.dwtag  DW_TAG_member
	.dwattr $C$DW$1347, DW_AT_type(*$C$DW$T$217)
	.dwattr $C$DW$1347, DW_AT_name("error_first_element")
	.dwattr $C$DW$1347, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$1347, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$1347, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1348	.dwtag  DW_TAG_member
	.dwattr $C$DW$1348, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$1348, DW_AT_name("error_register")
	.dwattr $C$DW$1348, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$1348, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$1348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1349	.dwtag  DW_TAG_member
	.dwattr $C$DW$1349, DW_AT_type(*$C$DW$T$217)
	.dwattr $C$DW$1349, DW_AT_name("error_cobid")
	.dwattr $C$DW$1349, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$1349, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$1349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1350	.dwtag  DW_TAG_member
	.dwattr $C$DW$1350, DW_AT_type(*$C$DW$T$243)
	.dwattr $C$DW$1350, DW_AT_name("error_data")
	.dwattr $C$DW$1350, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$1350, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$1350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1351	.dwtag  DW_TAG_member
	.dwattr $C$DW$1351, DW_AT_type(*$C$DW$T$246)
	.dwattr $C$DW$1351, DW_AT_name("post_emcy")
	.dwattr $C$DW$1351, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$1351, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$1351, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1352	.dwtag  DW_TAG_member
	.dwattr $C$DW$1352, DW_AT_type(*$C$DW$T$247)
	.dwattr $C$DW$1352, DW_AT_name("lss_transfer")
	.dwattr $C$DW$1352, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$1352, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$1352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1353	.dwtag  DW_TAG_member
	.dwattr $C$DW$1353, DW_AT_type(*$C$DW$T$249)
	.dwattr $C$DW$1353, DW_AT_name("eeprom_index")
	.dwattr $C$DW$1353, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$1353, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$1353, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1354	.dwtag  DW_TAG_member
	.dwattr $C$DW$1354, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1354, DW_AT_name("eeprom_size")
	.dwattr $C$DW$1354, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$1354, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$1354, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$250

$C$DW$T$209	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$209, DW_AT_type(*$C$DW$T$250)
	.dwattr $C$DW$T$209, DW_AT_language(DW_LANG_C)
$C$DW$T$210	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$210, DW_AT_type(*$C$DW$T$209)
	.dwattr $C$DW$T$210, DW_AT_address_class(0x16)

$C$DW$T$252	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$252, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$252, DW_AT_byte_size(0x0e)
$C$DW$1355	.dwtag  DW_TAG_member
	.dwattr $C$DW$1355, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1355, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$1355, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$1355, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1356	.dwtag  DW_TAG_member
	.dwattr $C$DW$1356, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1356, DW_AT_name("event_timer")
	.dwattr $C$DW$1356, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$1356, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1356, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1357	.dwtag  DW_TAG_member
	.dwattr $C$DW$1357, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1357, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$1357, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$1357, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1358	.dwtag  DW_TAG_member
	.dwattr $C$DW$1358, DW_AT_type(*$C$DW$T$251)
	.dwattr $C$DW$1358, DW_AT_name("last_message")
	.dwattr $C$DW$1358, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$1358, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1358, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$252

$C$DW$T$192	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$192, DW_AT_type(*$C$DW$T$252)
	.dwattr $C$DW$T$192, DW_AT_language(DW_LANG_C)
$C$DW$T$193	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$193, DW_AT_type(*$C$DW$T$192)
	.dwattr $C$DW$T$193, DW_AT_address_class(0x16)

$C$DW$T$254	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$254, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$254, DW_AT_byte_size(0x14)
$C$DW$1359	.dwtag  DW_TAG_member
	.dwattr $C$DW$1359, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1359, DW_AT_name("nodeId")
	.dwattr $C$DW$1359, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$1359, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1360	.dwtag  DW_TAG_member
	.dwattr $C$DW$1360, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1360, DW_AT_name("whoami")
	.dwattr $C$DW$1360, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$1360, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1361	.dwtag  DW_TAG_member
	.dwattr $C$DW$1361, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1361, DW_AT_name("state")
	.dwattr $C$DW$1361, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$1361, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1361, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1362	.dwtag  DW_TAG_member
	.dwattr $C$DW$1362, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1362, DW_AT_name("toggle")
	.dwattr $C$DW$1362, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$1362, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1362, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1363	.dwtag  DW_TAG_member
	.dwattr $C$DW$1363, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1363, DW_AT_name("abortCode")
	.dwattr $C$DW$1363, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$1363, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1363, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1364	.dwtag  DW_TAG_member
	.dwattr $C$DW$1364, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1364, DW_AT_name("index")
	.dwattr $C$DW$1364, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$1364, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1365	.dwtag  DW_TAG_member
	.dwattr $C$DW$1365, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1365, DW_AT_name("subIndex")
	.dwattr $C$DW$1365, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$1365, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$1365, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1366	.dwtag  DW_TAG_member
	.dwattr $C$DW$1366, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1366, DW_AT_name("port")
	.dwattr $C$DW$1366, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$1366, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1367	.dwtag  DW_TAG_member
	.dwattr $C$DW$1367, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1367, DW_AT_name("count")
	.dwattr $C$DW$1367, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$1367, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$1367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1368	.dwtag  DW_TAG_member
	.dwattr $C$DW$1368, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1368, DW_AT_name("offset")
	.dwattr $C$DW$1368, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$1368, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1368, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1369	.dwtag  DW_TAG_member
	.dwattr $C$DW$1369, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$1369, DW_AT_name("datap")
	.dwattr $C$DW$1369, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$1369, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$1369, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1370	.dwtag  DW_TAG_member
	.dwattr $C$DW$1370, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1370, DW_AT_name("dataType")
	.dwattr $C$DW$1370, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$1370, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1370, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1371	.dwtag  DW_TAG_member
	.dwattr $C$DW$1371, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1371, DW_AT_name("timer")
	.dwattr $C$DW$1371, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$1371, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$1371, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1372	.dwtag  DW_TAG_member
	.dwattr $C$DW$1372, DW_AT_type(*$C$DW$T$253)
	.dwattr $C$DW$1372, DW_AT_name("Callback")
	.dwattr $C$DW$1372, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$1372, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$1372, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$254

$C$DW$T$204	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$204, DW_AT_type(*$C$DW$T$254)
	.dwattr $C$DW$T$204, DW_AT_language(DW_LANG_C)

$C$DW$T$205	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$205, DW_AT_type(*$C$DW$T$204)
	.dwattr $C$DW$T$205, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$205, DW_AT_byte_size(0x3c)
$C$DW$1373	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1373, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$205


$C$DW$T$258	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$258, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$258, DW_AT_byte_size(0x04)
$C$DW$1374	.dwtag  DW_TAG_member
	.dwattr $C$DW$1374, DW_AT_type(*$C$DW$T$257)
	.dwattr $C$DW$1374, DW_AT_name("pSubindex")
	.dwattr $C$DW$1374, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$1374, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1374, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1375	.dwtag  DW_TAG_member
	.dwattr $C$DW$1375, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1375, DW_AT_name("bSubCount")
	.dwattr $C$DW$1375, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$1375, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1375, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1376	.dwtag  DW_TAG_member
	.dwattr $C$DW$1376, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1376, DW_AT_name("index")
	.dwattr $C$DW$1376, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$1376, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1376, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$258

$C$DW$T$189	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$189, DW_AT_type(*$C$DW$T$258)
	.dwattr $C$DW$T$189, DW_AT_language(DW_LANG_C)
$C$DW$1377	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1377, DW_AT_type(*$C$DW$T$189)
$C$DW$T$190	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$190, DW_AT_type(*$C$DW$1377)
$C$DW$T$191	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$191, DW_AT_type(*$C$DW$T$190)
	.dwattr $C$DW$T$191, DW_AT_address_class(0x16)

$C$DW$T$234	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$234, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$T$234, DW_AT_language(DW_LANG_C)
$C$DW$1378	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1378, DW_AT_type(*$C$DW$T$9)
$C$DW$1379	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1379, DW_AT_type(*$C$DW$T$217)
$C$DW$1380	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1380, DW_AT_type(*$C$DW$T$233)
	.dwendtag $C$DW$T$234

$C$DW$T$235	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$235, DW_AT_type(*$C$DW$T$234)
	.dwattr $C$DW$T$235, DW_AT_address_class(0x16)
$C$DW$T$236	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$236, DW_AT_type(*$C$DW$T$235)
	.dwattr $C$DW$T$236, DW_AT_language(DW_LANG_C)

$C$DW$T$259	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$259, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$259, DW_AT_byte_size(0x08)
$C$DW$1381	.dwtag  DW_TAG_member
	.dwattr $C$DW$1381, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1381, DW_AT_name("bAccessType")
	.dwattr $C$DW$1381, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$1381, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1382	.dwtag  DW_TAG_member
	.dwattr $C$DW$1382, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1382, DW_AT_name("bDataType")
	.dwattr $C$DW$1382, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$1382, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1383	.dwtag  DW_TAG_member
	.dwattr $C$DW$1383, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1383, DW_AT_name("size")
	.dwattr $C$DW$1383, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$1383, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1383, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1384	.dwtag  DW_TAG_member
	.dwattr $C$DW$1384, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1384, DW_AT_name("pObject")
	.dwattr $C$DW$1384, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$1384, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1384, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1385	.dwtag  DW_TAG_member
	.dwattr $C$DW$1385, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1385, DW_AT_name("bProcessor")
	.dwattr $C$DW$1385, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$1385, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1385, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$259

$C$DW$1386	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1386, DW_AT_type(*$C$DW$T$259)
$C$DW$T$255	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$255, DW_AT_type(*$C$DW$1386)
$C$DW$T$256	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$256, DW_AT_type(*$C$DW$T$255)
	.dwattr $C$DW$T$256, DW_AT_language(DW_LANG_C)
$C$DW$T$257	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$257, DW_AT_type(*$C$DW$T$256)
	.dwattr $C$DW$T$257, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$1387	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$1387, DW_AT_location[DW_OP_reg0]
$C$DW$1388	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$1388, DW_AT_location[DW_OP_reg1]
$C$DW$1389	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$1389, DW_AT_location[DW_OP_reg2]
$C$DW$1390	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$1390, DW_AT_location[DW_OP_reg3]
$C$DW$1391	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$1391, DW_AT_location[DW_OP_reg20]
$C$DW$1392	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$1392, DW_AT_location[DW_OP_reg21]
$C$DW$1393	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$1393, DW_AT_location[DW_OP_reg22]
$C$DW$1394	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$1394, DW_AT_location[DW_OP_reg23]
$C$DW$1395	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$1395, DW_AT_location[DW_OP_reg24]
$C$DW$1396	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$1396, DW_AT_location[DW_OP_reg25]
$C$DW$1397	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$1397, DW_AT_location[DW_OP_reg26]
$C$DW$1398	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$1398, DW_AT_location[DW_OP_reg28]
$C$DW$1399	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$1399, DW_AT_location[DW_OP_reg29]
$C$DW$1400	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$1400, DW_AT_location[DW_OP_reg30]
$C$DW$1401	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$1401, DW_AT_location[DW_OP_reg31]
$C$DW$1402	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$1402, DW_AT_location[DW_OP_regx 0x20]
$C$DW$1403	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$1403, DW_AT_location[DW_OP_regx 0x21]
$C$DW$1404	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$1404, DW_AT_location[DW_OP_regx 0x22]
$C$DW$1405	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$1405, DW_AT_location[DW_OP_regx 0x23]
$C$DW$1406	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$1406, DW_AT_location[DW_OP_regx 0x24]
$C$DW$1407	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$1407, DW_AT_location[DW_OP_regx 0x25]
$C$DW$1408	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$1408, DW_AT_location[DW_OP_regx 0x26]
$C$DW$1409	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$1409, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$1410	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$1410, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$1411	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$1411, DW_AT_location[DW_OP_reg4]
$C$DW$1412	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$1412, DW_AT_location[DW_OP_reg6]
$C$DW$1413	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$1413, DW_AT_location[DW_OP_reg8]
$C$DW$1414	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$1414, DW_AT_location[DW_OP_reg10]
$C$DW$1415	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$1415, DW_AT_location[DW_OP_reg12]
$C$DW$1416	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$1416, DW_AT_location[DW_OP_reg14]
$C$DW$1417	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$1417, DW_AT_location[DW_OP_reg16]
$C$DW$1418	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$1418, DW_AT_location[DW_OP_reg17]
$C$DW$1419	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$1419, DW_AT_location[DW_OP_reg18]
$C$DW$1420	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$1420, DW_AT_location[DW_OP_reg19]
$C$DW$1421	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$1421, DW_AT_location[DW_OP_reg5]
$C$DW$1422	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$1422, DW_AT_location[DW_OP_reg7]
$C$DW$1423	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$1423, DW_AT_location[DW_OP_reg9]
$C$DW$1424	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$1424, DW_AT_location[DW_OP_reg11]
$C$DW$1425	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$1425, DW_AT_location[DW_OP_reg13]
$C$DW$1426	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$1426, DW_AT_location[DW_OP_reg15]
$C$DW$1427	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$1427, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$1428	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$1428, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$1429	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$1429, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$1430	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$1430, DW_AT_location[DW_OP_regx 0x30]
$C$DW$1431	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$1431, DW_AT_location[DW_OP_regx 0x33]
$C$DW$1432	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$1432, DW_AT_location[DW_OP_regx 0x34]
$C$DW$1433	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$1433, DW_AT_location[DW_OP_regx 0x37]
$C$DW$1434	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$1434, DW_AT_location[DW_OP_regx 0x38]
$C$DW$1435	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$1435, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$1436	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$1436, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$1437	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$1437, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$1438	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$1438, DW_AT_location[DW_OP_regx 0x40]
$C$DW$1439	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$1439, DW_AT_location[DW_OP_regx 0x43]
$C$DW$1440	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$1440, DW_AT_location[DW_OP_regx 0x44]
$C$DW$1441	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$1441, DW_AT_location[DW_OP_regx 0x47]
$C$DW$1442	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$1442, DW_AT_location[DW_OP_regx 0x48]
$C$DW$1443	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$1443, DW_AT_location[DW_OP_regx 0x49]
$C$DW$1444	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$1444, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$1445	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$1445, DW_AT_location[DW_OP_regx 0x27]
$C$DW$1446	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$1446, DW_AT_location[DW_OP_regx 0x28]
$C$DW$1447	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$1447, DW_AT_location[DW_OP_reg27]
$C$DW$1448	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$1448, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

