;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue Feb 02 11:43:28 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../buffer.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\R_Module_48V_Standard\PTM078_7s")
_MeasBuffer:	.usect	".ebss",1050,1,1
$C$DW$1	.dwtag  DW_TAG_variable, DW_AT_name("MeasBuffer")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_MeasBuffer")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$1, DW_AT_location[DW_OP_addr _MeasBuffer]
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0891212 
	.sect	".text"
	.global	_BUF_Init

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("BUF_Init")
	.dwattr $C$DW$2, DW_AT_low_pc(_BUF_Init)
	.dwattr $C$DW$2, DW_AT_high_pc(0x00)
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_BUF_Init")
	.dwattr $C$DW$2, DW_AT_external
	.dwattr $C$DW$2, DW_AT_TI_begin_file("../buffer.c")
	.dwattr $C$DW$2, DW_AT_TI_begin_line(0x25)
	.dwattr $C$DW$2, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$2, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../buffer.c",line 38,column 1,is_stmt,address _BUF_Init

	.dwfde $C$DW$CIE, _BUF_Init
$C$DW$3	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$3, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _BUF_Init                     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_BUF_Init:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$4	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |38| 
	.dwpsn	file "../buffer.c",line 39,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |39| 
        MOVL      XAR4,#_MeasBuffer+64  ; [CPU_U] |39| 
        MPYB      ACC,T,#70             ; [CPU_] |39| 
        ADDL      XAR4,ACC              ; [CPU_] |39| 
        MOV       *+XAR4[0],#0          ; [CPU_] |39| 
	.dwpsn	file "../buffer.c",line 40,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |40| 
        MOVL      XAR4,#_MeasBuffer+65  ; [CPU_U] |40| 
        MPYB      ACC,T,#70             ; [CPU_] |40| 
        ADDL      XAR4,ACC              ; [CPU_] |40| 
        MOV       *+XAR4[0],#0          ; [CPU_] |40| 
	.dwpsn	file "../buffer.c",line 41,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |41| 
        MOVB      XAR6,#0               ; [CPU_] |41| 
        MOVL      XAR4,#_MeasBuffer+66  ; [CPU_U] |41| 
        MPYB      ACC,T,#70             ; [CPU_] |41| 
        ADDL      XAR4,ACC              ; [CPU_] |41| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |41| 
	.dwpsn	file "../buffer.c",line 42,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$5	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$5, DW_AT_low_pc(0x00)
	.dwattr $C$DW$5, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$2, DW_AT_TI_end_file("../buffer.c")
	.dwattr $C$DW$2, DW_AT_TI_end_line(0x2a)
	.dwattr $C$DW$2, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$2

	.sect	".text"
	.global	_BUF_Count

$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("BUF_Count")
	.dwattr $C$DW$6, DW_AT_low_pc(_BUF_Count)
	.dwattr $C$DW$6, DW_AT_high_pc(0x00)
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_BUF_Count")
	.dwattr $C$DW$6, DW_AT_external
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$6, DW_AT_TI_begin_file("../buffer.c")
	.dwattr $C$DW$6, DW_AT_TI_begin_line(0x2c)
	.dwattr $C$DW$6, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$6, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../buffer.c",line 44,column 30,is_stmt,address _BUF_Count

	.dwfde $C$DW$CIE, _BUF_Count
$C$DW$7	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _BUF_Count                    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_BUF_Count:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$8, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |44| 
	.dwpsn	file "../buffer.c",line 45,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |45| 
        MOVL      XAR4,#_MeasBuffer+64  ; [CPU_U] |45| 
        MPYB      ACC,T,#70             ; [CPU_] |45| 
        ADDL      XAR4,ACC              ; [CPU_] |45| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |45| 
	.dwpsn	file "../buffer.c",line 46,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$9	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$9, DW_AT_low_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$6, DW_AT_TI_end_file("../buffer.c")
	.dwattr $C$DW$6, DW_AT_TI_end_line(0x2e)
	.dwattr $C$DW$6, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$6

	.sect	".text"
	.global	_BUF_Write

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("BUF_Write")
	.dwattr $C$DW$10, DW_AT_low_pc(_BUF_Write)
	.dwattr $C$DW$10, DW_AT_high_pc(0x00)
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_BUF_Write")
	.dwattr $C$DW$10, DW_AT_external
	.dwattr $C$DW$10, DW_AT_TI_begin_file("../buffer.c")
	.dwattr $C$DW$10, DW_AT_TI_begin_line(0x31)
	.dwattr $C$DW$10, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$10, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../buffer.c",line 50,column 1,is_stmt,address _BUF_Write

	.dwfde $C$DW$CIE, _BUF_Write
$C$DW$11	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_reg0]
$C$DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("val")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_val")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _BUF_Write                    FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_BUF_Write:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_breg20 -1]
$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("val")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_val")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -2]
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -3]
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("index_n")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_index_n")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -4]
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -5]
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("sum")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_sum")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[2],AH            ; [CPU_] |50| 
        MOV       *-SP[1],AL            ; [CPU_] |50| 
	.dwpsn	file "../buffer.c",line 56,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |56| 
        MOVL      XAR7,#_MeasBuffer+64  ; [CPU_U] |56| 
        MPYB      ACC,T,#70             ; [CPU_] |56| 
        ADDL      XAR7,ACC              ; [CPU_] |56| 
        MOV       AL,*XAR7              ; [CPU_] |56| 
        MOV       *-SP[5],AL            ; [CPU_] |56| 
	.dwpsn	file "../buffer.c",line 57,column 3,is_stmt
        MOVL      XAR7,#_MeasBuffer+65  ; [CPU_U] |57| 
        MPYB      ACC,T,#70             ; [CPU_] |57| 
        ADDL      XAR7,ACC              ; [CPU_] |57| 
        MOV       AL,*XAR7              ; [CPU_] |57| 
        MOV       *-SP[3],AL            ; [CPU_] |57| 
	.dwpsn	file "../buffer.c",line 59,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |59| 
        ADD       AL,*-SP[3]            ; [CPU_] |59| 
        ANDB      AL,#0x3f              ; [CPU_] |59| 
        MOV       *-SP[4],AL            ; [CPU_] |59| 
	.dwpsn	file "../buffer.c",line 60,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |60| 
        CMPB      AL,#64                ; [CPU_] |60| 
        B         $C$L1,LO              ; [CPU_] |60| 
        ; branchcc occurs ; [] |60| 
	.dwpsn	file "../buffer.c",line 62,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |62| 
        ADDB      AL,#1                 ; [CPU_] |62| 
        ANDB      AL,#0x3f              ; [CPU_] |62| 
        MOV       *-SP[3],AL            ; [CPU_] |62| 
$C$L1:    
	.dwpsn	file "../buffer.c",line 64,column 3,is_stmt
        MPYB      ACC,T,#70             ; [CPU_] |64| 
        MOVL      XAR4,#_MeasBuffer+66  ; [CPU_U] |64| 
        ADDL      XAR4,ACC              ; [CPU_] |64| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |64| 
        MOVL      *-SP[8],ACC           ; [CPU_] |64| 
	.dwpsn	file "../buffer.c",line 65,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |65| 
        CMPB      AL,#64                ; [CPU_] |65| 
        B         $C$L2,LO              ; [CPU_] |65| 
        ; branchcc occurs ; [] |65| 
	.dwpsn	file "../buffer.c",line 66,column 5,is_stmt
        MOVZ      AR0,*-SP[4]           ; [CPU_] |66| 
        MPYB      ACC,T,#70             ; [CPU_] |66| 
        MOVL      XAR4,#_MeasBuffer     ; [CPU_U] |66| 
        ADDL      XAR4,ACC              ; [CPU_] |66| 
        MOVU      ACC,*+XAR4[AR0]       ; [CPU_] |66| 
        SUBL      *-SP[8],ACC           ; [CPU_] |66| 
$C$L2:    
	.dwpsn	file "../buffer.c",line 68,column 3,is_stmt
        MOVZ      AR0,*-SP[4]           ; [CPU_] |68| 
        MOVZ      AR6,*-SP[2]           ; [CPU_] |68| 
        MPYB      ACC,T,#70             ; [CPU_] |68| 
        MOVL      XAR4,#_MeasBuffer     ; [CPU_U] |68| 
        ADDL      XAR4,ACC              ; [CPU_] |68| 
        MOV       *+XAR4[AR0],AR6       ; [CPU_] |68| 
	.dwpsn	file "../buffer.c",line 69,column 3,is_stmt
        MOVU      ACC,*-SP[2]           ; [CPU_] |69| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |69| 
        MOVL      *-SP[8],ACC           ; [CPU_] |69| 
	.dwpsn	file "../buffer.c",line 70,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |70| 
        MOVL      XAR6,*-SP[8]          ; [CPU_] |70| 
        MOVL      XAR4,#_MeasBuffer+66  ; [CPU_U] |70| 
        MPYB      ACC,T,#70             ; [CPU_] |70| 
        ADDL      XAR4,ACC              ; [CPU_] |70| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |70| 
	.dwpsn	file "../buffer.c",line 71,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |71| 
        CMPB      AL,#64                ; [CPU_] |71| 
        B         $C$L3,HIS             ; [CPU_] |71| 
        ; branchcc occurs ; [] |71| 
	.dwpsn	file "../buffer.c",line 71,column 25,is_stmt
        INC       *-SP[5]               ; [CPU_] |71| 
$C$L3:    
	.dwpsn	file "../buffer.c",line 72,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |72| 
        MOVZ      AR6,*-SP[3]           ; [CPU_] |72| 
        MOVL      XAR4,#_MeasBuffer+65  ; [CPU_U] |72| 
        MPYB      ACC,T,#70             ; [CPU_] |72| 
        ADDL      XAR4,ACC              ; [CPU_] |72| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |72| 
	.dwpsn	file "../buffer.c",line 73,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |73| 
        MOVZ      AR6,*-SP[5]           ; [CPU_] |73| 
        MOVL      XAR4,#_MeasBuffer+64  ; [CPU_U] |73| 
        MPYB      ACC,T,#70             ; [CPU_] |73| 
        ADDL      XAR4,ACC              ; [CPU_] |73| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |73| 
	.dwpsn	file "../buffer.c",line 78,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |78| 
        MOVZ      AR6,*-SP[8]           ; [CPU_] |78| 
        MOVL      XAR4,#_MeasBuffer+68  ; [CPU_U] |78| 
        MPYB      ACC,T,#70             ; [CPU_] |78| 
        ADDL      XAR4,ACC              ; [CPU_] |78| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |78| 
	.dwpsn	file "../buffer.c",line 79,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$10, DW_AT_TI_end_file("../buffer.c")
	.dwattr $C$DW$10, DW_AT_TI_end_line(0x4f)
	.dwattr $C$DW$10, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$10

	.sect	".text"
	.global	_BUF_Sum

$C$DW$20	.dwtag  DW_TAG_subprogram, DW_AT_name("BUF_Sum")
	.dwattr $C$DW$20, DW_AT_low_pc(_BUF_Sum)
	.dwattr $C$DW$20, DW_AT_high_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_BUF_Sum")
	.dwattr $C$DW$20, DW_AT_external
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$20, DW_AT_TI_begin_file("../buffer.c")
	.dwattr $C$DW$20, DW_AT_TI_begin_line(0x51)
	.dwattr $C$DW$20, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$20, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../buffer.c",line 82,column 1,is_stmt,address _BUF_Sum

	.dwfde $C$DW$CIE, _BUF_Sum
$C$DW$21	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _BUF_Sum                      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_BUF_Sum:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |82| 
	.dwpsn	file "../buffer.c",line 83,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |83| 
        MOVL      XAR4,#_MeasBuffer+66  ; [CPU_U] |83| 
        MPYB      ACC,T,#70             ; [CPU_] |83| 
        ADDL      XAR4,ACC              ; [CPU_] |83| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |83| 
	.dwpsn	file "../buffer.c",line 84,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$20, DW_AT_TI_end_file("../buffer.c")
	.dwattr $C$DW$20, DW_AT_TI_end_line(0x54)
	.dwattr $C$DW$20, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$20

	.sect	".text"
	.global	_BUF_Mean

$C$DW$24	.dwtag  DW_TAG_subprogram, DW_AT_name("BUF_Mean")
	.dwattr $C$DW$24, DW_AT_low_pc(_BUF_Mean)
	.dwattr $C$DW$24, DW_AT_high_pc(0x00)
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_BUF_Mean")
	.dwattr $C$DW$24, DW_AT_external
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$24, DW_AT_TI_begin_file("../buffer.c")
	.dwattr $C$DW$24, DW_AT_TI_begin_line(0x56)
	.dwattr $C$DW$24, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$24, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../buffer.c",line 87,column 1,is_stmt,address _BUF_Mean

	.dwfde $C$DW$CIE, _BUF_Mean
$C$DW$25	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _BUF_Mean                     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_BUF_Mean:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -1]
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -2]
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[1],AL            ; [CPU_] |87| 
	.dwpsn	file "../buffer.c",line 90,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |90| 
        MOVL      XAR7,#_MeasBuffer+64  ; [CPU_U] |90| 
        MPYB      ACC,T,#70             ; [CPU_] |90| 
        ADDL      XAR7,ACC              ; [CPU_] |90| 
        MOV       AL,*XAR7              ; [CPU_] |90| 
        MOV       *-SP[2],AL            ; [CPU_] |90| 
	.dwpsn	file "../buffer.c",line 91,column 3,is_stmt
        BF        $C$L4,EQ              ; [CPU_] |91| 
        ; branchcc occurs ; [] |91| 
	.dwpsn	file "../buffer.c",line 91,column 12,is_stmt
        MPYB      ACC,T,#70             ; [CPU_] |91| 
        MOVL      XAR4,#_MeasBuffer+66  ; [CPU_U] |91| 
        MOVZ      AR6,*-SP[2]           ; [CPU_] |91| 
        ADDL      XAR4,ACC              ; [CPU_] |91| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |91| 
        MOVB      ACC,#0                ; [CPU_] |91| 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |91| 
        MOV       *-SP[3],P             ; [CPU_] |91| 
        B         $C$L5,UNC             ; [CPU_] |91| 
        ; branch occurs ; [] |91| 
$C$L4:    
	.dwpsn	file "../buffer.c",line 92,column 8,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |92| 
$C$L5:    
	.dwpsn	file "../buffer.c",line 93,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |93| 
	.dwpsn	file "../buffer.c",line 94,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$24, DW_AT_TI_end_file("../buffer.c")
	.dwattr $C$DW$24, DW_AT_TI_end_line(0x5e)
	.dwattr $C$DW$24, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$24

	.sect	".text"
	.global	_BUF_SumLast

$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("BUF_SumLast")
	.dwattr $C$DW$30, DW_AT_low_pc(_BUF_SumLast)
	.dwattr $C$DW$30, DW_AT_high_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_BUF_SumLast")
	.dwattr $C$DW$30, DW_AT_external
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$30, DW_AT_TI_begin_file("../buffer.c")
	.dwattr $C$DW$30, DW_AT_TI_begin_line(0x60)
	.dwattr $C$DW$30, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$30, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../buffer.c",line 96,column 51,is_stmt,address _BUF_SumLast

	.dwfde $C$DW$CIE, _BUF_SumLast
$C$DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg0]
$C$DW$32	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg1]
$C$DW$33	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filter")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_filter")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _BUF_SumLast                  FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_BUF_SumLast:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -1]
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -2]
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("filter")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_filter")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -3]
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -4]
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -5]
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("sum")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_sum")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -8]
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("min")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_min")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_breg20 -9]
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("max")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_max")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_breg20 -10]
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("meas")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_meas")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -11]
        MOV       *-SP[3],AR4           ; [CPU_] |96| 
        MOV       *-SP[2],AH            ; [CPU_] |96| 
        MOV       *-SP[1],AL            ; [CPU_] |96| 
	.dwpsn	file "../buffer.c",line 102,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |102| 
        BF        $C$L6,EQ              ; [CPU_] |102| 
        ; branchcc occurs ; [] |102| 
	.dwpsn	file "../buffer.c",line 102,column 16,is_stmt
        ADD       *-SP[2],#2            ; [CPU_] |102| 
$C$L6:    
	.dwpsn	file "../buffer.c",line 103,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |103| 
        MOVL      XAR4,#_MeasBuffer+65  ; [CPU_U] |103| 
        MOVL      XAR5,#_MeasBuffer+64  ; [CPU_U] |103| 
        MPYB      ACC,T,#70             ; [CPU_] |103| 
        ADDL      XAR4,ACC              ; [CPU_] |103| 
        MPYB      ACC,T,#70             ; [CPU_] |103| 
        ADDL      XAR5,ACC              ; [CPU_] |103| 
        MOV       AL,*+XAR5[0]          ; [CPU_] |103| 
        ADD       AL,*+XAR4[0]          ; [CPU_] |103| 
        SUB       AL,*-SP[2]            ; [CPU_] |103| 
        MOV       *-SP[4],AL            ; [CPU_] |103| 
	.dwpsn	file "../buffer.c",line 105,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |105| 
        MOVL      *-SP[8],ACC           ; [CPU_] |105| 
	.dwpsn	file "../buffer.c",line 106,column 8,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |106| 
        B         $C$L11,UNC            ; [CPU_] |106| 
        ; branch occurs ; [] |106| 
$C$L7:    
	.dwpsn	file "../buffer.c",line 107,column 5,is_stmt
        AND       *-SP[4],#0x003f       ; [CPU_] |107| 
	.dwpsn	file "../buffer.c",line 108,column 5,is_stmt
        MOVZ      AR0,*-SP[4]           ; [CPU_] |108| 
        MPYB      ACC,T,#70             ; [CPU_] |108| 
        MOVL      XAR4,#_MeasBuffer     ; [CPU_U] |108| 
        ADDL      XAR4,ACC              ; [CPU_] |108| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |108| 
        MOV       *-SP[11],AL           ; [CPU_] |108| 
	.dwpsn	file "../buffer.c",line 109,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |109| 
        BF        $C$L8,NEQ             ; [CPU_] |109| 
        ; branchcc occurs ; [] |109| 
	.dwpsn	file "../buffer.c",line 110,column 7,is_stmt
        MOV       AL,*-SP[11]           ; [CPU_] |110| 
        MOV       *-SP[9],AL            ; [CPU_] |110| 
	.dwpsn	file "../buffer.c",line 111,column 7,is_stmt
        MOV       *-SP[10],AL           ; [CPU_] |111| 
	.dwpsn	file "../buffer.c",line 112,column 5,is_stmt
        B         $C$L10,UNC            ; [CPU_] |112| 
        ; branch occurs ; [] |112| 
$C$L8:    
	.dwpsn	file "../buffer.c",line 114,column 7,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |114| 
        CMP       AL,*-SP[11]           ; [CPU_] |114| 
        B         $C$L9,LOS             ; [CPU_] |114| 
        ; branchcc occurs ; [] |114| 
	.dwpsn	file "../buffer.c",line 114,column 22,is_stmt
        MOV       AL,*-SP[11]           ; [CPU_] |114| 
        MOV       *-SP[9],AL            ; [CPU_] |114| 
	.dwpsn	file "../buffer.c",line 114,column 33,is_stmt
        B         $C$L10,UNC            ; [CPU_] |114| 
        ; branch occurs ; [] |114| 
$C$L9:    
	.dwpsn	file "../buffer.c",line 115,column 12,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |115| 
        CMP       AL,*-SP[11]           ; [CPU_] |115| 
        B         $C$L10,HIS            ; [CPU_] |115| 
        ; branchcc occurs ; [] |115| 
	.dwpsn	file "../buffer.c",line 115,column 27,is_stmt
        MOV       AL,*-SP[11]           ; [CPU_] |115| 
        MOV       *-SP[10],AL           ; [CPU_] |115| 
$C$L10:    
	.dwpsn	file "../buffer.c",line 117,column 5,is_stmt
        MPYB      ACC,T,#70             ; [CPU_] |117| 
        MOVL      XAR4,#_MeasBuffer     ; [CPU_U] |117| 
        ADDL      XAR4,ACC              ; [CPU_] |117| 
        MOVU      ACC,*+XAR4[AR0]       ; [CPU_] |117| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |117| 
        MOVL      *-SP[8],ACC           ; [CPU_] |117| 
	.dwpsn	file "../buffer.c",line 118,column 5,is_stmt
        INC       *-SP[4]               ; [CPU_] |118| 
	.dwpsn	file "../buffer.c",line 106,column 18,is_stmt
        INC       *-SP[5]               ; [CPU_] |106| 
$C$L11:    
	.dwpsn	file "../buffer.c",line 106,column 12,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |106| 
        CMP       AL,*-SP[5]            ; [CPU_] |106| 
        B         $C$L7,HI              ; [CPU_] |106| 
        ; branchcc occurs ; [] |106| 
	.dwpsn	file "../buffer.c",line 120,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |120| 
        BF        $C$L12,EQ             ; [CPU_] |120| 
        ; branchcc occurs ; [] |120| 
	.dwpsn	file "../buffer.c",line 121,column 5,is_stmt
        MOVU      ACC,*-SP[9]           ; [CPU_] |121| 
        SUBL      *-SP[8],ACC           ; [CPU_] |121| 
	.dwpsn	file "../buffer.c",line 122,column 5,is_stmt
        MOVU      ACC,*-SP[10]          ; [CPU_] |122| 
        SUBL      *-SP[8],ACC           ; [CPU_] |122| 
$C$L12:    
	.dwpsn	file "../buffer.c",line 124,column 3,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |124| 
	.dwpsn	file "../buffer.c",line 125,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$43	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$43, DW_AT_low_pc(0x00)
	.dwattr $C$DW$43, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$30, DW_AT_TI_end_file("../buffer.c")
	.dwattr $C$DW$30, DW_AT_TI_end_line(0x7d)
	.dwattr $C$DW$30, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$30

	.sect	".text"
	.global	_BUF_Delta

$C$DW$44	.dwtag  DW_TAG_subprogram, DW_AT_name("BUF_Delta")
	.dwattr $C$DW$44, DW_AT_low_pc(_BUF_Delta)
	.dwattr $C$DW$44, DW_AT_high_pc(0x00)
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_BUF_Delta")
	.dwattr $C$DW$44, DW_AT_external
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$44, DW_AT_TI_begin_file("../buffer.c")
	.dwattr $C$DW$44, DW_AT_TI_begin_line(0x7f)
	.dwattr $C$DW$44, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$44, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../buffer.c",line 127,column 36,is_stmt,address _BUF_Delta

	.dwfde $C$DW$CIE, _BUF_Delta
$C$DW$45	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg0]
$C$DW$46	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _BUF_Delta                    FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_BUF_Delta:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -1]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -2]
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg20 -3]
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("delta")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_delta")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[2],AH            ; [CPU_] |127| 
        MOV       *-SP[1],AL            ; [CPU_] |127| 
	.dwpsn	file "../buffer.c",line 131,column 3,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |131| 
        MOVL      XAR4,#_MeasBuffer+64  ; [CPU_U] |131| 
        MPYB      ACC,T,#70             ; [CPU_] |131| 
        ADDL      XAR4,ACC              ; [CPU_] |131| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |131| 
        CMP       AL,*-SP[2]            ; [CPU_] |131| 
        B         $C$L13,HIS            ; [CPU_] |131| 
        ; branchcc occurs ; [] |131| 
	.dwpsn	file "../buffer.c",line 132,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |132| 
        MOVL      *-SP[6],ACC           ; [CPU_] |132| 
$C$L13:    
	.dwpsn	file "../buffer.c",line 134,column 3,is_stmt
        MPYB      ACC,T,#70             ; [CPU_] |134| 
        MOVL      XAR4,#_MeasBuffer+65  ; [CPU_U] |134| 
        MOVL      XAR5,#_MeasBuffer+64  ; [CPU_U] |134| 
        ADDL      XAR4,ACC              ; [CPU_] |134| 
        MPYB      ACC,T,#70             ; [CPU_] |134| 
        ADDL      XAR5,ACC              ; [CPU_] |134| 
        MOV       AL,*+XAR5[0]          ; [CPU_] |134| 
        ADD       AL,*+XAR4[0]          ; [CPU_] |134| 
        ADDB      AL,#-1                ; [CPU_] |134| 
        MOV       *-SP[3],AL            ; [CPU_] |134| 
	.dwpsn	file "../buffer.c",line 136,column 3,is_stmt
        AND       *-SP[3],#0x003f       ; [CPU_] |136| 
	.dwpsn	file "../buffer.c",line 137,column 3,is_stmt
        MOVZ      AR0,*-SP[3]           ; [CPU_] |137| 
        MPYB      ACC,T,#70             ; [CPU_] |137| 
        MOVL      XAR4,#_MeasBuffer     ; [CPU_U] |137| 
        ADDL      XAR4,ACC              ; [CPU_] |137| 
        MOVU      ACC,*+XAR4[AR0]       ; [CPU_] |137| 
        MOVL      *-SP[6],ACC           ; [CPU_] |137| 
	.dwpsn	file "../buffer.c",line 139,column 3,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |139| 
        SUB       *-SP[3],AL            ; [CPU_] |139| 
	.dwpsn	file "../buffer.c",line 140,column 3,is_stmt
        AND       *-SP[3],#0x003f       ; [CPU_] |140| 
	.dwpsn	file "../buffer.c",line 141,column 3,is_stmt
        MOVZ      AR0,*-SP[3]           ; [CPU_] |141| 
        MPYB      ACC,T,#70             ; [CPU_] |141| 
        MOVL      XAR4,#_MeasBuffer     ; [CPU_U] |141| 
        ADDL      XAR4,ACC              ; [CPU_] |141| 
        MOVU      ACC,*+XAR4[AR0]       ; [CPU_] |141| 
        SUBL      *-SP[6],ACC           ; [CPU_] |141| 
	.dwpsn	file "../buffer.c",line 142,column 3,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |142| 
	.dwpsn	file "../buffer.c",line 143,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$51	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$51, DW_AT_low_pc(0x00)
	.dwattr $C$DW$51, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$44, DW_AT_TI_end_file("../buffer.c")
	.dwattr $C$DW$44, DW_AT_TI_end_line(0x8f)
	.dwattr $C$DW$44, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$44


;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x46)
$C$DW$52	.dwtag  DW_TAG_member
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$52, DW_AT_name("meas")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_meas")
	.dwattr $C$DW$52, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$52, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$53	.dwtag  DW_TAG_member
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$53, DW_AT_name("count")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$54	.dwtag  DW_TAG_member
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$54, DW_AT_name("index")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x41]
	.dwattr $C$DW$54, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$55	.dwtag  DW_TAG_member
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$55, DW_AT_name("sum")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_sum")
	.dwattr $C$DW$55, DW_AT_data_member_location[DW_OP_plus_uconst 0x42]
	.dwattr $C$DW$55, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$56	.dwtag  DW_TAG_member
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$56, DW_AT_name("mean")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_mean")
	.dwattr $C$DW$56, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$56, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$21	.dwtag  DW_TAG_typedef, DW_AT_name("TMeasBuffer")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x41a)
$C$DW$57	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$57, DW_AT_upper_bound(0x0e)
	.dwendtag $C$DW$T$22

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x40)
$C$DW$58	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$58, DW_AT_upper_bound(0x3f)
	.dwendtag $C$DW$T$19

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg0]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg1]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg2]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg3]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg20]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg21]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg22]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg23]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg24]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg25]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg26]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_reg28]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_reg29]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg30]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg31]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_regx 0x20]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_regx 0x21]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_regx 0x22]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_regx 0x23]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_regx 0x24]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_regx 0x25]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_regx 0x26]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg4]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg6]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg8]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg10]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg12]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg14]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg16]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg17]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg18]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg19]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_reg5]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_reg7]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_reg9]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg11]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg13]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_reg15]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x30]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x33]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x34]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x37]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x38]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x40]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x43]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x44]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_regx 0x47]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x48]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_regx 0x49]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x27]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x28]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg27]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

