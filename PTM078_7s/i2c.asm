;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue Feb 02 11:43:29 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../i2c.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\R_Module_48V_Standard\PTM078_7s")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_EepOddAdr+0,32
	.bits	0,16			; _EepOddAdr @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_EepState+0,32
	.bits	0,16			; _EepState @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("DSP28x_usDelay")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_DSP28x_usDelay")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$1

	.global	_EepOddAdr
_EepOddAdr:	.usect	".ebss",1,1,0
$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("EepOddAdr")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_EepOddAdr")
	.dwattr $C$DW$3, DW_AT_location[DW_OP_addr _EepOddAdr]
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$3, DW_AT_external

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$125)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$79)
	.dwendtag $C$DW$4


$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$120)
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$126)
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$79)
	.dwendtag $C$DW$7

	.global	_EepState
_EepState:	.usect	".ebss",1,1,0
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("EepState")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_EepState")
	.dwattr $C$DW$11, DW_AT_location[DW_OP_addr _EepState]
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$11, DW_AT_external

$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$120)
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$126)
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$79)
	.dwendtag $C$DW$12


$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("CLK_getltime")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_CLK_getltime")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("GpioDataRegs")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_GpioDataRegs")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("I2caRegs")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_I2caRegs")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("mailboxI2C_SendAndReceive")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_mailboxI2C_SendAndReceive")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("GpioCtrlRegs")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_GpioCtrlRegs")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0517212 
	.sect	".text"
	.global	_I2C_Init

$C$DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Init")
	.dwattr $C$DW$23, DW_AT_low_pc(_I2C_Init)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_I2C_Init")
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0x2a)
	.dwattr $C$DW$23, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../i2c.c",line 42,column 20,is_stmt,address _I2C_Init

	.dwfde $C$DW$CIE, _I2C_Init

;***************************************************************
;* FNAME: _I2C_Init                     FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_I2C_Init:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../i2c.c",line 44,column 3,is_stmt
        MOVW      DP,#_I2caRegs+9       ; [CPU_U] 
        MOV       @_I2caRegs+9,#0       ; [CPU_] |44| 
	.dwpsn	file "../i2c.c",line 45,column 3,is_stmt
        MOV       @_I2caRegs+32,#0      ; [CPU_] |45| 
	.dwpsn	file "../i2c.c",line 46,column 3,is_stmt
        MOV       @_I2caRegs+33,#0      ; [CPU_] |46| 
	.dwpsn	file "../i2c.c",line 47,column 3,is_stmt
        MOVB      @_I2caRegs+7,#80,UNC  ; [CPU_] |47| 
	.dwpsn	file "../i2c.c",line 48,column 3,is_stmt
        MOVB      @_I2caRegs+12,#3,UNC  ; [CPU_] |48| 
	.dwpsn	file "../i2c.c",line 49,column 3,is_stmt
        MOVB      @_I2caRegs+3,#55,UNC  ; [CPU_] |49| 
	.dwpsn	file "../i2c.c",line 50,column 3,is_stmt
        MOVB      @_I2caRegs+4,#50,UNC  ; [CPU_] |50| 
	.dwpsn	file "../i2c.c",line 51,column 3,is_stmt
        MOV       @_I2caRegs+1,#0       ; [CPU_] |51| 
	.dwpsn	file "../i2c.c",line 52,column 3,is_stmt
        MOVB      @_I2caRegs+9,#32,UNC  ; [CPU_] |52| 
	.dwpsn	file "../i2c.c",line 54,column 3,is_stmt
        MOV       @_I2caRegs+32,#24576  ; [CPU_] |54| 
	.dwpsn	file "../i2c.c",line 55,column 3,is_stmt
        MOV       @_I2caRegs+33,#8256   ; [CPU_] |55| 
	.dwpsn	file "../i2c.c",line 56,column 1,is_stmt
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$23, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0x38)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$23

	.sect	".text:retain"
	.global	_I2C_INT1A

$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_INT1A")
	.dwattr $C$DW$25, DW_AT_low_pc(_I2C_INT1A)
	.dwattr $C$DW$25, DW_AT_high_pc(0x00)
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_I2C_INT1A")
	.dwattr $C$DW$25, DW_AT_external
	.dwattr $C$DW$25, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$25, DW_AT_TI_begin_line(0x3b)
	.dwattr $C$DW$25, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$25, DW_AT_TI_interrupt
	.dwattr $C$DW$25, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../i2c.c",line 60,column 1,is_stmt,address _I2C_INT1A

	.dwfde $C$DW$CIE, _I2C_INT1A

;***************************************************************
;* FNAME: _I2C_INT1A                    FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  4 SOE     *
;***************************************************************

_I2C_INT1A:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 4
	.dwcfi	save_reg_to_mem, 40, 5
	.dwcfi	cfa_offset, -6
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("IntSource")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_IntSource")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -1]
	.dwpsn	file "../i2c.c",line 64,column 3,is_stmt
        MOVW      DP,#_I2caRegs+10      ; [CPU_U] 
        MOV       AL,@_I2caRegs+10      ; [CPU_] |64| 
        MOV       *-SP[1],AL            ; [CPU_] |64| 
	.dwpsn	file "../i2c.c",line 66,column 3,is_stmt
        CMPB      AL,#6                 ; [CPU_] |66| 
        BF        $C$L1,NEQ             ; [CPU_] |66| 
        ; branchcc occurs ; [] |66| 
	.dwpsn	file "../i2c.c",line 68,column 5,is_stmt
        MOVW      DP,#_EepState         ; [CPU_U] 
        MOV       AL,@_EepState         ; [CPU_] |68| 
        CMPB      AL,#1                 ; [CPU_] |68| 
        BF        $C$L1,NEQ             ; [CPU_] |68| 
        ; branchcc occurs ; [] |68| 
	.dwpsn	file "../i2c.c",line 70,column 7,is_stmt
        MOVB      @_EepState,#17,UNC    ; [CPU_] |70| 
$C$L1:    
	.dwpsn	file "../i2c.c",line 92,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        MOVB      @_PieCtrlRegs+1,#128,UNC ; [CPU_] |92| 
	.dwpsn	file "../i2c.c",line 93,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_TI_return
        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$25, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$25, DW_AT_TI_end_line(0x5d)
	.dwattr $C$DW$25, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$25

	.sect	".text"
	.global	_EepSetup

$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("EepSetup")
	.dwattr $C$DW$28, DW_AT_low_pc(_EepSetup)
	.dwattr $C$DW$28, DW_AT_high_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_EepSetup")
	.dwattr $C$DW$28, DW_AT_external
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$28, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$28, DW_AT_TI_begin_line(0x5f)
	.dwattr $C$DW$28, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$28, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../i2c.c",line 96,column 1,is_stmt,address _EepSetup

	.dwfde $C$DW$CIE, _EepSetup
$C$DW$29	.dwtag  DW_TAG_formal_parameter, DW_AT_name("device_code")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg0]
$C$DW$30	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg1]
$C$DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg12]
$C$DW$32	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address_size")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_address_size")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _EepSetup                     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_EepSetup:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("device_code")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -1]
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -2]
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -3]
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("address_size")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_address_size")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |96| 
        MOV       *-SP[3],AR4           ; [CPU_] |96| 
        MOV       *-SP[2],AH            ; [CPU_] |96| 
        MOV       *-SP[1],AL            ; [CPU_] |96| 
	.dwpsn	file "../i2c.c",line 101,column 3,is_stmt
        MOVW      DP,#_EepState         ; [CPU_U] 
        MOV       AL,@_EepState         ; [CPU_] |101| 
        BF        $C$L2,EQ              ; [CPU_] |101| 
        ; branchcc occurs ; [] |101| 
	.dwpsn	file "../i2c.c",line 102,column 5,is_stmt
 EALLOW
	.dwpsn	file "../i2c.c",line 103,column 5,is_stmt
        MOVW      DP,#_I2caRegs+5       ; [CPU_U] 
        MOV       @_I2caRegs+5,#0       ; [CPU_] |103| 
	.dwpsn	file "../i2c.c",line 104,column 5,is_stmt
        MOV       @_I2caRegs+9,#0       ; [CPU_] |104| 
	.dwpsn	file "../i2c.c",line 105,column 5,is_stmt
        AND       @_I2caRegs+32,#0xdfff ; [CPU_] |105| 
	.dwpsn	file "../i2c.c",line 106,column 5,is_stmt
        AND       @_I2caRegs+33,#0xdfff ; [CPU_] |106| 
	.dwpsn	file "../i2c.c",line 107,column 5,is_stmt
        MOVW      DP,#_GpioCtrlRegs+22  ; [CPU_U] 
        AND       @_GpioCtrlRegs+22,#0xfff3 ; [CPU_] |107| 
	.dwpsn	file "../i2c.c",line 108,column 5,is_stmt
        MOVW      DP,#_GpioDataRegs+8   ; [CPU_U] 
        AND       @_GpioDataRegs+8,#0xfffd ; [CPU_] |108| 
	.dwpsn	file "../i2c.c",line 109,column 5,is_stmt
 EDIS
	.dwpsn	file "../i2c.c",line 110,column 5,is_stmt
        SPM       #0                    ; [CPU_] 
        MOV       ACC,#798              ; [CPU_] |110| 
$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x00)
	.dwattr $C$DW$37, DW_AT_name("_DSP28x_usDelay")
	.dwattr $C$DW$37, DW_AT_TI_call
        LCR       #_DSP28x_usDelay      ; [CPU_] |110| 
        ; call occurs [#_DSP28x_usDelay] ; [] |110| 
	.dwpsn	file "../i2c.c",line 111,column 5,is_stmt
        MOVW      DP,#_GpioDataRegs+8   ; [CPU_U] 
        OR        @_GpioDataRegs+8,#0x0002 ; [CPU_] |111| 
	.dwpsn	file "../i2c.c",line 112,column 5,is_stmt
        MOV       ACC,#798              ; [CPU_] |112| 
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("_DSP28x_usDelay")
	.dwattr $C$DW$38, DW_AT_TI_call
        LCR       #_DSP28x_usDelay      ; [CPU_] |112| 
        ; call occurs [#_DSP28x_usDelay] ; [] |112| 
	.dwpsn	file "../i2c.c",line 113,column 5,is_stmt
 EALLOW
	.dwpsn	file "../i2c.c",line 114,column 5,is_stmt
        MOVW      DP,#_GpioCtrlRegs+22  ; [CPU_U] 
        AND       AL,@_GpioCtrlRegs+22,#0xfff3 ; [CPU_] |114| 
        ORB       AL,#0x04              ; [CPU_] |114| 
        MOV       @_GpioCtrlRegs+22,AL  ; [CPU_] |114| 
	.dwpsn	file "../i2c.c",line 115,column 5,is_stmt
 EDIS
	.dwpsn	file "../i2c.c",line 117,column 5,is_stmt
        MOVW      DP,#_I2caRegs+9       ; [CPU_U] 
        OR        @_I2caRegs+9,#0x0020  ; [CPU_] |117| 
	.dwpsn	file "../i2c.c",line 118,column 5,is_stmt
        OR        @_I2caRegs+32,#0x2000 ; [CPU_] |118| 
	.dwpsn	file "../i2c.c",line 119,column 5,is_stmt
        OR        @_I2caRegs+33,#0x2000 ; [CPU_] |119| 
	.dwpsn	file "../i2c.c",line 120,column 5,is_stmt
        AND       @_I2caRegs+9,#0xf7ff  ; [CPU_] |120| 
$C$L2:    
	.dwpsn	file "../i2c.c",line 122,column 3,is_stmt
        MOVW      DP,#_I2caRegs+9       ; [CPU_U] 
        AND       AL,@_I2caRegs+9,#0x0800 ; [CPU_] |122| 
        LSR       AL,11                 ; [CPU_] |122| 
        CMPB      AL,#1                 ; [CPU_] |122| 
        BF        $C$L3,NEQ             ; [CPU_] |122| 
        ; branchcc occurs ; [] |122| 
	.dwpsn	file "../i2c.c",line 124,column 5,is_stmt
        AND       @_I2caRegs+9,#0xf7ff  ; [CPU_] |124| 
	.dwpsn	file "../i2c.c",line 125,column 5,is_stmt
        MOV       AL,#21845             ; [CPU_] |125| 
        B         $C$L9,UNC             ; [CPU_] |125| 
        ; branch occurs ; [] |125| 
$C$L3:    
	.dwpsn	file "../i2c.c",line 127,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |127| 
        ADD       AL,*-SP[2]            ; [CPU_] |127| 
        CMP       AL,#8192              ; [CPU_] |127| 
        B         $C$L4,LOS             ; [CPU_] |127| 
        ; branchcc occurs ; [] |127| 
	.dwpsn	file "../i2c.c",line 127,column 33,is_stmt
        MOV       AL,#65535             ; [CPU_] |127| 
        B         $C$L9,UNC             ; [CPU_] |127| 
        ; branch occurs ; [] |127| 
$C$L4:    
	.dwpsn	file "../i2c.c",line 133,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |133| 
        CMPB      AL,#2                 ; [CPU_] |133| 
        BF        $C$L5,NEQ             ; [CPU_] |133| 
        ; branchcc occurs ; [] |133| 
	.dwpsn	file "../i2c.c",line 134,column 5,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |134| 
        LSR       AL,1                  ; [CPU_] |134| 
        MOV       @_I2caRegs+7,AL       ; [CPU_] |134| 
	.dwpsn	file "../i2c.c",line 135,column 3,is_stmt
        B         $C$L6,UNC             ; [CPU_] |135| 
        ; branch occurs ; [] |135| 
$C$L5:    
	.dwpsn	file "../i2c.c",line 137,column 5,is_stmt
        MOV       AH,*-SP[1]            ; [CPU_] |137| 
        AND       AL,*-SP[2],#0x0300    ; [CPU_] |137| 
        LSR       AH,1                  ; [CPU_] |137| 
        LSR       AL,8                  ; [CPU_] |137| 
        OR        AL,AH                 ; [CPU_] |137| 
        MOV       @_I2caRegs+7,AL       ; [CPU_] |137| 
$C$L6:    
	.dwpsn	file "../i2c.c",line 139,column 3,is_stmt
        AND       AL,@_I2caRegs+2,#0x1000 ; [CPU_] |139| 
        LSR       AL,12                 ; [CPU_] |139| 
        CMPB      AL,#1                 ; [CPU_] |139| 
        BF        $C$L7,NEQ             ; [CPU_] |139| 
        ; branchcc occurs ; [] |139| 
	.dwpsn	file "../i2c.c",line 141,column 6,is_stmt
        MOV       AL,#4096              ; [CPU_] |141| 
        B         $C$L9,UNC             ; [CPU_] |141| 
        ; branch occurs ; [] |141| 
$C$L7:    
	.dwpsn	file "../i2c.c",line 145,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |145| 
        ADD       AL,*-SP[3]            ; [CPU_] |145| 
        MOV       @_I2caRegs+5,AL       ; [CPU_] |145| 
	.dwpsn	file "../i2c.c",line 148,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |148| 
        CMPB      AL,#2                 ; [CPU_] |148| 
        BF        $C$L8,NEQ             ; [CPU_] |148| 
        ; branchcc occurs ; [] |148| 
	.dwpsn	file "../i2c.c",line 149,column 5,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |149| 
        LSR       AL,8                  ; [CPU_] |149| 
        MOV       @_I2caRegs+8,AL       ; [CPU_] |149| 
$C$L8:    
	.dwpsn	file "../i2c.c",line 151,column 3,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |151| 
        ANDB      AL,#0xff              ; [CPU_] |151| 
        MOV       @_I2caRegs+8,AL       ; [CPU_] |151| 
	.dwpsn	file "../i2c.c",line 152,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |152| 
$C$L9:    
	.dwpsn	file "../i2c.c",line 153,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
        SPM       #0                    ; [CPU_] 
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$28, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$28, DW_AT_TI_end_line(0x99)
	.dwattr $C$DW$28, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$28

	.sect	".text"
	.global	_I2C_EepRead

$C$DW$40	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_EepRead")
	.dwattr $C$DW$40, DW_AT_low_pc(_I2C_EepRead)
	.dwattr $C$DW$40, DW_AT_high_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_I2C_EepRead")
	.dwattr $C$DW$40, DW_AT_external
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$40, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$40, DW_AT_TI_begin_line(0x9e)
	.dwattr $C$DW$40, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$40, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../i2c.c",line 159,column 1,is_stmt,address _I2C_EepRead

	.dwfde $C$DW$CIE, _I2C_EepRead
$C$DW$41	.dwtag  DW_TAG_formal_parameter, DW_AT_name("device_code")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg0]
$C$DW$42	.dwtag  DW_TAG_formal_parameter, DW_AT_name("buffer")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg12]
$C$DW$43	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg1]
$C$DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _I2C_EepRead                  FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 13 Auto,  0 SOE     *
;***************************************************************

_I2C_EepRead:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("device_code")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_breg20 -1]
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -4]
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -5]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -6]
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg20 -7]
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("ftime")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_ftime")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_breg20 -10]
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("sample_time")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_sample_time")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -12]
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("wait_nb")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_wait_nb")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg20 -13]
        MOV       *-SP[6],AR5           ; [CPU_] |159| 
        MOV       *-SP[5],AH            ; [CPU_] |159| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |159| 
        MOV       *-SP[1],AL            ; [CPU_] |159| 
	.dwpsn	file "../i2c.c",line 163,column 3,is_stmt
        MOV       AH,*-SP[6]            ; [CPU_] |163| 
        MOVB      XAR5,#2               ; [CPU_] |163| 
        MOVB      XAR4,#0               ; [CPU_] |163| 
$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("_EepSetup")
	.dwattr $C$DW$53, DW_AT_TI_call
        LCR       #_EepSetup            ; [CPU_] |163| 
        ; call occurs [#_EepSetup] ; [] |163| 
        MOV       *-SP[7],AL            ; [CPU_] |163| 
	.dwpsn	file "../i2c.c",line 164,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |164| 
        BF        $C$L12,NEQ            ; [CPU_] |164| 
        ; branchcc occurs ; [] |164| 
	.dwpsn	file "../i2c.c",line 167,column 5,is_stmt
        MOV       @_I2caRegs+9,#26144   ; [CPU_] |167| 
	.dwpsn	file "../i2c.c",line 170,column 5,is_stmt
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$54, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |170| 
        ; call occurs [#_CLK_getltime] ; [] |170| 
        MOVL      *-SP[10],ACC          ; [CPU_] |170| 
$C$L10:    
	.dwpsn	file "../i2c.c",line 172,column 7,is_stmt
$C$DW$55	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$55, DW_AT_low_pc(0x00)
	.dwattr $C$DW$55, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$55, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |172| 
        ; call occurs [#_CLK_getltime] ; [] |172| 
        MOVL      *-SP[12],ACC          ; [CPU_] |172| 
	.dwpsn	file "../i2c.c",line 173,column 7,is_stmt
        MOVW      DP,#_I2caRegs+2       ; [CPU_U] 
        AND       AL,@_I2caRegs+2,#0x0004 ; [CPU_] |173| 
        LSR       AL,2                  ; [CPU_] |173| 
        MOV       *-SP[7],AL            ; [CPU_] |173| 
	.dwpsn	file "../i2c.c",line 174,column 7,is_stmt
        BF        $C$L11,NEQ            ; [CPU_] |174| 
        ; branchcc occurs ; [] |174| 
	.dwpsn	file "../i2c.c",line 175,column 9,is_stmt
        MOVB      XAR6,#10              ; [CPU_] |175| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |175| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |175| 
        CMPL      ACC,XAR6              ; [CPU_] |175| 
        B         $C$L11,LOS            ; [CPU_] |175| 
        ; branchcc occurs ; [] |175| 
	.dwpsn	file "../i2c.c",line 176,column 11,is_stmt
        MOV       *-SP[7],#65535        ; [CPU_] |176| 
$C$L11:    
	.dwpsn	file "../i2c.c",line 179,column 13,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |179| 
        BF        $C$L10,EQ             ; [CPU_] |179| 
        ; branchcc occurs ; [] |179| 
$C$L12:    
	.dwpsn	file "../i2c.c",line 181,column 3,is_stmt
        CMPB      AL,#1                 ; [CPU_] |181| 
        BF        $C$L20,NEQ            ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
	.dwpsn	file "../i2c.c",line 183,column 5,is_stmt
        OR        @_I2caRegs+2,#0x0004  ; [CPU_] |183| 
	.dwpsn	file "../i2c.c",line 184,column 5,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |184| 
	.dwpsn	file "../i2c.c",line 185,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |185| 
        ADD       AL,*-SP[6]            ; [CPU_] |185| 
        CMP       AL,#8192              ; [CPU_] |185| 
        B         $C$L13,LOS            ; [CPU_] |185| 
        ; branchcc occurs ; [] |185| 
	.dwpsn	file "../i2c.c",line 185,column 35,is_stmt
        MOV       AL,#65535             ; [CPU_] |185| 
        B         $C$L21,UNC            ; [CPU_] |185| 
        ; branch occurs ; [] |185| 
$C$L13:    
	.dwpsn	file "../i2c.c",line 186,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |186| 
        MOV       @_I2caRegs+5,AL       ; [CPU_] |186| 
	.dwpsn	file "../i2c.c",line 188,column 5,is_stmt
        MOV       @_I2caRegs+9,#27680   ; [CPU_] |188| 
	.dwpsn	file "../i2c.c",line 189,column 5,is_stmt
$C$DW$56	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$56, DW_AT_low_pc(0x00)
	.dwattr $C$DW$56, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$56, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |189| 
        ; call occurs [#_CLK_getltime] ; [] |189| 
        MOVL      *-SP[10],ACC          ; [CPU_] |189| 
	.dwpsn	file "../i2c.c",line 190,column 12,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |190| 
        BF        $C$L20,EQ             ; [CPU_] |190| 
        ; branchcc occurs ; [] |190| 
$C$L14:    
	.dwpsn	file "../i2c.c",line 192,column 7,is_stmt
        CMPB      AL,#2                 ; [CPU_] |192| 
        B         $C$L15,LO             ; [CPU_] |192| 
        ; branchcc occurs ; [] |192| 
	.dwpsn	file "../i2c.c",line 192,column 20,is_stmt
        MOVB      *-SP[13],#2,UNC       ; [CPU_] |192| 
        B         $C$L16,UNC            ; [CPU_] |192| 
        ; branch occurs ; [] |192| 
$C$L15:    
	.dwpsn	file "../i2c.c",line 192,column 38,is_stmt
        MOV       *-SP[13],AL           ; [CPU_] |192| 
$C$L16:    
	.dwpsn	file "../i2c.c",line 193,column 7,is_stmt
        MOVW      DP,#_I2caRegs+33      ; [CPU_U] 
        AND       AL,@_I2caRegs+33,#0x1f00 ; [CPU_] |193| 
        LSR       AL,8                  ; [CPU_] |193| 
        CMP       AL,*-SP[13]           ; [CPU_] |193| 
        B         $C$L18,LO             ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
	.dwpsn	file "../i2c.c",line 195,column 7,is_stmt
$C$DW$57	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$57, DW_AT_low_pc(0x00)
	.dwattr $C$DW$57, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$57, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |195| 
        ; call occurs [#_CLK_getltime] ; [] |195| 
        MOVL      *-SP[10],ACC          ; [CPU_] |195| 
	.dwpsn	file "../i2c.c",line 196,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |196| 
        MOVW      DP,#_I2caRegs+6       ; [CPU_U] 
        MOV       AL,@_I2caRegs+6       ; [CPU_] |196| 
        MOV       *+XAR4[0],AL          ; [CPU_] |196| 
	.dwpsn	file "../i2c.c",line 197,column 9,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |197| 
        CMPB      AL,#1                 ; [CPU_] |197| 
        B         $C$L17,LOS            ; [CPU_] |197| 
        ; branchcc occurs ; [] |197| 
	.dwpsn	file "../i2c.c",line 197,column 24,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |197| 
        MOV       ACC,@_I2caRegs+6 << #8 ; [CPU_] |197| 
        ADD       *+XAR4[0],AL          ; [CPU_] |197| 
$C$L17:    
	.dwpsn	file "../i2c.c",line 198,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |198| 
        ADDL      *-SP[4],ACC           ; [CPU_] |198| 
	.dwpsn	file "../i2c.c",line 199,column 9,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |199| 
        SUB       *-SP[5],AL            ; [CPU_] |199| 
$C$L18:    
	.dwpsn	file "../i2c.c",line 201,column 7,is_stmt
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$58, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |201| 
        ; call occurs [#_CLK_getltime] ; [] |201| 
        MOVB      XAR6,#10              ; [CPU_] |201| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |201| 
        CMPL      ACC,XAR6              ; [CPU_] |201| 
        B         $C$L19,LOS            ; [CPU_] |201| 
        ; branchcc occurs ; [] |201| 
	.dwpsn	file "../i2c.c",line 202,column 8,is_stmt
        MOV       *-SP[7],#65535        ; [CPU_] |202| 
	.dwpsn	file "../i2c.c",line 202,column 25,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |202| 
$C$L19:    
	.dwpsn	file "../i2c.c",line 190,column 12,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |190| 
        BF        $C$L14,NEQ            ; [CPU_] |190| 
        ; branchcc occurs ; [] |190| 
$C$L20:    
	.dwpsn	file "../i2c.c",line 205,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |205| 
        MOVW      DP,#_EepState         ; [CPU_U] 
        MOV       @_EepState,AL         ; [CPU_] |205| 
	.dwpsn	file "../i2c.c",line 206,column 3,is_stmt
        MOV       AH,*-SP[7]            ; [CPU_] |206| 
        MOVB      AL,#0                 ; [CPU_] |206| 
        CMPB      AH,#0                 ; [CPU_] |206| 
        BF        $C$L21,NEQ            ; [CPU_] |206| 
        ; branchcc occurs ; [] |206| 
        MOVB      AL,#1                 ; [CPU_] |206| 
$C$L21:    
	.dwpsn	file "../i2c.c",line 207,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$59	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$59, DW_AT_low_pc(0x00)
	.dwattr $C$DW$59, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$40, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$40, DW_AT_TI_end_line(0xcf)
	.dwattr $C$DW$40, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$40

	.sect	".text"
	.global	_I2C_AdsRead

$C$DW$60	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_AdsRead")
	.dwattr $C$DW$60, DW_AT_low_pc(_I2C_AdsRead)
	.dwattr $C$DW$60, DW_AT_high_pc(0x00)
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_I2C_AdsRead")
	.dwattr $C$DW$60, DW_AT_external
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$60, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$60, DW_AT_TI_begin_line(0xd2)
	.dwattr $C$DW$60, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$60, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../i2c.c",line 211,column 1,is_stmt,address _I2C_AdsRead

	.dwfde $C$DW$CIE, _I2C_AdsRead
$C$DW$61	.dwtag  DW_TAG_formal_parameter, DW_AT_name("device_code")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg0]
$C$DW$62	.dwtag  DW_TAG_formal_parameter, DW_AT_name("buffer")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg12]
$C$DW$63	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg1]
$C$DW$64	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _I2C_AdsRead                  FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 13 Auto,  0 SOE     *
;***************************************************************

_I2C_AdsRead:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("device_code")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_breg20 -1]
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_breg20 -4]
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_breg20 -5]
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_breg20 -6]
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_breg20 -7]
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("ftime")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_ftime")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -10]
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("sample_time")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_sample_time")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_breg20 -12]
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("wait_nb")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_wait_nb")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_breg20 -13]
        MOV       *-SP[6],AR5           ; [CPU_] |211| 
        MOV       *-SP[5],AH            ; [CPU_] |211| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |211| 
        MOV       *-SP[1],AL            ; [CPU_] |211| 
	.dwpsn	file "../i2c.c",line 215,column 3,is_stmt
        MOV       AH,*-SP[6]            ; [CPU_] |215| 
        MOVB      XAR5,#1               ; [CPU_] |215| 
        MOVB      XAR4,#0               ; [CPU_] |215| 
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_name("_EepSetup")
	.dwattr $C$DW$73, DW_AT_TI_call
        LCR       #_EepSetup            ; [CPU_] |215| 
        ; call occurs [#_EepSetup] ; [] |215| 
        MOV       *-SP[7],AL            ; [CPU_] |215| 
	.dwpsn	file "../i2c.c",line 217,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |217| 
        BF        $C$L24,NEQ            ; [CPU_] |217| 
        ; branchcc occurs ; [] |217| 
	.dwpsn	file "../i2c.c",line 220,column 5,is_stmt
        MOV       @_I2caRegs+9,#26144   ; [CPU_] |220| 
	.dwpsn	file "../i2c.c",line 224,column 5,is_stmt
$C$DW$74	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$74, DW_AT_low_pc(0x00)
	.dwattr $C$DW$74, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$74, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |224| 
        ; call occurs [#_CLK_getltime] ; [] |224| 
        MOVL      *-SP[10],ACC          ; [CPU_] |224| 
$C$L22:    
	.dwpsn	file "../i2c.c",line 226,column 7,is_stmt
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$75, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |226| 
        ; call occurs [#_CLK_getltime] ; [] |226| 
        MOVL      *-SP[12],ACC          ; [CPU_] |226| 
	.dwpsn	file "../i2c.c",line 227,column 7,is_stmt
        MOVW      DP,#_I2caRegs+2       ; [CPU_U] 
        AND       AL,@_I2caRegs+2,#0x0004 ; [CPU_] |227| 
        LSR       AL,2                  ; [CPU_] |227| 
        MOV       *-SP[7],AL            ; [CPU_] |227| 
	.dwpsn	file "../i2c.c",line 228,column 7,is_stmt
        BF        $C$L23,NEQ            ; [CPU_] |228| 
        ; branchcc occurs ; [] |228| 
	.dwpsn	file "../i2c.c",line 229,column 9,is_stmt
        MOVB      XAR6,#2               ; [CPU_] |229| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |229| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |229| 
        CMPL      ACC,XAR6              ; [CPU_] |229| 
        B         $C$L23,LOS            ; [CPU_] |229| 
        ; branchcc occurs ; [] |229| 
	.dwpsn	file "../i2c.c",line 230,column 11,is_stmt
        MOV       *-SP[7],#65535        ; [CPU_] |230| 
$C$L23:    
	.dwpsn	file "../i2c.c",line 233,column 12,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |233| 
        BF        $C$L22,EQ             ; [CPU_] |233| 
        ; branchcc occurs ; [] |233| 
$C$L24:    
	.dwpsn	file "../i2c.c",line 236,column 3,is_stmt
        CMPB      AL,#1                 ; [CPU_] |236| 
        BF        $C$L31,NEQ            ; [CPU_] |236| 
        ; branchcc occurs ; [] |236| 
	.dwpsn	file "../i2c.c",line 238,column 5,is_stmt
        OR        @_I2caRegs+2,#0x0004  ; [CPU_] |238| 
	.dwpsn	file "../i2c.c",line 239,column 5,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |239| 
	.dwpsn	file "../i2c.c",line 240,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |240| 
        MOV       @_I2caRegs+5,AL       ; [CPU_] |240| 
	.dwpsn	file "../i2c.c",line 242,column 5,is_stmt
        MOV       @_I2caRegs+9,#27680   ; [CPU_] |242| 
	.dwpsn	file "../i2c.c",line 243,column 5,is_stmt
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$76, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |243| 
        ; call occurs [#_CLK_getltime] ; [] |243| 
        MOVL      *-SP[10],ACC          ; [CPU_] |243| 
	.dwpsn	file "../i2c.c",line 244,column 12,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |244| 
        BF        $C$L31,EQ             ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
$C$L25:    
	.dwpsn	file "../i2c.c",line 246,column 7,is_stmt
        CMPB      AL,#2                 ; [CPU_] |246| 
        B         $C$L26,LO             ; [CPU_] |246| 
        ; branchcc occurs ; [] |246| 
	.dwpsn	file "../i2c.c",line 246,column 20,is_stmt
        MOVB      *-SP[13],#2,UNC       ; [CPU_] |246| 
        B         $C$L27,UNC            ; [CPU_] |246| 
        ; branch occurs ; [] |246| 
$C$L26:    
	.dwpsn	file "../i2c.c",line 246,column 38,is_stmt
        MOV       *-SP[13],AL           ; [CPU_] |246| 
$C$L27:    
	.dwpsn	file "../i2c.c",line 247,column 7,is_stmt
        MOVW      DP,#_I2caRegs+33      ; [CPU_U] 
        AND       AL,@_I2caRegs+33,#0x1f00 ; [CPU_] |247| 
        LSR       AL,8                  ; [CPU_] |247| 
        CMP       AL,*-SP[13]           ; [CPU_] |247| 
        B         $C$L29,LO             ; [CPU_] |247| 
        ; branchcc occurs ; [] |247| 
	.dwpsn	file "../i2c.c",line 249,column 9,is_stmt
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$77, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |249| 
        ; call occurs [#_CLK_getltime] ; [] |249| 
        MOVL      *-SP[10],ACC          ; [CPU_] |249| 
	.dwpsn	file "../i2c.c",line 250,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |250| 
        MOVW      DP,#_I2caRegs+6       ; [CPU_U] 
        MOV       AL,@_I2caRegs+6       ; [CPU_] |250| 
        MOV       *+XAR4[0],AL          ; [CPU_] |250| 
	.dwpsn	file "../i2c.c",line 251,column 9,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |251| 
        CMPB      AL,#1                 ; [CPU_] |251| 
        B         $C$L28,LOS            ; [CPU_] |251| 
        ; branchcc occurs ; [] |251| 
	.dwpsn	file "../i2c.c",line 251,column 24,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |251| 
        MOV       ACC,@_I2caRegs+6 << #8 ; [CPU_] |251| 
        ADD       *+XAR4[0],AL          ; [CPU_] |251| 
$C$L28:    
	.dwpsn	file "../i2c.c",line 252,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |252| 
        ADDL      *-SP[4],ACC           ; [CPU_] |252| 
	.dwpsn	file "../i2c.c",line 253,column 9,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |253| 
        SUB       *-SP[5],AL            ; [CPU_] |253| 
$C$L29:    
	.dwpsn	file "../i2c.c",line 255,column 7,is_stmt
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$78, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |255| 
        ; call occurs [#_CLK_getltime] ; [] |255| 
        MOVB      XAR6,#2               ; [CPU_] |255| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |255| 
        CMPL      ACC,XAR6              ; [CPU_] |255| 
        B         $C$L30,LOS            ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
	.dwpsn	file "../i2c.c",line 256,column 9,is_stmt
        MOV       *-SP[7],#65535        ; [CPU_] |256| 
	.dwpsn	file "../i2c.c",line 256,column 26,is_stmt
        B         $C$L31,UNC            ; [CPU_] |256| 
        ; branch occurs ; [] |256| 
$C$L30:    
	.dwpsn	file "../i2c.c",line 244,column 12,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |244| 
        BF        $C$L25,NEQ            ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
$C$L31:    
	.dwpsn	file "../i2c.c",line 260,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |260| 
        MOVW      DP,#_EepState         ; [CPU_U] 
        MOV       @_EepState,AL         ; [CPU_] |260| 
	.dwpsn	file "../i2c.c",line 261,column 3,is_stmt
        MOV       AH,*-SP[7]            ; [CPU_] |261| 
        MOVB      AL,#0                 ; [CPU_] |261| 
        CMPB      AH,#0                 ; [CPU_] |261| 
        BF        $C$L32,NEQ            ; [CPU_] |261| 
        ; branchcc occurs ; [] |261| 
        MOVB      AL,#1                 ; [CPU_] |261| 
$C$L32:    
	.dwpsn	file "../i2c.c",line 262,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$60, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$60, DW_AT_TI_end_line(0x106)
	.dwattr $C$DW$60, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$60

	.sect	".text"
	.global	_I2C_EepWrite

$C$DW$80	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_EepWrite")
	.dwattr $C$DW$80, DW_AT_low_pc(_I2C_EepWrite)
	.dwattr $C$DW$80, DW_AT_high_pc(0x00)
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_I2C_EepWrite")
	.dwattr $C$DW$80, DW_AT_external
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$80, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$80, DW_AT_TI_begin_line(0x10a)
	.dwattr $C$DW$80, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$80, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../i2c.c",line 267,column 1,is_stmt,address _I2C_EepWrite

	.dwfde $C$DW$CIE, _I2C_EepWrite
$C$DW$81	.dwtag  DW_TAG_formal_parameter, DW_AT_name("device_code")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg0]
$C$DW$82	.dwtag  DW_TAG_formal_parameter, DW_AT_name("buffer")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg12]
$C$DW$83	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg1]
$C$DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _I2C_EepWrite                 FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_I2C_EepWrite:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("device_code")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -2]
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -4]
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -5]
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_breg20 -6]
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -7]
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("pagenb")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_pagenb")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[6],AR5           ; [CPU_] |267| 
        MOV       *-SP[5],AH            ; [CPU_] |267| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |267| 
        MOV       *-SP[2],AL            ; [CPU_] |267| 
	.dwpsn	file "../i2c.c",line 269,column 3,is_stmt
        MOVW      DP,#_EepOddAdr        ; [CPU_U] 
        MOV       @_EepOddAdr,#0        ; [CPU_] |269| 
$C$L33:    
	.dwpsn	file "../i2c.c",line 272,column 5,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |272| 
        MOVB      AH,#32                ; [CPU_] |272| 
        ANDB      AL,#0x1f              ; [CPU_] |272| 
        SUB       AH,AL                 ; [CPU_] |272| 
        MOV       *-SP[8],AH            ; [CPU_] |272| 
	.dwpsn	file "../i2c.c",line 273,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |273| 
        CMP       AL,*-SP[8]            ; [CPU_] |273| 
        B         $C$L34,HIS            ; [CPU_] |273| 
        ; branchcc occurs ; [] |273| 
	.dwpsn	file "../i2c.c",line 273,column 22,is_stmt
        MOV       *-SP[8],AL            ; [CPU_] |273| 
$C$L34:    
	.dwpsn	file "../i2c.c",line 274,column 5,is_stmt
        MOVB      *-SP[1],#2,UNC        ; [CPU_] |274| 
        MOV       AL,*-SP[2]            ; [CPU_] |274| 
        MOVZ      AR5,*-SP[8]           ; [CPU_] |274| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |274| 
        MOV       AH,*-SP[6]            ; [CPU_] |274| 
$C$DW$91	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$91, DW_AT_low_pc(0x00)
	.dwattr $C$DW$91, DW_AT_name("_EepPageWrite")
	.dwattr $C$DW$91, DW_AT_TI_call
        LCR       #_EepPageWrite        ; [CPU_] |274| 
        ; call occurs [#_EepPageWrite] ; [] |274| 
        MOV       *-SP[7],AL            ; [CPU_] |274| 
	.dwpsn	file "../i2c.c",line 275,column 5,is_stmt
        CMPB      AL,#0                 ; [CPU_] |275| 
        BF        $C$L35,NEQ            ; [CPU_] |275| 
        ; branchcc occurs ; [] |275| 
	.dwpsn	file "../i2c.c",line 276,column 7,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |276| 
        MOVB      AL,#11                ; [CPU_] |276| 
$C$DW$92	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$92, DW_AT_low_pc(0x00)
	.dwattr $C$DW$92, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$92, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |276| 
        ; call occurs [#_SEM_pend] ; [] |276| 
        B         $C$L36,UNC            ; [CPU_] |276| 
        ; branch occurs ; [] |276| 
$C$L35:    
	.dwpsn	file "../i2c.c",line 278,column 7,is_stmt
        MOVB      AL,#0                 ; [CPU_] |278| 
        B         $C$L37,UNC            ; [CPU_] |278| 
        ; branch occurs ; [] |278| 
$C$L36:    
	.dwpsn	file "../i2c.c",line 279,column 5,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |279| 
        ADD       *-SP[6],AL            ; [CPU_] |279| 
	.dwpsn	file "../i2c.c",line 280,column 5,is_stmt
        LSR       AL,1                  ; [CPU_] |280| 
        MOVU      ACC,AL                ; [CPU_] |280| 
        ADDL      *-SP[4],ACC           ; [CPU_] |280| 
	.dwpsn	file "../i2c.c",line 281,column 5,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |281| 
        SUB       *-SP[5],AL            ; [CPU_] |281| 
	.dwpsn	file "../i2c.c",line 282,column 11,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |282| 
        BF        $C$L33,NEQ            ; [CPU_] |282| 
        ; branchcc occurs ; [] |282| 
	.dwpsn	file "../i2c.c",line 283,column 3,is_stmt
        MOV       AH,*-SP[7]            ; [CPU_] |283| 
        MOVB      AL,#0                 ; [CPU_] |283| 
        CMPB      AH,#0                 ; [CPU_] |283| 
        BF        $C$L37,NEQ            ; [CPU_] |283| 
        ; branchcc occurs ; [] |283| 
        MOVB      AL,#1                 ; [CPU_] |283| 
$C$L37:    
	.dwpsn	file "../i2c.c",line 284,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$80, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$80, DW_AT_TI_end_line(0x11c)
	.dwattr $C$DW$80, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$80

	.sect	".text"
	.global	_I2C_AdsWrite

$C$DW$94	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_AdsWrite")
	.dwattr $C$DW$94, DW_AT_low_pc(_I2C_AdsWrite)
	.dwattr $C$DW$94, DW_AT_high_pc(0x00)
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_I2C_AdsWrite")
	.dwattr $C$DW$94, DW_AT_external
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$94, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$94, DW_AT_TI_begin_line(0x11e)
	.dwattr $C$DW$94, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$94, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../i2c.c",line 287,column 1,is_stmt,address _I2C_AdsWrite

	.dwfde $C$DW$CIE, _I2C_AdsWrite
$C$DW$95	.dwtag  DW_TAG_formal_parameter, DW_AT_name("device_code")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$95, DW_AT_location[DW_OP_reg0]
$C$DW$96	.dwtag  DW_TAG_formal_parameter, DW_AT_name("buffer")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg12]
$C$DW$97	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg1]
$C$DW$98	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _I2C_AdsWrite                 FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_I2C_AdsWrite:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$99	.dwtag  DW_TAG_variable, DW_AT_name("device_code")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_breg20 -1]
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_breg20 -4]
$C$DW$101	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_breg20 -5]
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_breg20 -6]
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -7]
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("wait_nb")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_wait_nb")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_breg20 -8]
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("ftime")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_ftime")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[6],AR5           ; [CPU_] |287| 
        MOV       *-SP[5],AH            ; [CPU_] |287| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |287| 
        MOV       *-SP[1],AL            ; [CPU_] |287| 
	.dwpsn	file "../i2c.c",line 291,column 3,is_stmt
        MOVZ      AR4,*-SP[5]           ; [CPU_] |291| 
        MOV       AH,*-SP[6]            ; [CPU_] |291| 
        MOVB      XAR5,#1               ; [CPU_] |291| 
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("_EepSetup")
	.dwattr $C$DW$106, DW_AT_TI_call
        LCR       #_EepSetup            ; [CPU_] |291| 
        ; call occurs [#_EepSetup] ; [] |291| 
        MOV       *-SP[7],AL            ; [CPU_] |291| 
	.dwpsn	file "../i2c.c",line 292,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |292| 
        BF        $C$L46,NEQ            ; [CPU_] |292| 
        ; branchcc occurs ; [] |292| 
	.dwpsn	file "../i2c.c",line 295,column 5,is_stmt
        OR        @_I2caRegs+2,#0x0020  ; [CPU_] |295| 
	.dwpsn	file "../i2c.c",line 296,column 5,is_stmt
        MOV       @_I2caRegs+9,#28192   ; [CPU_] |296| 
	.dwpsn	file "../i2c.c",line 297,column 5,is_stmt
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$107, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |297| 
        ; call occurs [#_CLK_getltime] ; [] |297| 
        MOVL      *-SP[10],ACC          ; [CPU_] |297| 
	.dwpsn	file "../i2c.c",line 298,column 5,is_stmt
        B         $C$L45,UNC            ; [CPU_] |298| 
        ; branch occurs ; [] |298| 
$C$L38:    
	.dwpsn	file "../i2c.c",line 299,column 7,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |299| 
        CMPB      AL,#2                 ; [CPU_] |299| 
        B         $C$L39,LO             ; [CPU_] |299| 
        ; branchcc occurs ; [] |299| 
	.dwpsn	file "../i2c.c",line 299,column 20,is_stmt
        MOVB      *-SP[8],#2,UNC        ; [CPU_] |299| 
        B         $C$L40,UNC            ; [CPU_] |299| 
        ; branch occurs ; [] |299| 
$C$L39:    
	.dwpsn	file "../i2c.c",line 299,column 38,is_stmt
        MOV       *-SP[8],AL            ; [CPU_] |299| 
$C$L40:    
	.dwpsn	file "../i2c.c",line 300,column 7,is_stmt
        AND       AL,@_I2caRegs+32,#0x1f00 ; [CPU_] |300| 
        LSR       AL,8                  ; [CPU_] |300| 
        CMPB      AL,#2                 ; [CPU_] |300| 
        B         $C$L42,GT             ; [CPU_] |300| 
        ; branchcc occurs ; [] |300| 
        MOV       AL,*-SP[5]            ; [CPU_] |300| 
        BF        $C$L42,EQ             ; [CPU_] |300| 
        ; branchcc occurs ; [] |300| 
	.dwpsn	file "../i2c.c",line 302,column 9,is_stmt
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$108, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |302| 
        ; call occurs [#_CLK_getltime] ; [] |302| 
        MOVL      *-SP[10],ACC          ; [CPU_] |302| 
	.dwpsn	file "../i2c.c",line 303,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |303| 
        MOVW      DP,#_I2caRegs+8       ; [CPU_U] 
        MOV       AL,*+XAR4[0]          ; [CPU_] |303| 
        ANDB      AL,#0xff              ; [CPU_] |303| 
        MOV       @_I2caRegs+8,AL       ; [CPU_] |303| 
	.dwpsn	file "../i2c.c",line 304,column 9,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |304| 
        CMPB      AL,#1                 ; [CPU_] |304| 
        B         $C$L41,LOS            ; [CPU_] |304| 
        ; branchcc occurs ; [] |304| 
	.dwpsn	file "../i2c.c",line 305,column 11,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |305| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |305| 
        LSR       AL,8                  ; [CPU_] |305| 
        MOV       @_I2caRegs+8,AL       ; [CPU_] |305| 
$C$L41:    
	.dwpsn	file "../i2c.c",line 307,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |307| 
        ADDL      *-SP[4],ACC           ; [CPU_] |307| 
	.dwpsn	file "../i2c.c",line 308,column 9,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |308| 
        SUB       *-SP[5],AL            ; [CPU_] |308| 
$C$L42:    
	.dwpsn	file "../i2c.c",line 310,column 7,is_stmt
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$109, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |310| 
        ; call occurs [#_CLK_getltime] ; [] |310| 
        MOVB      XAR6,#2               ; [CPU_] |310| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |310| 
        CMPL      ACC,XAR6              ; [CPU_] |310| 
        B         $C$L43,LOS            ; [CPU_] |310| 
        ; branchcc occurs ; [] |310| 
	.dwpsn	file "../i2c.c",line 311,column 9,is_stmt
        MOV       *-SP[7],#65535        ; [CPU_] |311| 
	.dwpsn	file "../i2c.c",line 311,column 26,is_stmt
        B         $C$L46,UNC            ; [CPU_] |311| 
        ; branch occurs ; [] |311| 
$C$L43:    
	.dwpsn	file "../i2c.c",line 313,column 7,is_stmt
        MOVW      DP,#_I2caRegs+2       ; [CPU_U] 
        TBIT      @_I2caRegs+2,#1       ; [CPU_] |313| 
        BF        $C$L44,TC             ; [CPU_] |313| 
        ; branchcc occurs ; [] |313| 
        TBIT      @_I2caRegs+2,#0       ; [CPU_] |313| 
        BF        $C$L45,NTC            ; [CPU_] |313| 
        ; branchcc occurs ; [] |313| 
$C$L44:    
	.dwpsn	file "../i2c.c",line 314,column 9,is_stmt
        OR        @_I2caRegs+2,#0x0002  ; [CPU_] |314| 
	.dwpsn	file "../i2c.c",line 315,column 9,is_stmt
        OR        @_I2caRegs+2,#0x0001  ; [CPU_] |315| 
	.dwpsn	file "../i2c.c",line 316,column 9,is_stmt
        MOVB      *-SP[7],#2,UNC        ; [CPU_] |316| 
	.dwpsn	file "../i2c.c",line 316,column 31,is_stmt
        B         $C$L46,UNC            ; [CPU_] |316| 
        ; branch occurs ; [] |316| 
$C$L45:    
	.dwpsn	file "../i2c.c",line 298,column 12,is_stmt
        MOVW      DP,#_I2caRegs+2       ; [CPU_U] 
        TBIT      @_I2caRegs+2,#5       ; [CPU_] |298| 
        BF        $C$L38,NTC            ; [CPU_] |298| 
        ; branchcc occurs ; [] |298| 
$C$L46:    
	.dwpsn	file "../i2c.c",line 320,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |320| 
        MOVW      DP,#_EepState         ; [CPU_U] 
        MOV       @_EepState,AL         ; [CPU_] |320| 
	.dwpsn	file "../i2c.c",line 321,column 3,is_stmt
        MOV       AH,*-SP[7]            ; [CPU_] |321| 
        MOVB      AL,#0                 ; [CPU_] |321| 
        CMPB      AH,#0                 ; [CPU_] |321| 
        BF        $C$L47,NEQ            ; [CPU_] |321| 
        ; branchcc occurs ; [] |321| 
        MOVB      AL,#1                 ; [CPU_] |321| 
$C$L47:    
	.dwpsn	file "../i2c.c",line 322,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$94, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$94, DW_AT_TI_end_line(0x142)
	.dwattr $C$DW$94, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$94

	.sect	".text"
	.global	_EepPageWrite

$C$DW$111	.dwtag  DW_TAG_subprogram, DW_AT_name("EepPageWrite")
	.dwattr $C$DW$111, DW_AT_low_pc(_EepPageWrite)
	.dwattr $C$DW$111, DW_AT_high_pc(0x00)
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_EepPageWrite")
	.dwattr $C$DW$111, DW_AT_external
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$111, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$111, DW_AT_TI_begin_line(0x146)
	.dwattr $C$DW$111, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$111, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../i2c.c",line 327,column 1,is_stmt,address _EepPageWrite

	.dwfde $C$DW$CIE, _EepPageWrite
$C$DW$112	.dwtag  DW_TAG_formal_parameter, DW_AT_name("device_code")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg0]
$C$DW$113	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg1]
$C$DW$114	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pagenb")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_pagenb")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg14]
$C$DW$115	.dwtag  DW_TAG_formal_parameter, DW_AT_name("buffer")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg12]
$C$DW$116	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address_size")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_address_size")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$116, DW_AT_location[DW_OP_breg20 -13]

;***************************************************************
;* FNAME: _EepPageWrite                 FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_EepPageWrite:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$117	.dwtag  DW_TAG_variable, DW_AT_name("device_code")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_device_code")
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$117, DW_AT_location[DW_OP_breg20 -1]
$C$DW$118	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$118, DW_AT_location[DW_OP_breg20 -2]
$C$DW$119	.dwtag  DW_TAG_variable, DW_AT_name("pagenb")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_pagenb")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$119, DW_AT_location[DW_OP_breg20 -3]
$C$DW$120	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$120, DW_AT_location[DW_OP_breg20 -6]
$C$DW$121	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$121, DW_AT_location[DW_OP_breg20 -7]
$C$DW$122	.dwtag  DW_TAG_variable, DW_AT_name("wait_nb")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_wait_nb")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$122, DW_AT_location[DW_OP_breg20 -8]
$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("ftime")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_ftime")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$123, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[3],AR5           ; [CPU_] |327| 
        MOV       *-SP[2],AH            ; [CPU_] |327| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |327| 
        MOV       *-SP[1],AL            ; [CPU_] |327| 
	.dwpsn	file "../i2c.c",line 330,column 3,is_stmt
        MOVZ      AR5,*-SP[13]          ; [CPU_] |330| 
        MOVZ      AR4,*-SP[3]           ; [CPU_] |330| 
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_name("_EepSetup")
	.dwattr $C$DW$124, DW_AT_TI_call
        LCR       #_EepSetup            ; [CPU_] |330| 
        ; call occurs [#_EepSetup] ; [] |330| 
        MOV       *-SP[7],AL            ; [CPU_] |330| 
	.dwpsn	file "../i2c.c",line 331,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |331| 
        BF        $C$L51,NEQ            ; [CPU_] |331| 
        ; branchcc occurs ; [] |331| 
	.dwpsn	file "../i2c.c",line 334,column 5,is_stmt
        OR        @_I2caRegs+2,#0x0020  ; [CPU_] |334| 
	.dwpsn	file "../i2c.c",line 335,column 5,is_stmt
        MOV       @_I2caRegs+9,#28192   ; [CPU_] |335| 
	.dwpsn	file "../i2c.c",line 337,column 5,is_stmt
$C$DW$125	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$125, DW_AT_low_pc(0x00)
	.dwattr $C$DW$125, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$125, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |337| 
        ; call occurs [#_CLK_getltime] ; [] |337| 
        MOVL      *-SP[10],ACC          ; [CPU_] |337| 
	.dwpsn	file "../i2c.c",line 338,column 5,is_stmt
        B         $C$L50,UNC            ; [CPU_] |338| 
        ; branch occurs ; [] |338| 
$C$L48:    
	.dwpsn	file "../i2c.c",line 339,column 7,is_stmt
$C$DW$126	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$126, DW_AT_low_pc(0x00)
	.dwattr $C$DW$126, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$126, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |339| 
        ; call occurs [#_CLK_getltime] ; [] |339| 
        MOVB      XAR6,#10              ; [CPU_] |339| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |339| 
        CMPL      ACC,XAR6              ; [CPU_] |339| 
        B         $C$L49,LOS            ; [CPU_] |339| 
        ; branchcc occurs ; [] |339| 
	.dwpsn	file "../i2c.c",line 340,column 9,is_stmt
        MOV       *-SP[7],#65535        ; [CPU_] |340| 
	.dwpsn	file "../i2c.c",line 340,column 26,is_stmt
        B         $C$L51,UNC            ; [CPU_] |340| 
        ; branch occurs ; [] |340| 
$C$L49:    
	.dwpsn	file "../i2c.c",line 342,column 7,is_stmt
        MOVW      DP,#_I2caRegs+2       ; [CPU_U] 
        TBIT      @_I2caRegs+2,#1       ; [CPU_] |342| 
        BF        $C$L50,NTC            ; [CPU_] |342| 
        ; branchcc occurs ; [] |342| 
	.dwpsn	file "../i2c.c",line 343,column 9,is_stmt
        MOVB      *-SP[7],#2,UNC        ; [CPU_] |343| 
	.dwpsn	file "../i2c.c",line 344,column 9,is_stmt
        OR        @_I2caRegs+2,#0x0002  ; [CPU_] |344| 
	.dwpsn	file "../i2c.c",line 345,column 9,is_stmt
        B         $C$L51,UNC            ; [CPU_] |345| 
        ; branch occurs ; [] |345| 
$C$L50:    
	.dwpsn	file "../i2c.c",line 338,column 12,is_stmt
        MOVW      DP,#_I2caRegs+2       ; [CPU_U] 
        AND       AL,@_I2caRegs+2,#0x0400 ; [CPU_] |338| 
        LSR       AL,10                 ; [CPU_] |338| 
        CMPB      AL,#1                 ; [CPU_] |338| 
        BF        $C$L48,EQ             ; [CPU_] |338| 
        ; branchcc occurs ; [] |338| 
$C$L51:    
	.dwpsn	file "../i2c.c",line 349,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |349| 
        BF        $C$L62,NEQ            ; [CPU_] |349| 
        ; branchcc occurs ; [] |349| 
	.dwpsn	file "../i2c.c",line 350,column 5,is_stmt
        MOVW      DP,#_EepOddAdr        ; [CPU_U] 
        MOV       AL,@_EepOddAdr        ; [CPU_] |350| 
        BF        $C$L52,EQ             ; [CPU_] |350| 
        ; branchcc occurs ; [] |350| 
	.dwpsn	file "../i2c.c",line 351,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |351| 
        MOVW      DP,#_I2caRegs+8       ; [CPU_U] 
        MOV       AL,*+XAR4[0]          ; [CPU_] |351| 
        LSR       AL,8                  ; [CPU_] |351| 
        MOV       @_I2caRegs+8,AL       ; [CPU_] |351| 
	.dwpsn	file "../i2c.c",line 352,column 7,is_stmt
        MOVB      ACC,#1                ; [CPU_] |352| 
        ADDL      *-SP[6],ACC           ; [CPU_] |352| 
	.dwpsn	file "../i2c.c",line 353,column 7,is_stmt
        DEC       *-SP[3]               ; [CPU_] |353| 
	.dwpsn	file "../i2c.c",line 354,column 7,is_stmt
        MOVW      DP,#_EepOddAdr        ; [CPU_U] 
        MOV       @_EepOddAdr,#0        ; [CPU_] |354| 
$C$L52:    
	.dwpsn	file "../i2c.c",line 356,column 5,is_stmt
$C$DW$127	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$127, DW_AT_low_pc(0x00)
	.dwattr $C$DW$127, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$127, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |356| 
        ; call occurs [#_CLK_getltime] ; [] |356| 
        MOVL      *-SP[10],ACC          ; [CPU_] |356| 
	.dwpsn	file "../i2c.c",line 357,column 5,is_stmt
        B         $C$L61,UNC            ; [CPU_] |357| 
        ; branch occurs ; [] |357| 
$C$L53:    
	.dwpsn	file "../i2c.c",line 359,column 7,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |359| 
        CMPB      AL,#2                 ; [CPU_] |359| 
        B         $C$L54,LO             ; [CPU_] |359| 
        ; branchcc occurs ; [] |359| 
	.dwpsn	file "../i2c.c",line 359,column 24,is_stmt
        MOVB      *-SP[8],#2,UNC        ; [CPU_] |359| 
        B         $C$L55,UNC            ; [CPU_] |359| 
        ; branch occurs ; [] |359| 
$C$L54:    
	.dwpsn	file "../i2c.c",line 359,column 42,is_stmt
        MOV       *-SP[8],AL            ; [CPU_] |359| 
$C$L55:    
	.dwpsn	file "../i2c.c",line 360,column 7,is_stmt
        AND       AL,@_I2caRegs+32,#0x1f00 ; [CPU_] |360| 
        LSR       AL,8                  ; [CPU_] |360| 
        CMPB      AL,#2                 ; [CPU_] |360| 
        B         $C$L58,GT             ; [CPU_] |360| 
        ; branchcc occurs ; [] |360| 
        MOV       AL,*-SP[3]            ; [CPU_] |360| 
        BF        $C$L58,EQ             ; [CPU_] |360| 
        ; branchcc occurs ; [] |360| 
	.dwpsn	file "../i2c.c",line 362,column 9,is_stmt
$C$DW$128	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$128, DW_AT_low_pc(0x00)
	.dwattr $C$DW$128, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$128, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |362| 
        ; call occurs [#_CLK_getltime] ; [] |362| 
        MOVL      *-SP[10],ACC          ; [CPU_] |362| 
	.dwpsn	file "../i2c.c",line 363,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |363| 
        MOVW      DP,#_I2caRegs+8       ; [CPU_U] 
        MOV       AL,*+XAR4[0]          ; [CPU_] |363| 
        ANDB      AL,#0xff              ; [CPU_] |363| 
        MOV       @_I2caRegs+8,AL       ; [CPU_] |363| 
	.dwpsn	file "../i2c.c",line 364,column 9,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |364| 
        CMPB      AL,#1                 ; [CPU_] |364| 
        B         $C$L56,LOS            ; [CPU_] |364| 
        ; branchcc occurs ; [] |364| 
	.dwpsn	file "../i2c.c",line 365,column 11,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |365| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |365| 
        LSR       AL,8                  ; [CPU_] |365| 
        MOV       @_I2caRegs+8,AL       ; [CPU_] |365| 
	.dwpsn	file "../i2c.c",line 366,column 9,is_stmt
        B         $C$L57,UNC            ; [CPU_] |366| 
        ; branch occurs ; [] |366| 
$C$L56:    
	.dwpsn	file "../i2c.c",line 367,column 14,is_stmt
        MOVW      DP,#_EepOddAdr        ; [CPU_U] 
        MOVB      @_EepOddAdr,#1,UNC    ; [CPU_] |367| 
$C$L57:    
	.dwpsn	file "../i2c.c",line 368,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |368| 
        ADDL      *-SP[6],ACC           ; [CPU_] |368| 
	.dwpsn	file "../i2c.c",line 369,column 9,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |369| 
        SUB       *-SP[3],AL            ; [CPU_] |369| 
$C$L58:    
	.dwpsn	file "../i2c.c",line 371,column 7,is_stmt
$C$DW$129	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$129, DW_AT_low_pc(0x00)
	.dwattr $C$DW$129, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$129, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |371| 
        ; call occurs [#_CLK_getltime] ; [] |371| 
        MOVB      XAR6,#10              ; [CPU_] |371| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |371| 
        CMPL      ACC,XAR6              ; [CPU_] |371| 
        B         $C$L59,LOS            ; [CPU_] |371| 
        ; branchcc occurs ; [] |371| 
	.dwpsn	file "../i2c.c",line 372,column 9,is_stmt
        MOV       *-SP[7],#65535        ; [CPU_] |372| 
	.dwpsn	file "../i2c.c",line 372,column 26,is_stmt
        B         $C$L62,UNC            ; [CPU_] |372| 
        ; branch occurs ; [] |372| 
$C$L59:    
	.dwpsn	file "../i2c.c",line 374,column 7,is_stmt
        MOVW      DP,#_I2caRegs+2       ; [CPU_U] 
        TBIT      @_I2caRegs+2,#1       ; [CPU_] |374| 
        BF        $C$L60,TC             ; [CPU_] |374| 
        ; branchcc occurs ; [] |374| 
        TBIT      @_I2caRegs+2,#0       ; [CPU_] |374| 
        BF        $C$L61,NTC            ; [CPU_] |374| 
        ; branchcc occurs ; [] |374| 
$C$L60:    
	.dwpsn	file "../i2c.c",line 375,column 9,is_stmt
        OR        @_I2caRegs+2,#0x0002  ; [CPU_] |375| 
	.dwpsn	file "../i2c.c",line 376,column 9,is_stmt
        OR        @_I2caRegs+2,#0x0001  ; [CPU_] |376| 
	.dwpsn	file "../i2c.c",line 377,column 9,is_stmt
        MOVB      *-SP[7],#2,UNC        ; [CPU_] |377| 
	.dwpsn	file "../i2c.c",line 377,column 31,is_stmt
        B         $C$L62,UNC            ; [CPU_] |377| 
        ; branch occurs ; [] |377| 
$C$L61:    
	.dwpsn	file "../i2c.c",line 357,column 12,is_stmt
        MOVW      DP,#_I2caRegs+2       ; [CPU_U] 
        TBIT      @_I2caRegs+2,#5       ; [CPU_] |357| 
        BF        $C$L53,NTC            ; [CPU_] |357| 
        ; branchcc occurs ; [] |357| 
$C$L62:    
	.dwpsn	file "../i2c.c",line 381,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |381| 
        MOVW      DP,#_EepState         ; [CPU_U] 
        MOV       @_EepState,AL         ; [CPU_] |381| 
	.dwpsn	file "../i2c.c",line 382,column 3,is_stmt
	.dwpsn	file "../i2c.c",line 383,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$111, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$111, DW_AT_TI_end_line(0x17f)
	.dwattr $C$DW$111, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$111

	.sect	".text"
	.global	_I2C_CommandNoWait

$C$DW$131	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_CommandNoWait")
	.dwattr $C$DW$131, DW_AT_low_pc(_I2C_CommandNoWait)
	.dwattr $C$DW$131, DW_AT_high_pc(0x00)
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_I2C_CommandNoWait")
	.dwattr $C$DW$131, DW_AT_external
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$131, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$131, DW_AT_TI_begin_line(0x181)
	.dwattr $C$DW$131, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$131, DW_AT_TI_max_frame_size(-20)
	.dwpsn	file "../i2c.c",line 386,column 1,is_stmt,address _I2C_CommandNoWait

	.dwfde $C$DW$CIE, _I2C_CommandNoWait
$C$DW$132	.dwtag  DW_TAG_formal_parameter, DW_AT_name("cmd")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_cmd")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$132, DW_AT_location[DW_OP_reg0]
$C$DW$133	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_reg12]
$C$DW$134	.dwtag  DW_TAG_formal_parameter, DW_AT_name("length")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg1]
$C$DW$135	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_breg20 -21]
$C$DW$136	.dwtag  DW_TAG_formal_parameter, DW_AT_name("statusi2c")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_statusi2c")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _I2C_CommandNoWait            FR SIZE:  18           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 18 Auto,  0 SOE     *
;***************************************************************

_I2C_CommandNoWait:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -20
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("cmd")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_cmd")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_breg20 -1]
$C$DW$138	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$138, DW_AT_location[DW_OP_breg20 -4]
$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("length")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$139, DW_AT_location[DW_OP_breg20 -5]
$C$DW$140	.dwtag  DW_TAG_variable, DW_AT_name("statusi2c")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_statusi2c")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$140, DW_AT_location[DW_OP_breg20 -8]
$C$DW$141	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$141, DW_AT_location[DW_OP_breg20 -9]
$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("i2cmsg")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_i2cmsg")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$142, DW_AT_location[DW_OP_breg20 -18]
        MOV       *-SP[5],AH            ; [CPU_] |386| 
        MOVL      *-SP[8],XAR5          ; [CPU_] |386| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |386| 
        MOV       *-SP[1],AL            ; [CPU_] |386| 
	.dwpsn	file "../i2c.c",line 387,column 13,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |387| 
	.dwpsn	file "../i2c.c",line 389,column 3,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |389| 
        MOV       *+XAR4[0],#0          ; [CPU_] |389| 
	.dwpsn	file "../i2c.c",line 391,column 3,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |391| 
        MOVL      *-SP[12],ACC          ; [CPU_] |391| 
	.dwpsn	file "../i2c.c",line 392,column 3,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |392| 
        MOVL      *-SP[14],ACC          ; [CPU_] |392| 
	.dwpsn	file "../i2c.c",line 393,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |393| 
        MOV       *-SP[16],AL           ; [CPU_] |393| 
	.dwpsn	file "../i2c.c",line 394,column 3,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |394| 
        MOV       *-SP[15],AL           ; [CPU_] |394| 
	.dwpsn	file "../i2c.c",line 395,column 3,is_stmt
        B         $C$L73,UNC            ; [CPU_] |395| 
        ; branch occurs ; [] |395| 
$C$L63:    
	.dwpsn	file "../i2c.c",line 397,column 7,is_stmt
        MOVB      *-SP[18],#160,UNC     ; [CPU_] |397| 
	.dwpsn	file "../i2c.c",line 398,column 7,is_stmt
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |398| 
	.dwpsn	file "../i2c.c",line 399,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |399| 
        ; branch occurs ; [] |399| 
$C$L64:    
	.dwpsn	file "../i2c.c",line 401,column 7,is_stmt
        MOVB      *-SP[18],#160,UNC     ; [CPU_] |401| 
	.dwpsn	file "../i2c.c",line 402,column 7,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |402| 
	.dwpsn	file "../i2c.c",line 403,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |403| 
        ; branch occurs ; [] |403| 
$C$L65:    
	.dwpsn	file "../i2c.c",line 405,column 7,is_stmt
        MOVB      *-SP[18],#224,UNC     ; [CPU_] |405| 
	.dwpsn	file "../i2c.c",line 406,column 7,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |406| 
	.dwpsn	file "../i2c.c",line 407,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |407| 
        ; branch occurs ; [] |407| 
$C$L66:    
	.dwpsn	file "../i2c.c",line 409,column 7,is_stmt
        MOVB      *-SP[18],#224,UNC     ; [CPU_] |409| 
	.dwpsn	file "../i2c.c",line 410,column 7,is_stmt
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |410| 
	.dwpsn	file "../i2c.c",line 411,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |411| 
        ; branch occurs ; [] |411| 
$C$L67:    
	.dwpsn	file "../i2c.c",line 413,column 7,is_stmt
        MOVB      *-SP[18],#144,UNC     ; [CPU_] |413| 
	.dwpsn	file "../i2c.c",line 414,column 7,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |414| 
	.dwpsn	file "../i2c.c",line 415,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |415| 
        ; branch occurs ; [] |415| 
$C$L68:    
	.dwpsn	file "../i2c.c",line 417,column 7,is_stmt
        MOVB      *-SP[18],#144,UNC     ; [CPU_] |417| 
	.dwpsn	file "../i2c.c",line 418,column 7,is_stmt
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |418| 
	.dwpsn	file "../i2c.c",line 419,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |419| 
        ; branch occurs ; [] |419| 
$C$L69:    
	.dwpsn	file "../i2c.c",line 421,column 7,is_stmt
        MOVB      *-SP[18],#116,UNC     ; [CPU_] |421| 
	.dwpsn	file "../i2c.c",line 422,column 7,is_stmt
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |422| 
	.dwpsn	file "../i2c.c",line 423,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |423| 
        ; branch occurs ; [] |423| 
$C$L70:    
	.dwpsn	file "../i2c.c",line 425,column 7,is_stmt
        MOVB      *-SP[18],#116,UNC     ; [CPU_] |425| 
	.dwpsn	file "../i2c.c",line 426,column 7,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |426| 
	.dwpsn	file "../i2c.c",line 427,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |427| 
        ; branch occurs ; [] |427| 
$C$L71:    
	.dwpsn	file "../i2c.c",line 429,column 7,is_stmt
        MOVB      *-SP[18],#208,UNC     ; [CPU_] |429| 
	.dwpsn	file "../i2c.c",line 430,column 7,is_stmt
        MOVB      *-SP[17],#1,UNC       ; [CPU_] |430| 
	.dwpsn	file "../i2c.c",line 431,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |431| 
        ; branch occurs ; [] |431| 
$C$L72:    
	.dwpsn	file "../i2c.c",line 433,column 7,is_stmt
        MOVB      *-SP[18],#208,UNC     ; [CPU_] |433| 
	.dwpsn	file "../i2c.c",line 434,column 7,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |434| 
	.dwpsn	file "../i2c.c",line 435,column 7,is_stmt
        B         $C$L76,UNC            ; [CPU_] |435| 
        ; branch occurs ; [] |435| 
$C$L73:    
	.dwpsn	file "../i2c.c",line 395,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |395| 
        CMPB      AL,#6                 ; [CPU_] |395| 
        B         $C$L75,GT             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        CMPB      AL,#6                 ; [CPU_] |395| 
        BF        $C$L67,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        CMPB      AL,#3                 ; [CPU_] |395| 
        B         $C$L74,GT             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        CMPB      AL,#3                 ; [CPU_] |395| 
        BF        $C$L65,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        CMPB      AL,#0                 ; [CPU_] |395| 
        BF        $C$L63,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        CMPB      AL,#1                 ; [CPU_] |395| 
        BF        $C$L64,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        B         $C$L76,UNC            ; [CPU_] |395| 
        ; branch occurs ; [] |395| 
$C$L74:    
        CMPB      AL,#4                 ; [CPU_] |395| 
        BF        $C$L66,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        CMPB      AL,#5                 ; [CPU_] |395| 
        BF        $C$L68,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        B         $C$L76,UNC            ; [CPU_] |395| 
        ; branch occurs ; [] |395| 
$C$L75:    
        CMPB      AL,#7                 ; [CPU_] |395| 
        BF        $C$L69,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        CMPB      AL,#8                 ; [CPU_] |395| 
        BF        $C$L70,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        CMPB      AL,#9                 ; [CPU_] |395| 
        BF        $C$L72,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
        CMPB      AL,#10                ; [CPU_] |395| 
        BF        $C$L71,EQ             ; [CPU_] |395| 
        ; branchcc occurs ; [] |395| 
$C$L76:    
	.dwpsn	file "../i2c.c",line 437,column 3,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |437| 
        MOVL      XAR4,#_mailboxI2C_SendAndReceive ; [CPU_U] |437| 
        MOVB      AL,#0                 ; [CPU_] |437| 
        SUBB      XAR5,#18              ; [CPU_U] |437| 
$C$DW$143	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$143, DW_AT_low_pc(0x00)
	.dwattr $C$DW$143, DW_AT_name("_MBX_post")
	.dwattr $C$DW$143, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |437| 
        ; call occurs [#_MBX_post] ; [] |437| 
        MOV       *-SP[9],AL            ; [CPU_] |437| 
	.dwpsn	file "../i2c.c",line 438,column 3,is_stmt
	.dwpsn	file "../i2c.c",line 439,column 1,is_stmt
        SUBB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$131, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$131, DW_AT_TI_end_line(0x1b7)
	.dwattr $C$DW$131, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$131

	.sect	".text"
	.global	_I2C_Command

$C$DW$145	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$145, DW_AT_low_pc(_I2C_Command)
	.dwattr $C$DW$145, DW_AT_high_pc(0x00)
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$145, DW_AT_external
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$145, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$145, DW_AT_TI_begin_line(0x1bd)
	.dwattr $C$DW$145, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$145, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../i2c.c",line 446,column 1,is_stmt,address _I2C_Command

	.dwfde $C$DW$CIE, _I2C_Command
$C$DW$146	.dwtag  DW_TAG_formal_parameter, DW_AT_name("cmd")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_cmd")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$146, DW_AT_location[DW_OP_reg0]
$C$DW$147	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg12]
$C$DW$148	.dwtag  DW_TAG_formal_parameter, DW_AT_name("length")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg1]
$C$DW$149	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$149, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _I2C_Command                  FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_I2C_Command:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$150	.dwtag  DW_TAG_variable, DW_AT_name("cmd")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_cmd")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$150, DW_AT_location[DW_OP_breg20 -2]
$C$DW$151	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$151, DW_AT_location[DW_OP_breg20 -4]
$C$DW$152	.dwtag  DW_TAG_variable, DW_AT_name("length")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$152, DW_AT_location[DW_OP_breg20 -5]
$C$DW$153	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$153, DW_AT_location[DW_OP_breg20 -6]
$C$DW$154	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$154, DW_AT_location[DW_OP_breg20 -7]
$C$DW$155	.dwtag  DW_TAG_variable, DW_AT_name("start")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_start")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$155, DW_AT_location[DW_OP_breg20 -10]
$C$DW$156	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$156, DW_AT_location[DW_OP_breg20 -11]
$C$DW$157	.dwtag  DW_TAG_variable, DW_AT_name("retry")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_retry")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$157, DW_AT_location[DW_OP_breg20 -12]
$C$DW$158	.dwtag  DW_TAG_variable, DW_AT_name("statusi2c")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_statusi2c")
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$158, DW_AT_location[DW_OP_breg20 -13]
        MOV       *-SP[6],AR5           ; [CPU_] |446| 
        MOV       *-SP[5],AH            ; [CPU_] |446| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |446| 
        MOV       *-SP[2],AL            ; [CPU_] |446| 
	.dwpsn	file "../i2c.c",line 447,column 13,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |447| 
	.dwpsn	file "../i2c.c",line 449,column 10,is_stmt
        MOV       *-SP[11],#0           ; [CPU_] |449| 
	.dwpsn	file "../i2c.c",line 449,column 21,is_stmt
        MOVB      *-SP[12],#3,UNC       ; [CPU_] |449| 
	.dwpsn	file "../i2c.c",line 450,column 19,is_stmt
        MOV       *-SP[13],#0           ; [CPU_] |450| 
	.dwpsn	file "../i2c.c",line 453,column 3,is_stmt
        B         $C$L83,UNC            ; [CPU_] |453| 
        ; branch occurs ; [] |453| 
$C$L77:    
	.dwpsn	file "../i2c.c",line 454,column 5,is_stmt
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$159, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |454| 
        ; call occurs [#_CLK_getltime] ; [] |454| 
        MOVL      *-SP[10],ACC          ; [CPU_] |454| 
	.dwpsn	file "../i2c.c",line 455,column 5,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |455| 
        MOV       *-SP[1],AL            ; [CPU_] |455| 
        MOV       AL,*-SP[2]            ; [CPU_] |455| 
        MOV       AH,*-SP[5]            ; [CPU_] |455| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |455| 
        MOVZ      AR5,SP                ; [CPU_U] |455| 
        SUBB      XAR5,#13              ; [CPU_U] |455| 
$C$DW$160	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$160, DW_AT_low_pc(0x00)
	.dwattr $C$DW$160, DW_AT_name("_I2C_CommandNoWait")
	.dwattr $C$DW$160, DW_AT_TI_call
        LCR       #_I2C_CommandNoWait   ; [CPU_] |455| 
        ; call occurs [#_I2C_CommandNoWait] ; [] |455| 
        CMPB      AL,#0                 ; [CPU_] |455| 
        BF        $C$L82,EQ             ; [CPU_] |455| 
        ; branchcc occurs ; [] |455| 
	.dwpsn	file "../i2c.c",line 456,column 7,is_stmt
        B         $C$L79,UNC            ; [CPU_] |456| 
        ; branch occurs ; [] |456| 
$C$L78:    
	.dwpsn	file "../i2c.c",line 457,column 9,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |457| 
        MOVB      AL,#1                 ; [CPU_] |457| 
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$161, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |457| 
        ; call occurs [#_SEM_pend] ; [] |457| 
$C$L79:    
	.dwpsn	file "../i2c.c",line 456,column 14,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |456| 
        CMPB      AL,#1                 ; [CPU_] |456| 
        B         $C$L80,HI             ; [CPU_] |456| 
        ; branchcc occurs ; [] |456| 
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$162, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |456| 
        ; call occurs [#_CLK_getltime] ; [] |456| 
        MOVB      XAR6,#100             ; [CPU_] |456| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |456| 
        CMPL      ACC,XAR6              ; [CPU_] |456| 
        B         $C$L78,LO             ; [CPU_] |456| 
        ; branchcc occurs ; [] |456| 
$C$L80:    
	.dwpsn	file "../i2c.c",line 459,column 7,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |459| 
        CMPB      AL,#10                ; [CPU_] |459| 
        BF        $C$L81,EQ             ; [CPU_] |459| 
        ; branchcc occurs ; [] |459| 
        CMPB      AL,#12                ; [CPU_] |459| 
        BF        $C$L81,EQ             ; [CPU_] |459| 
        ; branchcc occurs ; [] |459| 
        CMPB      AL,#11                ; [CPU_] |459| 
        BF        $C$L82,NEQ            ; [CPU_] |459| 
        ; branchcc occurs ; [] |459| 
$C$L81:    
	.dwpsn	file "../i2c.c",line 460,column 9,is_stmt
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |460| 
$C$L82:    
	.dwpsn	file "../i2c.c",line 462,column 5,is_stmt
        INC       *-SP[11]              ; [CPU_] |462| 
	.dwpsn	file "../i2c.c",line 463,column 5,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |463| 
        BF        $C$L83,NEQ            ; [CPU_] |463| 
        ; branchcc occurs ; [] |463| 
	.dwpsn	file "../i2c.c",line 464,column 7,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |464| 
        MOVB      AL,#10                ; [CPU_] |464| 
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$163, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |464| 
        ; call occurs [#_SEM_pend] ; [] |464| 
$C$L83:    
	.dwpsn	file "../i2c.c",line 453,column 10,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |453| 
        CMP       AL,*-SP[11]           ; [CPU_] |453| 
        B         $C$L84,LEQ            ; [CPU_] |453| 
        ; branchcc occurs ; [] |453| 
        MOV       AL,*-SP[7]            ; [CPU_] |453| 
        BF        $C$L77,EQ             ; [CPU_] |453| 
        ; branchcc occurs ; [] |453| 
$C$L84:    
	.dwpsn	file "../i2c.c",line 466,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |466| 
	.dwpsn	file "../i2c.c",line 467,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$164	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$164, DW_AT_low_pc(0x00)
	.dwattr $C$DW$164, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$145, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$145, DW_AT_TI_end_line(0x1d3)
	.dwattr $C$DW$145, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$145

	.sect	".text"
	.global	_taskFnI2C_SendAndReceive

$C$DW$165	.dwtag  DW_TAG_subprogram, DW_AT_name("taskFnI2C_SendAndReceive")
	.dwattr $C$DW$165, DW_AT_low_pc(_taskFnI2C_SendAndReceive)
	.dwattr $C$DW$165, DW_AT_high_pc(0x00)
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_taskFnI2C_SendAndReceive")
	.dwattr $C$DW$165, DW_AT_external
	.dwattr $C$DW$165, DW_AT_TI_begin_file("../i2c.c")
	.dwattr $C$DW$165, DW_AT_TI_begin_line(0x1e7)
	.dwattr $C$DW$165, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$165, DW_AT_TI_max_frame_size(-80)
	.dwpsn	file "../i2c.c",line 487,column 36,is_stmt,address _taskFnI2C_SendAndReceive

	.dwfde $C$DW$CIE, _taskFnI2C_SendAndReceive

;***************************************************************
;* FNAME: _taskFnI2C_SendAndReceive     FR SIZE:  78           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 75 Auto,  2 SOE     *
;***************************************************************

_taskFnI2C_SendAndReceive:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR2            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 9, 2
	.dwcfi	cfa_offset, -4
        MOVZ      AR2,SP                ; [CPU_] 
        SUBB      FP,#4                 ; [CPU_U] 
        ADDB      SP,#76                ; [CPU_U] 
	.dwcfi	cfa_offset, -80
$C$DW$166	.dwtag  DW_TAG_variable, DW_AT_name("i2c_mail")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_i2c_mail")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$166, DW_AT_location[DW_OP_breg20 -8]
$C$DW$167	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$167, DW_AT_location[DW_OP_breg20 -72]
$C$DW$168	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$168, DW_AT_location[DW_OP_breg20 -73]
$C$DW$169	.dwtag  DW_TAG_variable, DW_AT_name("error_count")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_error_count")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$169, DW_AT_location[DW_OP_breg20 -74]
$C$DW$170	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_breg20 -75]
	.dwpsn	file "../i2c.c",line 492,column 9,is_stmt
$C$L85:    
	.dwpsn	file "../i2c.c",line 493,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |493| 
        MOVL      XAR4,#_mailboxI2C_SendAndReceive ; [CPU_U] |493| 
        MOV       AL,#65535             ; [CPU_] |493| 
        SUBB      XAR5,#8               ; [CPU_U] |493| 
$C$DW$171	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$171, DW_AT_low_pc(0x00)
	.dwattr $C$DW$171, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$171, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |493| 
        ; call occurs [#_MBX_pend] ; [] |493| 
	.dwpsn	file "../i2c.c",line 494,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |494| 
        MOVB      *+XAR4[0],#1,UNC      ; [CPU_] |494| 
	.dwpsn	file "../i2c.c",line 495,column 5,is_stmt
        MOV       *+FP[6],#0            ; [CPU_] |495| 
	.dwpsn	file "../i2c.c",line 496,column 5,is_stmt
        MOV       *+FP[5],#0            ; [CPU_] |496| 
	.dwpsn	file "../i2c.c",line 497,column 5,is_stmt
        B         $C$L110,UNC           ; [CPU_] |497| 
        ; branch occurs ; [] |497| 
$C$L86:    
	.dwpsn	file "../i2c.c",line 500,column 11,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |500| 
        CMPB      AL,#160               ; [CPU_] |500| 
        BF        $C$L87,NEQ            ; [CPU_] |500| 
        ; branchcc occurs ; [] |500| 
	.dwpsn	file "../i2c.c",line 501,column 13,is_stmt
        MOV       AH,*-SP[6]            ; [CPU_] |501| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |501| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |501| 
$C$DW$172	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$172, DW_AT_low_pc(0x00)
	.dwattr $C$DW$172, DW_AT_name("_I2C_EepWrite")
	.dwattr $C$DW$172, DW_AT_TI_call
        LCR       #_I2C_EepWrite        ; [CPU_] |501| 
        ; call occurs [#_I2C_EepWrite] ; [] |501| 
        MOV       *+FP[5],AL            ; [CPU_] |501| 
	.dwpsn	file "../i2c.c",line 502,column 11,is_stmt
        B         $C$L88,UNC            ; [CPU_] |502| 
        ; branch occurs ; [] |502| 
$C$L87:    
	.dwpsn	file "../i2c.c",line 504,column 13,is_stmt
        MOV       AH,*-SP[6]            ; [CPU_] |504| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |504| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |504| 
$C$DW$173	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$173, DW_AT_low_pc(0x00)
	.dwattr $C$DW$173, DW_AT_name("_I2C_AdsWrite")
	.dwattr $C$DW$173, DW_AT_TI_call
        LCR       #_I2C_AdsWrite        ; [CPU_] |504| 
        ; call occurs [#_I2C_AdsWrite] ; [] |504| 
        MOV       *+FP[5],AL            ; [CPU_] |504| 
$C$L88:    
	.dwpsn	file "../i2c.c",line 506,column 11,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |506| 
        BF        $C$L89,NEQ            ; [CPU_] |506| 
        ; branchcc occurs ; [] |506| 
	.dwpsn	file "../i2c.c",line 507,column 13,is_stmt
        INC       *+FP[6]               ; [CPU_] |507| 
$C$L89:    
	.dwpsn	file "../i2c.c",line 499,column 16,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |499| 
        BF        $C$L90,NEQ            ; [CPU_] |499| 
        ; branchcc occurs ; [] |499| 
        MOV       AL,*+FP[6]            ; [CPU_] |499| 
        B         $C$L86,LEQ            ; [CPU_] |499| 
        ; branchcc occurs ; [] |499| 
$C$L90:    
	.dwpsn	file "../i2c.c",line 511,column 9,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |511| 
        BF        $C$L91,EQ             ; [CPU_] |511| 
        ; branchcc occurs ; [] |511| 
	.dwpsn	file "../i2c.c",line 511,column 18,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |511| 
        MOVB      *+XAR4[0],#11,UNC     ; [CPU_] |511| 
        B         $C$L111,UNC           ; [CPU_] |511| 
        ; branch occurs ; [] |511| 
$C$L91:    
	.dwpsn	file "../i2c.c",line 512,column 14,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |512| 
        MOVB      *+XAR4[0],#21,UNC     ; [CPU_] |512| 
	.dwpsn	file "../i2c.c",line 513,column 9,is_stmt
        B         $C$L111,UNC           ; [CPU_] |513| 
        ; branch occurs ; [] |513| 
$C$L92:    
	.dwpsn	file "../i2c.c",line 516,column 11,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |516| 
        CMPB      AL,#160               ; [CPU_] |516| 
        BF        $C$L93,NEQ            ; [CPU_] |516| 
        ; branchcc occurs ; [] |516| 
	.dwpsn	file "../i2c.c",line 517,column 13,is_stmt
        MOV       AH,*-SP[6]            ; [CPU_] |517| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |517| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |517| 
$C$DW$174	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$174, DW_AT_low_pc(0x00)
	.dwattr $C$DW$174, DW_AT_name("_I2C_EepRead")
	.dwattr $C$DW$174, DW_AT_TI_call
        LCR       #_I2C_EepRead         ; [CPU_] |517| 
        ; call occurs [#_I2C_EepRead] ; [] |517| 
        MOV       *+FP[5],AL            ; [CPU_] |517| 
	.dwpsn	file "../i2c.c",line 518,column 11,is_stmt
        B         $C$L94,UNC            ; [CPU_] |518| 
        ; branch occurs ; [] |518| 
$C$L93:    
	.dwpsn	file "../i2c.c",line 520,column 13,is_stmt
        MOV       AH,*-SP[6]            ; [CPU_] |520| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |520| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |520| 
$C$DW$175	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$175, DW_AT_low_pc(0x00)
	.dwattr $C$DW$175, DW_AT_name("_I2C_AdsRead")
	.dwattr $C$DW$175, DW_AT_TI_call
        LCR       #_I2C_AdsRead         ; [CPU_] |520| 
        ; call occurs [#_I2C_AdsRead] ; [] |520| 
        MOV       *+FP[5],AL            ; [CPU_] |520| 
$C$L94:    
	.dwpsn	file "../i2c.c",line 522,column 11,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |522| 
        BF        $C$L95,NEQ            ; [CPU_] |522| 
        ; branchcc occurs ; [] |522| 
	.dwpsn	file "../i2c.c",line 523,column 13,is_stmt
        INC       *+FP[6]               ; [CPU_] |523| 
$C$L95:    
	.dwpsn	file "../i2c.c",line 515,column 16,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |515| 
        BF        $C$L96,NEQ            ; [CPU_] |515| 
        ; branchcc occurs ; [] |515| 
        MOV       AL,*+FP[6]            ; [CPU_] |515| 
        B         $C$L92,LEQ            ; [CPU_] |515| 
        ; branchcc occurs ; [] |515| 
$C$L96:    
	.dwpsn	file "../i2c.c",line 527,column 9,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |527| 
        BF        $C$L97,EQ             ; [CPU_] |527| 
        ; branchcc occurs ; [] |527| 
	.dwpsn	file "../i2c.c",line 527,column 18,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |527| 
        MOVB      *+XAR4[0],#10,UNC     ; [CPU_] |527| 
        B         $C$L111,UNC           ; [CPU_] |527| 
        ; branch occurs ; [] |527| 
$C$L97:    
	.dwpsn	file "../i2c.c",line 528,column 14,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |528| 
        MOVB      *+XAR4[0],#20,UNC     ; [CPU_] |528| 
	.dwpsn	file "../i2c.c",line 529,column 9,is_stmt
        B         $C$L111,UNC           ; [CPU_] |529| 
        ; branch occurs ; [] |529| 
$C$L98:    
	.dwpsn	file "../i2c.c",line 532,column 11,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |532| 
        MOV       AH,*-SP[6]            ; [CPU_] |532| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |532| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |532| 
$C$DW$176	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$176, DW_AT_low_pc(0x00)
	.dwattr $C$DW$176, DW_AT_name("_I2C_EepWrite")
	.dwattr $C$DW$176, DW_AT_TI_call
        LCR       #_I2C_EepWrite        ; [CPU_] |532| 
        ; call occurs [#_I2C_EepWrite] ; [] |532| 
        MOV       *+FP[5],AL            ; [CPU_] |532| 
	.dwpsn	file "../i2c.c",line 533,column 11,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |533| 
        BF        $C$L99,NEQ            ; [CPU_] |533| 
        ; branchcc occurs ; [] |533| 
	.dwpsn	file "../i2c.c",line 534,column 13,is_stmt
        INC       *+FP[6]               ; [CPU_] |534| 
	.dwpsn	file "../i2c.c",line 535,column 13,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |535| 
        MOVB      AL,#1                 ; [CPU_] |535| 
$C$DW$177	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$177, DW_AT_low_pc(0x00)
	.dwattr $C$DW$177, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$177, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |535| 
        ; call occurs [#_SEM_pend] ; [] |535| 
$C$L99:    
	.dwpsn	file "../i2c.c",line 531,column 16,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |531| 
        BF        $C$L100,NEQ           ; [CPU_] |531| 
        ; branchcc occurs ; [] |531| 
        MOV       AL,*+FP[6]            ; [CPU_] |531| 
        B         $C$L98,LEQ            ; [CPU_] |531| 
        ; branchcc occurs ; [] |531| 
$C$L100:    
	.dwpsn	file "../i2c.c",line 538,column 9,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |538| 
        BF        $C$L109,EQ            ; [CPU_] |538| 
        ; branchcc occurs ; [] |538| 
	.dwpsn	file "../i2c.c",line 539,column 11,is_stmt
        MOV       *+FP[5],#0            ; [CPU_] |539| 
	.dwpsn	file "../i2c.c",line 540,column 11,is_stmt
        B         $C$L102,UNC           ; [CPU_] |540| 
        ; branch occurs ; [] |540| 
$C$L101:    
	.dwpsn	file "../i2c.c",line 541,column 13,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |541| 
        MOV       AH,*-SP[6]            ; [CPU_] |541| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |541| 
        MOVZ      AR4,SP                ; [CPU_U] |541| 
        SUBB      XAR4,#72              ; [CPU_U] |541| 
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_name("_I2C_EepRead")
	.dwattr $C$DW$178, DW_AT_TI_call
        LCR       #_I2C_EepRead         ; [CPU_] |541| 
        ; call occurs [#_I2C_EepRead] ; [] |541| 
        MOV       *+FP[5],AL            ; [CPU_] |541| 
	.dwpsn	file "../i2c.c",line 542,column 13,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |542| 
        BF        $C$L102,NEQ           ; [CPU_] |542| 
        ; branchcc occurs ; [] |542| 
	.dwpsn	file "../i2c.c",line 543,column 15,is_stmt
        INC       *+FP[6]               ; [CPU_] |543| 
	.dwpsn	file "../i2c.c",line 544,column 15,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |544| 
        MOVB      AL,#1                 ; [CPU_] |544| 
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$179, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |544| 
        ; call occurs [#_SEM_pend] ; [] |544| 
$C$L102:    
	.dwpsn	file "../i2c.c",line 540,column 18,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |540| 
        BF        $C$L103,NEQ           ; [CPU_] |540| 
        ; branchcc occurs ; [] |540| 
        MOV       AL,*+FP[6]            ; [CPU_] |540| 
        B         $C$L101,LEQ           ; [CPU_] |540| 
        ; branchcc occurs ; [] |540| 
$C$L103:    
	.dwpsn	file "../i2c.c",line 547,column 11,is_stmt
        MOV       AL,*+FP[5]            ; [CPU_] |547| 
        BF        $C$L108,EQ            ; [CPU_] |547| 
        ; branchcc occurs ; [] |547| 
	.dwpsn	file "../i2c.c",line 548,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |548| 
        MOVB      *+XAR4[0],#12,UNC     ; [CPU_] |548| 
	.dwpsn	file "../i2c.c",line 549,column 18,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       *+FP[7],#0            ; [CPU_] |549| 
        B         $C$L107,UNC           ; [CPU_] |549| 
        ; branch occurs ; [] |549| 
$C$L104:    
	.dwpsn	file "../i2c.c",line 550,column 15,is_stmt
        MOV       AH,*+FP[7]            ; [CPU_] |550| 
        MOVZ      AR4,SP                ; [CPU_U] |550| 
        MOV       AL,AH                 ; [CPU_] |550| 
        LSR       AL,15                 ; [CPU_] |550| 
        SUBB      XAR4,#72              ; [CPU_U] |550| 
        ADD       AL,AH                 ; [CPU_] |550| 
        ASR       AL,1                  ; [CPU_] |550| 
        MOV       ACC,AL                ; [CPU_] |550| 
        ADDL      XAR4,ACC              ; [CPU_] |550| 
        MOV       AH,*+XAR4[0]          ; [CPU_] |550| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |550| 
        ANDB      AH,#0xff              ; [CPU_] |550| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |550| 
        ANDB      AL,#0xff              ; [CPU_] |550| 
        CMP       AH,AL                 ; [CPU_] |550| 
        BF        $C$L105,EQ            ; [CPU_] |550| 
        ; branchcc occurs ; [] |550| 
	.dwpsn	file "../i2c.c",line 551,column 17,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |551| 
        MOVB      *+XAR4[0],#22,UNC     ; [CPU_] |551| 
	.dwpsn	file "../i2c.c",line 552,column 17,is_stmt
        B         $C$L111,UNC           ; [CPU_] |552| 
        ; branch occurs ; [] |552| 
$C$L105:    
	.dwpsn	file "../i2c.c",line 554,column 15,is_stmt
        INC       *+FP[7]               ; [CPU_] |554| 
        MOV       AL,*+FP[7]            ; [CPU_] |554| 
        CMP       AL,*-SP[6]            ; [CPU_] |554| 
        B         $C$L106,HIS           ; [CPU_] |554| 
        ; branchcc occurs ; [] |554| 
	.dwpsn	file "../i2c.c",line 555,column 17,is_stmt
        MOV       AH,*+FP[7]            ; [CPU_] |555| 
        MOVZ      AR4,SP                ; [CPU_U] |555| 
        MOV       AL,AH                 ; [CPU_] |555| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |555| 
        LSR       AL,15                 ; [CPU_] |555| 
        SUBB      XAR4,#72              ; [CPU_U] |555| 
        ADD       AL,AH                 ; [CPU_] |555| 
        ASR       AL,1                  ; [CPU_] |555| 
        MOV       ACC,AL                ; [CPU_] |555| 
        ADDL      XAR4,ACC              ; [CPU_] |555| 
        AND       AH,*+XAR4[0],#0xff00  ; [CPU_] |555| 
        AND       AL,*+XAR5[0],#0xff00  ; [CPU_] |555| 
        CMP       AH,AL                 ; [CPU_] |555| 
        BF        $C$L106,EQ            ; [CPU_] |555| 
        ; branchcc occurs ; [] |555| 
	.dwpsn	file "../i2c.c",line 556,column 19,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |556| 
        MOVB      *+XAR4[0],#22,UNC     ; [CPU_] |556| 
	.dwpsn	file "../i2c.c",line 557,column 19,is_stmt
        B         $C$L111,UNC           ; [CPU_] |557| 
        ; branch occurs ; [] |557| 
$C$L106:    
	.dwpsn	file "../i2c.c",line 560,column 15,is_stmt
        MOVB      ACC,#1                ; [CPU_] |560| 
        ADDL      *-SP[4],ACC           ; [CPU_] |560| 
	.dwpsn	file "../i2c.c",line 549,column 40,is_stmt
        INC       *+FP[7]               ; [CPU_] |549| 
$C$L107:    
	.dwpsn	file "../i2c.c",line 549,column 22,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |549| 
        CMP       AL,*+FP[7]            ; [CPU_] |549| 
        B         $C$L104,HI            ; [CPU_] |549| 
        ; branchcc occurs ; [] |549| 
	.dwpsn	file "../i2c.c",line 562,column 11,is_stmt
        B         $C$L111,UNC           ; [CPU_] |562| 
        ; branch occurs ; [] |562| 
$C$L108:    
	.dwpsn	file "../i2c.c",line 563,column 16,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |563| 
        MOVB      *+XAR4[0],#22,UNC     ; [CPU_] |563| 
	.dwpsn	file "../i2c.c",line 564,column 9,is_stmt
        B         $C$L111,UNC           ; [CPU_] |564| 
        ; branch occurs ; [] |564| 
$C$L109:    
	.dwpsn	file "../i2c.c",line 565,column 14,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |565| 
        MOVB      *+XAR4[0],#21,UNC     ; [CPU_] |565| 
	.dwpsn	file "../i2c.c",line 566,column 9,is_stmt
        B         $C$L111,UNC           ; [CPU_] |566| 
        ; branch occurs ; [] |566| 
$C$L110:    
	.dwpsn	file "../i2c.c",line 497,column 5,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |497| 
        BF        $C$L95,EQ             ; [CPU_] |497| 
        ; branchcc occurs ; [] |497| 
        CMPB      AL,#1                 ; [CPU_] |497| 
        BF        $C$L89,EQ             ; [CPU_] |497| 
        ; branchcc occurs ; [] |497| 
        CMPB      AL,#2                 ; [CPU_] |497| 
        BF        $C$L99,EQ             ; [CPU_] |497| 
        ; branchcc occurs ; [] |497| 
$C$L111:    
	.dwpsn	file "../i2c.c",line 568,column 5,is_stmt
        MOV       ACC,#398              ; [CPU_] |568| 
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("_DSP28x_usDelay")
	.dwattr $C$DW$180, DW_AT_TI_call
        LCR       #_DSP28x_usDelay      ; [CPU_] |568| 
        ; call occurs [#_DSP28x_usDelay] ; [] |568| 
	.dwpsn	file "../i2c.c",line 492,column 9,is_stmt
        B         $C$L85,UNC            ; [CPU_] |492| 
        ; branch occurs ; [] |492| 
	.dwattr $C$DW$165, DW_AT_TI_end_file("../i2c.c")
	.dwattr $C$DW$165, DW_AT_TI_end_line(0x23b)
	.dwattr $C$DW$165, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$165

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_DSP28x_usDelay
	.global	_SEM_pend
	.global	_MBX_pend
	.global	_MBX_post
	.global	_CLK_getltime
	.global	_TSK_timerSem
	.global	_PieCtrlRegs
	.global	_GpioDataRegs
	.global	_I2caRegs
	.global	_mailboxI2C_SendAndReceive
	.global	_GpioCtrlRegs

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x08)
$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$181, DW_AT_name("devsel")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_devsel")
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$182, DW_AT_name("operation")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_operation")
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$183, DW_AT_name("length")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$184, DW_AT_name("address")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$185, DW_AT_name("data")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$186, DW_AT_name("status")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$97	.dwtag  DW_TAG_typedef, DW_AT_name("TI2C_mailbox")
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_name("AIODAT_BITS")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x02)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$187, DW_AT_name("rsvd1")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$187, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$188, DW_AT_name("rsvd2")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$188, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$189, DW_AT_name("AIO2")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_AIO2")
	.dwattr $C$DW$189, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$190, DW_AT_name("rsvd3")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$190, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$191, DW_AT_name("AIO4")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_AIO4")
	.dwattr $C$DW$191, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$192, DW_AT_name("rsvd4")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$192, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$193	.dwtag  DW_TAG_member
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$193, DW_AT_name("AIO6")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_AIO6")
	.dwattr $C$DW$193, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$193, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$193, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$194, DW_AT_name("rsvd5")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$194, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$195, DW_AT_name("rsvd6")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$195, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$196, DW_AT_name("rsvd7")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$196, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$197	.dwtag  DW_TAG_member
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$197, DW_AT_name("AIO10")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_AIO10")
	.dwattr $C$DW$197, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$197, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$197, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$198, DW_AT_name("rsvd8")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$198, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$198, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$199, DW_AT_name("AIO12")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_AIO12")
	.dwattr $C$DW$199, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$200	.dwtag  DW_TAG_member
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$200, DW_AT_name("rsvd9")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$200, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$200, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$201	.dwtag  DW_TAG_member
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$201, DW_AT_name("AIO14")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_AIO14")
	.dwattr $C$DW$201, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$201, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$201, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$202	.dwtag  DW_TAG_member
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$202, DW_AT_name("rsvd10")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$202, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$202, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$203	.dwtag  DW_TAG_member
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$203, DW_AT_name("rsvd11")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_rsvd11")
	.dwattr $C$DW$203, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$203, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$203, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22


$C$DW$T$24	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$24, DW_AT_name("AIODAT_REG")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x02)
$C$DW$204	.dwtag  DW_TAG_member
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$204, DW_AT_name("all")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$204, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$204, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$205	.dwtag  DW_TAG_member
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$205, DW_AT_name("bit")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$205, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$205, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_name("AIO_BITS")
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x02)
$C$DW$206	.dwtag  DW_TAG_member
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$206, DW_AT_name("rsvd1")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$206, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$206, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$207	.dwtag  DW_TAG_member
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$207, DW_AT_name("rsvd2")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$207, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$207, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$208	.dwtag  DW_TAG_member
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$208, DW_AT_name("AIO2")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_AIO2")
	.dwattr $C$DW$208, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$208, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$208, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$209	.dwtag  DW_TAG_member
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$209, DW_AT_name("rsvd3")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$209, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$209, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$210	.dwtag  DW_TAG_member
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$210, DW_AT_name("AIO4")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_AIO4")
	.dwattr $C$DW$210, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$210, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$210, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$211	.dwtag  DW_TAG_member
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$211, DW_AT_name("rsvd4")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$211, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$211, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$211, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$212	.dwtag  DW_TAG_member
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$212, DW_AT_name("AIO6")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_AIO6")
	.dwattr $C$DW$212, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$212, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$212, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$213	.dwtag  DW_TAG_member
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$213, DW_AT_name("rsvd5")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$213, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$213, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$213, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$214	.dwtag  DW_TAG_member
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$214, DW_AT_name("rsvd6")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$214, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$214, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$214, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$215	.dwtag  DW_TAG_member
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$215, DW_AT_name("rsvd7")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$215, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$215, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$215, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$216	.dwtag  DW_TAG_member
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$216, DW_AT_name("AIO10")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_AIO10")
	.dwattr $C$DW$216, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$216, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$216, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$217	.dwtag  DW_TAG_member
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$217, DW_AT_name("rsvd8")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$217, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$217, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$217, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$218	.dwtag  DW_TAG_member
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$218, DW_AT_name("AIO12")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_AIO12")
	.dwattr $C$DW$218, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$218, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$218, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$219	.dwtag  DW_TAG_member
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$219, DW_AT_name("rsvd9")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$219, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$219, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$219, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$220	.dwtag  DW_TAG_member
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$220, DW_AT_name("AIO14")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_AIO14")
	.dwattr $C$DW$220, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$220, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$220, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$221	.dwtag  DW_TAG_member
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$221, DW_AT_name("rsvd10")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$221, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$221, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$221, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$26, DW_AT_name("AIO_REG")
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x02)
$C$DW$222	.dwtag  DW_TAG_member
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$222, DW_AT_name("all")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$222, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$222, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$223	.dwtag  DW_TAG_member
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$223, DW_AT_name("bit")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$223, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$223, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_name("GPA1_BITS")
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x02)
$C$DW$224	.dwtag  DW_TAG_member
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$224, DW_AT_name("GPIO0")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_GPIO0")
	.dwattr $C$DW$224, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$224, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$224, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$225	.dwtag  DW_TAG_member
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$225, DW_AT_name("GPIO1")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_GPIO1")
	.dwattr $C$DW$225, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$225, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$225, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$226	.dwtag  DW_TAG_member
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$226, DW_AT_name("GPIO2")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_GPIO2")
	.dwattr $C$DW$226, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$226, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$226, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$227	.dwtag  DW_TAG_member
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$227, DW_AT_name("GPIO3")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_GPIO3")
	.dwattr $C$DW$227, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$227, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$227, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$228	.dwtag  DW_TAG_member
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$228, DW_AT_name("GPIO4")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_GPIO4")
	.dwattr $C$DW$228, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$228, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$228, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$229	.dwtag  DW_TAG_member
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$229, DW_AT_name("GPIO5")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_GPIO5")
	.dwattr $C$DW$229, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$229, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$229, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$230	.dwtag  DW_TAG_member
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$230, DW_AT_name("GPIO6")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_GPIO6")
	.dwattr $C$DW$230, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$230, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$231, DW_AT_name("GPIO7")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_GPIO7")
	.dwattr $C$DW$231, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$231, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$232	.dwtag  DW_TAG_member
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$232, DW_AT_name("GPIO8")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_GPIO8")
	.dwattr $C$DW$232, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$232, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$233, DW_AT_name("GPIO9")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_GPIO9")
	.dwattr $C$DW$233, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$234	.dwtag  DW_TAG_member
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$234, DW_AT_name("GPIO10")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_GPIO10")
	.dwattr $C$DW$234, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$234, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$235, DW_AT_name("GPIO11")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_GPIO11")
	.dwattr $C$DW$235, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$236	.dwtag  DW_TAG_member
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$236, DW_AT_name("GPIO12")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_GPIO12")
	.dwattr $C$DW$236, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$236, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$236, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$237, DW_AT_name("GPIO13")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_GPIO13")
	.dwattr $C$DW$237, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$238, DW_AT_name("GPIO14")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_GPIO14")
	.dwattr $C$DW$238, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$239, DW_AT_name("GPIO15")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_GPIO15")
	.dwattr $C$DW$239, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27


$C$DW$T$28	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$28, DW_AT_name("GPA1_REG")
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x02)
$C$DW$240	.dwtag  DW_TAG_member
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$240, DW_AT_name("all")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$240, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$240, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$241, DW_AT_name("bit")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28


$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_name("GPA2_BITS")
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x02)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$242, DW_AT_name("GPIO16")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_GPIO16")
	.dwattr $C$DW$242, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$243, DW_AT_name("GPIO17")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_GPIO17")
	.dwattr $C$DW$243, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$244, DW_AT_name("GPIO18")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_GPIO18")
	.dwattr $C$DW$244, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$245, DW_AT_name("GPIO19")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_GPIO19")
	.dwattr $C$DW$245, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$246, DW_AT_name("GPIO20")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_GPIO20")
	.dwattr $C$DW$246, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$247, DW_AT_name("GPIO21")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_GPIO21")
	.dwattr $C$DW$247, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$248, DW_AT_name("GPIO22")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_GPIO22")
	.dwattr $C$DW$248, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$249, DW_AT_name("GPIO23")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_GPIO23")
	.dwattr $C$DW$249, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$250, DW_AT_name("GPIO24")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_GPIO24")
	.dwattr $C$DW$250, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$251, DW_AT_name("GPIO25")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_GPIO25")
	.dwattr $C$DW$251, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$252, DW_AT_name("GPIO26")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_GPIO26")
	.dwattr $C$DW$252, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$253, DW_AT_name("GPIO27")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_GPIO27")
	.dwattr $C$DW$253, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$254, DW_AT_name("GPIO28")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_GPIO28")
	.dwattr $C$DW$254, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$255, DW_AT_name("GPIO29")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_GPIO29")
	.dwattr $C$DW$255, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$256, DW_AT_name("GPIO30")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_GPIO30")
	.dwattr $C$DW$256, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$257, DW_AT_name("GPIO31")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_GPIO31")
	.dwattr $C$DW$257, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$30, DW_AT_name("GPA2_REG")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x02)
$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$258, DW_AT_name("all")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$259, DW_AT_name("bit")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("GPACTRL2_BITS")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$260, DW_AT_name("USB0IOEN")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_USB0IOEN")
	.dwattr $C$DW$260, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$261, DW_AT_name("rsvd1")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$261, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$32, DW_AT_name("GPACTRL2_REG")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x01)
$C$DW$262	.dwtag  DW_TAG_member
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$262, DW_AT_name("all")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$262, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$262, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$263, DW_AT_name("bit")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("GPACTRL_BITS")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x02)
$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$264, DW_AT_name("QUALPRD0")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_QUALPRD0")
	.dwattr $C$DW$264, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$264, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$265, DW_AT_name("QUALPRD1")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_QUALPRD1")
	.dwattr $C$DW$265, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$266	.dwtag  DW_TAG_member
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$266, DW_AT_name("QUALPRD2")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_QUALPRD2")
	.dwattr $C$DW$266, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$266, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$266, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$267	.dwtag  DW_TAG_member
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$267, DW_AT_name("QUALPRD3")
	.dwattr $C$DW$267, DW_AT_TI_symbol_name("_QUALPRD3")
	.dwattr $C$DW$267, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$267, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$267, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$34, DW_AT_name("GPACTRL_REG")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x02)
$C$DW$268	.dwtag  DW_TAG_member
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$268, DW_AT_name("all")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$268, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$268, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$269	.dwtag  DW_TAG_member
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$269, DW_AT_name("bit")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$269, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$269, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$35, DW_AT_name("GPADAT_BITS")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x02)
$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$270, DW_AT_name("GPIO0")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_GPIO0")
	.dwattr $C$DW$270, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$270, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$271	.dwtag  DW_TAG_member
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$271, DW_AT_name("GPIO1")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_GPIO1")
	.dwattr $C$DW$271, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$271, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$272	.dwtag  DW_TAG_member
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$272, DW_AT_name("GPIO2")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_GPIO2")
	.dwattr $C$DW$272, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$272, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$272, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$273	.dwtag  DW_TAG_member
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$273, DW_AT_name("GPIO3")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_GPIO3")
	.dwattr $C$DW$273, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$273, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$274	.dwtag  DW_TAG_member
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$274, DW_AT_name("GPIO4")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_GPIO4")
	.dwattr $C$DW$274, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$274, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$274, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$275	.dwtag  DW_TAG_member
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$275, DW_AT_name("GPIO5")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_GPIO5")
	.dwattr $C$DW$275, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$275, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$275, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$276	.dwtag  DW_TAG_member
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$276, DW_AT_name("GPIO6")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_GPIO6")
	.dwattr $C$DW$276, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$276, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$276, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$277	.dwtag  DW_TAG_member
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$277, DW_AT_name("GPIO7")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_GPIO7")
	.dwattr $C$DW$277, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$277, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$277, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$278	.dwtag  DW_TAG_member
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$278, DW_AT_name("GPIO8")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_GPIO8")
	.dwattr $C$DW$278, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$278, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$278, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$279	.dwtag  DW_TAG_member
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$279, DW_AT_name("GPIO9")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_GPIO9")
	.dwattr $C$DW$279, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$279, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$279, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$280	.dwtag  DW_TAG_member
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$280, DW_AT_name("GPIO10")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_GPIO10")
	.dwattr $C$DW$280, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$280, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$280, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$281	.dwtag  DW_TAG_member
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$281, DW_AT_name("GPIO11")
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_GPIO11")
	.dwattr $C$DW$281, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$281, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$281, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$282	.dwtag  DW_TAG_member
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$282, DW_AT_name("GPIO12")
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_GPIO12")
	.dwattr $C$DW$282, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$282, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$282, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$283	.dwtag  DW_TAG_member
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$283, DW_AT_name("GPIO13")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_GPIO13")
	.dwattr $C$DW$283, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$283, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$283, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$284	.dwtag  DW_TAG_member
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$284, DW_AT_name("GPIO14")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_GPIO14")
	.dwattr $C$DW$284, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$284, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$284, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$285	.dwtag  DW_TAG_member
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$285, DW_AT_name("GPIO15")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_GPIO15")
	.dwattr $C$DW$285, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$285, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$285, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$286	.dwtag  DW_TAG_member
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$286, DW_AT_name("GPIO16")
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_GPIO16")
	.dwattr $C$DW$286, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$286, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$286, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$287	.dwtag  DW_TAG_member
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$287, DW_AT_name("GPIO17")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_GPIO17")
	.dwattr $C$DW$287, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$287, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$287, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$288	.dwtag  DW_TAG_member
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$288, DW_AT_name("GPIO18")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_GPIO18")
	.dwattr $C$DW$288, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$288, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$288, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$289, DW_AT_name("GPIO19")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_GPIO19")
	.dwattr $C$DW$289, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$290, DW_AT_name("GPIO20")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_GPIO20")
	.dwattr $C$DW$290, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$291	.dwtag  DW_TAG_member
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$291, DW_AT_name("GPIO21")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_GPIO21")
	.dwattr $C$DW$291, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$291, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$292	.dwtag  DW_TAG_member
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$292, DW_AT_name("GPIO22")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_GPIO22")
	.dwattr $C$DW$292, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$292, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$292, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$293	.dwtag  DW_TAG_member
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$293, DW_AT_name("GPIO23")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_GPIO23")
	.dwattr $C$DW$293, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$293, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$294, DW_AT_name("GPIO24")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_GPIO24")
	.dwattr $C$DW$294, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$295	.dwtag  DW_TAG_member
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$295, DW_AT_name("GPIO25")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_GPIO25")
	.dwattr $C$DW$295, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$295, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$295, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$296	.dwtag  DW_TAG_member
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$296, DW_AT_name("GPIO26")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_GPIO26")
	.dwattr $C$DW$296, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$296, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$296, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$297, DW_AT_name("GPIO27")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_GPIO27")
	.dwattr $C$DW$297, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$297, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$298	.dwtag  DW_TAG_member
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$298, DW_AT_name("GPIO28")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_GPIO28")
	.dwattr $C$DW$298, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$298, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$299	.dwtag  DW_TAG_member
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$299, DW_AT_name("GPIO29")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_GPIO29")
	.dwattr $C$DW$299, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$299, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$299, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$300	.dwtag  DW_TAG_member
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$300, DW_AT_name("GPIO30")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_GPIO30")
	.dwattr $C$DW$300, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$300, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$301	.dwtag  DW_TAG_member
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$301, DW_AT_name("GPIO31")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_GPIO31")
	.dwattr $C$DW$301, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$301, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$301, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35


$C$DW$T$36	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$36, DW_AT_name("GPADAT_REG")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$302	.dwtag  DW_TAG_member
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$302, DW_AT_name("all")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$302, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$302, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$303	.dwtag  DW_TAG_member
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$303, DW_AT_name("bit")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$303, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$303, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$36


$C$DW$T$37	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$37, DW_AT_name("GPB1_BITS")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x02)
$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$304, DW_AT_name("GPIO32")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_GPIO32")
	.dwattr $C$DW$304, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$304, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$305, DW_AT_name("GPIO33")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_GPIO33")
	.dwattr $C$DW$305, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$305, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$306, DW_AT_name("GPIO34")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_GPIO34")
	.dwattr $C$DW$306, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$307, DW_AT_name("GPIO35")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_GPIO35")
	.dwattr $C$DW$307, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$308, DW_AT_name("GPIO36")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_GPIO36")
	.dwattr $C$DW$308, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$309, DW_AT_name("GPIO37")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_GPIO37")
	.dwattr $C$DW$309, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$310, DW_AT_name("GPIO38")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_GPIO38")
	.dwattr $C$DW$310, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$311, DW_AT_name("GPIO39")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_GPIO39")
	.dwattr $C$DW$311, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$312, DW_AT_name("GPIO40")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_GPIO40")
	.dwattr $C$DW$312, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$312, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$313, DW_AT_name("GPIO41")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_GPIO41")
	.dwattr $C$DW$313, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$314, DW_AT_name("GPIO42")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_GPIO42")
	.dwattr $C$DW$314, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$315, DW_AT_name("GPIO43")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_GPIO43")
	.dwattr $C$DW$315, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$316, DW_AT_name("GPIO44")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_GPIO44")
	.dwattr $C$DW$316, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$317, DW_AT_name("rsvd1")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$317, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x06)
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$37


$C$DW$T$38	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$38, DW_AT_name("GPB1_REG")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x02)
$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$318, DW_AT_name("all")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$319, DW_AT_name("bit")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38


$C$DW$T$39	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$39, DW_AT_name("GPB2_BITS")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x02)
$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$320, DW_AT_name("rsvd1")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$320, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x04)
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$321, DW_AT_name("GPIO50")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_GPIO50")
	.dwattr $C$DW$321, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$322	.dwtag  DW_TAG_member
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$322, DW_AT_name("GPIO51")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_GPIO51")
	.dwattr $C$DW$322, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$322, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$323, DW_AT_name("GPIO52")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_GPIO52")
	.dwattr $C$DW$323, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$324, DW_AT_name("GPIO53")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_GPIO53")
	.dwattr $C$DW$324, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$324, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$325, DW_AT_name("GPIO54")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_GPIO54")
	.dwattr $C$DW$325, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$326, DW_AT_name("GPIO55")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_GPIO55")
	.dwattr $C$DW$326, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$327, DW_AT_name("GPIO56")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_GPIO56")
	.dwattr $C$DW$327, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$328, DW_AT_name("GPIO57")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_GPIO57")
	.dwattr $C$DW$328, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$329, DW_AT_name("GPIO58")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_GPIO58")
	.dwattr $C$DW$329, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$330, DW_AT_name("rsvd2")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$330, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0a)
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$39


$C$DW$T$40	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$40, DW_AT_name("GPB2_REG")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x02)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$331, DW_AT_name("all")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$332, DW_AT_name("bit")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40


$C$DW$T$41	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$41, DW_AT_name("GPBCTRL_BITS")
	.dwattr $C$DW$T$41, DW_AT_byte_size(0x02)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$333, DW_AT_name("QUALPRD0")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_QUALPRD0")
	.dwattr $C$DW$333, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$334, DW_AT_name("QUALPRD1")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_QUALPRD1")
	.dwattr $C$DW$334, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$335, DW_AT_name("QUALPRD2")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_QUALPRD2")
	.dwattr $C$DW$335, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$336, DW_AT_name("QUALPRD3")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_QUALPRD3")
	.dwattr $C$DW$336, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$41


$C$DW$T$42	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$42, DW_AT_name("GPBCTRL_REG")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x02)
$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$337, DW_AT_name("all")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$338, DW_AT_name("bit")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42


$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("GPBDAT_BITS")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x02)
$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$339, DW_AT_name("GPIO32")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_GPIO32")
	.dwattr $C$DW$339, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$340, DW_AT_name("GPIO33")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_GPIO33")
	.dwattr $C$DW$340, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$341, DW_AT_name("GPIO34")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_GPIO34")
	.dwattr $C$DW$341, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$342, DW_AT_name("GPIO35")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_GPIO35")
	.dwattr $C$DW$342, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$343, DW_AT_name("GPIO36")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_GPIO36")
	.dwattr $C$DW$343, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$344, DW_AT_name("GPIO37")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_GPIO37")
	.dwattr $C$DW$344, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$345, DW_AT_name("GPIO38")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_GPIO38")
	.dwattr $C$DW$345, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$346, DW_AT_name("GPIO39")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_GPIO39")
	.dwattr $C$DW$346, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$347, DW_AT_name("GPIO40")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_GPIO40")
	.dwattr $C$DW$347, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$348, DW_AT_name("GPIO41")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_GPIO41")
	.dwattr $C$DW$348, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$349, DW_AT_name("GPIO42")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_GPIO42")
	.dwattr $C$DW$349, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$350, DW_AT_name("GPIO43")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_GPIO43")
	.dwattr $C$DW$350, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$351, DW_AT_name("GPIO44")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_GPIO44")
	.dwattr $C$DW$351, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$352, DW_AT_name("rsvd1")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$352, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x03)
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$353, DW_AT_name("rsvd2")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$353, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$354, DW_AT_name("GPIO50")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_GPIO50")
	.dwattr $C$DW$354, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$355, DW_AT_name("GPIO51")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_GPIO51")
	.dwattr $C$DW$355, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$356, DW_AT_name("GPIO52")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_GPIO52")
	.dwattr $C$DW$356, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$357, DW_AT_name("GPIO53")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_GPIO53")
	.dwattr $C$DW$357, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$358, DW_AT_name("GPIO54")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_GPIO54")
	.dwattr $C$DW$358, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$359, DW_AT_name("GPIO55")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_GPIO55")
	.dwattr $C$DW$359, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$360, DW_AT_name("GPIO56")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_GPIO56")
	.dwattr $C$DW$360, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$361, DW_AT_name("GPIO57")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_GPIO57")
	.dwattr $C$DW$361, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$362, DW_AT_name("GPIO58")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_GPIO58")
	.dwattr $C$DW$362, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$363, DW_AT_name("rsvd3")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$363, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$43


$C$DW$T$44	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$44, DW_AT_name("GPBDAT_REG")
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x02)
$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$364, DW_AT_name("all")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$365, DW_AT_name("bit")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$44


$C$DW$T$48	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$48, DW_AT_name("GPIO_CTRL_REGS")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x40)
$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$366, DW_AT_name("GPACTRL")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_GPACTRL")
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$367, DW_AT_name("GPAQSEL1")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_GPAQSEL1")
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$368, DW_AT_name("GPAQSEL2")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_GPAQSEL2")
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$369, DW_AT_name("GPAMUX1")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_GPAMUX1")
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$370, DW_AT_name("GPAMUX2")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_GPAMUX2")
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$371, DW_AT_name("GPADIR")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_GPADIR")
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$372, DW_AT_name("GPAPUD")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_GPAPUD")
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$373, DW_AT_name("GPACTRL2")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_GPACTRL2")
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$374, DW_AT_name("rsvd1")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$375, DW_AT_name("GPBCTRL")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_GPBCTRL")
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$376	.dwtag  DW_TAG_member
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$376, DW_AT_name("GPBQSEL1")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_GPBQSEL1")
	.dwattr $C$DW$376, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$377, DW_AT_name("GPBQSEL2")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_GPBQSEL2")
	.dwattr $C$DW$377, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$378, DW_AT_name("GPBMUX1")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_GPBMUX1")
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$379, DW_AT_name("GPBMUX2")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_GPBMUX2")
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$380, DW_AT_name("GPBDIR")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_GPBDIR")
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$381, DW_AT_name("GPBPUD")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_GPBPUD")
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$382, DW_AT_name("rsvd2")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$383, DW_AT_name("AIOMUX1")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_AIOMUX1")
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$384, DW_AT_name("rsvd3")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$385	.dwtag  DW_TAG_member
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$385, DW_AT_name("AIODIR")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_AIODIR")
	.dwattr $C$DW$385, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$385, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$386	.dwtag  DW_TAG_member
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$386, DW_AT_name("rsvd4")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$386, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$386, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$48

$C$DW$387	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$48)
$C$DW$T$103	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$387)

$C$DW$T$50	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$50, DW_AT_name("GPIO_DATA_REGS")
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x20)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$388, DW_AT_name("GPADAT")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_GPADAT")
	.dwattr $C$DW$388, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$389, DW_AT_name("GPASET")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_GPASET")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$390, DW_AT_name("GPACLEAR")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_GPACLEAR")
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$391, DW_AT_name("GPATOGGLE")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_GPATOGGLE")
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$392, DW_AT_name("GPBDAT")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_GPBDAT")
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$393, DW_AT_name("GPBSET")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_GPBSET")
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$394, DW_AT_name("GPBCLEAR")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_GPBCLEAR")
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$395, DW_AT_name("GPBTOGGLE")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_GPBTOGGLE")
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$396, DW_AT_name("rsvd1")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$397, DW_AT_name("AIODAT")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_AIODAT")
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$398, DW_AT_name("AIOSET")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_AIOSET")
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$399, DW_AT_name("AIOCLEAR")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_AIOCLEAR")
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$400, DW_AT_name("AIOTOGGLE")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_AIOTOGGLE")
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$50

$C$DW$401	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$50)
$C$DW$T$104	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$401)

$C$DW$T$51	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$51, DW_AT_name("I2CEMDR_BITS")
	.dwattr $C$DW$T$51, DW_AT_byte_size(0x01)
$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$402, DW_AT_name("BC")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_BC")
	.dwattr $C$DW$402, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$403, DW_AT_name("rsvd1")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$403, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$51


$C$DW$T$52	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$52, DW_AT_name("I2CEMDR_REG")
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x01)
$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$404, DW_AT_name("all")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$405, DW_AT_name("bit")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$52


$C$DW$T$53	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$53, DW_AT_name("I2CFFRX_BITS")
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x01)
$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$406, DW_AT_name("RXFFIL")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_RXFFIL")
	.dwattr $C$DW$406, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$407, DW_AT_name("RXFFIENA")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_RXFFIENA")
	.dwattr $C$DW$407, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$408, DW_AT_name("RXFFINTCLR")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_RXFFINTCLR")
	.dwattr $C$DW$408, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$409	.dwtag  DW_TAG_member
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$409, DW_AT_name("RXFFINT")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_RXFFINT")
	.dwattr $C$DW$409, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$409, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$410, DW_AT_name("RXFFST")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_RXFFST")
	.dwattr $C$DW$410, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$411, DW_AT_name("RXFFRST")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_RXFFRST")
	.dwattr $C$DW$411, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$412, DW_AT_name("rsvd1")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$412, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$53


$C$DW$T$54	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$54, DW_AT_name("I2CFFRX_REG")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x01)
$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$413, DW_AT_name("all")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$414, DW_AT_name("bit")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$54


$C$DW$T$55	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$55, DW_AT_name("I2CFFTX_BITS")
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x01)
$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$415, DW_AT_name("TXFFIL")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_TXFFIL")
	.dwattr $C$DW$415, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$416, DW_AT_name("TXFIENA")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_TXFIENA")
	.dwattr $C$DW$416, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$417, DW_AT_name("TXFFINTCLR")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_TXFFINTCLR")
	.dwattr $C$DW$417, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$418, DW_AT_name("TXFFINT")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_TXFFINT")
	.dwattr $C$DW$418, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$419, DW_AT_name("TXFFST")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_TXFFST")
	.dwattr $C$DW$419, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$420	.dwtag  DW_TAG_member
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$420, DW_AT_name("TXFFRST")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_TXFFRST")
	.dwattr $C$DW$420, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$421, DW_AT_name("I2CFFEN")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_I2CFFEN")
	.dwattr $C$DW$421, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$422, DW_AT_name("rsvd1")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$422, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$55


$C$DW$T$56	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$56, DW_AT_name("I2CFFTX_REG")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x01)
$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$423, DW_AT_name("all")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$423, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$424, DW_AT_name("bit")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56


$C$DW$T$57	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$57, DW_AT_name("I2CIER_BITS")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x01)
$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$425, DW_AT_name("ARBL")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_ARBL")
	.dwattr $C$DW$425, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$426, DW_AT_name("NACK")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_NACK")
	.dwattr $C$DW$426, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$427, DW_AT_name("ARDY")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_ARDY")
	.dwattr $C$DW$427, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$428, DW_AT_name("RRDY")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_RRDY")
	.dwattr $C$DW$428, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$429, DW_AT_name("XRDY")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_XRDY")
	.dwattr $C$DW$429, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$430, DW_AT_name("SCD")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_SCD")
	.dwattr $C$DW$430, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$431	.dwtag  DW_TAG_member
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$431, DW_AT_name("AAS")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_AAS")
	.dwattr $C$DW$431, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$432, DW_AT_name("rsvd1")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$432, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x09)
	.dwattr $C$DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$57


$C$DW$T$58	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$58, DW_AT_name("I2CIER_REG")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x01)
$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$433, DW_AT_name("all")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$434, DW_AT_name("bit")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$58


$C$DW$T$59	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$59, DW_AT_name("I2CISRC_BITS")
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x01)
$C$DW$435	.dwtag  DW_TAG_member
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$435, DW_AT_name("INTCODE")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_INTCODE")
	.dwattr $C$DW$435, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$436, DW_AT_name("rsvd1")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$436, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x05)
	.dwattr $C$DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$437, DW_AT_name("rsvd2")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$437, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x04)
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$438, DW_AT_name("rsvd3")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$438, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$59


$C$DW$T$60	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$60, DW_AT_name("I2CISRC_REG")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x01)
$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$439, DW_AT_name("all")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$440, DW_AT_name("bit")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$60


$C$DW$T$61	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$61, DW_AT_name("I2CMDR_BITS")
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x01)
$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$441, DW_AT_name("BC")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_BC")
	.dwattr $C$DW$441, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$442, DW_AT_name("FDF")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_FDF")
	.dwattr $C$DW$442, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$443, DW_AT_name("STB")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_STB")
	.dwattr $C$DW$443, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$444, DW_AT_name("IRS")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_IRS")
	.dwattr $C$DW$444, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$445, DW_AT_name("DLB")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_DLB")
	.dwattr $C$DW$445, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$446, DW_AT_name("RM")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_RM")
	.dwattr $C$DW$446, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$447, DW_AT_name("XA")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_XA")
	.dwattr $C$DW$447, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$448, DW_AT_name("TRX")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_TRX")
	.dwattr $C$DW$448, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$449, DW_AT_name("MST")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_MST")
	.dwattr $C$DW$449, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$450, DW_AT_name("STP")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_STP")
	.dwattr $C$DW$450, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$451, DW_AT_name("rsvd1")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$451, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$452, DW_AT_name("STT")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_STT")
	.dwattr $C$DW$452, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$453, DW_AT_name("FREE")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_FREE")
	.dwattr $C$DW$453, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$454, DW_AT_name("NACKMOD")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_NACKMOD")
	.dwattr $C$DW$454, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$61


$C$DW$T$62	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$62, DW_AT_name("I2CMDR_REG")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)
$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$455, DW_AT_name("all")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$456, DW_AT_name("bit")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$62


$C$DW$T$63	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$63, DW_AT_name("I2CPSC_BITS")
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x01)
$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$457, DW_AT_name("IPSC")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_IPSC")
	.dwattr $C$DW$457, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$458, DW_AT_name("rsvd1")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$458, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$63


$C$DW$T$64	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$64, DW_AT_name("I2CPSC_REG")
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x01)
$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$459, DW_AT_name("all")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$460, DW_AT_name("bit")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$64


$C$DW$T$65	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$65, DW_AT_name("I2CSTR_BITS")
	.dwattr $C$DW$T$65, DW_AT_byte_size(0x01)
$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$461, DW_AT_name("ARBL")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_ARBL")
	.dwattr $C$DW$461, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$462, DW_AT_name("NACK")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_NACK")
	.dwattr $C$DW$462, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$463, DW_AT_name("ARDY")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_ARDY")
	.dwattr $C$DW$463, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$464, DW_AT_name("RRDY")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_RRDY")
	.dwattr $C$DW$464, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$465, DW_AT_name("XRDY")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_XRDY")
	.dwattr $C$DW$465, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$466, DW_AT_name("SCD")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_SCD")
	.dwattr $C$DW$466, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$467, DW_AT_name("rsvd1")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$467, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$468, DW_AT_name("AD0")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_AD0")
	.dwattr $C$DW$468, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$469, DW_AT_name("AAS")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_AAS")
	.dwattr $C$DW$469, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$470, DW_AT_name("XSMT")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_XSMT")
	.dwattr $C$DW$470, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$471, DW_AT_name("RSFULL")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_RSFULL")
	.dwattr $C$DW$471, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$472, DW_AT_name("BB")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_BB")
	.dwattr $C$DW$472, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$473, DW_AT_name("NACKSNT")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_NACKSNT")
	.dwattr $C$DW$473, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$474, DW_AT_name("SDIR")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_SDIR")
	.dwattr $C$DW$474, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$475, DW_AT_name("rsvd2")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$475, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$65


$C$DW$T$66	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$66, DW_AT_name("I2CSTR_REG")
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x01)
$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$476, DW_AT_name("all")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$477, DW_AT_name("bit")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$66


$C$DW$T$68	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$68, DW_AT_name("I2C_REGS")
	.dwattr $C$DW$T$68, DW_AT_byte_size(0x22)
$C$DW$478	.dwtag  DW_TAG_member
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$478, DW_AT_name("I2COAR")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_I2COAR")
	.dwattr $C$DW$478, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$479, DW_AT_name("I2CIER")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_I2CIER")
	.dwattr $C$DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$480, DW_AT_name("I2CSTR")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_I2CSTR")
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$481, DW_AT_name("I2CCLKL")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_I2CCLKL")
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$482, DW_AT_name("I2CCLKH")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_I2CCLKH")
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$483, DW_AT_name("I2CCNT")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_I2CCNT")
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$484, DW_AT_name("I2CDRR")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_I2CDRR")
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$485, DW_AT_name("I2CSAR")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_I2CSAR")
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$486, DW_AT_name("I2CDXR")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_I2CDXR")
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$487, DW_AT_name("I2CMDR")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_I2CMDR")
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$488	.dwtag  DW_TAG_member
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$488, DW_AT_name("I2CISRC")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_I2CISRC")
	.dwattr $C$DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$489	.dwtag  DW_TAG_member
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$489, DW_AT_name("I2CEMDR")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_I2CEMDR")
	.dwattr $C$DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$489, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$490, DW_AT_name("I2CPSC")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_I2CPSC")
	.dwattr $C$DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$491	.dwtag  DW_TAG_member
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$491, DW_AT_name("rsvd1")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$491, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$492	.dwtag  DW_TAG_member
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$492, DW_AT_name("I2CFFTX")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_I2CFFTX")
	.dwattr $C$DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$492, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$493	.dwtag  DW_TAG_member
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$493, DW_AT_name("I2CFFRX")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_I2CFFRX")
	.dwattr $C$DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0x21]
	.dwattr $C$DW$493, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$68

$C$DW$494	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$68)
$C$DW$T$116	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$494)

$C$DW$T$76	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$76, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$76, DW_AT_byte_size(0x08)
$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$495, DW_AT_name("wListElem")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$496, DW_AT_name("wCount")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$497, DW_AT_name("fxn")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$76

$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$71	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$71, DW_AT_address_class(0x16)
$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)

$C$DW$T$83	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$83, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$83, DW_AT_byte_size(0x30)
$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$498, DW_AT_name("dataQue")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$499, DW_AT_name("freeQue")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$500, DW_AT_name("dataSem")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$501, DW_AT_name("freeSem")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$502, DW_AT_name("segid")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$503, DW_AT_name("size")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$504, DW_AT_name("length")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$505, DW_AT_name("name")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$83

$C$DW$T$117	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$T$119	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$119, DW_AT_address_class(0x16)
$C$DW$T$120	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$120, DW_AT_language(DW_LANG_C)

$C$DW$T$84	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$84, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$84, DW_AT_byte_size(0x01)
$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$506, DW_AT_name("ACK1")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$506, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$507, DW_AT_name("ACK2")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$507, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$508, DW_AT_name("ACK3")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$508, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$509, DW_AT_name("ACK4")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$509, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$510, DW_AT_name("ACK5")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$510, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$511, DW_AT_name("ACK6")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$511, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$512, DW_AT_name("ACK7")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$512, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$513, DW_AT_name("ACK8")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$513, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$514, DW_AT_name("ACK9")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$514, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$515, DW_AT_name("ACK10")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$515, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$516, DW_AT_name("ACK11")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$516, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$517, DW_AT_name("ACK12")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$517, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$518, DW_AT_name("rsvd1")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$518, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$84


$C$DW$T$85	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$85, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x01)
$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$519, DW_AT_name("all")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$520, DW_AT_name("bit")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$85


$C$DW$T$86	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$86, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$86, DW_AT_byte_size(0x01)
$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$521, DW_AT_name("ENPIE")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$521, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$522, DW_AT_name("PIEVECT")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$522, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$86


$C$DW$T$87	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$87, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$87, DW_AT_byte_size(0x01)
$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$523, DW_AT_name("all")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$524, DW_AT_name("bit")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$87


$C$DW$T$88	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$88, DW_AT_name("PIEIER_BITS")
	.dwattr $C$DW$T$88, DW_AT_byte_size(0x01)
$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$525, DW_AT_name("INTx1")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$525, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$526, DW_AT_name("INTx2")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$526, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$527, DW_AT_name("INTx3")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$527, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$528, DW_AT_name("INTx4")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$528, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$529, DW_AT_name("INTx5")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$529, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$530, DW_AT_name("INTx6")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$530, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$531, DW_AT_name("INTx7")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$531, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$532, DW_AT_name("INTx8")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$532, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$533, DW_AT_name("rsvd1")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$533, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$88


$C$DW$T$89	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$89, DW_AT_name("PIEIER_REG")
	.dwattr $C$DW$T$89, DW_AT_byte_size(0x01)
$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$534, DW_AT_name("all")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$535, DW_AT_name("bit")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$89


$C$DW$T$90	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$90, DW_AT_name("PIEIFR_BITS")
	.dwattr $C$DW$T$90, DW_AT_byte_size(0x01)
$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$536, DW_AT_name("INTx1")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$536, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$537, DW_AT_name("INTx2")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$537, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$538, DW_AT_name("INTx3")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$538, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$539, DW_AT_name("INTx4")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$539, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$540, DW_AT_name("INTx5")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$540, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$541, DW_AT_name("INTx6")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$541, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$542, DW_AT_name("INTx7")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$542, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$543, DW_AT_name("INTx8")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$543, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$544, DW_AT_name("rsvd1")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$544, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$90


$C$DW$T$91	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$91, DW_AT_name("PIEIFR_REG")
	.dwattr $C$DW$T$91, DW_AT_byte_size(0x01)
$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$545, DW_AT_name("all")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$546, DW_AT_name("bit")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$91


$C$DW$T$92	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$92, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$92, DW_AT_byte_size(0x1a)
$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$547, DW_AT_name("PIECTRL")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$548, DW_AT_name("PIEACK")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$549, DW_AT_name("PIEIER1")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$550, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$551, DW_AT_name("PIEIER2")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$552, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$553, DW_AT_name("PIEIER3")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$554, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$555, DW_AT_name("PIEIER4")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$556, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$557, DW_AT_name("PIEIER5")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$558, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$559, DW_AT_name("PIEIER6")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$560, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$561, DW_AT_name("PIEIER7")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$562, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$563, DW_AT_name("PIEIER8")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$564, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$565, DW_AT_name("PIEIER9")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$566, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$567, DW_AT_name("PIEIER10")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$568, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$569, DW_AT_name("PIEIER11")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$570, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$571, DW_AT_name("PIEIER12")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$572, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$92

$C$DW$573	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$92)
$C$DW$T$122	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$573)

$C$DW$T$94	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$94, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$94, DW_AT_byte_size(0x04)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$574, DW_AT_name("next")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$575, DW_AT_name("prev")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$94

$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$T$93	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$93, DW_AT_address_class(0x16)

$C$DW$T$96	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$96, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x10)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$576, DW_AT_name("job")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$577, DW_AT_name("count")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$578, DW_AT_name("pendQ")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$579, DW_AT_name("name")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$96

$C$DW$T$77	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$77, DW_AT_language(DW_LANG_C)
$C$DW$T$124	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$124, DW_AT_address_class(0x16)
$C$DW$T$125	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$126	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)

$C$DW$T$73	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$73, DW_AT_language(DW_LANG_C)
$C$DW$580	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$72)
	.dwendtag $C$DW$T$73

$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$T$75	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$75, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$19	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_address_class(0x16)

$C$DW$T$136	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$136, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$136, DW_AT_byte_size(0x40)
$C$DW$581	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$581, DW_AT_upper_bound(0x3f)
	.dwendtag $C$DW$T$136

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$137	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$137, DW_AT_language(DW_LANG_C)
$C$DW$T$130	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$130, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$21	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)

$C$DW$T$45	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$45, DW_AT_byte_size(0x18)
$C$DW$582	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$582, DW_AT_upper_bound(0x17)
	.dwendtag $C$DW$T$45


$C$DW$T$46	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x02)
$C$DW$583	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$583, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$46


$C$DW$T$47	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$47, DW_AT_byte_size(0x04)
$C$DW$584	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$584, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$47


$C$DW$T$49	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x08)
$C$DW$585	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$585, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$49


$C$DW$T$67	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x13)
$C$DW$586	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$586, DW_AT_upper_bound(0x12)
	.dwendtag $C$DW$T$67

$C$DW$T$79	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$148	.dwtag  DW_TAG_typedef, DW_AT_name("LgUns")
	.dwattr $C$DW$T$148, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$148, DW_AT_language(DW_LANG_C)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("Uint32")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$81	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$81, DW_AT_address_class(0x16)
$C$DW$T$82	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$587	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$587, DW_AT_location[DW_OP_reg0]
$C$DW$588	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$588, DW_AT_location[DW_OP_reg1]
$C$DW$589	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$589, DW_AT_location[DW_OP_reg2]
$C$DW$590	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$590, DW_AT_location[DW_OP_reg3]
$C$DW$591	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$591, DW_AT_location[DW_OP_reg20]
$C$DW$592	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$592, DW_AT_location[DW_OP_reg21]
$C$DW$593	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$593, DW_AT_location[DW_OP_reg22]
$C$DW$594	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$594, DW_AT_location[DW_OP_reg23]
$C$DW$595	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$595, DW_AT_location[DW_OP_reg24]
$C$DW$596	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$596, DW_AT_location[DW_OP_reg25]
$C$DW$597	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$597, DW_AT_location[DW_OP_reg26]
$C$DW$598	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$598, DW_AT_location[DW_OP_reg28]
$C$DW$599	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$599, DW_AT_location[DW_OP_reg29]
$C$DW$600	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$600, DW_AT_location[DW_OP_reg30]
$C$DW$601	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$601, DW_AT_location[DW_OP_reg31]
$C$DW$602	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$602, DW_AT_location[DW_OP_regx 0x20]
$C$DW$603	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$603, DW_AT_location[DW_OP_regx 0x21]
$C$DW$604	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$604, DW_AT_location[DW_OP_regx 0x22]
$C$DW$605	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$605, DW_AT_location[DW_OP_regx 0x23]
$C$DW$606	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$606, DW_AT_location[DW_OP_regx 0x24]
$C$DW$607	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$607, DW_AT_location[DW_OP_regx 0x25]
$C$DW$608	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$608, DW_AT_location[DW_OP_regx 0x26]
$C$DW$609	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$609, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$610	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$610, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$611	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$611, DW_AT_location[DW_OP_reg4]
$C$DW$612	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$612, DW_AT_location[DW_OP_reg6]
$C$DW$613	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$613, DW_AT_location[DW_OP_reg8]
$C$DW$614	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$614, DW_AT_location[DW_OP_reg10]
$C$DW$615	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$615, DW_AT_location[DW_OP_reg12]
$C$DW$616	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$616, DW_AT_location[DW_OP_reg14]
$C$DW$617	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$617, DW_AT_location[DW_OP_reg16]
$C$DW$618	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$618, DW_AT_location[DW_OP_reg17]
$C$DW$619	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$619, DW_AT_location[DW_OP_reg18]
$C$DW$620	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$620, DW_AT_location[DW_OP_reg19]
$C$DW$621	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$621, DW_AT_location[DW_OP_reg5]
$C$DW$622	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$622, DW_AT_location[DW_OP_reg7]
$C$DW$623	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$623, DW_AT_location[DW_OP_reg9]
$C$DW$624	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$624, DW_AT_location[DW_OP_reg11]
$C$DW$625	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$625, DW_AT_location[DW_OP_reg13]
$C$DW$626	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$626, DW_AT_location[DW_OP_reg15]
$C$DW$627	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$627, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$628	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$628, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$629	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$629, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$630	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$630, DW_AT_location[DW_OP_regx 0x30]
$C$DW$631	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$631, DW_AT_location[DW_OP_regx 0x33]
$C$DW$632	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$632, DW_AT_location[DW_OP_regx 0x34]
$C$DW$633	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$633, DW_AT_location[DW_OP_regx 0x37]
$C$DW$634	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$634, DW_AT_location[DW_OP_regx 0x38]
$C$DW$635	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$635, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$636	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$636, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$637	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$637, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$638	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$638, DW_AT_location[DW_OP_regx 0x40]
$C$DW$639	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$639, DW_AT_location[DW_OP_regx 0x43]
$C$DW$640	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$640, DW_AT_location[DW_OP_regx 0x44]
$C$DW$641	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$641, DW_AT_location[DW_OP_regx 0x47]
$C$DW$642	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$642, DW_AT_location[DW_OP_regx 0x48]
$C$DW$643	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$643, DW_AT_location[DW_OP_regx 0x49]
$C$DW$644	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$644, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$645	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$645, DW_AT_location[DW_OP_regx 0x27]
$C$DW$646	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$646, DW_AT_location[DW_OP_regx 0x28]
$C$DW$647	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$647, DW_AT_location[DW_OP_reg27]
$C$DW$648	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$648, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

