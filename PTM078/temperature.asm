;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue May 25 14:03:58 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../temperature.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\EBuilding\PTM078")
	.global	_TempTable
	.sect	".econst:_TempTable"
	.clink
	.align	2
_TempTable:
	.bits	-55,16			; _TempTable[0]._temp @ 0
	.space	16
	.xfloat	$strtod("0x1.fffcccp+11")		; _TempTable[0]._adval[0] @ 32
	.xfloat	$strtod("0x1.fffcccp+11")		; _TempTable[0]._adval[1] @ 64
	.bits	-50,16			; _TempTable[1]._temp @ 96
	.space	16
	.xfloat	$strtod("0x1.f8599ap+11")		; _TempTable[1]._adval[0] @ 128
	.xfloat	$strtod("0x1.f0ccccp+11")		; _TempTable[1]._adval[1] @ 160
	.bits	-45,16			; _TempTable[2]._temp @ 192
	.space	16
	.xfloat	$strtod("0x1.f54p+11")		; _TempTable[2]._adval[0] @ 224
	.xfloat	$strtod("0x1.ec0334p+11")		; _TempTable[2]._adval[1] @ 256
	.bits	-40,16			; _TempTable[3]._temp @ 288
	.space	16
	.xfloat	$strtod("0x1.f1199ap+11")		; _TempTable[3]._adval[0] @ 320
	.xfloat	$strtod("0x1.e61666p+11")		; _TempTable[3]._adval[1] @ 352
	.bits	-35,16			; _TempTable[4]._temp @ 384
	.space	16
	.xfloat	$strtod("0x1.eb9cccp+11")		; _TempTable[4]._adval[0] @ 416
	.xfloat	$strtod("0x1.dea99ap+11")		; _TempTable[4]._adval[1] @ 448
	.bits	-30,16			; _TempTable[5]._temp @ 480
	.space	16
	.xfloat	$strtod("0x1.e48p+11")		; _TempTable[5]._adval[0] @ 512
	.xfloat	$strtod("0x1.d5acccp+11")		; _TempTable[5]._adval[1] @ 544
	.bits	-25,16			; _TempTable[6]._temp @ 576
	.space	16
	.xfloat	$strtod("0x1.db699ap+11")		; _TempTable[6]._adval[0] @ 608
	.xfloat	$strtod("0x1.cac99ap+11")		; _TempTable[6]._adval[1] @ 640
	.bits	-20,16			; _TempTable[7]._temp @ 672
	.space	16
	.xfloat	$strtod("0x1.d01p+11")		; _TempTable[7]._adval[0] @ 704
	.xfloat	$strtod("0x1.be0cccp+11")		; _TempTable[7]._adval[1] @ 736
	.bits	-15,16			; _TempTable[8]._temp @ 768
	.space	16
	.xfloat	$strtod("0x1.c22666p+11")		; _TempTable[8]._adval[0] @ 800
	.xfloat	$strtod("0x1.af2666p+11")		; _TempTable[8]._adval[1] @ 832
	.bits	-10,16			; _TempTable[9]._temp @ 864
	.space	16
	.xfloat	$strtod("0x1.b18666p+11")		; _TempTable[9]._adval[0] @ 896
	.xfloat	$strtod("0x1.9e5334p+11")		; _TempTable[9]._adval[1] @ 928
	.bits	-5,16			; _TempTable[10]._temp @ 960
	.space	16
	.xfloat	$strtod("0x1.9e099ap+11")		; _TempTable[10]._adval[0] @ 992
	.xfloat	$strtod("0x1.8b4666p+11")		; _TempTable[10]._adval[1] @ 1024
	.bits	0,16			; _TempTable[11]._temp @ 1056
	.space	16
	.xfloat	$strtod("0x1.87dcccp+11")		; _TempTable[11]._adval[0] @ 1088
	.xfloat	$strtod("0x1.769334p+11")		; _TempTable[11]._adval[1] @ 1120
	.bits	5,16			; _TempTable[12]._temp @ 1152
	.space	16
	.xfloat	$strtod("0x1.6f3cccp+11")		; _TempTable[12]._adval[0] @ 1184
	.xfloat	$strtod("0x1.60299ap+11")		; _TempTable[12]._adval[1] @ 1216
	.bits	10,16			; _TempTable[13]._temp @ 1248
	.space	16
	.xfloat	$strtod("0x1.54acccp+11")		; _TempTable[13]._adval[0] @ 1280
	.xfloat	$strtod("0x1.48ccccp+11")		; _TempTable[13]._adval[1] @ 1312
	.bits	15,16			; _TempTable[14]._temp @ 1344
	.space	16
	.xfloat	$strtod("0x1.38c666p+11")		; _TempTable[14]._adval[0] @ 1376
	.xfloat	$strtod("0x1.308cccp+11")		; _TempTable[14]._adval[1] @ 1408
	.bits	20,16			; _TempTable[15]._temp @ 1440
	.space	16
	.xfloat	$strtod("0x1.1c4666p+11")		; _TempTable[15]._adval[0] @ 1472
	.xfloat	$strtod("0x1.182666p+11")		; _TempTable[15]._adval[1] @ 1504
	.bits	25,16			; _TempTable[16]._temp @ 1536
	.space	16
	.xfloat	$strtod("0x1.ffep+10")		; _TempTable[16]._adval[0] @ 1568
	.xfloat	$strtod("0x1.ffep+10")		; _TempTable[16]._adval[1] @ 1600
	.bits	30,16			; _TempTable[17]._temp @ 1632
	.space	16
	.xfloat	$strtod("0x1.c8ccccp+10")		; _TempTable[17]._adval[0] @ 1664
	.xfloat	$strtod("0x1.d0b99ap+10")		; _TempTable[17]._adval[1] @ 1696
	.bits	35,16			; _TempTable[18]._temp @ 1728
	.space	16
	.xfloat	$strtod("0x1.947334p+10")		; _TempTable[18]._adval[0] @ 1760
	.xfloat	$strtod("0x1.a36666p+10")		; _TempTable[18]._adval[1] @ 1792
	.bits	40,16			; _TempTable[19]._temp @ 1824
	.space	16
	.xfloat	$strtod("0x1.63ccccp+10")		; _TempTable[19]._adval[0] @ 1856
	.xfloat	$strtod("0x1.78e666p+10")		; _TempTable[19]._adval[1] @ 1888
	.bits	45,16			; _TempTable[20]._temp @ 1920
	.space	16
	.xfloat	$strtod("0x1.374666p+10")		; _TempTable[20]._adval[0] @ 1952
	.xfloat	$strtod("0x1.512cccp+10")		; _TempTable[20]._adval[1] @ 1984
	.bits	50,16			; _TempTable[21]._temp @ 2016
	.space	16
	.xfloat	$strtod("0x1.0f2666p+10")		; _TempTable[21]._adval[0] @ 2048
	.xfloat	$strtod("0x1.2cc666p+10")		; _TempTable[21]._adval[1] @ 2080
	.bits	55,16			; _TempTable[22]._temp @ 2112
	.space	16
	.xfloat	$strtod("0x1.d6ccccp+9")		; _TempTable[22]._adval[0] @ 2144
	.xfloat	$strtod("0x1.0b6cccp+10")		; _TempTable[22]._adval[1] @ 2176
	.bits	60,16			; _TempTable[23]._temp @ 2208
	.space	16
	.xfloat	$strtod("0x1.97f334p+9")		; _TempTable[23]._adval[0] @ 2240
	.xfloat	$strtod("0x1.dae666p+9")		; _TempTable[23]._adval[1] @ 2272
	.bits	65,16			; _TempTable[24]._temp @ 2304
	.space	16
	.xfloat	$strtod("0x1.60f334p+9")		; _TempTable[24]._adval[0] @ 2336
	.xfloat	$strtod("0x1.a4f334p+9")		; _TempTable[24]._adval[1] @ 2368
	.bits	70,16			; _TempTable[25]._temp @ 2400
	.space	16
	.xfloat	$strtod("0x1.314p+9")		; _TempTable[25]._adval[0] @ 2432
	.xfloat	$strtod("0x1.750cccp+9")		; _TempTable[25]._adval[1] @ 2464
	.bits	75,16			; _TempTable[26]._temp @ 2496
	.space	16
	.xfloat	$strtod("0x1.08199ap+9")		; _TempTable[26]._adval[0] @ 2528
	.xfloat	$strtod("0x1.4a599ap+9")		; _TempTable[26]._adval[1] @ 2560
	.bits	80,16			; _TempTable[27]._temp @ 2592
	.space	16
	.xfloat	$strtod("0x1.c9999ap+8")		; _TempTable[27]._adval[0] @ 2624
	.xfloat	$strtod("0x1.24b334p+9")		; _TempTable[27]._adval[1] @ 2656
	.bits	85,16			; _TempTable[28]._temp @ 2688
	.space	16
	.xfloat	$strtod("0x1.8c8p+8")		; _TempTable[28]._adval[0] @ 2720
	.xfloat	$strtod("0x1.037334p+9")		; _TempTable[28]._adval[1] @ 2752
	.bits	90,16			; _TempTable[29]._temp @ 2784
	.space	16
	.xfloat	$strtod("0x1.583334p+8")		; _TempTable[29]._adval[0] @ 2816
	.xfloat	$strtod("0x1.cc3334p+8")		; _TempTable[29]._adval[1] @ 2848
	.bits	95,16			; _TempTable[30]._temp @ 2880
	.space	16
	.xfloat	$strtod("0x1.2b4cccp+8")		; _TempTable[30]._adval[0] @ 2912
	.xfloat	$strtod("0x1.988p+8")		; _TempTable[30]._adval[1] @ 2944
	.bits	100,16			; _TempTable[31]._temp @ 2976
	.space	16
	.xfloat	$strtod("0x1.04b334p+8")		; _TempTable[31]._adval[0] @ 3008
	.xfloat	$strtod("0x1.6b199ap+8")		; _TempTable[31]._adval[1] @ 3040
	.bits	105,16			; _TempTable[32]._temp @ 3072
	.space	16
	.xfloat	$strtod("0x1.c73334p+7")		; _TempTable[32]._adval[0] @ 3104
	.xfloat	$strtod("0x1.434cccp+8")		; _TempTable[32]._adval[1] @ 3136
	.bits	110,16			; _TempTable[33]._temp @ 3168
	.space	16
	.xfloat	$strtod("0x1.8e6666p+7")		; _TempTable[33]._adval[0] @ 3200
	.xfloat	$strtod("0x1.206666p+8")		; _TempTable[33]._adval[1] @ 3232
	.bits	115,16			; _TempTable[34]._temp @ 3264
	.space	16
	.xfloat	$strtod("0x1.5d3334p+7")		; _TempTable[34]._adval[0] @ 3296
	.xfloat	$strtod("0x1.fcp+7")		; _TempTable[34]._adval[1] @ 3328
	.bits	120,16			; _TempTable[35]._temp @ 3360
	.space	16
	.xfloat	$strtod("0x1.32ccccp+7")		; _TempTable[35]._adval[0] @ 3392
	.xfloat	$strtod("0x1.c2p+7")		; _TempTable[35]._adval[1] @ 3424
	.bits	125,16			; _TempTable[36]._temp @ 3456
	.space	16
	.xfloat	$strtod("0x0p+0")		; _TempTable[36]._adval[0] @ 3488
	.xfloat	$strtod("0x0p+0")		; _TempTable[36]._adval[1] @ 3520

$C$DW$1	.dwtag  DW_TAG_variable, DW_AT_name("TempTable")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_TempTable")
	.dwattr $C$DW$1, DW_AT_location[DW_OP_addr _TempTable]
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2352812 

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x06)
$C$DW$2	.dwtag  DW_TAG_member
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$2, DW_AT_name("temp")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$2, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$2, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$3	.dwtag  DW_TAG_member
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$3, DW_AT_name("adval")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_adval")
	.dwattr $C$DW$3, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$3, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$21	.dwtag  DW_TAG_typedef, DW_AT_name("TTempTable")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)
$C$DW$4	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$21)
$C$DW$T$22	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$4)

$C$DW$T$23	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$23, DW_AT_byte_size(0xde)
$C$DW$5	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$5, DW_AT_upper_bound(0x24)
	.dwendtag $C$DW$T$23

$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x04)
$C$DW$6	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$6, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$19

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	undefined, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	undefined, 6
	.dwcfi	undefined, 8
	.dwcfi	undefined, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	undefined, 7
	.dwcfi	undefined, 9
	.dwcfi	undefined, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	undefined, 59
	.dwcfi	undefined, 60
	.dwcfi	undefined, 63
	.dwcfi	undefined, 64
	.dwcfi	undefined, 67
	.dwcfi	undefined, 68
	.dwcfi	undefined, 71
	.dwcfi	undefined, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$7	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$7, DW_AT_location[DW_OP_reg0]
$C$DW$8	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$8, DW_AT_location[DW_OP_reg1]
$C$DW$9	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$9, DW_AT_location[DW_OP_reg2]
$C$DW$10	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg3]
$C$DW$11	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$11, DW_AT_location[DW_OP_reg20]
$C$DW$12	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg21]
$C$DW$13	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg22]
$C$DW$14	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$14, DW_AT_location[DW_OP_reg23]
$C$DW$15	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$15, DW_AT_location[DW_OP_reg24]
$C$DW$16	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg25]
$C$DW$17	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg26]
$C$DW$18	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg28]
$C$DW$19	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$19, DW_AT_location[DW_OP_reg29]
$C$DW$20	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$20, DW_AT_location[DW_OP_reg30]
$C$DW$21	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg31]
$C$DW$22	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_regx 0x20]
$C$DW$23	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$23, DW_AT_location[DW_OP_regx 0x21]
$C$DW$24	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_regx 0x22]
$C$DW$25	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$25, DW_AT_location[DW_OP_regx 0x23]
$C$DW$26	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_regx 0x24]
$C$DW$27	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_regx 0x25]
$C$DW$28	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_regx 0x26]
$C$DW$29	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDO")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$30	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$31	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$32	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg4]
$C$DW$33	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg6]
$C$DW$34	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg8]
$C$DW$35	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg10]
$C$DW$36	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg12]
$C$DW$37	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg14]
$C$DW$38	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg16]
$C$DW$39	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg17]
$C$DW$40	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg18]
$C$DW$41	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg19]
$C$DW$42	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg5]
$C$DW$43	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg7]
$C$DW$44	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg9]
$C$DW$45	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg11]
$C$DW$46	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg13]
$C$DW$47	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg15]
$C$DW$48	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$49	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_regx 0x30]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_regx 0x33]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_regx 0x34]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_regx 0x37]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_regx 0x38]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_regx 0x40]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_regx 0x43]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_regx 0x44]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_regx 0x47]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_regx 0x48]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x49]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x27]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x28]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg27]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

