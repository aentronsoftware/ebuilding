;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue May 25 14:03:57 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../recorder.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\EBuilding\PTM078")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_DataIndex+0,32
	.bits	0,16			; _DataIndex @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_REC_PosSampleInterval+0,32
	.bits	1,16			; _REC_PosSampleInterval @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_BitIndex+0,32
	.bits	0,32			; _BitIndex @ 0

	.sect	".cinit"
	.align	1
	.field  	-4,16
	.field  	_REC_RecPoint+0,32
	.bits		0,32
	.bits		0,32			; _REC_RecPoint @ 0

	.sect	".cinit"
	.align	1
	.field  	-4,16
	.field  	_REC_OldRecPoint+0,32
	.bits		0,32
	.bits		0,32			; _REC_OldRecPoint @ 0

$C$DW$1	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_ReadIndex")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_ODV_Recorder_ReadIndex")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Start")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_ODV_Recorder_Start")
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_PreTrigger")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_ODV_Recorder_PreTrigger")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_TriggerIndex")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_ODV_Recorder_TriggerIndex")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Control")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_ODV_Recorder_Control")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
	.global	_DataIndex
_DataIndex:	.usect	".ebss",1,1,0
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("DataIndex")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_DataIndex")
	.dwattr $C$DW$6, DW_AT_location[DW_OP_addr _DataIndex]
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$6, DW_AT_external
	.global	_REC_PosSampleInterval
_REC_PosSampleInterval:	.usect	".ebss",1,1,0
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("REC_PosSampleInterval")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_REC_PosSampleInterval")
	.dwattr $C$DW$7, DW_AT_location[DW_OP_addr _REC_PosSampleInterval]
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$7, DW_AT_external
	.global	_gaindelta
	.sect	".econst"
	.align	2
_gaindelta:
	.xfloat	$strtod("0x1.4p+4")		; _gaindelta @ 0

$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("gaindelta")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_gaindelta")
	.dwattr $C$DW$8, DW_AT_location[DW_OP_addr _gaindelta]
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$8, DW_AT_external
	.global	_gain_speed
	.sect	".econst"
	.align	2
_gain_speed:
	.xfloat	$strtod("0x1.4p+2")		; _gain_speed @ 0

$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("gain_speed")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_gain_speed")
	.dwattr $C$DW$9, DW_AT_location[DW_OP_addr _gain_speed]
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$9, DW_AT_external
	.global	_gaincell
	.sect	".econst"
	.align	2
_gaincell:
	.xfloat	$strtod("0x1.388272p-3")		; _gaincell @ 0

$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("gaincell")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_gaincell")
	.dwattr $C$DW$10, DW_AT_location[DW_OP_addr _gaincell]
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$10, DW_AT_external
	.global	_gainpack
	.sect	".econst"
	.align	2
_gainpack:
	.xfloat	$strtod("0x1.e849e8p+0")		; _gainpack @ 0

$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("gainpack")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_gainpack")
	.dwattr $C$DW$11, DW_AT_location[DW_OP_addr _gainpack]
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$11, DW_AT_external
	.global	_gainsec
	.sect	".econst"
	.align	2
_gainsec:
	.xfloat	$strtod("0x1.111112p-6")		; _gainsec @ 0

$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("gainsec")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_gainsec")
	.dwattr $C$DW$12, DW_AT_location[DW_OP_addr _gainsec]
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$12, DW_AT_external
	.global	_gainI2
	.sect	".econst"
	.align	2
_gainI2:
	.xfloat	$strtod("0x1.4p-2")		; _gainI2 @ 0

$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("gainI2")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_gainI2")
	.dwattr $C$DW$13, DW_AT_location[DW_OP_addr _gainI2]
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$13, DW_AT_external
	.global	_gainvalve
	.sect	".econst"
	.align	2
_gainvalve:
	.xfloat	$strtod("0x1.9c99cap+0")		; _gainvalve @ 0

$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("gainvalve")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_gainvalve")
	.dwattr $C$DW$14, DW_AT_location[DW_OP_addr _gainvalve]
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$14, DW_AT_external
	.global	_inc0001
	.sect	".econst"
	.align	2
_inc0001:
	.xfloat	$strtod("0x1.0624dep-10")		; _inc0001 @ 0

$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("inc0001")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_inc0001")
	.dwattr $C$DW$15, DW_AT_location[DW_OP_addr _inc0001]
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$15, DW_AT_external
	.global	_gain01
	.sect	".econst"
	.align	2
_gain01:
	.xfloat	$strtod("0x1.99999ap-4")		; _gain01 @ 0

$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("gain01")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_gain01")
	.dwattr $C$DW$16, DW_AT_location[DW_OP_addr _gain01]
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$16, DW_AT_external
	.global	_VAR_INC1
	.sect	".econst"
	.align	2
_VAR_INC1:
	.xfloat	$strtod("0x1p+0")		; _VAR_INC1 @ 0

$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("VAR_INC1")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_VAR_INC1")
	.dwattr $C$DW$17, DW_AT_location[DW_OP_addr _VAR_INC1]
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$17, DW_AT_external
	.global	_VAR_OFFSET_NULL
	.sect	".econst"
	.align	2
_VAR_OFFSET_NULL:
	.xfloat	$strtod("0x0p+0")		; _VAR_OFFSET_NULL @ 0

$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("VAR_OFFSET_NULL")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_VAR_OFFSET_NULL")
	.dwattr $C$DW$18, DW_AT_location[DW_OP_addr _VAR_OFFSET_NULL]
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$18, DW_AT_external
	.global	_VAR_INC0_1
	.sect	".econst"
	.align	2
_VAR_INC0_1:
	.xfloat	$strtod("0x1.99999ap-4")		; _VAR_INC0_1 @ 0

$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("VAR_INC0_1")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_VAR_INC0_1")
	.dwattr $C$DW$19, DW_AT_location[DW_OP_addr _VAR_INC0_1]
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$19, DW_AT_external
	.global	_BitIndex
_BitIndex:	.usect	".ebss",2,1,1
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("BitIndex")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_BitIndex")
	.dwattr $C$DW$20, DW_AT_location[DW_OP_addr _BitIndex]
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$20, DW_AT_external
	.global	_offsettemper
	.sect	".econst"
	.align	2
_offsettemper:
	.xfloat	$strtod("-0x1.9p+5")		; _offsettemper @ 0

$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("offsettemper")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_offsettemper")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_addr _offsettemper]
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$21, DW_AT_external
	.global	_gain_rpm
	.sect	".econst"
	.align	2
_gain_rpm:
	.xfloat	$strtod("0x1.ep+3")		; _gain_rpm @ 0

$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("gain_rpm")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_gain_rpm")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_addr _gain_rpm]
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$22, DW_AT_external
	.global	_VAR_GAIN_NORM
	.sect	".econst"
	.align	2
_VAR_GAIN_NORM:
	.xfloat	$strtod("0x1p+0")		; _VAR_GAIN_NORM @ 0

$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("VAR_GAIN_NORM")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_VAR_GAIN_NORM")
	.dwattr $C$DW$23, DW_AT_location[DW_OP_addr _VAR_GAIN_NORM]
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$23, DW_AT_external
	.global	_offsetad2
	.sect	".econst"
	.align	2
_offsetad2:
	.xfloat	$strtod("0x1p+11")		; _offsetad2 @ 0

$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("offsetad2")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_offsetad2")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_addr _offsetad2]
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$24, DW_AT_external

$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("_getODentry")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("__getODentry")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external
$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$71)
$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$9)
$C$DW$28	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$6)
$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$3)
$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$26)
$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$49)
$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$6)
$C$DW$33	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$6)
$C$DW$34	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$25

$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SysTick_ms")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_ODV_SysTick_ms")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external
	.global	_gain3
	.sect	".econst"
	.align	2
_gain3:
	.xfloat	$strtod("0x1.8p+1")		; _gain3 @ 0

$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("gain3")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_gain3")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_addr _gain3]
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$36, DW_AT_external
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("CNV_DegUnit")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_CNV_DegUnit")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$37, DW_AT_declaration
	.dwattr $C$DW$37, DW_AT_external
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Period")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_ODV_Recorder_Period")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$39, DW_AT_declaration
	.dwattr $C$DW$39, DW_AT_external
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_TriggerLevel")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_ODV_Recorder_TriggerLevel")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_NbOfSamples")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ODV_Recorder_NbOfSamples")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
	.global	_VAR_MODULO_0
	.sect	".econst"
	.align	2
_VAR_MODULO_0:
	.bits	0,32			; _VAR_MODULO_0 @ 0

$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("VAR_MODULO_0")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_VAR_MODULO_0")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_addr _VAR_MODULO_0]
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$165)
	.dwattr $C$DW$42, DW_AT_external
	.global	_gain0_125
	.sect	".econst"
	.align	2
_gain0_125:
	.xfloat	$strtod("0x1p-3")		; _gain0_125 @ 0

$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("gain0_125")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_gain0_125")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_addr _gain0_125]
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$43, DW_AT_external
	.global	_gain_1000
	.sect	".econst"
	.align	2
_gain_1000:
	.xfloat	$strtod("0x1.f4p+9")		; _gain_1000 @ 0

$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("gain_1000")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_gain_1000")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_addr _gain_1000]
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$44, DW_AT_external
	.global	_gain0_0625
	.sect	".econst"
	.align	2
_gain0_0625:
	.xfloat	$strtod("0x1p-4")		; _gain0_0625 @ 0

$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("gain0_0625")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_gain0_0625")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_addr _gain0_0625]
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$45, DW_AT_external
	.global	_gainhas500
	.sect	".econst"
	.align	2
_gainhas500:
	.xfloat	$strtod("0x1.7702eep-4")		; _gainhas500 @ 0

$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("gainhas500")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_gainhas500")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_addr _gainhas500]
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$46, DW_AT_external
	.global	_gainlv25
	.sect	".econst"
	.align	2
_gainlv25:
	.xfloat	$strtod("0x1.907208p-3")		; _gainlv25 @ 0

$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("gainlv25")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_gainlv25")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_addr _gainlv25]
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$47, DW_AT_external
	.global	_gain0_25
	.sect	".econst"
	.align	2
_gain0_25:
	.xfloat	$strtod("0x1p-2")		; _gain0_25 @ 0

$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("gain0_25")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_gain0_25")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_addr _gain0_25]
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$48, DW_AT_external
	.global	_gainshunt
	.sect	".econst"
	.align	2
_gainshunt:
	.xfloat	$strtod("0x1.f403e8p-3")		; _gainshunt @ 0

$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("gainshunt")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_gainshunt")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_addr _gainshunt]
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$49, DW_AT_external
	.global	_gain0_001
	.sect	".econst"
	.align	2
_gain0_001:
	.xfloat	$strtod("0x1.0624dep-10")		; _gain0_001 @ 0

$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("gain0_001")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_gain0_001")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_addr _gain0_001]
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$50, DW_AT_external
	.global	_REC_RecPoint
_REC_RecPoint:	.usect	".ebss",4,1,1
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("REC_RecPoint")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_REC_RecPoint")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_addr _REC_RecPoint]
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$51, DW_AT_external
	.global	_REC_OldRecPoint
_REC_OldRecPoint:	.usect	".ebss",4,1,1
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("REC_OldRecPoint")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_REC_OldRecPoint")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_addr _REC_OldRecPoint]
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$52, DW_AT_external
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Vectors")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_ODV_Recorder_Vectors")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
	.global	_REC_RecordSize
_REC_RecordSize:	.usect	".ebss",7,1,0
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("REC_RecordSize")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_REC_RecordSize")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_addr _REC_RecordSize]
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$54, DW_AT_external
	.global	_CapacityMultiUnit
	.sect	".econst:_CapacityMultiUnit"
	.clink
	.align	2
_CapacityMultiUnit:
	.bits	_VAR_GAIN_NORM,32		; _CapacityMultiUnit[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _CapacityMultiUnit[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _CapacityMultiUnit[0]._Modulo @ 64
	.bits	$C$FSL1,32		; _CapacityMultiUnit[0]._Name @ 96
	.bits	$C$FSL2,32		; _CapacityMultiUnit[0]._UnitName @ 128

$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("CapacityMultiUnit")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_CapacityMultiUnit")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_addr _CapacityMultiUnit]
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$55, DW_AT_external
	.global	_RadMultiUnit
	.sect	".econst:_RadMultiUnit"
	.clink
	.align	2
_RadMultiUnit:
	.bits	_VAR_GAIN_NORM,32		; _RadMultiUnit[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _RadMultiUnit[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _RadMultiUnit[0]._Modulo @ 64
	.bits	$C$FSL3,32		; _RadMultiUnit[0]._Name @ 96
	.bits	$C$FSL4,32		; _RadMultiUnit[0]._UnitName @ 128

$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("RadMultiUnit")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_RadMultiUnit")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_addr _RadMultiUnit]
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$56, DW_AT_external
	.global	_MultiUnitPow
	.sect	".econst:_MultiUnitPow"
	.clink
	.align	2
_MultiUnitPow:
	.bits	_VAR_GAIN_NORM,32		; _MultiUnitPow[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _MultiUnitPow[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _MultiUnitPow[0]._Modulo @ 64
	.bits	$C$FSL5,32		; _MultiUnitPow[0]._Name @ 96
	.bits	$C$FSL6,32		; _MultiUnitPow[0]._UnitName @ 128

$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("MultiUnitPow")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_MultiUnitPow")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_addr _MultiUnitPow]
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$57, DW_AT_external
	.global	_NoMultiUnit
	.sect	".econst:_NoMultiUnit"
	.clink
	.align	2
_NoMultiUnit:
	.bits	_VAR_GAIN_NORM,32		; _NoMultiUnit[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _NoMultiUnit[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _NoMultiUnit[0]._Modulo @ 64
	.bits	$C$FSL7,32		; _NoMultiUnit[0]._Name @ 96
	.bits	$C$FSL8,32		; _NoMultiUnit[0]._UnitName @ 128

$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("NoMultiUnit")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_NoMultiUnit")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_addr _NoMultiUnit]
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$58, DW_AT_external
	.global	_TempMultiUnit
	.sect	".econst:_TempMultiUnit"
	.clink
	.align	2
_TempMultiUnit:
	.bits	_VAR_GAIN_NORM,32		; _TempMultiUnit[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _TempMultiUnit[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _TempMultiUnit[0]._Modulo @ 64
	.bits	$C$FSL9,32		; _TempMultiUnit[0]._Name @ 96
	.bits	$C$FSL10,32		; _TempMultiUnit[0]._UnitName @ 128

$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("TempMultiUnit")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_TempMultiUnit")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_addr _TempMultiUnit]
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$59, DW_AT_external
	.global	_REC_RecordSource
_REC_RecordSource:	.usect	".ebss",12,1,1
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("REC_RecordSource")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_REC_RecordSource")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_addr _REC_RecordSource]
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$60, DW_AT_external
	.global	_ADMultiUnit
	.sect	".econst:_ADMultiUnit"
	.clink
	.align	2
_ADMultiUnit:
	.bits	_VAR_GAIN_NORM,32		; _ADMultiUnit[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _ADMultiUnit[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _ADMultiUnit[0]._Modulo @ 64
	.bits	$C$FSL11,32		; _ADMultiUnit[0]._Name @ 96
	.bits	$C$FSL12,32		; _ADMultiUnit[0]._UnitName @ 128
	.bits	_gainvalve,32		; _ADMultiUnit[1]._Gain @ 160
	.bits	_VAR_OFFSET_NULL,32		; _ADMultiUnit[1]._Offset @ 192
	.bits	_VAR_MODULO_0,32		; _ADMultiUnit[1]._Modulo @ 224
	.bits	$C$FSL13,32		; _ADMultiUnit[1]._Name @ 256
	.bits	$C$FSL14,32		; _ADMultiUnit[1]._UnitName @ 288

$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("ADMultiUnit")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_ADMultiUnit")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_addr _ADMultiUnit]
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$61, DW_AT_external
	.global	_MultiUnite3
	.sect	".econst:_MultiUnite3"
	.clink
	.align	2
_MultiUnite3:
	.bits	_gain0_001,32		; _MultiUnite3[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _MultiUnite3[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _MultiUnite3[0]._Modulo @ 64
	.bits	$C$FSL15,32		; _MultiUnite3[0]._Name @ 96
	.bits	$C$FSL16,32		; _MultiUnite3[0]._UnitName @ 128
	.bits	_VAR_GAIN_NORM,32		; _MultiUnite3[1]._Gain @ 160
	.bits	_VAR_OFFSET_NULL,32		; _MultiUnite3[1]._Offset @ 192
	.bits	_VAR_MODULO_0,32		; _MultiUnite3[1]._Modulo @ 224
	.bits	$C$FSL15,32		; _MultiUnite3[1]._Name @ 256
	.bits	$C$FSL17,32		; _MultiUnite3[1]._UnitName @ 288

$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("MultiUnite3")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_MultiUnite3")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_addr _MultiUnite3]
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$62, DW_AT_external
	.global	_MultiUnitDerivate
	.sect	".econst:_MultiUnitDerivate"
	.clink
	.align	2
_MultiUnitDerivate:
	.bits	_VAR_GAIN_NORM,32		; _MultiUnitDerivate[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _MultiUnitDerivate[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _MultiUnitDerivate[0]._Modulo @ 64
	.bits	$C$FSL18,32		; _MultiUnitDerivate[0]._Name @ 96
	.bits	$C$FSL19,32		; _MultiUnitDerivate[0]._UnitName @ 128
	.bits	_gain0_001,32		; _MultiUnitDerivate[1]._Gain @ 160
	.bits	_VAR_OFFSET_NULL,32		; _MultiUnitDerivate[1]._Offset @ 192
	.bits	_VAR_MODULO_0,32		; _MultiUnitDerivate[1]._Modulo @ 224
	.bits	$C$FSL18,32		; _MultiUnitDerivate[1]._Name @ 256
	.bits	$C$FSL20,32		; _MultiUnitDerivate[1]._UnitName @ 288

$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("MultiUnitDerivate")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_MultiUnitDerivate")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_addr _MultiUnitDerivate]
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$63, DW_AT_external
	.global	_MultiUnite1
	.sect	".econst:_MultiUnite1"
	.clink
	.align	2
_MultiUnite1:
	.bits	_VAR_GAIN_NORM,32		; _MultiUnite1[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _MultiUnite1[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _MultiUnite1[0]._Modulo @ 64
	.bits	$C$FSL13,32		; _MultiUnite1[0]._Name @ 96
	.bits	$C$FSL21,32		; _MultiUnite1[0]._UnitName @ 128
	.bits	_gain_1000,32		; _MultiUnite1[1]._Gain @ 160
	.bits	_VAR_OFFSET_NULL,32		; _MultiUnite1[1]._Offset @ 192
	.bits	_VAR_MODULO_0,32		; _MultiUnite1[1]._Modulo @ 224
	.bits	$C$FSL13,32		; _MultiUnite1[1]._Name @ 256
	.bits	$C$FSL14,32		; _MultiUnite1[1]._UnitName @ 288

$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("MultiUnite1")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_MultiUnite1")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_addr _MultiUnite1]
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$64, DW_AT_external
	.global	_MultiUnite2
	.sect	".econst:_MultiUnite2"
	.clink
	.align	2
_MultiUnite2:
	.bits	_VAR_GAIN_NORM,32		; _MultiUnite2[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _MultiUnite2[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _MultiUnite2[0]._Modulo @ 64
	.bits	$C$FSL22,32		; _MultiUnite2[0]._Name @ 96
	.bits	$C$FSL23,32		; _MultiUnite2[0]._UnitName @ 128
	.bits	_CNV_DegUnit,32		; _MultiUnite2[1]._Gain @ 160
	.bits	_VAR_OFFSET_NULL,32		; _MultiUnite2[1]._Offset @ 192
	.bits	_VAR_MODULO_0,32		; _MultiUnite2[1]._Modulo @ 224
	.bits	$C$FSL22,32		; _MultiUnite2[1]._Name @ 256
	.bits	$C$FSL24,32		; _MultiUnite2[1]._UnitName @ 288

$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("MultiUnite2")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_MultiUnite2")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_addr _MultiUnite2]
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$65, DW_AT_external
	.global	_SpeedMultiUnit
	.sect	".econst:_SpeedMultiUnit"
	.clink
	.align	2
_SpeedMultiUnit:
	.bits	_VAR_GAIN_NORM,32		; _SpeedMultiUnit[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _SpeedMultiUnit[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _SpeedMultiUnit[0]._Modulo @ 64
	.bits	$C$FSL25,32		; _SpeedMultiUnit[0]._Name @ 96
	.bits	$C$FSL26,32		; _SpeedMultiUnit[0]._UnitName @ 128
	.bits	_gain_rpm,32		; _SpeedMultiUnit[1]._Gain @ 160
	.bits	_VAR_OFFSET_NULL,32		; _SpeedMultiUnit[1]._Offset @ 192
	.bits	_VAR_MODULO_0,32		; _SpeedMultiUnit[1]._Modulo @ 224
	.bits	$C$FSL25,32		; _SpeedMultiUnit[1]._Name @ 256
	.bits	$C$FSL27,32		; _SpeedMultiUnit[1]._UnitName @ 288

$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("SpeedMultiUnit")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_SpeedMultiUnit")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_addr _SpeedMultiUnit]
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$66, DW_AT_external
	.global	_RecordOffset
_RecordOffset:	.usect	".ebss",24,1,1
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("RecordOffset")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_RecordOffset")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_addr _RecordOffset]
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$67, DW_AT_external
	.global	_TimeMultiUnit
	.sect	".econst:_TimeMultiUnit"
	.clink
	.align	2
_TimeMultiUnit:
	.bits	_VAR_GAIN_NORM,32		; _TimeMultiUnit[0]._Gain @ 0
	.bits	_VAR_OFFSET_NULL,32		; _TimeMultiUnit[0]._Offset @ 32
	.bits	_VAR_MODULO_0,32		; _TimeMultiUnit[0]._Modulo @ 64
	.bits	$C$FSL28,32		; _TimeMultiUnit[0]._Name @ 96
	.bits	$C$FSL29,32		; _TimeMultiUnit[0]._UnitName @ 128
	.bits	_gain0_001,32		; _TimeMultiUnit[1]._Gain @ 160
	.bits	_VAR_OFFSET_NULL,32		; _TimeMultiUnit[1]._Offset @ 192
	.bits	_VAR_MODULO_0,32		; _TimeMultiUnit[1]._Modulo @ 224
	.bits	$C$FSL28,32		; _TimeMultiUnit[1]._Name @ 256
	.bits	$C$FSL30,32		; _TimeMultiUnit[1]._UnitName @ 288
	.bits	_gain_1000,32		; _TimeMultiUnit[2]._Gain @ 320
	.bits	_VAR_OFFSET_NULL,32		; _TimeMultiUnit[2]._Offset @ 352
	.bits	_VAR_MODULO_0,32		; _TimeMultiUnit[2]._Modulo @ 384
	.bits	$C$FSL28,32		; _TimeMultiUnit[2]._Name @ 416
	.bits	$C$FSL31,32		; _TimeMultiUnit[2]._UnitName @ 448

$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("TimeMultiUnit")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_TimeMultiUnit")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_addr _TimeMultiUnit]
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$68, DW_AT_external
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Input_Scaling_Float")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ODP_Analogue_Input_Scaling_Float")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_external
	.global	_MultiUnitListe1
	.sect	".econst:_MultiUnitListe1"
	.clink
	.align	2
_MultiUnitListe1:
	.bits	2,16			; _MultiUnitListe1[0]._Size @ 0
	.bits	5,16			; _MultiUnitListe1[0]._Key @ 16
	.bits	_MultiUnite2,32		; _MultiUnitListe1[0]._MultiUnit @ 32
	.bits	2,16			; _MultiUnitListe1[1]._Size @ 64
	.bits	1,16			; _MultiUnitListe1[1]._Key @ 80
	.bits	_MultiUnite1,32		; _MultiUnitListe1[1]._MultiUnit @ 96
	.bits	2,16			; _MultiUnitListe1[2]._Size @ 128
	.bits	2,16			; _MultiUnitListe1[2]._Key @ 144
	.bits	_MultiUnite3,32		; _MultiUnitListe1[2]._MultiUnit @ 160
	.bits	1,16			; _MultiUnitListe1[3]._Size @ 192
	.bits	16,16			; _MultiUnitListe1[3]._Key @ 208
	.bits	_NoMultiUnit,32		; _MultiUnitListe1[3]._MultiUnit @ 224
	.bits	2,16			; _MultiUnitListe1[4]._Size @ 256
	.bits	11,16			; _MultiUnitListe1[4]._Key @ 272
	.bits	_ADMultiUnit,32		; _MultiUnitListe1[4]._MultiUnit @ 288
	.bits	3,16			; _MultiUnitListe1[5]._Size @ 320
	.bits	10,16			; _MultiUnitListe1[5]._Key @ 336
	.bits	_TimeMultiUnit,32		; _MultiUnitListe1[5]._MultiUnit @ 352
	.bits	2,16			; _MultiUnitListe1[6]._Size @ 384
	.bits	3,16			; _MultiUnitListe1[6]._Key @ 400
	.bits	_MultiUnitDerivate,32		; _MultiUnitListe1[6]._MultiUnit @ 416
	.bits	1,16			; _MultiUnitListe1[7]._Size @ 448
	.bits	13,16			; _MultiUnitListe1[7]._Key @ 464
	.bits	_RadMultiUnit,32		; _MultiUnitListe1[7]._MultiUnit @ 480
	.bits	2,16			; _MultiUnitListe1[8]._Size @ 512
	.bits	14,16			; _MultiUnitListe1[8]._Key @ 528
	.bits	_SpeedMultiUnit,32		; _MultiUnitListe1[8]._MultiUnit @ 544
	.bits	1,16			; _MultiUnitListe1[9]._Size @ 576
	.bits	15,16			; _MultiUnitListe1[9]._Key @ 592
	.bits	_TempMultiUnit,32		; _MultiUnitListe1[9]._MultiUnit @ 608
	.bits	1,16			; _MultiUnitListe1[10]._Size @ 640
	.bits	17,16			; _MultiUnitListe1[10]._Key @ 656
	.bits	_MultiUnitPow,32		; _MultiUnitListe1[10]._MultiUnit @ 672
	.bits	1,16			; _MultiUnitListe1[11]._Size @ 704
	.bits	6,16			; _MultiUnitListe1[11]._Key @ 720
	.bits	_CapacityMultiUnit,32		; _MultiUnitListe1[11]._MultiUnit @ 736

$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("MultiUnitListe1")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_MultiUnitListe1")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_addr _MultiUnitListe1]
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$70, DW_AT_external
	.global	_Variables
	.sect	".econst:_Variables"
	.clink
	.align	2
_Variables:
	.bits	25601,16			; _Variables[0]._Index @ 0
	.bits	16,16			; _Variables[0]._Subindex @ 16
	.bits	$C$FSL32,32		; _Variables[0]._Suffix @ 32
	.bits	15,16			; _Variables[0]._MultiUnitTypeKey @ 64
	.space	16
	.bits	_ODP_Analogue_Input_Scaling_Float+2,32		; _Variables[0]._Gain @ 96
	.bits	_VAR_OFFSET_NULL,32		; _Variables[0]._Offset @ 128
	.bits	25601,16			; _Variables[1]._Index @ 160
	.bits	15,16			; _Variables[1]._Subindex @ 176
	.bits	$C$FSL33,32		; _Variables[1]._Suffix @ 192
	.bits	15,16			; _Variables[1]._MultiUnitTypeKey @ 224
	.space	16
	.bits	_ODP_Analogue_Input_Scaling_Float+2,32		; _Variables[1]._Gain @ 256
	.bits	_VAR_OFFSET_NULL,32		; _Variables[1]._Offset @ 288
	.bits	25601,16			; _Variables[2]._Index @ 320
	.bits	21,16			; _Variables[2]._Subindex @ 336
	.bits	$C$FSL34,32		; _Variables[2]._Suffix @ 352
	.bits	15,16			; _Variables[2]._MultiUnitTypeKey @ 384
	.space	16
	.bits	_ODP_Analogue_Input_Scaling_Float+2,32		; _Variables[2]._Gain @ 416
	.bits	_VAR_OFFSET_NULL,32		; _Variables[2]._Offset @ 448
	.bits	25601,16			; _Variables[3]._Index @ 480
	.bits	18,16			; _Variables[3]._Subindex @ 496
	.bits	$C$FSL35,32		; _Variables[3]._Suffix @ 512
	.bits	15,16			; _Variables[3]._MultiUnitTypeKey @ 544
	.space	16
	.bits	_ODP_Analogue_Input_Scaling_Float,32		; _Variables[3]._Gain @ 576
	.bits	_VAR_OFFSET_NULL,32		; _Variables[3]._Offset @ 608
	.bits	25601,16			; _Variables[4]._Index @ 640
	.bits	22,16			; _Variables[4]._Subindex @ 656
	.bits	$C$FSL36,32		; _Variables[4]._Suffix @ 672
	.bits	15,16			; _Variables[4]._MultiUnitTypeKey @ 704
	.space	16
	.bits	_ODP_Analogue_Input_Scaling_Float,32		; _Variables[4]._Gain @ 736
	.bits	_VAR_OFFSET_NULL,32		; _Variables[4]._Offset @ 768
	.bits	8704,16			; _Variables[5]._Index @ 800
	.bits	4,16			; _Variables[5]._Subindex @ 816
	.bits	$C$FSL37,32		; _Variables[5]._Suffix @ 832
	.bits	2,16			; _Variables[5]._MultiUnitTypeKey @ 864
	.space	16
	.bits	_ODP_Analogue_Input_Scaling_Float+4,32		; _Variables[5]._Gain @ 896
	.bits	_VAR_OFFSET_NULL,32		; _Variables[5]._Offset @ 928
	.bits	8704,16			; _Variables[6]._Index @ 960
	.bits	5,16			; _Variables[6]._Subindex @ 976
	.bits	$C$FSL38,32		; _Variables[6]._Suffix @ 992
	.bits	1,16			; _Variables[6]._MultiUnitTypeKey @ 1024
	.space	16
	.bits	_ODP_Analogue_Input_Scaling_Float+6,32		; _Variables[6]._Gain @ 1056
	.bits	_VAR_OFFSET_NULL,32		; _Variables[6]._Offset @ 1088
	.bits	8704,16			; _Variables[7]._Index @ 1120
	.bits	6,16			; _Variables[7]._Subindex @ 1136
	.bits	$C$FSL39,32		; _Variables[7]._Suffix @ 1152
	.bits	15,16			; _Variables[7]._MultiUnitTypeKey @ 1184
	.space	16
	.bits	_VAR_GAIN_NORM,32		; _Variables[7]._Gain @ 1216
	.bits	_VAR_OFFSET_NULL,32		; _Variables[7]._Offset @ 1248
	.bits	8704,16			; _Variables[8]._Index @ 1280
	.bits	11,16			; _Variables[8]._Subindex @ 1296
	.bits	$C$FSL40,32		; _Variables[8]._Suffix @ 1312
	.bits	15,16			; _Variables[8]._MultiUnitTypeKey @ 1344
	.space	16
	.bits	_VAR_GAIN_NORM,32		; _Variables[8]._Gain @ 1376
	.bits	_VAR_OFFSET_NULL,32		; _Variables[8]._Offset @ 1408
	.bits	8704,16			; _Variables[9]._Index @ 1440
	.bits	7,16			; _Variables[9]._Subindex @ 1456
	.bits	$C$FSL41,32		; _Variables[9]._Suffix @ 1472
	.bits	17,16			; _Variables[9]._MultiUnitTypeKey @ 1504
	.space	16
	.bits	_gain0_0625,32		; _Variables[9]._Gain @ 1536
	.bits	_VAR_OFFSET_NULL,32		; _Variables[9]._Offset @ 1568
	.bits	8704,16			; _Variables[10]._Index @ 1600
	.bits	8,16			; _Variables[10]._Subindex @ 1616
	.bits	$C$FSL42,32		; _Variables[10]._Suffix @ 1632
	.bits	2,16			; _Variables[10]._MultiUnitTypeKey @ 1664
	.space	16
	.bits	_gaindelta,32		; _Variables[10]._Gain @ 1696
	.bits	_VAR_OFFSET_NULL,32		; _Variables[10]._Offset @ 1728
	.bits	8704,16			; _Variables[11]._Index @ 1760
	.bits	9,16			; _Variables[11]._Subindex @ 1776
	.bits	$C$FSL43,32		; _Variables[11]._Suffix @ 1792
	.bits	2,16			; _Variables[11]._MultiUnitTypeKey @ 1824
	.space	16
	.bits	_gaindelta,32		; _Variables[11]._Gain @ 1856
	.bits	_VAR_OFFSET_NULL,32		; _Variables[11]._Offset @ 1888
	.bits	8704,16			; _Variables[12]._Index @ 1920
	.bits	13,16			; _Variables[12]._Subindex @ 1936
	.bits	$C$FSL44,32		; _Variables[12]._Suffix @ 1952
	.bits	2,16			; _Variables[12]._MultiUnitTypeKey @ 1984
	.space	16
	.bits	_gaindelta,32		; _Variables[12]._Gain @ 2016
	.bits	_VAR_OFFSET_NULL,32		; _Variables[12]._Offset @ 2048
	.bits	8704,16			; _Variables[13]._Index @ 2080
	.bits	15,16			; _Variables[13]._Subindex @ 2096
	.bits	$C$FSL45,32		; _Variables[13]._Suffix @ 2112
	.bits	6,16			; _Variables[13]._MultiUnitTypeKey @ 2144
	.space	16
	.bits	_gain01,32		; _Variables[13]._Gain @ 2176
	.bits	_VAR_OFFSET_NULL,32		; _Variables[13]._Offset @ 2208
	.bits	25601,16			; _Variables[14]._Index @ 2240
	.bits	23,16			; _Variables[14]._Subindex @ 2256
	.bits	$C$FSL46,32		; _Variables[14]._Suffix @ 2272
	.bits	11,16			; _Variables[14]._MultiUnitTypeKey @ 2304
	.space	16
	.bits	_ODP_Analogue_Input_Scaling_Float+12,32		; _Variables[14]._Gain @ 2336
	.bits	_VAR_OFFSET_NULL,32		; _Variables[14]._Offset @ 2368
	.bits	25617,16			; _Variables[15]._Index @ 2400
	.bits	1,16			; _Variables[15]._Subindex @ 2416
	.bits	$C$FSL47,32		; _Variables[15]._Suffix @ 2432
	.bits	2,16			; _Variables[15]._MultiUnitTypeKey @ 2464
	.space	16
	.bits	_VAR_GAIN_NORM,32		; _Variables[15]._Gain @ 2496
	.bits	_VAR_OFFSET_NULL,32		; _Variables[15]._Offset @ 2528
	.bits	25601,16			; _Variables[16]._Index @ 2560
	.bits	1,16			; _Variables[16]._Subindex @ 2576
	.bits	$C$FSL48,32		; _Variables[16]._Suffix @ 2592
	.bits	2,16			; _Variables[16]._MultiUnitTypeKey @ 2624
	.space	16
	.bits	_gaincell,32		; _Variables[16]._Gain @ 2656
	.bits	_VAR_OFFSET_NULL,32		; _Variables[16]._Offset @ 2688
	.bits	25601,16			; _Variables[17]._Index @ 2720
	.bits	2,16			; _Variables[17]._Subindex @ 2736
	.bits	$C$FSL49,32		; _Variables[17]._Suffix @ 2752
	.bits	2,16			; _Variables[17]._MultiUnitTypeKey @ 2784
	.space	16
	.bits	_gaincell,32		; _Variables[17]._Gain @ 2816
	.bits	_VAR_OFFSET_NULL,32		; _Variables[17]._Offset @ 2848
	.bits	25601,16			; _Variables[18]._Index @ 2880
	.bits	3,16			; _Variables[18]._Subindex @ 2896
	.bits	$C$FSL50,32		; _Variables[18]._Suffix @ 2912
	.bits	2,16			; _Variables[18]._MultiUnitTypeKey @ 2944
	.space	16
	.bits	_gaincell,32		; _Variables[18]._Gain @ 2976
	.bits	_VAR_OFFSET_NULL,32		; _Variables[18]._Offset @ 3008
	.bits	25601,16			; _Variables[19]._Index @ 3040
	.bits	4,16			; _Variables[19]._Subindex @ 3056
	.bits	$C$FSL51,32		; _Variables[19]._Suffix @ 3072
	.bits	2,16			; _Variables[19]._MultiUnitTypeKey @ 3104
	.space	16
	.bits	_gaincell,32		; _Variables[19]._Gain @ 3136
	.bits	_VAR_OFFSET_NULL,32		; _Variables[19]._Offset @ 3168
	.bits	25601,16			; _Variables[20]._Index @ 3200
	.bits	5,16			; _Variables[20]._Subindex @ 3216
	.bits	$C$FSL52,32		; _Variables[20]._Suffix @ 3232
	.bits	2,16			; _Variables[20]._MultiUnitTypeKey @ 3264
	.space	16
	.bits	_gaincell,32		; _Variables[20]._Gain @ 3296
	.bits	_VAR_OFFSET_NULL,32		; _Variables[20]._Offset @ 3328
	.bits	25601,16			; _Variables[21]._Index @ 3360
	.bits	6,16			; _Variables[21]._Subindex @ 3376
	.bits	$C$FSL53,32		; _Variables[21]._Suffix @ 3392
	.bits	2,16			; _Variables[21]._MultiUnitTypeKey @ 3424
	.space	16
	.bits	_gaincell,32		; _Variables[21]._Gain @ 3456
	.bits	_VAR_OFFSET_NULL,32		; _Variables[21]._Offset @ 3488
	.bits	25601,16			; _Variables[22]._Index @ 3520
	.bits	7,16			; _Variables[22]._Subindex @ 3536
	.bits	$C$FSL54,32		; _Variables[22]._Suffix @ 3552
	.bits	2,16			; _Variables[22]._MultiUnitTypeKey @ 3584
	.space	16
	.bits	_gaincell,32		; _Variables[22]._Gain @ 3616
	.bits	_VAR_OFFSET_NULL,32		; _Variables[22]._Offset @ 3648
	.bits	25601,16			; _Variables[23]._Index @ 3680
	.bits	8,16			; _Variables[23]._Subindex @ 3696
	.bits	$C$FSL55,32		; _Variables[23]._Suffix @ 3712
	.bits	2,16			; _Variables[23]._MultiUnitTypeKey @ 3744
	.space	16
	.bits	_gaincell,32		; _Variables[23]._Gain @ 3776
	.bits	_VAR_OFFSET_NULL,32		; _Variables[23]._Offset @ 3808
	.bits	25601,16			; _Variables[24]._Index @ 3840
	.bits	9,16			; _Variables[24]._Subindex @ 3856
	.bits	$C$FSL56,32		; _Variables[24]._Suffix @ 3872
	.bits	2,16			; _Variables[24]._MultiUnitTypeKey @ 3904
	.space	16
	.bits	_gaincell,32		; _Variables[24]._Gain @ 3936
	.bits	_VAR_OFFSET_NULL,32		; _Variables[24]._Offset @ 3968
	.bits	25601,16			; _Variables[25]._Index @ 4000
	.bits	10,16			; _Variables[25]._Subindex @ 4016
	.bits	$C$FSL57,32		; _Variables[25]._Suffix @ 4032
	.bits	2,16			; _Variables[25]._MultiUnitTypeKey @ 4064
	.space	16
	.bits	_gaincell,32		; _Variables[25]._Gain @ 4096
	.bits	_VAR_OFFSET_NULL,32		; _Variables[25]._Offset @ 4128
	.bits	25601,16			; _Variables[26]._Index @ 4160
	.bits	11,16			; _Variables[26]._Subindex @ 4176
	.bits	$C$FSL58,32		; _Variables[26]._Suffix @ 4192
	.bits	2,16			; _Variables[26]._MultiUnitTypeKey @ 4224
	.space	16
	.bits	_gaincell,32		; _Variables[26]._Gain @ 4256
	.bits	_VAR_OFFSET_NULL,32		; _Variables[26]._Offset @ 4288
	.bits	25601,16			; _Variables[27]._Index @ 4320
	.bits	12,16			; _Variables[27]._Subindex @ 4336
	.bits	$C$FSL59,32		; _Variables[27]._Suffix @ 4352
	.bits	2,16			; _Variables[27]._MultiUnitTypeKey @ 4384
	.space	16
	.bits	_gaincell,32		; _Variables[27]._Gain @ 4416
	.bits	_VAR_OFFSET_NULL,32		; _Variables[27]._Offset @ 4448
	.bits	25601,16			; _Variables[28]._Index @ 4480
	.bits	13,16			; _Variables[28]._Subindex @ 4496
	.bits	$C$FSL60,32		; _Variables[28]._Suffix @ 4512
	.bits	2,16			; _Variables[28]._MultiUnitTypeKey @ 4544
	.space	16
	.bits	_gaincell,32		; _Variables[28]._Gain @ 4576
	.bits	_VAR_OFFSET_NULL,32		; _Variables[28]._Offset @ 4608
	.bits	25601,16			; _Variables[29]._Index @ 4640
	.bits	14,16			; _Variables[29]._Subindex @ 4656
	.bits	$C$FSL61,32		; _Variables[29]._Suffix @ 4672
	.bits	2,16			; _Variables[29]._MultiUnitTypeKey @ 4704
	.space	16
	.bits	_gaincell,32		; _Variables[29]._Gain @ 4736
	.bits	_VAR_OFFSET_NULL,32		; _Variables[29]._Offset @ 4768
	.bits	8200,16			; _Variables[30]._Index @ 4800
	.bits	12,16			; _Variables[30]._Subindex @ 4816
	.bits	$C$FSL62,32		; _Variables[30]._Suffix @ 4832
	.bits	17,16			; _Variables[30]._MultiUnitTypeKey @ 4864
	.space	16
	.bits	_gain0_0625,32		; _Variables[30]._Gain @ 4896
	.bits	_VAR_OFFSET_NULL,32		; _Variables[30]._Offset @ 4928
	.bits	8200,16			; _Variables[31]._Index @ 4960
	.bits	6,16			; _Variables[31]._Subindex @ 4976
	.bits	$C$FSL63,32		; _Variables[31]._Suffix @ 4992
	.bits	16,16			; _Variables[31]._MultiUnitTypeKey @ 5024
	.space	16
	.bits	_VAR_GAIN_NORM,32		; _Variables[31]._Gain @ 5056
	.bits	_VAR_OFFSET_NULL,32		; _Variables[31]._Offset @ 5088
	.bits	24832,16			; _Variables[32]._Index @ 5120
	.bits	1,16			; _Variables[32]._Subindex @ 5136
	.bits	$C$FSL64,32		; _Variables[32]._Suffix @ 5152
	.bits	16,16			; _Variables[32]._MultiUnitTypeKey @ 5184
	.space	16
	.bits	_VAR_GAIN_NORM,32		; _Variables[32]._Gain @ 5216
	.bits	_VAR_OFFSET_NULL,32		; _Variables[32]._Offset @ 5248
	.bits	24832,16			; _Variables[33]._Index @ 5280
	.bits	2,16			; _Variables[33]._Subindex @ 5296
	.bits	$C$FSL65,32		; _Variables[33]._Suffix @ 5312
	.bits	16,16			; _Variables[33]._MultiUnitTypeKey @ 5344
	.space	16
	.bits	_VAR_GAIN_NORM,32		; _Variables[33]._Gain @ 5376
	.bits	_VAR_OFFSET_NULL,32		; _Variables[33]._Offset @ 5408
	.bits	8452,16			; _Variables[34]._Index @ 5440
	.bits	0,16			; _Variables[34]._Subindex @ 5456
	.bits	$C$FSL66,32		; _Variables[34]._Suffix @ 5472
	.bits	10,16			; _Variables[34]._MultiUnitTypeKey @ 5504
	.space	16
	.bits	_VAR_GAIN_NORM,32		; _Variables[34]._Gain @ 5536
	.bits	_VAR_OFFSET_NULL,32		; _Variables[34]._Offset @ 5568

$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("Variables")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_Variables")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_addr _Variables]
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$71, DW_AT_external
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RecorderData1")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_ODV_RecorderData1")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$72, DW_AT_declaration
	.dwattr $C$DW$72, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1362812 
	.sect	".text"
	.global	_REC_Record

$C$DW$73	.dwtag  DW_TAG_subprogram, DW_AT_name("REC_Record")
	.dwattr $C$DW$73, DW_AT_low_pc(_REC_Record)
	.dwattr $C$DW$73, DW_AT_high_pc(0x00)
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_REC_Record")
	.dwattr $C$DW$73, DW_AT_external
	.dwattr $C$DW$73, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$73, DW_AT_TI_begin_line(0xb6)
	.dwattr $C$DW$73, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$73, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../recorder.c",line 182,column 22,is_stmt,address _REC_Record

	.dwfde $C$DW$CIE, _REC_Record

;***************************************************************
;* FNAME: _REC_Record                   FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  1 Auto,  2 SOE     *
;***************************************************************

_REC_Record:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR2            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 9, 2
	.dwcfi	cfa_offset, -4
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -5]
	.dwpsn	file "../recorder.c",line 184,column 3,is_stmt
        MOVW      DP,#_REC_PosSampleInterval ; [CPU_U] 
        MOVZ      AR6,@_REC_PosSampleInterval ; [CPU_] |184| 
        MOVB      ACC,#0                ; [CPU_] |184| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      P,@_ODV_SysTick_ms    ; [CPU_] |184| 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |184| 
        TEST      ACC                   ; [CPU_] |184| 
        BF        $C$L11,NEQ            ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
	.dwpsn	file "../recorder.c",line 186,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_NbOfSamples ; [CPU_U] 
        MOVL      ACC,@_ODV_Recorder_NbOfSamples ; [CPU_] |186| 
        BF        $C$L8,NEQ             ; [CPU_] |186| 
        ; branchcc occurs ; [] |186| 
	.dwpsn	file "../recorder.c",line 188,column 7,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        TBIT      @_ODV_Recorder_Control,#5 ; [CPU_] |188| 
        BF        $C$L8,NTC             ; [CPU_] |188| 
        ; branchcc occurs ; [] |188| 
	.dwpsn	file "../recorder.c",line 189,column 9,is_stmt
        MOVW      DP,#_ODV_Recorder_Vectors+2 ; [CPU_U] 
        MOV       T,#48                 ; [CPU_] |189| 
        MOVL      ACC,@_ODV_Recorder_Vectors+2 ; [CPU_] |189| 
        MOVL      P,@_ODV_Recorder_Vectors ; [CPU_] |189| 
        LSR64     ACC:P,T               ; [CPU_] |189| 
        MOV       *-SP[5],P             ; [CPU_] |189| 
	.dwpsn	file "../recorder.c",line 190,column 9,is_stmt
        MOVL      XAR4,#_REC_RecordSource ; [CPU_U] |190| 
        MOV       AL,*-SP[5]            ; [CPU_] |190| 
        ANDB      AL,#0x07              ; [CPU_] |190| 
        MOVU      ACC,AL                ; [CPU_] |190| 
        LSL       ACC,1                 ; [CPU_] |190| 
        ADDL      XAR4,ACC              ; [CPU_] |190| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |190| 
        TBIT      AL,#0                 ; [CPU_] |190| 
        BF        $C$L1,NTC             ; [CPU_] |190| 
        ; branchcc occurs ; [] |190| 
	.dwpsn	file "../recorder.c",line 191,column 11,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |191| 
        MOVL      XAR4,#_REC_RecordSource ; [CPU_U] |191| 
        ANDB      AL,#0x07              ; [CPU_] |191| 
        MOVU      ACC,AL                ; [CPU_] |191| 
        LSL       ACC,1                 ; [CPU_] |191| 
        ADDL      XAR4,ACC              ; [CPU_] |191| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |191| 
        MOVW      DP,#_REC_RecPoint     ; [CPU_U] 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |191| 
        MOVL      @_REC_RecPoint,ACC    ; [CPU_] |191| 
        MOV       @_REC_RecPoint+2,#0   ; [CPU_] |191| 
        MOV       @_REC_RecPoint+3,#0   ; [CPU_] |191| 
        B         $C$L2,UNC             ; [CPU_] |191| 
        ; branch occurs ; [] |191| 
$C$L1:    
	.dwpsn	file "../recorder.c",line 193,column 11,is_stmt
        ZAPA      ; [CPU_] |193| 
        MOV       PL,#1                 ; [CPU_] |193| 
        MOVL      XAR7,ACC              ; [CPU_] |193| 
        MOVL      XAR6,P                ; [CPU_] |193| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |193| 
        ZAPA      ; [CPU_] |193| 
        MOV       PL,#1                 ; [CPU_] |193| 
        MOVL      XAR2,ACC              ; [CPU_] |193| 
        MOV       AL,*-SP[5]            ; [CPU_] |193| 
        MOVL      XAR5,P                ; [CPU_] |193| 
        ANDB      AL,#0x07              ; [CPU_] |193| 
        MOVZ      AR0,AL                ; [CPU_] |193| 
        MOVL      P,XAR6                ; [CPU_] |193| 
        MOV       T,*+XAR4[AR0]         ; [CPU_] |193| 
        MOVL      ACC,XAR7              ; [CPU_] |193| 
        MOVL      XAR4,#_REC_RecordSource ; [CPU_U] |193| 
        LSL64     ACC:P,T               ; [CPU_] |193| 
        SUBUL     P,XAR5                ; [CPU_] |193| 
        MOVL      *-SP[4],P             ; [CPU_] |193| 
        SUBBL     ACC,XAR2              ; [CPU_] |193| 
        MOVL      *-SP[2],ACC           ; [CPU_] |193| 
        MOV       AL,*-SP[5]            ; [CPU_] |193| 
        ANDB      AL,#0x07              ; [CPU_] |193| 
        MOVU      ACC,AL                ; [CPU_] |193| 
        LSL       ACC,1                 ; [CPU_] |193| 
        ADDL      XAR4,ACC              ; [CPU_] |193| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |193| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |193| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |193| 
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_name("LL$$AND")
	.dwattr $C$DW$75, DW_AT_TI_call
        FFC       XAR7,#LL$$AND         ; [CPU_] |193| 
        ; call occurs [#LL$$AND] ; [] |193| 
        MOVW      DP,#_REC_RecPoint     ; [CPU_U] 
        MOVL      @_REC_RecPoint,P      ; [CPU_] |193| 
        MOVL      @_REC_RecPoint+2,ACC  ; [CPU_] |193| 
$C$L2:    
	.dwpsn	file "../recorder.c",line 194,column 9,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |194| 
        MOVL      XAR4,#_RecordOffset   ; [CPU_U] |194| 
        ANDB      AL,#0x07              ; [CPU_] |194| 
        CMP64     ACC:P                 ; [CPU_] |194| 
        MOVU      ACC,AL                ; [CPU_] |194| 
        LSL       ACC,2                 ; [CPU_] |194| 
        ADDL      XAR4,ACC              ; [CPU_] |194| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |194| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |194| 
        CMP64     ACC:P                 ; [CPU_] |194| 
        BF        $C$L3,EQ              ; [CPU_] |194| 
        ; branchcc occurs ; [] |194| 
	.dwpsn	file "../recorder.c",line 195,column 11,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |195| 
        MOVL      XAR4,#_RecordOffset   ; [CPU_U] |195| 
        ANDB      AL,#0x07              ; [CPU_] |195| 
        MOVL      P,@_REC_RecPoint      ; [CPU_] |195| 
        MOVU      ACC,AL                ; [CPU_] |195| 
        LSL       ACC,2                 ; [CPU_] |195| 
        ADDL      XAR4,ACC              ; [CPU_] |195| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |195| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |195| 
        MOVL      *-SP[4],ACC           ; [CPU_] |195| 
        MOVL      ACC,@_REC_RecPoint+2  ; [CPU_] |195| 
        MOVL      *-SP[2],XAR6          ; [CPU_] |195| 
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_name("ULL$$CMP")
	.dwattr $C$DW$76, DW_AT_TI_call
        FFC       XAR7,#ULL$$CMP        ; [CPU_] |195| 
        ; call occurs [#ULL$$CMP] ; [] |195| 
        CMPB      AL,#0                 ; [CPU_] |195| 
        B         $C$L3,LEQ             ; [CPU_] |195| 
        ; branchcc occurs ; [] |195| 
	.dwpsn	file "../recorder.c",line 195,column 56,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |195| 
        MOVL      XAR4,#_RecordOffset   ; [CPU_U] |195| 
        ANDB      AL,#0x07              ; [CPU_] |195| 
        MOVU      ACC,AL                ; [CPU_] |195| 
        LSL       ACC,2                 ; [CPU_] |195| 
        ADDL      XAR4,ACC              ; [CPU_] |195| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |195| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |195| 
        LSL64     ACC:P,1               ; [CPU_] |195| 
        MOVL      XAR6,P                ; [CPU_] |195| 
        MOVL      XAR7,ACC              ; [CPU_] |195| 
        MOVL      P,@_REC_RecPoint      ; [CPU_] |195| 
        MOVL      ACC,@_REC_RecPoint+2  ; [CPU_] |195| 
        SUBUL     P,XAR6                ; [CPU_] |195| 
        MOVL      @_REC_RecPoint,P      ; [CPU_] |195| 
        SUBBL     ACC,XAR7              ; [CPU_] |195| 
        MOVL      @_REC_RecPoint+2,ACC  ; [CPU_] |195| 
$C$L3:    
	.dwpsn	file "../recorder.c",line 197,column 9,is_stmt
        MOVW      DP,#_ODV_Recorder_Start ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Start ; [CPU_] |197| 
        BF        $C$L4,NEQ             ; [CPU_] |197| 
        ; branchcc occurs ; [] |197| 
	.dwpsn	file "../recorder.c",line 198,column 11,is_stmt
        MOVW      DP,#_REC_RecordSize+6 ; [CPU_U] 
        MOV       T,@_REC_RecordSize+6  ; [CPU_] |198| 
        MOVW      DP,#_ODV_Recorder_PreTrigger ; [CPU_U] 
        MPY       ACC,T,@_ODV_Recorder_PreTrigger ; [CPU_] |198| 
        MOVW      DP,#_BitIndex         ; [CPU_U] 
        MOVU      ACC,AL                ; [CPU_] |198| 
        CMPL      ACC,@_BitIndex        ; [CPU_] |198| 
        B         $C$L7,HI              ; [CPU_] |198| 
        ; branchcc occurs ; [] |198| 
	.dwpsn	file "../recorder.c",line 199,column 13,is_stmt
        MOVW      DP,#_ODV_Recorder_Start ; [CPU_U] 
        MOV       @_ODV_Recorder_Start,#65535 ; [CPU_] |199| 
	.dwpsn	file "../recorder.c",line 200,column 9,is_stmt
        B         $C$L7,UNC             ; [CPU_] |200| 
        ; branch occurs ; [] |200| 
$C$L4:    
	.dwpsn	file "../recorder.c",line 201,column 14,is_stmt
        MOVW      DP,#_ODV_Recorder_TriggerLevel ; [CPU_U] 
        MOVL      ACC,@_ODV_Recorder_TriggerLevel ; [CPU_] |201| 
        MOVW      DP,#_REC_RecPoint     ; [CPU_U] 
        ASR64     ACC:P,16              ; [CPU_] |201| 
        ASR64     ACC:P,16              ; [CPU_] |201| 
        MOVL      *-SP[4],P             ; [CPU_] |201| 
        MOVL      *-SP[2],ACC           ; [CPU_] |201| 
        MOVL      P,@_REC_RecPoint      ; [CPU_] |201| 
        MOVL      ACC,@_REC_RecPoint+2  ; [CPU_] |201| 
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("LL$$CMP")
	.dwattr $C$DW$77, DW_AT_TI_call
        FFC       XAR7,#LL$$CMP         ; [CPU_] |201| 
        ; call occurs [#LL$$CMP] ; [] |201| 
        CMPB      AL,#0                 ; [CPU_] |201| 
        B         $C$L5,LT              ; [CPU_] |201| 
        ; branchcc occurs ; [] |201| 
        MOVW      DP,#_ODV_Recorder_TriggerLevel ; [CPU_U] 
        MOVL      ACC,@_ODV_Recorder_TriggerLevel ; [CPU_] |201| 
        MOVW      DP,#_REC_OldRecPoint  ; [CPU_U] 
        ASR64     ACC:P,16              ; [CPU_] |201| 
        ASR64     ACC:P,16              ; [CPU_] |201| 
        MOVL      *-SP[4],P             ; [CPU_] |201| 
        MOVL      *-SP[2],ACC           ; [CPU_] |201| 
        MOVL      P,@_REC_OldRecPoint   ; [CPU_] |201| 
        MOVL      ACC,@_REC_OldRecPoint+2 ; [CPU_] |201| 
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_name("LL$$CMP")
	.dwattr $C$DW$78, DW_AT_TI_call
        FFC       XAR7,#LL$$CMP         ; [CPU_] |201| 
        ; call occurs [#LL$$CMP] ; [] |201| 
        CMPB      AL,#0                 ; [CPU_] |201| 
        B         $C$L5,GEQ             ; [CPU_] |201| 
        ; branchcc occurs ; [] |201| 
        TBIT      *-SP[5],#3            ; [CPU_] |201| 
        BF        $C$L6,TC              ; [CPU_] |201| 
        ; branchcc occurs ; [] |201| 
$C$L5:    
        MOVW      DP,#_ODV_Recorder_TriggerLevel ; [CPU_U] 
        MOVL      ACC,@_ODV_Recorder_TriggerLevel ; [CPU_] |201| 
        MOVW      DP,#_REC_RecPoint     ; [CPU_U] 
        ASR64     ACC:P,16              ; [CPU_] |201| 
        ASR64     ACC:P,16              ; [CPU_] |201| 
        MOVL      *-SP[4],P             ; [CPU_] |201| 
        MOVL      *-SP[2],ACC           ; [CPU_] |201| 
        MOVL      P,@_REC_RecPoint      ; [CPU_] |201| 
        MOVL      ACC,@_REC_RecPoint+2  ; [CPU_] |201| 
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_name("LL$$CMP")
	.dwattr $C$DW$79, DW_AT_TI_call
        FFC       XAR7,#LL$$CMP         ; [CPU_] |201| 
        ; call occurs [#LL$$CMP] ; [] |201| 
        CMPB      AL,#0                 ; [CPU_] |201| 
        B         $C$L7,GT              ; [CPU_] |201| 
        ; branchcc occurs ; [] |201| 
        MOVW      DP,#_ODV_Recorder_TriggerLevel ; [CPU_U] 
        MOVL      ACC,@_ODV_Recorder_TriggerLevel ; [CPU_] |201| 
        MOVW      DP,#_REC_OldRecPoint  ; [CPU_U] 
        ASR64     ACC:P,16              ; [CPU_] |201| 
        ASR64     ACC:P,16              ; [CPU_] |201| 
        MOVL      *-SP[4],P             ; [CPU_] |201| 
        MOVL      *-SP[2],ACC           ; [CPU_] |201| 
        MOVL      P,@_REC_OldRecPoint   ; [CPU_] |201| 
        MOVL      ACC,@_REC_OldRecPoint+2 ; [CPU_] |201| 
$C$DW$80	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$80, DW_AT_low_pc(0x00)
	.dwattr $C$DW$80, DW_AT_name("LL$$CMP")
	.dwattr $C$DW$80, DW_AT_TI_call
        FFC       XAR7,#LL$$CMP         ; [CPU_] |201| 
        ; call occurs [#LL$$CMP] ; [] |201| 
        CMPB      AL,#0                 ; [CPU_] |201| 
        B         $C$L7,LEQ             ; [CPU_] |201| 
        ; branchcc occurs ; [] |201| 
        TBIT      *-SP[5],#4            ; [CPU_] |201| 
        BF        $C$L7,NTC             ; [CPU_] |201| 
        ; branchcc occurs ; [] |201| 
$C$L6:    
	.dwpsn	file "../recorder.c",line 203,column 11,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        AND       @_ODV_Recorder_Control,#0xffdf ; [CPU_] |203| 
	.dwpsn	file "../recorder.c",line 205,column 11,is_stmt
        MOVW      DP,#_REC_RecordSize+6 ; [CPU_U] 
        MOV       T,@_REC_RecordSize+6  ; [CPU_] |205| 
        MOV       AL,@_BitIndex         ; [CPU_] |205| 
        MOVW      DP,#_ODV_Recorder_PreTrigger ; [CPU_U] 
        MPY       P,T,@_ODV_Recorder_PreTrigger ; [CPU_] |205| 
        MOVW      DP,#_ODV_Recorder_TriggerIndex ; [CPU_U] 
        SUB       AL,PL                 ; [CPU_] |205| 
        MOV       @_ODV_Recorder_TriggerIndex,AL ; [CPU_] |205| 
	.dwpsn	file "../recorder.c",line 206,column 11,is_stmt
        MOVW      DP,#_ODV_Recorder_PreTrigger ; [CPU_U] 
        MOVU      ACC,@_ODV_Recorder_PreTrigger ; [CPU_] |206| 
        MOVW      DP,#_ODV_Recorder_NbOfSamples ; [CPU_U] 
        MOVL      @_ODV_Recorder_NbOfSamples,ACC ; [CPU_] |206| 
	.dwpsn	file "../recorder.c",line 207,column 11,is_stmt
        MOVW      DP,#_ODV_Recorder_TriggerIndex ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_TriggerIndex ; [CPU_] |207| 
        MOVW      DP,#_ODV_Recorder_ReadIndex ; [CPU_U] 
        LSR       AL,3                  ; [CPU_] |207| 
        MOV       @_ODV_Recorder_ReadIndex,AL ; [CPU_] |207| 
$C$L7:    
	.dwpsn	file "../recorder.c",line 209,column 9,is_stmt
        MOVW      DP,#_REC_RecPoint     ; [CPU_U] 
        MOVL      XAR6,@_REC_RecPoint+2 ; [CPU_] |209| 
        MOVL      ACC,@_REC_RecPoint    ; [CPU_] |209| 
        MOVL      @_REC_OldRecPoint,ACC ; [CPU_] |209| 
        MOVL      @_REC_OldRecPoint+2,XAR6 ; [CPU_] |209| 
$C$L8:    
	.dwpsn	file "../recorder.c",line 213,column 5,is_stmt
        MOVW      DP,#_REC_RecordSize   ; [CPU_U] 
        MOV       AL,@_REC_RecordSize   ; [CPU_] |213| 
        MOVL      XAR4,@_REC_RecordSource ; [CPU_] |213| 
$C$DW$81	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$81, DW_AT_low_pc(0x00)
	.dwattr $C$DW$81, DW_AT_name("_AddData")
	.dwattr $C$DW$81, DW_AT_TI_call
        LCR       #_AddData             ; [CPU_] |213| 
        ; call occurs [#_AddData] ; [] |213| 
	.dwpsn	file "../recorder.c",line 214,column 5,is_stmt
        MOVW      DP,#_REC_RecordSize+1 ; [CPU_U] 
        MOV       AL,@_REC_RecordSize+1 ; [CPU_] |214| 
        MOVL      XAR4,@_REC_RecordSource+2 ; [CPU_] |214| 
$C$DW$82	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$82, DW_AT_low_pc(0x00)
	.dwattr $C$DW$82, DW_AT_name("_AddData")
	.dwattr $C$DW$82, DW_AT_TI_call
        LCR       #_AddData             ; [CPU_] |214| 
        ; call occurs [#_AddData] ; [] |214| 
	.dwpsn	file "../recorder.c",line 215,column 5,is_stmt
        MOVW      DP,#_REC_RecordSize+2 ; [CPU_U] 
        MOV       AL,@_REC_RecordSize+2 ; [CPU_] |215| 
        MOVL      XAR4,@_REC_RecordSource+4 ; [CPU_] |215| 
$C$DW$83	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$83, DW_AT_low_pc(0x00)
	.dwattr $C$DW$83, DW_AT_name("_AddData")
	.dwattr $C$DW$83, DW_AT_TI_call
        LCR       #_AddData             ; [CPU_] |215| 
        ; call occurs [#_AddData] ; [] |215| 
	.dwpsn	file "../recorder.c",line 216,column 5,is_stmt
        MOVW      DP,#_REC_RecordSize+3 ; [CPU_U] 
        MOV       AL,@_REC_RecordSize+3 ; [CPU_] |216| 
        MOVL      XAR4,@_REC_RecordSource+6 ; [CPU_] |216| 
$C$DW$84	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$84, DW_AT_low_pc(0x00)
	.dwattr $C$DW$84, DW_AT_name("_AddData")
	.dwattr $C$DW$84, DW_AT_TI_call
        LCR       #_AddData             ; [CPU_] |216| 
        ; call occurs [#_AddData] ; [] |216| 
	.dwpsn	file "../recorder.c",line 217,column 5,is_stmt
        MOVW      DP,#_REC_RecordSize+4 ; [CPU_U] 
        MOV       AL,@_REC_RecordSize+4 ; [CPU_] |217| 
        MOVL      XAR4,@_REC_RecordSource+8 ; [CPU_] |217| 
$C$DW$85	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$85, DW_AT_low_pc(0x00)
	.dwattr $C$DW$85, DW_AT_name("_AddData")
	.dwattr $C$DW$85, DW_AT_TI_call
        LCR       #_AddData             ; [CPU_] |217| 
        ; call occurs [#_AddData] ; [] |217| 
	.dwpsn	file "../recorder.c",line 218,column 5,is_stmt
        MOVW      DP,#_REC_RecordSize+5 ; [CPU_U] 
        MOV       AL,@_REC_RecordSize+5 ; [CPU_] |218| 
        MOVL      XAR4,@_REC_RecordSource+10 ; [CPU_] |218| 
$C$DW$86	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$86, DW_AT_low_pc(0x00)
	.dwattr $C$DW$86, DW_AT_name("_AddData")
	.dwattr $C$DW$86, DW_AT_TI_call
        LCR       #_AddData             ; [CPU_] |218| 
        ; call occurs [#_AddData] ; [] |218| 
	.dwpsn	file "../recorder.c",line 219,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        TBIT      @_ODV_Recorder_Control,#5 ; [CPU_] |219| 
        BF        $C$L11,TC             ; [CPU_] |219| 
        ; branchcc occurs ; [] |219| 
	.dwpsn	file "../recorder.c",line 220,column 7,is_stmt
        MOVL      XAR4,#65536           ; [CPU_U] |220| 
        MOVW      DP,#_REC_RecordSize+6 ; [CPU_U] 
        MOVL      ACC,XAR4              ; [CPU_] |220| 
        SUBU      ACC,@_REC_RecordSize+6 ; [CPU_] |220| 
        CMPL      ACC,@_BitIndex        ; [CPU_] |220| 
        B         $C$L9,HI              ; [CPU_] |220| 
        ; branchcc occurs ; [] |220| 
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        TBIT      @_ODV_Recorder_Control,#4 ; [CPU_] |220| 
        BF        $C$L9,TC              ; [CPU_] |220| 
        ; branchcc occurs ; [] |220| 
	.dwpsn	file "../recorder.c",line 221,column 9,is_stmt
        AND       @_ODV_Recorder_Control,#0xfffe ; [CPU_] |221| 
$C$L9:    
	.dwpsn	file "../recorder.c",line 222,column 7,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        TBIT      @_ODV_Recorder_Control,#4 ; [CPU_] |222| 
        BF        $C$L10,NTC            ; [CPU_] |222| 
        ; branchcc occurs ; [] |222| 
        MOVW      DP,#_ODV_Recorder_ReadIndex ; [CPU_U] 
        MOVZ      AR6,@_ODV_Recorder_ReadIndex ; [CPU_] |222| 
        CLRC      SXM                   ; [CPU_] 
        MOVW      DP,#_BitIndex         ; [CPU_U] 
        MOVL      ACC,@_BitIndex        ; [CPU_] |222| 
        ADD       AR6,#8192             ; [CPU_] |222| 
        AND       AR6,#0x1fff           ; [CPU_] |222| 
        SFR       ACC,3                 ; [CPU_] |222| 
        MOVZ      AR6,AR6               ; [CPU_] |222| 
        CMPL      ACC,XAR6              ; [CPU_] |222| 
        B         $C$L10,HI             ; [CPU_] |222| 
        ; branchcc occurs ; [] |222| 
        MOVU      ACC,@_REC_RecordSize+6 ; [CPU_] |222| 
        MOVW      DP,#_ODV_Recorder_ReadIndex ; [CPU_U] 
        MOVZ      AR6,@_ODV_Recorder_ReadIndex ; [CPU_] |222| 
        MOVW      DP,#_BitIndex         ; [CPU_U] 
        ADDL      ACC,@_BitIndex        ; [CPU_] |222| 
        SFR       ACC,3                 ; [CPU_] |222| 
        MOVB      AH,#0                 ; [CPU_] |222| 
        AND       AL,#0x1fff            ; [CPU_] |222| 
        CMPL      ACC,XAR6              ; [CPU_] |222| 
        B         $C$L10,LO             ; [CPU_] |222| 
        ; branchcc occurs ; [] |222| 
	.dwpsn	file "../recorder.c",line 224,column 9,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        AND       @_ODV_Recorder_Control,#0xfffe ; [CPU_] |224| 
$C$L10:    
	.dwpsn	file "../recorder.c",line 225,column 7,is_stmt
        MOVB      ACC,#1                ; [CPU_] |225| 
        MOVW      DP,#_ODV_Recorder_NbOfSamples ; [CPU_U] 
        ADDL      @_ODV_Recorder_NbOfSamples,ACC ; [CPU_] |225| 
	.dwpsn	file "../recorder.c",line 228,column 1,is_stmt
$C$L11:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR2,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 9
$C$DW$87	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$87, DW_AT_low_pc(0x00)
	.dwattr $C$DW$87, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$73, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$73, DW_AT_TI_end_line(0xe4)
	.dwattr $C$DW$73, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$73

	.sect	".text"
	.global	_REC_StartRecorder

$C$DW$88	.dwtag  DW_TAG_subprogram, DW_AT_name("REC_StartRecorder")
	.dwattr $C$DW$88, DW_AT_low_pc(_REC_StartRecorder)
	.dwattr $C$DW$88, DW_AT_high_pc(0x00)
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_REC_StartRecorder")
	.dwattr $C$DW$88, DW_AT_external
	.dwattr $C$DW$88, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$88, DW_AT_TI_begin_line(0xeb)
	.dwattr $C$DW$88, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$88, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../recorder.c",line 235,column 29,is_stmt,address _REC_StartRecorder

	.dwfde $C$DW$CIE, _REC_StartRecorder

;***************************************************************
;* FNAME: _REC_StartRecorder            FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            7 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_REC_StartRecorder:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -8]
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("vec")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_vec")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_breg20 -9]
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_breg20 -12]
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("type")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_type")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_breg20 -13]
	.dwpsn	file "../recorder.c",line 240,column 3,is_stmt
        MOVW      DP,#_REC_RecordSize+6 ; [CPU_U] 
        MOV       @_REC_RecordSize+6,#0 ; [CPU_] |240| 
	.dwpsn	file "../recorder.c",line 241,column 8,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |241| 
	.dwpsn	file "../recorder.c",line 241,column 13,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |241| 
        CMPB      AL,#6                 ; [CPU_] |241| 
        B         $C$L40,HIS            ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
$C$L12:    
	.dwpsn	file "../recorder.c",line 242,column 5,is_stmt
        MOV       ACC,*-SP[8] << #3     ; [CPU_] |242| 
        MOVW      DP,#_ODV_Recorder_Vectors ; [CPU_U] 
        MOV       T,AL                  ; [CPU_] |242| 
        MOVL      P,@_ODV_Recorder_Vectors ; [CPU_] |242| 
        MOVL      ACC,@_ODV_Recorder_Vectors+2 ; [CPU_] |242| 
        LSR64     ACC:P,T               ; [CPU_] |242| 
        MOV       AL,PL                 ; [CPU_] |242| 
        ANDB      AL,#0xff              ; [CPU_] |242| 
        MOV       *-SP[9],AL            ; [CPU_] |242| 
	.dwpsn	file "../recorder.c",line 243,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |243| 
        MOVL      *-SP[12],ACC          ; [CPU_] |243| 
	.dwpsn	file "../recorder.c",line 244,column 5,is_stmt
        MOVL      XAR4,#_RecordOffset   ; [CPU_U] |244| 
        ZAPA      ; [CPU_] |244| 
        MOVL      XAR6,ACC              ; [CPU_] |244| 
        MOVU      ACC,*-SP[8]           ; [CPU_] |244| 
        LSL       ACC,2                 ; [CPU_] |244| 
        ADDL      XAR4,ACC              ; [CPU_] |244| 
        MOVL      *+XAR4[0],P           ; [CPU_] |244| 
        MOVL      *+XAR4[2],XAR6        ; [CPU_] |244| 
	.dwpsn	file "../recorder.c",line 245,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |245| 
        BF        $C$L13,NEQ            ; [CPU_] |245| 
        ; branchcc occurs ; [] |245| 
	.dwpsn	file "../recorder.c",line 247,column 7,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |247| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |247| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |247| 
	.dwpsn	file "../recorder.c",line 248,column 5,is_stmt
        B         $C$L39,UNC            ; [CPU_] |248| 
        ; branch occurs ; [] |248| 
$C$L13:    
	.dwpsn	file "../recorder.c",line 250,column 7,is_stmt
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("_GetVarSize")
	.dwattr $C$DW$93, DW_AT_TI_call
        LCR       #_GetVarSize          ; [CPU_] |250| 
        ; call occurs [#_GetVarSize] ; [] |250| 
        MOV       AH,*-SP[9]            ; [CPU_] |250| 
        ADDB      AH,#-1                ; [CPU_] |250| 
        CMP       AL,AH                 ; [CPU_] |250| 
        MOV       *-SP[9],AH            ; [CPU_] |250| 
        B         $C$L14,LOS            ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
	.dwpsn	file "../recorder.c",line 251,column 9,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |251| 
        MOVZ      AR4,SP                ; [CPU_U] |251| 
        SUBB      XAR5,#12              ; [CPU_U] |251| 
        SUBB      XAR4,#13              ; [CPU_U] |251| 
        MOVL      *-SP[2],XAR5          ; [CPU_] |251| 
        MOV       T,#10                 ; [CPU_] |251| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |251| 
        MOV       *-SP[5],#0            ; [CPU_] |251| 
        MOVL      XAR5,#_Variables      ; [CPU_U] |251| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOV       *-SP[6],#0            ; [CPU_] |251| 
        MOVL      XAR4,#_Variables+1    ; [CPU_U] |251| 
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |251| 
        MPYXU     ACC,T,*-SP[9]         ; [CPU_] |251| 
        ADDL      XAR5,ACC              ; [CPU_] |251| 
        MOVZ      AR7,*+XAR5[0]         ; [CPU_] |251| 
        MPYXU     ACC,T,*-SP[9]         ; [CPU_] |251| 
        MOVL      XAR5,#_REC_RecordSource ; [CPU_U] |251| 
        ADDL      XAR4,ACC              ; [CPU_] |251| 
        MOVU      ACC,*-SP[8]           ; [CPU_] |251| 
        LSL       ACC,1                 ; [CPU_] |251| 
        ADDL      XAR5,ACC              ; [CPU_] |251| 
        MOV       AL,AR7                ; [CPU_] |251| 
        MOV       AH,*+XAR4[0]          ; [CPU_] |251| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |251| 
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("__getODentry")
	.dwattr $C$DW$94, DW_AT_TI_call
        LCR       #__getODentry         ; [CPU_] |251| 
        ; call occurs [#__getODentry] ; [] |251| 
$C$L14:    
	.dwpsn	file "../recorder.c",line 254,column 7,is_stmt
        MOV       ACC,#1000             ; [CPU_] |254| 
        MOVW      DP,#_ODV_Recorder_Period ; [CPU_U] 
        CMPL      ACC,@_ODV_Recorder_Period ; [CPU_] |254| 
        B         $C$L35,LOS            ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
	.dwpsn	file "../recorder.c",line 255,column 9,is_stmt
        B         $C$L20,UNC            ; [CPU_] |255| 
        ; branch occurs ; [] |255| 
$C$L15:    
	.dwpsn	file "../recorder.c",line 258,column 34,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |258| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |258| 
        MOVB      *+XAR4[AR0],#8,UNC    ; [CPU_] |258| 
	.dwpsn	file "../recorder.c",line 258,column 57,is_stmt
        B         $C$L39,UNC            ; [CPU_] |258| 
        ; branch occurs ; [] |258| 
$C$L16:    
	.dwpsn	file "../recorder.c",line 261,column 34,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |261| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |261| 
        MOVB      *+XAR4[AR0],#16,UNC   ; [CPU_] |261| 
	.dwpsn	file "../recorder.c",line 261,column 58,is_stmt
        B         $C$L39,UNC            ; [CPU_] |261| 
        ; branch occurs ; [] |261| 
$C$L17:    
	.dwpsn	file "../recorder.c",line 264,column 34,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |264| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |264| 
        MOVB      *+XAR4[AR0],#32,UNC   ; [CPU_] |264| 
	.dwpsn	file "../recorder.c",line 264,column 58,is_stmt
        B         $C$L39,UNC            ; [CPU_] |264| 
        ; branch occurs ; [] |264| 
$C$L18:    
	.dwpsn	file "../recorder.c",line 267,column 34,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |267| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |267| 
        MOVB      *+XAR4[AR0],#64,UNC   ; [CPU_] |267| 
	.dwpsn	file "../recorder.c",line 267,column 58,is_stmt
        B         $C$L39,UNC            ; [CPU_] |267| 
        ; branch occurs ; [] |267| 
$C$L19:    
	.dwpsn	file "../recorder.c",line 268,column 20,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |268| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |268| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |268| 
	.dwpsn	file "../recorder.c",line 268,column 43,is_stmt
        B         $C$L39,UNC            ; [CPU_] |268| 
        ; branch occurs ; [] |268| 
$C$L20:    
	.dwpsn	file "../recorder.c",line 255,column 9,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |255| 
        CMPB      AL,#7                 ; [CPU_] |255| 
        B         $C$L22,GT             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#7                 ; [CPU_] |255| 
        BF        $C$L17,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#4                 ; [CPU_] |255| 
        B         $C$L21,GT             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#4                 ; [CPU_] |255| 
        BF        $C$L17,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#1                 ; [CPU_] |255| 
        BF        $C$L15,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#2                 ; [CPU_] |255| 
        BF        $C$L15,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#3                 ; [CPU_] |255| 
        BF        $C$L16,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        B         $C$L19,UNC            ; [CPU_] |255| 
        ; branch occurs ; [] |255| 
$C$L21:    
        CMPB      AL,#5                 ; [CPU_] |255| 
        BF        $C$L15,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#6                 ; [CPU_] |255| 
        BF        $C$L16,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        B         $C$L19,UNC            ; [CPU_] |255| 
        ; branch occurs ; [] |255| 
$C$L22:    
        CMPB      AL,#21                ; [CPU_] |255| 
        B         $C$L23,GT             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#21                ; [CPU_] |255| 
        BF        $C$L18,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#8                 ; [CPU_] |255| 
        BF        $C$L17,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#17                ; [CPU_] |255| 
        BF        $C$L18,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        B         $C$L19,UNC            ; [CPU_] |255| 
        ; branch occurs ; [] |255| 
$C$L23:    
        CMPB      AL,#27                ; [CPU_] |255| 
        BF        $C$L18,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        CMPB      AL,#28                ; [CPU_] |255| 
        BF        $C$L16,EQ             ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
        B         $C$L19,UNC            ; [CPU_] |255| 
        ; branch occurs ; [] |255| 
$C$L24:    
	.dwpsn	file "../recorder.c",line 272,column 35,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |272| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |272| 
        MOVB      *+XAR4[AR0],#1,UNC    ; [CPU_] |272| 
	.dwpsn	file "../recorder.c",line 272,column 58,is_stmt
        B         $C$L39,UNC            ; [CPU_] |272| 
        ; branch occurs ; [] |272| 
$C$L25:    
	.dwpsn	file "../recorder.c",line 273,column 34,is_stmt
        MOVU      ACC,*-SP[8]           ; [CPU_] |273| 
        MOVB      XAR6,#128             ; [CPU_] |273| 
        MOVL      XAR4,#_RecordOffset   ; [CPU_U] |273| 
        LSL       ACC,2                 ; [CPU_] |273| 
        ADDL      XAR4,ACC              ; [CPU_] |273| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |273| 
        MOV       *+XAR4[2],#0          ; [CPU_] |273| 
        MOV       *+XAR4[3],#0          ; [CPU_] |273| 
$C$L26:    
	.dwpsn	file "../recorder.c",line 274,column 34,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |274| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |274| 
        MOVB      *+XAR4[AR0],#8,UNC    ; [CPU_] |274| 
	.dwpsn	file "../recorder.c",line 274,column 57,is_stmt
        B         $C$L39,UNC            ; [CPU_] |274| 
        ; branch occurs ; [] |274| 
$C$L27:    
	.dwpsn	file "../recorder.c",line 275,column 34,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |275| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |275| 
        MOVB      *+XAR4[AR0],#12,UNC   ; [CPU_] |275| 
	.dwpsn	file "../recorder.c",line 275,column 58,is_stmt
        B         $C$L39,UNC            ; [CPU_] |275| 
        ; branch occurs ; [] |275| 
$C$L28:    
	.dwpsn	file "../recorder.c",line 276,column 34,is_stmt
        MOVU      ACC,*-SP[8]           ; [CPU_] |276| 
        MOVL      XAR4,#_RecordOffset   ; [CPU_U] |276| 
        MOVL      XAR5,#32768           ; [CPU_U] |276| 
        LSL       ACC,2                 ; [CPU_] |276| 
        ADDL      XAR4,ACC              ; [CPU_] |276| 
        MOVL      *+XAR4[0],XAR5        ; [CPU_] |276| 
        MOV       *+XAR4[2],#0          ; [CPU_] |276| 
        MOV       *+XAR4[3],#0          ; [CPU_] |276| 
$C$L29:    
	.dwpsn	file "../recorder.c",line 277,column 34,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |277| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |277| 
        MOVB      *+XAR4[AR0],#16,UNC   ; [CPU_] |277| 
	.dwpsn	file "../recorder.c",line 277,column 58,is_stmt
        B         $C$L39,UNC            ; [CPU_] |277| 
        ; branch occurs ; [] |277| 
$C$L30:    
	.dwpsn	file "../recorder.c",line 278,column 34,is_stmt
        MOVU      ACC,*-SP[8]           ; [CPU_] |278| 
        MOVL      XAR4,#_RecordOffset   ; [CPU_U] |278| 
        LSL       ACC,2                 ; [CPU_] |278| 
        ADDL      XAR4,ACC              ; [CPU_] |278| 
        MOV       AL,#0                 ; [CPU_] |278| 
        MOV       AH,#32768             ; [CPU_] |278| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |278| 
        MOV       *+XAR4[2],#0          ; [CPU_] |278| 
        MOV       *+XAR4[3],#0          ; [CPU_] |278| 
$C$L31:    
	.dwpsn	file "../recorder.c",line 280,column 34,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |280| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |280| 
        MOVB      *+XAR4[AR0],#32,UNC   ; [CPU_] |280| 
	.dwpsn	file "../recorder.c",line 280,column 58,is_stmt
        B         $C$L39,UNC            ; [CPU_] |280| 
        ; branch occurs ; [] |280| 
$C$L32:    
	.dwpsn	file "../recorder.c",line 281,column 34,is_stmt
        MOV       AL,#0                 ; [CPU_] |281| 
        MOV       AH,#32768             ; [CPU_] |281| 
        MOVL      XAR4,#_RecordOffset   ; [CPU_U] |281| 
        MOV       P,#0                  ; [CPU_] |281| 
        MOVL      XAR6,ACC              ; [CPU_] |281| 
        MOVU      ACC,*-SP[8]           ; [CPU_] |281| 
        LSL       ACC,2                 ; [CPU_] |281| 
        ADDL      XAR4,ACC              ; [CPU_] |281| 
        MOVL      *+XAR4[0],P           ; [CPU_] |281| 
        MOVL      *+XAR4[2],XAR6        ; [CPU_] |281| 
$C$L33:    
	.dwpsn	file "../recorder.c",line 283,column 34,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |283| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |283| 
        MOVB      *+XAR4[AR0],#64,UNC   ; [CPU_] |283| 
	.dwpsn	file "../recorder.c",line 283,column 58,is_stmt
        B         $C$L39,UNC            ; [CPU_] |283| 
        ; branch occurs ; [] |283| 
$C$L34:    
	.dwpsn	file "../recorder.c",line 284,column 20,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |284| 
        MOVL      XAR4,#_REC_RecordSize ; [CPU_U] |284| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |284| 
	.dwpsn	file "../recorder.c",line 284,column 43,is_stmt
        B         $C$L39,UNC            ; [CPU_] |284| 
        ; branch occurs ; [] |284| 
$C$L35:    
	.dwpsn	file "../recorder.c",line 271,column 9,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |271| 
        CMPB      AL,#7                 ; [CPU_] |271| 
        B         $C$L37,GT             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#7                 ; [CPU_] |271| 
        BF        $C$L31,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#4                 ; [CPU_] |271| 
        B         $C$L36,GT             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#4                 ; [CPU_] |271| 
        BF        $C$L30,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#1                 ; [CPU_] |271| 
        BF        $C$L24,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#2                 ; [CPU_] |271| 
        BF        $C$L25,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#3                 ; [CPU_] |271| 
        BF        $C$L28,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        B         $C$L34,UNC            ; [CPU_] |271| 
        ; branch occurs ; [] |271| 
$C$L36:    
        CMPB      AL,#5                 ; [CPU_] |271| 
        BF        $C$L26,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#6                 ; [CPU_] |271| 
        BF        $C$L29,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        B         $C$L34,UNC            ; [CPU_] |271| 
        ; branch occurs ; [] |271| 
$C$L37:    
        CMPB      AL,#21                ; [CPU_] |271| 
        B         $C$L38,GT             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#21                ; [CPU_] |271| 
        BF        $C$L32,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#8                 ; [CPU_] |271| 
        BF        $C$L31,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#17                ; [CPU_] |271| 
        BF        $C$L33,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        B         $C$L34,UNC            ; [CPU_] |271| 
        ; branch occurs ; [] |271| 
$C$L38:    
        CMPB      AL,#27                ; [CPU_] |271| 
        BF        $C$L33,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        CMPB      AL,#28                ; [CPU_] |271| 
        BF        $C$L27,EQ             ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
        B         $C$L34,UNC            ; [CPU_] |271| 
        ; branch occurs ; [] |271| 
$C$L39:    
	.dwpsn	file "../recorder.c",line 288,column 5,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |288| 
        MOVW      DP,#_REC_RecordSize+6 ; [CPU_U] 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |288| 
        ADD       @_REC_RecordSize+6,AL ; [CPU_] |288| 
	.dwpsn	file "../recorder.c",line 241,column 18,is_stmt
        INC       *-SP[8]               ; [CPU_] |241| 
	.dwpsn	file "../recorder.c",line 241,column 13,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |241| 
        CMPB      AL,#6                 ; [CPU_] |241| 
        B         $C$L12,LO             ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
$C$L40:    
	.dwpsn	file "../recorder.c",line 290,column 3,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        AND       @_ODV_Recorder_Control,#0xfffd ; [CPU_] |290| 
	.dwpsn	file "../recorder.c",line 291,column 3,is_stmt
        AND       @_ODV_Recorder_Control,#0xfffe ; [CPU_] |291| 
	.dwpsn	file "../recorder.c",line 292,column 3,is_stmt
        MOVW      DP,#_ODV_Recorder_Start ; [CPU_U] 
        CMP       @_ODV_Recorder_Start,#65535 ; [CPU_] |292| 
        BF        $C$L43,EQ             ; [CPU_] |292| 
        ; branchcc occurs ; [] |292| 
	.dwpsn	file "../recorder.c",line 293,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |293| 
        MOVW      DP,#_ODV_Recorder_NbOfSamples ; [CPU_U] 
        MOVL      @_ODV_Recorder_NbOfSamples,ACC ; [CPU_] |293| 
	.dwpsn	file "../recorder.c",line 294,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_ReadIndex ; [CPU_U] 
        MOV       @_ODV_Recorder_ReadIndex,#0 ; [CPU_] |294| 
	.dwpsn	file "../recorder.c",line 295,column 5,is_stmt
        MOVW      DP,#_BitIndex         ; [CPU_U] 
        MOVL      @_BitIndex,ACC        ; [CPU_] |295| 
	.dwpsn	file "../recorder.c",line 296,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_TriggerIndex ; [CPU_U] 
        MOV       @_ODV_Recorder_TriggerIndex,#0 ; [CPU_] |296| 
	.dwpsn	file "../recorder.c",line 297,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        TBIT      @_ODV_Recorder_Control,#2 ; [CPU_] |297| 
        BF        $C$L41,NTC            ; [CPU_] |297| 
        ; branchcc occurs ; [] |297| 
	.dwpsn	file "../recorder.c",line 297,column 41,is_stmt
        OR        @_ODV_Recorder_Control,#0x0020 ; [CPU_] |297| 
$C$L41:    
	.dwpsn	file "../recorder.c",line 298,column 5,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |298| 
        MOVW      DP,#_ODV_Recorder_Period ; [CPU_U] 
        MOVL      ACC,XAR4              ; [CPU_] |298| 
        CMPL      ACC,@_ODV_Recorder_Period ; [CPU_] |298| 
        B         $C$L42,LOS            ; [CPU_] |298| 
        ; branchcc occurs ; [] |298| 
	.dwpsn	file "../recorder.c",line 299,column 7,is_stmt
        MOVB      XAR6,#80              ; [CPU_] |299| 
        MOVL      P,@_ODV_Recorder_Period ; [CPU_] |299| 
        MOVB      ACC,#0                ; [CPU_] |299| 
        MOVW      DP,#_REC_PosSampleInterval ; [CPU_U] 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |299| 
        MOV       @_REC_PosSampleInterval,P ; [CPU_] |299| 
	.dwpsn	file "../recorder.c",line 300,column 7,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        OR        @_ODV_Recorder_Control,#0x0002 ; [CPU_] |300| 
	.dwpsn	file "../recorder.c",line 301,column 5,is_stmt
        B         $C$L43,UNC            ; [CPU_] |301| 
        ; branch occurs ; [] |301| 
$C$L42:    
	.dwpsn	file "../recorder.c",line 303,column 7,is_stmt
        MOVL      P,@_ODV_Recorder_Period ; [CPU_] |303| 
        MOVB      ACC,#0                ; [CPU_] |303| 
        MOVW      DP,#_REC_PosSampleInterval ; [CPU_U] 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |303| 
        MOV       @_REC_PosSampleInterval,P ; [CPU_] |303| 
	.dwpsn	file "../recorder.c",line 304,column 7,is_stmt
        MOVW      DP,#_ODV_Recorder_Control ; [CPU_U] 
        OR        @_ODV_Recorder_Control,#0x0001 ; [CPU_] |304| 
	.dwpsn	file "../recorder.c",line 307,column 1,is_stmt
$C$L43:    
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$88, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$88, DW_AT_TI_end_line(0x133)
	.dwattr $C$DW$88, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$88

	.sect	"ramfuncs"
	.global	_AddData

$C$DW$96	.dwtag  DW_TAG_subprogram, DW_AT_name("AddData")
	.dwattr $C$DW$96, DW_AT_low_pc(_AddData)
	.dwattr $C$DW$96, DW_AT_high_pc(0x00)
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_AddData")
	.dwattr $C$DW$96, DW_AT_external
	.dwattr $C$DW$96, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$96, DW_AT_TI_begin_line(0x141)
	.dwattr $C$DW$96, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$96, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../recorder.c",line 322,column 1,is_stmt,address _AddData

	.dwfde $C$DW$CIE, _AddData
$C$DW$97	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg12]
$C$DW$98	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nbbit")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_nbbit")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _AddData                      FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_AddData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$99	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_breg20 -2]
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("nbbit")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_nbbit")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_breg20 -3]
$C$DW$101	.dwtag  DW_TAG_variable, DW_AT_name("ldatal")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_ldatal")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_breg20 -8]
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_breg20 -9]
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("decal")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_decal")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[3],AL            ; [CPU_] |322| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |322| 
	.dwpsn	file "../recorder.c",line 331,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |331| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |331| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |331| 
        MOVL      *-SP[8],ACC           ; [CPU_] |331| 
        MOVL      *-SP[6],XAR6          ; [CPU_] |331| 
	.dwpsn	file "../recorder.c",line 332,column 3,is_stmt
        MOVB      XAR6,#1               ; [CPU_] |332| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |332| 
        MOVB      AH,#0                 ; [CPU_] |332| 
        ANDB      AL,#0x01              ; [CPU_] |332| 
        CMPL      ACC,XAR6              ; [CPU_] |332| 
        BF        $C$L44,NEQ            ; [CPU_] |332| 
        ; branchcc occurs ; [] |332| 
	.dwpsn	file "../recorder.c",line 333,column 5,is_stmt
        MOVL      P,*-SP[8]             ; [CPU_] |333| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |333| 
        LSR64     ACC:P,16              ; [CPU_] |333| 
        MOVL      *-SP[8],P             ; [CPU_] |333| 
        MOVL      *-SP[6],ACC           ; [CPU_] |333| 
$C$L44:    
	.dwpsn	file "../recorder.c",line 334,column 3,is_stmt
        MOV       T,#64                 ; [CPU_] |334| 
        MOV       AL,T                  ; [CPU_] |334| 
        MOVL      P,*-SP[8]             ; [CPU_] |334| 
        SUB       AL,*-SP[3]            ; [CPU_] |334| 
        MOV       T,AL                  ; [CPU_] |334| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |334| 
        LSL64     ACC:P,T               ; [CPU_] |334| 
        MOVL      XAR6,ACC              ; [CPU_] |334| 
        MOVL      *-SP[8],P             ; [CPU_] |334| 
        MOVL      *-SP[6],XAR6          ; [CPU_] |334| 
	.dwpsn	file "../recorder.c",line 335,column 10,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |335| 
        BF        $C$L49,EQ             ; [CPU_] |335| 
        ; branchcc occurs ; [] |335| 
$C$L45:    
	.dwpsn	file "../recorder.c",line 336,column 5,is_stmt
        MOVW      DP,#_BitIndex         ; [CPU_U] 
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,@_BitIndex        ; [CPU_] |336| 
        SFR       ACC,4                 ; [CPU_] |336| 
        MOV       *-SP[9],AL            ; [CPU_] |336| 
	.dwpsn	file "../recorder.c",line 337,column 5,is_stmt
        MOVB      AH,#15                ; [CPU_] |337| 
        MOV       AL,@_BitIndex         ; [CPU_] |337| 
        ANDB      AL,#0x0f              ; [CPU_] |337| 
        SUB       AH,AL                 ; [CPU_] |337| 
        MOV       *-SP[10],AH           ; [CPU_] |337| 
	.dwpsn	file "../recorder.c",line 338,column 5,is_stmt
        TBIT      *-SP[5],#15           ; [CPU_] |338| 
        BF        $C$L46,NTC            ; [CPU_] |338| 
        ; branchcc occurs ; [] |338| 
	.dwpsn	file "../recorder.c",line 339,column 7,is_stmt
        MOVZ      AR6,*-SP[9]           ; [CPU_] |339| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |339| 
        MOV       T,*-SP[10]            ; [CPU_] |339| 
        MOVL      ACC,XAR4              ; [CPU_] |339| 
        ADDU      ACC,AR6               ; [CPU_] |339| 
        MOVL      XAR4,ACC              ; [CPU_] |339| 
        MOVB      AL,#1                 ; [CPU_] |339| 
        LSL       AL,T                  ; [CPU_] |339| 
        OR        *+XAR4[0],AL          ; [CPU_] |339| 
        B         $C$L47,UNC            ; [CPU_] |339| 
        ; branch occurs ; [] |339| 
$C$L46:    
	.dwpsn	file "../recorder.c",line 341,column 7,is_stmt
        MOVZ      AR6,*-SP[9]           ; [CPU_] |341| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |341| 
        MOV       T,*-SP[10]            ; [CPU_] |341| 
        MOVL      ACC,XAR4              ; [CPU_] |341| 
        ADDU      ACC,AR6               ; [CPU_] |341| 
        MOVL      XAR4,ACC              ; [CPU_] |341| 
        MOVB      AL,#1                 ; [CPU_] |341| 
        LSL       AL,T                  ; [CPU_] |341| 
        NOT       AL                    ; [CPU_] |341| 
        AND       *+XAR4[0],AL          ; [CPU_] |341| 
$C$L47:    
	.dwpsn	file "../recorder.c",line 342,column 5,is_stmt
        DEC       *-SP[3]               ; [CPU_] |342| 
	.dwpsn	file "../recorder.c",line 343,column 5,is_stmt
        MOVL      P,*-SP[8]             ; [CPU_] |343| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |343| 
        LSL64     ACC:P,1               ; [CPU_] |343| 
        MOVL      *-SP[8],P             ; [CPU_] |343| 
        MOVL      *-SP[6],ACC           ; [CPU_] |343| 
	.dwpsn	file "../recorder.c",line 344,column 5,is_stmt
        MOVL      XAR4,#65536           ; [CPU_U] |344| 
        MOVB      ACC,#1                ; [CPU_] |344| 
        ADDL      ACC,@_BitIndex        ; [CPU_] |344| 
        MOVL      XAR6,ACC              ; [CPU_] |344| 
        MOVL      @_BitIndex,ACC        ; [CPU_] |344| 
        MOVL      ACC,XAR4              ; [CPU_] |344| 
        CMPL      ACC,XAR6              ; [CPU_] |344| 
        BF        $C$L48,NEQ            ; [CPU_] |344| 
        ; branchcc occurs ; [] |344| 
	.dwpsn	file "../recorder.c",line 344,column 38,is_stmt
        MOVB      ACC,#0                ; [CPU_] |344| 
        MOVL      @_BitIndex,ACC        ; [CPU_] |344| 
$C$L48:    
	.dwpsn	file "../recorder.c",line 335,column 10,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |335| 
        BF        $C$L45,NEQ            ; [CPU_] |335| 
        ; branchcc occurs ; [] |335| 
	.dwpsn	file "../recorder.c",line 346,column 1,is_stmt
$C$L49:    
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$104	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$104, DW_AT_low_pc(0x00)
	.dwattr $C$DW$104, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$96, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$96, DW_AT_TI_end_line(0x15a)
	.dwattr $C$DW$96, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$96

	.sect	"ramfuncs"
	.global	_AddData2

$C$DW$105	.dwtag  DW_TAG_subprogram, DW_AT_name("AddData2")
	.dwattr $C$DW$105, DW_AT_low_pc(_AddData2)
	.dwattr $C$DW$105, DW_AT_high_pc(0x00)
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_AddData2")
	.dwattr $C$DW$105, DW_AT_external
	.dwattr $C$DW$105, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$105, DW_AT_TI_begin_line(0x168)
	.dwattr $C$DW$105, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$105, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../recorder.c",line 361,column 1,is_stmt,address _AddData2

	.dwfde $C$DW$CIE, _AddData2
$C$DW$106	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_reg12]
$C$DW$107	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nbbit")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_nbbit")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _AddData2                     FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_AddData2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$108	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_breg20 -2]
$C$DW$109	.dwtag  DW_TAG_variable, DW_AT_name("nbbit")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_nbbit")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_breg20 -3]
$C$DW$110	.dwtag  DW_TAG_variable, DW_AT_name("ldata")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_ldata")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_breg20 -6]
$C$DW$111	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_breg20 -7]
        MOV       *-SP[3],AL            ; [CPU_] |361| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |361| 
	.dwpsn	file "../recorder.c",line 362,column 15,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |362| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |362| 
        MOVL      *-SP[6],ACC           ; [CPU_] |362| 
	.dwpsn	file "../recorder.c",line 363,column 15,is_stmt
        MOVW      DP,#_BitIndex         ; [CPU_U] 
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,@_BitIndex        ; [CPU_] |363| 
        SFR       ACC,4                 ; [CPU_] |363| 
        MOV       *-SP[7],AL            ; [CPU_] |363| 
	.dwpsn	file "../recorder.c",line 364,column 3,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |364| 
        TBIT      AL,#0                 ; [CPU_] |364| 
        BF        $C$L50,NTC            ; [CPU_] |364| 
        ; branchcc occurs ; [] |364| 
	.dwpsn	file "../recorder.c",line 364,column 23,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |364| 
        MOVU      ACC,AH                ; [CPU_] |364| 
        MOVL      *-SP[6],ACC           ; [CPU_] |364| 
$C$L50:    
	.dwpsn	file "../recorder.c",line 365,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |365| 
        CMPB      AL,#8                 ; [CPU_] |365| 
        B         $C$L53,HI             ; [CPU_] |365| 
        ; branchcc occurs ; [] |365| 
	.dwpsn	file "../recorder.c",line 366,column 5,is_stmt
        TBIT      @_BitIndex,#3         ; [CPU_] |366| 
        BF        $C$L51,NTC            ; [CPU_] |366| 
        ; branchcc occurs ; [] |366| 
	.dwpsn	file "../recorder.c",line 367,column 7,is_stmt
        MOVZ      AR6,*-SP[7]           ; [CPU_] |367| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |367| 
        MOVL      ACC,XAR4              ; [CPU_] |367| 
        ADDU      ACC,AR6               ; [CPU_] |367| 
        MOVL      XAR4,ACC              ; [CPU_] |367| 
        MOV       AL,*-SP[6]            ; [CPU_] |367| 
        ANDB      AL,#0xff              ; [CPU_] |367| 
        ADD       *+XAR4[0],AL          ; [CPU_] |367| 
        B         $C$L52,UNC            ; [CPU_] |367| 
        ; branch occurs ; [] |367| 
$C$L51:    
	.dwpsn	file "../recorder.c",line 369,column 7,is_stmt
        MOVZ      AR0,*-SP[7]           ; [CPU_] |369| 
        MOV       ACC,*-SP[6] << #8     ; [CPU_] |369| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |369| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |369| 
$C$L52:    
	.dwpsn	file "../recorder.c",line 370,column 5,is_stmt
        MOVB      ACC,#8                ; [CPU_] |370| 
        ADDL      @_BitIndex,ACC        ; [CPU_] |370| 
	.dwpsn	file "../recorder.c",line 371,column 3,is_stmt
        B         $C$L59,UNC            ; [CPU_] |371| 
        ; branch occurs ; [] |371| 
$C$L53:    
	.dwpsn	file "../recorder.c",line 372,column 8,is_stmt
        CMPB      AL,#16                ; [CPU_] |372| 
        B         $C$L56,HI             ; [CPU_] |372| 
        ; branchcc occurs ; [] |372| 
	.dwpsn	file "../recorder.c",line 373,column 5,is_stmt
        TBIT      @_BitIndex,#3         ; [CPU_] |373| 
        BF        $C$L54,NTC            ; [CPU_] |373| 
        ; branchcc occurs ; [] |373| 
	.dwpsn	file "../recorder.c",line 374,column 7,is_stmt
        MOVZ      AR6,*-SP[7]           ; [CPU_] |374| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |374| 
        MOVL      ACC,XAR4              ; [CPU_] |374| 
        ADDU      ACC,AR6               ; [CPU_] |374| 
        MOVL      XAR4,ACC              ; [CPU_] |374| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |374| 
        SFR       ACC,8                 ; [CPU_] |374| 
        ADD       *+XAR4[0],AL          ; [CPU_] |374| 
	.dwpsn	file "../recorder.c",line 375,column 7,is_stmt
        MOVZ      AR0,*-SP[7]           ; [CPU_] |375| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |375| 
        MOV       ACC,*-SP[6] << #8     ; [CPU_] |375| 
        ADDB      XAR0,#1               ; [CPU_] |375| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |375| 
	.dwpsn	file "../recorder.c",line 376,column 5,is_stmt
        B         $C$L55,UNC            ; [CPU_] |376| 
        ; branch occurs ; [] |376| 
$C$L54:    
	.dwpsn	file "../recorder.c",line 378,column 7,is_stmt
        MOVZ      AR0,*-SP[7]           ; [CPU_] |378| 
        MOV       AL,*-SP[6]            ; [CPU_] |378| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |378| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |378| 
$C$L55:    
	.dwpsn	file "../recorder.c",line 379,column 5,is_stmt
        MOVB      ACC,#16               ; [CPU_] |379| 
        ADDL      @_BitIndex,ACC        ; [CPU_] |379| 
	.dwpsn	file "../recorder.c",line 380,column 3,is_stmt
        B         $C$L59,UNC            ; [CPU_] |380| 
        ; branch occurs ; [] |380| 
$C$L56:    
	.dwpsn	file "../recorder.c",line 382,column 5,is_stmt
        TBIT      @_BitIndex,#3         ; [CPU_] |382| 
        BF        $C$L57,NTC            ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
	.dwpsn	file "../recorder.c",line 383,column 7,is_stmt
        MOVZ      AR6,*-SP[7]           ; [CPU_] |383| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |383| 
        MOV       T,#24                 ; [CPU_] |383| 
        MOVL      ACC,XAR4              ; [CPU_] |383| 
        ADDU      ACC,AR6               ; [CPU_] |383| 
        MOVL      XAR4,ACC              ; [CPU_] |383| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |383| 
        LSRL      ACC,T                 ; [CPU_] |383| 
        ADD       *+XAR4[0],AL          ; [CPU_] |383| 
	.dwpsn	file "../recorder.c",line 384,column 7,is_stmt
        MOVZ      AR0,*-SP[7]           ; [CPU_] |384| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |384| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |384| 
        SFR       ACC,8                 ; [CPU_] |384| 
        ADDB      XAR0,#1               ; [CPU_] |384| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |384| 
	.dwpsn	file "../recorder.c",line 385,column 7,is_stmt
        MOVZ      AR0,*-SP[7]           ; [CPU_] |385| 
        MOV       ACC,*-SP[6] << #8     ; [CPU_] |385| 
        ADDB      XAR0,#2               ; [CPU_] |385| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |385| 
	.dwpsn	file "../recorder.c",line 386,column 5,is_stmt
        B         $C$L58,UNC            ; [CPU_] |386| 
        ; branch occurs ; [] |386| 
$C$L57:    
	.dwpsn	file "../recorder.c",line 388,column 7,is_stmt
        MOVZ      AR0,*-SP[7]           ; [CPU_] |388| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |388| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |388| 
        MOVH      *+XAR4[AR0],ACC << 0  ; [CPU_] |388| 
	.dwpsn	file "../recorder.c",line 389,column 7,is_stmt
        MOVZ      AR0,*-SP[7]           ; [CPU_] |389| 
        MOV       AL,*-SP[6]            ; [CPU_] |389| 
        ADDB      XAR0,#1               ; [CPU_] |389| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |389| 
$C$L58:    
	.dwpsn	file "../recorder.c",line 391,column 5,is_stmt
        MOVB      ACC,#32               ; [CPU_] |391| 
        ADDL      @_BitIndex,ACC        ; [CPU_] |391| 
$C$L59:    
	.dwpsn	file "../recorder.c",line 393,column 3,is_stmt
        MOVL      XAR4,#65536           ; [CPU_U] |393| 
        MOVL      ACC,XAR4              ; [CPU_] |393| 
        CMPL      ACC,@_BitIndex        ; [CPU_] |393| 
        B         $C$L60,HI             ; [CPU_] |393| 
        ; branchcc occurs ; [] |393| 
	.dwpsn	file "../recorder.c",line 393,column 34,is_stmt
        SUBL      @_BitIndex,ACC        ; [CPU_] |393| 
	.dwpsn	file "../recorder.c",line 394,column 1,is_stmt
$C$L60:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$105, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$105, DW_AT_TI_end_line(0x18a)
	.dwattr $C$DW$105, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$105

	.sect	".text"
	.global	_AddByte

$C$DW$113	.dwtag  DW_TAG_subprogram, DW_AT_name("AddByte")
	.dwattr $C$DW$113, DW_AT_low_pc(_AddByte)
	.dwattr $C$DW$113, DW_AT_high_pc(0x00)
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_AddByte")
	.dwattr $C$DW$113, DW_AT_external
	.dwattr $C$DW$113, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$113, DW_AT_TI_begin_line(0x190)
	.dwattr $C$DW$113, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$113, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../recorder.c",line 401,column 1,is_stmt,address _AddByte

	.dwfde $C$DW$CIE, _AddByte
$C$DW$114	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _AddByte                      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_AddByte:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$115	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |401| 
	.dwpsn	file "../recorder.c",line 402,column 3,is_stmt
        AND       *-SP[1],#0x00ff       ; [CPU_] |402| 
	.dwpsn	file "../recorder.c",line 403,column 3,is_stmt
        MOVW      DP,#_DataIndex        ; [CPU_U] 
        TBIT      @_DataIndex,#0        ; [CPU_] |403| 
        BF        $C$L61,NTC            ; [CPU_] |403| 
        ; branchcc occurs ; [] |403| 
	.dwpsn	file "../recorder.c",line 404,column 5,is_stmt
        MOV       AL,@_DataIndex        ; [CPU_] |404| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |404| 
        LSR       AL,1                  ; [CPU_] |404| 
        MOVZ      AR6,AL                ; [CPU_] |404| 
        MOVL      ACC,XAR4              ; [CPU_] |404| 
        ADDU      ACC,AR6               ; [CPU_] |404| 
        MOVL      XAR4,ACC              ; [CPU_] |404| 
        MOV       ACC,*-SP[1] << #8     ; [CPU_] |404| 
        ADD       *+XAR4[0],AL          ; [CPU_] |404| 
        B         $C$L62,UNC            ; [CPU_] |404| 
        ; branch occurs ; [] |404| 
$C$L61:    
	.dwpsn	file "../recorder.c",line 406,column 5,is_stmt
        MOV       AL,@_DataIndex        ; [CPU_] |406| 
        MOV       AH,*-SP[1]            ; [CPU_] |406| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |406| 
        LSR       AL,1                  ; [CPU_] |406| 
        MOVZ      AR0,AL                ; [CPU_] |406| 
        MOV       *+XAR4[AR0],AH        ; [CPU_] |406| 
$C$L62:    
	.dwpsn	file "../recorder.c",line 407,column 3,is_stmt
        INC       @_DataIndex           ; [CPU_] |407| 
	.dwpsn	file "../recorder.c",line 408,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$113, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$113, DW_AT_TI_end_line(0x198)
	.dwattr $C$DW$113, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$113

	.sect	".text"
	.global	_AddWord

$C$DW$117	.dwtag  DW_TAG_subprogram, DW_AT_name("AddWord")
	.dwattr $C$DW$117, DW_AT_low_pc(_AddWord)
	.dwattr $C$DW$117, DW_AT_high_pc(0x00)
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_AddWord")
	.dwattr $C$DW$117, DW_AT_external
	.dwattr $C$DW$117, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$117, DW_AT_TI_begin_line(0x19a)
	.dwattr $C$DW$117, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$117, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../recorder.c",line 411,column 1,is_stmt,address _AddWord

	.dwfde $C$DW$CIE, _AddWord
$C$DW$118	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _AddWord                      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_AddWord:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$119	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$119, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |411| 
	.dwpsn	file "../recorder.c",line 412,column 3,is_stmt
        MOVW      DP,#_DataIndex        ; [CPU_U] 
        CMP       @_DataIndex,#8192     ; [CPU_] |412| 
        B         $C$L64,HIS            ; [CPU_] |412| 
        ; branchcc occurs ; [] |412| 
	.dwpsn	file "../recorder.c",line 413,column 5,is_stmt
        TBIT      @_DataIndex,#0        ; [CPU_] |413| 
        BF        $C$L63,NTC            ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
	.dwpsn	file "../recorder.c",line 415,column 7,is_stmt
        MOV       AL,@_DataIndex        ; [CPU_] |415| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |415| 
        LSR       AL,1                  ; [CPU_] |415| 
        MOVZ      AR6,AL                ; [CPU_] |415| 
        MOVL      ACC,XAR4              ; [CPU_] |415| 
        ADDU      ACC,AR6               ; [CPU_] |415| 
        MOVL      XAR4,ACC              ; [CPU_] |415| 
        MOV       ACC,*-SP[1] << #8     ; [CPU_] |415| 
        ADD       *+XAR4[0],AL          ; [CPU_] |415| 
	.dwpsn	file "../recorder.c",line 416,column 7,is_stmt
        INC       @_DataIndex           ; [CPU_] |416| 
	.dwpsn	file "../recorder.c",line 417,column 4,is_stmt
        MOV       AL,@_DataIndex        ; [CPU_] |417| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |417| 
        LSR       AL,1                  ; [CPU_] |417| 
        MOVZ      AR0,AL                ; [CPU_] |417| 
        MOV       AL,*-SP[1]            ; [CPU_] |417| 
        LSR       AL,8                  ; [CPU_] |417| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |417| 
	.dwpsn	file "../recorder.c",line 418,column 4,is_stmt
        INC       @_DataIndex           ; [CPU_] |418| 
	.dwpsn	file "../recorder.c",line 419,column 5,is_stmt
        B         $C$L64,UNC            ; [CPU_] |419| 
        ; branch occurs ; [] |419| 
$C$L63:    
	.dwpsn	file "../recorder.c",line 422,column 7,is_stmt
        MOV       AL,@_DataIndex        ; [CPU_] |422| 
        MOV       AH,*-SP[1]            ; [CPU_] |422| 
        MOVL      XAR4,#_ODV_RecorderData1 ; [CPU_U] |422| 
        LSR       AL,1                  ; [CPU_] |422| 
        MOVZ      AR0,AL                ; [CPU_] |422| 
        MOV       *+XAR4[AR0],AH        ; [CPU_] |422| 
	.dwpsn	file "../recorder.c",line 423,column 7,is_stmt
        ADD       @_DataIndex,#2        ; [CPU_] |423| 
	.dwpsn	file "../recorder.c",line 426,column 1,is_stmt
$C$L64:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$117, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$117, DW_AT_TI_end_line(0x1aa)
	.dwattr $C$DW$117, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$117

	.sect	".text"
	.global	_AddLWord

$C$DW$121	.dwtag  DW_TAG_subprogram, DW_AT_name("AddLWord")
	.dwattr $C$DW$121, DW_AT_low_pc(_AddLWord)
	.dwattr $C$DW$121, DW_AT_high_pc(0x00)
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_AddLWord")
	.dwattr $C$DW$121, DW_AT_external
	.dwattr $C$DW$121, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$121, DW_AT_TI_begin_line(0x1ac)
	.dwattr $C$DW$121, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$121, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../recorder.c",line 429,column 1,is_stmt,address _AddLWord

	.dwfde $C$DW$CIE, _AddLWord
$C$DW$122	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _AddLWord                     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_AddLWord:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$123, DW_AT_location[DW_OP_breg20 -2]
$C$DW$124	.dwtag  DW_TAG_variable, DW_AT_name("wdata")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_wdata")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$124, DW_AT_location[DW_OP_breg20 -3]
        MOVL      *-SP[2],ACC           ; [CPU_] |429| 
	.dwpsn	file "../recorder.c",line 431,column 3,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |431| 
        MOV       *-SP[3],AL            ; [CPU_] |431| 
	.dwpsn	file "../recorder.c",line 432,column 3,is_stmt
$C$DW$125	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$125, DW_AT_low_pc(0x00)
	.dwattr $C$DW$125, DW_AT_name("_AddWord")
	.dwattr $C$DW$125, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |432| 
        ; call occurs [#_AddWord] ; [] |432| 
	.dwpsn	file "../recorder.c",line 433,column 3,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |433| 
        MOVU      ACC,AH                ; [CPU_] |433| 
        MOV       *-SP[3],AL            ; [CPU_] |433| 
	.dwpsn	file "../recorder.c",line 434,column 3,is_stmt
$C$DW$126	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$126, DW_AT_low_pc(0x00)
	.dwattr $C$DW$126, DW_AT_name("_AddWord")
	.dwattr $C$DW$126, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |434| 
        ; call occurs [#_AddWord] ; [] |434| 
	.dwpsn	file "../recorder.c",line 435,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$127	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$127, DW_AT_low_pc(0x00)
	.dwattr $C$DW$127, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$121, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$121, DW_AT_TI_end_line(0x1b3)
	.dwattr $C$DW$121, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$121

	.sect	".text"
	.global	_AddFloat

$C$DW$128	.dwtag  DW_TAG_subprogram, DW_AT_name("AddFloat")
	.dwattr $C$DW$128, DW_AT_low_pc(_AddFloat)
	.dwattr $C$DW$128, DW_AT_high_pc(0x00)
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_AddFloat")
	.dwattr $C$DW$128, DW_AT_external
	.dwattr $C$DW$128, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$128, DW_AT_TI_begin_line(0x1b6)
	.dwattr $C$DW$128, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$128, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../recorder.c",line 439,column 1,is_stmt,address _AddFloat

	.dwfde $C$DW$CIE, _AddFloat
$C$DW$129	.dwtag  DW_TAG_formal_parameter, DW_AT_name("fdata")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_fdata")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x2b]

;***************************************************************
;* FNAME: _AddFloat                     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_AddFloat:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$130	.dwtag  DW_TAG_variable, DW_AT_name("fdata")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_fdata")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$130, DW_AT_location[DW_OP_breg20 -2]
$C$DW$131	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$131, DW_AT_location[DW_OP_breg20 -4]
        MOV32     *-SP[2],R0H           ; [CPU_] |439| 
	.dwpsn	file "../recorder.c",line 441,column 3,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |441| 
        MOVL      *-SP[4],ACC           ; [CPU_] |441| 
	.dwpsn	file "../recorder.c",line 442,column 3,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |442| 
$C$DW$132	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$132, DW_AT_low_pc(0x00)
	.dwattr $C$DW$132, DW_AT_name("_AddLWord")
	.dwattr $C$DW$132, DW_AT_TI_call
        LCR       #_AddLWord            ; [CPU_] |442| 
        ; call occurs [#_AddLWord] ; [] |442| 
	.dwpsn	file "../recorder.c",line 443,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$133	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$133, DW_AT_low_pc(0x00)
	.dwattr $C$DW$133, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$128, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$128, DW_AT_TI_end_line(0x1bb)
	.dwattr $C$DW$128, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$128

	.sect	".text"
	.global	_AddString

$C$DW$134	.dwtag  DW_TAG_subprogram, DW_AT_name("AddString")
	.dwattr $C$DW$134, DW_AT_low_pc(_AddString)
	.dwattr $C$DW$134, DW_AT_high_pc(0x00)
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_AddString")
	.dwattr $C$DW$134, DW_AT_external
	.dwattr $C$DW$134, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$134, DW_AT_TI_begin_line(0x1bd)
	.dwattr $C$DW$134, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$134, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../recorder.c",line 447,column 1,is_stmt,address _AddString

	.dwfde $C$DW$CIE, _AddString
$C$DW$135	.dwtag  DW_TAG_formal_parameter, DW_AT_name("mystr")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_mystr")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _AddString                    FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_AddString:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("mystr")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_mystr")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_breg20 -2]
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("ch")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_ch")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_breg20 -3]
        MOVL      *-SP[2],XAR4          ; [CPU_] |447| 
$C$L65:    
	.dwpsn	file "../recorder.c",line 451,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |451| 
        MOV       AL,*XAR4++            ; [CPU_] |451| 
        MOV       *-SP[3],AL            ; [CPU_] |451| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |451| 
	.dwpsn	file "../recorder.c",line 452,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |452| 
$C$DW$138	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$138, DW_AT_low_pc(0x00)
	.dwattr $C$DW$138, DW_AT_name("_AddByte")
	.dwattr $C$DW$138, DW_AT_TI_call
        LCR       #_AddByte             ; [CPU_] |452| 
        ; call occurs [#_AddByte] ; [] |452| 
	.dwpsn	file "../recorder.c",line 453,column 12,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |453| 
        BF        $C$L66,EQ             ; [CPU_] |453| 
        ; branchcc occurs ; [] |453| 
        CMP       @_DataIndex,#8192     ; [CPU_] |453| 
        B         $C$L65,LO             ; [CPU_] |453| 
        ; branchcc occurs ; [] |453| 
	.dwpsn	file "../recorder.c",line 454,column 1,is_stmt
$C$L66:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$139	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$139, DW_AT_low_pc(0x00)
	.dwattr $C$DW$139, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$134, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$134, DW_AT_TI_end_line(0x1c6)
	.dwattr $C$DW$134, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$134

	.sect	".text"
	.global	_PAR_AddVariables

$C$DW$140	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddVariables")
	.dwattr $C$DW$140, DW_AT_low_pc(_PAR_AddVariables)
	.dwattr $C$DW$140, DW_AT_high_pc(0x00)
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_PAR_AddVariables")
	.dwattr $C$DW$140, DW_AT_external
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$140, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$140, DW_AT_TI_begin_line(0x1c8)
	.dwattr $C$DW$140, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$140, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../recorder.c",line 457,column 1,is_stmt,address _PAR_AddVariables

	.dwfde $C$DW$CIE, _PAR_AddVariables
$C$DW$141	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PAR_AddVariables             FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_PAR_AddVariables:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$142, DW_AT_location[DW_OP_breg20 -1]
$C$DW$143	.dwtag  DW_TAG_variable, DW_AT_name("nb1")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_nb1")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_breg20 -2]
$C$DW$144	.dwtag  DW_TAG_variable, DW_AT_name("nb2")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_nb2")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$144, DW_AT_location[DW_OP_breg20 -3]
$C$DW$145	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$145, DW_AT_location[DW_OP_breg20 -4]
$C$DW$146	.dwtag  DW_TAG_variable, DW_AT_name("Var1")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_Var1")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$146, DW_AT_location[DW_OP_breg20 -6]
$C$DW$147	.dwtag  DW_TAG_variable, DW_AT_name("uservar")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_uservar")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$147, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[1],AL            ; [CPU_] |457| 
	.dwpsn	file "../recorder.c",line 461,column 3,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |461| 
	.dwpsn	file "../recorder.c",line 462,column 3,is_stmt
        MOVB      *-SP[3],#35,UNC       ; [CPU_] |462| 
	.dwpsn	file "../recorder.c",line 463,column 3,is_stmt
        MOVW      DP,#_DataIndex        ; [CPU_U] 
        MOV       @_DataIndex,#0        ; [CPU_] |463| 
	.dwpsn	file "../recorder.c",line 464,column 3,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |464| 
	.dwpsn	file "../recorder.c",line 465,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |465| 
        BF        $C$L71,NEQ            ; [CPU_] |465| 
        ; branchcc occurs ; [] |465| 
$C$L67:    
	.dwpsn	file "../recorder.c",line 468,column 7,is_stmt
        MOV       T,#10                 ; [CPU_] |468| 
        MOVL      XAR4,#_Variables      ; [CPU_U] |468| 
        MPYXU     ACC,T,*-SP[4]         ; [CPU_] |468| 
        ADDL      XAR4,ACC              ; [CPU_] |468| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |468| 
	.dwpsn	file "../recorder.c",line 469,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |469| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |469| 
$C$DW$148	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$148, DW_AT_low_pc(0x00)
	.dwattr $C$DW$148, DW_AT_name("_AddWord")
	.dwattr $C$DW$148, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |469| 
        ; call occurs [#_AddWord] ; [] |469| 
	.dwpsn	file "../recorder.c",line 470,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |470| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |470| 
$C$DW$149	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$149, DW_AT_low_pc(0x00)
	.dwattr $C$DW$149, DW_AT_name("_AddWord")
	.dwattr $C$DW$149, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |470| 
        ; call occurs [#_AddWord] ; [] |470| 
	.dwpsn	file "../recorder.c",line 471,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |471| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |471| 
$C$DW$150	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$150, DW_AT_low_pc(0x00)
	.dwattr $C$DW$150, DW_AT_name("_AddString")
	.dwattr $C$DW$150, DW_AT_TI_call
        LCR       #_AddString           ; [CPU_] |471| 
        ; call occurs [#_AddString] ; [] |471| 
	.dwpsn	file "../recorder.c",line 472,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |472| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |472| 
$C$DW$151	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$151, DW_AT_low_pc(0x00)
	.dwattr $C$DW$151, DW_AT_name("_AddWord")
	.dwattr $C$DW$151, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |472| 
        ; call occurs [#_AddWord] ; [] |472| 
	.dwpsn	file "../recorder.c",line 473,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |473| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |473| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |473| 
$C$DW$152	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$152, DW_AT_low_pc(0x00)
	.dwattr $C$DW$152, DW_AT_name("_AddFloat")
	.dwattr $C$DW$152, DW_AT_TI_call
        LCR       #_AddFloat            ; [CPU_] |473| 
        ; call occurs [#_AddFloat] ; [] |473| 
	.dwpsn	file "../recorder.c",line 474,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |474| 
        MOVB      XAR0,#8               ; [CPU_] |474| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |474| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |474| 
$C$DW$153	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$153, DW_AT_low_pc(0x00)
	.dwattr $C$DW$153, DW_AT_name("_AddFloat")
	.dwattr $C$DW$153, DW_AT_TI_call
        LCR       #_AddFloat            ; [CPU_] |474| 
        ; call occurs [#_AddFloat] ; [] |474| 
	.dwpsn	file "../recorder.c",line 475,column 7,is_stmt
        INC       *-SP[4]               ; [CPU_] |475| 
	.dwpsn	file "../recorder.c",line 476,column 12,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |476| 
        CMP       AL,*-SP[4]            ; [CPU_] |476| 
        B         $C$L67,HI             ; [CPU_] |476| 
        ; branchcc occurs ; [] |476| 
	.dwpsn	file "../recorder.c",line 477,column 5,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |477| 
        BF        $C$L71,EQ             ; [CPU_] |477| 
        ; branchcc occurs ; [] |477| 
	.dwpsn	file "../recorder.c",line 478,column 7,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |478| 
$C$L68:    
	.dwpsn	file "../recorder.c",line 480,column 9,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |480| 
        MPYXU     P,T,*-SP[4]           ; [CPU_] |480| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |480| 
        ADDL      ACC,P                 ; [CPU_] |480| 
        MOVL      *-SP[6],ACC           ; [CPU_] |480| 
	.dwpsn	file "../recorder.c",line 481,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |481| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |481| 
$C$DW$154	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$154, DW_AT_low_pc(0x00)
	.dwattr $C$DW$154, DW_AT_name("_AddWord")
	.dwattr $C$DW$154, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |481| 
        ; call occurs [#_AddWord] ; [] |481| 
	.dwpsn	file "../recorder.c",line 482,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |482| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |482| 
$C$DW$155	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$155, DW_AT_low_pc(0x00)
	.dwattr $C$DW$155, DW_AT_name("_AddWord")
	.dwattr $C$DW$155, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |482| 
        ; call occurs [#_AddWord] ; [] |482| 
	.dwpsn	file "../recorder.c",line 483,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |483| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |483| 
$C$DW$156	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$156, DW_AT_low_pc(0x00)
	.dwattr $C$DW$156, DW_AT_name("_AddString")
	.dwattr $C$DW$156, DW_AT_TI_call
        LCR       #_AddString           ; [CPU_] |483| 
        ; call occurs [#_AddString] ; [] |483| 
	.dwpsn	file "../recorder.c",line 484,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |484| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |484| 
        CMPB      AL,#100               ; [CPU_] |484| 
        B         $C$L69,HIS            ; [CPU_] |484| 
        ; branchcc occurs ; [] |484| 
	.dwpsn	file "../recorder.c",line 485,column 11,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |485| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |485| 
        ADD       AL,#256               ; [CPU_] |485| 
$C$DW$157	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$157, DW_AT_low_pc(0x00)
	.dwattr $C$DW$157, DW_AT_name("_AddWord")
	.dwattr $C$DW$157, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |485| 
        ; call occurs [#_AddWord] ; [] |485| 
        B         $C$L70,UNC            ; [CPU_] |485| 
        ; branch occurs ; [] |485| 
$C$L69:    
	.dwpsn	file "../recorder.c",line 487,column 11,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |487| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |487| 
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("_AddWord")
	.dwattr $C$DW$158, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |487| 
        ; call occurs [#_AddWord] ; [] |487| 
$C$L70:    
	.dwpsn	file "../recorder.c",line 488,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |488| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |488| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |488| 
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_name("_AddFloat")
	.dwattr $C$DW$159, DW_AT_TI_call
        LCR       #_AddFloat            ; [CPU_] |488| 
        ; call occurs [#_AddFloat] ; [] |488| 
	.dwpsn	file "../recorder.c",line 489,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |489| 
        MOVB      XAR0,#8               ; [CPU_] |489| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |489| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |489| 
$C$DW$160	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$160, DW_AT_low_pc(0x00)
	.dwattr $C$DW$160, DW_AT_name("_AddFloat")
	.dwattr $C$DW$160, DW_AT_TI_call
        LCR       #_AddFloat            ; [CPU_] |489| 
        ; call occurs [#_AddFloat] ; [] |489| 
	.dwpsn	file "../recorder.c",line 490,column 9,is_stmt
        INC       *-SP[4]               ; [CPU_] |490| 
	.dwpsn	file "../recorder.c",line 491,column 14,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |491| 
        CMP       AL,*-SP[4]            ; [CPU_] |491| 
        B         $C$L68,HI             ; [CPU_] |491| 
        ; branchcc occurs ; [] |491| 
$C$L71:    
	.dwpsn	file "../recorder.c",line 494,column 3,is_stmt
        MOV       AL,@_DataIndex        ; [CPU_] |494| 
	.dwpsn	file "../recorder.c",line 495,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$140, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$140, DW_AT_TI_end_line(0x1ef)
	.dwattr $C$DW$140, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$140

	.sect	".text"
	.global	_PAR_AddMultiUnits

$C$DW$162	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddMultiUnits")
	.dwattr $C$DW$162, DW_AT_low_pc(_PAR_AddMultiUnits)
	.dwattr $C$DW$162, DW_AT_high_pc(0x00)
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$162, DW_AT_external
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$162, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$162, DW_AT_TI_begin_line(0x1f1)
	.dwattr $C$DW$162, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$162, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../recorder.c",line 498,column 1,is_stmt,address _PAR_AddMultiUnits

	.dwfde $C$DW$CIE, _PAR_AddMultiUnits
$C$DW$163	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$163, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PAR_AddMultiUnits            FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_PAR_AddMultiUnits:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$164	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$164, DW_AT_location[DW_OP_breg20 -1]
$C$DW$165	.dwtag  DW_TAG_variable, DW_AT_name("nb1")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_nb1")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_breg20 -2]
$C$DW$166	.dwtag  DW_TAG_variable, DW_AT_name("nb2")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_nb2")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$166, DW_AT_location[DW_OP_breg20 -3]
$C$DW$167	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$167, DW_AT_location[DW_OP_breg20 -4]
$C$DW$168	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$168, DW_AT_location[DW_OP_breg20 -5]
$C$DW$169	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$169, DW_AT_location[DW_OP_breg20 -6]
$C$DW$170	.dwtag  DW_TAG_variable, DW_AT_name("Unit")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_Unit")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_breg20 -8]
$C$DW$171	.dwtag  DW_TAG_variable, DW_AT_name("usermulti")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_usermulti")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_breg20 -10]
$C$DW$172	.dwtag  DW_TAG_variable, DW_AT_name("multiunit")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_multiunit")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_breg20 -12]
        MOV       *-SP[1],AL            ; [CPU_] |498| 
	.dwpsn	file "../recorder.c",line 504,column 3,is_stmt
        MOVW      DP,#_DataIndex        ; [CPU_U] 
        MOV       @_DataIndex,#0        ; [CPU_] |504| 
	.dwpsn	file "../recorder.c",line 505,column 3,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |505| 
	.dwpsn	file "../recorder.c",line 506,column 3,is_stmt
        MOVB      *-SP[3],#12,UNC       ; [CPU_] |506| 
	.dwpsn	file "../recorder.c",line 507,column 3,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |507| 
	.dwpsn	file "../recorder.c",line 508,column 3,is_stmt
        MOVL      XAR4,#_MultiUnitListe1 ; [CPU_U] |508| 
        MOVL      *-SP[12],XAR4         ; [CPU_] |508| 
	.dwpsn	file "../recorder.c",line 509,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |509| 
        BF        $C$L80,NEQ            ; [CPU_] |509| 
        ; branchcc occurs ; [] |509| 
$C$L72:    
	.dwpsn	file "../recorder.c",line 511,column 7,is_stmt
        MOVL      XAR7,*-SP[12]         ; [CPU_] |511| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |511| 
        LSL       ACC,2                 ; [CPU_] |511| 
        ADDL      XAR7,ACC              ; [CPU_] |511| 
        MOV       AL,*XAR7              ; [CPU_] |511| 
        MOV       *-SP[6],AL            ; [CPU_] |511| 
	.dwpsn	file "../recorder.c",line 512,column 7,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |512| 
$C$DW$173	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$173, DW_AT_low_pc(0x00)
	.dwattr $C$DW$173, DW_AT_name("_AddWord")
	.dwattr $C$DW$173, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |512| 
        ; call occurs [#_AddWord] ; [] |512| 
	.dwpsn	file "../recorder.c",line 513,column 7,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |513| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |513| 
        LSL       ACC,2                 ; [CPU_] |513| 
        ADDL      XAR4,ACC              ; [CPU_] |513| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |513| 
$C$DW$174	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$174, DW_AT_low_pc(0x00)
	.dwattr $C$DW$174, DW_AT_name("_AddWord")
	.dwattr $C$DW$174, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |513| 
        ; call occurs [#_AddWord] ; [] |513| 
	.dwpsn	file "../recorder.c",line 514,column 12,is_stmt
        MOV       T,#10                 ; [CPU_] |515| 
        MOV       *-SP[5],#0            ; [CPU_] |514| 
        B         $C$L74,UNC            ; [CPU_] |514| 
        ; branch occurs ; [] |514| 
$C$L73:    
	.dwpsn	file "../recorder.c",line 515,column 9,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |515| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |515| 
        MPYXU     P,T,*-SP[5]           ; [CPU_] |515| 
        LSL       ACC,2                 ; [CPU_] |515| 
        ADDL      XAR4,ACC              ; [CPU_] |515| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |515| 
        ADDL      ACC,P                 ; [CPU_] |515| 
        MOVL      *-SP[8],ACC           ; [CPU_] |515| 
	.dwpsn	file "../recorder.c",line 516,column 9,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |516| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |516| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |516| 
$C$DW$175	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$175, DW_AT_low_pc(0x00)
	.dwattr $C$DW$175, DW_AT_name("_AddFloat")
	.dwattr $C$DW$175, DW_AT_TI_call
        LCR       #_AddFloat            ; [CPU_] |516| 
        ; call occurs [#_AddFloat] ; [] |516| 
	.dwpsn	file "../recorder.c",line 517,column 9,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |517| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |517| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |517| 
$C$DW$176	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$176, DW_AT_low_pc(0x00)
	.dwattr $C$DW$176, DW_AT_name("_AddFloat")
	.dwattr $C$DW$176, DW_AT_TI_call
        LCR       #_AddFloat            ; [CPU_] |517| 
        ; call occurs [#_AddFloat] ; [] |517| 
	.dwpsn	file "../recorder.c",line 518,column 9,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |518| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |518| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |518| 
$C$DW$177	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$177, DW_AT_low_pc(0x00)
	.dwattr $C$DW$177, DW_AT_name("_AddLWord")
	.dwattr $C$DW$177, DW_AT_TI_call
        LCR       #_AddLWord            ; [CPU_] |518| 
        ; call occurs [#_AddLWord] ; [] |518| 
	.dwpsn	file "../recorder.c",line 519,column 9,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |519| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |519| 
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_name("_AddString")
	.dwattr $C$DW$178, DW_AT_TI_call
        LCR       #_AddString           ; [CPU_] |519| 
        ; call occurs [#_AddString] ; [] |519| 
	.dwpsn	file "../recorder.c",line 520,column 9,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |520| 
        MOVB      XAR0,#8               ; [CPU_] |520| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |520| 
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_name("_AddString")
	.dwattr $C$DW$179, DW_AT_TI_call
        LCR       #_AddString           ; [CPU_] |520| 
        ; call occurs [#_AddString] ; [] |520| 
	.dwpsn	file "../recorder.c",line 514,column 25,is_stmt
        INC       *-SP[5]               ; [CPU_] |514| 
$C$L74:    
	.dwpsn	file "../recorder.c",line 514,column 17,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |514| 
        CMP       AL,*-SP[5]            ; [CPU_] |514| 
        B         $C$L73,HI             ; [CPU_] |514| 
        ; branchcc occurs ; [] |514| 
	.dwpsn	file "../recorder.c",line 522,column 7,is_stmt
        INC       *-SP[4]               ; [CPU_] |522| 
	.dwpsn	file "../recorder.c",line 523,column 14,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |523| 
        CMP       AL,*-SP[4]            ; [CPU_] |523| 
        B         $C$L72,HI             ; [CPU_] |523| 
        ; branchcc occurs ; [] |523| 
	.dwpsn	file "../recorder.c",line 524,column 5,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |524| 
        BF        $C$L80,EQ             ; [CPU_] |524| 
        ; branchcc occurs ; [] |524| 
	.dwpsn	file "../recorder.c",line 525,column 7,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |525| 
	.dwpsn	file "../recorder.c",line 526,column 7,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |526| 
        MOV       T,#10                 ; [CPU_] |535| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |526| 
        MOVL      *-SP[12],ACC          ; [CPU_] |526| 
$C$L75:    
	.dwpsn	file "../recorder.c",line 528,column 9,is_stmt
        MOVL      XAR7,*-SP[12]         ; [CPU_] |528| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |528| 
        LSL       ACC,2                 ; [CPU_] |528| 
        ADDL      XAR7,ACC              ; [CPU_] |528| 
        MOV       AL,*XAR7              ; [CPU_] |528| 
        MOV       *-SP[6],AL            ; [CPU_] |528| 
	.dwpsn	file "../recorder.c",line 529,column 9,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |529| 
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("_AddWord")
	.dwattr $C$DW$180, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |529| 
        ; call occurs [#_AddWord] ; [] |529| 
	.dwpsn	file "../recorder.c",line 530,column 9,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |530| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |530| 
        LSL       ACC,2                 ; [CPU_] |530| 
        ADDL      XAR4,ACC              ; [CPU_] |530| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |530| 
        CMPB      AL,#100               ; [CPU_] |530| 
        B         $C$L76,HIS            ; [CPU_] |530| 
        ; branchcc occurs ; [] |530| 
	.dwpsn	file "../recorder.c",line 531,column 11,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |531| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |531| 
        LSL       ACC,2                 ; [CPU_] |531| 
        ADDL      XAR4,ACC              ; [CPU_] |531| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |531| 
        ADD       AL,#256               ; [CPU_] |531| 
$C$DW$181	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$181, DW_AT_low_pc(0x00)
	.dwattr $C$DW$181, DW_AT_name("_AddWord")
	.dwattr $C$DW$181, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |531| 
        ; call occurs [#_AddWord] ; [] |531| 
        B         $C$L77,UNC            ; [CPU_] |531| 
        ; branch occurs ; [] |531| 
$C$L76:    
	.dwpsn	file "../recorder.c",line 533,column 11,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |533| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |533| 
        LSL       ACC,2                 ; [CPU_] |533| 
        ADDL      XAR4,ACC              ; [CPU_] |533| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |533| 
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_name("_AddWord")
	.dwattr $C$DW$182, DW_AT_TI_call
        LCR       #_AddWord             ; [CPU_] |533| 
        ; call occurs [#_AddWord] ; [] |533| 
$C$L77:    
	.dwpsn	file "../recorder.c",line 534,column 14,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |534| 
        B         $C$L79,UNC            ; [CPU_] |534| 
        ; branch occurs ; [] |534| 
$C$L78:    
	.dwpsn	file "../recorder.c",line 535,column 11,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |535| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |535| 
        MPYXU     P,T,*-SP[5]           ; [CPU_] |535| 
        LSL       ACC,2                 ; [CPU_] |535| 
        ADDL      XAR4,ACC              ; [CPU_] |535| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |535| 
        ADDL      ACC,P                 ; [CPU_] |535| 
        MOVL      *-SP[8],ACC           ; [CPU_] |535| 
	.dwpsn	file "../recorder.c",line 536,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |536| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |536| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |536| 
$C$DW$183	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$183, DW_AT_low_pc(0x00)
	.dwattr $C$DW$183, DW_AT_name("_AddFloat")
	.dwattr $C$DW$183, DW_AT_TI_call
        LCR       #_AddFloat            ; [CPU_] |536| 
        ; call occurs [#_AddFloat] ; [] |536| 
	.dwpsn	file "../recorder.c",line 537,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |537| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |537| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |537| 
$C$DW$184	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$184, DW_AT_low_pc(0x00)
	.dwattr $C$DW$184, DW_AT_name("_AddFloat")
	.dwattr $C$DW$184, DW_AT_TI_call
        LCR       #_AddFloat            ; [CPU_] |537| 
        ; call occurs [#_AddFloat] ; [] |537| 
	.dwpsn	file "../recorder.c",line 538,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |538| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |538| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |538| 
$C$DW$185	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$185, DW_AT_low_pc(0x00)
	.dwattr $C$DW$185, DW_AT_name("_AddLWord")
	.dwattr $C$DW$185, DW_AT_TI_call
        LCR       #_AddLWord            ; [CPU_] |538| 
        ; call occurs [#_AddLWord] ; [] |538| 
	.dwpsn	file "../recorder.c",line 539,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |539| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |539| 
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("_AddString")
	.dwattr $C$DW$186, DW_AT_TI_call
        LCR       #_AddString           ; [CPU_] |539| 
        ; call occurs [#_AddString] ; [] |539| 
	.dwpsn	file "../recorder.c",line 540,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |540| 
        MOVB      XAR0,#8               ; [CPU_] |540| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |540| 
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_name("_AddString")
	.dwattr $C$DW$187, DW_AT_TI_call
        LCR       #_AddString           ; [CPU_] |540| 
        ; call occurs [#_AddString] ; [] |540| 
	.dwpsn	file "../recorder.c",line 534,column 27,is_stmt
        INC       *-SP[5]               ; [CPU_] |534| 
$C$L79:    
	.dwpsn	file "../recorder.c",line 534,column 19,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |534| 
        CMP       AL,*-SP[5]            ; [CPU_] |534| 
        B         $C$L78,HI             ; [CPU_] |534| 
        ; branchcc occurs ; [] |534| 
	.dwpsn	file "../recorder.c",line 542,column 9,is_stmt
        INC       *-SP[4]               ; [CPU_] |542| 
	.dwpsn	file "../recorder.c",line 543,column 16,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |543| 
        CMP       AL,*-SP[4]            ; [CPU_] |543| 
        B         $C$L75,HI             ; [CPU_] |543| 
        ; branchcc occurs ; [] |543| 
$C$L80:    
	.dwpsn	file "../recorder.c",line 546,column 3,is_stmt
        MOVW      DP,#_DataIndex        ; [CPU_U] 
        MOV       AL,@_DataIndex        ; [CPU_] |546| 
	.dwpsn	file "../recorder.c",line 547,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$162, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$162, DW_AT_TI_end_line(0x223)
	.dwattr $C$DW$162, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$162

	.sect	".text"
	.global	_GetVarSize

$C$DW$189	.dwtag  DW_TAG_subprogram, DW_AT_name("GetVarSize")
	.dwattr $C$DW$189, DW_AT_low_pc(_GetVarSize)
	.dwattr $C$DW$189, DW_AT_high_pc(0x00)
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_GetVarSize")
	.dwattr $C$DW$189, DW_AT_external
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$189, DW_AT_TI_begin_file("../recorder.c")
	.dwattr $C$DW$189, DW_AT_TI_begin_line(0x225)
	.dwattr $C$DW$189, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$189, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../recorder.c",line 550,column 1,is_stmt,address _GetVarSize

	.dwfde $C$DW$CIE, _GetVarSize

;***************************************************************
;* FNAME: _GetVarSize                   FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_GetVarSize:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../recorder.c",line 551,column 3,is_stmt
        MOVB      AL,#35                ; [CPU_] |551| 
	.dwpsn	file "../recorder.c",line 552,column 1,is_stmt
$C$DW$190	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$190, DW_AT_low_pc(0x00)
	.dwattr $C$DW$190, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$189, DW_AT_TI_end_file("../recorder.c")
	.dwattr $C$DW$189, DW_AT_TI_end_line(0x228)
	.dwattr $C$DW$189, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$189

;***************************************************************
;* FAR STRINGS                                                 *
;***************************************************************
	.sect	".econst:.string"
	.align	2
$C$FSL1:	.string	"Capacity",0
	.align	2
$C$FSL2:	.string	"[Ah]",0
	.align	2
$C$FSL3:	.string	"angle",0
	.align	2
$C$FSL4:	.string	"[rad]",0
	.align	2
$C$FSL5:	.string	"Power",0
	.align	2
$C$FSL6:	.string	"[W]",0
	.align	2
$C$FSL7:	.string	"No Unit",0
	.align	2
$C$FSL8:	.string	"[-]",0
	.align	2
$C$FSL9:	.string	"Temperature",0
	.align	2
$C$FSL10:	.string	"[",176,"C]",0
	.align	2
$C$FSL11:	.string	"AD",0
	.align	2
$C$FSL12:	.string	"[incAD]",0
	.align	2
$C$FSL13:	.string	"Current",0
	.align	2
$C$FSL14:	.string	"[mA]",0
	.align	2
$C$FSL15:	.string	"Voltage",0
	.align	2
$C$FSL16:	.string	"[V]",0
	.align	2
$C$FSL17:	.string	"[mV]",0
	.align	2
$C$FSL18:	.string	"current slope",0
	.align	2
$C$FSL19:	.string	"[mA/time]",0
	.align	2
$C$FSL20:	.string	"[A/time]",0
	.align	2
$C$FSL21:	.string	"[A]",0
	.align	2
$C$FSL22:	.string	"Position",0
	.align	2
$C$FSL23:	.string	"[qc]",0
	.align	2
$C$FSL24:	.string	"[Deg]",0
	.align	2
$C$FSL25:	.string	"Speed",0
	.align	2
$C$FSL26:	.string	"[qc/s]",0
	.align	2
$C$FSL27:	.string	"[rpm]",0
	.align	2
$C$FSL28:	.string	"Time",0
	.align	2
$C$FSL29:	.string	"[ms]",0
	.align	2
$C$FSL30:	.string	"[s]",0
	.align	2
$C$FSL31:	.string	"[us]",0
	.align	2
$C$FSL32:	.string	"NTC-2",0
	.align	2
$C$FSL33:	.string	"NTC-1",0
	.align	2
$C$FSL34:	.string	"NTC-3",0
	.align	2
$C$FSL35:	.string	"Mos Temp",0
	.align	2
$C$FSL36:	.string	"Res Temp",0
	.align	2
$C$FSL37:	.string	"M_Voltage",0
	.align	2
$C$FSL38:	.string	"M_Current",0
	.align	2
$C$FSL39:	.string	"M_Temperature",0
	.align	2
$C$FSL40:	.string	"M_Temperature2",0
	.align	2
$C$FSL41:	.string	"M_Power",0
	.align	2
$C$FSL42:	.string	"MinCell_Voltage",0
	.align	2
$C$FSL43:	.string	"MaxCell_Voltage",0
	.align	2
$C$FSL44:	.string	"DeltaCell_Voltage",0
	.align	2
$C$FSL45:	.string	"Used Capacity",0
	.align	2
$C$FSL46:	.string	"Relay current1",0
	.align	2
$C$FSL47:	.string	"12V",0
	.align	2
$C$FSL48:	.string	"Cell1",0
	.align	2
$C$FSL49:	.string	"Cell2",0
	.align	2
$C$FSL50:	.string	"Cell3",0
	.align	2
$C$FSL51:	.string	"Cell4",0
	.align	2
$C$FSL52:	.string	"Cell5",0
	.align	2
$C$FSL53:	.string	"Cell6",0
	.align	2
$C$FSL54:	.string	"Cell7",0
	.align	2
$C$FSL55:	.string	"Cell8",0
	.align	2
$C$FSL56:	.string	"Cell9",0
	.align	2
$C$FSL57:	.string	"Cell10",0
	.align	2
$C$FSL58:	.string	"Cell11",0
	.align	2
$C$FSL59:	.string	"Cell12",0
	.align	2
$C$FSL60:	.string	"Cell13",0
	.align	2
$C$FSL61:	.string	"Cell14",0
	.align	2
$C$FSL62:	.string	"G_Power",0
	.align	2
$C$FSL63:	.string	"G_State",0
	.align	2
$C$FSL64:	.string	"Digital in 1..16",0
	.align	2
$C$FSL65:	.string	"balancing 1..16",0
	.align	2
$C$FSL66:	.string	"OnTime",0
;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_ODV_Recorder_ReadIndex
	.global	_ODV_Recorder_Start
	.global	_ODV_Recorder_PreTrigger
	.global	_ODV_Recorder_TriggerIndex
	.global	_ODV_Recorder_Control
	.global	__getODentry
	.global	_ODV_SysTick_ms
	.global	_CNV_DegUnit
	.global	_BoardODdata
	.global	_ODV_Recorder_Period
	.global	_ODV_Recorder_TriggerLevel
	.global	_ODV_Recorder_NbOfSamples
	.global	_ODV_Recorder_Vectors
	.global	_ODP_Analogue_Input_Scaling_Float
	.global	_ODV_RecorderData1
	.global	LL$$AND
	.global	ULL$$CMP
	.global	LL$$CMP

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$191, DW_AT_name("cob_id")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$192, DW_AT_name("rtr")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$193	.dwtag  DW_TAG_member
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$193, DW_AT_name("len")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$193, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$193, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$194, DW_AT_name("data")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$111	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$111, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$195, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$196, DW_AT_name("csSDO")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$197	.dwtag  DW_TAG_member
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$197, DW_AT_name("csEmergency")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$197, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$197, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$198, DW_AT_name("csSYNC")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$198, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$199, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$200	.dwtag  DW_TAG_member
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$200, DW_AT_name("csPDO")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$200, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$201	.dwtag  DW_TAG_member
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$201, DW_AT_name("csLSS")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$201, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$201, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$202	.dwtag  DW_TAG_member
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$202, DW_AT_name("errCode")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$202, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$203	.dwtag  DW_TAG_member
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$203, DW_AT_name("errRegMask")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$203, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$203, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$204	.dwtag  DW_TAG_member
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$204, DW_AT_name("active")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$204, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$204, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)

$C$DW$T$103	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$103, DW_AT_byte_size(0x18)
$C$DW$205	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$205, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$103


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$206	.dwtag  DW_TAG_member
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$206, DW_AT_name("index")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$206, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$207	.dwtag  DW_TAG_member
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$207, DW_AT_name("subindex")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$207, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$208	.dwtag  DW_TAG_member
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$208, DW_AT_name("size")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$208, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$208, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$209	.dwtag  DW_TAG_member
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$209, DW_AT_name("address")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$209, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$108	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)
$C$DW$T$109	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$109, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$210	.dwtag  DW_TAG_member
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$210, DW_AT_name("Pos_Record")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_Pos_Record")
	.dwattr $C$DW$210, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$210, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$210, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$211	.dwtag  DW_TAG_member
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$211, DW_AT_name("Cur_Record")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_Cur_Record")
	.dwattr $C$DW$211, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$211, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$211, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$212	.dwtag  DW_TAG_member
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$212, DW_AT_name("AutoTrigg")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_AutoTrigg")
	.dwattr $C$DW$212, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$212, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$212, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$213	.dwtag  DW_TAG_member
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$213, DW_AT_name("AutoRecord")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_AutoRecord")
	.dwattr $C$DW$213, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$213, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$213, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$214	.dwtag  DW_TAG_member
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$214, DW_AT_name("ContinuousRecord")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_ContinuousRecord")
	.dwattr $C$DW$214, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$214, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$214, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$215	.dwtag  DW_TAG_member
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$215, DW_AT_name("Trig_Record")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_Trig_Record")
	.dwattr $C$DW$215, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$215, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$215, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$216	.dwtag  DW_TAG_member
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$216, DW_AT_name("unused")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_unused")
	.dwattr $C$DW$216, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0a)
	.dwattr $C$DW$216, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$216, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24

$C$DW$T$120	.dwtag  DW_TAG_typedef, DW_AT_name("TControl")
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$120, DW_AT_language(DW_LANG_C)

$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x0a)
$C$DW$217	.dwtag  DW_TAG_member
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$217, DW_AT_name("Gain")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_Gain")
	.dwattr $C$DW$217, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$217, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$218	.dwtag  DW_TAG_member
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$218, DW_AT_name("Offset")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_Offset")
	.dwattr $C$DW$218, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$218, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$219	.dwtag  DW_TAG_member
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$219, DW_AT_name("Modulo")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_Modulo")
	.dwattr $C$DW$219, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$219, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$220	.dwtag  DW_TAG_member
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$220, DW_AT_name("Name")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_Name")
	.dwattr $C$DW$220, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$220, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$221	.dwtag  DW_TAG_member
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$221, DW_AT_name("UnitName")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_UnitName")
	.dwattr $C$DW$221, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$221, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29

$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("T_Unit")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$222	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$30)
$C$DW$T$121	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$222)

$C$DW$T$122	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$122, DW_AT_byte_size(0x14)
$C$DW$223	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$223, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$122


$C$DW$T$124	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$124, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$124, DW_AT_byte_size(0x0a)
$C$DW$224	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$224, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$124


$C$DW$T$126	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$126, DW_AT_byte_size(0x1e)
$C$DW$225	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$225, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$126


$C$DW$T$31	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$226	.dwtag  DW_TAG_subrange_type
	.dwendtag $C$DW$T$31

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("T_MultiUnit")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$33	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_address_class(0x16)
$C$DW$T$128	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$128, DW_AT_address_class(0x16)

$C$DW$T$34	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x04)
$C$DW$227	.dwtag  DW_TAG_member
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$227, DW_AT_name("Size")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_Size")
	.dwattr $C$DW$227, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$227, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$228	.dwtag  DW_TAG_member
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$228, DW_AT_name("Key")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_Key")
	.dwattr $C$DW$228, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$228, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$229	.dwtag  DW_TAG_member
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$229, DW_AT_name("MultiUnit")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_MultiUnit")
	.dwattr $C$DW$229, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$229, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34

$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("T_MultiUnitType")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$230	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$41)
$C$DW$T$129	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$230)

$C$DW$T$130	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$T$130, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x30)
$C$DW$231	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$231, DW_AT_upper_bound(0x0b)
	.dwendtag $C$DW$T$130


$C$DW$T$42	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$232	.dwtag  DW_TAG_subrange_type
	.dwendtag $C$DW$T$42

$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("T_MultiUnitList")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$T$132	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$132, DW_AT_address_class(0x16)

$C$DW$T$35	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x0a)
$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$233, DW_AT_name("Index")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_Index")
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$234	.dwtag  DW_TAG_member
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$234, DW_AT_name("Subindex")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_Subindex")
	.dwattr $C$DW$234, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$235, DW_AT_name("Suffix")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_Suffix")
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$236	.dwtag  DW_TAG_member
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$236, DW_AT_name("MultiUnitTypeKey")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_MultiUnitTypeKey")
	.dwattr $C$DW$236, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$236, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$237, DW_AT_name("Gain")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_Gain")
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$238, DW_AT_name("Offset")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_Offset")
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35

$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("T_Variable")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$239	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$36)
$C$DW$T$133	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$239)

$C$DW$T$134	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$T$134, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$134, DW_AT_byte_size(0x15e)
$C$DW$240	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$240, DW_AT_upper_bound(0x22)
	.dwendtag $C$DW$T$134


$C$DW$T$37	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$241	.dwtag  DW_TAG_subrange_type
	.dwendtag $C$DW$T$37

$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("T_Variables")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x16)
$C$DW$T$136	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$136, DW_AT_address_class(0x16)

$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x04)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$242, DW_AT_name("Size")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_Size")
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$243, DW_AT_name("Var")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_Var")
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40

$C$DW$T$137	.dwtag  DW_TAG_typedef, DW_AT_name("T_UserVar")
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$137, DW_AT_language(DW_LANG_C)
$C$DW$T$138	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$138, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$T$138, DW_AT_address_class(0x16)

$C$DW$T$45	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$45, DW_AT_byte_size(0x04)
$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$244, DW_AT_name("Size")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_Size")
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$245, DW_AT_name("Multi")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_Multi")
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$45

$C$DW$T$139	.dwtag  DW_TAG_typedef, DW_AT_name("T_UserMulti")
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$139, DW_AT_language(DW_LANG_C)
$C$DW$T$140	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$T$140, DW_AT_address_class(0x16)

$C$DW$T$47	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$47, DW_AT_name("U_LongLong")
	.dwattr $C$DW$T$47, DW_AT_byte_size(0x04)
$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$246, DW_AT_name("datall")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_datall")
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$247, DW_AT_name("dataw")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_dataw")
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$47

$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)

$C$DW$T$72	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)
$C$DW$248	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$71)
	.dwendtag $C$DW$T$72

$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x16)
$C$DW$T$74	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$74, DW_AT_language(DW_LANG_C)
$C$DW$T$76	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$T$85	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
$C$DW$T$84	.dwtag  DW_TAG_typedef, DW_AT_name("post_sync_t")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$T$75	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$75, DW_AT_language(DW_LANG_C)
$C$DW$T$77	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$77, DW_AT_language(DW_LANG_C)

$C$DW$T$80	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
$C$DW$249	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$71)
$C$DW$250	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$80

$C$DW$T$81	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$T$81, DW_AT_address_class(0x16)
$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)
$C$DW$T$82	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)
$C$DW$T$86	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$86, DW_AT_language(DW_LANG_C)

$C$DW$T$104	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$251	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$71)
$C$DW$252	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$6)
$C$DW$253	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$9)
$C$DW$254	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$104

$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)
$C$DW$T$106	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$T$106, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$87	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$87, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$255	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$255, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$49	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x16)
$C$DW$256	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$6)
$C$DW$T$60	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$256)
$C$DW$T$61	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_address_class(0x16)

$C$DW$T$154	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$154, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$154, DW_AT_byte_size(0x07)
$C$DW$257	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$257, DW_AT_upper_bound(0x06)
	.dwendtag $C$DW$T$154

$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$78	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$78, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)

$C$DW$T$46	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x04)
$C$DW$258	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$258, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$46

$C$DW$259	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$9)
$C$DW$T$58	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$259)
$C$DW$T$59	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_address_class(0x16)
$C$DW$T$79	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$79, DW_AT_address_class(0x16)

$C$DW$T$158	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$158, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$158, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$158, DW_AT_byte_size(0x1000)
$C$DW$260	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$260, DW_AT_upper_bound(0xfff)
	.dwendtag $C$DW$T$158

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$159	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$159, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$159, DW_AT_address_class(0x16)

$C$DW$T$160	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$T$160, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$160, DW_AT_byte_size(0x0c)
$C$DW$261	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$261, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$160

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$162	.dwtag  DW_TAG_typedef, DW_AT_name("Uint32")
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$162, DW_AT_language(DW_LANG_C)
$C$DW$T$26	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$26, DW_AT_address_class(0x16)

$C$DW$T$62	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$62, DW_AT_language(DW_LANG_C)
$C$DW$262	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$6)
$C$DW$263	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$62

$C$DW$T$63	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_address_class(0x16)
$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$88	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$88, DW_AT_language(DW_LANG_C)
$C$DW$264	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$71)
$C$DW$265	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$52)
$C$DW$266	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$6)
$C$DW$267	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$88

$C$DW$T$89	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_address_class(0x16)
$C$DW$T$90	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$268	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$90)
$C$DW$T$91	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$268)
$C$DW$T$92	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_address_class(0x16)
$C$DW$T$93	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_address_class(0x16)

$C$DW$T$97	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)
$C$DW$269	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$71)
$C$DW$270	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$9)
$C$DW$271	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$97

$C$DW$T$98	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_address_class(0x16)
$C$DW$T$99	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
$C$DW$272	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$13)
$C$DW$T$165	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$165, DW_AT_type(*$C$DW$272)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)

$C$DW$T$167	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$167, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$167, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$167, DW_AT_byte_size(0x18)
$C$DW$273	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$273, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$167

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$25	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$25, DW_AT_address_class(0x16)

$C$DW$T$169	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$169, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$169, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$169, DW_AT_byte_size(0x2e)
$C$DW$274	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$274, DW_AT_upper_bound(0x16)
	.dwendtag $C$DW$T$169

$C$DW$275	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$16)
$C$DW$T$170	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$170, DW_AT_type(*$C$DW$275)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$28	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x16)

$C$DW$T$100	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$100, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$100, DW_AT_byte_size(0x01)
$C$DW$276	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$277	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$100

$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)

$C$DW$T$67	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$67, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x01)
$C$DW$278	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$279	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$280	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$281	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$282	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$283	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$284	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$285	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$67

$C$DW$T$68	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$68, DW_AT_language(DW_LANG_C)

$C$DW$T$83	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$83, DW_AT_byte_size(0x80)
$C$DW$286	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$286, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$83


$C$DW$T$48	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$48, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x06)
$C$DW$287	.dwtag  DW_TAG_member
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$287, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$287, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$287, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$288	.dwtag  DW_TAG_member
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$288, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$288, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$288, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$289, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$290, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$291	.dwtag  DW_TAG_member
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$291, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$291, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$292	.dwtag  DW_TAG_member
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$292, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$292, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$292, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$48

$C$DW$T$55	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)
$C$DW$293	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$55)
$C$DW$T$56	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$293)
$C$DW$T$57	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$57, DW_AT_address_class(0x16)

$C$DW$T$110	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$110, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x132)
$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$294, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$295	.dwtag  DW_TAG_member
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$295, DW_AT_name("objdict")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$295, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$295, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$296	.dwtag  DW_TAG_member
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$296, DW_AT_name("PDO_status")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$296, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$296, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$297, DW_AT_name("firstIndex")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$297, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$298	.dwtag  DW_TAG_member
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$298, DW_AT_name("lastIndex")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$298, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$299	.dwtag  DW_TAG_member
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$299, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$299, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$299, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$300	.dwtag  DW_TAG_member
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$300, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$300, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$301	.dwtag  DW_TAG_member
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$301, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$301, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$301, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$302	.dwtag  DW_TAG_member
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$302, DW_AT_name("transfers")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$302, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$302, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$303	.dwtag  DW_TAG_member
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$303, DW_AT_name("nodeState")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$303, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$303, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$304, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$304, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$305, DW_AT_name("initialisation")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$305, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$306, DW_AT_name("preOperational")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$307, DW_AT_name("operational")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$308, DW_AT_name("stopped")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$309, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$310, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$311, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$312, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$312, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$313, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$314, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$315, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$316, DW_AT_name("heartbeatError")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$317, DW_AT_name("NMTable")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$318, DW_AT_name("syncTimer")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$319, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$320, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$321, DW_AT_name("post_sync")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_post_sync")
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$322	.dwtag  DW_TAG_member
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$322, DW_AT_name("post_TPDO")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$322, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$323, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$324, DW_AT_name("toggle")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$324, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$325, DW_AT_name("canHandle")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$326, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$327, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$328, DW_AT_name("globalCallback")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$329, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$330, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$331, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$332, DW_AT_name("dcf_request")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$333, DW_AT_name("error_state")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$334, DW_AT_name("error_history_size")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$335, DW_AT_name("error_number")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$336, DW_AT_name("error_first_element")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$337, DW_AT_name("error_register")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$338, DW_AT_name("error_cobid")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$339, DW_AT_name("error_data")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$340, DW_AT_name("post_emcy")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$341, DW_AT_name("lss_transfer")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$342, DW_AT_name("eeprom_index")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$343, DW_AT_name("eeprom_size")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$110

$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
$C$DW$T$71	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$71, DW_AT_address_class(0x16)

$C$DW$T$112	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$112, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$112, DW_AT_byte_size(0x0e)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$344, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$345, DW_AT_name("event_timer")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$346, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$347, DW_AT_name("last_message")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$112

$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
$C$DW$T$54	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x16)

$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x14)
$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$348, DW_AT_name("nodeId")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$349, DW_AT_name("whoami")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$350, DW_AT_name("state")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$351, DW_AT_name("toggle")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$352, DW_AT_name("abortCode")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$353, DW_AT_name("index")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$354, DW_AT_name("subIndex")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$355, DW_AT_name("port")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$356, DW_AT_name("count")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$357, DW_AT_name("offset")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$358, DW_AT_name("datap")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$359, DW_AT_name("dataType")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$360, DW_AT_name("timer")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$361, DW_AT_name("Callback")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)

$C$DW$T$66	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x3c)
$C$DW$362	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$362, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$66


$C$DW$T$118	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$118, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$118, DW_AT_byte_size(0x04)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$363, DW_AT_name("pSubindex")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$364, DW_AT_name("bSubCount")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$365, DW_AT_name("index")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$118

$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$366	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$50)
$C$DW$T$51	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$366)
$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x16)

$C$DW$T$94	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)
$C$DW$367	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$9)
$C$DW$368	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$26)
$C$DW$369	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$93)
	.dwendtag $C$DW$T$94

$C$DW$T$95	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_address_class(0x16)
$C$DW$T$96	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$96, DW_AT_language(DW_LANG_C)

$C$DW$T$119	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$119, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$119, DW_AT_byte_size(0x08)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$370, DW_AT_name("bAccessType")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$371, DW_AT_name("bDataType")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$372, DW_AT_name("size")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$373, DW_AT_name("pObject")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$374, DW_AT_name("bProcessor")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$119

$C$DW$375	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$119)
$C$DW$T$115	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$375)
$C$DW$T$116	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$116, DW_AT_language(DW_LANG_C)
$C$DW$T$117	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$T$117, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$376	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$376, DW_AT_location[DW_OP_reg0]
$C$DW$377	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$377, DW_AT_location[DW_OP_reg1]
$C$DW$378	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$378, DW_AT_location[DW_OP_reg2]
$C$DW$379	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$379, DW_AT_location[DW_OP_reg3]
$C$DW$380	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$380, DW_AT_location[DW_OP_reg20]
$C$DW$381	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$381, DW_AT_location[DW_OP_reg21]
$C$DW$382	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$382, DW_AT_location[DW_OP_reg22]
$C$DW$383	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$383, DW_AT_location[DW_OP_reg23]
$C$DW$384	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$384, DW_AT_location[DW_OP_reg24]
$C$DW$385	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$385, DW_AT_location[DW_OP_reg25]
$C$DW$386	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$386, DW_AT_location[DW_OP_reg26]
$C$DW$387	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$387, DW_AT_location[DW_OP_reg28]
$C$DW$388	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$388, DW_AT_location[DW_OP_reg29]
$C$DW$389	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$389, DW_AT_location[DW_OP_reg30]
$C$DW$390	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$390, DW_AT_location[DW_OP_reg31]
$C$DW$391	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$391, DW_AT_location[DW_OP_regx 0x20]
$C$DW$392	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$392, DW_AT_location[DW_OP_regx 0x21]
$C$DW$393	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$393, DW_AT_location[DW_OP_regx 0x22]
$C$DW$394	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$394, DW_AT_location[DW_OP_regx 0x23]
$C$DW$395	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$395, DW_AT_location[DW_OP_regx 0x24]
$C$DW$396	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$396, DW_AT_location[DW_OP_regx 0x25]
$C$DW$397	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$397, DW_AT_location[DW_OP_regx 0x26]
$C$DW$398	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$398, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$399	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$399, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$400	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$400, DW_AT_location[DW_OP_reg4]
$C$DW$401	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$401, DW_AT_location[DW_OP_reg6]
$C$DW$402	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$402, DW_AT_location[DW_OP_reg8]
$C$DW$403	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$403, DW_AT_location[DW_OP_reg10]
$C$DW$404	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$404, DW_AT_location[DW_OP_reg12]
$C$DW$405	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$405, DW_AT_location[DW_OP_reg14]
$C$DW$406	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$406, DW_AT_location[DW_OP_reg16]
$C$DW$407	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$407, DW_AT_location[DW_OP_reg17]
$C$DW$408	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$408, DW_AT_location[DW_OP_reg18]
$C$DW$409	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$409, DW_AT_location[DW_OP_reg19]
$C$DW$410	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$410, DW_AT_location[DW_OP_reg5]
$C$DW$411	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$411, DW_AT_location[DW_OP_reg7]
$C$DW$412	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$412, DW_AT_location[DW_OP_reg9]
$C$DW$413	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$413, DW_AT_location[DW_OP_reg11]
$C$DW$414	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$414, DW_AT_location[DW_OP_reg13]
$C$DW$415	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$415, DW_AT_location[DW_OP_reg15]
$C$DW$416	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$416, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$417	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$417, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$418	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$418, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$419	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$419, DW_AT_location[DW_OP_regx 0x30]
$C$DW$420	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$420, DW_AT_location[DW_OP_regx 0x33]
$C$DW$421	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$421, DW_AT_location[DW_OP_regx 0x34]
$C$DW$422	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$422, DW_AT_location[DW_OP_regx 0x37]
$C$DW$423	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$423, DW_AT_location[DW_OP_regx 0x38]
$C$DW$424	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$424, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$425	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$425, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$426	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$426, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$427	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$427, DW_AT_location[DW_OP_regx 0x40]
$C$DW$428	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$428, DW_AT_location[DW_OP_regx 0x43]
$C$DW$429	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$429, DW_AT_location[DW_OP_regx 0x44]
$C$DW$430	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$430, DW_AT_location[DW_OP_regx 0x47]
$C$DW$431	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$431, DW_AT_location[DW_OP_regx 0x48]
$C$DW$432	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$432, DW_AT_location[DW_OP_regx 0x49]
$C$DW$433	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$433, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$434	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$434, DW_AT_location[DW_OP_regx 0x27]
$C$DW$435	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$435, DW_AT_location[DW_OP_regx 0x28]
$C$DW$436	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$436, DW_AT_location[DW_OP_reg27]
$C$DW$437	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$437, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

