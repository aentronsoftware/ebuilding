;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue May 25 14:03:58 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../param.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\EBuilding\PTM078")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_PAR_Flag+0,32
	.bits	1,16			; _PAR_Flag @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TimeLogIndex+0,32
	.bits	-1,16			; _TimeLogIndex @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_PAR_Capacity_Left+0,32
	.bits	4677120,32			; _PAR_Capacity_Left @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_PAR_Operating_Hours_day+0,32
	.bits	0,32			; _PAR_Operating_Hours_day @ 0

	.sect	".cinit"
	.align	1
	.field  	-4,16
	.field  	_PAR_Capacity_TotalLife_Used+0,32
	.bits		0,32
	.bits		0,32			; _PAR_Capacity_TotalLife_Used @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_1,16
	.field  	_PAR_StatEepromCommandState+0,32
	.bits	0,16			; _PAR_StatEepromCommandState[0] @ 0
	.bits	0,16			; _PAR_StatEepromCommandState[1] @ 16
	.bits	0,16			; _PAR_StatEepromCommandState[2] @ 32
	.bits	0,16			; _PAR_StatEepromCommandState[3] @ 48
	.bits	0,16			; _PAR_StatEepromCommandState[4] @ 64
$C$IR_1:	.set	5

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_2,16
	.field  	_TimeLog+0,32
	.bits		0,32
	.bits		0,32			; _TimeLog[0]._date @ 0
	.bits	0,32			; _TimeLog[0]._error @ 64
	.bits		0,32
	.bits		0,32			; _TimeLog[1]._date @ 96
	.bits	0,32			; _TimeLog[1]._error @ 160
	.bits		0,32
	.bits		0,32			; _TimeLog[2]._date @ 192
	.bits	0,32			; _TimeLog[2]._error @ 256
	.bits		0,32
	.bits		0,32			; _TimeLog[3]._date @ 288
	.bits	0,32			; _TimeLog[3]._error @ 352
	.bits		0,32
	.bits		0,32			; _TimeLog[4]._date @ 384
	.bits	0,32			; _TimeLog[4]._error @ 448
	.bits		0,32
	.bits		0,32			; _TimeLog[5]._date @ 480
	.bits	0,32			; _TimeLog[5]._error @ 544
	.bits		0,32
	.bits		0,32			; _TimeLog[6]._date @ 576
	.bits	0,32			; _TimeLog[6]._error @ 640
	.bits		0,32
	.bits		0,32			; _TimeLog[7]._date @ 672
	.bits	0,32			; _TimeLog[7]._error @ 736
$C$IR_2:	.set	48


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("canOpenInit")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_canOpenInit")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_Max")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_ODV_Current_Max")
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_OverTemp_ErrCounter")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_ODP_CommError_OverTemp_ErrCounter")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Voltage_Max")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_ODV_Voltage_Max")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("ODP_VersionParameters")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_ODP_VersionParameters")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Voltage_Min")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_ODV_Voltage_Min")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Temperature_Max")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_ODV_Temperature_Max")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
	.global	_PAR_Flag
_PAR_Flag:	.usect	".ebss",1,1,0
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Flag")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_PAR_Flag")
	.dwattr $C$DW$8, DW_AT_location[DW_OP_addr _PAR_Flag]
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$8, DW_AT_external
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("CNV_CurrentRange")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_CNV_CurrentRange")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$178)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
	.global	_TimeLogIndex
_TimeLogIndex:	.usect	".ebss",1,1,0
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("TimeLogIndex")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_TimeLogIndex")
	.dwattr $C$DW$10, DW_AT_location[DW_OP_addr _TimeLogIndex]
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj100A")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_mms_dict_obj100A")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
	.global	_EepromIndexesSize
	.sect	".econst"
	.align	1
_EepromIndexesSize:
	.bits	224,16			; _EepromIndexesSize @ 0

$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("EepromIndexesSize")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_EepromIndexesSize")
	.dwattr $C$DW$12, DW_AT_location[DW_OP_addr _EepromIndexesSize]
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_OverVoltage_ErrCounter")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_ODP_CommError_OverVoltage_ErrCounter")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external
$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_Min")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_ODV_Current_Min")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_LowVoltage_ErrCounter")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_ODP_CommError_LowVoltage_ErrCounter")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Version")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_ODV_Version")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_BaudRate")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_ODP_Board_BaudRate")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("InitOK")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_InitOK")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external

$C$DW$19	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external
$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$146)
$C$DW$21	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$150)
$C$DW$22	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$40)
	.dwendtag $C$DW$19


$C$DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$23, DW_AT_declaration
	.dwattr $C$DW$23, DW_AT_external
$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$9)
$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$42)
$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$9)
$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$23


$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$149)
$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$40)
	.dwendtag $C$DW$28


$C$DW$31	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$31, DW_AT_declaration
	.dwattr $C$DW$31, DW_AT_external
$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$146)
$C$DW$33	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$150)
$C$DW$34	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$40)
	.dwendtag $C$DW$31

$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("CNV_CurrentUnit")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_CNV_CurrentUnit")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$197)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external

$C$DW$36	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_Round")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_CNV_Round")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
$C$DW$37	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$36


$C$DW$38	.dwtag  DW_TAG_subprogram, DW_AT_name("getCRC32_cpu")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_getCRC32_cpu")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external
$C$DW$39	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$13)
$C$DW$40	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$81)
$C$DW$41	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$123)
$C$DW$42	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$38

$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Capacity")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_ODP_Battery_Capacity")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1018_Revision_Number")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_mms_dict_obj1018_Revision_Number")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$44, DW_AT_declaration
	.dwattr $C$DW$44, DW_AT_external

$C$DW$45	.dwtag  DW_TAG_subprogram, DW_AT_name("_getODentry")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("__getODentry")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$72)
$C$DW$47	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$9)
$C$DW$48	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$6)
$C$DW$49	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$3)
$C$DW$50	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$79)
$C$DW$51	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$50)
$C$DW$52	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$6)
$C$DW$53	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$6)
$C$DW$54	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$45

$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1018_Vendor_ID")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_mms_dict_obj1018_Vendor_ID")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1018_Serial_Number")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_mms_dict_obj1018_Serial_Number")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$56, DW_AT_declaration
	.dwattr $C$DW$56, DW_AT_external
	.global	_PAR_Capacity_Left
_PAR_Capacity_Left:	.usect	".ebss",2,1,1
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Left")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_PAR_Capacity_Left")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_addr _PAR_Capacity_Left]
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$57, DW_AT_external

$C$DW$58	.dwtag  DW_TAG_subprogram, DW_AT_name("_setODentry")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("__setODentry")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$58, DW_AT_declaration
	.dwattr $C$DW$58, DW_AT_external
$C$DW$59	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$72)
$C$DW$60	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$9)
$C$DW$61	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$6)
$C$DW$62	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$3)
$C$DW$63	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$79)
$C$DW$64	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$6)
$C$DW$65	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$6)
$C$DW$66	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$58

	.global	_PAR_Operating_Hours_day
_PAR_Operating_Hours_day:	.usect	".ebss",2,1,1
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Operating_Hours_day")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_PAR_Operating_Hours_day")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_addr _PAR_Operating_Hours_day]
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$67, DW_AT_external
	.global	_PAR_Capacity_Total
_PAR_Capacity_Total:	.usect	".ebss",2,1,1
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Total")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_PAR_Capacity_Total")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_addr _PAR_Capacity_Total]
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$68, DW_AT_external
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Errorcode")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ODV_Gateway_Errorcode")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_external
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CrcParameters")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_ODP_CrcParameters")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$70, DW_AT_declaration
	.dwattr $C$DW$70, DW_AT_external
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$71, DW_AT_declaration
	.dwattr $C$DW$71, DW_AT_external
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("ODP_OnTime")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_ODP_OnTime")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$72, DW_AT_declaration
	.dwattr $C$DW$72, DW_AT_external
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_SerialNumber")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_ODP_Board_SerialNumber")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$73, DW_AT_declaration
	.dwattr $C$DW$73, DW_AT_external
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$74, DW_AT_declaration
	.dwattr $C$DW$74, DW_AT_external
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("mms_dict_obj1018_Product_Code")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_mms_dict_obj1018_Product_Code")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$75, DW_AT_declaration
	.dwattr $C$DW$75, DW_AT_external
	.global	_PAR_Capacity_TotalLife_Used
_PAR_Capacity_TotalLife_Used:	.usect	".ebss",4,1,1
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_addr _PAR_Capacity_TotalLife_Used]
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$76, DW_AT_external
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Date_Time")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_ODV_Gateway_Date_Time")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$77, DW_AT_declaration
	.dwattr $C$DW$77, DW_AT_external
	.global	_PAR_StatEepromCommandState
_PAR_StatEepromCommandState:	.usect	".ebss",5,1,0
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("PAR_StatEepromCommandState")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_PAR_StatEepromCommandState")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_addr _PAR_StatEepromCommandState]
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$78, DW_AT_external
	.global	_IndexesOldRate
	.sect	".econst:_IndexesOldRate"
	.clink
	.align	1
_IndexesOldRate:
	.bits	8256,16			; _IndexesOldRate[0]._index @ 0
	.bits	4,16			; _IndexesOldRate[0]._subindex @ 16
	.bits	2,16			; _IndexesOldRate[0]._size @ 32
	.bits	106,16			; _IndexesOldRate[0]._address @ 48
	.bits	8256,16			; _IndexesOldRate[1]._index @ 64
	.bits	4,16			; _IndexesOldRate[1]._subindex @ 80
	.bits	2,16			; _IndexesOldRate[1]._size @ 96
	.bits	110,16			; _IndexesOldRate[1]._address @ 112
	.bits	8256,16			; _IndexesOldRate[2]._index @ 128
	.bits	4,16			; _IndexesOldRate[2]._subindex @ 144
	.bits	2,16			; _IndexesOldRate[2]._size @ 160
	.bits	117,16			; _IndexesOldRate[2]._address @ 176

$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("IndexesOldRate")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_IndexesOldRate")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_addr _IndexesOldRate]
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$79, DW_AT_external
	.global	_IndexesOldId
	.sect	".econst:_IndexesOldId"
	.clink
	.align	1
_IndexesOldId:
	.bits	8256,16			; _IndexesOldId[0]._index @ 0
	.bits	1,16			; _IndexesOldId[0]._subindex @ 16
	.bits	4,16			; _IndexesOldId[0]._size @ 32
	.bits	96,16			; _IndexesOldId[0]._address @ 48
	.bits	8256,16			; _IndexesOldId[1]._index @ 64
	.bits	1,16			; _IndexesOldId[1]._subindex @ 80
	.bits	4,16			; _IndexesOldId[1]._size @ 96
	.bits	100,16			; _IndexesOldId[1]._address @ 112
	.bits	8256,16			; _IndexesOldId[2]._index @ 128
	.bits	1,16			; _IndexesOldId[2]._subindex @ 144
	.bits	4,16			; _IndexesOldId[2]._size @ 160
	.bits	107,16			; _IndexesOldId[2]._address @ 176

$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("IndexesOldId")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_IndexesOldId")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_addr _IndexesOldId]
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$80, DW_AT_external
	.global	_STAT_PAGE4
	.sect	".econst:_STAT_PAGE4"
	.clink
	.align	2
_STAT_PAGE4:
	.bits	_TimeLog+24,32		; _STAT_PAGE4[0]._pdata @ 0
	.bits	8,16			; _STAT_PAGE4[0]._size @ 32
	.bits	1120,16			; _STAT_PAGE4[0]._address @ 48
	.bits	_TimeLog+30,32		; _STAT_PAGE4[1]._pdata @ 64
	.bits	8,16			; _STAT_PAGE4[1]._size @ 96
	.bits	1128,16			; _STAT_PAGE4[1]._address @ 112
	.bits	_TimeLog+36,32		; _STAT_PAGE4[2]._pdata @ 128
	.bits	8,16			; _STAT_PAGE4[2]._size @ 160
	.bits	1136,16			; _STAT_PAGE4[2]._address @ 176
	.bits	_TimeLog+42,32		; _STAT_PAGE4[3]._pdata @ 192
	.bits	8,16			; _STAT_PAGE4[3]._size @ 224
	.bits	1144,16			; _STAT_PAGE4[3]._address @ 240

$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE4")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_STAT_PAGE4")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_addr _STAT_PAGE4]
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$81, DW_AT_external
	.global	_STAT_PAGE2
	.sect	".econst:_STAT_PAGE2"
	.clink
	.align	2
_STAT_PAGE2:
	.bits	_TimeLog,32		; _STAT_PAGE2[0]._pdata @ 0
	.bits	8,16			; _STAT_PAGE2[0]._size @ 32
	.bits	1056,16			; _STAT_PAGE2[0]._address @ 48
	.bits	_TimeLog+6,32		; _STAT_PAGE2[1]._pdata @ 64
	.bits	8,16			; _STAT_PAGE2[1]._size @ 96
	.bits	1064,16			; _STAT_PAGE2[1]._address @ 112
	.bits	_TimeLog+12,32		; _STAT_PAGE2[2]._pdata @ 128
	.bits	8,16			; _STAT_PAGE2[2]._size @ 160
	.bits	1072,16			; _STAT_PAGE2[2]._address @ 176
	.bits	_TimeLog+18,32		; _STAT_PAGE2[3]._pdata @ 192
	.bits	8,16			; _STAT_PAGE2[3]._size @ 224
	.bits	1080,16			; _STAT_PAGE2[3]._address @ 240

$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE2")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_STAT_PAGE2")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_addr _STAT_PAGE2]
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$82, DW_AT_external
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$83, DW_AT_declaration
	.dwattr $C$DW$83, DW_AT_external
	.global	_STAT_PAGE5
	.sect	".econst:_STAT_PAGE5"
	.clink
	.align	2
_STAT_PAGE5:
	.bits	_PAR_Capacity_TotalLife_Used,32		; _STAT_PAGE5[0]._pdata @ 0
	.bits	8,16			; _STAT_PAGE5[0]._size @ 32
	.bits	1152,16			; _STAT_PAGE5[0]._address @ 48
	.bits	_PAR_Operating_Hours_day,32		; _STAT_PAGE5[1]._pdata @ 64
	.bits	4,16			; _STAT_PAGE5[1]._size @ 96
	.bits	1160,16			; _STAT_PAGE5[1]._address @ 112
	.bits	_ODP_CommError_OverTemp_ErrCounter,32		; _STAT_PAGE5[2]._pdata @ 128
	.bits	1,16			; _STAT_PAGE5[2]._size @ 160
	.bits	1164,16			; _STAT_PAGE5[2]._address @ 176
	.bits	_ODP_CommError_OverVoltage_ErrCounter,32		; _STAT_PAGE5[3]._pdata @ 192
	.bits	1,16			; _STAT_PAGE5[3]._size @ 224
	.bits	1165,16			; _STAT_PAGE5[3]._address @ 240
	.bits	_ODP_CommError_LowVoltage_ErrCounter,32		; _STAT_PAGE5[4]._pdata @ 256
	.bits	1,16			; _STAT_PAGE5[4]._size @ 288
	.bits	1166,16			; _STAT_PAGE5[4]._address @ 304

$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE5")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_STAT_PAGE5")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_addr _STAT_PAGE5]
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$84, DW_AT_external
	.global	_PAR_EEPROM_INDEXES
	.sect	".econst:_PAR_EEPROM_INDEXES"
	.clink
	.align	2
_PAR_EEPROM_INDEXES:
	.bits	32,16			; _PAR_EEPROM_INDEXES[0]._size @ 0
	.bits	1024,16			; _PAR_EEPROM_INDEXES[0]._address @ 16
	.bits	10,16			; _PAR_EEPROM_INDEXES[0]._nb @ 32
	.space	16
	.bits	_STAT_PAGE1,32		; _PAR_EEPROM_INDEXES[0]._indexes @ 64
	.bits	32,16			; _PAR_EEPROM_INDEXES[1]._size @ 96
	.bits	1056,16			; _PAR_EEPROM_INDEXES[1]._address @ 112
	.bits	4,16			; _PAR_EEPROM_INDEXES[1]._nb @ 128
	.space	16
	.bits	_STAT_PAGE2,32		; _PAR_EEPROM_INDEXES[1]._indexes @ 160
	.bits	32,16			; _PAR_EEPROM_INDEXES[2]._size @ 192
	.bits	1088,16			; _PAR_EEPROM_INDEXES[2]._address @ 208
	.bits	8,16			; _PAR_EEPROM_INDEXES[2]._nb @ 224
	.space	16
	.bits	_STAT_PAGE3,32		; _PAR_EEPROM_INDEXES[2]._indexes @ 256
	.bits	32,16			; _PAR_EEPROM_INDEXES[3]._size @ 288
	.bits	1120,16			; _PAR_EEPROM_INDEXES[3]._address @ 304
	.bits	4,16			; _PAR_EEPROM_INDEXES[3]._nb @ 320
	.space	16
	.bits	_STAT_PAGE4,32		; _PAR_EEPROM_INDEXES[3]._indexes @ 352
	.bits	32,16			; _PAR_EEPROM_INDEXES[4]._size @ 384
	.bits	1152,16			; _PAR_EEPROM_INDEXES[4]._address @ 400
	.bits	5,16			; _PAR_EEPROM_INDEXES[4]._nb @ 416
	.space	16
	.bits	_STAT_PAGE5,32		; _PAR_EEPROM_INDEXES[4]._indexes @ 448

$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("PAR_EEPROM_INDEXES")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_PAR_EEPROM_INDEXES")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_addr _PAR_EEPROM_INDEXES]
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$85, DW_AT_external
	.global	_STAT_PAGE3
	.sect	".econst:_STAT_PAGE3"
	.clink
	.align	2
_STAT_PAGE3:
	.bits	_TimeLog+4,32		; _STAT_PAGE3[0]._pdata @ 0
	.bits	4,16			; _STAT_PAGE3[0]._size @ 32
	.bits	1088,16			; _STAT_PAGE3[0]._address @ 48
	.bits	_TimeLog+10,32		; _STAT_PAGE3[1]._pdata @ 64
	.bits	4,16			; _STAT_PAGE3[1]._size @ 96
	.bits	1092,16			; _STAT_PAGE3[1]._address @ 112
	.bits	_TimeLog+16,32		; _STAT_PAGE3[2]._pdata @ 128
	.bits	4,16			; _STAT_PAGE3[2]._size @ 160
	.bits	1096,16			; _STAT_PAGE3[2]._address @ 176
	.bits	_TimeLog+22,32		; _STAT_PAGE3[3]._pdata @ 192
	.bits	4,16			; _STAT_PAGE3[3]._size @ 224
	.bits	1100,16			; _STAT_PAGE3[3]._address @ 240
	.bits	_TimeLog+28,32		; _STAT_PAGE3[4]._pdata @ 256
	.bits	4,16			; _STAT_PAGE3[4]._size @ 288
	.bits	1104,16			; _STAT_PAGE3[4]._address @ 304
	.bits	_TimeLog+34,32		; _STAT_PAGE3[5]._pdata @ 320
	.bits	4,16			; _STAT_PAGE3[5]._size @ 352
	.bits	1108,16			; _STAT_PAGE3[5]._address @ 368
	.bits	_TimeLog+40,32		; _STAT_PAGE3[6]._pdata @ 384
	.bits	4,16			; _STAT_PAGE3[6]._size @ 416
	.bits	1112,16			; _STAT_PAGE3[6]._address @ 432
	.bits	_TimeLog+46,32		; _STAT_PAGE3[7]._pdata @ 448
	.bits	4,16			; _STAT_PAGE3[7]._size @ 480
	.bits	1116,16			; _STAT_PAGE3[7]._address @ 496

$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE3")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_STAT_PAGE3")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_addr _STAT_PAGE3]
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$86, DW_AT_external
	.global	_STAT_PAGE1
	.sect	".econst:_STAT_PAGE1"
	.clink
	.align	2
_STAT_PAGE1:
	.bits	_ODP_OnTime,32		; _STAT_PAGE1[0]._pdata @ 0
	.bits	4,16			; _STAT_PAGE1[0]._size @ 32
	.bits	1024,16			; _STAT_PAGE1[0]._address @ 48
	.bits	_TimeLogIndex,32		; _STAT_PAGE1[1]._pdata @ 64
	.bits	2,16			; _STAT_PAGE1[1]._size @ 96
	.bits	1028,16			; _STAT_PAGE1[1]._address @ 112
	.bits	_ODV_Gateway_Date_Time,32		; _STAT_PAGE1[2]._pdata @ 128
	.bits	8,16			; _STAT_PAGE1[2]._size @ 160
	.bits	1030,16			; _STAT_PAGE1[2]._address @ 176
	.bits	_ODV_Gateway_Errorcode,32		; _STAT_PAGE1[3]._pdata @ 192
	.bits	4,16			; _STAT_PAGE1[3]._size @ 224
	.bits	1038,16			; _STAT_PAGE1[3]._address @ 240
	.bits	_ODV_Temperature_Max,32		; _STAT_PAGE1[4]._pdata @ 256
	.bits	2,16			; _STAT_PAGE1[4]._size @ 288
	.bits	1042,16			; _STAT_PAGE1[4]._address @ 304
	.bits	_ODV_Current_Max,32		; _STAT_PAGE1[5]._pdata @ 320
	.bits	2,16			; _STAT_PAGE1[5]._size @ 352
	.bits	1044,16			; _STAT_PAGE1[5]._address @ 368
	.bits	_ODV_Current_Min,32		; _STAT_PAGE1[6]._pdata @ 384
	.bits	2,16			; _STAT_PAGE1[6]._size @ 416
	.bits	1046,16			; _STAT_PAGE1[6]._address @ 432
	.bits	_ODV_Voltage_Max,32		; _STAT_PAGE1[7]._pdata @ 448
	.bits	2,16			; _STAT_PAGE1[7]._size @ 480
	.bits	1048,16			; _STAT_PAGE1[7]._address @ 496
	.bits	_ODV_Voltage_Min,32		; _STAT_PAGE1[8]._pdata @ 512
	.bits	2,16			; _STAT_PAGE1[8]._size @ 544
	.bits	1050,16			; _STAT_PAGE1[8]._address @ 560
	.bits	_PAR_Capacity_Left,32		; _STAT_PAGE1[9]._pdata @ 576
	.bits	4,16			; _STAT_PAGE1[9]._size @ 608
	.bits	1052,16			; _STAT_PAGE1[9]._address @ 624

$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE1")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_STAT_PAGE1")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_addr _STAT_PAGE1]
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$87, DW_AT_external
	.global	_TimeLog
_TimeLog:	.usect	".ebss",48,1,1
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("TimeLog")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_TimeLog")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_addr _TimeLog]
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$88, DW_AT_external
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("mailboxWriteParameters")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_mailboxWriteParameters")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$89, DW_AT_declaration
	.dwattr $C$DW$89, DW_AT_external
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("ODI_EEPROM_INDEXES")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_ODI_EEPROM_INDEXES")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$90, DW_AT_declaration
	.dwattr $C$DW$90, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1948012 
	.sect	".text"
	.global	_PAR_AddLog

$C$DW$91	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddLog")
	.dwattr $C$DW$91, DW_AT_low_pc(_PAR_AddLog)
	.dwattr $C$DW$91, DW_AT_high_pc(0x00)
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_PAR_AddLog")
	.dwattr $C$DW$91, DW_AT_external
	.dwattr $C$DW$91, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$91, DW_AT_TI_begin_line(0x6b)
	.dwattr $C$DW$91, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$91, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../param.c",line 107,column 22,is_stmt,address _PAR_AddLog

	.dwfde $C$DW$CIE, _PAR_AddLog

;***************************************************************
;* FNAME: _PAR_AddLog                   FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_PAR_AddLog:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../param.c",line 108,column 3,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       AL,@_TimeLogIndex     ; [CPU_] |108| 
        CMPB      AL,#7                 ; [CPU_] |108| 
        B         $C$L1,GEQ             ; [CPU_] |108| 
        ; branchcc occurs ; [] |108| 
	.dwpsn	file "../param.c",line 108,column 32,is_stmt
        INC       @_TimeLogIndex        ; [CPU_] |108| 
        B         $C$L2,UNC             ; [CPU_] |108| 
        ; branch occurs ; [] |108| 
$C$L1:    
	.dwpsn	file "../param.c",line 109,column 8,is_stmt
        MOV       @_TimeLogIndex,#0     ; [CPU_] |109| 
$C$L2:    
	.dwpsn	file "../param.c",line 110,column 3,is_stmt
        MOV       T,@_TimeLogIndex      ; [CPU_] |110| 
        MOVW      DP,#_ODV_Gateway_Date_Time ; [CPU_U] 
        MOVL      XAR4,#_TimeLog        ; [CPU_U] |110| 
        MOVL      XAR6,@_ODV_Gateway_Date_Time ; [CPU_] |110| 
        MOVL      XAR7,@_ODV_Gateway_Date_Time+2 ; [CPU_] |110| 
        MPYB      ACC,T,#6              ; [CPU_] |110| 
        ADDL      XAR4,ACC              ; [CPU_] |110| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |110| 
        MOVL      *+XAR4[2],XAR7        ; [CPU_] |110| 
	.dwpsn	file "../param.c",line 111,column 3,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       T,@_TimeLogIndex      ; [CPU_] |111| 
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      XAR6,@_ODV_Gateway_Errorcode ; [CPU_] |111| 
        MOVL      XAR4,#_TimeLog+4      ; [CPU_U] |111| 
        MPYB      ACC,T,#6              ; [CPU_] |111| 
        ADDL      XAR4,ACC              ; [CPU_] |111| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |111| 
	.dwpsn	file "../param.c",line 112,column 3,is_stmt
        MOV       AL,#-1                ; [CPU_] |112| 
$C$DW$92	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$92, DW_AT_low_pc(0x00)
	.dwattr $C$DW$92, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$92, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |112| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |112| 
	.dwpsn	file "../param.c",line 113,column 3,is_stmt
        MOV       AL,#-3                ; [CPU_] |113| 
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$93, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |113| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |113| 
	.dwpsn	file "../param.c",line 114,column 3,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       AL,@_TimeLogIndex     ; [CPU_] |114| 
        B         $C$L3,LT              ; [CPU_] |114| 
        ; branchcc occurs ; [] |114| 
        CMPB      AL,#4                 ; [CPU_] |114| 
        B         $C$L3,GEQ             ; [CPU_] |114| 
        ; branchcc occurs ; [] |114| 
	.dwpsn	file "../param.c",line 115,column 5,is_stmt
        MOV       AL,#-2                ; [CPU_] |115| 
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$94, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |115| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |115| 
	.dwpsn	file "../param.c",line 116,column 3,is_stmt
        B         $C$L4,UNC             ; [CPU_] |116| 
        ; branch occurs ; [] |116| 
$C$L3:    
	.dwpsn	file "../param.c",line 116,column 9,is_stmt
        MOV       AL,#-4                ; [CPU_] |116| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$95, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |116| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |116| 
	.dwpsn	file "../param.c",line 117,column 1,is_stmt
$C$L4:    
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$91, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$91, DW_AT_TI_end_line(0x75)
	.dwattr $C$DW$91, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$91

	.sect	".text"
	.global	_PAR_GetLogNB

$C$DW$97	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_GetLogNB")
	.dwattr $C$DW$97, DW_AT_low_pc(_PAR_GetLogNB)
	.dwattr $C$DW$97, DW_AT_high_pc(0x00)
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_PAR_GetLogNB")
	.dwattr $C$DW$97, DW_AT_external
	.dwattr $C$DW$97, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$97, DW_AT_TI_begin_line(0x77)
	.dwattr $C$DW$97, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$97, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../param.c",line 119,column 28,is_stmt,address _PAR_GetLogNB

	.dwfde $C$DW$CIE, _PAR_GetLogNB
$C$DW$98	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PAR_GetLogNB                 FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_PAR_GetLogNB:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$99	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |119| 
	.dwpsn	file "../param.c",line 120,column 3,is_stmt
        MOV       T,#6                  ; [CPU_] |120| 
        MOVL      XAR4,#_TimeLog        ; [CPU_U] |120| 
        MPYXU     ACC,T,*-SP[1]         ; [CPU_] |120| 
        ADDL      XAR4,ACC              ; [CPU_] |120| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |120| 
        MOVW      DP,#_ODV_Gateway_Date_Time ; [CPU_U] 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |120| 
        MOVL      @_ODV_Gateway_Date_Time,ACC ; [CPU_] |120| 
        MOVL      @_ODV_Gateway_Date_Time+2,XAR6 ; [CPU_] |120| 
	.dwpsn	file "../param.c",line 121,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      XAR4,#_TimeLog+4      ; [CPU_U] |121| 
        MPYXU     ACC,T,*-SP[1]         ; [CPU_] |121| 
        ADDL      XAR4,ACC              ; [CPU_] |121| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |121| 
        MOVL      @_ODV_Gateway_Errorcode,ACC ; [CPU_] |121| 
	.dwpsn	file "../param.c",line 122,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$100	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$100, DW_AT_low_pc(0x00)
	.dwattr $C$DW$100, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$97, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$97, DW_AT_TI_end_line(0x7a)
	.dwattr $C$DW$97, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$97

	.sect	".text"
	.global	_PAR_TestEeprom

$C$DW$101	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_TestEeprom")
	.dwattr $C$DW$101, DW_AT_low_pc(_PAR_TestEeprom)
	.dwattr $C$DW$101, DW_AT_high_pc(0x00)
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_PAR_TestEeprom")
	.dwattr $C$DW$101, DW_AT_external
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$101, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$101, DW_AT_TI_begin_line(0x89)
	.dwattr $C$DW$101, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$101, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../param.c",line 137,column 27,is_stmt,address _PAR_TestEeprom

	.dwfde $C$DW$CIE, _PAR_TestEeprom

;***************************************************************
;* FNAME: _PAR_TestEeprom               FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 19 Auto,  0 SOE     *
;***************************************************************

_PAR_TestEeprom:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_breg20 -1]
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -2]
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_breg20 -3]
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -19]
	.dwpsn	file "../param.c",line 139,column 13,is_stmt
        MOVB      *-SP[3],#1,UNC        ; [CPU_] |139| 
	.dwpsn	file "../param.c",line 141,column 8,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |141| 
	.dwpsn	file "../param.c",line 141,column 12,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |141| 
        CMPB      AL,#16                ; [CPU_] |141| 
        B         $C$L6,GEQ             ; [CPU_] |141| 
        ; branchcc occurs ; [] |141| 
$C$L5:    
	.dwpsn	file "../param.c",line 142,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[1]           ; [CPU_] |142| 
        MOVZ      AR5,*-SP[1]           ; [CPU_] |142| 
        MOVZ      AR4,SP                ; [CPU_U] |142| 
        SUBB      XAR4,#19              ; [CPU_U] |142| 
        ADDL      XAR4,ACC              ; [CPU_] |142| 
        ADDB      XAR5,#1               ; [CPU_] |142| 
        MOV       ACC,AR5 << #8         ; [CPU_] |142| 
        ADD       AL,*-SP[1]            ; [CPU_] |142| 
        ADDB      AL,#2                 ; [CPU_] |142| 
        MOV       *+XAR4[0],AL          ; [CPU_] |142| 
	.dwpsn	file "../param.c",line 141,column 32,is_stmt
        INC       *-SP[1]               ; [CPU_] |141| 
	.dwpsn	file "../param.c",line 141,column 12,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |141| 
        CMPB      AL,#16                ; [CPU_] |141| 
        B         $C$L5,LT              ; [CPU_] |141| 
        ; branchcc occurs ; [] |141| 
$C$L6:    
	.dwpsn	file "../param.c",line 143,column 3,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |143| 
	.dwpsn	file "../param.c",line 144,column 3,is_stmt
        B         $C$L12,UNC            ; [CPU_] |144| 
        ; branch occurs ; [] |144| 
$C$L7:    
	.dwpsn	file "../param.c",line 145,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |145| 
        MOVZ      AR5,*-SP[2]           ; [CPU_] |145| 
        MOVB      AL,#0                 ; [CPU_] |145| 
        MOVB      AH,#32                ; [CPU_] |145| 
        SUBB      XAR4,#19              ; [CPU_U] |145| 
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$106, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |145| 
        ; call occurs [#_I2C_Command] ; [] |145| 
        MOV       *-SP[3],AL            ; [CPU_] |145| 
	.dwpsn	file "../param.c",line 146,column 5,is_stmt
        MOVB      AL,#2                 ; [CPU_] |146| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |146| 
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$107, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |146| 
        ; call occurs [#_SEM_pend] ; [] |146| 
	.dwpsn	file "../param.c",line 147,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |147| 
        BF        $C$L8,EQ              ; [CPU_] |147| 
        ; branchcc occurs ; [] |147| 
	.dwpsn	file "../param.c",line 148,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |148| 
        MOVZ      AR5,*-SP[2]           ; [CPU_] |148| 
        MOVB      AL,#1                 ; [CPU_] |148| 
        MOVB      AH,#32                ; [CPU_] |148| 
        SUBB      XAR4,#19              ; [CPU_U] |148| 
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$108, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |148| 
        ; call occurs [#_I2C_Command] ; [] |148| 
        MOV       *-SP[3],AL            ; [CPU_] |148| 
$C$L8:    
	.dwpsn	file "../param.c",line 149,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |149| 
        MOVB      AL,#2                 ; [CPU_] |149| 
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$109, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |149| 
        ; call occurs [#_SEM_pend] ; [] |149| 
	.dwpsn	file "../param.c",line 150,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |150| 
        BF        $C$L13,EQ             ; [CPU_] |150| 
        ; branchcc occurs ; [] |150| 
	.dwpsn	file "../param.c",line 151,column 12,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |151| 
	.dwpsn	file "../param.c",line 151,column 16,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |151| 
        CMPB      AL,#16                ; [CPU_] |151| 
        B         $C$L11,GEQ            ; [CPU_] |151| 
        ; branchcc occurs ; [] |151| 
$C$L9:    
	.dwpsn	file "../param.c",line 152,column 9,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[1]           ; [CPU_] |152| 
        MOVZ      AR5,*-SP[1]           ; [CPU_] |152| 
        MOVZ      AR4,SP                ; [CPU_U] |152| 
        SUBB      XAR4,#19              ; [CPU_U] |152| 
        ADDL      XAR4,ACC              ; [CPU_] |152| 
        ADDB      XAR5,#1               ; [CPU_] |152| 
        MOV       ACC,AR5 << #8         ; [CPU_] |152| 
        ADD       AL,*-SP[1]            ; [CPU_] |152| 
        ADDB      AL,#2                 ; [CPU_] |152| 
        CMP       AL,*+XAR4[0]          ; [CPU_] |152| 
        BF        $C$L10,EQ             ; [CPU_] |152| 
        ; branchcc occurs ; [] |152| 
	.dwpsn	file "../param.c",line 153,column 11,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |153| 
	.dwpsn	file "../param.c",line 154,column 11,is_stmt
        B         $C$L11,UNC            ; [CPU_] |154| 
        ; branch occurs ; [] |154| 
$C$L10:    
	.dwpsn	file "../param.c",line 151,column 36,is_stmt
        INC       *-SP[1]               ; [CPU_] |151| 
	.dwpsn	file "../param.c",line 151,column 16,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |151| 
        CMPB      AL,#16                ; [CPU_] |151| 
        B         $C$L9,LT              ; [CPU_] |151| 
        ; branchcc occurs ; [] |151| 
	.dwpsn	file "../param.c",line 157,column 5,is_stmt
        B         $C$L11,UNC            ; [CPU_] |157| 
        ; branch occurs ; [] |157| 
$C$L11:    
	.dwpsn	file "../param.c",line 160,column 5,is_stmt
        ADD       *-SP[2],#32           ; [CPU_] |160| 
$C$L12:    
	.dwpsn	file "../param.c",line 144,column 10,is_stmt
        CMP       *-SP[2],#8192         ; [CPU_] |144| 
        B         $C$L13,GEQ            ; [CPU_] |144| 
        ; branchcc occurs ; [] |144| 
        MOV       AL,*-SP[3]            ; [CPU_] |144| 
        BF        $C$L7,NEQ             ; [CPU_] |144| 
        ; branchcc occurs ; [] |144| 
$C$L13:    
	.dwpsn	file "../param.c",line 162,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |162| 
        BF        $C$L14,EQ             ; [CPU_] |162| 
        ; branchcc occurs ; [] |162| 
	.dwpsn	file "../param.c",line 163,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |163| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_PAR_SetDefaultParameters")
	.dwattr $C$DW$110, DW_AT_TI_call
        LCR       #_PAR_SetDefaultParameters ; [CPU_] |163| 
        ; call occurs [#_PAR_SetDefaultParameters] ; [] |163| 
        MOV       *-SP[3],AL            ; [CPU_] |163| 
$C$L14:    
	.dwpsn	file "../param.c",line 164,column 3,is_stmt
	.dwpsn	file "../param.c",line 165,column 1,is_stmt
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$111	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$111, DW_AT_low_pc(0x00)
	.dwattr $C$DW$111, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$101, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$101, DW_AT_TI_end_line(0xa5)
	.dwattr $C$DW$101, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$101

	.sect	".text"
	.global	_ComputeParamCRC

$C$DW$112	.dwtag  DW_TAG_subprogram, DW_AT_name("ComputeParamCRC")
	.dwattr $C$DW$112, DW_AT_low_pc(_ComputeParamCRC)
	.dwattr $C$DW$112, DW_AT_high_pc(0x00)
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_ComputeParamCRC")
	.dwattr $C$DW$112, DW_AT_external
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$112, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$112, DW_AT_TI_begin_line(0xb5)
	.dwattr $C$DW$112, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$112, DW_AT_TI_max_frame_size(-28)
	.dwpsn	file "../param.c",line 181,column 29,is_stmt,address _ComputeParamCRC

	.dwfde $C$DW$CIE, _ComputeParamCRC

;***************************************************************
;* FNAME: _ComputeParamCRC              FR SIZE:  26           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 24 Auto,  0 SOE     *
;***************************************************************

_ComputeParamCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -28
$C$DW$113	.dwtag  DW_TAG_variable, DW_AT_name("nb_data")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_nb_data")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$113, DW_AT_location[DW_OP_breg20 -2]
$C$DW$114	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_breg20 -18]
$C$DW$115	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_breg20 -19]
$C$DW$116	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$116, DW_AT_location[DW_OP_breg20 -20]
$C$DW$117	.dwtag  DW_TAG_variable, DW_AT_name("indexcrc")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_indexcrc")
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$117, DW_AT_location[DW_OP_breg20 -21]
$C$DW$118	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$118, DW_AT_location[DW_OP_breg20 -24]
$C$DW$119	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$119, DW_AT_location[DW_OP_breg20 -25]
	.dwpsn	file "../param.c",line 182,column 18,is_stmt
        MOVW      DP,#_EepromIndexesSize ; [CPU_U] 
        MOV       AL,@_EepromIndexesSize ; [CPU_] |182| 
        MOVL      XAR4,#_ODI_EEPROM_INDEXES+3 ; [CPU_U] |182| 
        ADDB      AL,#-1                ; [CPU_] |182| 
        MOVL      XAR5,#_ODI_EEPROM_INDEXES+2 ; [CPU_U] |182| 
        MOVU      ACC,AL                ; [CPU_] |182| 
        LSL       ACC,2                 ; [CPU_] |182| 
        ADDL      XAR4,ACC              ; [CPU_] |182| 
        MOV       AL,@_EepromIndexesSize ; [CPU_] |182| 
        ADDB      AL,#-1                ; [CPU_] |182| 
        MOVU      ACC,AL                ; [CPU_] |182| 
        LSL       ACC,2                 ; [CPU_] |182| 
        ADDL      XAR5,ACC              ; [CPU_] |182| 
        MOV       AL,*+XAR5[0]          ; [CPU_] |182| 
        ADD       AL,*+XAR4[0]          ; [CPU_] |182| 
        MOV       *-SP[2],AL            ; [CPU_] |182| 
	.dwpsn	file "../param.c",line 183,column 37,is_stmt
        MOV       *-SP[19],#0           ; [CPU_] |183| 
	.dwpsn	file "../param.c",line 183,column 47,is_stmt
        MOVB      *-SP[20],#32,UNC      ; [CPU_] |183| 
	.dwpsn	file "../param.c",line 184,column 15,is_stmt
        MOVB      ACC,#0                ; [CPU_] |184| 
        MOVL      *-SP[24],ACC          ; [CPU_] |184| 
	.dwpsn	file "../param.c",line 186,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |186| 
        MOVZ      AR5,SP                ; [CPU_U] |186| 
        MOV       AL,#8195              ; [CPU_] |186| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |186| 
        SUBB      XAR5,#21              ; [CPU_U] |186| 
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$120, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |186| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |186| 
        CMPB      AL,#0                 ; [CPU_] |186| 
        BF        $C$L15,EQ             ; [CPU_] |186| 
        ; branchcc occurs ; [] |186| 
	.dwpsn	file "../param.c",line 187,column 5,is_stmt
        MOVU      ACC,*-SP[21]          ; [CPU_] |187| 
        MOVL      XAR4,#_ODI_EEPROM_INDEXES+3 ; [CPU_U] |187| 
        LSL       ACC,2                 ; [CPU_] |187| 
        ADDL      XAR4,ACC              ; [CPU_] |187| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |187| 
        LSR       AL,1                  ; [CPU_] |187| 
        MOV       *-SP[21],AL           ; [CPU_] |187| 
	.dwpsn	file "../param.c",line 188,column 3,is_stmt
        B         $C$L21,UNC            ; [CPU_] |188| 
        ; branch occurs ; [] |188| 
$C$L15:    
	.dwpsn	file "../param.c",line 190,column 5,is_stmt
        MOVB      *-SP[21],#1,UNC       ; [CPU_] |190| 
	.dwpsn	file "../param.c",line 192,column 3,is_stmt
        B         $C$L21,UNC            ; [CPU_] |192| 
        ; branch occurs ; [] |192| 
$C$L16:    
	.dwpsn	file "../param.c",line 193,column 5,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |193| 
        ADD       AL,*-SP[19]           ; [CPU_] |193| 
        CMP       AL,*-SP[2]            ; [CPU_] |193| 
        B         $C$L17,LOS            ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
	.dwpsn	file "../param.c",line 193,column 29,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |193| 
        SUB       AL,*-SP[19]           ; [CPU_] |193| 
        MOV       *-SP[20],AL           ; [CPU_] |193| 
$C$L17:    
	.dwpsn	file "../param.c",line 194,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |194| 
        MOVZ      AR5,*-SP[19]          ; [CPU_] |194| 
        MOV       AH,*-SP[20]           ; [CPU_] |194| 
        MOVB      AL,#1                 ; [CPU_] |194| 
        SUBB      XAR4,#18              ; [CPU_U] |194| 
$C$DW$121	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$121, DW_AT_low_pc(0x00)
	.dwattr $C$DW$121, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$121, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |194| 
        ; call occurs [#_I2C_Command] ; [] |194| 
        MOV       *-SP[25],AL           ; [CPU_] |194| 
	.dwpsn	file "../param.c",line 196,column 5,is_stmt
        MOV       AL,*-SP[19]           ; [CPU_] |196| 
        BF        $C$L18,NEQ            ; [CPU_] |196| 
        ; branchcc occurs ; [] |196| 
	.dwpsn	file "../param.c",line 197,column 7,is_stmt
        MOVZ      AR0,*-SP[21]          ; [CPU_] |197| 
        MOVZ      AR4,SP                ; [CPU_U] |197| 
        SUBB      XAR4,#18              ; [CPU_U] |197| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |197| 
	.dwpsn	file "../param.c",line 198,column 7,is_stmt
        MOVZ      AR0,*-SP[21]          ; [CPU_] |198| 
        MOVZ      AR4,SP                ; [CPU_U] |198| 
        SUBB      XAR4,#18              ; [CPU_U] |198| 
        ADDB      XAR0,#1               ; [CPU_] |198| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |198| 
$C$L18:    
	.dwpsn	file "../param.c",line 200,column 5,is_stmt
        MOV       AL,*-SP[25]           ; [CPU_] |200| 
        BF        $C$L19,EQ             ; [CPU_] |200| 
        ; branchcc occurs ; [] |200| 
	.dwpsn	file "../param.c",line 201,column 7,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |201| 
        MOVZ      AR4,SP                ; [CPU_U] |201| 
        MOV       *-SP[1],AL            ; [CPU_] |201| 
        MOVL      ACC,*-SP[24]          ; [CPU_] |201| 
        MOVB      XAR5,#0               ; [CPU_] |201| 
        SUBB      XAR4,#18              ; [CPU_U] |201| 
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_name("_getCRC32_cpu")
	.dwattr $C$DW$122, DW_AT_TI_call
        LCR       #_getCRC32_cpu        ; [CPU_] |201| 
        ; call occurs [#_getCRC32_cpu] ; [] |201| 
        MOVL      *-SP[24],ACC          ; [CPU_] |201| 
        B         $C$L20,UNC            ; [CPU_] |201| 
        ; branch occurs ; [] |201| 
$C$L19:    
	.dwpsn	file "../param.c",line 202,column 10,is_stmt
        MOVB      ACC,#0                ; [CPU_] |202| 
        B         $C$L22,UNC            ; [CPU_] |202| 
        ; branch occurs ; [] |202| 
$C$L20:    
	.dwpsn	file "../param.c",line 203,column 5,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |203| 
        ADD       *-SP[19],AL           ; [CPU_] |203| 
$C$L21:    
	.dwpsn	file "../param.c",line 192,column 9,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |192| 
        CMP       AL,*-SP[19]           ; [CPU_] |192| 
        B         $C$L16,HI             ; [CPU_] |192| 
        ; branchcc occurs ; [] |192| 
	.dwpsn	file "../param.c",line 205,column 3,is_stmt
        MOVL      ACC,*-SP[24]          ; [CPU_] |205| 
$C$L22:    
	.dwpsn	file "../param.c",line 206,column 1,is_stmt
        SUBB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$123	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$123, DW_AT_low_pc(0x00)
	.dwattr $C$DW$123, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$112, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$112, DW_AT_TI_end_line(0xce)
	.dwattr $C$DW$112, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$112

	.sect	".text"
	.global	_PAR_UpdateCode

$C$DW$124	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_UpdateCode")
	.dwattr $C$DW$124, DW_AT_low_pc(_PAR_UpdateCode)
	.dwattr $C$DW$124, DW_AT_high_pc(0x00)
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_PAR_UpdateCode")
	.dwattr $C$DW$124, DW_AT_external
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$124, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$124, DW_AT_TI_begin_line(0xdf)
	.dwattr $C$DW$124, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$124, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../param.c",line 224,column 1,is_stmt,address _PAR_UpdateCode

	.dwfde $C$DW$CIE, _PAR_UpdateCode
$C$DW$125	.dwtag  DW_TAG_formal_parameter, DW_AT_name("set")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_set")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$125, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PAR_UpdateCode               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_PAR_UpdateCode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$126	.dwtag  DW_TAG_variable, DW_AT_name("set")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_set")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_breg20 -1]
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[1],AL            ; [CPU_] |224| 
	.dwpsn	file "../param.c",line 225,column 17,is_stmt
        MOVB      ACC,#0                ; [CPU_] |225| 
        MOVL      *-SP[4],ACC           ; [CPU_] |225| 
	.dwpsn	file "../param.c",line 226,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |226| 
        BF        $C$L23,EQ             ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
	.dwpsn	file "../param.c",line 227,column 5,is_stmt
        MOV       AL,#12824             ; [CPU_] |227| 
        MOV       AH,#2258              ; [CPU_] |227| 
        MOVL      *-SP[4],ACC           ; [CPU_] |227| 
$C$L23:    
	.dwpsn	file "../param.c",line 228,column 3,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |228| 
        MOVB      AL,#0                 ; [CPU_] |228| 
        MOVB      AH,#4                 ; [CPU_] |228| 
        MOVL      XAR5,#8188            ; [CPU_] |228| 
        SUBB      XAR4,#4               ; [CPU_U] |228| 
$C$DW$128	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$128, DW_AT_low_pc(0x00)
	.dwattr $C$DW$128, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$128, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |228| 
        ; call occurs [#_I2C_Command] ; [] |228| 
	.dwpsn	file "../param.c",line 229,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$129	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$129, DW_AT_low_pc(0x00)
	.dwattr $C$DW$129, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$124, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$124, DW_AT_TI_end_line(0xe5)
	.dwattr $C$DW$124, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$124

	.sect	".text"
	.global	_PAR_SetDefaultParameters

$C$DW$130	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_SetDefaultParameters")
	.dwattr $C$DW$130, DW_AT_low_pc(_PAR_SetDefaultParameters)
	.dwattr $C$DW$130, DW_AT_high_pc(0x00)
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_PAR_SetDefaultParameters")
	.dwattr $C$DW$130, DW_AT_external
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$130, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$130, DW_AT_TI_begin_line(0xf6)
	.dwattr $C$DW$130, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$130, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../param.c",line 246,column 43,is_stmt,address _PAR_SetDefaultParameters

	.dwfde $C$DW$CIE, _PAR_SetDefaultParameters
$C$DW$131	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$131, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PAR_SetDefaultParameters     FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_PAR_SetDefaultParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$132	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$132, DW_AT_location[DW_OP_breg20 -2]
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_breg20 -3]
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_breg20 -4]
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("index_param")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_index_param")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_breg20 -5]
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_breg20 -8]
        MOVL      *-SP[2],XAR4          ; [CPU_] |246| 
	.dwpsn	file "../param.c",line 247,column 12,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |247| 
	.dwpsn	file "../param.c",line 248,column 13,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |248| 
	.dwpsn	file "../param.c",line 250,column 21,is_stmt
        MOVB      ACC,#0                ; [CPU_] |250| 
        MOVL      *-SP[8],ACC           ; [CPU_] |250| 
	.dwpsn	file "../param.c",line 252,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |252| 
        MOVB      AH,#0                 ; [CPU_] |252| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |252| 
        MOV       AL,#8194              ; [CPU_] |252| 
        SUBB      XAR5,#5               ; [CPU_U] |252| 
$C$DW$137	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$137, DW_AT_low_pc(0x00)
	.dwattr $C$DW$137, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$137, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |252| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |252| 
        CMPB      AL,#0                 ; [CPU_] |252| 
        BF        $C$L26,NEQ            ; [CPU_] |252| 
        ; branchcc occurs ; [] |252| 
	.dwpsn	file "../param.c",line 253,column 5,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |253| 
	.dwpsn	file "../param.c",line 256,column 3,is_stmt
        B         $C$L26,UNC            ; [CPU_] |256| 
        ; branch occurs ; [] |256| 
$C$L24:    
	.dwpsn	file "../param.c",line 257,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |257| 
        CMP       AL,*-SP[3]            ; [CPU_] |257| 
        BF        $C$L25,NEQ            ; [CPU_] |257| 
        ; branchcc occurs ; [] |257| 
	.dwpsn	file "../param.c",line 257,column 27,is_stmt
        INC       *-SP[3]               ; [CPU_] |257| 
$C$L25:    
	.dwpsn	file "../param.c",line 258,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |258| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |258| 
        INC       *-SP[3]               ; [CPU_] |258| 
$C$DW$138	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$138, DW_AT_low_pc(0x00)
	.dwattr $C$DW$138, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$138, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |258| 
        ; call occurs [#_WritePermanentParam] ; [] |258| 
        MOVL      *-SP[8],ACC           ; [CPU_] |258| 
$C$L26:    
	.dwpsn	file "../param.c",line 256,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |256| 
        MOVL      XAR0,#304             ; [CPU_] |256| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |256| 
        CMP       AL,*-SP[3]            ; [CPU_] |256| 
        B         $C$L27,LOS            ; [CPU_] |256| 
        ; branchcc occurs ; [] |256| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |256| 
        BF        $C$L24,EQ             ; [CPU_] |256| 
        ; branchcc occurs ; [] |256| 
$C$L27:    
	.dwpsn	file "../param.c",line 260,column 3,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |260| 
        BF        $C$L29,NEQ            ; [CPU_] |260| 
        ; branchcc occurs ; [] |260| 
	.dwpsn	file "../param.c",line 261,column 5,is_stmt
        MOV       AL,#-1                ; [CPU_] |261| 
$C$DW$139	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$139, DW_AT_low_pc(0x00)
	.dwattr $C$DW$139, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$139, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |261| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |261| 
	.dwpsn	file "../param.c",line 262,column 5,is_stmt
        MOVB      AL,#1                 ; [CPU_] |262| 
$C$DW$140	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$140, DW_AT_low_pc(0x00)
	.dwattr $C$DW$140, DW_AT_name("_PAR_UpdateCode")
	.dwattr $C$DW$140, DW_AT_TI_call
        LCR       #_PAR_UpdateCode      ; [CPU_] |262| 
        ; call occurs [#_PAR_UpdateCode] ; [] |262| 
        MOV       *-SP[4],AL            ; [CPU_] |262| 
	.dwpsn	file "../param.c",line 263,column 5,is_stmt
        CMPB      AL,#0                 ; [CPU_] |263| 
        BF        $C$L29,EQ             ; [CPU_] |263| 
        ; branchcc occurs ; [] |263| 
	.dwpsn	file "../param.c",line 264,column 7,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |264| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |264| 
$C$DW$141	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$141, DW_AT_low_pc(0x00)
	.dwattr $C$DW$141, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$141, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |264| 
        ; call occurs [#_WritePermanentParam] ; [] |264| 
        MOVL      *-SP[8],ACC           ; [CPU_] |264| 
	.dwpsn	file "../param.c",line 265,column 7,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |265| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |265| 
        BF        $C$L28,NEQ            ; [CPU_] |265| 
        ; branchcc occurs ; [] |265| 
        MOVB      XAR6,#1               ; [CPU_] |265| 
$C$L28:    
        MOV       *-SP[4],AR6           ; [CPU_] |265| 
$C$L29:    
	.dwpsn	file "../param.c",line 268,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |268| 
        BF        $C$L32,EQ             ; [CPU_] |268| 
        ; branchcc occurs ; [] |268| 
	.dwpsn	file "../param.c",line 269,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |269| 
        MOVB      AH,#0                 ; [CPU_] |269| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |269| 
        MOV       AL,#8195              ; [CPU_] |269| 
        SUBB      XAR5,#5               ; [CPU_U] |269| 
$C$DW$142	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$142, DW_AT_low_pc(0x00)
	.dwattr $C$DW$142, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$142, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |269| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |269| 
        CMPB      AL,#0                 ; [CPU_] |269| 
        BF        $C$L30,NEQ            ; [CPU_] |269| 
        ; branchcc occurs ; [] |269| 
	.dwpsn	file "../param.c",line 269,column 91,is_stmt
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |269| 
$C$L30:    
	.dwpsn	file "../param.c",line 270,column 5,is_stmt
$C$DW$143	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$143, DW_AT_low_pc(0x00)
	.dwattr $C$DW$143, DW_AT_name("_ComputeParamCRC")
	.dwattr $C$DW$143, DW_AT_TI_call
        LCR       #_ComputeParamCRC     ; [CPU_] |270| 
        ; call occurs [#_ComputeParamCRC] ; [] |270| 
        MOVW      DP,#_ODP_CrcParameters ; [CPU_U] 
        MOVL      @_ODP_CrcParameters,ACC ; [CPU_] |270| 
	.dwpsn	file "../param.c",line 271,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |271| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |271| 
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$144, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |271| 
        ; call occurs [#_WritePermanentParam] ; [] |271| 
        MOVL      *-SP[8],ACC           ; [CPU_] |271| 
	.dwpsn	file "../param.c",line 272,column 5,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |272| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |272| 
        BF        $C$L31,NEQ            ; [CPU_] |272| 
        ; branchcc occurs ; [] |272| 
        MOVB      XAR6,#1               ; [CPU_] |272| 
$C$L31:    
        MOV       *-SP[4],AR6           ; [CPU_] |272| 
$C$L32:    
	.dwpsn	file "../param.c",line 274,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |274| 
	.dwpsn	file "../param.c",line 275,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$145	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$145, DW_AT_low_pc(0x00)
	.dwattr $C$DW$145, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$130, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$130, DW_AT_TI_end_line(0x113)
	.dwattr $C$DW$130, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$130

	.sect	".text"
	.global	_PAR_InitParam

$C$DW$146	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_InitParam")
	.dwattr $C$DW$146, DW_AT_low_pc(_PAR_InitParam)
	.dwattr $C$DW$146, DW_AT_high_pc(0x00)
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_PAR_InitParam")
	.dwattr $C$DW$146, DW_AT_external
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$146, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$146, DW_AT_TI_begin_line(0x127)
	.dwattr $C$DW$146, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$146, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../param.c",line 296,column 1,is_stmt,address _PAR_InitParam

	.dwfde $C$DW$CIE, _PAR_InitParam
$C$DW$147	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PAR_InitParam                FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_PAR_InitParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$148	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$148, DW_AT_location[DW_OP_breg20 -2]
$C$DW$149	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$149, DW_AT_location[DW_OP_breg20 -4]
$C$DW$150	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$150, DW_AT_location[DW_OP_breg20 -5]
$C$DW$151	.dwtag  DW_TAG_variable, DW_AT_name("index_param")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_index_param")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$151, DW_AT_location[DW_OP_breg20 -6]
$C$DW$152	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$152, DW_AT_location[DW_OP_breg20 -10]
        MOVL      *-SP[2],XAR4          ; [CPU_] |296| 
	.dwpsn	file "../param.c",line 297,column 17,is_stmt
        MOVB      ACC,#0                ; [CPU_] |297| 
        MOVL      *-SP[4],ACC           ; [CPU_] |297| 
	.dwpsn	file "../param.c",line 302,column 3,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |302| 
        MOVB      AL,#1                 ; [CPU_] |302| 
        MOVB      AH,#4                 ; [CPU_] |302| 
        MOVL      XAR5,#8188            ; [CPU_] |302| 
        SUBB      XAR4,#4               ; [CPU_U] |302| 
$C$DW$153	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$153, DW_AT_low_pc(0x00)
	.dwattr $C$DW$153, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$153, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |302| 
        ; call occurs [#_I2C_Command] ; [] |302| 
        MOV       *-SP[5],AL            ; [CPU_] |302| 
	.dwpsn	file "../param.c",line 303,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |303| 
        BF        $C$L33,NEQ            ; [CPU_] |303| 
        ; branchcc occurs ; [] |303| 
	.dwpsn	file "../param.c",line 304,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |304| 
        MOVB      AL,#2                 ; [CPU_] |304| 
$C$DW$154	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$154, DW_AT_low_pc(0x00)
	.dwattr $C$DW$154, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$154, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |304| 
        ; call occurs [#_SEM_pend] ; [] |304| 
	.dwpsn	file "../param.c",line 305,column 5,is_stmt
        MOVB      AL,#1                 ; [CPU_] |305| 
        MOVB      AH,#4                 ; [CPU_] |305| 
        MOVZ      AR4,SP                ; [CPU_U] |305| 
        MOVL      XAR5,#8188            ; [CPU_] |305| 
        SUBB      XAR4,#4               ; [CPU_U] |305| 
$C$DW$155	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$155, DW_AT_low_pc(0x00)
	.dwattr $C$DW$155, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$155, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |305| 
        ; call occurs [#_I2C_Command] ; [] |305| 
        MOV       *-SP[5],AL            ; [CPU_] |305| 
$C$L33:    
	.dwpsn	file "../param.c",line 307,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |307| 
        BF        $C$L41,EQ             ; [CPU_] |307| 
        ; branchcc occurs ; [] |307| 
	.dwpsn	file "../param.c",line 308,column 5,is_stmt
        MOV       AL,#12824             ; [CPU_] |308| 
        MOV       AH,#2258              ; [CPU_] |308| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |308| 
        BF        $C$L40,NEQ            ; [CPU_] |308| 
        ; branchcc occurs ; [] |308| 
	.dwpsn	file "../param.c",line 309,column 7,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |309| 
        MOVB      AL,#2                 ; [CPU_] |309| 
$C$DW$156	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$156, DW_AT_low_pc(0x00)
	.dwattr $C$DW$156, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$156, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |309| 
        ; call occurs [#_SEM_pend] ; [] |309| 
	.dwpsn	file "../param.c",line 310,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |310| 
        MOVL      *-SP[4],ACC           ; [CPU_] |310| 
	.dwpsn	file "../param.c",line 311,column 7,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |311| 
        MOVB      AH,#0                 ; [CPU_] |311| 
        MOV       AL,#8194              ; [CPU_] |311| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |311| 
        SUBB      XAR5,#6               ; [CPU_U] |311| 
$C$DW$157	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$157, DW_AT_low_pc(0x00)
	.dwattr $C$DW$157, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$157, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |311| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |311| 
        CMPB      AL,#0                 ; [CPU_] |311| 
        BF        $C$L34,NEQ            ; [CPU_] |311| 
        ; branchcc occurs ; [] |311| 
	.dwpsn	file "../param.c",line 312,column 9,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |312| 
$C$L34:    
	.dwpsn	file "../param.c",line 314,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |314| 
        MOVL      XAR0,#302             ; [CPU_] |314| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |314| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |314| 
        LSL       ACC,2                 ; [CPU_] |314| 
        MOVZ      AR4,SP                ; [CPU_U] |314| 
        SUBB      XAR4,#10              ; [CPU_U] |314| 
        ADDL      XAR7,ACC              ; [CPU_] |314| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |314| 
	.dwpsn	file "../param.c",line 315,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |315| 
        MOVZ      AR5,*-SP[7]           ; [CPU_] |315| 
        MOV       AH,*-SP[8]            ; [CPU_] |315| 
        MOVB      AL,#1                 ; [CPU_] |315| 
        SUBB      XAR4,#4               ; [CPU_U] |315| 
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$158, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |315| 
        ; call occurs [#_I2C_Command] ; [] |315| 
        MOV       *-SP[5],AL            ; [CPU_] |315| 
	.dwpsn	file "../param.c",line 316,column 7,is_stmt
        CMPB      AL,#0                 ; [CPU_] |316| 
        BF        $C$L41,EQ             ; [CPU_] |316| 
        ; branchcc occurs ; [] |316| 
	.dwpsn	file "../param.c",line 317,column 9,is_stmt
        MOVW      DP,#_ODP_VersionParameters ; [CPU_U] 
        MOVU      ACC,@_ODP_VersionParameters ; [CPU_] |317| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |317| 
        B         $C$L36,HI             ; [CPU_] |317| 
        ; branchcc occurs ; [] |317| 
	.dwpsn	file "../param.c",line 318,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |318| 
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_name("_PAR_ReadAllPermanentParam")
	.dwattr $C$DW$159, DW_AT_TI_call
        LCR       #_PAR_ReadAllPermanentParam ; [CPU_] |318| 
        ; call occurs [#_PAR_ReadAllPermanentParam] ; [] |318| 
        MOV       *-SP[5],AL            ; [CPU_] |318| 
	.dwpsn	file "../param.c",line 319,column 11,is_stmt
        CMPB      AL,#0                 ; [CPU_] |319| 
        BF        $C$L35,EQ             ; [CPU_] |319| 
        ; branchcc occurs ; [] |319| 
$C$DW$160	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$160, DW_AT_low_pc(0x00)
	.dwattr $C$DW$160, DW_AT_name("_ComputeParamCRC")
	.dwattr $C$DW$160, DW_AT_TI_call
        LCR       #_ComputeParamCRC     ; [CPU_] |319| 
        ; call occurs [#_ComputeParamCRC] ; [] |319| 
        MOVW      DP,#_ODP_CrcParameters ; [CPU_U] 
        CMPL      ACC,@_ODP_CrcParameters ; [CPU_] |319| 
        BF        $C$L35,NEQ            ; [CPU_] |319| 
        ; branchcc occurs ; [] |319| 
	.dwpsn	file "../param.c",line 320,column 13,is_stmt
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_name("_PAR_ReadAllStatisticParam")
	.dwattr $C$DW$161, DW_AT_TI_call
        LCR       #_PAR_ReadAllStatisticParam ; [CPU_] |320| 
        ; call occurs [#_PAR_ReadAllStatisticParam] ; [] |320| 
        MOV       *-SP[5],AL            ; [CPU_] |320| 
        B         $C$L41,UNC            ; [CPU_] |320| 
        ; branch occurs ; [] |320| 
$C$L35:    
	.dwpsn	file "../param.c",line 322,column 13,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |322| 
	.dwpsn	file "../param.c",line 325,column 9,is_stmt
        B         $C$L41,UNC            ; [CPU_] |325| 
        ; branch occurs ; [] |325| 
$C$L36:    
	.dwpsn	file "../param.c",line 327,column 11,is_stmt
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_PAR_ReadAllStatisticParam")
	.dwattr $C$DW$162, DW_AT_TI_call
        LCR       #_PAR_ReadAllStatisticParam ; [CPU_] |327| 
        ; call occurs [#_PAR_ReadAllStatisticParam] ; [] |327| 
	.dwpsn	file "../param.c",line 328,column 11,is_stmt
        MOVB      ACC,#128              ; [CPU_] |328| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |328| 
        B         $C$L37,HI             ; [CPU_] |328| 
        ; branchcc occurs ; [] |328| 
	.dwpsn	file "../param.c",line 329,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |329| 
        MOVL      XAR5,#_IndexesOldId+8 ; [CPU_U] |329| 
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$163, DW_AT_TI_call
        LCR       #_PAR_ReadPermanentParam ; [CPU_] |329| 
        ; call occurs [#_PAR_ReadPermanentParam] ; [] |329| 
	.dwpsn	file "../param.c",line 330,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |330| 
        MOVL      XAR5,#_IndexesOldRate+8 ; [CPU_U] |330| 
$C$DW$164	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$164, DW_AT_low_pc(0x00)
	.dwattr $C$DW$164, DW_AT_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$164, DW_AT_TI_call
        LCR       #_PAR_ReadPermanentParam ; [CPU_] |330| 
        ; call occurs [#_PAR_ReadPermanentParam] ; [] |330| 
	.dwpsn	file "../param.c",line 331,column 11,is_stmt
        B         $C$L39,UNC            ; [CPU_] |331| 
        ; branch occurs ; [] |331| 
$C$L37:    
	.dwpsn	file "../param.c",line 332,column 16,is_stmt
        MOVB      ACC,#124              ; [CPU_] |332| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |332| 
        B         $C$L38,HI             ; [CPU_] |332| 
        ; branchcc occurs ; [] |332| 
	.dwpsn	file "../param.c",line 333,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |333| 
        MOVL      XAR5,#_IndexesOldId   ; [CPU_U] |333| 
$C$DW$165	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$165, DW_AT_low_pc(0x00)
	.dwattr $C$DW$165, DW_AT_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$165, DW_AT_TI_call
        LCR       #_PAR_ReadPermanentParam ; [CPU_] |333| 
        ; call occurs [#_PAR_ReadPermanentParam] ; [] |333| 
	.dwpsn	file "../param.c",line 334,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |334| 
        MOVL      XAR5,#_IndexesOldRate ; [CPU_U] |334| 
$C$DW$166	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$166, DW_AT_low_pc(0x00)
	.dwattr $C$DW$166, DW_AT_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$166, DW_AT_TI_call
        LCR       #_PAR_ReadPermanentParam ; [CPU_] |334| 
        ; call occurs [#_PAR_ReadPermanentParam] ; [] |334| 
	.dwpsn	file "../param.c",line 335,column 11,is_stmt
        B         $C$L39,UNC            ; [CPU_] |335| 
        ; branch occurs ; [] |335| 
$C$L38:    
	.dwpsn	file "../param.c",line 337,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |337| 
        MOVL      XAR5,#_IndexesOldId+4 ; [CPU_U] |337| 
$C$DW$167	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$167, DW_AT_low_pc(0x00)
	.dwattr $C$DW$167, DW_AT_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$167, DW_AT_TI_call
        LCR       #_PAR_ReadPermanentParam ; [CPU_] |337| 
        ; call occurs [#_PAR_ReadPermanentParam] ; [] |337| 
	.dwpsn	file "../param.c",line 338,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |338| 
        MOVL      XAR5,#_IndexesOldRate+4 ; [CPU_U] |338| 
$C$DW$168	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$168, DW_AT_low_pc(0x00)
	.dwattr $C$DW$168, DW_AT_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$168, DW_AT_TI_call
        LCR       #_PAR_ReadPermanentParam ; [CPU_] |338| 
        ; call occurs [#_PAR_ReadPermanentParam] ; [] |338| 
$C$L39:    
	.dwpsn	file "../param.c",line 341,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |341| 
$C$DW$169	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$169, DW_AT_low_pc(0x00)
	.dwattr $C$DW$169, DW_AT_name("_PAR_SetDefaultParameters")
	.dwattr $C$DW$169, DW_AT_TI_call
        LCR       #_PAR_SetDefaultParameters ; [CPU_] |341| 
        ; call occurs [#_PAR_SetDefaultParameters] ; [] |341| 
        MOV       *-SP[5],AL            ; [CPU_] |341| 
	.dwpsn	file "../param.c",line 344,column 5,is_stmt
        B         $C$L41,UNC            ; [CPU_] |344| 
        ; branch occurs ; [] |344| 
$C$L40:    
	.dwpsn	file "../param.c",line 346,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |346| 
$C$DW$170	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$170, DW_AT_low_pc(0x00)
	.dwattr $C$DW$170, DW_AT_name("_PAR_SetDefaultParameters")
	.dwattr $C$DW$170, DW_AT_TI_call
        LCR       #_PAR_SetDefaultParameters ; [CPU_] |346| 
        ; call occurs [#_PAR_SetDefaultParameters] ; [] |346| 
        MOV       *-SP[5],AL            ; [CPU_] |346| 
$C$L41:    
	.dwpsn	file "../param.c",line 349,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |349| 
	.dwpsn	file "../param.c",line 350,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$171	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$171, DW_AT_low_pc(0x00)
	.dwattr $C$DW$171, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$146, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$146, DW_AT_TI_end_line(0x15e)
	.dwattr $C$DW$146, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$146

	.sect	".text"
	.global	_PAR_ReadPermanentParam

$C$DW$172	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_ReadPermanentParam")
	.dwattr $C$DW$172, DW_AT_low_pc(_PAR_ReadPermanentParam)
	.dwattr $C$DW$172, DW_AT_high_pc(0x00)
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$172, DW_AT_external
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$172, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$172, DW_AT_TI_begin_line(0x172)
	.dwattr $C$DW$172, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$172, DW_AT_TI_max_frame_size(-24)
	.dwpsn	file "../param.c",line 371,column 1,is_stmt,address _PAR_ReadPermanentParam

	.dwfde $C$DW$CIE, _PAR_ReadPermanentParam
$C$DW$173	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$173, DW_AT_location[DW_OP_reg12]
$C$DW$174	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indexes")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$174, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _PAR_ReadPermanentParam       FR SIZE:  22           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            5 Parameter, 17 Auto,  0 SOE     *
;***************************************************************

_PAR_ReadPermanentParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#22                ; [CPU_U] 
	.dwcfi	cfa_offset, -24
$C$DW$175	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$175, DW_AT_location[DW_OP_breg20 -8]
$C$DW$176	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_breg20 -10]
$C$DW$177	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$177, DW_AT_location[DW_OP_breg20 -14]
$C$DW$178	.dwtag  DW_TAG_variable, DW_AT_name("datap")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$178, DW_AT_location[DW_OP_breg20 -16]
$C$DW$179	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$179, DW_AT_location[DW_OP_breg20 -18]
$C$DW$180	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$180, DW_AT_location[DW_OP_breg20 -20]
$C$DW$181	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$181, DW_AT_location[DW_OP_breg20 -21]
$C$DW$182	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$182, DW_AT_location[DW_OP_breg20 -22]
        MOVL      *-SP[10],XAR5         ; [CPU_] |371| 
        MOVL      XAR7,*-SP[10]         ; [CPU_] |371| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |371| 
        MOVZ      AR4,SP                ; [CPU_U] |371| 
        SUBB      XAR4,#14              ; [CPU_U] |371| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |371| 
	.dwpsn	file "../param.c",line 380,column 3,is_stmt
        MOVU      ACC,*-SP[12]          ; [CPU_] |380| 
        MOVL      *-SP[20],ACC          ; [CPU_] |380| 
	.dwpsn	file "../param.c",line 381,column 3,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |381| 
        SUBB      XAR4,#20              ; [CPU_U] |381| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |381| 
        MOV       *-SP[3],#0            ; [CPU_] |381| 
        MOVB      *-SP[4],#1,UNC        ; [CPU_] |381| 
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |381| 
        MOV       AH,*-SP[13]           ; [CPU_] |381| 
        MOV       AL,*-SP[14]           ; [CPU_] |381| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |381| 
        MOVZ      AR5,SP                ; [CPU_U] |381| 
        SUBB      XAR5,#16              ; [CPU_U] |381| 
$C$DW$183	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$183, DW_AT_low_pc(0x00)
	.dwattr $C$DW$183, DW_AT_name("__setODentry")
	.dwattr $C$DW$183, DW_AT_TI_call
        LCR       #__setODentry         ; [CPU_] |381| 
        ; call occurs [#__setODentry] ; [] |381| 
        MOVL      *-SP[18],ACC          ; [CPU_] |381| 
	.dwpsn	file "../param.c",line 382,column 3,is_stmt
        MOVL      ACC,*-SP[18]          ; [CPU_] |382| 
        BF        $C$L47,NEQ            ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
	.dwpsn	file "../param.c",line 383,column 5,is_stmt
        MOV       *-SP[21],#0           ; [CPU_] |383| 
	.dwpsn	file "../param.c",line 384,column 5,is_stmt
        MOV       *-SP[22],#0           ; [CPU_] |384| 
	.dwpsn	file "../param.c",line 385,column 5,is_stmt
        B         $C$L44,UNC            ; [CPU_] |385| 
        ; branch occurs ; [] |385| 
$C$L42:    
	.dwpsn	file "../param.c",line 386,column 7,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |386| 
        MOVZ      AR5,*-SP[11]          ; [CPU_] |386| 
        MOV       AH,*-SP[12]           ; [CPU_] |386| 
        MOVB      AL,#1                 ; [CPU_] |386| 
$C$DW$184	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$184, DW_AT_low_pc(0x00)
	.dwattr $C$DW$184, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$184, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |386| 
        ; call occurs [#_I2C_Command] ; [] |386| 
        MOV       *-SP[22],AL           ; [CPU_] |386| 
	.dwpsn	file "../param.c",line 387,column 7,is_stmt
        CMPB      AL,#0                 ; [CPU_] |387| 
        BF        $C$L43,NEQ            ; [CPU_] |387| 
        ; branchcc occurs ; [] |387| 
	.dwpsn	file "../param.c",line 388,column 9,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |388| 
        MOVB      AL,#30                ; [CPU_] |388| 
$C$DW$185	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$185, DW_AT_low_pc(0x00)
	.dwattr $C$DW$185, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$185, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |388| 
        ; call occurs [#_SEM_pend] ; [] |388| 
$C$L43:    
	.dwpsn	file "../param.c",line 390,column 7,is_stmt
        INC       *-SP[21]              ; [CPU_] |390| 
$C$L44:    
	.dwpsn	file "../param.c",line 385,column 12,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |385| 
        CMPB      AL,#3                 ; [CPU_] |385| 
        B         $C$L45,HIS            ; [CPU_] |385| 
        ; branchcc occurs ; [] |385| 
        MOV       AL,*-SP[22]           ; [CPU_] |385| 
        BF        $C$L42,EQ             ; [CPU_] |385| 
        ; branchcc occurs ; [] |385| 
$C$L45:    
	.dwpsn	file "../param.c",line 392,column 5,is_stmt
        MOV       AL,*-SP[22]           ; [CPU_] |392| 
        BF        $C$L46,NEQ            ; [CPU_] |392| 
        ; branchcc occurs ; [] |392| 
	.dwpsn	file "../param.c",line 393,column 7,is_stmt
        MOV       AL,#32                ; [CPU_] |393| 
        MOV       AH,#2048              ; [CPU_] |393| 
        MOVL      *-SP[18],ACC          ; [CPU_] |393| 
        B         $C$L47,UNC            ; [CPU_] |393| 
        ; branch occurs ; [] |393| 
$C$L46:    
	.dwpsn	file "../param.c",line 395,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |395| 
        SUBB      XAR4,#20              ; [CPU_U] |395| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |395| 
        MOV       *-SP[3],#0            ; [CPU_] |395| 
        MOVB      *-SP[4],#1,UNC        ; [CPU_] |395| 
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |395| 
        MOV       AH,*-SP[13]           ; [CPU_] |395| 
        MOV       AL,*-SP[14]           ; [CPU_] |395| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |395| 
        MOVZ      AR5,SP                ; [CPU_U] |395| 
        SUBB      XAR5,#16              ; [CPU_U] |395| 
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("__setODentry")
	.dwattr $C$DW$186, DW_AT_TI_call
        LCR       #__setODentry         ; [CPU_] |395| 
        ; call occurs [#__setODentry] ; [] |395| 
        MOVL      *-SP[18],ACC          ; [CPU_] |395| 
$C$L47:    
	.dwpsn	file "../param.c",line 398,column 3,is_stmt
        MOVL      ACC,*-SP[18]          ; [CPU_] |398| 
	.dwpsn	file "../param.c",line 399,column 1,is_stmt
        SUBB      SP,#22                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$172, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$172, DW_AT_TI_end_line(0x18f)
	.dwattr $C$DW$172, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$172

	.sect	".text"
	.global	_PAR_ReadAllPermanentParam

$C$DW$188	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_ReadAllPermanentParam")
	.dwattr $C$DW$188, DW_AT_low_pc(_PAR_ReadAllPermanentParam)
	.dwattr $C$DW$188, DW_AT_high_pc(0x00)
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_PAR_ReadAllPermanentParam")
	.dwattr $C$DW$188, DW_AT_external
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$188, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$188, DW_AT_TI_begin_line(0x1a0)
	.dwattr $C$DW$188, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$188, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../param.c",line 417,column 1,is_stmt,address _PAR_ReadAllPermanentParam

	.dwfde $C$DW$CIE, _PAR_ReadAllPermanentParam
$C$DW$189	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$189, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PAR_ReadAllPermanentParam    FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_PAR_ReadAllPermanentParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$190	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$190, DW_AT_location[DW_OP_breg20 -2]
$C$DW$191	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$191, DW_AT_location[DW_OP_breg20 -3]
$C$DW$192	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$192, DW_AT_location[DW_OP_breg20 -8]
$C$DW$193	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$193, DW_AT_location[DW_OP_breg20 -9]
$C$DW$194	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_breg20 -12]
        MOVL      *-SP[2],XAR4          ; [CPU_] |417| 
	.dwpsn	file "../param.c",line 418,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |418| 
        MOVL      XAR0,#304             ; [CPU_] |418| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |418| 
        MOV       *-SP[3],AL            ; [CPU_] |418| 
	.dwpsn	file "../param.c",line 420,column 13,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |420| 
	.dwpsn	file "../param.c",line 421,column 21,is_stmt
        MOVB      ACC,#0                ; [CPU_] |421| 
        MOVL      *-SP[12],ACC          ; [CPU_] |421| 
	.dwpsn	file "../param.c",line 422,column 3,is_stmt
        B         $C$L49,UNC            ; [CPU_] |422| 
        ; branch occurs ; [] |422| 
$C$L48:    
	.dwpsn	file "../param.c",line 423,column 5,is_stmt
        MOV       AH,*-SP[9]            ; [CPU_] |423| 
        MOVB      AL,#1                 ; [CPU_] |423| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |423| 
        MOVL      XAR0,#302             ; [CPU_] |423| 
        SETC      SXM                   ; [CPU_] 
        ADD       AL,AH                 ; [CPU_] |423| 
        MOV       *-SP[9],AL            ; [CPU_] |423| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |423| 
        MOV       ACC,AH << 2           ; [CPU_] |423| 
        MOVZ      AR4,SP                ; [CPU_U] |423| 
        SUBB      XAR4,#8               ; [CPU_U] |423| 
        ADDL      XAR7,ACC              ; [CPU_] |423| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |423| 
	.dwpsn	file "../param.c",line 424,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |424| 
        MOVZ      AR5,SP                ; [CPU_U] |424| 
        SUBB      XAR5,#8               ; [CPU_U] |424| 
$C$DW$195	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$195, DW_AT_low_pc(0x00)
	.dwattr $C$DW$195, DW_AT_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$195, DW_AT_TI_call
        LCR       #_PAR_ReadPermanentParam ; [CPU_] |424| 
        ; call occurs [#_PAR_ReadPermanentParam] ; [] |424| 
        MOVL      *-SP[12],ACC          ; [CPU_] |424| 
$C$L49:    
	.dwpsn	file "../param.c",line 422,column 9,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |422| 
        CMP       AL,*-SP[9]            ; [CPU_] |422| 
        B         $C$L50,LEQ            ; [CPU_] |422| 
        ; branchcc occurs ; [] |422| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |422| 
        BF        $C$L48,EQ             ; [CPU_] |422| 
        ; branchcc occurs ; [] |422| 
$C$L50:    
	.dwpsn	file "../param.c",line 426,column 3,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |426| 
        MOVB      XAR6,#0               ; [CPU_] |426| 
        BF        $C$L51,NEQ            ; [CPU_] |426| 
        ; branchcc occurs ; [] |426| 
        MOVB      XAR6,#1               ; [CPU_] |426| 
$C$L51:    
        MOV       AL,AR6                ; [CPU_] |426| 
	.dwpsn	file "../param.c",line 427,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$188, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$188, DW_AT_TI_end_line(0x1ab)
	.dwattr $C$DW$188, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$188

	.sect	".text"
	.global	_WritePermanentParam

$C$DW$197	.dwtag  DW_TAG_subprogram, DW_AT_name("WritePermanentParam")
	.dwattr $C$DW$197, DW_AT_low_pc(_WritePermanentParam)
	.dwattr $C$DW$197, DW_AT_high_pc(0x00)
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_WritePermanentParam")
	.dwattr $C$DW$197, DW_AT_external
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$197, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$197, DW_AT_TI_begin_line(0x1bf)
	.dwattr $C$DW$197, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$197, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "../param.c",line 448,column 1,is_stmt,address _WritePermanentParam

	.dwfde $C$DW$CIE, _WritePermanentParam
$C$DW$198	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg12]
$C$DW$199	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _WritePermanentParam          FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            7 Parameter, 17 Auto,  0 SOE     *
;***************************************************************

_WritePermanentParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
$C$DW$200	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$200, DW_AT_location[DW_OP_breg20 -10]
$C$DW$201	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$201, DW_AT_location[DW_OP_breg20 -11]
$C$DW$202	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$202, DW_AT_location[DW_OP_breg20 -16]
$C$DW$203	.dwtag  DW_TAG_variable, DW_AT_name("datap")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$203, DW_AT_location[DW_OP_breg20 -18]
$C$DW$204	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$204, DW_AT_location[DW_OP_breg20 -20]
$C$DW$205	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$205, DW_AT_location[DW_OP_breg20 -22]
$C$DW$206	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$206, DW_AT_location[DW_OP_breg20 -23]
$C$DW$207	.dwtag  DW_TAG_variable, DW_AT_name("data_type")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_data_type")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_breg20 -24]
        MOV       *-SP[11],AL           ; [CPU_] |448| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |448| 
	.dwpsn	file "../param.c",line 456,column 3,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |456| 
        MOVL      XAR0,#304             ; [CPU_] |456| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |456| 
        CMP       AL,*-SP[11]           ; [CPU_] |456| 
        B         $C$L52,LOS            ; [CPU_] |456| 
        ; branchcc occurs ; [] |456| 
	.dwpsn	file "../param.c",line 457,column 5,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |457| 
        MOVL      XAR0,#302             ; [CPU_] |457| 
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |457| 
        MOV       ACC,*-SP[11] << 2     ; [CPU_] |457| 
        MOVZ      AR4,SP                ; [CPU_U] |457| 
        SUBB      XAR4,#16              ; [CPU_U] |457| 
        ADDL      XAR7,ACC              ; [CPU_] |457| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |457| 
	.dwpsn	file "../param.c",line 458,column 5,is_stmt
        MOVU      ACC,*-SP[14]          ; [CPU_] |458| 
        MOVL      *-SP[20],ACC          ; [CPU_] |458| 
	.dwpsn	file "../param.c",line 459,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |459| 
        MOVZ      AR4,SP                ; [CPU_U] |459| 
        SUBB      XAR5,#20              ; [CPU_U] |459| 
        SUBB      XAR4,#24              ; [CPU_U] |459| 
        MOVU      ACC,AR5               ; [CPU_] |459| 
        MOVL      *-SP[2],ACC           ; [CPU_] |459| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |459| 
        MOV       *-SP[5],#0            ; [CPU_] |459| 
        MOVB      *-SP[6],#1,UNC        ; [CPU_] |459| 
        MOVB      *-SP[7],#4,UNC        ; [CPU_] |459| 
        MOV       AH,*-SP[15]           ; [CPU_] |459| 
        MOV       AL,*-SP[16]           ; [CPU_] |459| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |459| 
        MOVZ      AR5,SP                ; [CPU_U] |459| 
        SUBB      XAR5,#18              ; [CPU_U] |459| 
$C$DW$208	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$208, DW_AT_low_pc(0x00)
	.dwattr $C$DW$208, DW_AT_name("__getODentry")
	.dwattr $C$DW$208, DW_AT_TI_call
        LCR       #__getODentry         ; [CPU_] |459| 
        ; call occurs [#__getODentry] ; [] |459| 
        MOVL      *-SP[22],ACC          ; [CPU_] |459| 
	.dwpsn	file "../param.c",line 461,column 5,is_stmt
        MOVL      ACC,*-SP[22]          ; [CPU_] |461| 
        BF        $C$L53,NEQ            ; [CPU_] |461| 
        ; branchcc occurs ; [] |461| 
	.dwpsn	file "../param.c",line 462,column 7,is_stmt
        MOVL      XAR4,*-SP[18]         ; [CPU_] |462| 
        MOVZ      AR5,*-SP[13]          ; [CPU_] |462| 
        MOV       AH,*-SP[14]           ; [CPU_] |462| 
        MOVB      AL,#0                 ; [CPU_] |462| 
$C$DW$209	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$209, DW_AT_low_pc(0x00)
	.dwattr $C$DW$209, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$209, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |462| 
        ; call occurs [#_I2C_Command] ; [] |462| 
        MOV       *-SP[23],AL           ; [CPU_] |462| 
	.dwpsn	file "../param.c",line 463,column 7,is_stmt
        CMPB      AL,#0                 ; [CPU_] |463| 
        BF        $C$L53,NEQ            ; [CPU_] |463| 
        ; branchcc occurs ; [] |463| 
	.dwpsn	file "../param.c",line 464,column 9,is_stmt
        MOV       AL,#32                ; [CPU_] |464| 
        MOV       AH,#2048              ; [CPU_] |464| 
        MOVL      *-SP[22],ACC          ; [CPU_] |464| 
	.dwpsn	file "../param.c",line 466,column 3,is_stmt
        B         $C$L53,UNC            ; [CPU_] |466| 
        ; branch occurs ; [] |466| 
$C$L52:    
	.dwpsn	file "../param.c",line 468,column 5,is_stmt
        MOV       AL,#5                 ; [CPU_] |468| 
        MOV       AH,#1284              ; [CPU_] |468| 
        MOVL      *-SP[22],ACC          ; [CPU_] |468| 
$C$L53:    
	.dwpsn	file "../param.c",line 469,column 3,is_stmt
        MOVL      ACC,*-SP[22]          ; [CPU_] |469| 
	.dwpsn	file "../param.c",line 470,column 1,is_stmt
        SUBB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$197, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$197, DW_AT_TI_end_line(0x1d6)
	.dwattr $C$DW$197, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$197

	.sect	".text"
	.global	_PAR_WriteAllPermanentParam

$C$DW$211	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteAllPermanentParam")
	.dwattr $C$DW$211, DW_AT_low_pc(_PAR_WriteAllPermanentParam)
	.dwattr $C$DW$211, DW_AT_high_pc(0x00)
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$211, DW_AT_external
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$211, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$211, DW_AT_TI_begin_line(0x1d9)
	.dwattr $C$DW$211, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$211, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../param.c",line 473,column 46,is_stmt,address _PAR_WriteAllPermanentParam

	.dwfde $C$DW$CIE, _PAR_WriteAllPermanentParam
$C$DW$212	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PAR_WriteAllPermanentParam   FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_PAR_WriteAllPermanentParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$213	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_breg20 -2]
$C$DW$214	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_breg20 -3]
$C$DW$215	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$215, DW_AT_location[DW_OP_breg20 -4]
$C$DW$216	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$216, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[2],XAR4          ; [CPU_] |473| 
	.dwpsn	file "../param.c",line 474,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |474| 
        MOVL      XAR0,#304             ; [CPU_] |474| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |474| 
        MOV       *-SP[3],AL            ; [CPU_] |474| 
	.dwpsn	file "../param.c",line 475,column 13,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |475| 
	.dwpsn	file "../param.c",line 476,column 21,is_stmt
        MOVB      ACC,#0                ; [CPU_] |476| 
        MOVL      *-SP[6],ACC           ; [CPU_] |476| 
	.dwpsn	file "../param.c",line 478,column 3,is_stmt
        B         $C$L55,UNC            ; [CPU_] |478| 
        ; branch occurs ; [] |478| 
$C$L54:    
	.dwpsn	file "../param.c",line 479,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |479| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |479| 
        INC       *-SP[4]               ; [CPU_] |479| 
$C$DW$217	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$217, DW_AT_low_pc(0x00)
	.dwattr $C$DW$217, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$217, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |479| 
        ; call occurs [#_WritePermanentParam] ; [] |479| 
        MOVL      *-SP[6],ACC           ; [CPU_] |479| 
$C$L55:    
	.dwpsn	file "../param.c",line 478,column 9,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |478| 
        CMP       AL,*-SP[4]            ; [CPU_] |478| 
        B         $C$L56,LEQ            ; [CPU_] |478| 
        ; branchcc occurs ; [] |478| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |478| 
        BF        $C$L54,EQ             ; [CPU_] |478| 
        ; branchcc occurs ; [] |478| 
$C$L56:    
	.dwpsn	file "../param.c",line 481,column 3,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |481| 
	.dwpsn	file "../param.c",line 482,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$218	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$218, DW_AT_low_pc(0x00)
	.dwattr $C$DW$218, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$211, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$211, DW_AT_TI_end_line(0x1e2)
	.dwattr $C$DW$211, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$211

	.sect	".text"
	.global	_WriteStatisticParam

$C$DW$219	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteStatisticParam")
	.dwattr $C$DW$219, DW_AT_low_pc(_WriteStatisticParam)
	.dwattr $C$DW$219, DW_AT_high_pc(0x00)
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_WriteStatisticParam")
	.dwattr $C$DW$219, DW_AT_external
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$219, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$219, DW_AT_TI_begin_line(0x1f1)
	.dwattr $C$DW$219, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$219, DW_AT_TI_max_frame_size(-28)
	.dwpsn	file "../param.c",line 497,column 40,is_stmt,address _WriteStatisticParam

	.dwfde $C$DW$CIE, _WriteStatisticParam
$C$DW$220	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$220, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _WriteStatisticParam          FR SIZE:  26           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 25 Auto,  0 SOE     *
;***************************************************************

_WriteStatisticParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -28
$C$DW$221	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_breg20 -1]
$C$DW$222	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$222, DW_AT_location[DW_OP_breg20 -2]
$C$DW$223	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$223, DW_AT_location[DW_OP_breg20 -4]
$C$DW$224	.dwtag  DW_TAG_variable, DW_AT_name("buff")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_buff")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_breg20 -20]
$C$DW$225	.dwtag  DW_TAG_variable, DW_AT_name("pdata")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$225, DW_AT_location[DW_OP_breg20 -22]
$C$DW$226	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$226, DW_AT_location[DW_OP_breg20 -23]
$C$DW$227	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$227, DW_AT_location[DW_OP_breg20 -24]
$C$DW$228	.dwtag  DW_TAG_variable, DW_AT_name("k")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_k")
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$228, DW_AT_location[DW_OP_breg20 -25]
        MOV       *-SP[1],AL            ; [CPU_] |497| 
	.dwpsn	file "../param.c",line 499,column 21,is_stmt
        MOV       AH,#2048              ; [CPU_] |499| 
        MOV       AL,#32                ; [CPU_] |499| 
        MOVL      *-SP[4],ACC           ; [CPU_] |499| 
	.dwpsn	file "../param.c",line 503,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |503| 
        B         $C$L57,GEQ            ; [CPU_] |503| 
        ; branchcc occurs ; [] |503| 
	.dwpsn	file "../param.c",line 503,column 18,is_stmt
        NOT       AL                    ; [CPU_] |503| 
        MOV       *-SP[1],AL            ; [CPU_] |503| 
$C$L57:    
	.dwpsn	file "../param.c",line 504,column 3,is_stmt
        CMPB      AL,#5                 ; [CPU_] |504| 
        B         $C$L63,GEQ            ; [CPU_] |504| 
        ; branchcc occurs ; [] |504| 
	.dwpsn	file "../param.c",line 505,column 5,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |505| 
	.dwpsn	file "../param.c",line 505,column 9,is_stmt
        MOV       *-SP[25],#0           ; [CPU_] |505| 
	.dwpsn	file "../param.c",line 506,column 5,is_stmt
        B         $C$L61,UNC            ; [CPU_] |506| 
        ; branch occurs ; [] |506| 
$C$L58:    
	.dwpsn	file "../param.c",line 507,column 7,is_stmt
        MPYB      ACC,T,#6              ; [CPU_] |507| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+4 ; [CPU_U] |507| 
        ADDL      XAR4,ACC              ; [CPU_] |507| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |507| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[23] << 2     ; [CPU_] |507| 
        ADDL      XAR4,ACC              ; [CPU_] |507| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |507| 
        MOVL      *-SP[22],ACC          ; [CPU_] |507| 
	.dwpsn	file "../param.c",line 508,column 7,is_stmt
        MOV       *-SP[24],#0           ; [CPU_] |508| 
	.dwpsn	file "../param.c",line 509,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        B         $C$L60,UNC            ; [CPU_] |509| 
        ; branch occurs ; [] |509| 
$C$L59:    
	.dwpsn	file "../param.c",line 510,column 9,is_stmt
        MOVZ      AR7,*-SP[25]          ; [CPU_] |510| 
        MOVZ      AR4,SP                ; [CPU_U] |510| 
        MOVL      XAR5,*-SP[22]         ; [CPU_] |510| 
        MOV       ACC,AR7               ; [CPU_] |510| 
        MOVL      P,ACC                 ; [CPU_] |510| 
        SUBB      XAR4,#20              ; [CPU_U] |510| 
        MOVB      AL,#1                 ; [CPU_] |510| 
        ADD       AL,AR7                ; [CPU_] |510| 
        MOVZ      AR7,*XAR5++           ; [CPU_] |510| 
        MOV       *-SP[25],AL           ; [CPU_] |510| 
        MOVL      *-SP[22],XAR5         ; [CPU_] |510| 
        MOVL      ACC,P                 ; [CPU_] |510| 
        ADDL      XAR4,ACC              ; [CPU_] |510| 
        MOV       *+XAR4[0],AR7         ; [CPU_] |510| 
	.dwpsn	file "../param.c",line 511,column 9,is_stmt
        ADD       *-SP[24],#2           ; [CPU_] |511| 
$C$L60:    
	.dwpsn	file "../param.c",line 509,column 14,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |509| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+4 ; [CPU_U] |509| 
        MPYB      ACC,T,#6              ; [CPU_] |509| 
        ADDL      XAR4,ACC              ; [CPU_] |509| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |509| 
        MOV       ACC,*-SP[23] << 2     ; [CPU_] |509| 
        ADDL      XAR4,ACC              ; [CPU_] |509| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |509| 
        CMP       AL,*-SP[24]           ; [CPU_] |509| 
        B         $C$L59,HI             ; [CPU_] |509| 
        ; branchcc occurs ; [] |509| 
	.dwpsn	file "../param.c",line 513,column 7,is_stmt
        INC       *-SP[23]              ; [CPU_] |513| 
$C$L61:    
	.dwpsn	file "../param.c",line 506,column 12,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |506| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+2 ; [CPU_U] |506| 
        MPYB      ACC,T,#6              ; [CPU_] |506| 
        ADDL      XAR4,ACC              ; [CPU_] |506| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |506| 
        CMP       AL,*-SP[23]           ; [CPU_] |506| 
        B         $C$L58,HI             ; [CPU_] |506| 
        ; branchcc occurs ; [] |506| 
	.dwpsn	file "../param.c",line 515,column 5,is_stmt
        MPYB      ACC,T,#6              ; [CPU_] |515| 
        MOVL      XAR6,#_PAR_EEPROM_INDEXES ; [CPU_U] |515| 
        MOVZ      AR4,SP                ; [CPU_U] |515| 
        MOVL      XAR5,#_PAR_EEPROM_INDEXES+1 ; [CPU_U] |515| 
        ADDL      XAR6,ACC              ; [CPU_] |515| 
        SUBB      XAR4,#20              ; [CPU_U] |515| 
        MPYB      ACC,T,#6              ; [CPU_] |515| 
        ADDL      XAR5,ACC              ; [CPU_] |515| 
        MOVB      AL,#0                 ; [CPU_] |515| 
        MOVZ      AR5,*+XAR5[0]         ; [CPU_] |515| 
        MOV       AH,*+XAR6[0]          ; [CPU_] |515| 
$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$229, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |515| 
        ; call occurs [#_I2C_Command] ; [] |515| 
        MOV       *-SP[2],AL            ; [CPU_] |515| 
	.dwpsn	file "../param.c",line 517,column 5,is_stmt
        CMPB      AL,#0                 ; [CPU_] |517| 
        BF        $C$L62,EQ             ; [CPU_] |517| 
        ; branchcc occurs ; [] |517| 
	.dwpsn	file "../param.c",line 517,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |517| 
        MOVL      *-SP[4],ACC           ; [CPU_] |517| 
$C$L62:    
	.dwpsn	file "../param.c",line 518,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_PAR_StatEepromCommandState ; [CPU_U] |518| 
        MOV       ACC,*-SP[1]           ; [CPU_] |518| 
        ADDL      XAR4,ACC              ; [CPU_] |518| 
        MOVB      *+XAR4[0],#2,UNC      ; [CPU_] |518| 
$C$L63:    
	.dwpsn	file "../param.c",line 520,column 3,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |520| 
	.dwpsn	file "../param.c",line 521,column 1,is_stmt
        SUBB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$219, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$219, DW_AT_TI_end_line(0x209)
	.dwattr $C$DW$219, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$219

	.sect	".text"
	.global	_PAR_WriteStatisticParam

$C$DW$231	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteStatisticParam")
	.dwattr $C$DW$231, DW_AT_low_pc(_PAR_WriteStatisticParam)
	.dwattr $C$DW$231, DW_AT_high_pc(0x00)
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$231, DW_AT_external
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$231, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$231, DW_AT_TI_begin_line(0x20b)
	.dwattr $C$DW$231, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$231, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../param.c",line 523,column 44,is_stmt,address _PAR_WriteStatisticParam

	.dwfde $C$DW$CIE, _PAR_WriteStatisticParam
$C$DW$232	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$232, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PAR_WriteStatisticParam      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_PAR_WriteStatisticParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$233	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$233, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |523| 
	.dwpsn	file "../param.c",line 524,column 3,is_stmt
        MOVL      XAR4,#_mailboxWriteParameters ; [CPU_U] |524| 
        MOVZ      AR5,SP                ; [CPU_U] |524| 
        MOVB      AL,#0                 ; [CPU_] |524| 
        SUBB      XAR5,#1               ; [CPU_U] |524| 
$C$DW$234	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$234, DW_AT_low_pc(0x00)
	.dwattr $C$DW$234, DW_AT_name("_MBX_post")
	.dwattr $C$DW$234, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |524| 
        ; call occurs [#_MBX_post] ; [] |524| 
        MOVU      ACC,AL                ; [CPU_] |524| 
	.dwpsn	file "../param.c",line 525,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$235	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$235, DW_AT_low_pc(0x00)
	.dwattr $C$DW$235, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$231, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$231, DW_AT_TI_end_line(0x20d)
	.dwattr $C$DW$231, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$231

	.sect	".text"
	.global	_PAR_ReadAllStatisticParam

$C$DW$236	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_ReadAllStatisticParam")
	.dwattr $C$DW$236, DW_AT_low_pc(_PAR_ReadAllStatisticParam)
	.dwattr $C$DW$236, DW_AT_high_pc(0x00)
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_PAR_ReadAllStatisticParam")
	.dwattr $C$DW$236, DW_AT_external
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$236, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$236, DW_AT_TI_begin_line(0x217)
	.dwattr $C$DW$236, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$236, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "../param.c",line 535,column 38,is_stmt,address _PAR_ReadAllStatisticParam

	.dwfde $C$DW$CIE, _PAR_ReadAllStatisticParam

;***************************************************************
;* FNAME: _PAR_ReadAllStatisticParam    FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 23 Auto,  0 SOE     *
;***************************************************************

_PAR_ReadAllStatisticParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
$C$DW$237	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_breg20 -1]
$C$DW$238	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_breg20 -2]
$C$DW$239	.dwtag  DW_TAG_variable, DW_AT_name("buff")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_buff")
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$239, DW_AT_location[DW_OP_breg20 -18]
$C$DW$240	.dwtag  DW_TAG_variable, DW_AT_name("pdata")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$240, DW_AT_location[DW_OP_breg20 -20]
$C$DW$241	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$241, DW_AT_location[DW_OP_breg20 -21]
$C$DW$242	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$242, DW_AT_location[DW_OP_breg20 -22]
$C$DW$243	.dwtag  DW_TAG_variable, DW_AT_name("k")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_k")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_breg20 -23]
	.dwpsn	file "../param.c",line 536,column 13,is_stmt
        MOVB      *-SP[1],#1,UNC        ; [CPU_] |536| 
	.dwpsn	file "../param.c",line 537,column 16,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |537| 
	.dwpsn	file "../param.c",line 541,column 3,is_stmt
        B         $C$L69,UNC            ; [CPU_] |541| 
        ; branch occurs ; [] |541| 
$C$L64:    
	.dwpsn	file "../param.c",line 542,column 5,is_stmt
        MOV       T,#6                  ; [CPU_] |542| 
        MOVZ      AR4,SP                ; [CPU_U] |542| 
        MOVL      XAR6,#_PAR_EEPROM_INDEXES ; [CPU_U] |542| 
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |542| 
        MOVL      XAR5,#_PAR_EEPROM_INDEXES+1 ; [CPU_U] |542| 
        SUBB      XAR4,#18              ; [CPU_U] |542| 
        ADDL      XAR6,ACC              ; [CPU_] |542| 
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |542| 
        ADDL      XAR5,ACC              ; [CPU_] |542| 
        MOVB      AL,#1                 ; [CPU_] |542| 
        MOVZ      AR5,*+XAR5[0]         ; [CPU_] |542| 
        MOV       AH,*+XAR6[0]          ; [CPU_] |542| 
$C$DW$244	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$244, DW_AT_low_pc(0x00)
	.dwattr $C$DW$244, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$244, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |542| 
        ; call occurs [#_I2C_Command] ; [] |542| 
        MOV       *-SP[1],AL            ; [CPU_] |542| 
	.dwpsn	file "../param.c",line 544,column 5,is_stmt
        MOV       *-SP[21],#0           ; [CPU_] |544| 
	.dwpsn	file "../param.c",line 544,column 9,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |544| 
	.dwpsn	file "../param.c",line 545,column 5,is_stmt
        MOV       T,#6                  ; [CPU_] |545| 
        B         $C$L68,UNC            ; [CPU_] |545| 
        ; branch occurs ; [] |545| 
$C$L65:    
	.dwpsn	file "../param.c",line 546,column 7,is_stmt
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |546| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+4 ; [CPU_U] |546| 
        ADDL      XAR4,ACC              ; [CPU_] |546| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |546| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[21] << 2     ; [CPU_] |546| 
        ADDL      XAR4,ACC              ; [CPU_] |546| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |546| 
        MOVL      *-SP[20],ACC          ; [CPU_] |546| 
	.dwpsn	file "../param.c",line 547,column 7,is_stmt
        MOV       *-SP[22],#0           ; [CPU_] |547| 
	.dwpsn	file "../param.c",line 548,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        B         $C$L67,UNC            ; [CPU_] |548| 
        ; branch occurs ; [] |548| 
$C$L66:    
	.dwpsn	file "../param.c",line 549,column 9,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |549| 
        MOVB      AH,#1                 ; [CPU_] |549| 
        MOVL      XAR4,*-SP[20]         ; [CPU_] |549| 
        MOVZ      AR7,SP                ; [CPU_U] |549| 
        ADD       AH,AL                 ; [CPU_] |549| 
        SUBB      XAR7,#18              ; [CPU_U] |549| 
        MOVL      XAR5,XAR4             ; [CPU_] |549| 
        MOV       *-SP[23],AH           ; [CPU_] |549| 
        MOV       ACC,AL                ; [CPU_] |549| 
        ADDL      XAR7,ACC              ; [CPU_] |549| 
        ADDB      XAR5,#1               ; [CPU_] |549| 
        MOVL      *-SP[20],XAR5         ; [CPU_] |549| 
        MOV       AL,*XAR7              ; [CPU_] |549| 
        MOV       *+XAR4[0],AL          ; [CPU_] |549| 
	.dwpsn	file "../param.c",line 550,column 9,is_stmt
        ADD       *-SP[22],#2           ; [CPU_] |550| 
$C$L67:    
	.dwpsn	file "../param.c",line 548,column 14,is_stmt
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |548| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+4 ; [CPU_U] |548| 
        ADDL      XAR4,ACC              ; [CPU_] |548| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |548| 
        MOV       ACC,*-SP[21] << 2     ; [CPU_] |548| 
        ADDL      XAR4,ACC              ; [CPU_] |548| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |548| 
        CMP       AL,*-SP[22]           ; [CPU_] |548| 
        B         $C$L66,HI             ; [CPU_] |548| 
        ; branchcc occurs ; [] |548| 
	.dwpsn	file "../param.c",line 552,column 7,is_stmt
        INC       *-SP[21]              ; [CPU_] |552| 
$C$L68:    
	.dwpsn	file "../param.c",line 545,column 12,is_stmt
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |545| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+2 ; [CPU_U] |545| 
        ADDL      XAR4,ACC              ; [CPU_] |545| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |545| 
        CMP       AL,*-SP[21]           ; [CPU_] |545| 
        B         $C$L65,HI             ; [CPU_] |545| 
        ; branchcc occurs ; [] |545| 
	.dwpsn	file "../param.c",line 554,column 5,is_stmt
        INC       *-SP[2]               ; [CPU_] |554| 
$C$L69:    
	.dwpsn	file "../param.c",line 541,column 10,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |541| 
        CMPB      AL,#5                 ; [CPU_] |541| 
        B         $C$L70,HIS            ; [CPU_] |541| 
        ; branchcc occurs ; [] |541| 
        MOV       AL,*-SP[1]            ; [CPU_] |541| 
        BF        $C$L64,NEQ            ; [CPU_] |541| 
        ; branchcc occurs ; [] |541| 
$C$L70:    
	.dwpsn	file "../param.c",line 556,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |556| 
	.dwpsn	file "../param.c",line 557,column 1,is_stmt
        SUBB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$245	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$245, DW_AT_low_pc(0x00)
	.dwattr $C$DW$245, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$236, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$236, DW_AT_TI_end_line(0x22d)
	.dwattr $C$DW$236, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$236

	.sect	".text"
	.global	_PAR_FindODPermanentParamIndex

$C$DW$246	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$246, DW_AT_low_pc(_PAR_FindODPermanentParamIndex)
	.dwattr $C$DW$246, DW_AT_high_pc(0x00)
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$246, DW_AT_external
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$246, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$246, DW_AT_TI_begin_line(0x24a)
	.dwattr $C$DW$246, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$246, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../param.c",line 587,column 1,is_stmt,address _PAR_FindODPermanentParamIndex

	.dwfde $C$DW$CIE, _PAR_FindODPermanentParamIndex
$C$DW$247	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$247, DW_AT_location[DW_OP_reg12]
$C$DW$248	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$248, DW_AT_location[DW_OP_reg0]
$C$DW$249	.dwtag  DW_TAG_formal_parameter, DW_AT_name("subindex")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$249, DW_AT_location[DW_OP_reg1]
$C$DW$250	.dwtag  DW_TAG_formal_parameter, DW_AT_name("array_index")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$250, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _PAR_FindODPermanentParamIndex FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_PAR_FindODPermanentParamIndex:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$251	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$251, DW_AT_location[DW_OP_breg20 -2]
$C$DW$252	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_breg20 -3]
$C$DW$253	.dwtag  DW_TAG_variable, DW_AT_name("subindex")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$253, DW_AT_location[DW_OP_breg20 -4]
$C$DW$254	.dwtag  DW_TAG_variable, DW_AT_name("array_index")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$254, DW_AT_location[DW_OP_breg20 -6]
$C$DW$255	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$255, DW_AT_location[DW_OP_breg20 -7]
$C$DW$256	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$256, DW_AT_location[DW_OP_breg20 -8]
$C$DW$257	.dwtag  DW_TAG_variable, DW_AT_name("result")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_result")
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$257, DW_AT_location[DW_OP_breg20 -9]
$C$DW$258	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$258, DW_AT_location[DW_OP_breg20 -14]
        MOV       *-SP[4],AH            ; [CPU_] |587| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |587| 
        MOV       *-SP[3],AL            ; [CPU_] |587| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |587| 
	.dwpsn	file "../param.c",line 588,column 12,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |588| 
        MOVL      XAR0,#304             ; [CPU_] |588| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |588| 
        MOV       *-SP[7],AL            ; [CPU_] |588| 
	.dwpsn	file "../param.c",line 589,column 12,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |589| 
	.dwpsn	file "../param.c",line 590,column 16,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |590| 
	.dwpsn	file "../param.c",line 593,column 3,is_stmt
        MOVL      XAR0,#302             ; [CPU_] |594| 
        B         $C$L74,UNC            ; [CPU_] |593| 
        ; branch occurs ; [] |593| 
$C$L71:    
	.dwpsn	file "../param.c",line 594,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |594| 
        MOVU      ACC,*-SP[8]           ; [CPU_] |594| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |594| 
        LSL       ACC,2                 ; [CPU_] |594| 
        MOVZ      AR4,SP                ; [CPU_U] |594| 
        SUBB      XAR4,#14              ; [CPU_U] |594| 
        ADDL      XAR7,ACC              ; [CPU_] |594| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |594| 
	.dwpsn	file "../param.c",line 595,column 5,is_stmt
        MOVB      AH,#0                 ; [CPU_] |595| 
        MOV       AL,*-SP[3]            ; [CPU_] |595| 
        CMP       AL,*-SP[14]           ; [CPU_] |595| 
        BF        $C$L72,NEQ            ; [CPU_] |595| 
        ; branchcc occurs ; [] |595| 
        MOV       AL,*-SP[4]            ; [CPU_] |595| 
        CMP       AL,*-SP[13]           ; [CPU_] |595| 
        BF        $C$L72,NEQ            ; [CPU_] |595| 
        ; branchcc occurs ; [] |595| 
        MOVB      AH,#1                 ; [CPU_] |595| 
$C$L72:    
        MOV       *-SP[9],AH            ; [CPU_] |595| 
	.dwpsn	file "../param.c",line 596,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |596| 
        BF        $C$L73,EQ             ; [CPU_] |596| 
        ; branchcc occurs ; [] |596| 
	.dwpsn	file "../param.c",line 597,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |597| 
        MOV       AL,*-SP[8]            ; [CPU_] |597| 
        MOV       *+XAR4[0],AL          ; [CPU_] |597| 
        B         $C$L74,UNC            ; [CPU_] |597| 
        ; branch occurs ; [] |597| 
$C$L73:    
	.dwpsn	file "../param.c",line 599,column 7,is_stmt
        INC       *-SP[8]               ; [CPU_] |599| 
$C$L74:    
	.dwpsn	file "../param.c",line 593,column 10,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |593| 
        CMP       AL,*-SP[8]            ; [CPU_] |593| 
        B         $C$L75,LOS            ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
        MOV       AL,*-SP[9]            ; [CPU_] |593| 
        BF        $C$L71,EQ             ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
$C$L75:    
	.dwpsn	file "../param.c",line 601,column 3,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |601| 
	.dwpsn	file "../param.c",line 602,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$259	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$259, DW_AT_low_pc(0x00)
	.dwattr $C$DW$259, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$246, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$246, DW_AT_TI_end_line(0x25a)
	.dwattr $C$DW$246, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$246

	.sect	".text"
	.global	_PAR_FindODPermanentParamIndex2

$C$DW$260	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_FindODPermanentParamIndex2")
	.dwattr $C$DW$260, DW_AT_low_pc(_PAR_FindODPermanentParamIndex2)
	.dwattr $C$DW$260, DW_AT_high_pc(0x00)
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_PAR_FindODPermanentParamIndex2")
	.dwattr $C$DW$260, DW_AT_external
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$260, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$260, DW_AT_TI_begin_line(0x272)
	.dwattr $C$DW$260, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$260, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../param.c",line 627,column 1,is_stmt,address _PAR_FindODPermanentParamIndex2

	.dwfde $C$DW$CIE, _PAR_FindODPermanentParamIndex2
$C$DW$261	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$261, DW_AT_location[DW_OP_reg12]
$C$DW$262	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$262, DW_AT_location[DW_OP_reg0]
$C$DW$263	.dwtag  DW_TAG_formal_parameter, DW_AT_name("array_index")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$263, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _PAR_FindODPermanentParamIndex2 FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_PAR_FindODPermanentParamIndex2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$264	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$264, DW_AT_location[DW_OP_breg20 -2]
$C$DW$265	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$265, DW_AT_location[DW_OP_breg20 -3]
$C$DW$266	.dwtag  DW_TAG_variable, DW_AT_name("array_index")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$266, DW_AT_location[DW_OP_breg20 -6]
$C$DW$267	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$267, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$267, DW_AT_location[DW_OP_breg20 -7]
$C$DW$268	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$268, DW_AT_location[DW_OP_breg20 -8]
$C$DW$269	.dwtag  DW_TAG_variable, DW_AT_name("result")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_result")
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$269, DW_AT_location[DW_OP_breg20 -9]
$C$DW$270	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$270, DW_AT_location[DW_OP_breg20 -14]
        MOV       *-SP[3],AL            ; [CPU_] |627| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |627| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |627| 
	.dwpsn	file "../param.c",line 628,column 12,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |628| 
        MOVL      XAR0,#304             ; [CPU_] |628| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |628| 
        MOV       *-SP[7],AL            ; [CPU_] |628| 
	.dwpsn	file "../param.c",line 629,column 12,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |629| 
	.dwpsn	file "../param.c",line 630,column 16,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |630| 
	.dwpsn	file "../param.c",line 633,column 3,is_stmt
        MOVL      XAR0,#302             ; [CPU_] |634| 
        B         $C$L79,UNC            ; [CPU_] |633| 
        ; branch occurs ; [] |633| 
$C$L76:    
	.dwpsn	file "../param.c",line 634,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |634| 
        MOVU      ACC,*-SP[8]           ; [CPU_] |634| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |634| 
        LSL       ACC,2                 ; [CPU_] |634| 
        MOVZ      AR4,SP                ; [CPU_U] |634| 
        SUBB      XAR4,#14              ; [CPU_U] |634| 
        ADDL      XAR7,ACC              ; [CPU_] |634| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |634| 
	.dwpsn	file "../param.c",line 635,column 5,is_stmt
        MOVB      AH,#0                 ; [CPU_] |635| 
        MOV       AL,*-SP[3]            ; [CPU_] |635| 
        CMP       AL,*-SP[11]           ; [CPU_] |635| 
        BF        $C$L77,NEQ            ; [CPU_] |635| 
        ; branchcc occurs ; [] |635| 
        MOVB      AH,#1                 ; [CPU_] |635| 
$C$L77:    
        MOV       *-SP[9],AH            ; [CPU_] |635| 
	.dwpsn	file "../param.c",line 636,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |636| 
        BF        $C$L78,EQ             ; [CPU_] |636| 
        ; branchcc occurs ; [] |636| 
	.dwpsn	file "../param.c",line 637,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |637| 
        MOV       AL,*-SP[8]            ; [CPU_] |637| 
        MOV       *+XAR4[0],AL          ; [CPU_] |637| 
        B         $C$L79,UNC            ; [CPU_] |637| 
        ; branch occurs ; [] |637| 
$C$L78:    
	.dwpsn	file "../param.c",line 639,column 7,is_stmt
        INC       *-SP[8]               ; [CPU_] |639| 
$C$L79:    
	.dwpsn	file "../param.c",line 633,column 10,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |633| 
        CMP       AL,*-SP[8]            ; [CPU_] |633| 
        B         $C$L80,LOS            ; [CPU_] |633| 
        ; branchcc occurs ; [] |633| 
        MOV       AL,*-SP[9]            ; [CPU_] |633| 
        BF        $C$L76,EQ             ; [CPU_] |633| 
        ; branchcc occurs ; [] |633| 
$C$L80:    
	.dwpsn	file "../param.c",line 641,column 3,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |641| 
	.dwpsn	file "../param.c",line 642,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$271	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$271, DW_AT_low_pc(0x00)
	.dwattr $C$DW$271, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$260, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$260, DW_AT_TI_end_line(0x282)
	.dwattr $C$DW$260, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$260

	.sect	".text"
	.global	_PAR_GetEepromIndexes

$C$DW$272	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_GetEepromIndexes")
	.dwattr $C$DW$272, DW_AT_low_pc(_PAR_GetEepromIndexes)
	.dwattr $C$DW$272, DW_AT_high_pc(0x00)
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_PAR_GetEepromIndexes")
	.dwattr $C$DW$272, DW_AT_external
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$272, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$272, DW_AT_TI_begin_line(0x29b)
	.dwattr $C$DW$272, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$272, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../param.c",line 667,column 84,is_stmt,address _PAR_GetEepromIndexes

	.dwfde $C$DW$CIE, _PAR_GetEepromIndexes
$C$DW$273	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$273, DW_AT_location[DW_OP_reg12]
$C$DW$274	.dwtag  DW_TAG_formal_parameter, DW_AT_name("array_index")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$274, DW_AT_location[DW_OP_reg0]
$C$DW$275	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indexes")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$275, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _PAR_GetEepromIndexes         FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_PAR_GetEepromIndexes:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$276	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$276, DW_AT_location[DW_OP_breg20 -2]
$C$DW$277	.dwtag  DW_TAG_variable, DW_AT_name("array_index")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$277, DW_AT_location[DW_OP_breg20 -3]
$C$DW$278	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$278, DW_AT_location[DW_OP_breg20 -6]
$C$DW$279	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$279, DW_AT_location[DW_OP_breg20 -7]
$C$DW$280	.dwtag  DW_TAG_variable, DW_AT_name("result")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_result")
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$280, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[3],AL            ; [CPU_] |667| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |667| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |667| 
	.dwpsn	file "../param.c",line 669,column 12,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |669| 
        MOVL      XAR0,#304             ; [CPU_] |669| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |669| 
        MOV       *-SP[7],AL            ; [CPU_] |669| 
	.dwpsn	file "../param.c",line 671,column 3,is_stmt
        MOVB      AH,#0                 ; [CPU_] |671| 
        CMP       AL,*-SP[3]            ; [CPU_] |671| 
        B         $C$L81,LOS            ; [CPU_] |671| 
        ; branchcc occurs ; [] |671| 
        MOVB      AH,#1                 ; [CPU_] |671| 
$C$L81:    
        MOV       *-SP[8],AH            ; [CPU_] |671| 
	.dwpsn	file "../param.c",line 672,column 3,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |672| 
        BF        $C$L82,EQ             ; [CPU_] |672| 
        ; branchcc occurs ; [] |672| 
	.dwpsn	file "../param.c",line 673,column 5,is_stmt
        MOVL      XAR5,*-SP[2]          ; [CPU_] |673| 
        MOVL      XAR0,#302             ; [CPU_] |673| 
        MOVL      XAR7,*+XAR5[AR0]      ; [CPU_] |673| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |673| 
        MOVU      ACC,*-SP[3]           ; [CPU_] |673| 
        LSL       ACC,2                 ; [CPU_] |673| 
        ADDL      XAR7,ACC              ; [CPU_] |673| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |673| 
$C$L82:    
	.dwpsn	file "../param.c",line 675,column 3,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |675| 
	.dwpsn	file "../param.c",line 676,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$272, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$272, DW_AT_TI_end_line(0x2a4)
	.dwattr $C$DW$272, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$272

	.sect	".text"
	.global	_PAR_StoreODSubIndex

$C$DW$282	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_StoreODSubIndex")
	.dwattr $C$DW$282, DW_AT_low_pc(_PAR_StoreODSubIndex)
	.dwattr $C$DW$282, DW_AT_high_pc(0x00)
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$282, DW_AT_external
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$282, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$282, DW_AT_TI_begin_line(0x2bf)
	.dwattr $C$DW$282, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$282, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../param.c",line 704,column 1,is_stmt,address _PAR_StoreODSubIndex

	.dwfde $C$DW$CIE, _PAR_StoreODSubIndex
$C$DW$283	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$283, DW_AT_location[DW_OP_reg12]
$C$DW$284	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$284, DW_AT_location[DW_OP_reg0]
$C$DW$285	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$285, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _PAR_StoreODSubIndex          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_PAR_StoreODSubIndex:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$286	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$286, DW_AT_location[DW_OP_breg20 -2]
$C$DW$287	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$287, DW_AT_location[DW_OP_breg20 -3]
$C$DW$288	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$288, DW_AT_location[DW_OP_breg20 -4]
$C$DW$289	.dwtag  DW_TAG_variable, DW_AT_name("array_index")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$289, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[4],AH            ; [CPU_] |704| 
        MOV       *-SP[3],AL            ; [CPU_] |704| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |704| 
	.dwpsn	file "../param.c",line 707,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |707| 
        MOVZ      AR5,SP                ; [CPU_U] |707| 
        SUBB      XAR5,#5               ; [CPU_U] |707| 
$C$DW$290	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$290, DW_AT_low_pc(0x00)
	.dwattr $C$DW$290, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$290, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |707| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |707| 
        CMPB      AL,#0                 ; [CPU_] |707| 
        BF        $C$L84,EQ             ; [CPU_] |707| 
        ; branchcc occurs ; [] |707| 
	.dwpsn	file "../param.c",line 708,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |708| 
        MOVL      XAR4,#_mailboxWriteParameters ; [CPU_U] |708| 
        MOVB      AL,#0                 ; [CPU_] |708| 
        SUBB      XAR5,#5               ; [CPU_U] |708| 
$C$DW$291	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$291, DW_AT_low_pc(0x00)
	.dwattr $C$DW$291, DW_AT_name("_MBX_post")
	.dwattr $C$DW$291, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |708| 
        ; call occurs [#_MBX_post] ; [] |708| 
        CMPB      AL,#0                 ; [CPU_] |708| 
        BF        $C$L83,EQ             ; [CPU_] |708| 
        ; branchcc occurs ; [] |708| 
	.dwpsn	file "../param.c",line 709,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |709| 
        B         $C$L85,UNC            ; [CPU_] |709| 
        ; branch occurs ; [] |709| 
$C$L83:    
	.dwpsn	file "../param.c",line 711,column 7,is_stmt
        MOV       AL,#5                 ; [CPU_] |711| 
        MOV       AH,#1284              ; [CPU_] |711| 
        B         $C$L85,UNC            ; [CPU_] |711| 
        ; branch occurs ; [] |711| 
$C$L84:    
	.dwpsn	file "../param.c",line 714,column 5,is_stmt
        MOV       ACC,#3076 << 15       ; [CPU_] |714| 
$C$L85:    
	.dwpsn	file "../param.c",line 715,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$292	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$292, DW_AT_low_pc(0x00)
	.dwattr $C$DW$292, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$282, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$282, DW_AT_TI_end_line(0x2cb)
	.dwattr $C$DW$282, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$282

	.sect	".text"
	.global	_TaskStoreParam

$C$DW$293	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskStoreParam")
	.dwattr $C$DW$293, DW_AT_low_pc(_TaskStoreParam)
	.dwattr $C$DW$293, DW_AT_high_pc(0x00)
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_TaskStoreParam")
	.dwattr $C$DW$293, DW_AT_external
	.dwattr $C$DW$293, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$293, DW_AT_TI_begin_line(0x2dc)
	.dwattr $C$DW$293, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$293, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../param.c",line 733,column 1,is_stmt,address _TaskStoreParam

	.dwfde $C$DW$CIE, _TaskStoreParam

;***************************************************************
;* FNAME: _TaskStoreParam               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_TaskStoreParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$294	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$294, DW_AT_location[DW_OP_breg20 -1]
$C$DW$295	.dwtag  DW_TAG_variable, DW_AT_name("indexcrc")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_indexcrc")
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$295, DW_AT_location[DW_OP_breg20 -2]
$C$DW$296	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$296, DW_AT_location[DW_OP_breg20 -4]
$C$DW$297	.dwtag  DW_TAG_variable, DW_AT_name("crc_to_be_computed")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_crc_to_be_computed")
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$297, DW_AT_location[DW_OP_breg20 -5]
	.dwpsn	file "../param.c",line 737,column 28,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |737| 
	.dwpsn	file "../param.c",line 738,column 3,is_stmt
$C$DW$298	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$298, DW_AT_low_pc(0x00)
	.dwattr $C$DW$298, DW_AT_name("_canOpenInit")
	.dwattr $C$DW$298, DW_AT_TI_call
        LCR       #_canOpenInit         ; [CPU_] |738| 
        ; call occurs [#_canOpenInit] ; [] |738| 
	.dwpsn	file "../param.c",line 739,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |739| 
        MOVZ      AR5,SP                ; [CPU_U] |739| 
        MOV       AL,#8195              ; [CPU_] |739| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |739| 
        SUBB      XAR5,#2               ; [CPU_U] |739| 
$C$DW$299	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$299, DW_AT_low_pc(0x00)
	.dwattr $C$DW$299, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$299, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |739| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |739| 
        CMPB      AL,#0                 ; [CPU_] |739| 
        BF        $C$L86,NEQ            ; [CPU_] |739| 
        ; branchcc occurs ; [] |739| 
	.dwpsn	file "../param.c",line 739,column 86,is_stmt
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |739| 
$C$L86:    
	.dwpsn	file "../param.c",line 740,column 9,is_stmt
$C$L87:    
	.dwpsn	file "../param.c",line 741,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |741| 
        MOVL      XAR4,#_mailboxWriteParameters ; [CPU_U] |741| 
        MOVB      AL,#10                ; [CPU_] |741| 
        SUBB      XAR5,#1               ; [CPU_U] |741| 
$C$DW$300	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$300, DW_AT_low_pc(0x00)
	.dwattr $C$DW$300, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$300, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |741| 
        ; call occurs [#_MBX_pend] ; [] |741| 
        CMPB      AL,#0                 ; [CPU_] |741| 
        BF        $C$L91,EQ             ; [CPU_] |741| 
        ; branchcc occurs ; [] |741| 
	.dwpsn	file "../param.c",line 742,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |742| 
        MOVL      *-SP[4],ACC           ; [CPU_] |742| 
	.dwpsn	file "../param.c",line 743,column 7,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |743| 
        CMP       AL,*-SP[1]            ; [CPU_] |743| 
        BF        $C$L88,NEQ            ; [CPU_] |743| 
        ; branchcc occurs ; [] |743| 
	.dwpsn	file "../param.c",line 743,column 30,is_stmt
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |743| 
        B         $C$L87,UNC            ; [CPU_] |743| 
        ; branch occurs ; [] |743| 
$C$L88:    
	.dwpsn	file "../param.c",line 744,column 12,is_stmt
        CMP       AL,*-SP[1]            ; [CPU_] |744| 
        BF        $C$L90,EQ             ; [CPU_] |744| 
        ; branchcc occurs ; [] |744| 
        MOV       AL,*-SP[1]            ; [CPU_] |744| 
        B         $C$L90,LT             ; [CPU_] |744| 
        ; branchcc occurs ; [] |744| 
	.dwpsn	file "../param.c",line 745,column 9,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |745| 
$C$DW$301	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$301, DW_AT_low_pc(0x00)
	.dwattr $C$DW$301, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$301, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |745| 
        ; call occurs [#_WritePermanentParam] ; [] |745| 
        MOVL      *-SP[4],ACC           ; [CPU_] |745| 
	.dwpsn	file "../param.c",line 746,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |746| 
        BF        $C$L89,NEQ            ; [CPU_] |746| 
        ; branchcc occurs ; [] |746| 
	.dwpsn	file "../param.c",line 747,column 11,is_stmt
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |747| 
        B         $C$L87,UNC            ; [CPU_] |747| 
        ; branch occurs ; [] |747| 
$C$L89:    
	.dwpsn	file "../param.c",line 748,column 14,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |748| 
	.dwpsn	file "../param.c",line 749,column 7,is_stmt
        B         $C$L87,UNC            ; [CPU_] |749| 
        ; branch occurs ; [] |749| 
$C$L90:    
	.dwpsn	file "../param.c",line 750,column 12,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |750| 
        B         $C$L87,GEQ            ; [CPU_] |750| 
        ; branchcc occurs ; [] |750| 
	.dwpsn	file "../param.c",line 751,column 9,is_stmt
$C$DW$302	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$302, DW_AT_low_pc(0x00)
	.dwattr $C$DW$302, DW_AT_name("_WriteStatisticParam")
	.dwattr $C$DW$302, DW_AT_TI_call
        LCR       #_WriteStatisticParam ; [CPU_] |751| 
        ; call occurs [#_WriteStatisticParam] ; [] |751| 
        MOVL      *-SP[4],ACC           ; [CPU_] |751| 
	.dwpsn	file "../param.c",line 753,column 5,is_stmt
        B         $C$L87,UNC            ; [CPU_] |753| 
        ; branch occurs ; [] |753| 
$C$L91:    
	.dwpsn	file "../param.c",line 754,column 10,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |754| 
        BF        $C$L87,EQ             ; [CPU_] |754| 
        ; branchcc occurs ; [] |754| 
	.dwpsn	file "../param.c",line 755,column 7,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |755| 
	.dwpsn	file "../param.c",line 756,column 7,is_stmt
$C$DW$303	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$303, DW_AT_low_pc(0x00)
	.dwattr $C$DW$303, DW_AT_name("_ComputeParamCRC")
	.dwattr $C$DW$303, DW_AT_TI_call
        LCR       #_ComputeParamCRC     ; [CPU_] |756| 
        ; call occurs [#_ComputeParamCRC] ; [] |756| 
        MOVW      DP,#_ODP_CrcParameters ; [CPU_U] 
        MOVL      @_ODP_CrcParameters,ACC ; [CPU_] |756| 
	.dwpsn	file "../param.c",line 757,column 7,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |757| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |757| 
$C$DW$304	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$304, DW_AT_low_pc(0x00)
	.dwattr $C$DW$304, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$304, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |757| 
        ; call occurs [#_WritePermanentParam] ; [] |757| 
	.dwpsn	file "../param.c",line 740,column 9,is_stmt
        B         $C$L87,UNC            ; [CPU_] |740| 
        ; branch occurs ; [] |740| 
	.dwattr $C$DW$293, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$293, DW_AT_TI_end_line(0x2f8)
	.dwattr $C$DW$293, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$293

	.sect	".text"
	.global	_PAR_SetParamDependantVars

$C$DW$305	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_SetParamDependantVars")
	.dwattr $C$DW$305, DW_AT_low_pc(_PAR_SetParamDependantVars)
	.dwattr $C$DW$305, DW_AT_high_pc(0x00)
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$305, DW_AT_external
	.dwattr $C$DW$305, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$305, DW_AT_TI_begin_line(0x301)
	.dwattr $C$DW$305, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$305, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../param.c",line 771,column 1,is_stmt,address _PAR_SetParamDependantVars

	.dwfde $C$DW$CIE, _PAR_SetParamDependantVars

;***************************************************************
;* FNAME: _PAR_SetParamDependantVars    FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_PAR_SetParamDependantVars:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../param.c",line 773,column 3,is_stmt
        MOVW      DP,#_mms_dict_obj100A ; [CPU_U] 
        MOVB      @_mms_dict_obj100A,#68,UNC ; [CPU_] |773| 
	.dwpsn	file "../param.c",line 774,column 3,is_stmt
        MOVW      DP,#_ODV_Version      ; [CPU_U] 
        MOVB      @_ODV_Version,#52,UNC ; [CPU_] |774| 
	.dwpsn	file "../param.c",line 775,column 3,is_stmt
        MOVL      XAR4,#1137            ; [CPU_U] |775| 
        MOVW      DP,#_mms_dict_obj1018_Vendor_ID ; [CPU_U] 
        MOVL      @_mms_dict_obj1018_Vendor_ID,XAR4 ; [CPU_] |775| 
	.dwpsn	file "../param.c",line 776,column 3,is_stmt
        MOVW      DP,#_mms_dict_obj1018_Product_Code ; [CPU_U] 
        MOVL      XAR4,#4201            ; [CPU_U] |776| 
        MOVL      @_mms_dict_obj1018_Product_Code,XAR4 ; [CPU_] |776| 
	.dwpsn	file "../param.c",line 778,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOVL      ACC,@_ODP_Board_RevisionNumber ; [CPU_] |778| 
        BF        $C$L92,EQ             ; [CPU_] |778| 
        ; branchcc occurs ; [] |778| 
        MOVB      ACC,#98               ; [CPU_] |778| 
        CMPL      ACC,@_ODP_Board_RevisionNumber ; [CPU_] |778| 
        B         $C$L92,LO             ; [CPU_] |778| 
        ; branchcc occurs ; [] |778| 
        MOVW      DP,#_ODP_Board_BaudRate ; [CPU_U] 
        MOV       AL,@_ODP_Board_BaudRate ; [CPU_] |778| 
        CMPB      AL,#250               ; [CPU_] |778| 
        BF        $C$L93,EQ             ; [CPU_] |778| 
        ; branchcc occurs ; [] |778| 
        CMP       @_ODP_Board_BaudRate,#500 ; [CPU_] |778| 
        BF        $C$L93,EQ             ; [CPU_] |778| 
        ; branchcc occurs ; [] |778| 
        CMP       @_ODP_Board_BaudRate,#1000 ; [CPU_] |778| 
        BF        $C$L93,EQ             ; [CPU_] |778| 
        ; branchcc occurs ; [] |778| 
        CMPB      AL,#125               ; [CPU_] |778| 
        BF        $C$L93,EQ             ; [CPU_] |778| 
        ; branchcc occurs ; [] |778| 
$C$L92:    
	.dwpsn	file "../param.c",line 780,column 4,is_stmt
        MOVB      ACC,#1                ; [CPU_] |780| 
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOVL      @_ODP_Board_RevisionNumber,ACC ; [CPU_] |780| 
	.dwpsn	file "../param.c",line 781,column 4,is_stmt
        MOVW      DP,#_ODP_Board_BaudRate ; [CPU_U] 
        MOVB      @_ODP_Board_BaudRate,#250,UNC ; [CPU_] |781| 
$C$L93:    
	.dwpsn	file "../param.c",line 784,column 3,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |784| 
        CMPB      AL,#1                 ; [CPU_] |784| 
        BF        $C$L94,EQ             ; [CPU_] |784| 
        ; branchcc occurs ; [] |784| 
	.dwpsn	file "../param.c",line 785,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |785| 
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOVL      @_ODP_Board_RevisionNumber,ACC ; [CPU_] |785| 
	.dwpsn	file "../param.c",line 786,column 5,is_stmt
        MOVW      DP,#_ODP_Board_SerialNumber ; [CPU_U] 
        MOVB      ACC,#0                ; [CPU_] |786| 
        MOVL      @_ODP_Board_SerialNumber,ACC ; [CPU_] |786| 
	.dwpsn	file "../param.c",line 787,column 5,is_stmt
        MOVIZ     R0H,#16930            ; [CPU_] |787| 
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOVXI     R0H,#26214            ; [CPU_] |787| 
        MOV32     @_ODP_Battery_Capacity,R0H ; [CPU_] |787| 
	.dwpsn	file "../param.c",line 788,column 5,is_stmt
        MOVW      DP,#_ODP_Board_BaudRate ; [CPU_U] 
        MOVB      @_ODP_Board_BaudRate,#250,UNC ; [CPU_] |788| 
$C$L94:    
	.dwpsn	file "../param.c",line 791,column 3,is_stmt
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOV32     R0H,@_ODP_Battery_Capacity ; [CPU_] |791| 
        MPYF32    R0H,R0H,#17761        ; [CPU_] |791| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R0H,#16896        ; [CPU_] |791| 
$C$DW$306	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$306, DW_AT_low_pc(0x00)
	.dwattr $C$DW$306, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$306, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |791| 
        ; call occurs [#_CNV_Round] ; [] |791| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      @_PAR_Capacity_Total,ACC ; [CPU_] |791| 
	.dwpsn	file "../param.c",line 792,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOVL      ACC,@_ODP_Board_RevisionNumber ; [CPU_] |792| 
        MOVW      DP,#_mms_dict_obj1018_Revision_Number ; [CPU_U] 
        MOVL      @_mms_dict_obj1018_Revision_Number,ACC ; [CPU_] |792| 
	.dwpsn	file "../param.c",line 793,column 3,is_stmt
        MOVW      DP,#_ODP_Board_SerialNumber ; [CPU_U] 
        MOVL      ACC,@_ODP_Board_SerialNumber ; [CPU_] |793| 
        MOVW      DP,#_mms_dict_obj1018_Serial_Number ; [CPU_U] 
        MOVL      @_mms_dict_obj1018_Serial_Number,ACC ; [CPU_] |793| 
	.dwpsn	file "../param.c",line 795,column 3,is_stmt
        MOVW      DP,#_CNV_CurrentUnit  ; [CPU_U] 
        MOVIZ     R0H,#16256            ; [CPU_] |795| 
        MOV32     @_CNV_CurrentUnit,R0H ; [CPU_] |795| 
	.dwpsn	file "../param.c",line 796,column 3,is_stmt
        MOVW      DP,#_CNV_CurrentRange ; [CPU_U] 
        MOV       @_CNV_CurrentRange,#2047 ; [CPU_] |796| 
	.dwpsn	file "../param.c",line 797,column 1,is_stmt
$C$DW$307	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$307, DW_AT_low_pc(0x00)
	.dwattr $C$DW$307, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$305, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$305, DW_AT_TI_end_line(0x31d)
	.dwattr $C$DW$305, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$305

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_canOpenInit
	.global	_ODV_Current_Max
	.global	_ODP_CommError_OverTemp_ErrCounter
	.global	_ODV_Voltage_Max
	.global	_ODP_VersionParameters
	.global	_ODV_Voltage_Min
	.global	_ODV_Temperature_Max
	.global	_CNV_CurrentRange
	.global	_mms_dict_obj100A
	.global	_ODP_CommError_OverVoltage_ErrCounter
	.global	_ODV_Current_Min
	.global	_ODP_CommError_LowVoltage_ErrCounter
	.global	_ODV_Version
	.global	_ODP_Board_BaudRate
	.global	_InitOK
	.global	_MBX_post
	.global	_I2C_Command
	.global	_SEM_pend
	.global	_MBX_pend
	.global	_CNV_CurrentUnit
	.global	_CNV_Round
	.global	_getCRC32_cpu
	.global	_ODP_Battery_Capacity
	.global	_mms_dict_obj1018_Revision_Number
	.global	__getODentry
	.global	_mms_dict_obj1018_Vendor_ID
	.global	_mms_dict_obj1018_Serial_Number
	.global	__setODentry
	.global	_ODV_Gateway_Errorcode
	.global	_ODP_CrcParameters
	.global	_BoardODdata
	.global	_ODP_OnTime
	.global	_ODP_Board_SerialNumber
	.global	_ODP_Board_RevisionNumber
	.global	_mms_dict_obj1018_Product_Code
	.global	_ODV_Gateway_Date_Time
	.global	_TSK_timerSem
	.global	_mailboxWriteParameters
	.global	_ODI_EEPROM_INDEXES

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$122	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$122, DW_AT_byte_size(0x01)
$C$DW$308	.dwtag  DW_TAG_enumerator, DW_AT_name("EVEN"), DW_AT_const_value(0x00)
$C$DW$309	.dwtag  DW_TAG_enumerator, DW_AT_name("ODD"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$122

$C$DW$T$123	.dwtag  DW_TAG_typedef, DW_AT_name("parity_t")
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$310, DW_AT_name("cob_id")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$311, DW_AT_name("rtr")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$312, DW_AT_name("len")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$312, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$313, DW_AT_name("data")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$314, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$315, DW_AT_name("csSDO")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$316, DW_AT_name("csEmergency")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$317, DW_AT_name("csSYNC")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$318, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$319, DW_AT_name("csPDO")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$320, DW_AT_name("csLSS")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$321, DW_AT_name("errCode")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$322	.dwtag  DW_TAG_member
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$322, DW_AT_name("errRegMask")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$322, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$323, DW_AT_name("active")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)

$C$DW$T$105	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$105, DW_AT_byte_size(0x18)
$C$DW$324	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$324, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$105


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$325, DW_AT_name("index")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$326, DW_AT_name("subindex")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$327, DW_AT_name("size")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$328, DW_AT_name("address")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
$C$DW$329	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$110)
$C$DW$T$124	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$329)

$C$DW$T$125	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$125, DW_AT_byte_size(0x380)
$C$DW$330	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$330, DW_AT_upper_bound(0xdf)
	.dwendtag $C$DW$T$125


$C$DW$T$126	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$126, DW_AT_byte_size(0x0c)
$C$DW$331	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$331, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$126

$C$DW$T$128	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$110)
$C$DW$T$129	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$T$129, DW_AT_address_class(0x16)
$C$DW$T$111	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$111, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x06)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$332, DW_AT_name("date")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_date")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$333, DW_AT_name("error")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_error")
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24

$C$DW$T$130	.dwtag  DW_TAG_typedef, DW_AT_name("T_StatLog")
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$130, DW_AT_language(DW_LANG_C)

$C$DW$T$131	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$T$131, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$131, DW_AT_byte_size(0x30)
$C$DW$334	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$334, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$131


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x04)
$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$335, DW_AT_name("pdata")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$336, DW_AT_name("size")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$337, DW_AT_name("address")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("T_StatIndexes")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$338	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$26)
$C$DW$T$27	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$338)
$C$DW$T$28	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x16)

$C$DW$T$134	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$134, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$134, DW_AT_byte_size(0x28)
$C$DW$339	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$339, DW_AT_upper_bound(0x09)
	.dwendtag $C$DW$T$134


$C$DW$T$135	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$135, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$135, DW_AT_byte_size(0x10)
$C$DW$340	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$340, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$135


$C$DW$T$136	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$136, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$136, DW_AT_byte_size(0x20)
$C$DW$341	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$341, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$136


$C$DW$T$137	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$137, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$137, DW_AT_byte_size(0x14)
$C$DW$342	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$342, DW_AT_upper_bound(0x04)
	.dwendtag $C$DW$T$137


$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x06)
$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$343, DW_AT_name("size")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$344, DW_AT_name("address")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$345, DW_AT_name("nb")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$346, DW_AT_name("indexes")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29

$C$DW$T$139	.dwtag  DW_TAG_typedef, DW_AT_name("T_StatPages")
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$139, DW_AT_language(DW_LANG_C)
$C$DW$347	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$139)
$C$DW$T$140	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$347)

$C$DW$T$141	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$T$141, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$141, DW_AT_byte_size(0x1e)
$C$DW$348	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$348, DW_AT_upper_bound(0x04)
	.dwendtag $C$DW$T$141


$C$DW$T$37	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$37, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x08)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$349, DW_AT_name("wListElem")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$350, DW_AT_name("wCount")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$351, DW_AT_name("fxn")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$37

$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$32	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$32, DW_AT_address_class(0x16)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)

$C$DW$T$44	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$44, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x30)
$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$352, DW_AT_name("dataQue")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$353, DW_AT_name("freeQue")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$354, DW_AT_name("dataSem")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$355, DW_AT_name("freeSem")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$356, DW_AT_name("segid")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$357, DW_AT_name("size")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$358, DW_AT_name("length")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$359, DW_AT_name("name")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$44

$C$DW$T$143	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$143, DW_AT_language(DW_LANG_C)
$C$DW$T$145	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$145, DW_AT_address_class(0x16)
$C$DW$T$146	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$T$146, DW_AT_language(DW_LANG_C)

$C$DW$T$46	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$46, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x04)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$360, DW_AT_name("next")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$361, DW_AT_name("prev")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$46

$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$45	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$45, DW_AT_address_class(0x16)

$C$DW$T$48	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$48, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x10)
$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$362, DW_AT_name("job")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$363, DW_AT_name("count")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$364, DW_AT_name("pendQ")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$365, DW_AT_name("name")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$48

$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$T$148	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$148, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$148, DW_AT_address_class(0x16)
$C$DW$T$149	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$T$149, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$150	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$150, DW_AT_language(DW_LANG_C)

$C$DW$T$34	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$366	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$33)
	.dwendtag $C$DW$T$34

$C$DW$T$35	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x16)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)

$C$DW$T$73	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$73, DW_AT_language(DW_LANG_C)
$C$DW$367	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$72)
	.dwendtag $C$DW$T$73

$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$T$75	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$75, DW_AT_language(DW_LANG_C)
$C$DW$T$77	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$77, DW_AT_language(DW_LANG_C)
$C$DW$T$87	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$87, DW_AT_language(DW_LANG_C)
$C$DW$T$86	.dwtag  DW_TAG_typedef, DW_AT_name("post_sync_t")
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$86, DW_AT_language(DW_LANG_C)
$C$DW$T$76	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)

$C$DW$T$82	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)
$C$DW$368	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$72)
$C$DW$369	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$82

$C$DW$T$83	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_address_class(0x16)
$C$DW$T$115	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$115, DW_AT_language(DW_LANG_C)
$C$DW$T$84	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$T$88	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$88, DW_AT_language(DW_LANG_C)

$C$DW$T$106	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$106, DW_AT_language(DW_LANG_C)
$C$DW$370	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$72)
$C$DW$371	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$6)
$C$DW$372	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$9)
$C$DW$373	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$106

$C$DW$T$107	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$107, DW_AT_address_class(0x16)
$C$DW$T$108	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$89	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$89, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$374	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$374, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$50	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$50, DW_AT_address_class(0x16)
$C$DW$375	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$6)
$C$DW$T$61	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$375)
$C$DW$T$62	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$T$62, DW_AT_address_class(0x16)

$C$DW$T$168	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$168, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$168, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$168, DW_AT_byte_size(0x10)
$C$DW$376	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$376, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$168

$C$DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$80	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$80, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$170	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$170, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$170, DW_AT_language(DW_LANG_C)
$C$DW$377	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$9)
$C$DW$T$59	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$377)
$C$DW$T$60	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$T$60, DW_AT_address_class(0x16)
$C$DW$T$81	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$81, DW_AT_address_class(0x16)

$C$DW$T$175	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$175, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$175, DW_AT_byte_size(0x05)
$C$DW$378	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$378, DW_AT_upper_bound(0x04)
	.dwendtag $C$DW$T$175


$C$DW$T$176	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$176, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$176, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$176, DW_AT_byte_size(0x10)
$C$DW$379	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$379, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$176

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$380	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$10)
$C$DW$T$178	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$178, DW_AT_type(*$C$DW$380)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$63	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)
$C$DW$381	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$6)
$C$DW$382	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$63

$C$DW$T$64	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$64, DW_AT_address_class(0x16)
$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$T$79	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$79, DW_AT_address_class(0x16)

$C$DW$T$90	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$383	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$72)
$C$DW$384	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$53)
$C$DW$385	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$6)
$C$DW$386	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$90

$C$DW$T$91	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x16)
$C$DW$T$92	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)
$C$DW$387	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$92)
$C$DW$T$93	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$387)
$C$DW$T$94	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_address_class(0x16)
$C$DW$T$95	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_address_class(0x16)

$C$DW$T$99	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
$C$DW$388	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$72)
$C$DW$389	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$9)
$C$DW$390	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$99

$C$DW$T$100	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$100, DW_AT_address_class(0x16)
$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$391	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$16)
$C$DW$T$197	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$197, DW_AT_type(*$C$DW$391)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$42	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$42, DW_AT_address_class(0x16)
$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)

$C$DW$T$102	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$102, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$102, DW_AT_byte_size(0x01)
$C$DW$392	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$393	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$102

$C$DW$T$103	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)

$C$DW$T$68	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$68, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$68, DW_AT_byte_size(0x01)
$C$DW$394	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$395	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$396	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$397	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$398	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$399	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$400	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$401	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$68

$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)

$C$DW$T$85	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x80)
$C$DW$402	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$402, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$85


$C$DW$T$49	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$49, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x06)
$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$403, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$404, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$405, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$406, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$407, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$408, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$49

$C$DW$T$56	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$56, DW_AT_language(DW_LANG_C)
$C$DW$409	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$56)
$C$DW$T$57	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$409)
$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)

$C$DW$T$112	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$112, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$112, DW_AT_byte_size(0x132)
$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$410, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$411, DW_AT_name("objdict")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$412, DW_AT_name("PDO_status")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$413, DW_AT_name("firstIndex")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$414, DW_AT_name("lastIndex")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$415, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$416, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$417, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$418, DW_AT_name("transfers")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$419, DW_AT_name("nodeState")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$420	.dwtag  DW_TAG_member
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$420, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$421, DW_AT_name("initialisation")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$422, DW_AT_name("preOperational")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$423, DW_AT_name("operational")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$423, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$424, DW_AT_name("stopped")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$425, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$426, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$427, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$428, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$429, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$430, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$431	.dwtag  DW_TAG_member
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$431, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$432, DW_AT_name("heartbeatError")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$433, DW_AT_name("NMTable")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$434, DW_AT_name("syncTimer")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$435	.dwtag  DW_TAG_member
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$435, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$436, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$437, DW_AT_name("post_sync")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_post_sync")
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$438, DW_AT_name("post_TPDO")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$439, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$440, DW_AT_name("toggle")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$441, DW_AT_name("canHandle")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$442, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$443, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$444, DW_AT_name("globalCallback")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$445, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$446, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$447, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$448, DW_AT_name("dcf_request")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$449, DW_AT_name("error_state")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$450, DW_AT_name("error_history_size")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$451, DW_AT_name("error_number")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$452, DW_AT_name("error_first_element")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$453, DW_AT_name("error_register")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$454, DW_AT_name("error_cobid")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$455, DW_AT_name("error_data")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$456, DW_AT_name("post_emcy")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$457, DW_AT_name("lss_transfer")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$458, DW_AT_name("eeprom_index")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$459, DW_AT_name("eeprom_size")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$112

$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$T$72	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$72, DW_AT_address_class(0x16)

$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x0e)
$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$460, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$461, DW_AT_name("event_timer")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$462, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$463, DW_AT_name("last_message")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114

$C$DW$T$54	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$54, DW_AT_language(DW_LANG_C)
$C$DW$T$55	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$55, DW_AT_address_class(0x16)

$C$DW$T$116	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$116, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$116, DW_AT_byte_size(0x14)
$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$464, DW_AT_name("nodeId")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$465, DW_AT_name("whoami")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$466, DW_AT_name("state")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$467, DW_AT_name("toggle")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$468, DW_AT_name("abortCode")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$469, DW_AT_name("index")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$470, DW_AT_name("subIndex")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$471, DW_AT_name("port")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$472, DW_AT_name("count")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$473, DW_AT_name("offset")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$474, DW_AT_name("datap")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$475, DW_AT_name("dataType")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$476, DW_AT_name("timer")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$477, DW_AT_name("Callback")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$116

$C$DW$T$66	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$T$66, DW_AT_language(DW_LANG_C)

$C$DW$T$67	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x3c)
$C$DW$478	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$478, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$67


$C$DW$T$120	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$120, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$120, DW_AT_byte_size(0x04)
$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$479, DW_AT_name("pSubindex")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$480, DW_AT_name("bSubCount")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$481, DW_AT_name("index")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$120

$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$482	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$51)
$C$DW$T$52	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$482)
$C$DW$T$53	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_address_class(0x16)

$C$DW$T$96	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$96, DW_AT_language(DW_LANG_C)
$C$DW$483	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$9)
$C$DW$484	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$79)
$C$DW$485	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$95)
	.dwendtag $C$DW$T$96

$C$DW$T$97	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_address_class(0x16)
$C$DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)

$C$DW$T$121	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$121, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$121, DW_AT_byte_size(0x08)
$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$486, DW_AT_name("bAccessType")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$487, DW_AT_name("bDataType")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$488	.dwtag  DW_TAG_member
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$488, DW_AT_name("size")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$489	.dwtag  DW_TAG_member
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$489, DW_AT_name("pObject")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$489, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$490, DW_AT_name("bProcessor")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$121

$C$DW$491	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$121)
$C$DW$T$117	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$491)
$C$DW$T$118	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$118, DW_AT_language(DW_LANG_C)
$C$DW$T$119	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$119, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$492	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$492, DW_AT_location[DW_OP_reg0]
$C$DW$493	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$493, DW_AT_location[DW_OP_reg1]
$C$DW$494	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$494, DW_AT_location[DW_OP_reg2]
$C$DW$495	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$495, DW_AT_location[DW_OP_reg3]
$C$DW$496	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$496, DW_AT_location[DW_OP_reg20]
$C$DW$497	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$497, DW_AT_location[DW_OP_reg21]
$C$DW$498	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$498, DW_AT_location[DW_OP_reg22]
$C$DW$499	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$499, DW_AT_location[DW_OP_reg23]
$C$DW$500	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$500, DW_AT_location[DW_OP_reg24]
$C$DW$501	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$501, DW_AT_location[DW_OP_reg25]
$C$DW$502	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$502, DW_AT_location[DW_OP_reg26]
$C$DW$503	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$503, DW_AT_location[DW_OP_reg28]
$C$DW$504	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$504, DW_AT_location[DW_OP_reg29]
$C$DW$505	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$505, DW_AT_location[DW_OP_reg30]
$C$DW$506	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$506, DW_AT_location[DW_OP_reg31]
$C$DW$507	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$507, DW_AT_location[DW_OP_regx 0x20]
$C$DW$508	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$508, DW_AT_location[DW_OP_regx 0x21]
$C$DW$509	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$509, DW_AT_location[DW_OP_regx 0x22]
$C$DW$510	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$510, DW_AT_location[DW_OP_regx 0x23]
$C$DW$511	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$511, DW_AT_location[DW_OP_regx 0x24]
$C$DW$512	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$512, DW_AT_location[DW_OP_regx 0x25]
$C$DW$513	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$513, DW_AT_location[DW_OP_regx 0x26]
$C$DW$514	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$514, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$515	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$515, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$516	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$516, DW_AT_location[DW_OP_reg4]
$C$DW$517	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$517, DW_AT_location[DW_OP_reg6]
$C$DW$518	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$518, DW_AT_location[DW_OP_reg8]
$C$DW$519	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$519, DW_AT_location[DW_OP_reg10]
$C$DW$520	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$520, DW_AT_location[DW_OP_reg12]
$C$DW$521	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$521, DW_AT_location[DW_OP_reg14]
$C$DW$522	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$522, DW_AT_location[DW_OP_reg16]
$C$DW$523	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$523, DW_AT_location[DW_OP_reg17]
$C$DW$524	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$524, DW_AT_location[DW_OP_reg18]
$C$DW$525	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$525, DW_AT_location[DW_OP_reg19]
$C$DW$526	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$526, DW_AT_location[DW_OP_reg5]
$C$DW$527	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$527, DW_AT_location[DW_OP_reg7]
$C$DW$528	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$528, DW_AT_location[DW_OP_reg9]
$C$DW$529	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$529, DW_AT_location[DW_OP_reg11]
$C$DW$530	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$530, DW_AT_location[DW_OP_reg13]
$C$DW$531	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$531, DW_AT_location[DW_OP_reg15]
$C$DW$532	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$532, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$533	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$533, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$534	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$534, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$535	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$535, DW_AT_location[DW_OP_regx 0x30]
$C$DW$536	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$536, DW_AT_location[DW_OP_regx 0x33]
$C$DW$537	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$537, DW_AT_location[DW_OP_regx 0x34]
$C$DW$538	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$538, DW_AT_location[DW_OP_regx 0x37]
$C$DW$539	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$539, DW_AT_location[DW_OP_regx 0x38]
$C$DW$540	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$540, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$541	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$541, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$542	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$542, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$543	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$543, DW_AT_location[DW_OP_regx 0x40]
$C$DW$544	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$544, DW_AT_location[DW_OP_regx 0x43]
$C$DW$545	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$545, DW_AT_location[DW_OP_regx 0x44]
$C$DW$546	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$546, DW_AT_location[DW_OP_regx 0x47]
$C$DW$547	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$547, DW_AT_location[DW_OP_regx 0x48]
$C$DW$548	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$548, DW_AT_location[DW_OP_regx 0x49]
$C$DW$549	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$549, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$550	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$550, DW_AT_location[DW_OP_regx 0x27]
$C$DW$551	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$551, DW_AT_location[DW_OP_regx 0x28]
$C$DW$552	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$552, DW_AT_location[DW_OP_reg27]
$C$DW$553	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$553, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

