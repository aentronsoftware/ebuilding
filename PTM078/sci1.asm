;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue May 25 14:03:56 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../sci1.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\EBuilding\PTM078")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_RS232Timer+0,32
	.bits	0,16			; _RS232Timer @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SCI1_CheckSum+0,32
	.bits	0,16			; _SCI1_CheckSum @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_RS232RecvState+0,32
	.bits	0,16			; _RS232RecvState @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Temp_Tempareture_Max+0,32
	.bits	0,16			; _Temp_Tempareture_Max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Temp_Tempareture_Min+0,32
	.bits	0,16			; _Temp_Tempareture_Min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Module1_Average_Temp_Max+0,32
	.bits	0,16			; _Module1_Average_Temp_Max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Module1_Average_Temp_Min+0,32
	.bits	0,16			; _Module1_Average_Temp_Min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_i16+0,32
	.bits	0,16			; _i16 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_CurrCounterSec+0,32
	.bits	0,16			; _CurrCounterSec @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_toto+0,32
	.bits	4608,16			; _toto @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Temp_Tempareture2+0,32
	.bits	0,16			; _Temp_Tempareture2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Temp_Tempareture3+0,32
	.bits	0,16			; _Temp_Tempareture3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SCI_MsgAvailable+0,32
	.bits	0,16			; _SCI_MsgAvailable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SCI_Available+0,32
	.bits	0,16			; _SCI_Available @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ByteStuffed+0,32
	.bits	0,16			; _ByteStuffed @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ComStarted+0,32
	.bits	0,16			; _ComStarted @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Temp_Tempareture1+0,32
	.bits	0,16			; _Temp_Tempareture1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ModuleVolt+0,32
	.bits	0,16			; _ModuleVolt @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SCI_GpioState+0,32
	.bits	0,16			; _SCI_GpioState @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_Sumtemp_Min+0,32
	.bits	0,32			; _Sumtemp_Min @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_Sumtemp_Max+0,32
	.bits	0,32			; _Sumtemp_Max @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_Low_Voltage_ResetCounter+0,32
	.bits	0,32			; _Low_Voltage_ResetCounter @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("DSP28x_usDelay")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_DSP28x_usDelay")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$1

$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Current_Average")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_ODV_Module1_Current_Average")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Module1_StandbyCurrent")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_ODP_Module1_StandbyCurrent")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MaxDeltaVoltage")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_ODV_Module1_MaxDeltaVoltage")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Module_SOC_Calibration")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_ODV_Module1_Module_SOC_Calibration")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
	.global	_RS232Timer
_RS232Timer:	.usect	".ebss",1,1,0
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("RS232Timer")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_RS232Timer")
	.dwattr $C$DW$7, DW_AT_location[DW_OP_addr _RS232Timer]
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$7, DW_AT_external
	.global	_SCI_CharLen
_SCI_CharLen:	.usect	".ebss",1,1,0
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("SCI_CharLen")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_SCI_CharLen")
	.dwattr $C$DW$8, DW_AT_location[DW_OP_addr _SCI_CharLen]
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$8, DW_AT_external
	.global	_SCI1_CheckSum
_SCI1_CheckSum:	.usect	".ebss",1,1,0
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("SCI1_CheckSum")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_SCI1_CheckSum")
	.dwattr $C$DW$9, DW_AT_location[DW_OP_addr _SCI1_CheckSum]
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("HAL_Enable")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_HAL_Enable")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
	.global	_RS232RecvState
_RS232RecvState:	.usect	".ebss",1,1,0
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("RS232RecvState")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_RS232RecvState")
	.dwattr $C$DW$11, DW_AT_location[DW_OP_addr _RS232RecvState]
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$11, DW_AT_external
$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("ODV_CommError_Set")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ODV_CommError_Set")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Temperature_Average_data")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_ODV_Temperature_Average_data")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external
$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Current")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_ODV_Module1_Current")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Voltage")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_ODV_Module1_Voltage")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MinCellVoltage")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_ODV_Module1_MinCellVoltage")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_BalancingTimeout")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_ODP_SafetyLimits_BalancingTimeout")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Module1_Module_SOC_Current")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_ODP_Module1_Module_SOC_Current")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MaxCellVoltage")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_ODV_Module1_MaxCellVoltage")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_DamagedVoltage")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_ODP_SafetyLimits_DamagedVoltage")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external
	.global	_Temp_Tempareture_Max
_Temp_Tempareture_Max:	.usect	".ebss",1,1,0
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("Temp_Tempareture_Max")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_Temp_Tempareture_Max")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_addr _Temp_Tempareture_Max]
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$21, DW_AT_external
	.global	_Temp_Tempareture_Min
_Temp_Tempareture_Min:	.usect	".ebss",1,1,0
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("Temp_Tempareture_Min")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_Temp_Tempareture_Min")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_addr _Temp_Tempareture_Min]
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$22, DW_AT_external
	.global	_Module1_Average_Temp_Max
_Module1_Average_Temp_Max:	.usect	".ebss",1,1,0
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("Module1_Average_Temp_Max")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_Module1_Average_Temp_Max")
	.dwattr $C$DW$23, DW_AT_location[DW_OP_addr _Module1_Average_Temp_Max]
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$23, DW_AT_external
	.global	_Module1_Average_Temp_Min
_Module1_Average_Temp_Min:	.usect	".ebss",1,1,0
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("Module1_Average_Temp_Min")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_Module1_Average_Temp_Min")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_addr _Module1_Average_Temp_Min]
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$24, DW_AT_external
	.global	_i16
_i16:	.usect	".ebss",1,1,0
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("i16")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_i16")
	.dwattr $C$DW$25, DW_AT_location[DW_OP_addr _i16]
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$25, DW_AT_external
	.global	_CurrCounterSec
_CurrCounterSec:	.usect	".ebss",1,1,0
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("CurrCounterSec")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_CurrCounterSec")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_addr _CurrCounterSec]
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$26, DW_AT_external
	.global	_toto
_toto:	.usect	".ebss",1,1,0
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("toto")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_toto")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_addr _toto]
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$27, DW_AT_external
	.global	_Temp_Tempareture2
_Temp_Tempareture2:	.usect	".ebss",1,1,0
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("Temp_Tempareture2")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_Temp_Tempareture2")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_addr _Temp_Tempareture2]
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$28, DW_AT_external
	.global	_Temp_Tempareture3
_Temp_Tempareture3:	.usect	".ebss",1,1,0
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("Temp_Tempareture3")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_Temp_Tempareture3")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_addr _Temp_Tempareture3]
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$29, DW_AT_external
	.global	_SCI_MsgAvailable
_SCI_MsgAvailable:	.usect	".ebss",1,1,0
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("SCI_MsgAvailable")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_SCI_MsgAvailable")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_addr _SCI_MsgAvailable]
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$195)
	.dwattr $C$DW$30, DW_AT_external
	.global	_SCI_Available
_SCI_Available:	.usect	".ebss",1,1,0
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("SCI_Available")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_SCI_Available")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_addr _SCI_Available]
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$31, DW_AT_external
	.global	_ByteStuffed
_ByteStuffed:	.usect	".ebss",1,1,0
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("ByteStuffed")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_ByteStuffed")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_addr _ByteStuffed]
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$32, DW_AT_external
	.global	_ComStarted
_ComStarted:	.usect	".ebss",1,1,0
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("ComStarted")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_ComStarted")
	.dwattr $C$DW$33, DW_AT_location[DW_OP_addr _ComStarted]
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$33, DW_AT_external
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Temperature_Min")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ODV_Temperature_Min")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external
	.global	_MinTemp
_MinTemp:	.usect	".ebss",1,1,0
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("MinTemp")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_MinTemp")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_addr _MinTemp]
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$35, DW_AT_external
	.global	_Temp_Tempareture1
_Temp_Tempareture1:	.usect	".ebss",1,1,0
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("Temp_Tempareture1")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_Temp_Tempareture1")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_addr _Temp_Tempareture1]
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$36, DW_AT_external
	.global	_ModuleVolt
_ModuleVolt:	.usect	".ebss",1,1,0
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("ModuleVolt")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ModuleVolt")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_addr _ModuleVolt]
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$37, DW_AT_external
	.global	_MaxTemp
_MaxTemp:	.usect	".ebss",1,1,0
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("MaxTemp")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_MaxTemp")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_addr _MaxTemp]
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$38, DW_AT_external
	.global	_SCI_GpioState
_SCI_GpioState:	.usect	".ebss",1,1,0
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("SCI_GpioState")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_SCI_GpioState")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_addr _SCI_GpioState]
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$39, DW_AT_external
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SciSend")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_ODV_SciSend")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Current")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ODV_Gateway_Current")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$42, DW_AT_declaration
	.dwattr $C$DW$42, DW_AT_external
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$44, DW_AT_declaration
	.dwattr $C$DW$44, DW_AT_external

$C$DW$45	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$159)
$C$DW$47	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$182)
$C$DW$48	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$77)
	.dwendtag $C$DW$45


$C$DW$49	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external
$C$DW$50	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$159)
$C$DW$51	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$182)
$C$DW$52	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$77)
	.dwendtag $C$DW$49


$C$DW$53	.dwtag  DW_TAG_subprogram, DW_AT_name("abs")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_abs")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
$C$DW$54	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$10)
	.dwendtag $C$DW$53


$C$DW$55	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_LectureADTemperature")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external
$C$DW$56	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$8)
$C$DW$57	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$55


$C$DW$58	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$58, DW_AT_declaration
	.dwattr $C$DW$58, DW_AT_external
$C$DW$59	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$180)
$C$DW$60	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$77)
	.dwendtag $C$DW$58

$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$61, DW_AT_declaration
	.dwattr $C$DW$61, DW_AT_external
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$62, DW_AT_declaration
	.dwattr $C$DW$62, DW_AT_external
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$63, DW_AT_declaration
	.dwattr $C$DW$63, DW_AT_external
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Cell_Nb")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Cell_Nb")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$64, DW_AT_declaration
	.dwattr $C$DW$64, DW_AT_external
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$65, DW_AT_declaration
	.dwattr $C$DW$65, DW_AT_external
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$66, DW_AT_declaration
	.dwattr $C$DW$66, DW_AT_external
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Mosfet_Tmin")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Mosfet_Tmin")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$67, DW_AT_declaration
	.dwattr $C$DW$67, DW_AT_external
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Mosfet_Tmax")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Mosfet_Tmax")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$68, DW_AT_declaration
	.dwattr $C$DW$68, DW_AT_external
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_T3_min")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ODP_SafetyLimits_T3_min")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_external
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_T3_max")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_ODP_SafetyLimits_T3_max")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$70, DW_AT_declaration
	.dwattr $C$DW$70, DW_AT_external
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$71, DW_AT_declaration
	.dwattr $C$DW$71, DW_AT_external
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$72, DW_AT_declaration
	.dwattr $C$DW$72, DW_AT_external

$C$DW$73	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_LectureTemperature")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_HAL_LectureTemperature")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$73, DW_AT_declaration
	.dwattr $C$DW$73, DW_AT_external
$C$DW$74	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$16)
$C$DW$75	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$73

$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Read_Inputs_16_Bit")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_ODV_Read_Inputs_16_Bit")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$76, DW_AT_declaration
	.dwattr $C$DW$76, DW_AT_external
	.global	_Sumtemp_Min
_Sumtemp_Min:	.usect	".ebss",2,1,1
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("Sumtemp_Min")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_Sumtemp_Min")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_addr _Sumtemp_Min]
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$77, DW_AT_external
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_SerialNumber")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_ODP_Board_SerialNumber")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$78, DW_AT_declaration
	.dwattr $C$DW$78, DW_AT_external
	.global	_Sumtemp_Max
_Sumtemp_Max:	.usect	".ebss",2,1,1
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("Sumtemp_Max")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_Sumtemp_Max")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_addr _Sumtemp_Max]
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$79, DW_AT_external
	.global	_SciRegs
_SciRegs:	.usect	".ebss",2,1,1
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("SciRegs")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_SciRegs")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_addr _SciRegs]
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$80, DW_AT_external

$C$DW$81	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_Round")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_CNV_Round")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$81, DW_AT_declaration
	.dwattr $C$DW$81, DW_AT_external
$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$81

	.global	_Low_Voltage_ResetCounter
_Low_Voltage_ResetCounter:	.usect	".ebss",2,1,1
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("Low_Voltage_ResetCounter")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_Low_Voltage_ResetCounter")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_addr _Low_Voltage_ResetCounter]
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$83, DW_AT_external
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Total")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_PAR_Capacity_Total")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$84, DW_AT_declaration
	.dwattr $C$DW$84, DW_AT_external
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("HAL_Current_Sum")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_HAL_Current_Sum")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$85, DW_AT_declaration
	.dwattr $C$DW$85, DW_AT_external
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("MMSConfig")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_MMSConfig")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$86, DW_AT_declaration
	.dwattr $C$DW$86, DW_AT_external
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Left")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_PAR_Capacity_Left")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$87, DW_AT_declaration
	.dwattr $C$DW$87, DW_AT_external
	.global	_SCI_CharPtr
_SCI_CharPtr:	.usect	".ebss",2,1,1
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("SCI_CharPtr")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_SCI_CharPtr")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_addr _SCI_CharPtr]
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$88, DW_AT_external
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$89, DW_AT_declaration
	.dwattr $C$DW$89, DW_AT_external
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$90, DW_AT_declaration
	.dwattr $C$DW$90, DW_AT_external
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("gaincell")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_gaincell")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$221)
	.dwattr $C$DW$91, DW_AT_declaration
	.dwattr $C$DW$91, DW_AT_external
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$92, DW_AT_declaration
	.dwattr $C$DW$92, DW_AT_external
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("SciaRegs")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_SciaRegs")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$93, DW_AT_declaration
	.dwattr $C$DW$93, DW_AT_external
$C$DW$94	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$94, DW_AT_declaration
	.dwattr $C$DW$94, DW_AT_external
$C$DW$95	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$199)
	.dwattr $C$DW$95, DW_AT_declaration
	.dwattr $C$DW$95, DW_AT_external
$C$DW$96	.dwtag  DW_TAG_variable, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$163)
	.dwattr $C$DW$96, DW_AT_declaration
	.dwattr $C$DW$96, DW_AT_external
	.global	_BaudTable
	.sect	".econst:_BaudTable"
	.clink
	.align	2
_BaudTable:
	.bits	9600,32			; _BaudTable[0]._baud @ 0
	.bits	1,16			; _BaudTable[0]._n @ 32
	.bits	4,16			; _BaudTable[0]._brr @ 48
	.bits	19200,32			; _BaudTable[1]._baud @ 64
	.bits	0,16			; _BaudTable[1]._n @ 96
	.bits	129,16			; _BaudTable[1]._brr @ 112
	.bits	38400,32			; _BaudTable[2]._baud @ 128
	.bits	0,16			; _BaudTable[2]._n @ 160
	.bits	64,16			; _BaudTable[2]._brr @ 176
	.bits	57600,32			; _BaudTable[3]._baud @ 192
	.bits	0,16			; _BaudTable[3]._n @ 224
	.bits	42,16			; _BaudTable[3]._brr @ 240
	.bits	115200,32			; _BaudTable[4]._baud @ 256
	.bits	0,16			; _BaudTable[4]._n @ 288
	.bits	21,16			; _BaudTable[4]._brr @ 304
	.bits	230400,32			; _BaudTable[5]._baud @ 320
	.bits	0,16			; _BaudTable[5]._n @ 352
	.bits	10,16			; _BaudTable[5]._brr @ 368
	.bits	250000,32			; _BaudTable[6]._baud @ 384
	.bits	0,16			; _BaudTable[6]._n @ 416
	.bits	9,16			; _BaudTable[6]._brr @ 432
	.bits	500000,32			; _BaudTable[7]._baud @ 448
	.bits	0,16			; _BaudTable[7]._n @ 480
	.bits	4,16			; _BaudTable[7]._brr @ 496

$C$DW$97	.dwtag  DW_TAG_variable, DW_AT_name("BaudTable")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_BaudTable")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_addr _BaudTable]
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$97, DW_AT_external
$C$DW$98	.dwtag  DW_TAG_variable, DW_AT_name("GpioDataRegs")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_GpioDataRegs")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$98, DW_AT_declaration
	.dwattr $C$DW$98, DW_AT_external
$C$DW$99	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Input_Scaling_Float")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_ODP_Analogue_Input_Scaling_Float")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$99, DW_AT_declaration
	.dwattr $C$DW$99, DW_AT_external
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Analogue_Input_Offset_Integer")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_ODP_Analogue_Input_Offset_Integer")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$219)
	.dwattr $C$DW$100, DW_AT_declaration
	.dwattr $C$DW$100, DW_AT_external
$C$DW$101	.dwtag  DW_TAG_variable, DW_AT_name("sci_rx_mbox")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_sci_rx_mbox")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$101, DW_AT_declaration
	.dwattr $C$DW$101, DW_AT_external
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("SysCtrlRegs")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_SysCtrlRegs")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$102, DW_AT_declaration
	.dwattr $C$DW$102, DW_AT_external
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("GpioCtrlRegs")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_GpioCtrlRegs")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$103, DW_AT_declaration
	.dwattr $C$DW$103, DW_AT_external
	.global	_SCI_MsgInRS232
_SCI_MsgInRS232:	.usect	".ebss",72,1,0
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("SCI_MsgInRS232")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_SCI_MsgInRS232")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_addr _SCI_MsgInRS232]
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$104, DW_AT_external
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("PieVectTable")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_PieVectTable")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$105, DW_AT_declaration
	.dwattr $C$DW$105, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2003612 
	.sect	".text"
	.global	_SCI1_InitGpio

$C$DW$106	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_InitGpio")
	.dwattr $C$DW$106, DW_AT_low_pc(_SCI1_InitGpio)
	.dwattr $C$DW$106, DW_AT_high_pc(0x00)
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_SCI1_InitGpio")
	.dwattr $C$DW$106, DW_AT_external
	.dwattr $C$DW$106, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$106, DW_AT_TI_begin_line(0x49)
	.dwattr $C$DW$106, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$106, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../sci1.c",line 74,column 1,is_stmt,address _SCI1_InitGpio

	.dwfde $C$DW$CIE, _SCI1_InitGpio

;***************************************************************
;* FNAME: _SCI1_InitGpio                FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SCI1_InitGpio:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../sci1.c",line 77,column 3,is_stmt
 EALLOW
	.dwpsn	file "../sci1.c",line 78,column 3,is_stmt
        MOVW      DP,#_GpioCtrlRegs+12  ; [CPU_U] 
        AND       @_GpioCtrlRegs+12,#0xff7f ; [CPU_] |78| 
	.dwpsn	file "../sci1.c",line 79,column 3,is_stmt
        AND       @_GpioCtrlRegs+13,#0xdfff ; [CPU_] |79| 
	.dwpsn	file "../sci1.c",line 83,column 3,is_stmt
 EDIS
	.dwpsn	file "../sci1.c",line 85,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$106, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$106, DW_AT_TI_end_line(0x55)
	.dwattr $C$DW$106, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$106

	.sect	".text"
	.global	_SCI1_RS232Init

$C$DW$108	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_RS232Init")
	.dwattr $C$DW$108, DW_AT_low_pc(_SCI1_RS232Init)
	.dwattr $C$DW$108, DW_AT_high_pc(0x00)
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_SCI1_RS232Init")
	.dwattr $C$DW$108, DW_AT_external
	.dwattr $C$DW$108, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$108, DW_AT_TI_begin_line(0x57)
	.dwattr $C$DW$108, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$108, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../sci1.c",line 88,column 1,is_stmt,address _SCI1_RS232Init

	.dwfde $C$DW$CIE, _SCI1_RS232Init

;***************************************************************
;* FNAME: _SCI1_RS232Init               FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_SCI1_RS232Init:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$109	.dwtag  DW_TAG_variable, DW_AT_name("baudindex")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_baudindex")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_breg20 -1]
	.dwpsn	file "../sci1.c",line 89,column 20,is_stmt
        MOVB      *-SP[1],#6,UNC        ; [CPU_] |89| 
	.dwpsn	file "../sci1.c",line 91,column 3,is_stmt
 EALLOW
	.dwpsn	file "../sci1.c",line 93,column 3,is_stmt
        MOVL      XAR4,#_SciaRegs       ; [CPU_U] |93| 
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      @_SciRegs,XAR4        ; [CPU_] |93| 
	.dwpsn	file "../sci1.c",line 94,column 3,is_stmt
        MOVW      DP,#_PieVectTable+192 ; [CPU_U] 
        MOVL      XAR4,#_RS232_RXI_Int  ; [CPU_U] |94| 
        MOVL      @_PieVectTable+192,XAR4 ; [CPU_] |94| 
	.dwpsn	file "../sci1.c",line 97,column 3,is_stmt
        MOVW      DP,#_SysCtrlRegs+11   ; [CPU_U] 
        MOVB      @_SysCtrlRegs+11,#1,UNC ; [CPU_] |97| 
	.dwpsn	file "../sci1.c",line 98,column 3,is_stmt
 EDIS
	.dwpsn	file "../sci1.c",line 100,column 3,is_stmt
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |100| 
        MOV       *+XAR4[1],#0          ; [CPU_] |100| 
	.dwpsn	file "../sci1.c",line 101,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |101| 
        MOVB      XAR0,#10              ; [CPU_] |101| 
        MOV       *+XAR4[AR0],#16448    ; [CPU_] |101| 
	.dwpsn	file "../sci1.c",line 102,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |102| 
        MOVB      XAR0,#11              ; [CPU_] |102| 
        MOV       *+XAR4[AR0],#16463    ; [CPU_] |102| 
	.dwpsn	file "../sci1.c",line 106,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |106| 
        MOVB      *+XAR4[0],#7,UNC      ; [CPU_] |106| 
	.dwpsn	file "../sci1.c",line 107,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |107| 
        CMPB      AL,#8                 ; [CPU_] |107| 
        B         $C$L1,LO              ; [CPU_] |107| 
        ; branchcc occurs ; [] |107| 
	.dwpsn	file "../sci1.c",line 107,column 29,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |107| 
$C$L1:    
	.dwpsn	file "../sci1.c",line 108,column 3,is_stmt
        MOVU      ACC,*-SP[1]           ; [CPU_] |108| 
        MOVL      XAR7,#_BaudTable+2    ; [CPU_U] |108| 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |108| 
        LSL       ACC,2                 ; [CPU_] |108| 
        ADDL      XAR7,ACC              ; [CPU_] |108| 
        MOV       AL,*XAR7              ; [CPU_] |108| 
        MOV       *+XAR4[2],AL          ; [CPU_] |108| 
	.dwpsn	file "../sci1.c",line 109,column 3,is_stmt
        MOVL      XAR7,#_BaudTable+3    ; [CPU_U] |109| 
        MOVU      ACC,*-SP[1]           ; [CPU_] |109| 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |109| 
        LSL       ACC,2                 ; [CPU_] |109| 
        ADDL      XAR7,ACC              ; [CPU_] |109| 
        MOV       AL,*XAR7              ; [CPU_] |109| 
        MOV       *+XAR4[3],AL          ; [CPU_] |109| 
	.dwpsn	file "../sci1.c",line 111,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |111| 
        MOVB      XAR0,#10              ; [CPU_] |111| 
        MOV       *+XAR4[AR0],#57344    ; [CPU_] |111| 
	.dwpsn	file "../sci1.c",line 112,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |112| 
        MOVB      XAR0,#11              ; [CPU_] |112| 
        MOV       *+XAR4[AR0],#8225     ; [CPU_] |112| 
	.dwpsn	file "../sci1.c",line 113,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |113| 
        MOVB      XAR0,#12              ; [CPU_] |113| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |113| 
	.dwpsn	file "../sci1.c",line 116,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+18   ; [CPU_U] 
        OR        @_PieCtrlRegs+18,#0x0001 ; [CPU_] |116| 
	.dwpsn	file "../sci1.c",line 119,column 3,is_stmt
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |119| 
        MOVB      *+XAR4[1],#99,UNC     ; [CPU_] |119| 
	.dwpsn	file "../sci1.c",line 120,column 3,is_stmt
        MOV       @_RS232RecvState,#0   ; [CPU_] |120| 
	.dwpsn	file "../sci1.c",line 121,column 3,is_stmt
        OR        IER,#0x0100           ; [CPU_] |121| 
	.dwpsn	file "../sci1.c",line 122,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$108, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$108, DW_AT_TI_end_line(0x7a)
	.dwattr $C$DW$108, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$108

	.sect	".text"
	.global	_RS232ReceiveEnable

$C$DW$111	.dwtag  DW_TAG_subprogram, DW_AT_name("RS232ReceiveEnable")
	.dwattr $C$DW$111, DW_AT_low_pc(_RS232ReceiveEnable)
	.dwattr $C$DW$111, DW_AT_high_pc(0x00)
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_RS232ReceiveEnable")
	.dwattr $C$DW$111, DW_AT_external
	.dwattr $C$DW$111, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$111, DW_AT_TI_begin_line(0x7c)
	.dwattr $C$DW$111, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$111, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../sci1.c",line 125,column 1,is_stmt,address _RS232ReceiveEnable

	.dwfde $C$DW$CIE, _RS232ReceiveEnable

;***************************************************************
;* FNAME: _RS232ReceiveEnable           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_RS232ReceiveEnable:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$112	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_breg20 -1]
	.dwpsn	file "../sci1.c",line 128,column 3,is_stmt
 EALLOW
	.dwpsn	file "../sci1.c",line 129,column 3,is_stmt
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |129| 
        ADDB      XAR4,#11              ; [CPU_] |129| 
        AND       *+XAR4[0],#0xffdf     ; [CPU_] |129| 
	.dwpsn	file "../sci1.c",line 130,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |130| 
        AND       *+XAR4[1],#0xffbf     ; [CPU_] |130| 
	.dwpsn	file "../sci1.c",line 131,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |131| 
        AND       *+XAR4[1],#0xfffe     ; [CPU_] |131| 
	.dwpsn	file "../sci1.c",line 132,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |132| 
        AND       *+XAR4[1],#0xfffd     ; [CPU_] |132| 
	.dwpsn	file "../sci1.c",line 133,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |133| 
        AND       *+XAR4[1],#0xffdf     ; [CPU_] |133| 
	.dwpsn	file "../sci1.c",line 134,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |134| 
        ADDB      XAR4,#10              ; [CPU_] |134| 
        AND       *+XAR4[0],#0xdfff     ; [CPU_] |134| 
	.dwpsn	file "../sci1.c",line 135,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |135| 
        ADDB      XAR4,#11              ; [CPU_] |135| 
        AND       *+XAR4[0],#0xdfff     ; [CPU_] |135| 
	.dwpsn	file "../sci1.c",line 136,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |136| 
        OR        *+XAR4[1],#0x0020     ; [CPU_] |136| 
	.dwpsn	file "../sci1.c",line 137,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |137| 
        ADDB      XAR4,#10              ; [CPU_] |137| 
        OR        *+XAR4[0],#0x2000     ; [CPU_] |137| 
	.dwpsn	file "../sci1.c",line 138,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |138| 
        ADDB      XAR4,#11              ; [CPU_] |138| 
        OR        *+XAR4[0],#0x2000     ; [CPU_] |138| 
	.dwpsn	file "../sci1.c",line 139,column 3,is_stmt
        MOVW      DP,#_GpioCtrlRegs+9   ; [CPU_U] 
        AND       @_GpioCtrlRegs+9,#0xf3ff ; [CPU_] |139| 
	.dwpsn	file "../sci1.c",line 140,column 3,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       @_GpioDataRegs+1,#0xdfff ; [CPU_] |140| 
	.dwpsn	file "../sci1.c",line 141,column 3,is_stmt
        SPM       #0                    ; [CPU_] 
        MOV       ACC,#1998             ; [CPU_] |141| 
$C$DW$113	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$113, DW_AT_low_pc(0x00)
	.dwattr $C$DW$113, DW_AT_name("_DSP28x_usDelay")
	.dwattr $C$DW$113, DW_AT_TI_call
        LCR       #_DSP28x_usDelay      ; [CPU_] |141| 
        ; call occurs [#_DSP28x_usDelay] ; [] |141| 
	.dwpsn	file "../sci1.c",line 142,column 3,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x2000 ; [CPU_] |142| 
	.dwpsn	file "../sci1.c",line 143,column 3,is_stmt
        MOVW      DP,#_GpioCtrlRegs+9   ; [CPU_U] 
        AND       AL,@_GpioCtrlRegs+9,#0xf3ff ; [CPU_] |143| 
        OR        AL,#0x0400            ; [CPU_] |143| 
        MOV       @_GpioCtrlRegs+9,AL   ; [CPU_] |143| 
	.dwpsn	file "../sci1.c",line 145,column 3,is_stmt
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |145| 
        ADDB      XAR4,#11              ; [CPU_] |145| 
        AND       AL,*+XAR4[0],#0xffe0  ; [CPU_] |145| 
        ORB       AL,#0x01              ; [CPU_] |145| 
        MOV       *+XAR4[0],AL          ; [CPU_] |145| 
	.dwpsn	file "../sci1.c",line 146,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |146| 
        OR        *+XAR4[1],#0x0002     ; [CPU_] |146| 
	.dwpsn	file "../sci1.c",line 147,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |147| 
        OR        *+XAR4[1],#0x0001     ; [CPU_] |147| 
	.dwpsn	file "../sci1.c",line 148,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |148| 
        OR        *+XAR4[1],#0x0040     ; [CPU_] |148| 
	.dwpsn	file "../sci1.c",line 149,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |149| 
        ADDB      XAR4,#11              ; [CPU_] |149| 
        OR        *+XAR4[0],#0x0020     ; [CPU_] |149| 
	.dwpsn	file "../sci1.c",line 150,column 3,is_stmt
 EDIS
	.dwpsn	file "../sci1.c",line 151,column 3,is_stmt
$C$L2:    
        MOVZ      AR5,SP                ; [CPU_U] |151| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |151| 
        MOVB      AL,#0                 ; [CPU_] |151| 
        SPM       #0                    ; [CPU_] 
        SUBB      XAR5,#1               ; [CPU_U] |151| 
$C$DW$114	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$114, DW_AT_low_pc(0x00)
	.dwattr $C$DW$114, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$114, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |151| 
        ; call occurs [#_MBX_pend] ; [] |151| 
        CMPB      AL,#0                 ; [CPU_] |151| 
        BF        $C$L2,NEQ             ; [CPU_] |151| 
        ; branchcc occurs ; [] |151| 
	.dwpsn	file "../sci1.c",line 152,column 3,is_stmt
        MOVB      *-SP[1],#70,UNC       ; [CPU_] |152| 
	.dwpsn	file "../sci1.c",line 152,column 27,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |152| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |152| 
        MOVB      AL,#0                 ; [CPU_] |152| 
        SUBB      XAR5,#1               ; [CPU_U] |152| 
$C$DW$115	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$115, DW_AT_low_pc(0x00)
	.dwattr $C$DW$115, DW_AT_name("_MBX_post")
	.dwattr $C$DW$115, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |152| 
        ; call occurs [#_MBX_post] ; [] |152| 
	.dwpsn	file "../sci1.c",line 153,column 3,is_stmt
        MOVW      DP,#_RS232RecvState   ; [CPU_U] 
        MOV       @_RS232RecvState,#0   ; [CPU_] |153| 
	.dwpsn	file "../sci1.c",line 154,column 3,is_stmt
        MOV       @_ByteStuffed,#0      ; [CPU_] |154| 
	.dwpsn	file "../sci1.c",line 155,column 3,is_stmt
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |155| 
	.dwpsn	file "../sci1.c",line 156,column 3,is_stmt
        MOV       @_ComStarted,#0       ; [CPU_] |156| 
	.dwpsn	file "../sci1.c",line 157,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$111, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$111, DW_AT_TI_end_line(0x9d)
	.dwattr $C$DW$111, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$111

	.sect	".text"
	.global	_RS232ReceiveDisable

$C$DW$117	.dwtag  DW_TAG_subprogram, DW_AT_name("RS232ReceiveDisable")
	.dwattr $C$DW$117, DW_AT_low_pc(_RS232ReceiveDisable)
	.dwattr $C$DW$117, DW_AT_high_pc(0x00)
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_RS232ReceiveDisable")
	.dwattr $C$DW$117, DW_AT_external
	.dwattr $C$DW$117, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$117, DW_AT_TI_begin_line(0xa0)
	.dwattr $C$DW$117, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$117, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../sci1.c",line 161,column 1,is_stmt,address _RS232ReceiveDisable

	.dwfde $C$DW$CIE, _RS232ReceiveDisable

;***************************************************************
;* FNAME: _RS232ReceiveDisable          FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_RS232ReceiveDisable:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../sci1.c",line 162,column 3,is_stmt
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |162| 
        ADDB      XAR4,#11              ; [CPU_] |162| 
        AND       *+XAR4[0],#0xffdf     ; [CPU_] |162| 
	.dwpsn	file "../sci1.c",line 163,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |163| 
        AND       *+XAR4[1],#0xffbf     ; [CPU_] |163| 
	.dwpsn	file "../sci1.c",line 164,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |164| 
        AND       *+XAR4[1],#0xfffe     ; [CPU_] |164| 
	.dwpsn	file "../sci1.c",line 165,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |165| 
        AND       *+XAR4[1],#0xfffd     ; [CPU_] |165| 
	.dwpsn	file "../sci1.c",line 166,column 1,is_stmt
$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$117, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$117, DW_AT_TI_end_line(0xa6)
	.dwattr $C$DW$117, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$117

	.sect	".text"
	.global	_SCI1_Update

$C$DW$119	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_Update")
	.dwattr $C$DW$119, DW_AT_low_pc(_SCI1_Update)
	.dwattr $C$DW$119, DW_AT_high_pc(0x00)
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_SCI1_Update")
	.dwattr $C$DW$119, DW_AT_external
	.dwattr $C$DW$119, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$119, DW_AT_TI_begin_line(0xa8)
	.dwattr $C$DW$119, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$119, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../sci1.c",line 168,column 23,is_stmt,address _SCI1_Update

	.dwfde $C$DW$CIE, _SCI1_Update

;***************************************************************
;* FNAME: _SCI1_Update                  FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SCI1_Update:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../sci1.c",line 169,column 3,is_stmt
        MOVW      DP,#_RS232Timer       ; [CPU_U] 
        MOV       AL,@_RS232Timer       ; [CPU_] |169| 
        BF        $C$L3,EQ              ; [CPU_] |169| 
        ; branchcc occurs ; [] |169| 
	.dwpsn	file "../sci1.c",line 170,column 5,is_stmt
        ADDB      AL,#-1                ; [CPU_] |170| 
        MOV       @_RS232Timer,AL       ; [CPU_] |170| 
        BF        $C$L3,NEQ             ; [CPU_] |170| 
        ; branchcc occurs ; [] |170| 
	.dwpsn	file "../sci1.c",line 171,column 7,is_stmt
        MOVW      DP,#_ODV_CommError_Set ; [CPU_U] 
        MOVB      @_ODV_CommError_Set,#1,UNC ; [CPU_] |171| 
	.dwpsn	file "../sci1.c",line 173,column 1,is_stmt
$C$L3:    
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$119, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$119, DW_AT_TI_end_line(0xad)
	.dwattr $C$DW$119, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$119

	.sect	".text"
	.global	_GetWord

$C$DW$121	.dwtag  DW_TAG_subprogram, DW_AT_name("GetWord")
	.dwattr $C$DW$121, DW_AT_low_pc(_GetWord)
	.dwattr $C$DW$121, DW_AT_high_pc(0x00)
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_GetWord")
	.dwattr $C$DW$121, DW_AT_external
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$121, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$121, DW_AT_TI_begin_line(0xaf)
	.dwattr $C$DW$121, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$121, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../sci1.c",line 175,column 29,is_stmt,address _GetWord

	.dwfde $C$DW$CIE, _GetWord
$C$DW$122	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _GetWord                      FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  2 SOE     *
;***************************************************************

_GetWord:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR1            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 7, 2
	.dwcfi	cfa_offset, -4
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$123, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |175| 
	.dwpsn	file "../sci1.c",line 176,column 3,is_stmt
        CMPB      AL,#35                ; [CPU_] |176| 
        B         $C$L4,HIS             ; [CPU_] |176| 
        ; branchcc occurs ; [] |176| 
	.dwpsn	file "../sci1.c",line 177,column 5,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |177| 
        MOVZ      AR0,AL                ; [CPU_] |177| 
        MOVL      XAR4,#_SCI_MsgInRS232+2 ; [CPU_U] |177| 
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |177| 
        MOVZ      AR1,AL                ; [CPU_] |177| 
        ADDB      XAR0,#1               ; [CPU_] |177| 
        MOV       ACC,*+XAR4[AR1] << #8 ; [CPU_] |177| 
        ADD       AL,*+XAR4[AR0]        ; [CPU_] |177| 
        B         $C$L5,UNC             ; [CPU_] |177| 
        ; branch occurs ; [] |177| 
$C$L4:    
	.dwpsn	file "../sci1.c",line 180,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |180| 
$C$L5:    
	.dwpsn	file "../sci1.c",line 181,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR1,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 7
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$121, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$121, DW_AT_TI_end_line(0xb5)
	.dwattr $C$DW$121, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$121

	.sect	".text"
	.global	_SetWord

$C$DW$125	.dwtag  DW_TAG_subprogram, DW_AT_name("SetWord")
	.dwattr $C$DW$125, DW_AT_low_pc(_SetWord)
	.dwattr $C$DW$125, DW_AT_high_pc(0x00)
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_SetWord")
	.dwattr $C$DW$125, DW_AT_external
	.dwattr $C$DW$125, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$125, DW_AT_TI_begin_line(0xb7)
	.dwattr $C$DW$125, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$125, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../sci1.c",line 183,column 40,is_stmt,address _SetWord

	.dwfde $C$DW$CIE, _SetWord
$C$DW$126	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_reg0]
$C$DW$127	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SetWord                      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SetWord:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_breg20 -1]
$C$DW$129	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_breg20 -2]
        MOV       *-SP[2],AH            ; [CPU_] |183| 
        MOV       *-SP[1],AL            ; [CPU_] |183| 
	.dwpsn	file "../sci1.c",line 184,column 3,is_stmt
        CMPB      AL,#35                ; [CPU_] |184| 
        B         $C$L6,HIS             ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
	.dwpsn	file "../sci1.c",line 185,column 5,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |185| 
        MOVZ      AR0,AL                ; [CPU_] |185| 
        MOVL      XAR4,#_SCI_MsgInRS232+2 ; [CPU_U] |185| 
        MOV       AL,*-SP[2]            ; [CPU_] |185| 
        LSR       AL,8                  ; [CPU_] |185| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |185| 
	.dwpsn	file "../sci1.c",line 186,column 5,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |186| 
        MOVZ      AR0,AL                ; [CPU_] |186| 
        MOV       AL,*-SP[2]            ; [CPU_] |186| 
        ANDB      AL,#0xff              ; [CPU_] |186| 
        ADDB      XAR0,#1               ; [CPU_] |186| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |186| 
	.dwpsn	file "../sci1.c",line 188,column 1,is_stmt
$C$L6:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$125, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$125, DW_AT_TI_end_line(0xbc)
	.dwattr $C$DW$125, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$125

	.sect	".text"
	.global	_SCI1_ReadMsg

$C$DW$131	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_ReadMsg")
	.dwattr $C$DW$131, DW_AT_low_pc(_SCI1_ReadMsg)
	.dwattr $C$DW$131, DW_AT_high_pc(0x00)
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_SCI1_ReadMsg")
	.dwattr $C$DW$131, DW_AT_external
	.dwattr $C$DW$131, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$131, DW_AT_TI_begin_line(0xbe)
	.dwattr $C$DW$131, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$131, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../sci1.c",line 190,column 53,is_stmt,address _SCI1_ReadMsg

	.dwfde $C$DW$CIE, _SCI1_ReadMsg
$C$DW$132	.dwtag  DW_TAG_formal_parameter, DW_AT_name("reg")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_reg")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$132, DW_AT_location[DW_OP_reg0]
$C$DW$133	.dwtag  DW_TAG_formal_parameter, DW_AT_name("buf")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_buf")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_reg12]
$C$DW$134	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SCI1_ReadMsg                 FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_SCI1_ReadMsg:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("reg")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_reg")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_breg20 -1]
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("buf")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_buf")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_breg20 -4]
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("len")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_breg20 -5]
$C$DW$138	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$138, DW_AT_location[DW_OP_breg20 -6]
$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$139, DW_AT_location[DW_OP_breg20 -7]
        MOV       *-SP[5],AH            ; [CPU_] |190| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |190| 
        MOV       *-SP[1],AL            ; [CPU_] |190| 
	.dwpsn	file "../sci1.c",line 191,column 11,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |191| 
	.dwpsn	file "../sci1.c",line 193,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |193| 
        BF        $C$L10,EQ             ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
        CMPB      AL,#9                 ; [CPU_] |193| 
        B         $C$L10,HIS            ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
	.dwpsn	file "../sci1.c",line 194,column 5,is_stmt
        ADDB      AL,#3                 ; [CPU_] |194| 
        MOVW      DP,#_SCI_MsgInRS232+1 ; [CPU_U] 
        MOV       @_SCI_MsgInRS232+1,AL ; [CPU_] |194| 
	.dwpsn	file "../sci1.c",line 195,column 5,is_stmt
        MOVB      AL,#128               ; [CPU_] |195| 
        ADD       AL,*-SP[5]            ; [CPU_] |195| 
        MOV       @_SCI_MsgInRS232+2,AL ; [CPU_] |195| 
	.dwpsn	file "../sci1.c",line 196,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |196| 
        CMPB      AL,#8                 ; [CPU_] |196| 
        BF        $C$L7,NEQ             ; [CPU_] |196| 
        ; branchcc occurs ; [] |196| 
	.dwpsn	file "../sci1.c",line 196,column 19,is_stmt
        MOVB      @_SCI_MsgInRS232+2,#135,UNC ; [CPU_] |196| 
$C$L7:    
	.dwpsn	file "../sci1.c",line 197,column 5,is_stmt
        MOV       @_SCI_MsgInRS232+3,#0 ; [CPU_] |197| 
	.dwpsn	file "../sci1.c",line 198,column 5,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |198| 
        MOV       @_SCI_MsgInRS232+4,AL ; [CPU_] |198| 
	.dwpsn	file "../sci1.c",line 199,column 5,is_stmt
        B         $C$L9,UNC             ; [CPU_] |199| 
        ; branch occurs ; [] |199| 
$C$L8:    
	.dwpsn	file "../sci1.c",line 200,column 7,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |200| 
        MOV       AL,*XAR4++            ; [CPU_] |200| 
        MOV       *-SP[7],AL            ; [CPU_] |200| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |200| 
	.dwpsn	file "../sci1.c",line 201,column 7,is_stmt
        MOVZ      AR0,*-SP[6]           ; [CPU_] |201| 
        LSR       AL,8                  ; [CPU_] |201| 
        MOVL      XAR4,#_SCI_MsgInRS232+2 ; [CPU_U] |201| 
        ADDB      XAR0,#3               ; [CPU_] |201| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |201| 
	.dwpsn	file "../sci1.c",line 202,column 7,is_stmt
        MOVZ      AR0,*-SP[6]           ; [CPU_] |202| 
        MOV       AL,*-SP[7]            ; [CPU_] |202| 
        ANDB      AL,#0xff              ; [CPU_] |202| 
        ADDB      XAR0,#4               ; [CPU_] |202| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |202| 
	.dwpsn	file "../sci1.c",line 203,column 7,is_stmt
        ADD       *-SP[6],#2            ; [CPU_] |203| 
$C$L9:    
	.dwpsn	file "../sci1.c",line 199,column 12,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |199| 
        CMP       AL,*-SP[6]            ; [CPU_] |199| 
        B         $C$L8,HI              ; [CPU_] |199| 
        ; branchcc occurs ; [] |199| 
	.dwpsn	file "../sci1.c",line 205,column 5,is_stmt
        MOVL      XAR4,#_SCI_MsgInRS232 ; [CPU_U] |205| 
$C$DW$140	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$140, DW_AT_low_pc(0x00)
	.dwattr $C$DW$140, DW_AT_name("_SCI1_SendMessage")
	.dwattr $C$DW$140, DW_AT_TI_call
        LCR       #_SCI1_SendMessage    ; [CPU_] |205| 
        ; call occurs [#_SCI1_SendMessage] ; [] |205| 
	.dwpsn	file "../sci1.c",line 206,column 5,is_stmt
        MOVW      DP,#_RS232Timer       ; [CPU_U] 
        MOVB      @_RS232Timer,#20,UNC  ; [CPU_] |206| 
	.dwpsn	file "../sci1.c",line 207,column 3,is_stmt
        B         $C$L11,UNC            ; [CPU_] |207| 
        ; branch occurs ; [] |207| 
$C$L10:    
	.dwpsn	file "../sci1.c",line 209,column 5,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |209| 
	.dwpsn	file "../sci1.c",line 210,column 1,is_stmt
$C$L11:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$141	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$141, DW_AT_low_pc(0x00)
	.dwattr $C$DW$141, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$131, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$131, DW_AT_TI_end_line(0xd2)
	.dwattr $C$DW$131, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$131

	.sect	".text"
	.global	_SCI1_WriteMsg

$C$DW$142	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_WriteMsg")
	.dwattr $C$DW$142, DW_AT_low_pc(_SCI1_WriteMsg)
	.dwattr $C$DW$142, DW_AT_high_pc(0x00)
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_SCI1_WriteMsg")
	.dwattr $C$DW$142, DW_AT_external
	.dwattr $C$DW$142, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$142, DW_AT_TI_begin_line(0xd4)
	.dwattr $C$DW$142, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$142, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../sci1.c",line 212,column 54,is_stmt,address _SCI1_WriteMsg

	.dwfde $C$DW$CIE, _SCI1_WriteMsg
$C$DW$143	.dwtag  DW_TAG_formal_parameter, DW_AT_name("reg")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_reg")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg0]
$C$DW$144	.dwtag  DW_TAG_formal_parameter, DW_AT_name("buf")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_buf")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$144, DW_AT_location[DW_OP_reg12]
$C$DW$145	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$145, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SCI1_WriteMsg                FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_SCI1_WriteMsg:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$146	.dwtag  DW_TAG_variable, DW_AT_name("reg")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_reg")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$146, DW_AT_location[DW_OP_breg20 -1]
$C$DW$147	.dwtag  DW_TAG_variable, DW_AT_name("buf")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_buf")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$147, DW_AT_location[DW_OP_breg20 -4]
$C$DW$148	.dwtag  DW_TAG_variable, DW_AT_name("len")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$148, DW_AT_location[DW_OP_breg20 -5]
$C$DW$149	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$149, DW_AT_location[DW_OP_breg20 -6]
$C$DW$150	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$150, DW_AT_location[DW_OP_breg20 -7]
        MOV       *-SP[5],AH            ; [CPU_] |212| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |212| 
        MOV       *-SP[1],AL            ; [CPU_] |212| 
	.dwpsn	file "../sci1.c",line 213,column 11,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |213| 
	.dwpsn	file "../sci1.c",line 215,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |215| 
        BF        $C$L15,EQ             ; [CPU_] |215| 
        ; branchcc occurs ; [] |215| 
        CMPB      AL,#9                 ; [CPU_] |215| 
        B         $C$L15,HIS            ; [CPU_] |215| 
        ; branchcc occurs ; [] |215| 
	.dwpsn	file "../sci1.c",line 216,column 5,is_stmt
        ADDB      AL,#3                 ; [CPU_] |216| 
        MOVW      DP,#_SCI_MsgInRS232+1 ; [CPU_U] 
        MOV       @_SCI_MsgInRS232+1,AL ; [CPU_] |216| 
	.dwpsn	file "../sci1.c",line 217,column 5,is_stmt
        MOVB      AL,#144               ; [CPU_] |217| 
        ADD       AL,*-SP[5]            ; [CPU_] |217| 
        MOV       @_SCI_MsgInRS232+2,AL ; [CPU_] |217| 
	.dwpsn	file "../sci1.c",line 218,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |218| 
        CMPB      AL,#8                 ; [CPU_] |218| 
        BF        $C$L12,NEQ            ; [CPU_] |218| 
        ; branchcc occurs ; [] |218| 
	.dwpsn	file "../sci1.c",line 218,column 19,is_stmt
        MOVB      @_SCI_MsgInRS232+2,#151,UNC ; [CPU_] |218| 
$C$L12:    
	.dwpsn	file "../sci1.c",line 219,column 5,is_stmt
        MOV       @_SCI_MsgInRS232+3,#0 ; [CPU_] |219| 
	.dwpsn	file "../sci1.c",line 220,column 5,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |220| 
        MOV       @_SCI_MsgInRS232+4,AL ; [CPU_] |220| 
	.dwpsn	file "../sci1.c",line 221,column 5,is_stmt
        B         $C$L14,UNC            ; [CPU_] |221| 
        ; branch occurs ; [] |221| 
$C$L13:    
	.dwpsn	file "../sci1.c",line 222,column 7,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |222| 
        MOV       AL,*XAR4++            ; [CPU_] |222| 
        MOV       *-SP[7],AL            ; [CPU_] |222| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |222| 
	.dwpsn	file "../sci1.c",line 223,column 7,is_stmt
        MOVZ      AR0,*-SP[6]           ; [CPU_] |223| 
        LSR       AL,8                  ; [CPU_] |223| 
        MOVL      XAR4,#_SCI_MsgInRS232+2 ; [CPU_U] |223| 
        ADDB      XAR0,#3               ; [CPU_] |223| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |223| 
	.dwpsn	file "../sci1.c",line 224,column 7,is_stmt
        MOVZ      AR0,*-SP[6]           ; [CPU_] |224| 
        MOV       AL,*-SP[7]            ; [CPU_] |224| 
        ANDB      AL,#0xff              ; [CPU_] |224| 
        ADDB      XAR0,#4               ; [CPU_] |224| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |224| 
	.dwpsn	file "../sci1.c",line 225,column 7,is_stmt
        ADD       *-SP[6],#2            ; [CPU_] |225| 
$C$L14:    
	.dwpsn	file "../sci1.c",line 221,column 12,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |221| 
        CMP       AL,*-SP[6]            ; [CPU_] |221| 
        B         $C$L13,HI             ; [CPU_] |221| 
        ; branchcc occurs ; [] |221| 
	.dwpsn	file "../sci1.c",line 227,column 5,is_stmt
        MOVL      XAR4,#_SCI_MsgInRS232 ; [CPU_U] |227| 
$C$DW$151	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$151, DW_AT_low_pc(0x00)
	.dwattr $C$DW$151, DW_AT_name("_SCI1_SendMessage")
	.dwattr $C$DW$151, DW_AT_TI_call
        LCR       #_SCI1_SendMessage    ; [CPU_] |227| 
        ; call occurs [#_SCI1_SendMessage] ; [] |227| 
	.dwpsn	file "../sci1.c",line 229,column 1,is_stmt
$C$L15:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$152	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$152, DW_AT_low_pc(0x00)
	.dwattr $C$DW$152, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$142, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$142, DW_AT_TI_end_line(0xe5)
	.dwattr $C$DW$142, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$142

	.sect	".text"
	.global	_SCI1_Command

$C$DW$153	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_Command")
	.dwattr $C$DW$153, DW_AT_low_pc(_SCI1_Command)
	.dwattr $C$DW$153, DW_AT_high_pc(0x00)
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_SCI1_Command")
	.dwattr $C$DW$153, DW_AT_external
	.dwattr $C$DW$153, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$153, DW_AT_TI_begin_line(0xe8)
	.dwattr $C$DW$153, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$153, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../sci1.c",line 232,column 24,is_stmt,address _SCI1_Command

	.dwfde $C$DW$CIE, _SCI1_Command

;***************************************************************
;* FNAME: _SCI1_Command                 FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_SCI1_Command:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$154	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$213)
	.dwattr $C$DW$154, DW_AT_location[DW_OP_breg20 -4]
$C$DW$155	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$155, DW_AT_location[DW_OP_breg20 -5]
$C$DW$156	.dwtag  DW_TAG_variable, DW_AT_name("val")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_val")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$156, DW_AT_location[DW_OP_breg20 -8]
	.dwpsn	file "../sci1.c",line 235,column 3,is_stmt
        B         $C$L45,UNC            ; [CPU_] |235| 
        ; branch occurs ; [] |235| 
$C$L16:    
	.dwpsn	file "../sci1.c",line 239,column 7,is_stmt
        MOV       *-SP[4],#35885        ; [CPU_] |239| 
	.dwpsn	file "../sci1.c",line 240,column 7,is_stmt
        MOV       *-SP[3],#45460        ; [CPU_] |240| 
	.dwpsn	file "../sci1.c",line 241,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |241| 
        MOVB      AL,#130               ; [CPU_] |241| 
        MOVB      AH,#4                 ; [CPU_] |241| 
        SUBB      XAR4,#4               ; [CPU_U] |241| 
$C$DW$157	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$157, DW_AT_low_pc(0x00)
	.dwattr $C$DW$157, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$157, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |241| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |241| 
	.dwpsn	file "../sci1.c",line 242,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |242| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |242| 
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$158, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |242| 
        ; call occurs [#_SEM_pend] ; [] |242| 
	.dwpsn	file "../sci1.c",line 243,column 7,is_stmt
        MOV       *-SP[4],#41845        ; [CPU_] |243| 
	.dwpsn	file "../sci1.c",line 244,column 7,is_stmt
        MOV       *-SP[3],#58895        ; [CPU_] |244| 
	.dwpsn	file "../sci1.c",line 245,column 7,is_stmt
        MOVB      AL,#252               ; [CPU_] |245| 
        MOVB      AH,#4                 ; [CPU_] |245| 
        MOVZ      AR4,SP                ; [CPU_U] |245| 
        SUBB      XAR4,#4               ; [CPU_U] |245| 
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$159, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |245| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |245| 
	.dwpsn	file "../sci1.c",line 246,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |246| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |246| 
$C$DW$160	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$160, DW_AT_low_pc(0x00)
	.dwattr $C$DW$160, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$160, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |246| 
        ; call occurs [#_SEM_pend] ; [] |246| 
	.dwpsn	file "../sci1.c",line 247,column 7,is_stmt
        MOV       *-SP[4],#4096         ; [CPU_] |247| 
	.dwpsn	file "../sci1.c",line 248,column 7,is_stmt
        MOVB      AL,#12                ; [CPU_] |248| 
        MOVB      AH,#1                 ; [CPU_] |248| 
        MOVZ      AR4,SP                ; [CPU_U] |248| 
        SUBB      XAR4,#4               ; [CPU_U] |248| 
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$161, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |248| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |248| 
	.dwpsn	file "../sci1.c",line 249,column 7,is_stmt
        MOVB      AL,#200               ; [CPU_] |249| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |249| 
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$162, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |249| 
        ; call occurs [#_SEM_pend] ; [] |249| 
	.dwpsn	file "../sci1.c",line 250,column 7,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |250| 
	.dwpsn	file "../sci1.c",line 251,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |251| 
        ; branch occurs ; [] |251| 
$C$L17:    
	.dwpsn	file "../sci1.c",line 253,column 7,is_stmt
        MOV       *-SP[4],#14591        ; [CPU_] |253| 
	.dwpsn	file "../sci1.c",line 254,column 7,is_stmt
        MOV       *-SP[3],#49152        ; [CPU_] |254| 
	.dwpsn	file "../sci1.c",line 255,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |255| 
        MOVB      AL,#81                ; [CPU_] |255| 
        MOVB      AH,#3                 ; [CPU_] |255| 
        SUBB      XAR4,#4               ; [CPU_U] |255| 
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$163, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |255| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |255| 
	.dwpsn	file "../sci1.c",line 256,column 7,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |256| 
	.dwpsn	file "../sci1.c",line 257,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |257| 
        ; branch occurs ; [] |257| 
$C$L18:    
	.dwpsn	file "../sci1.c",line 259,column 7,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |259| 
	.dwpsn	file "../sci1.c",line 260,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |260| 
        MOVB      AL,#250               ; [CPU_] |260| 
        MOVB      AH,#1                 ; [CPU_] |260| 
        SUBB      XAR4,#4               ; [CPU_U] |260| 
$C$DW$164	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$164, DW_AT_low_pc(0x00)
	.dwattr $C$DW$164, DW_AT_name("_SCI1_ReadMsg")
	.dwattr $C$DW$164, DW_AT_TI_call
        LCR       #_SCI1_ReadMsg        ; [CPU_] |260| 
        ; call occurs [#_SCI1_ReadMsg] ; [] |260| 
	.dwpsn	file "../sci1.c",line 261,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |261| 
        ; branch occurs ; [] |261| 
$C$L19:    
	.dwpsn	file "../sci1.c",line 263,column 7,is_stmt
        MOV       *-SP[4],#1792         ; [CPU_] |263| 
	.dwpsn	file "../sci1.c",line 264,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |264| 
        MOVB      AL,#82                ; [CPU_] |264| 
        MOVB      AH,#1                 ; [CPU_] |264| 
        SUBB      XAR4,#4               ; [CPU_U] |264| 
$C$DW$165	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$165, DW_AT_low_pc(0x00)
	.dwattr $C$DW$165, DW_AT_name("_SCI1_ReadMsg")
	.dwattr $C$DW$165, DW_AT_TI_call
        LCR       #_SCI1_ReadMsg        ; [CPU_] |264| 
        ; call occurs [#_SCI1_ReadMsg] ; [] |264| 
	.dwpsn	file "../sci1.c",line 265,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |265| 
        ; branch occurs ; [] |265| 
$C$L20:    
	.dwpsn	file "../sci1.c",line 267,column 7,is_stmt
        MOV       *-SP[4],#768          ; [CPU_] |267| 
	.dwpsn	file "../sci1.c",line 268,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |268| 
        MOVB      AL,#244               ; [CPU_] |268| 
        MOVB      AH,#1                 ; [CPU_] |268| 
        SUBB      XAR4,#4               ; [CPU_U] |268| 
$C$DW$166	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$166, DW_AT_low_pc(0x00)
	.dwattr $C$DW$166, DW_AT_name("_SCI1_ReadMsg")
	.dwattr $C$DW$166, DW_AT_TI_call
        LCR       #_SCI1_ReadMsg        ; [CPU_] |268| 
        ; call occurs [#_SCI1_ReadMsg] ; [] |268| 
	.dwpsn	file "../sci1.c",line 269,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |269| 
        ; branch occurs ; [] |269| 
$C$L21:    
	.dwpsn	file "../sci1.c",line 271,column 7,is_stmt
        MOV       *-SP[4],#256          ; [CPU_] |271| 
	.dwpsn	file "../sci1.c",line 272,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |272| 
        MOVB      AL,#198               ; [CPU_] |272| 
        MOVB      AH,#1                 ; [CPU_] |272| 
        SUBB      XAR4,#4               ; [CPU_U] |272| 
$C$DW$167	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$167, DW_AT_low_pc(0x00)
	.dwattr $C$DW$167, DW_AT_name("_SCI1_ReadMsg")
	.dwattr $C$DW$167, DW_AT_TI_call
        LCR       #_SCI1_ReadMsg        ; [CPU_] |272| 
        ; call occurs [#_SCI1_ReadMsg] ; [] |272| 
	.dwpsn	file "../sci1.c",line 273,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |273| 
        ; branch occurs ; [] |273| 
$C$L22:    
	.dwpsn	file "../sci1.c",line 275,column 7,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |275| 
	.dwpsn	file "../sci1.c",line 276,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |276| 
        MOVB      AL,#40                ; [CPU_] |276| 
        MOVB      AH,#1                 ; [CPU_] |276| 
        SUBB      XAR4,#4               ; [CPU_U] |276| 
$C$DW$168	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$168, DW_AT_low_pc(0x00)
	.dwattr $C$DW$168, DW_AT_name("_SCI1_ReadMsg")
	.dwattr $C$DW$168, DW_AT_TI_call
        LCR       #_SCI1_ReadMsg        ; [CPU_] |276| 
        ; call occurs [#_SCI1_ReadMsg] ; [] |276| 
	.dwpsn	file "../sci1.c",line 277,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |277| 
        ; branch occurs ; [] |277| 
$C$L23:    
	.dwpsn	file "../sci1.c",line 279,column 7,is_stmt
        MOVW      DP,#_SCI_GpioState    ; [CPU_U] 
        MOV       AL,@_SCI_GpioState    ; [CPU_] |279| 
        MOV       *-SP[4],AL            ; [CPU_] |279| 
	.dwpsn	file "../sci1.c",line 280,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |280| 
        MOVB      AH,#1                 ; [CPU_] |280| 
        MOVB      AL,#121               ; [CPU_] |280| 
        SUBB      XAR4,#4               ; [CPU_U] |280| 
$C$DW$169	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$169, DW_AT_low_pc(0x00)
	.dwattr $C$DW$169, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$169, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |280| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |280| 
	.dwpsn	file "../sci1.c",line 281,column 7,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |281| 
	.dwpsn	file "../sci1.c",line 282,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |282| 
        ; branch occurs ; [] |282| 
$C$L24:    
	.dwpsn	file "../sci1.c",line 284,column 7,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |284| 
	.dwpsn	file "../sci1.c",line 285,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |285| 
        MOVB      AL,#121               ; [CPU_] |285| 
        MOVB      AH,#1                 ; [CPU_] |285| 
        SUBB      XAR4,#4               ; [CPU_U] |285| 
$C$DW$170	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$170, DW_AT_low_pc(0x00)
	.dwattr $C$DW$170, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$170, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |285| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |285| 
	.dwpsn	file "../sci1.c",line 286,column 7,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |286| 
	.dwpsn	file "../sci1.c",line 287,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |287| 
        ; branch occurs ; [] |287| 
$C$L25:    
	.dwpsn	file "../sci1.c",line 289,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOVIZ     R1H,#17096            ; [CPU_] |289| 
        I16TOF32  R0H,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |289| 
$C$DW$171	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$171, DW_AT_low_pc(0x00)
	.dwattr $C$DW$171, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$171, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |289| 
        ; call occurs [#FS$$DIV] ; [] |289| 
        MOVIZ     R1H,#18303            ; [CPU_] |289| 
        MOVXI     R1H,#65280            ; [CPU_] |289| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |289| 
        MOVIZ     R1H,#16544            ; [CPU_] |289| 
$C$DW$172	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$172, DW_AT_low_pc(0x00)
	.dwattr $C$DW$172, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$172, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |289| 
        ; call occurs [#FS$$DIV] ; [] |289| 
        MOV32     *-SP[8],R0H           ; [CPU_] |289| 
	.dwpsn	file "../sci1.c",line 290,column 7,is_stmt
$C$DW$173	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$173, DW_AT_low_pc(0x00)
	.dwattr $C$DW$173, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$173, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |290| 
        ; call occurs [#_CNV_Round] ; [] |290| 
        AND       AL,#0xfffc            ; [CPU_] |290| 
        MOV       *-SP[5],AL            ; [CPU_] |290| 
	.dwpsn	file "../sci1.c",line 291,column 7,is_stmt
        MOV       *-SP[4],AL            ; [CPU_] |291| 
	.dwpsn	file "../sci1.c",line 292,column 7,is_stmt
        MOVB      AH,#2                 ; [CPU_] |292| 
        MOVZ      AR4,SP                ; [CPU_U] |292| 
        MOVB      AL,#142               ; [CPU_] |292| 
        SUBB      XAR4,#4               ; [CPU_U] |292| 
$C$DW$174	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$174, DW_AT_low_pc(0x00)
	.dwattr $C$DW$174, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$174, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |292| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |292| 
	.dwpsn	file "../sci1.c",line 293,column 7,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |293| 
	.dwpsn	file "../sci1.c",line 294,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |294| 
        ; branch occurs ; [] |294| 
$C$L26:    
	.dwpsn	file "../sci1.c",line 296,column 7,is_stmt
        MOV       *-SP[4],#32768        ; [CPU_] |296| 
	.dwpsn	file "../sci1.c",line 297,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |297| 
        MOVB      AL,#107               ; [CPU_] |297| 
        MOVB      AH,#2                 ; [CPU_] |297| 
        SUBB      XAR4,#4               ; [CPU_U] |297| 
$C$DW$175	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$175, DW_AT_low_pc(0x00)
	.dwattr $C$DW$175, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$175, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |297| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |297| 
	.dwpsn	file "../sci1.c",line 298,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |298| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |298| 
$C$DW$176	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$176, DW_AT_low_pc(0x00)
	.dwattr $C$DW$176, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$176, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |298| 
        ; call occurs [#_SEM_pend] ; [] |298| 
	.dwpsn	file "../sci1.c",line 299,column 7,is_stmt
        MOV       *-SP[4],#16383        ; [CPU_] |299| 
	.dwpsn	file "../sci1.c",line 300,column 7,is_stmt
        MOV       *-SP[3],#65280        ; [CPU_] |300| 
	.dwpsn	file "../sci1.c",line 301,column 7,is_stmt
        MOV       *-SP[2],#64000        ; [CPU_] |301| 
	.dwpsn	file "../sci1.c",line 302,column 7,is_stmt
        MOVB      AL,#3                 ; [CPU_] |302| 
        MOVB      AH,#5                 ; [CPU_] |302| 
        MOVZ      AR4,SP                ; [CPU_U] |302| 
        SUBB      XAR4,#4               ; [CPU_U] |302| 
$C$DW$177	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$177, DW_AT_low_pc(0x00)
	.dwattr $C$DW$177, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$177, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |302| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |302| 
	.dwpsn	file "../sci1.c",line 303,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |303| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |303| 
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$178, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |303| 
        ; call occurs [#_SEM_pend] ; [] |303| 
	.dwpsn	file "../sci1.c",line 304,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Cell_Nb ; [CPU_U] 
        MOV       ACC,@_ODP_SafetyLimits_Cell_Nb << #8 ; [CPU_] |304| 
        MOV       *-SP[4],AL            ; [CPU_] |304| 
	.dwpsn	file "../sci1.c",line 305,column 7,is_stmt
        MOVB      AH,#1                 ; [CPU_] |305| 
        MOVZ      AR4,SP                ; [CPU_U] |305| 
        MOVB      AL,#13                ; [CPU_] |305| 
        SUBB      XAR4,#4               ; [CPU_U] |305| 
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$179, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |305| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |305| 
	.dwpsn	file "../sci1.c",line 306,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |306| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |306| 
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$180, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |306| 
        ; call occurs [#_SEM_pend] ; [] |306| 
	.dwpsn	file "../sci1.c",line 307,column 7,is_stmt
        MOV       *-SP[4],#12800        ; [CPU_] |307| 
	.dwpsn	file "../sci1.c",line 308,column 7,is_stmt
        MOVB      AL,#40                ; [CPU_] |308| 
        MOVB      AH,#1                 ; [CPU_] |308| 
        MOVZ      AR4,SP                ; [CPU_U] |308| 
        SUBB      XAR4,#4               ; [CPU_U] |308| 
$C$DW$181	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$181, DW_AT_low_pc(0x00)
	.dwattr $C$DW$181, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$181, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |308| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |308| 
	.dwpsn	file "../sci1.c",line 309,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |309| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |309| 
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$182, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |309| 
        ; call occurs [#_SEM_pend] ; [] |309| 
	.dwpsn	file "../sci1.c",line 310,column 7,is_stmt
        MOV       *-SP[4],#16128        ; [CPU_] |310| 
	.dwpsn	file "../sci1.c",line 312,column 7,is_stmt
        MOVB      AL,#120               ; [CPU_] |312| 
        MOVB      AH,#2                 ; [CPU_] |312| 
        MOVZ      AR4,SP                ; [CPU_U] |312| 
        SUBB      XAR4,#4               ; [CPU_U] |312| 
$C$DW$183	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$183, DW_AT_low_pc(0x00)
	.dwattr $C$DW$183, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$183, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |312| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |312| 
	.dwpsn	file "../sci1.c",line 313,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |313| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |313| 
$C$DW$184	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$184, DW_AT_low_pc(0x00)
	.dwattr $C$DW$184, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$184, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |313| 
        ; call occurs [#_SEM_pend] ; [] |313| 
	.dwpsn	file "../sci1.c",line 314,column 7,is_stmt
        MOV       *-SP[4],#14591        ; [CPU_] |314| 
	.dwpsn	file "../sci1.c",line 315,column 7,is_stmt
        MOV       *-SP[3],#49152        ; [CPU_] |315| 
	.dwpsn	file "../sci1.c",line 316,column 7,is_stmt
        MOVB      AL,#81                ; [CPU_] |316| 
        MOVB      AH,#3                 ; [CPU_] |316| 
        MOVZ      AR4,SP                ; [CPU_U] |316| 
        SUBB      XAR4,#4               ; [CPU_U] |316| 
$C$DW$185	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$185, DW_AT_low_pc(0x00)
	.dwattr $C$DW$185, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$185, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |316| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |316| 
	.dwpsn	file "../sci1.c",line 317,column 7,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |317| 
        MOVB      AL,#1                 ; [CPU_] |317| 
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$186, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |317| 
        ; call occurs [#_SEM_pend] ; [] |317| 
	.dwpsn	file "../sci1.c",line 318,column 7,is_stmt
        MOV       *-SP[4],#4608         ; [CPU_] |318| 
	.dwpsn	file "../sci1.c",line 321,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_BalancingTimeout ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_BalancingTimeout ; [CPU_] |321| 
        MOV       *-SP[5],AL            ; [CPU_] |321| 
	.dwpsn	file "../sci1.c",line 322,column 7,is_stmt
        BF        $C$L27,NEQ            ; [CPU_] |322| 
        ; branchcc occurs ; [] |322| 
	.dwpsn	file "../sci1.c",line 322,column 22,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |322| 
        B         $C$L35,UNC            ; [CPU_] |322| 
        ; branch occurs ; [] |322| 
$C$L27:    
	.dwpsn	file "../sci1.c",line 322,column 40,is_stmt
        CMPB      AL,#1                 ; [CPU_] |322| 
        BF        $C$L28,NEQ            ; [CPU_] |322| 
        ; branchcc occurs ; [] |322| 
	.dwpsn	file "../sci1.c",line 322,column 55,is_stmt
        MOV       *-SP[4],#8192         ; [CPU_] |322| 
        B         $C$L35,UNC            ; [CPU_] |322| 
        ; branch occurs ; [] |322| 
$C$L28:    
	.dwpsn	file "../sci1.c",line 322,column 78,is_stmt
        CMPB      AL,#2                 ; [CPU_] |322| 
        BF        $C$L29,NEQ            ; [CPU_] |322| 
        ; branchcc occurs ; [] |322| 
	.dwpsn	file "../sci1.c",line 322,column 93,is_stmt
        MOV       *-SP[4],#12288        ; [CPU_] |322| 
        B         $C$L35,UNC            ; [CPU_] |322| 
        ; branch occurs ; [] |322| 
$C$L29:    
	.dwpsn	file "../sci1.c",line 323,column 12,is_stmt
        CMPB      AL,#5                 ; [CPU_] |323| 
        B         $C$L30,HI             ; [CPU_] |323| 
        ; branchcc occurs ; [] |323| 
	.dwpsn	file "../sci1.c",line 323,column 27,is_stmt
        MOV       *-SP[4],#16384        ; [CPU_] |323| 
        B         $C$L35,UNC            ; [CPU_] |323| 
        ; branch occurs ; [] |323| 
$C$L30:    
	.dwpsn	file "../sci1.c",line 323,column 50,is_stmt
        CMPB      AL,#10                ; [CPU_] |323| 
        B         $C$L31,HI             ; [CPU_] |323| 
        ; branchcc occurs ; [] |323| 
	.dwpsn	file "../sci1.c",line 323,column 66,is_stmt
        MOV       *-SP[4],#20480        ; [CPU_] |323| 
        B         $C$L35,UNC            ; [CPU_] |323| 
        ; branch occurs ; [] |323| 
$C$L31:    
	.dwpsn	file "../sci1.c",line 323,column 89,is_stmt
        CMPB      AL,#15                ; [CPU_] |323| 
        B         $C$L32,HI             ; [CPU_] |323| 
        ; branchcc occurs ; [] |323| 
	.dwpsn	file "../sci1.c",line 323,column 105,is_stmt
        MOV       *-SP[4],#24576        ; [CPU_] |323| 
        B         $C$L35,UNC            ; [CPU_] |323| 
        ; branch occurs ; [] |323| 
$C$L32:    
	.dwpsn	file "../sci1.c",line 324,column 12,is_stmt
        CMPB      AL,#20                ; [CPU_] |324| 
        B         $C$L33,HI             ; [CPU_] |324| 
        ; branchcc occurs ; [] |324| 
	.dwpsn	file "../sci1.c",line 324,column 28,is_stmt
        MOV       *-SP[4],#28672        ; [CPU_] |324| 
        B         $C$L35,UNC            ; [CPU_] |324| 
        ; branch occurs ; [] |324| 
$C$L33:    
	.dwpsn	file "../sci1.c",line 324,column 51,is_stmt
        CMPB      AL,#30                ; [CPU_] |324| 
        B         $C$L34,HI             ; [CPU_] |324| 
        ; branchcc occurs ; [] |324| 
	.dwpsn	file "../sci1.c",line 324,column 67,is_stmt
        MOV       *-SP[4],#32768        ; [CPU_] |324| 
        B         $C$L35,UNC            ; [CPU_] |324| 
        ; branch occurs ; [] |324| 
$C$L34:    
	.dwpsn	file "../sci1.c",line 324,column 90,is_stmt
        MOV       *-SP[4],#36864        ; [CPU_] |324| 
$C$L35:    
	.dwpsn	file "../sci1.c",line 325,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |325| 
        MOVB      AL,#19                ; [CPU_] |325| 
        MOVB      AH,#1                 ; [CPU_] |325| 
        SUBB      XAR4,#4               ; [CPU_U] |325| 
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$187, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |325| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |325| 
	.dwpsn	file "../sci1.c",line 326,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |326| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |326| 
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$188, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |326| 
        ; call occurs [#_SEM_pend] ; [] |326| 
	.dwpsn	file "../sci1.c",line 327,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_DamagedVoltage ; [CPU_U] 
        MOVIZ     R1H,#16672            ; [CPU_] |327| 
        UI16TOF32 R0H,@_ODP_SafetyLimits_DamagedVoltage ; [CPU_] |327| 
$C$DW$189	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$189, DW_AT_low_pc(0x00)
	.dwattr $C$DW$189, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$189, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |327| 
        ; call occurs [#FS$$DIV] ; [] |327| 
        MOVIZ     R1H,#16179            ; [CPU_] |327| 
        MOVXI     R1H,#13107            ; [CPU_] |327| 
        SUBF32    R0H,R0H,R1H           ; [CPU_] |327| 
        NOP       ; [CPU_] 
        MOV32     *-SP[8],R0H           ; [CPU_] |327| 
	.dwpsn	file "../sci1.c",line 328,column 7,is_stmt
        MOVIZ     R1H,#15564            ; [CPU_] |328| 
        MOVXI     R1H,#52429            ; [CPU_] |328| 
$C$DW$190	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$190, DW_AT_low_pc(0x00)
	.dwattr $C$DW$190, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$190, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |328| 
        ; call occurs [#FS$$DIV] ; [] |328| 
$C$DW$191	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$191, DW_AT_low_pc(0x00)
	.dwattr $C$DW$191, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$191, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |328| 
        ; call occurs [#_CNV_Round] ; [] |328| 
        MOV       *-SP[5],AL            ; [CPU_] |328| 
	.dwpsn	file "../sci1.c",line 329,column 7,is_stmt
        MOV       ACC,*-SP[5] << #9     ; [CPU_] |329| 
        MOV       *-SP[4],AL            ; [CPU_] |329| 
	.dwpsn	file "../sci1.c",line 330,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        SETC      SXM                   ; [CPU_] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |330| 
        ADDB      AL,#10                ; [CPU_] |330| 
        MOV       ACC,AL                ; [CPU_] |330| 
        MOV32     R0H,ACC               ; [CPU_] |330| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#17096            ; [CPU_] |330| 
        I32TOF32  R0H,R0H               ; [CPU_] |330| 
$C$DW$192	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$192, DW_AT_low_pc(0x00)
	.dwattr $C$DW$192, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$192, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |330| 
        ; call occurs [#FS$$DIV] ; [] |330| 
        ADDF32    R0H,R0H,#49152        ; [CPU_] |330| 
        NOP       ; [CPU_] 
        MOV32     *-SP[8],R0H           ; [CPU_] |330| 
	.dwpsn	file "../sci1.c",line 331,column 7,is_stmt
        MOVIZ     R1H,#15564            ; [CPU_] |331| 
        MOVXI     R1H,#52429            ; [CPU_] |331| 
$C$DW$193	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$193, DW_AT_low_pc(0x00)
	.dwattr $C$DW$193, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$193, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |331| 
        ; call occurs [#FS$$DIV] ; [] |331| 
$C$DW$194	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$194, DW_AT_low_pc(0x00)
	.dwattr $C$DW$194, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$194, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |331| 
        ; call occurs [#_CNV_Round] ; [] |331| 
        MOV       *-SP[5],AL            ; [CPU_] |331| 
	.dwpsn	file "../sci1.c",line 332,column 7,is_stmt
        MOV       ACC,*-SP[5] << #1     ; [CPU_] |332| 
        ADD       *-SP[4],AL            ; [CPU_] |332| 
	.dwpsn	file "../sci1.c",line 333,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_DamagedVoltage ; [CPU_U] 
        MOVIZ     R1H,#16672            ; [CPU_] |333| 
        UI16TOF32 R0H,@_ODP_SafetyLimits_DamagedVoltage ; [CPU_] |333| 
$C$DW$195	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$195, DW_AT_low_pc(0x00)
	.dwattr $C$DW$195, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$195, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |333| 
        ; call occurs [#FS$$DIV] ; [] |333| 
        MOVIZ     R1H,#18303            ; [CPU_] |333| 
        MOVXI     R1H,#65280            ; [CPU_] |333| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |333| 
        MOVIZ     R1H,#16544            ; [CPU_] |333| 
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$196, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |333| 
        ; call occurs [#FS$$DIV] ; [] |333| 
        MOV32     *-SP[8],R0H           ; [CPU_] |333| 
	.dwpsn	file "../sci1.c",line 334,column 7,is_stmt
$C$DW$197	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$197, DW_AT_low_pc(0x00)
	.dwattr $C$DW$197, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$197, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |334| 
        ; call occurs [#_CNV_Round] ; [] |334| 
        AND       AL,#0xfffc            ; [CPU_] |334| 
        MOV       *-SP[5],AL            ; [CPU_] |334| 
	.dwpsn	file "../sci1.c",line 335,column 7,is_stmt
        MOV       *-SP[3],AL            ; [CPU_] |335| 
	.dwpsn	file "../sci1.c",line 336,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOVIZ     R1H,#17096            ; [CPU_] |336| 
        I16TOF32  R0H,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |336| 
$C$DW$198	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$198, DW_AT_low_pc(0x00)
	.dwattr $C$DW$198, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$198, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |336| 
        ; call occurs [#FS$$DIV] ; [] |336| 
        MOVIZ     R1H,#18303            ; [CPU_] |336| 
        MOVXI     R1H,#65280            ; [CPU_] |336| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |336| 
        MOVIZ     R1H,#16544            ; [CPU_] |336| 
$C$DW$199	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$199, DW_AT_low_pc(0x00)
	.dwattr $C$DW$199, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$199, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |336| 
        ; call occurs [#FS$$DIV] ; [] |336| 
        MOV32     *-SP[8],R0H           ; [CPU_] |336| 
	.dwpsn	file "../sci1.c",line 337,column 7,is_stmt
        ADDF32    R0H,R0H,#16512        ; [CPU_] |337| 
$C$DW$200	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$200, DW_AT_low_pc(0x00)
	.dwattr $C$DW$200, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$200, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |337| 
        ; call occurs [#_CNV_Round] ; [] |337| 
        AND       AL,#0xfffc            ; [CPU_] |337| 
        MOV       *-SP[5],AL            ; [CPU_] |337| 
	.dwpsn	file "../sci1.c",line 338,column 7,is_stmt
        MOV       *-SP[2],AL            ; [CPU_] |338| 
	.dwpsn	file "../sci1.c",line 339,column 7,is_stmt
        MOVB      AH,#6                 ; [CPU_] |339| 
        MOVZ      AR4,SP                ; [CPU_U] |339| 
        MOVB      AL,#140               ; [CPU_] |339| 
        SUBB      XAR4,#4               ; [CPU_U] |339| 
$C$DW$201	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$201, DW_AT_low_pc(0x00)
	.dwattr $C$DW$201, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$201, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |339| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |339| 
	.dwpsn	file "../sci1.c",line 340,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |340| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |340| 
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$202, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |340| 
        ; call occurs [#_SEM_pend] ; [] |340| 
	.dwpsn	file "../sci1.c",line 342,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Tmax ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |342| 
        MOV       AL,@_ODP_SafetyLimits_Resistor_Tmax ; [CPU_] |342| 
$C$DW$203	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$203, DW_AT_low_pc(0x00)
	.dwattr $C$DW$203, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$203, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |342| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |342| 
        MOV       *-SP[5],AL            ; [CPU_] |342| 
	.dwpsn	file "../sci1.c",line 343,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |343| 
        MOV       *-SP[4],AL            ; [CPU_] |343| 
	.dwpsn	file "../sci1.c",line 344,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Tmin ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |344| 
        MOV       AL,@_ODP_SafetyLimits_Resistor_Tmin ; [CPU_] |344| 
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$204, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |344| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |344| 
        MOV       *-SP[5],AL            ; [CPU_] |344| 
	.dwpsn	file "../sci1.c",line 345,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |345| 
        MOV       *-SP[3],AL            ; [CPU_] |345| 
	.dwpsn	file "../sci1.c",line 347,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_T3_max ; [CPU_U] 
        MOVB      AH,#1                 ; [CPU_] |347| 
        MOV       AL,@_ODP_SafetyLimits_T3_max ; [CPU_] |347| 
$C$DW$205	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$205, DW_AT_low_pc(0x00)
	.dwattr $C$DW$205, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$205, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |347| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |347| 
        MOV       *-SP[5],AL            ; [CPU_] |347| 
	.dwpsn	file "../sci1.c",line 348,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |348| 
        MOV       *-SP[2],AL            ; [CPU_] |348| 
	.dwpsn	file "../sci1.c",line 349,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_T3_min ; [CPU_U] 
        MOVB      AH,#1                 ; [CPU_] |349| 
        MOV       AL,@_ODP_SafetyLimits_T3_min ; [CPU_] |349| 
$C$DW$206	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$206, DW_AT_low_pc(0x00)
	.dwattr $C$DW$206, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$206, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |349| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |349| 
        MOV       *-SP[5],AL            ; [CPU_] |349| 
	.dwpsn	file "../sci1.c",line 350,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |350| 
        MOV       *-SP[1],AL            ; [CPU_] |350| 
	.dwpsn	file "../sci1.c",line 351,column 7,is_stmt
        MOVB      AH,#8                 ; [CPU_] |351| 
        MOVZ      AR4,SP                ; [CPU_U] |351| 
        MOVB      AL,#146               ; [CPU_] |351| 
        SUBB      XAR4,#4               ; [CPU_U] |351| 
$C$DW$207	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$207, DW_AT_low_pc(0x00)
	.dwattr $C$DW$207, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$207, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |351| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |351| 
	.dwpsn	file "../sci1.c",line 352,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |352| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |352| 
$C$DW$208	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$208, DW_AT_low_pc(0x00)
	.dwattr $C$DW$208, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$208, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |352| 
        ; call occurs [#_SEM_pend] ; [] |352| 
	.dwpsn	file "../sci1.c",line 353,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderCurrent ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_UnderCurrent ; [CPU_] |353| 
        MOVW      DP,#_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_] |353| 
$C$DW$209	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$209, DW_AT_low_pc(0x00)
	.dwattr $C$DW$209, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$209, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |353| 
        ; call occurs [#FS$$DIV] ; [] |353| 
        MOVW      DP,#_ODP_Analogue_Input_Scaling_Float+6 ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Input_Scaling_Float+6 ; [CPU_] |353| 
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$210, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |353| 
        ; call occurs [#FS$$DIV] ; [] |353| 
        MOVW      DP,#_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_U] 
        I32TOF32  R1H,@_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_] |353| 
        NOP       ; [CPU_] 
        SUBF32    R0H,R0H,R1H           ; [CPU_] |353| 
$C$DW$211	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$211, DW_AT_low_pc(0x00)
	.dwattr $C$DW$211, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$211, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |353| 
        ; call occurs [#_CNV_Round] ; [] |353| 
        MOV       *-SP[5],AL            ; [CPU_] |353| 
	.dwpsn	file "../sci1.c",line 355,column 7,is_stmt
        CMP       AL,#32768             ; [CPU_] |355| 
        B         $C$L36,LOS            ; [CPU_] |355| 
        ; branchcc occurs ; [] |355| 
	.dwpsn	file "../sci1.c",line 355,column 26,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |355| 
$C$L36:    
	.dwpsn	file "../sci1.c",line 356,column 7,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |356| 
        TBIT      *+XAR4[0],#6          ; [CPU_] |356| 
        BF        $C$L37,TC             ; [CPU_] |356| 
        ; branchcc occurs ; [] |356| 
	.dwpsn	file "../sci1.c",line 356,column 34,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |356| 
$C$L37:    
	.dwpsn	file "../sci1.c",line 357,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |357| 
        MOV       *-SP[4],AL            ; [CPU_] |357| 
	.dwpsn	file "../sci1.c",line 358,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Overcurrent ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_Overcurrent ; [CPU_] |358| 
        MOVW      DP,#_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_] |358| 
$C$DW$212	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$212, DW_AT_low_pc(0x00)
	.dwattr $C$DW$212, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$212, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |358| 
        ; call occurs [#FS$$DIV] ; [] |358| 
        MOVW      DP,#_ODP_Analogue_Input_Scaling_Float+6 ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Input_Scaling_Float+6 ; [CPU_] |358| 
$C$DW$213	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$213, DW_AT_low_pc(0x00)
	.dwattr $C$DW$213, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$213, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |358| 
        ; call occurs [#FS$$DIV] ; [] |358| 
        MOVW      DP,#_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_U] 
        I32TOF32  R1H,@_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_] |358| 
        NOP       ; [CPU_] 
        SUBF32    R0H,R0H,R1H           ; [CPU_] |358| 
$C$DW$214	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$214, DW_AT_low_pc(0x00)
	.dwattr $C$DW$214, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$214, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |358| 
        ; call occurs [#_CNV_Round] ; [] |358| 
        MOV       *-SP[5],AL            ; [CPU_] |358| 
	.dwpsn	file "../sci1.c",line 360,column 7,is_stmt
        CMP       AL,#32768             ; [CPU_] |360| 
        B         $C$L38,HIS            ; [CPU_] |360| 
        ; branchcc occurs ; [] |360| 
	.dwpsn	file "../sci1.c",line 360,column 26,is_stmt
        MOV       *-SP[5],#65535        ; [CPU_] |360| 
$C$L38:    
	.dwpsn	file "../sci1.c",line 361,column 7,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |361| 
        TBIT      *+XAR4[0],#6          ; [CPU_] |361| 
        BF        $C$L39,TC             ; [CPU_] |361| 
        ; branchcc occurs ; [] |361| 
	.dwpsn	file "../sci1.c",line 361,column 34,is_stmt
        MOV       *-SP[5],#65532        ; [CPU_] |361| 
$C$L39:    
	.dwpsn	file "../sci1.c",line 362,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |362| 
        MOV       *-SP[3],AL            ; [CPU_] |362| 
	.dwpsn	file "../sci1.c",line 363,column 7,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |363| 
	.dwpsn	file "../sci1.c",line 364,column 7,is_stmt
        MOV       *-SP[1],#65532        ; [CPU_] |364| 
	.dwpsn	file "../sci1.c",line 365,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |365| 
        MOVB      AH,#8                 ; [CPU_] |365| 
        MOVB      AL,#154               ; [CPU_] |365| 
        SUBB      XAR4,#4               ; [CPU_U] |365| 
$C$DW$215	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$215, DW_AT_low_pc(0x00)
	.dwattr $C$DW$215, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$215, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |365| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |365| 
	.dwpsn	file "../sci1.c",line 366,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |366| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |366| 
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$216, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |366| 
        ; call occurs [#_SEM_pend] ; [] |366| 
	.dwpsn	file "../sci1.c",line 367,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Mosfet_Tmax ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |367| 
        MOV       AL,@_ODP_SafetyLimits_Mosfet_Tmax ; [CPU_] |367| 
$C$DW$217	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$217, DW_AT_low_pc(0x00)
	.dwattr $C$DW$217, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$217, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |367| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |367| 
        MOV       *-SP[5],AL            ; [CPU_] |367| 
	.dwpsn	file "../sci1.c",line 368,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |368| 
        MOV       *-SP[4],AL            ; [CPU_] |368| 
	.dwpsn	file "../sci1.c",line 369,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Mosfet_Tmin ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |369| 
        MOV       AL,@_ODP_SafetyLimits_Mosfet_Tmin ; [CPU_] |369| 
$C$DW$218	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$218, DW_AT_low_pc(0x00)
	.dwattr $C$DW$218, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$218, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |369| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |369| 
        MOV       *-SP[5],AL            ; [CPU_] |369| 
	.dwpsn	file "../sci1.c",line 370,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |370| 
        MOV       *-SP[3],AL            ; [CPU_] |370| 
	.dwpsn	file "../sci1.c",line 371,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderCurrent ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_UnderCurrent ; [CPU_] |371| 
        MOVW      DP,#_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_] |371| 
$C$DW$219	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$219, DW_AT_low_pc(0x00)
	.dwattr $C$DW$219, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$219, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |371| 
        ; call occurs [#FS$$DIV] ; [] |371| 
        MOVW      DP,#_ODP_Analogue_Input_Scaling_Float+6 ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Input_Scaling_Float+6 ; [CPU_] |371| 
$C$DW$220	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$220, DW_AT_low_pc(0x00)
	.dwattr $C$DW$220, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$220, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |371| 
        ; call occurs [#FS$$DIV] ; [] |371| 
        MOVW      DP,#_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_U] 
        I32TOF32  R1H,@_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_] |371| 
        NOP       ; [CPU_] 
        SUBF32    R0H,R0H,R1H           ; [CPU_] |371| 
$C$DW$221	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$221, DW_AT_low_pc(0x00)
	.dwattr $C$DW$221, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$221, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |371| 
        ; call occurs [#_CNV_Round] ; [] |371| 
        MOV       *-SP[5],AL            ; [CPU_] |371| 
	.dwpsn	file "../sci1.c",line 373,column 7,is_stmt
        CMP       AL,#32768             ; [CPU_] |373| 
        B         $C$L40,LOS            ; [CPU_] |373| 
        ; branchcc occurs ; [] |373| 
	.dwpsn	file "../sci1.c",line 373,column 26,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |373| 
$C$L40:    
	.dwpsn	file "../sci1.c",line 374,column 7,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |374| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |374| 
        BF        $C$L41,TC             ; [CPU_] |374| 
        ; branchcc occurs ; [] |374| 
	.dwpsn	file "../sci1.c",line 374,column 32,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |374| 
$C$L41:    
	.dwpsn	file "../sci1.c",line 375,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |375| 
        MOV       *-SP[2],AL            ; [CPU_] |375| 
	.dwpsn	file "../sci1.c",line 376,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Overcurrent ; [CPU_U] 
        I16TOF32  R0H,@_ODP_SafetyLimits_Overcurrent ; [CPU_] |376| 
        MOVW      DP,#_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_] |376| 
$C$DW$222	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$222, DW_AT_low_pc(0x00)
	.dwattr $C$DW$222, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$222, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |376| 
        ; call occurs [#FS$$DIV] ; [] |376| 
        MOVW      DP,#_ODP_Analogue_Input_Scaling_Float+6 ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Input_Scaling_Float+6 ; [CPU_] |376| 
$C$DW$223	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$223, DW_AT_low_pc(0x00)
	.dwattr $C$DW$223, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$223, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |376| 
        ; call occurs [#FS$$DIV] ; [] |376| 
        MOVW      DP,#_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_U] 
        I32TOF32  R1H,@_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_] |376| 
        NOP       ; [CPU_] 
        SUBF32    R0H,R0H,R1H           ; [CPU_] |376| 
$C$DW$224	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$224, DW_AT_low_pc(0x00)
	.dwattr $C$DW$224, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$224, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |376| 
        ; call occurs [#_CNV_Round] ; [] |376| 
        MOV       *-SP[5],AL            ; [CPU_] |376| 
	.dwpsn	file "../sci1.c",line 378,column 7,is_stmt
        CMP       AL,#32768             ; [CPU_] |378| 
        B         $C$L42,HIS            ; [CPU_] |378| 
        ; branchcc occurs ; [] |378| 
	.dwpsn	file "../sci1.c",line 378,column 26,is_stmt
        MOV       *-SP[5],#65535        ; [CPU_] |378| 
$C$L42:    
	.dwpsn	file "../sci1.c",line 379,column 7,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |379| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |379| 
        BF        $C$L43,TC             ; [CPU_] |379| 
        ; branchcc occurs ; [] |379| 
	.dwpsn	file "../sci1.c",line 379,column 32,is_stmt
        MOV       *-SP[5],#65532        ; [CPU_] |379| 
$C$L43:    
	.dwpsn	file "../sci1.c",line 380,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |380| 
        MOV       *-SP[1],AL            ; [CPU_] |380| 
	.dwpsn	file "../sci1.c",line 381,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |381| 
        MOVB      AH,#8                 ; [CPU_] |381| 
        MOVB      AL,#162               ; [CPU_] |381| 
        SUBB      XAR4,#4               ; [CPU_U] |381| 
$C$DW$225	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$225, DW_AT_low_pc(0x00)
	.dwattr $C$DW$225, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$225, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |381| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |381| 
	.dwpsn	file "../sci1.c",line 382,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |382| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |382| 
$C$DW$226	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$226, DW_AT_low_pc(0x00)
	.dwattr $C$DW$226, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$226, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |382| 
        ; call occurs [#_SEM_pend] ; [] |382| 
	.dwpsn	file "../sci1.c",line 384,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_T3_max ; [CPU_U] 
        MOVB      AH,#1                 ; [CPU_] |384| 
        MOV       AL,@_ODP_SafetyLimits_T3_max ; [CPU_] |384| 
$C$DW$227	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$227, DW_AT_low_pc(0x00)
	.dwattr $C$DW$227, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$227, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |384| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |384| 
        MOV       *-SP[5],AL            ; [CPU_] |384| 
	.dwpsn	file "../sci1.c",line 385,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |385| 
        MOV       *-SP[4],AL            ; [CPU_] |385| 
	.dwpsn	file "../sci1.c",line 386,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_T3_min ; [CPU_U] 
        MOVB      AH,#1                 ; [CPU_] |386| 
        MOV       AL,@_ODP_SafetyLimits_T3_min ; [CPU_] |386| 
$C$DW$228	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$228, DW_AT_low_pc(0x00)
	.dwattr $C$DW$228, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$228, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |386| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |386| 
        MOV       *-SP[5],AL            ; [CPU_] |386| 
	.dwpsn	file "../sci1.c",line 387,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |387| 
        MOV       *-SP[3],AL            ; [CPU_] |387| 
	.dwpsn	file "../sci1.c",line 389,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_T3_max ; [CPU_U] 
        MOVB      AH,#1                 ; [CPU_] |389| 
        MOV       AL,@_ODP_SafetyLimits_T3_max ; [CPU_] |389| 
$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$229, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |389| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |389| 
        MOV       *-SP[5],AL            ; [CPU_] |389| 
	.dwpsn	file "../sci1.c",line 390,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |390| 
        MOV       *-SP[2],AL            ; [CPU_] |390| 
	.dwpsn	file "../sci1.c",line 391,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_T3_min ; [CPU_U] 
        MOVB      AH,#1                 ; [CPU_] |391| 
        MOV       AL,@_ODP_SafetyLimits_T3_min ; [CPU_] |391| 
$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_name("_HAL_LectureADTemperature")
	.dwattr $C$DW$230, DW_AT_TI_call
        LCR       #_HAL_LectureADTemperature ; [CPU_] |391| 
        ; call occurs [#_HAL_LectureADTemperature] ; [] |391| 
        MOV       *-SP[5],AL            ; [CPU_] |391| 
	.dwpsn	file "../sci1.c",line 392,column 7,is_stmt
        AND       AL,*-SP[5],#0xfffc    ; [CPU_] |392| 
        MOV       *-SP[1],AL            ; [CPU_] |392| 
	.dwpsn	file "../sci1.c",line 393,column 7,is_stmt
        MOVB      AH,#8                 ; [CPU_] |393| 
        MOVZ      AR4,SP                ; [CPU_U] |393| 
        MOVB      AL,#170               ; [CPU_] |393| 
        SUBB      XAR4,#4               ; [CPU_U] |393| 
$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$231, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |393| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |393| 
	.dwpsn	file "../sci1.c",line 394,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |394| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |394| 
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$232, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |394| 
        ; call occurs [#_SEM_pend] ; [] |394| 
	.dwpsn	file "../sci1.c",line 395,column 7,is_stmt
        MOV       *-SP[4],#14591        ; [CPU_] |395| 
	.dwpsn	file "../sci1.c",line 396,column 7,is_stmt
        MOV       *-SP[3],#49152        ; [CPU_] |396| 
	.dwpsn	file "../sci1.c",line 397,column 7,is_stmt
        MOVB      AL,#81                ; [CPU_] |397| 
        MOVB      AH,#3                 ; [CPU_] |397| 
        MOVZ      AR4,SP                ; [CPU_U] |397| 
        SUBB      XAR4,#4               ; [CPU_U] |397| 
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$233, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |397| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |397| 
	.dwpsn	file "../sci1.c",line 398,column 7,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |398| 
	.dwpsn	file "../sci1.c",line 399,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |399| 
        ; branch occurs ; [] |399| 
$C$L44:    
	.dwpsn	file "../sci1.c",line 401,column 7,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |401| 
	.dwpsn	file "../sci1.c",line 402,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |402| 
        MOVB      AL,#2                 ; [CPU_] |402| 
        MOVB      AH,#1                 ; [CPU_] |402| 
        SUBB      XAR4,#4               ; [CPU_U] |402| 
$C$DW$234	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$234, DW_AT_low_pc(0x00)
	.dwattr $C$DW$234, DW_AT_name("_SCI1_ReadMsg")
	.dwattr $C$DW$234, DW_AT_TI_call
        LCR       #_SCI1_ReadMsg        ; [CPU_] |402| 
        ; call occurs [#_SCI1_ReadMsg] ; [] |402| 
	.dwpsn	file "../sci1.c",line 403,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |403| 
        ; branch occurs ; [] |403| 
$C$L45:    
	.dwpsn	file "../sci1.c",line 235,column 3,is_stmt
        MOVW      DP,#_SCI_MsgInRS232   ; [CPU_U] 
        MOV       AL,@_SCI_MsgInRS232   ; [CPU_] |235| 
        CMPB      AL,#72                ; [CPU_] |235| 
        B         $C$L47,GT             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#72                ; [CPU_] |235| 
        BF        $C$L17,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#69                ; [CPU_] |235| 
        B         $C$L46,GT             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#69                ; [CPU_] |235| 
        BF        $C$L19,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#65                ; [CPU_] |235| 
        BF        $C$L20,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#67                ; [CPU_] |235| 
        BF        $C$L21,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#68                ; [CPU_] |235| 
        BF        $C$L22,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        B         $C$L49,UNC            ; [CPU_] |235| 
        ; branch occurs ; [] |235| 
$C$L46:    
        CMPB      AL,#70                ; [CPU_] |235| 
        BF        $C$L26,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#71                ; [CPU_] |235| 
        BF        $C$L44,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        B         $C$L49,UNC            ; [CPU_] |235| 
        ; branch occurs ; [] |235| 
$C$L47:    
        CMPB      AL,#82                ; [CPU_] |235| 
        B         $C$L48,GT             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#82                ; [CPU_] |235| 
        BF        $C$L16,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#73                ; [CPU_] |235| 
        BF        $C$L23,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#80                ; [CPU_] |235| 
        BF        $C$L24,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#81                ; [CPU_] |235| 
        BF        $C$L25,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        B         $C$L49,UNC            ; [CPU_] |235| 
        ; branch occurs ; [] |235| 
$C$L48:    
        CMPB      AL,#83                ; [CPU_] |235| 
        BF        $C$L18,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
        CMPB      AL,#255               ; [CPU_] |235| 
        BF        $C$L49,EQ             ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
	.dwpsn	file "../sci1.c",line 407,column 1,is_stmt
$C$L49:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$235	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$235, DW_AT_low_pc(0x00)
	.dwattr $C$DW$235, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$153, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$153, DW_AT_TI_end_line(0x197)
	.dwattr $C$DW$153, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$153

	.sect	".text"
	.global	_SCI1_Receive

$C$DW$236	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_Receive")
	.dwattr $C$DW$236, DW_AT_low_pc(_SCI1_Receive)
	.dwattr $C$DW$236, DW_AT_high_pc(0x00)
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_SCI1_Receive")
	.dwattr $C$DW$236, DW_AT_external
	.dwattr $C$DW$236, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$236, DW_AT_TI_begin_line(0x199)
	.dwattr $C$DW$236, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$236, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../sci1.c",line 409,column 24,is_stmt,address _SCI1_Receive

	.dwfde $C$DW$CIE, _SCI1_Receive

;***************************************************************
;* FNAME: _SCI1_Receive                 FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter, 16 Auto,  0 SOE     *
;***************************************************************

_SCI1_Receive:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$237	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_breg20 -5]
$C$DW$238	.dwtag  DW_TAG_variable, DW_AT_name("max")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_max")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_breg20 -6]
$C$DW$239	.dwtag  DW_TAG_variable, DW_AT_name("min")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_min")
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$239, DW_AT_location[DW_OP_breg20 -7]
$C$DW$240	.dwtag  DW_TAG_variable, DW_AT_name("val")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_val")
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$240, DW_AT_location[DW_OP_breg20 -8]
$C$DW$241	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$241, DW_AT_location[DW_OP_breg20 -9]
$C$DW$242	.dwtag  DW_TAG_variable, DW_AT_name("val2")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_val2")
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$242, DW_AT_location[DW_OP_breg20 -10]
$C$DW$243	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$213)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_breg20 -14]
$C$DW$244	.dwtag  DW_TAG_variable, DW_AT_name("sum")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_sum")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_breg20 -16]
	.dwpsn	file "../sci1.c",line 412,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |412| 
        MOVL      *-SP[16],ACC          ; [CPU_] |412| 
	.dwpsn	file "../sci1.c",line 413,column 3,is_stmt
        B         $C$L102,UNC           ; [CPU_] |413| 
        ; branch occurs ; [] |413| 

$C$DW$245	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$246	.dwtag  DW_TAG_variable, DW_AT_name("Temp_Tempareture21")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_Temp_Tempareture21")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_breg20 -17]
$C$DW$247	.dwtag  DW_TAG_variable, DW_AT_name("Temp_Tempareture22")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_Temp_Tempareture22")
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$247, DW_AT_location[DW_OP_breg20 -18]
$C$DW$248	.dwtag  DW_TAG_variable, DW_AT_name("Temp_Tempareture23")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_Temp_Tempareture23")
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$248, DW_AT_location[DW_OP_breg20 -19]
$C$L50:    
	.dwpsn	file "../sci1.c",line 418,column 14,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |418| 
	.dwpsn	file "../sci1.c",line 418,column 18,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |418| 
        CMPB      AL,#4                 ; [CPU_] |418| 
        B         $C$L52,HIS            ; [CPU_] |418| 
        ; branchcc occurs ; [] |418| 
$C$L51:    
	.dwpsn	file "../sci1.c",line 419,column 11,is_stmt
$C$DW$249	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$249, DW_AT_low_pc(0x00)
	.dwattr $C$DW$249, DW_AT_name("_GetWord")
	.dwattr $C$DW$249, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |419| 
        ; call occurs [#_GetWord] ; [] |419| 
        MOVZ      AR0,*-SP[5]           ; [CPU_] |419| 
        MOVZ      AR4,SP                ; [CPU_U] |419| 
        SUBB      XAR4,#14              ; [CPU_U] |419| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |419| 
	.dwpsn	file "../sci1.c",line 418,column 22,is_stmt
        INC       *-SP[5]               ; [CPU_] |418| 
	.dwpsn	file "../sci1.c",line 418,column 18,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |418| 
        CMPB      AL,#4                 ; [CPU_] |418| 
        B         $C$L51,LO             ; [CPU_] |418| 
        ; branchcc occurs ; [] |418| 
$C$L52:    
	.dwpsn	file "../sci1.c",line 422,column 9,is_stmt
        MOV       AL,*-SP[14]           ; [CPU_] |422| 
        BF        $C$L104,EQ            ; [CPU_] |422| 
        ; branchcc occurs ; [] |422| 
	.dwpsn	file "../sci1.c",line 425,column 11,is_stmt
	.dwpsn	file "../sci1.c",line 429,column 11,is_stmt
        TBIT      *-SP[14],#12          ; [CPU_] |429| 
        BF        $C$L104,NTC           ; [CPU_] |429| 
        ; branchcc occurs ; [] |429| 
	.dwpsn	file "../sci1.c",line 442,column 7,is_stmt
        B         $C$L104,UNC           ; [CPU_] |442| 
        ; branch occurs ; [] |442| 
$C$L53:    
	.dwpsn	file "../sci1.c",line 444,column 9,is_stmt
        MOV       AL,@_SCI_MsgInRS232+2 ; [CPU_] |444| 
        MOVW      DP,#_ODV_SciSend      ; [CPU_U] 
        MOV       @_ODV_SciSend,AL      ; [CPU_] |444| 
	.dwpsn	file "../sci1.c",line 445,column 7,is_stmt
        B         $C$L104,UNC           ; [CPU_] |445| 
        ; branch occurs ; [] |445| 
$C$L54:    
	.dwpsn	file "../sci1.c",line 447,column 9,is_stmt
        MOVB      AL,#0                 ; [CPU_] |447| 
$C$DW$250	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$250, DW_AT_low_pc(0x00)
	.dwattr $C$DW$250, DW_AT_name("_GetWord")
	.dwattr $C$DW$250, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |447| 
        ; call occurs [#_GetWord] ; [] |447| 
        MOV       *-SP[14],AL           ; [CPU_] |447| 
	.dwpsn	file "../sci1.c",line 448,column 9,is_stmt
        MOVB      AL,#1                 ; [CPU_] |448| 
$C$DW$251	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$251, DW_AT_low_pc(0x00)
	.dwattr $C$DW$251, DW_AT_name("_GetWord")
	.dwattr $C$DW$251, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |448| 
        ; call occurs [#_GetWord] ; [] |448| 
        MOV       *-SP[13],AL           ; [CPU_] |448| 
	.dwpsn	file "../sci1.c",line 449,column 9,is_stmt
        MOVB      AH,#4                 ; [CPU_] |449| 
        MOVZ      AR4,SP                ; [CPU_U] |449| 
        MOVB      AL,#240               ; [CPU_] |449| 
        SUBB      XAR4,#14              ; [CPU_U] |449| 
$C$DW$252	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$252, DW_AT_low_pc(0x00)
	.dwattr $C$DW$252, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$252, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |449| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |449| 
	.dwpsn	file "../sci1.c",line 451,column 7,is_stmt
        B         $C$L104,UNC           ; [CPU_] |451| 
        ; branch occurs ; [] |451| 
$C$L55:    
	.dwpsn	file "../sci1.c",line 453,column 9,is_stmt
        MOVB      AL,#0                 ; [CPU_] |453| 
$C$DW$253	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$253, DW_AT_low_pc(0x00)
	.dwattr $C$DW$253, DW_AT_name("_GetWord")
	.dwattr $C$DW$253, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |453| 
        ; call occurs [#_GetWord] ; [] |453| 
        MOVW      DP,#_ODP_Board_SerialNumber ; [CPU_U] 
        MOVU      ACC,AL                ; [CPU_] |453| 
        MOVL      @_ODP_Board_SerialNumber,ACC ; [CPU_] |453| 
	.dwpsn	file "../sci1.c",line 454,column 7,is_stmt
        B         $C$L104,UNC           ; [CPU_] |454| 
        ; branch occurs ; [] |454| 
$C$L56:    
	.dwpsn	file "../sci1.c",line 456,column 9,is_stmt
        MOV       AL,@_SCI_MsgInRS232+2 ; [CPU_] |456| 
        MOVW      DP,#_ODV_SciSend      ; [CPU_U] 
        MOV       @_ODV_SciSend,AL      ; [CPU_] |456| 
	.dwpsn	file "../sci1.c",line 457,column 7,is_stmt
        B         $C$L104,UNC           ; [CPU_] |457| 
        ; branch occurs ; [] |457| 
$C$L57:    
	.dwpsn	file "../sci1.c",line 459,column 9,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |459| 
	.dwpsn	file "../sci1.c",line 459,column 18,is_stmt
        MOV       *-SP[7],#65535        ; [CPU_] |459| 
	.dwpsn	file "../sci1.c",line 459,column 31,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Cell_Nb ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Cell_Nb ; [CPU_] |459| 
        MOV       *-SP[9],AL            ; [CPU_] |459| 
	.dwpsn	file "../sci1.c",line 460,column 14,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |460| 
        B         $C$L61,UNC            ; [CPU_] |460| 
        ; branch occurs ; [] |460| 
$C$L58:    
	.dwpsn	file "../sci1.c",line 461,column 11,is_stmt
        MOVB      AL,#13                ; [CPU_] |461| 
        SUB       AL,*-SP[5]            ; [CPU_] |461| 
$C$DW$254	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$254, DW_AT_low_pc(0x00)
	.dwattr $C$DW$254, DW_AT_name("_GetWord")
	.dwattr $C$DW$254, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |461| 
        ; call occurs [#_GetWord] ; [] |461| 
        MOV       *-SP[8],AL            ; [CPU_] |461| 
	.dwpsn	file "../sci1.c",line 462,column 11,is_stmt
        MOVU      ACC,*-SP[8]           ; [CPU_] |462| 
        ADDL      ACC,*-SP[16]          ; [CPU_] |462| 
        MOVL      *-SP[16],ACC          ; [CPU_] |462| 
	.dwpsn	file "../sci1.c",line 463,column 11,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |463| 
        CMP       AL,*-SP[8]            ; [CPU_] |463| 
        B         $C$L59,HIS            ; [CPU_] |463| 
        ; branchcc occurs ; [] |463| 
	.dwpsn	file "../sci1.c",line 463,column 26,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |463| 
        MOV       *-SP[6],AL            ; [CPU_] |463| 
$C$L59:    
	.dwpsn	file "../sci1.c",line 464,column 11,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |464| 
        CMP       AL,*-SP[8]            ; [CPU_] |464| 
        B         $C$L60,LOS            ; [CPU_] |464| 
        ; branchcc occurs ; [] |464| 
	.dwpsn	file "../sci1.c",line 464,column 26,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |464| 
        MOV       *-SP[7],AL            ; [CPU_] |464| 
$C$L60:    
	.dwpsn	file "../sci1.c",line 465,column 11,is_stmt
        MOVZ      AR0,*-SP[5]           ; [CPU_] |465| 
        MOV       AL,*-SP[8]            ; [CPU_] |465| 
        MOVL      XAR4,#_ODV_Read_Analogue_Input_16_Bit ; [CPU_U] |465| 
        LSR       AL,1                  ; [CPU_] |465| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |465| 
	.dwpsn	file "../sci1.c",line 460,column 23,is_stmt
        INC       *-SP[5]               ; [CPU_] |460| 
$C$L61:    
	.dwpsn	file "../sci1.c",line 460,column 18,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |460| 
        CMP       AL,*-SP[5]            ; [CPU_] |460| 
        B         $C$L58,HI             ; [CPU_] |460| 
        ; branchcc occurs ; [] |460| 
	.dwpsn	file "../sci1.c",line 467,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax_bal_delta ; [CPU_U] 
        UI16TOF32 R0H,@_ODP_SafetyLimits_Umax_bal_delta ; [CPU_] |467| 
        MOVW      DP,#_gaincell         ; [CPU_U] 
        MOV32     R1H,@_gaincell        ; [CPU_] |467| 
$C$DW$255	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$255, DW_AT_low_pc(0x00)
	.dwattr $C$DW$255, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$255, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |467| 
        ; call occurs [#FS$$DIV] ; [] |467| 
        MOV       AL,*-SP[7]            ; [CPU_] |467| 
        LSR       AL,1                  ; [CPU_] |467| 
        MOVU      ACC,AL                ; [CPU_] |467| 
        MOV32     R1H,ACC               ; [CPU_] |467| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        UI32TOF32 R1H,R1H               ; [CPU_] |467| 
        NOP       ; [CPU_] 
        ADDF32    R0H,R0H,R1H           ; [CPU_] |467| 
$C$DW$256	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$256, DW_AT_low_pc(0x00)
	.dwattr $C$DW$256, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$256, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |467| 
        ; call occurs [#_CNV_Round] ; [] |467| 
        MOV       *-SP[8],AL            ; [CPU_] |467| 
	.dwpsn	file "../sci1.c",line 468,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umin_bal_delta ; [CPU_U] 
        UI16TOF32 R0H,@_ODP_SafetyLimits_Umin_bal_delta ; [CPU_] |468| 
        MOVW      DP,#_gaincell         ; [CPU_U] 
        MOV32     R1H,@_gaincell        ; [CPU_] |468| 
$C$DW$257	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$257, DW_AT_low_pc(0x00)
	.dwattr $C$DW$257, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$257, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |468| 
        ; call occurs [#FS$$DIV] ; [] |468| 
        MOV       AL,*-SP[7]            ; [CPU_] |468| 
        LSR       AL,1                  ; [CPU_] |468| 
        MOVU      ACC,AL                ; [CPU_] |468| 
        MOV32     R1H,ACC               ; [CPU_] |468| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        UI32TOF32 R1H,R1H               ; [CPU_] |468| 
        NOP       ; [CPU_] 
        ADDF32    R0H,R0H,R1H           ; [CPU_] |468| 
$C$DW$258	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$258, DW_AT_low_pc(0x00)
	.dwattr $C$DW$258, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$258, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |468| 
        ; call occurs [#_CNV_Round] ; [] |468| 
        MOV       *-SP[10],AL           ; [CPU_] |468| 
	.dwpsn	file "../sci1.c",line 469,column 9,is_stmt
        MOVB      AL,#17                ; [CPU_] |469| 
$C$DW$259	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$259, DW_AT_low_pc(0x00)
	.dwattr $C$DW$259, DW_AT_name("_GetWord")
	.dwattr $C$DW$259, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |469| 
        ; call occurs [#_GetWord] ; [] |469| 
        MOVU      ACC,AL                ; [CPU_] |469| 
        MOV32     R0H,ACC               ; [CPU_] |469| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#16768            ; [CPU_] |469| 
        UI32TOF32 R0H,R0H               ; [CPU_] |469| 
$C$DW$260	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$260, DW_AT_low_pc(0x00)
	.dwattr $C$DW$260, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$260, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |469| 
        ; call occurs [#FS$$DIV] ; [] |469| 
        MOVB      AL,#0                 ; [CPU_] |469| 
$C$DW$261	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$261, DW_AT_low_pc(0x00)
	.dwattr $C$DW$261, DW_AT_name("_HAL_LectureTemperature")
	.dwattr $C$DW$261, DW_AT_TI_call
        LCR       #_HAL_LectureTemperature ; [CPU_] |469| 
        ; call occurs [#_HAL_LectureTemperature] ; [] |469| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+17 ; [CPU_U] 
        MOV       @_ODV_Read_Analogue_Input_16_Bit+17,AL ; [CPU_] |469| 
	.dwpsn	file "../sci1.c",line 470,column 9,is_stmt
        MOVB      AL,#21                ; [CPU_] |470| 
$C$DW$262	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$262, DW_AT_low_pc(0x00)
	.dwattr $C$DW$262, DW_AT_name("_GetWord")
	.dwattr $C$DW$262, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |470| 
        ; call occurs [#_GetWord] ; [] |470| 
        MOVU      ACC,AL                ; [CPU_] |470| 
        MOV32     R0H,ACC               ; [CPU_] |470| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#16768            ; [CPU_] |470| 
        UI32TOF32 R0H,R0H               ; [CPU_] |470| 
$C$DW$263	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$263, DW_AT_low_pc(0x00)
	.dwattr $C$DW$263, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$263, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |470| 
        ; call occurs [#FS$$DIV] ; [] |470| 
        MOVB      AL,#0                 ; [CPU_] |470| 
$C$DW$264	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$264, DW_AT_low_pc(0x00)
	.dwattr $C$DW$264, DW_AT_name("_HAL_LectureTemperature")
	.dwattr $C$DW$264, DW_AT_TI_call
        LCR       #_HAL_LectureTemperature ; [CPU_] |470| 
        ; call occurs [#_HAL_LectureTemperature] ; [] |470| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+21 ; [CPU_U] 
        MOV       @_ODV_Read_Analogue_Input_16_Bit+21,AL ; [CPU_] |470| 
	.dwpsn	file "../sci1.c",line 471,column 9,is_stmt
        MOVB      AL,#14                ; [CPU_] |471| 
$C$DW$265	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$265, DW_AT_low_pc(0x00)
	.dwattr $C$DW$265, DW_AT_name("_GetWord")
	.dwattr $C$DW$265, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |471| 
        ; call occurs [#_GetWord] ; [] |471| 
        MOVU      ACC,AL                ; [CPU_] |471| 
        MOV32     R0H,ACC               ; [CPU_] |471| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#16768            ; [CPU_] |471| 
        UI32TOF32 R0H,R0H               ; [CPU_] |471| 
$C$DW$266	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$266, DW_AT_low_pc(0x00)
	.dwattr $C$DW$266, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$266, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |471| 
        ; call occurs [#FS$$DIV] ; [] |471| 
        MOVB      AL,#1                 ; [CPU_] |471| 
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_name("_HAL_LectureTemperature")
	.dwattr $C$DW$267, DW_AT_TI_call
        LCR       #_HAL_LectureTemperature ; [CPU_] |471| 
        ; call occurs [#_HAL_LectureTemperature] ; [] |471| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+14 ; [CPU_U] 
        MOV       @_ODV_Read_Analogue_Input_16_Bit+14,AL ; [CPU_] |471| 
	.dwpsn	file "../sci1.c",line 472,column 9,is_stmt
        MOVB      AL,#15                ; [CPU_] |472| 
$C$DW$268	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$268, DW_AT_low_pc(0x00)
	.dwattr $C$DW$268, DW_AT_name("_GetWord")
	.dwattr $C$DW$268, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |472| 
        ; call occurs [#_GetWord] ; [] |472| 
        MOVU      ACC,AL                ; [CPU_] |472| 
        MOV32     R0H,ACC               ; [CPU_] |472| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#16768            ; [CPU_] |472| 
        UI32TOF32 R0H,R0H               ; [CPU_] |472| 
$C$DW$269	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$269, DW_AT_low_pc(0x00)
	.dwattr $C$DW$269, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$269, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |472| 
        ; call occurs [#FS$$DIV] ; [] |472| 
        MOVB      AL,#1                 ; [CPU_] |472| 
$C$DW$270	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$270, DW_AT_low_pc(0x00)
	.dwattr $C$DW$270, DW_AT_name("_HAL_LectureTemperature")
	.dwattr $C$DW$270, DW_AT_TI_call
        LCR       #_HAL_LectureTemperature ; [CPU_] |472| 
        ; call occurs [#_HAL_LectureTemperature] ; [] |472| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+15 ; [CPU_U] 
        MOV       @_ODV_Read_Analogue_Input_16_Bit+15,AL ; [CPU_] |472| 
	.dwpsn	file "../sci1.c",line 473,column 9,is_stmt
        MOVB      AL,#20                ; [CPU_] |473| 
$C$DW$271	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$271, DW_AT_low_pc(0x00)
	.dwattr $C$DW$271, DW_AT_name("_GetWord")
	.dwattr $C$DW$271, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |473| 
        ; call occurs [#_GetWord] ; [] |473| 
        MOVU      ACC,AL                ; [CPU_] |473| 
        MOV32     R0H,ACC               ; [CPU_] |473| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVIZ     R1H,#16768            ; [CPU_] |473| 
        UI32TOF32 R0H,R0H               ; [CPU_] |473| 
$C$DW$272	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$272, DW_AT_low_pc(0x00)
	.dwattr $C$DW$272, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$272, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |473| 
        ; call occurs [#FS$$DIV] ; [] |473| 
        MOVB      AL,#1                 ; [CPU_] |473| 
$C$DW$273	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$273, DW_AT_low_pc(0x00)
	.dwattr $C$DW$273, DW_AT_name("_HAL_LectureTemperature")
	.dwattr $C$DW$273, DW_AT_TI_call
        LCR       #_HAL_LectureTemperature ; [CPU_] |473| 
        ; call occurs [#_HAL_LectureTemperature] ; [] |473| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+20 ; [CPU_U] 
        MOV       @_ODV_Read_Analogue_Input_16_Bit+20,AL ; [CPU_] |473| 
	.dwpsn	file "../sci1.c",line 475,column 9,is_stmt
        MOVB      AL,#19                ; [CPU_] |475| 
$C$DW$274	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$274, DW_AT_low_pc(0x00)
	.dwattr $C$DW$274, DW_AT_name("_GetWord")
	.dwattr $C$DW$274, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |475| 
        ; call occurs [#_GetWord] ; [] |475| 
        MOVW      DP,#_ODP_Analogue_Input_Offset_Integer+38 ; [CPU_U] 
        MOV       AH,@_ODP_Analogue_Input_Offset_Integer+38 ; [CPU_] |475| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+19 ; [CPU_U] 
        ADD       AH,AL                 ; [CPU_] |475| 
        MOV       @_ODV_Read_Analogue_Input_16_Bit+19,AH ; [CPU_] |475| 
	.dwpsn	file "../sci1.c",line 476,column 9,is_stmt
        MOVB      AL,#18                ; [CPU_] |476| 
$C$DW$275	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$275, DW_AT_low_pc(0x00)
	.dwattr $C$DW$275, DW_AT_name("_GetWord")
	.dwattr $C$DW$275, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |476| 
        ; call occurs [#_GetWord] ; [] |476| 
        MOV       @_ODV_Read_Analogue_Input_16_Bit+18,AL ; [CPU_] |476| 
	.dwpsn	file "../sci1.c",line 477,column 9,is_stmt
        MOVB      AL,#16                ; [CPU_] |477| 
$C$DW$276	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$276, DW_AT_low_pc(0x00)
	.dwattr $C$DW$276, DW_AT_name("_GetWord")
	.dwattr $C$DW$276, DW_AT_TI_call
        LCR       #_GetWord             ; [CPU_] |477| 
        ; call occurs [#_GetWord] ; [] |477| 
        MOVW      DP,#_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_U] 
        MOV       AH,@_ODP_Analogue_Input_Offset_Integer+32 ; [CPU_] |477| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+16 ; [CPU_U] 
        ADD       AH,AL                 ; [CPU_] |477| 
        MOV       @_ODV_Read_Analogue_Input_16_Bit+16,AH ; [CPU_] |477| 
	.dwpsn	file "../sci1.c",line 479,column 9,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |479| 
        BF        $C$L62,EQ             ; [CPU_] |479| 
        ; branchcc occurs ; [] |479| 
	.dwpsn	file "../sci1.c",line 481,column 10,is_stmt
        MOVW      DP,#_gaincell         ; [CPU_U] 
        UI32TOF32 R0H,*-SP[16]          ; [CPU_] |481| 
        MOV32     R1H,@_gaincell        ; [CPU_] |481| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |481| 
        MOVIZ     R1H,#16384            ; [CPU_] |481| 
$C$DW$277	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$277, DW_AT_low_pc(0x00)
	.dwattr $C$DW$277, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$277, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |481| 
        ; call occurs [#FS$$DIV] ; [] |481| 
$C$DW$278	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$278, DW_AT_low_pc(0x00)
	.dwattr $C$DW$278, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$278, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |481| 
        ; call occurs [#_CNV_Round] ; [] |481| 
        MOVW      DP,#_ODV_Module1_Voltage ; [CPU_U] 
        MOV       @_ODV_Module1_Voltage,AL ; [CPU_] |481| 
	.dwpsn	file "../sci1.c",line 482,column 9,is_stmt
        B         $C$L64,UNC            ; [CPU_] |482| 
        ; branch occurs ; [] |482| 
$C$L62:    

$C$DW$279	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$280	.dwtag  DW_TAG_variable, DW_AT_name("NeuModuleVolt")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_NeuModuleVolt")
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$280, DW_AT_location[DW_OP_breg20 -20]
	.dwpsn	file "../sci1.c",line 485,column 31,is_stmt
        MOVW      DP,#_gaincell         ; [CPU_U] 
        UI32TOF32 R0H,*-SP[16]          ; [CPU_] |485| 
        MOV32     R1H,@_gaincell        ; [CPU_] |485| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |485| 
        MOVIZ     R1H,#16384            ; [CPU_] |485| 
$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$281, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |485| 
        ; call occurs [#FS$$DIV] ; [] |485| 
$C$DW$282	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$282, DW_AT_low_pc(0x00)
	.dwattr $C$DW$282, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$282, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |485| 
        ; call occurs [#_CNV_Round] ; [] |485| 
        MOV       *-SP[20],AL           ; [CPU_] |485| 
	.dwpsn	file "../sci1.c",line 486,column 10,is_stmt
        MOVW      DP,#_ModuleVolt       ; [CPU_U] 
        SETC      SXM                   ; [CPU_] 
        SUB       AL,@_ModuleVolt       ; [CPU_] |486| 
        MOV       ACC,AL                ; [CPU_] |486| 
        ABS       ACC                   ; [CPU_] |486| 
        CMPB      AL,#100               ; [CPU_] |486| 
        B         $C$L63,LEQ            ; [CPU_] |486| 
        ; branchcc occurs ; [] |486| 
	.dwpsn	file "../sci1.c",line 488,column 11,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |488| 
        MOVW      DP,#_ODV_Module1_Voltage ; [CPU_U] 
        MOV       @_ODV_Module1_Voltage,AL ; [CPU_] |488| 
	.dwpsn	file "../sci1.c",line 489,column 11,is_stmt
        MOVW      DP,#_ModuleVolt       ; [CPU_U] 
        MOV       @_ModuleVolt,AL       ; [CPU_] |489| 
$C$L63:    
	.dwendtag $C$DW$279

$C$L64:    
	.dwpsn	file "../sci1.c",line 494,column 9,is_stmt
        MOVW      DP,#_gaincell         ; [CPU_U] 
        UI16TOF32 R0H,*-SP[7]           ; [CPU_] |494| 
        MOV32     R1H,@_gaincell        ; [CPU_] |494| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |494| 
        MOVIZ     R1H,#16928            ; [CPU_] |494| 
$C$DW$283	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$283, DW_AT_low_pc(0x00)
	.dwattr $C$DW$283, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$283, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |494| 
        ; call occurs [#FS$$DIV] ; [] |494| 
$C$DW$284	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$284, DW_AT_low_pc(0x00)
	.dwattr $C$DW$284, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$284, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |494| 
        ; call occurs [#_CNV_Round] ; [] |494| 
        MOVW      DP,#_ODV_Module1_MinCellVoltage ; [CPU_U] 
        MOV       @_ODV_Module1_MinCellVoltage,AL ; [CPU_] |494| 
	.dwpsn	file "../sci1.c",line 495,column 9,is_stmt
        MOVW      DP,#_gaincell         ; [CPU_U] 
        UI16TOF32 R0H,*-SP[6]           ; [CPU_] |495| 
        MOV32     R1H,@_gaincell        ; [CPU_] |495| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |495| 
        MOVIZ     R1H,#16928            ; [CPU_] |495| 
$C$DW$285	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$285, DW_AT_low_pc(0x00)
	.dwattr $C$DW$285, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$285, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |495| 
        ; call occurs [#FS$$DIV] ; [] |495| 
$C$DW$286	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$286, DW_AT_low_pc(0x00)
	.dwattr $C$DW$286, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$286, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |495| 
        ; call occurs [#_CNV_Round] ; [] |495| 
        MOVW      DP,#_ODV_Module1_MaxCellVoltage ; [CPU_U] 
        MOV       @_ODV_Module1_MaxCellVoltage,AL ; [CPU_] |495| 
	.dwpsn	file "../sci1.c",line 496,column 9,is_stmt
        MOVW      DP,#_ODV_Module1_MinCellVoltage ; [CPU_U] 
        SUB       AL,@_ODV_Module1_MinCellVoltage ; [CPU_] |496| 
        MOVW      DP,#_ODV_Module1_MaxDeltaVoltage ; [CPU_U] 
        MOV       @_ODV_Module1_MaxDeltaVoltage,AL ; [CPU_] |496| 
	.dwpsn	file "../sci1.c",line 498,column 3,is_stmt
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+14 ; [CPU_U] 
        MOVB      AH,#10                ; [CPU_] |498| 
        MOV       AL,@_ODV_Read_Analogue_Input_16_Bit+14 ; [CPU_] |498| 
$C$DW$287	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$287, DW_AT_low_pc(0x00)
	.dwattr $C$DW$287, DW_AT_name("I$$DIV")
	.dwattr $C$DW$287, DW_AT_TI_call
        FFC       XAR7,#I$$DIV          ; [CPU_] |498| 
        ; call occurs [#I$$DIV] ; [] |498| 
        MOVW      DP,#_Temp_Tempareture1 ; [CPU_U] 
        MOV       @_Temp_Tempareture1,AL ; [CPU_] |498| 
	.dwpsn	file "../sci1.c",line 499,column 9,is_stmt
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+15 ; [CPU_U] 
        MOVB      AH,#10                ; [CPU_] |499| 
        MOV       AL,@_ODV_Read_Analogue_Input_16_Bit+15 ; [CPU_] |499| 
$C$DW$288	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$288, DW_AT_low_pc(0x00)
	.dwattr $C$DW$288, DW_AT_name("I$$DIV")
	.dwattr $C$DW$288, DW_AT_TI_call
        FFC       XAR7,#I$$DIV          ; [CPU_] |499| 
        ; call occurs [#I$$DIV] ; [] |499| 
        MOVW      DP,#_Temp_Tempareture2 ; [CPU_U] 
        MOV       @_Temp_Tempareture2,AL ; [CPU_] |499| 
	.dwpsn	file "../sci1.c",line 500,column 9,is_stmt
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+20 ; [CPU_U] 
        MOVB      AH,#10                ; [CPU_] |500| 
        MOV       AL,@_ODV_Read_Analogue_Input_16_Bit+20 ; [CPU_] |500| 
$C$DW$289	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$289, DW_AT_low_pc(0x00)
	.dwattr $C$DW$289, DW_AT_name("I$$DIV")
	.dwattr $C$DW$289, DW_AT_TI_call
        FFC       XAR7,#I$$DIV          ; [CPU_] |500| 
        ; call occurs [#I$$DIV] ; [] |500| 
        MOVW      DP,#_Temp_Tempareture3 ; [CPU_U] 
        MOV       @_Temp_Tempareture3,AL ; [CPU_] |500| 
	.dwpsn	file "../sci1.c",line 502,column 32,is_stmt
        MOV       AL,@_Temp_Tempareture1 ; [CPU_] |502| 
        MOV       *-SP[17],AL           ; [CPU_] |502| 
	.dwpsn	file "../sci1.c",line 503,column 32,is_stmt
        MOV       AL,@_Temp_Tempareture2 ; [CPU_] |503| 
        MOV       *-SP[18],AL           ; [CPU_] |503| 
	.dwpsn	file "../sci1.c",line 504,column 32,is_stmt
        MOV       AL,@_Temp_Tempareture3 ; [CPU_] |504| 
        MOV       *-SP[19],AL           ; [CPU_] |504| 
	.dwpsn	file "../sci1.c",line 506,column 9,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |506| 
        CMPB      AL,#100               ; [CPU_] |506| 
        B         $C$L65,GEQ            ; [CPU_] |506| 
        ; branchcc occurs ; [] |506| 
        CMP       AL,#-50               ; [CPU_] |506| 
        B         $C$L66,GT             ; [CPU_] |506| 
        ; branchcc occurs ; [] |506| 
	.dwpsn	file "../sci1.c",line 508,column 9,is_stmt
$C$L65:    
	.dwpsn	file "../sci1.c",line 510,column 10,is_stmt
        MOVB      *-SP[17],#100,UNC     ; [CPU_] |510| 
$C$L66:    
	.dwpsn	file "../sci1.c",line 512,column 9,is_stmt
        MOV       AL,*-SP[18]           ; [CPU_] |512| 
        CMPB      AL,#100               ; [CPU_] |512| 
        B         $C$L67,GEQ            ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        CMP       AL,#-50               ; [CPU_] |512| 
        B         $C$L68,GT             ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
	.dwpsn	file "../sci1.c",line 514,column 9,is_stmt
$C$L67:    
	.dwpsn	file "../sci1.c",line 516,column 10,is_stmt
        MOVB      *-SP[18],#100,UNC     ; [CPU_] |516| 
$C$L68:    
	.dwpsn	file "../sci1.c",line 518,column 9,is_stmt
        MOV       AL,*-SP[19]           ; [CPU_] |518| 
        CMPB      AL,#100               ; [CPU_] |518| 
        B         $C$L69,GEQ            ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
        CMP       AL,#-50               ; [CPU_] |518| 
        B         $C$L70,GT             ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
	.dwpsn	file "../sci1.c",line 520,column 9,is_stmt
$C$L69:    
	.dwpsn	file "../sci1.c",line 522,column 10,is_stmt
        MOVB      *-SP[19],#100,UNC     ; [CPU_] |522| 
$C$L70:    
	.dwpsn	file "../sci1.c",line 525,column 9,is_stmt
        MOV       AL,@_Temp_Tempareture2 ; [CPU_] |525| 
        CMP       AL,@_Temp_Tempareture1 ; [CPU_] |525| 
        B         $C$L71,GEQ            ; [CPU_] |525| 
        ; branchcc occurs ; [] |525| 
        MOV       AL,@_Temp_Tempareture3 ; [CPU_] |525| 
        CMP       AL,@_Temp_Tempareture1 ; [CPU_] |525| 
        B         $C$L71,GEQ            ; [CPU_] |525| 
        ; branchcc occurs ; [] |525| 
	.dwpsn	file "../sci1.c",line 527,column 10,is_stmt
        MOV       AL,@_Temp_Tempareture1 ; [CPU_] |527| 
        MOV       @_Temp_Tempareture_Max,AL ; [CPU_] |527| 
	.dwpsn	file "../sci1.c",line 528,column 9,is_stmt
        B         $C$L74,UNC            ; [CPU_] |528| 
        ; branch occurs ; [] |528| 
$C$L71:    
	.dwpsn	file "../sci1.c",line 529,column 14,is_stmt
        MOV       AL,@_Temp_Tempareture1 ; [CPU_] |529| 
        CMP       AL,@_Temp_Tempareture2 ; [CPU_] |529| 
        B         $C$L72,GEQ            ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        MOV       AL,@_Temp_Tempareture3 ; [CPU_] |529| 
        CMP       AL,@_Temp_Tempareture2 ; [CPU_] |529| 
        B         $C$L72,GEQ            ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
	.dwpsn	file "../sci1.c",line 531,column 10,is_stmt
        MOV       AL,@_Temp_Tempareture2 ; [CPU_] |531| 
        MOV       @_Temp_Tempareture_Max,AL ; [CPU_] |531| 
	.dwpsn	file "../sci1.c",line 532,column 9,is_stmt
        B         $C$L74,UNC            ; [CPU_] |532| 
        ; branch occurs ; [] |532| 
$C$L72:    
	.dwpsn	file "../sci1.c",line 533,column 14,is_stmt
        MOV       AL,@_Temp_Tempareture1 ; [CPU_] |533| 
        CMP       AL,@_Temp_Tempareture3 ; [CPU_] |533| 
        B         $C$L73,GEQ            ; [CPU_] |533| 
        ; branchcc occurs ; [] |533| 
        MOV       AL,@_Temp_Tempareture2 ; [CPU_] |533| 
        CMP       AL,@_Temp_Tempareture3 ; [CPU_] |533| 
        B         $C$L73,GEQ            ; [CPU_] |533| 
        ; branchcc occurs ; [] |533| 
	.dwpsn	file "../sci1.c",line 535,column 10,is_stmt
        MOV       AL,@_Temp_Tempareture3 ; [CPU_] |535| 
        MOV       @_Temp_Tempareture_Max,AL ; [CPU_] |535| 
	.dwpsn	file "../sci1.c",line 536,column 9,is_stmt
        B         $C$L74,UNC            ; [CPU_] |536| 
        ; branch occurs ; [] |536| 
$C$L73:    
	.dwpsn	file "../sci1.c",line 539,column 10,is_stmt
        MOV       AL,@_Temp_Tempareture3 ; [CPU_] |539| 
        MOV       @_Temp_Tempareture_Max,AL ; [CPU_] |539| 
$C$L74:    
	.dwpsn	file "../sci1.c",line 542,column 9,is_stmt
        MOV       AL,*-SP[18]           ; [CPU_] |542| 
        CMP       AL,*-SP[17]           ; [CPU_] |542| 
        B         $C$L75,LEQ            ; [CPU_] |542| 
        ; branchcc occurs ; [] |542| 
        MOV       AL,*-SP[19]           ; [CPU_] |542| 
        CMP       AL,*-SP[17]           ; [CPU_] |542| 
        B         $C$L75,LEQ            ; [CPU_] |542| 
        ; branchcc occurs ; [] |542| 
	.dwpsn	file "../sci1.c",line 544,column 11,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |544| 
        MOV       @_Temp_Tempareture_Min,AL ; [CPU_] |544| 
	.dwpsn	file "../sci1.c",line 545,column 10,is_stmt
        B         $C$L78,UNC            ; [CPU_] |545| 
        ; branch occurs ; [] |545| 
$C$L75:    
	.dwpsn	file "../sci1.c",line 546,column 15,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |546| 
        CMP       AL,*-SP[18]           ; [CPU_] |546| 
        B         $C$L76,LEQ            ; [CPU_] |546| 
        ; branchcc occurs ; [] |546| 
        MOV       AL,*-SP[19]           ; [CPU_] |546| 
        CMP       AL,*-SP[18]           ; [CPU_] |546| 
        B         $C$L76,LEQ            ; [CPU_] |546| 
        ; branchcc occurs ; [] |546| 
	.dwpsn	file "../sci1.c",line 548,column 11,is_stmt
        MOV       AL,*-SP[18]           ; [CPU_] |548| 
        MOV       @_Temp_Tempareture_Min,AL ; [CPU_] |548| 
	.dwpsn	file "../sci1.c",line 549,column 10,is_stmt
        B         $C$L78,UNC            ; [CPU_] |549| 
        ; branch occurs ; [] |549| 
$C$L76:    
	.dwpsn	file "../sci1.c",line 550,column 15,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |550| 
        CMP       AL,*-SP[19]           ; [CPU_] |550| 
        B         $C$L77,LEQ            ; [CPU_] |550| 
        ; branchcc occurs ; [] |550| 
        MOV       AL,*-SP[18]           ; [CPU_] |550| 
        CMP       AL,*-SP[19]           ; [CPU_] |550| 
        B         $C$L77,LEQ            ; [CPU_] |550| 
        ; branchcc occurs ; [] |550| 
	.dwpsn	file "../sci1.c",line 552,column 11,is_stmt
        MOV       AL,*-SP[19]           ; [CPU_] |552| 
        MOV       @_Temp_Tempareture_Min,AL ; [CPU_] |552| 
	.dwpsn	file "../sci1.c",line 553,column 10,is_stmt
        B         $C$L78,UNC            ; [CPU_] |553| 
        ; branch occurs ; [] |553| 
$C$L77:    
	.dwpsn	file "../sci1.c",line 556,column 11,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |556| 
        MOV       @_Temp_Tempareture_Min,AL ; [CPU_] |556| 
$C$L78:    
	.dwpsn	file "../sci1.c",line 568,column 9,is_stmt
        INC       @_i16                 ; [CPU_] |568| 
	.dwpsn	file "../sci1.c",line 569,column 9,is_stmt
        MOV       AL,@_i16              ; [CPU_] |569| 
        CMPB      AL,#200               ; [CPU_] |569| 
        BF        $C$L79,NEQ            ; [CPU_] |569| 
        ; branchcc occurs ; [] |569| 
	.dwpsn	file "../sci1.c",line 571,column 10,is_stmt
        MOVB      ACC,#200              ; [CPU_] |571| 
        MOVL      *-SP[2],ACC           ; [CPU_] |571| 
        MOVL      ACC,@_Sumtemp_Max     ; [CPU_] |571| 
$C$DW$290	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$290, DW_AT_low_pc(0x00)
	.dwattr $C$DW$290, DW_AT_name("L$$DIV")
	.dwattr $C$DW$290, DW_AT_TI_call
        FFC       XAR7,#L$$DIV          ; [CPU_] |571| 
        ; call occurs [#L$$DIV] ; [] |571| 
        MOV       @_Module1_Average_Temp_Max,AL ; [CPU_] |571| 
	.dwpsn	file "../sci1.c",line 572,column 10,is_stmt
        MOVB      ACC,#0                ; [CPU_] |572| 
        MOVL      @_Sumtemp_Max,ACC     ; [CPU_] |572| 
	.dwpsn	file "../sci1.c",line 573,column 10,is_stmt
        MOVB      ACC,#200              ; [CPU_] |573| 
        MOVL      *-SP[2],ACC           ; [CPU_] |573| 
        MOVL      ACC,@_Sumtemp_Min     ; [CPU_] |573| 
$C$DW$291	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$291, DW_AT_low_pc(0x00)
	.dwattr $C$DW$291, DW_AT_name("L$$DIV")
	.dwattr $C$DW$291, DW_AT_TI_call
        FFC       XAR7,#L$$DIV          ; [CPU_] |573| 
        ; call occurs [#L$$DIV] ; [] |573| 
        MOV       @_Module1_Average_Temp_Min,AL ; [CPU_] |573| 
	.dwpsn	file "../sci1.c",line 574,column 10,is_stmt
        MOVB      ACC,#0                ; [CPU_] |574| 
        MOVL      @_Sumtemp_Min,ACC     ; [CPU_] |574| 
$C$L79:    
	.dwpsn	file "../sci1.c",line 576,column 9,is_stmt
        MOV       AL,@_i16              ; [CPU_] |576| 
        CMPB      AL,#200               ; [CPU_] |576| 
        B         $C$L80,HIS            ; [CPU_] |576| 
        ; branchcc occurs ; [] |576| 
	.dwpsn	file "../sci1.c",line 578,column 10,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,@_Temp_Tempareture_Max ; [CPU_] |578| 
        ADDL      @_Sumtemp_Max,ACC     ; [CPU_] |578| 
	.dwpsn	file "../sci1.c",line 579,column 10,is_stmt
        MOV       ACC,@_Temp_Tempareture_Min ; [CPU_] |579| 
        ADDL      @_Sumtemp_Min,ACC     ; [CPU_] |579| 
	.dwpsn	file "../sci1.c",line 580,column 9,is_stmt
        B         $C$L81,UNC            ; [CPU_] |580| 
        ; branch occurs ; [] |580| 
$C$L80:    
	.dwpsn	file "../sci1.c",line 583,column 10,is_stmt
        MOV       @_i16,#0              ; [CPU_] |583| 
$C$L81:    
	.dwpsn	file "../sci1.c",line 585,column 9,is_stmt
        MOV       AL,@_Module1_Average_Temp_Max ; [CPU_] |585| 
        MOVW      DP,#_ODV_Temperature_Average_data ; [CPU_U] 
        MOV       @_ODV_Temperature_Average_data,AL ; [CPU_] |585| 
	.dwpsn	file "../sci1.c",line 586,column 9,is_stmt
        MOVW      DP,#_Module1_Average_Temp_Min ; [CPU_U] 
        MOV       AL,@_Module1_Average_Temp_Min ; [CPU_] |586| 
        MOVW      DP,#_ODV_Temperature_Min ; [CPU_U] 
        MOV       @_ODV_Temperature_Min,AL ; [CPU_] |586| 
	.dwpsn	file "../sci1.c",line 589,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |589| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |589| 
        BF        $C$L82,NTC            ; [CPU_] |589| 
        ; branchcc occurs ; [] |589| 
	.dwpsn	file "../sci1.c",line 591,column 10,is_stmt
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+16 ; [CPU_U] 
        I16TOF32  R0H,@_ODV_Read_Analogue_Input_16_Bit+16 ; [CPU_] |591| 
        MOVW      DP,#_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_U] 
        MOV32     R1H,@_ODP_Analogue_Input_Scaling_Float+32 ; [CPU_] |591| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |591| 
$C$DW$292	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$292, DW_AT_low_pc(0x00)
	.dwattr $C$DW$292, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$292, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |591| 
        ; call occurs [#_CNV_Round] ; [] |591| 
        MOVW      DP,#_ODV_Module1_Current_Average ; [CPU_U] 
        MOV       @_ODV_Module1_Current_Average,AL ; [CPU_] |591| 
	.dwpsn	file "../sci1.c",line 594,column 9,is_stmt
        B         $C$L83,UNC            ; [CPU_] |594| 
        ; branch occurs ; [] |594| 
$C$L82:    
	.dwpsn	file "../sci1.c",line 600,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_Current ; [CPU_U] 
        MOV       ACC,@_ODV_Gateway_Current << #3 ; [CPU_] |600| 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       @_ODV_Module1_Current,AL ; [CPU_] |600| 
$C$L83:    
	.dwpsn	file "../sci1.c",line 621,column 9,is_stmt
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Current ; [CPU_] |621| 
        B         $C$L84,LEQ            ; [CPU_] |621| 
        ; branchcc occurs ; [] |621| 
	.dwpsn	file "../sci1.c",line 623,column 10,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,@_ODV_Module1_Current ; [CPU_] |623| 
        MOVW      DP,#_PAR_Capacity_TotalLife_Used ; [CPU_U] 
        ASR64     ACC:P,16              ; [CPU_] |623| 
        ASR64     ACC:P,16              ; [CPU_] |623| 
        ADDUL     P,@_PAR_Capacity_TotalLife_Used ; [CPU_] |623| 
        ADDCL     ACC,@_PAR_Capacity_TotalLife_Used+2 ; [CPU_] |623| 
        MOVL      @_PAR_Capacity_TotalLife_Used,P ; [CPU_] |623| 
        MOVL      @_PAR_Capacity_TotalLife_Used+2,ACC ; [CPU_] |623| 
$C$L84:    
	.dwpsn	file "../sci1.c",line 626,column 9,is_stmt
        MOVW      DP,#_ODV_Module1_Module_SOC_Calibration ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Module_SOC_Calibration ; [CPU_] |626| 
        BF        $C$L85,EQ             ; [CPU_] |626| 
        ; branchcc occurs ; [] |626| 
	.dwpsn	file "../sci1.c",line 628,column 10,is_stmt
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      P,@_PAR_Capacity_Total ; [CPU_] |628| 
        MOVB      XAR6,#100             ; [CPU_] |628| 
        MOVB      ACC,#0                ; [CPU_] |628| 
        MOVW      DP,#_ODV_Module1_Module_SOC_Calibration ; [CPU_U] 
        MOVZ      AR7,@_ODV_Module1_Module_SOC_Calibration ; [CPU_] |628| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |628| 
        MOVL      XT,P                  ; [CPU_] |628| 
        IMPYXUL   P,XT,XAR7             ; [CPU_] |628| 
        MOVL      @_PAR_Capacity_Left,P ; [CPU_] |628| 
	.dwpsn	file "../sci1.c",line 629,column 10,is_stmt
        MOVW      DP,#_CurrCounterSec   ; [CPU_U] 
        MOV       @_CurrCounterSec,#0   ; [CPU_] |629| 
	.dwpsn	file "../sci1.c",line 630,column 9,is_stmt
        B         $C$L88,UNC            ; [CPU_] |630| 
        ; branch occurs ; [] |630| 
$C$L85:    
	.dwpsn	file "../sci1.c",line 633,column 10,is_stmt
        MOVW      DP,#_CurrCounterSec   ; [CPU_U] 
        MOV       AL,@_CurrCounterSec   ; [CPU_] |633| 
        CMPB      AL,#100               ; [CPU_] |633| 
        B         $C$L88,LOS            ; [CPU_] |633| 
        ; branchcc occurs ; [] |633| 
	.dwpsn	file "../sci1.c",line 635,column 11,is_stmt
        MOV       @_CurrCounterSec,#0   ; [CPU_] |635| 
	.dwpsn	file "../sci1.c",line 637,column 11,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_Current ; [CPU_] |637| 
        MOVW      DP,#_HAL_Current_Sum  ; [CPU_U] 
        ADDL      @_HAL_Current_Sum,ACC ; [CPU_] |637| 
	.dwpsn	file "../sci1.c",line 639,column 11,is_stmt
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOVB      XAR4,#0               ; [CPU_] |639| 
        MOVL      XAR6,@_PAR_Capacity_Left ; [CPU_] |639| 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_Current ; [CPU_] |639| 
        ASR64     ACC:P,16              ; [CPU_] |639| 
        ASR64     ACC:P,16              ; [CPU_] |639| 
        ADDUL     P,XAR6                ; [CPU_] |639| 
        ADDCL     ACC,XAR4              ; [CPU_] |639| 
        CMP64     ACC:P                 ; [CPU_] |639| 
        CMP64     ACC:P                 ; [CPU_] |639| 
        B         $C$L86,GEQ            ; [CPU_] |639| 
        ; branchcc occurs ; [] |639| 
	.dwpsn	file "../sci1.c",line 641,column 12,is_stmt
        MOVW      DP,#_ODV_Module1_Module_SOC_Calibration ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Module_SOC_Calibration ; [CPU_] |641| 
        BF        $C$L88,NEQ            ; [CPU_] |641| 
        ; branchcc occurs ; [] |641| 
	.dwpsn	file "../sci1.c",line 645,column 11,is_stmt
        B         $C$L88,UNC            ; [CPU_] |645| 
        ; branch occurs ; [] |645| 
$C$L86:    
	.dwpsn	file "../sci1.c",line 648,column 12,is_stmt
        MOVW      DP,#_ODV_Module1_Module_SOC_Calibration ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Module_SOC_Calibration ; [CPU_] |648| 
        BF        $C$L88,NEQ            ; [CPU_] |648| 
        ; branchcc occurs ; [] |648| 
	.dwpsn	file "../sci1.c",line 650,column 13,is_stmt
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOVZ      AR6,@_ODV_Module1_Current ; [CPU_] |650| 
        MOVW      DP,#_ODP_Module1_Module_SOC_Current ; [CPU_U] 
        MOV       ACC,@_ODP_Module1_Module_SOC_Current << #5 ; [CPU_] |650| 
        MOVZ      AR7,AL                ; [CPU_] |650| 
        MOV       ACC,AR6               ; [CPU_] |650| 
        ABS       ACC                   ; [CPU_] |650| 
        MOV       AH,AR7                ; [CPU_] |650| 
        MOVZ      AR6,AL                ; [CPU_] |650| 
        CMP       AH,AR6                ; [CPU_] |650| 
        B         $C$L87,LEQ            ; [CPU_] |650| 
        ; branchcc occurs ; [] |650| 
	.dwpsn	file "../sci1.c",line 652,column 14,is_stmt
        MOVW      DP,#_ODP_Module1_StandbyCurrent ; [CPU_U] 
        MOV       ACC,@_ODP_Module1_StandbyCurrent ; [CPU_] |652| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        ADDL      @_PAR_Capacity_Left,ACC ; [CPU_] |652| 
	.dwpsn	file "../sci1.c",line 653,column 13,is_stmt
        B         $C$L88,UNC            ; [CPU_] |653| 
        ; branch occurs ; [] |653| 
$C$L87:    
	.dwpsn	file "../sci1.c",line 656,column 14,is_stmt
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_Current ; [CPU_] |656| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        ADDL      @_PAR_Capacity_Left,ACC ; [CPU_] |656| 
	.dwpsn	file "../sci1.c",line 658,column 12,is_stmt
$C$L88:    
	.dwpsn	file "../sci1.c",line 666,column 9,is_stmt
        MOVW      DP,#_CurrCounterSec   ; [CPU_U] 
        INC       @_CurrCounterSec      ; [CPU_] |666| 
	.dwpsn	file "../sci1.c",line 668,column 9,is_stmt
        MOVW      DP,#_ODV_Module1_MaxCellVoltage ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_MaxCellVoltage << #1 ; [CPU_] |668| 
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        CMP       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |668| 
        B         $C$L89,LO             ; [CPU_] |668| 
        ; branchcc occurs ; [] |668| 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Current ; [CPU_] |668| 
        CMPB      AL,#6                 ; [CPU_] |668| 
        B         $C$L89,LT             ; [CPU_] |668| 
        ; branchcc occurs ; [] |668| 
	.dwpsn	file "../sci1.c",line 670,column 10,is_stmt
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      ACC,@_PAR_Capacity_Total ; [CPU_] |670| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOVL      @_PAR_Capacity_Left,ACC ; [CPU_] |670| 
	.dwpsn	file "../sci1.c",line 671,column 9,is_stmt
        B         $C$L91,UNC            ; [CPU_] |671| 
        ; branch occurs ; [] |671| 
$C$L89:    
	.dwpsn	file "../sci1.c",line 672,column 14,is_stmt
        MOVW      DP,#_ODV_Module1_MaxCellVoltage ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_MaxCellVoltage << #1 ; [CPU_] |672| 
        MOVW      DP,#_ODP_SafetyLimits_Umin ; [CPU_U] 
        CMP       AL,@_ODP_SafetyLimits_Umin ; [CPU_] |672| 
        B         $C$L90,HI             ; [CPU_] |672| 
        ; branchcc occurs ; [] |672| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |672| 
        BF        $C$L90,NEQ            ; [CPU_] |672| 
        ; branchcc occurs ; [] |672| 
	.dwpsn	file "../sci1.c",line 674,column 10,is_stmt
        MOVB      ACC,#1                ; [CPU_] |674| 
        MOVW      DP,#_Low_Voltage_ResetCounter ; [CPU_U] 
        ADDL      @_Low_Voltage_ResetCounter,ACC ; [CPU_] |674| 
	.dwpsn	file "../sci1.c",line 676,column 10,is_stmt
        MOV       ACC,#1200             ; [CPU_] |676| 
        CMPL      ACC,@_Low_Voltage_ResetCounter ; [CPU_] |676| 
        B         $C$L91,HIS            ; [CPU_] |676| 
        ; branchcc occurs ; [] |676| 
	.dwpsn	file "../sci1.c",line 678,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |678| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOVL      @_PAR_Capacity_Left,ACC ; [CPU_] |678| 
	.dwpsn	file "../sci1.c",line 685,column 9,is_stmt
        B         $C$L91,UNC            ; [CPU_] |685| 
        ; branch occurs ; [] |685| 
$C$L90:    
	.dwpsn	file "../sci1.c",line 688,column 10,is_stmt
        MOVB      ACC,#0                ; [CPU_] |688| 
        MOVW      DP,#_Low_Voltage_ResetCounter ; [CPU_U] 
        MOVL      @_Low_Voltage_ResetCounter,ACC ; [CPU_] |688| 
$C$L91:    
	.dwpsn	file "../sci1.c",line 699,column 9,is_stmt
	.dwpsn	file "../sci1.c",line 705,column 9,is_stmt
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Current ; [CPU_] |705| 
        CMPB      AL,#2                 ; [CPU_] |705| 
        B         $C$L92,LEQ            ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |705| 
        TBIT      *+XAR4[0],#3          ; [CPU_] |705| 
        BF        $C$L93,TC             ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
$C$L92:    
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        CMP       @_ODV_Module1_Current,#-2 ; [CPU_] |705| 
        B         $C$L100,GEQ           ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |705| 
        TBIT      *+XAR4[0],#2          ; [CPU_] |705| 
        BF        $C$L100,NTC           ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
$C$L93:    
        MOVW      DP,#_ODV_Module1_MaxCellVoltage ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_MaxCellVoltage << #1 ; [CPU_] |705| 
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        CMP       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |705| 
        B         $C$L100,HI            ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
        MOVW      DP,#_ODV_Module1_MinCellVoltage ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_MinCellVoltage << #1 ; [CPU_] |705| 
        MOVW      DP,#_ODP_SafetyLimits_Umin ; [CPU_U] 
        CMP       AL,@_ODP_SafetyLimits_Umin ; [CPU_] |705| 
        B         $C$L100,LO            ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
	.dwpsn	file "../sci1.c",line 707,column 11,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit+1 ; [CPU_U] 
        MOV       AL,@_ODV_Read_Inputs_16_Bit+1 ; [CPU_] |707| 
        MOV       *-SP[7],AL            ; [CPU_] |707| 
	.dwpsn	file "../sci1.c",line 708,column 11,is_stmt
        TBIT      *-SP[7],#1            ; [CPU_] |708| 
        BF        $C$L94,NTC            ; [CPU_] |708| 
        ; branchcc occurs ; [] |708| 
	.dwpsn	file "../sci1.c",line 709,column 13,is_stmt
        MOVW      DP,#_ODP_Analogue_Input_Offset_Integer ; [CPU_U] 
        MOV       AL,@_ODP_Analogue_Input_Offset_Integer ; [CPU_] |709| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit ; [CPU_U] 
        SUB       @_ODV_Read_Analogue_Input_16_Bit,AL ; [CPU_] |709| 
	.dwpsn	file "../sci1.c",line 710,column 13,is_stmt
        MOVW      DP,#_ODP_Analogue_Input_Offset_Integer+2 ; [CPU_U] 
        MOV       AL,@_ODP_Analogue_Input_Offset_Integer+2 ; [CPU_] |710| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_U] 
        ADD       @_ODV_Read_Analogue_Input_16_Bit+1,AL ; [CPU_] |710| 
$C$L94:    
	.dwpsn	file "../sci1.c",line 712,column 11,is_stmt
        TBIT      *-SP[7],#0            ; [CPU_] |712| 
        BF        $C$L95,NTC            ; [CPU_] |712| 
        ; branchcc occurs ; [] |712| 
	.dwpsn	file "../sci1.c",line 712,column 22,is_stmt
        MOVW      DP,#_ODP_Analogue_Input_Offset_Integer ; [CPU_U] 
        MOV       AL,@_ODP_Analogue_Input_Offset_Integer ; [CPU_] |712| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit ; [CPU_U] 
        ADD       @_ODV_Read_Analogue_Input_16_Bit,AL ; [CPU_] |712| 
$C$L95:    
	.dwpsn	file "../sci1.c",line 713,column 16,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |713| 
        MOVL      XAR4,#_ODV_Read_Analogue_Input_16_Bit ; [CPU_U] |714| 
        B         $C$L99,UNC            ; [CPU_] |713| 
        ; branch occurs ; [] |713| 
$C$L96:    
	.dwpsn	file "../sci1.c",line 714,column 13,is_stmt
        MOVZ      AR0,*-SP[5]           ; [CPU_] |714| 
        MOV       AL,*-SP[8]            ; [CPU_] |714| 
        CMP       AL,*+XAR4[AR0]        ; [CPU_] |714| 
        B         $C$L97,HIS            ; [CPU_] |714| 
        ; branchcc occurs ; [] |714| 
	.dwpsn	file "../sci1.c",line 715,column 15,is_stmt
        MOV       T,*-SP[5]             ; [CPU_] |715| 
        MOVB      AL,#1                 ; [CPU_] |715| 
        MOVW      DP,#_ODV_Read_Inputs_16_Bit+1 ; [CPU_U] 
        LSL       AL,T                  ; [CPU_] |715| 
        OR        @_ODV_Read_Inputs_16_Bit+1,AL ; [CPU_] |715| 
$C$L97:    
	.dwpsn	file "../sci1.c",line 716,column 13,is_stmt
        MOV       T,*-SP[5]             ; [CPU_] |716| 
        MOVB      AL,#1                 ; [CPU_] |716| 
        MOVW      DP,#_ODV_Read_Inputs_16_Bit+1 ; [CPU_U] 
        LSL       AL,T                  ; [CPU_] |716| 
        AND       AL,@_ODV_Read_Inputs_16_Bit+1 ; [CPU_] |716| 
        BF        $C$L98,EQ             ; [CPU_] |716| 
        ; branchcc occurs ; [] |716| 
        MOV       AL,*-SP[10]           ; [CPU_] |716| 
        CMP       AL,*+XAR4[AR0]        ; [CPU_] |716| 
        B         $C$L98,LOS            ; [CPU_] |716| 
        ; branchcc occurs ; [] |716| 
	.dwpsn	file "../sci1.c",line 717,column 15,is_stmt
        MOVB      AL,#1                 ; [CPU_] |717| 
        LSL       AL,T                  ; [CPU_] |717| 
        NOT       AL                    ; [CPU_] |717| 
        AND       @_ODV_Read_Inputs_16_Bit+1,AL ; [CPU_] |717| 
$C$L98:    
	.dwpsn	file "../sci1.c",line 713,column 25,is_stmt
        INC       *-SP[5]               ; [CPU_] |713| 
$C$L99:    
	.dwpsn	file "../sci1.c",line 713,column 20,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |713| 
        CMP       AL,*-SP[5]            ; [CPU_] |713| 
        B         $C$L96,HI             ; [CPU_] |713| 
        ; branchcc occurs ; [] |713| 
	.dwpsn	file "../sci1.c",line 719,column 11,is_stmt
        MOVW      DP,#_ODV_Read_Inputs_16_Bit+1 ; [CPU_U] 
        MOV       AL,@_ODV_Read_Inputs_16_Bit+1 ; [CPU_] |719| 
        CMP       AL,*-SP[7]            ; [CPU_] |719| 
        BF        $C$L101,EQ            ; [CPU_] |719| 
        ; branchcc occurs ; [] |719| 
	.dwpsn	file "../sci1.c",line 720,column 13,is_stmt
        MOVL      XAR4,#_ODV_Read_Inputs_16_Bit+1 ; [CPU_U] |720| 
        MOVB      AL,#20                ; [CPU_] |720| 
        MOVB      AH,#2                 ; [CPU_] |720| 
$C$DW$293	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$293, DW_AT_low_pc(0x00)
	.dwattr $C$DW$293, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$293, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |720| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |720| 
	.dwpsn	file "../sci1.c",line 721,column 9,is_stmt
        B         $C$L101,UNC           ; [CPU_] |721| 
        ; branch occurs ; [] |721| 
$C$L100:    
	.dwpsn	file "../sci1.c",line 722,column 14,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit+1 ; [CPU_U] 
        MOV       AL,@_ODV_Write_Outputs_16_Bit+1 ; [CPU_] |722| 
        MOVW      DP,#_ODV_Read_Inputs_16_Bit+1 ; [CPU_U] 
        CMP       AL,@_ODV_Read_Inputs_16_Bit+1 ; [CPU_] |722| 
        BF        $C$L101,EQ            ; [CPU_] |722| 
        ; branchcc occurs ; [] |722| 
	.dwpsn	file "../sci1.c",line 723,column 11,is_stmt
        MOV       @_ODV_Read_Inputs_16_Bit+1,AL ; [CPU_] |723| 
	.dwpsn	file "../sci1.c",line 724,column 11,is_stmt
        MOVL      XAR4,#_ODV_Read_Inputs_16_Bit+1 ; [CPU_U] |724| 
        MOVB      AH,#2                 ; [CPU_] |724| 
        MOVB      AL,#20                ; [CPU_] |724| 
$C$DW$294	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$294, DW_AT_low_pc(0x00)
	.dwattr $C$DW$294, DW_AT_name("_SCI1_WriteMsg")
	.dwattr $C$DW$294, DW_AT_TI_call
        LCR       #_SCI1_WriteMsg       ; [CPU_] |724| 
        ; call occurs [#_SCI1_WriteMsg] ; [] |724| 
$C$L101:    
	.dwpsn	file "../sci1.c",line 726,column 9,is_stmt
        MOVW      DP,#_HAL_Enable       ; [CPU_U] 
        MOVB      @_HAL_Enable,#1,UNC   ; [CPU_] |726| 
	.dwpsn	file "../sci1.c",line 727,column 7,is_stmt
        B         $C$L104,UNC           ; [CPU_] |727| 
        ; branch occurs ; [] |727| 
	.dwendtag $C$DW$245

$C$L102:    
	.dwpsn	file "../sci1.c",line 413,column 3,is_stmt
        MOVW      DP,#_SCI_MsgInRS232   ; [CPU_U] 
        MOV       AL,@_SCI_MsgInRS232   ; [CPU_] |413| 
        CMPB      AL,#197               ; [CPU_] |413| 
        B         $C$L103,GT            ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
        CMPB      AL,#197               ; [CPU_] |413| 
        BF        $C$L50,EQ             ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
        CMPB      AL,#193               ; [CPU_] |413| 
        BF        $C$L54,EQ             ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
        CMPB      AL,#195               ; [CPU_] |413| 
        BF        $C$L55,EQ             ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
        CMPB      AL,#196               ; [CPU_] |413| 
        BF        $C$L56,EQ             ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
        B         $C$L104,UNC           ; [CPU_] |413| 
        ; branch occurs ; [] |413| 
$C$L103:    
        CMPB      AL,#199               ; [CPU_] |413| 
        BF        $C$L57,EQ             ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
        CMPB      AL,#211               ; [CPU_] |413| 
        BF        $C$L53,EQ             ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
        CMPB      AL,#255               ; [CPU_] |413| 
        BF        $C$L104,EQ            ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
	.dwpsn	file "../sci1.c",line 729,column 1,is_stmt
$C$L104:    
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$295	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$295, DW_AT_low_pc(0x00)
	.dwattr $C$DW$295, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$236, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$236, DW_AT_TI_end_line(0x2d9)
	.dwattr $C$DW$236, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$236

	.sect	".text"
	.global	_Put_Sci1

$C$DW$296	.dwtag  DW_TAG_subprogram, DW_AT_name("Put_Sci1")
	.dwattr $C$DW$296, DW_AT_low_pc(_Put_Sci1)
	.dwattr $C$DW$296, DW_AT_high_pc(0x00)
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_Put_Sci1")
	.dwattr $C$DW$296, DW_AT_external
	.dwattr $C$DW$296, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$296, DW_AT_TI_begin_line(0x2de)
	.dwattr $C$DW$296, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$296, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../sci1.c",line 734,column 24,is_stmt,address _Put_Sci1

	.dwfde $C$DW$CIE, _Put_Sci1
$C$DW$297	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ch")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_ch")
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$297, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Put_Sci1                     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_Put_Sci1:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$298	.dwtag  DW_TAG_variable, DW_AT_name("ch")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_ch")
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$298, DW_AT_location[DW_OP_breg20 -1]
$C$DW$299	.dwtag  DW_TAG_variable, DW_AT_name("fifo_len")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_fifo_len")
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$299, DW_AT_location[DW_OP_breg20 -2]
        MOV       *-SP[1],AL            ; [CPU_] |734| 
	.dwpsn	file "../sci1.c",line 735,column 19,is_stmt
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |735| 
        MOVB      XAR0,#10              ; [CPU_] |735| 
        AND       AL,*+XAR4[AR0],#0x1f00 ; [CPU_] |735| 
        LSR       AL,8                  ; [CPU_] |735| 
        MOV       *-SP[2],AL            ; [CPU_] |735| 
	.dwpsn	file "../sci1.c",line 736,column 9,is_stmt
        CMPB      AL,#4                 ; [CPU_] |736| 
        B         $C$L106,LO            ; [CPU_] |736| 
        ; branchcc occurs ; [] |736| 
$C$L105:    
	.dwpsn	file "../sci1.c",line 737,column 5,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |737| 
        MOVB      XAR0,#10              ; [CPU_] |737| 
        AND       AL,*+XAR4[AR0],#0x1f00 ; [CPU_] |737| 
        LSR       AL,8                  ; [CPU_] |737| 
        MOV       *-SP[2],AL            ; [CPU_] |737| 
	.dwpsn	file "../sci1.c",line 736,column 9,is_stmt
        CMPB      AL,#4                 ; [CPU_] |736| 
        B         $C$L105,HIS           ; [CPU_] |736| 
        ; branchcc occurs ; [] |736| 
$C$L106:    
	.dwpsn	file "../sci1.c",line 739,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |739| 
        MOV       AL,*-SP[1]            ; [CPU_] |739| 
        MOVB      XAR0,#9               ; [CPU_] |739| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |739| 
	.dwpsn	file "../sci1.c",line 740,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$300	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$300, DW_AT_low_pc(0x00)
	.dwattr $C$DW$300, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$296, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$296, DW_AT_TI_end_line(0x2e4)
	.dwattr $C$DW$296, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$296

	.sect	".text"
	.global	_SCI1_ComputeCRC16

$C$DW$301	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_ComputeCRC16")
	.dwattr $C$DW$301, DW_AT_low_pc(_SCI1_ComputeCRC16)
	.dwattr $C$DW$301, DW_AT_high_pc(0x00)
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_SCI1_ComputeCRC16")
	.dwattr $C$DW$301, DW_AT_external
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$301, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$301, DW_AT_TI_begin_line(0x2e6)
	.dwattr $C$DW$301, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$301, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../sci1.c",line 742,column 49,is_stmt,address _SCI1_ComputeCRC16

	.dwfde $C$DW$CIE, _SCI1_ComputeCRC16
$C$DW$302	.dwtag  DW_TAG_formal_parameter, DW_AT_name("buf")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_buf")
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$193)
	.dwattr $C$DW$302, DW_AT_location[DW_OP_reg12]
$C$DW$303	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$303, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI1_ComputeCRC16            FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  9 Auto,  0 SOE     *
;***************************************************************

_SCI1_ComputeCRC16:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$304	.dwtag  DW_TAG_variable, DW_AT_name("buf")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_buf")
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$193)
	.dwattr $C$DW$304, DW_AT_location[DW_OP_breg20 -2]
$C$DW$305	.dwtag  DW_TAG_variable, DW_AT_name("len")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$305, DW_AT_location[DW_OP_breg20 -3]
$C$DW$306	.dwtag  DW_TAG_variable, DW_AT_name("crc")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_crc")
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$306, DW_AT_location[DW_OP_breg20 -6]
$C$DW$307	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$307, DW_AT_location[DW_OP_breg20 -8]
$C$DW$308	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$308, DW_AT_location[DW_OP_breg20 -9]
        MOV       *-SP[3],AL            ; [CPU_] |742| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |742| 
	.dwpsn	file "../sci1.c",line 743,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |743| 
        MOVL      *-SP[6],ACC           ; [CPU_] |743| 
	.dwpsn	file "../sci1.c",line 743,column 24,is_stmt
        MOVL      *-SP[8],ACC           ; [CPU_] |743| 
	.dwpsn	file "../sci1.c",line 745,column 3,is_stmt
        B         $C$L111,UNC           ; [CPU_] |745| 
        ; branch occurs ; [] |745| 
$C$L107:    
	.dwpsn	file "../sci1.c",line 746,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |746| 
        MOV       AL,*XAR4++            ; [CPU_] |746| 
        MOVU      ACC,AL                ; [CPU_] |746| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |746| 
        MOVL      *-SP[8],ACC           ; [CPU_] |746| 
	.dwpsn	file "../sci1.c",line 747,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |747| 
        LSL       ACC,16                ; [CPU_] |747| 
        OR        *-SP[6],AL            ; [CPU_] |747| 
        OR        *-SP[5],AH            ; [CPU_] |747| 
	.dwpsn	file "../sci1.c",line 748,column 10,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |748| 
	.dwpsn	file "../sci1.c",line 748,column 17,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |748| 
        CMPB      AL,#8                 ; [CPU_] |748| 
        B         $C$L111,HIS           ; [CPU_] |748| 
        ; branchcc occurs ; [] |748| 
$C$L108:    
	.dwpsn	file "../sci1.c",line 749,column 7,is_stmt
        TBIT      *-SP[6],#0            ; [CPU_] |749| 
        BF        $C$L109,NTC           ; [CPU_] |749| 
        ; branchcc occurs ; [] |749| 
        MOVL      XAR6,#40961           ; [CPU_] |749| 
        B         $C$L110,UNC           ; [CPU_] |749| 
        ; branch occurs ; [] |749| 
$C$L109:    
        MOVB      XAR6,#0               ; [CPU_] |749| 
$C$L110:    
        MOVL      ACC,*-SP[6]           ; [CPU_] |749| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,1                 ; [CPU_] |749| 
        XOR       ACC,AR6               ; [CPU_] |749| 
        MOVL      *-SP[6],ACC           ; [CPU_] |749| 
	.dwpsn	file "../sci1.c",line 748,column 24,is_stmt
        INC       *-SP[9]               ; [CPU_] |748| 
	.dwpsn	file "../sci1.c",line 748,column 17,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |748| 
        CMPB      AL,#8                 ; [CPU_] |748| 
        B         $C$L108,LO            ; [CPU_] |748| 
        ; branchcc occurs ; [] |748| 
$C$L111:    
	.dwpsn	file "../sci1.c",line 750,column 3,is_stmt
        MOV       AH,*-SP[3]            ; [CPU_] |750| 
        DEC       *-SP[3]               ; [CPU_] |750| 
        CMPB      AH,#0                 ; [CPU_] |750| 
        BF        $C$L107,NEQ           ; [CPU_] |750| 
        ; branchcc occurs ; [] |750| 
	.dwpsn	file "../sci1.c",line 751,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |751| 
	.dwpsn	file "../sci1.c",line 752,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$309	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$309, DW_AT_low_pc(0x00)
	.dwattr $C$DW$309, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$301, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$301, DW_AT_TI_end_line(0x2f0)
	.dwattr $C$DW$301, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$301

	.sect	".text"
	.global	_SCI1_SendMessage

$C$DW$310	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_SendMessage")
	.dwattr $C$DW$310, DW_AT_low_pc(_SCI1_SendMessage)
	.dwattr $C$DW$310, DW_AT_high_pc(0x00)
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_SCI1_SendMessage")
	.dwattr $C$DW$310, DW_AT_external
	.dwattr $C$DW$310, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$310, DW_AT_TI_begin_line(0x2f2)
	.dwattr $C$DW$310, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$310, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../sci1.c",line 754,column 41,is_stmt,address _SCI1_SendMessage

	.dwfde $C$DW$CIE, _SCI1_SendMessage
$C$DW$311	.dwtag  DW_TAG_formal_parameter, DW_AT_name("msg")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$311, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _SCI1_SendMessage             FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_SCI1_SendMessage:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$312	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$312, DW_AT_location[DW_OP_breg20 -2]
$C$DW$313	.dwtag  DW_TAG_variable, DW_AT_name("len")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$313, DW_AT_location[DW_OP_breg20 -3]
$C$DW$314	.dwtag  DW_TAG_variable, DW_AT_name("cks")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_cks")
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$314, DW_AT_location[DW_OP_breg20 -4]
$C$DW$315	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$315, DW_AT_location[DW_OP_breg20 -5]
        MOVL      *-SP[2],XAR4          ; [CPU_] |754| 
	.dwpsn	file "../sci1.c",line 758,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |758| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |758| 
        MOV       *-SP[3],AL            ; [CPU_] |758| 
	.dwpsn	file "../sci1.c",line 759,column 3,is_stmt
        MOVW      DP,#_SCI1_CheckSum    ; [CPU_U] 
        MOV       @_SCI1_CheckSum,#0    ; [CPU_] |759| 
	.dwpsn	file "../sci1.c",line 760,column 8,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |760| 
        B         $C$L113,UNC           ; [CPU_] |760| 
        ; branch occurs ; [] |760| 
$C$L112:    
	.dwpsn	file "../sci1.c",line 761,column 5,is_stmt
        MOVZ      AR6,*-SP[5]           ; [CPU_] |761| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |761| 
        ADDU      ACC,AR6               ; [CPU_] |761| 
        MOVL      XAR4,ACC              ; [CPU_] |761| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |761| 
$C$DW$316	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$316, DW_AT_low_pc(0x00)
	.dwattr $C$DW$316, DW_AT_name("_Put_Sci1")
	.dwattr $C$DW$316, DW_AT_TI_call
        LCR       #_Put_Sci1            ; [CPU_] |761| 
        ; call occurs [#_Put_Sci1] ; [] |761| 
	.dwpsn	file "../sci1.c",line 760,column 20,is_stmt
        INC       *-SP[5]               ; [CPU_] |760| 
$C$L113:    
	.dwpsn	file "../sci1.c",line 760,column 13,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |760| 
        CMP       AL,*-SP[5]            ; [CPU_] |760| 
        B         $C$L112,HI            ; [CPU_] |760| 
        ; branchcc occurs ; [] |760| 
	.dwpsn	file "../sci1.c",line 763,column 3,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |763| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |763| 
        ADDU      ACC,AR6               ; [CPU_] |763| 
        MOVL      XAR4,ACC              ; [CPU_] |763| 
        MOV       *+XAR4[2],#0          ; [CPU_] |763| 
	.dwpsn	file "../sci1.c",line 763,column 23,is_stmt
        MOVZ      AR5,*-SP[3]           ; [CPU_] |763| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |763| 
        ADDB      XAR5,#1               ; [CPU_] |763| 
        ADDU      ACC,AR5               ; [CPU_] |763| 
        MOVL      XAR4,ACC              ; [CPU_] |763| 
        MOV       *+XAR4[2],#0          ; [CPU_] |763| 
	.dwpsn	file "../sci1.c",line 764,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |764| 
        MOV       AL,*-SP[3]            ; [CPU_] |764| 
        ADDB      AL,#2                 ; [CPU_] |764| 
        ADDB      XAR4,#2               ; [CPU_] |764| 
$C$DW$317	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$317, DW_AT_low_pc(0x00)
	.dwattr $C$DW$317, DW_AT_name("_SCI1_ComputeCRC16")
	.dwattr $C$DW$317, DW_AT_TI_call
        LCR       #_SCI1_ComputeCRC16   ; [CPU_] |764| 
        ; call occurs [#_SCI1_ComputeCRC16] ; [] |764| 
        MOV       *-SP[4],AL            ; [CPU_] |764| 
	.dwpsn	file "../sci1.c",line 765,column 3,is_stmt
        AND       AL,*-SP[4],#0x00ff    ; [CPU_] |765| 
$C$DW$318	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$318, DW_AT_low_pc(0x00)
	.dwattr $C$DW$318, DW_AT_name("_Put_Sci1")
	.dwattr $C$DW$318, DW_AT_TI_call
        LCR       #_Put_Sci1            ; [CPU_] |765| 
        ; call occurs [#_Put_Sci1] ; [] |765| 
	.dwpsn	file "../sci1.c",line 766,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |766| 
        LSR       AL,8                  ; [CPU_] |766| 
$C$DW$319	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$319, DW_AT_low_pc(0x00)
	.dwattr $C$DW$319, DW_AT_name("_Put_Sci1")
	.dwattr $C$DW$319, DW_AT_TI_call
        LCR       #_Put_Sci1            ; [CPU_] |766| 
        ; call occurs [#_Put_Sci1] ; [] |766| 
	.dwpsn	file "../sci1.c",line 767,column 3,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |767| 
        MOVZ      AR7,*-SP[4]           ; [CPU_] |767| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |767| 
        ADDU      ACC,AR6               ; [CPU_] |767| 
        MOVL      XAR4,ACC              ; [CPU_] |767| 
        MOV       *+XAR4[2],AR7         ; [CPU_] |767| 
	.dwpsn	file "../sci1.c",line 768,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |768| 
        OR        *+XAR4[0],#0x0080     ; [CPU_] |768| 
	.dwpsn	file "../sci1.c",line 769,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$320	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$320, DW_AT_low_pc(0x00)
	.dwattr $C$DW$320, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$310, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$310, DW_AT_TI_end_line(0x301)
	.dwattr $C$DW$310, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$310

	.sect	".text:retain"
	.global	_RS232_RXI_Int

$C$DW$321	.dwtag  DW_TAG_subprogram, DW_AT_name("RS232_RXI_Int")
	.dwattr $C$DW$321, DW_AT_low_pc(_RS232_RXI_Int)
	.dwattr $C$DW$321, DW_AT_high_pc(0x00)
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_RS232_RXI_Int")
	.dwattr $C$DW$321, DW_AT_external
	.dwattr $C$DW$321, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$321, DW_AT_TI_begin_line(0x31c)
	.dwattr $C$DW$321, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$321, DW_AT_TI_interrupt
	.dwattr $C$DW$321, DW_AT_TI_max_frame_size(-30)
	.dwpsn	file "../sci1.c",line 798,column 1,is_stmt,address _RS232_RXI_Int

	.dwfde $C$DW$CIE, _RS232_RXI_Int

;***************************************************************
;* FNAME: _RS232_RXI_Int                FR SIZE:  28           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto, 24 SOE     *
;***************************************************************

_RS232_RXI_Int:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        PUSH      AR1H:AR0H             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 5, 4
	.dwcfi	save_reg_to_mem, 7, 5
	.dwcfi	cfa_offset, -6
        MOVL      *SP++,XT              ; [CPU_] 
	.dwcfi	save_reg_to_mem, 21, 6
	.dwcfi	save_reg_to_mem, 22, 7
	.dwcfi	cfa_offset, -8
        MOVL      *SP++,XAR4            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 12, 8
	.dwcfi	save_reg_to_mem, 13, 9
	.dwcfi	cfa_offset, -10
        MOVL      *SP++,XAR5            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 14, 10
	.dwcfi	save_reg_to_mem, 15, 11
	.dwcfi	cfa_offset, -12
        MOVL      *SP++,XAR6            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 16, 12
	.dwcfi	save_reg_to_mem, 17, 13
	.dwcfi	cfa_offset, -14
        MOVL      *SP++,XAR7            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 18, 14
	.dwcfi	save_reg_to_mem, 19, 15
	.dwcfi	cfa_offset, -16
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 16
	.dwcfi	save_reg_to_mem, 40, 17
	.dwcfi	cfa_offset, -18
        MOV32     *SP++,R0H             ; [CPU_] 
        MOV32     *SP++,R1H             ; [CPU_] 
        MOV32     *SP++,R2H             ; [CPU_] 
        MOV32     *SP++,R3H             ; [CPU_] 
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -30
        SPM       0                     ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
$C$DW$322	.dwtag  DW_TAG_variable, DW_AT_name("incar")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_incar")
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$322, DW_AT_location[DW_OP_breg20 -1]
$C$DW$323	.dwtag  DW_TAG_variable, DW_AT_name("cks")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_cks")
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$323, DW_AT_location[DW_OP_breg20 -2]
$C$DW$324	.dwtag  DW_TAG_variable, DW_AT_name("len")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$324, DW_AT_location[DW_OP_breg20 -3]
	.dwpsn	file "../sci1.c",line 800,column 14,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |800| 
	.dwpsn	file "../sci1.c",line 802,column 3,is_stmt
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |802| 
        MOV       AL,*+XAR4[7]          ; [CPU_] |802| 
        ANDB      AL,#0xff              ; [CPU_] |802| 
        MOV       *-SP[1],AL            ; [CPU_] |802| 
	.dwpsn	file "../sci1.c",line 803,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |803| 
        ADDB      XAR4,#11              ; [CPU_] |803| 
        OR        *+XAR4[0],#0x4000     ; [CPU_] |803| 
	.dwpsn	file "../sci1.c",line 804,column 3,is_stmt
        MOVL      XAR4,@_SciRegs        ; [CPU_] |804| 
        ADDB      XAR4,#11              ; [CPU_] |804| 
        OR        *+XAR4[0],#0x0040     ; [CPU_] |804| 
	.dwpsn	file "../sci1.c",line 805,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        OR        @_PieCtrlRegs+1,#0x0100 ; [CPU_] |805| 
	.dwpsn	file "../sci1.c",line 807,column 3,is_stmt
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |807| 
        TBIT      *+XAR4[5],#7          ; [CPU_] |807| 
        BF        $C$L119,NTC           ; [CPU_] |807| 
        ; branchcc occurs ; [] |807| 
	.dwpsn	file "../sci1.c",line 809,column 5,is_stmt
        MOVW      DP,#_ODV_CommError_Set ; [CPU_U] 
        MOVB      @_ODV_CommError_Set,#1,UNC ; [CPU_] |809| 
	.dwpsn	file "../sci1.c",line 810,column 5,is_stmt
        B         $C$L120,UNC           ; [CPU_] |810| 
        ; branch occurs ; [] |810| 
$C$L114:    
	.dwpsn	file "../sci1.c",line 816,column 7,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |816| 
        MOVW      DP,#_SCI_MsgInRS232+1 ; [CPU_U] 
        MOV       @_SCI_MsgInRS232+1,AL ; [CPU_] |816| 
	.dwpsn	file "../sci1.c",line 817,column 7,is_stmt
        MOVW      DP,#_ComStarted       ; [CPU_U] 
        MOVB      @_ComStarted,#1,UNC   ; [CPU_] |817| 
	.dwpsn	file "../sci1.c",line 818,column 7,is_stmt
        MOVL      XAR4,#_SCI_MsgInRS232+2 ; [CPU_U] |818| 
        MOVL      @_SCI_CharPtr,XAR4    ; [CPU_] |818| 
	.dwpsn	file "../sci1.c",line 819,column 7,is_stmt
        MOVB      @_RS232RecvState,#2,UNC ; [CPU_] |819| 
	.dwpsn	file "../sci1.c",line 820,column 7,is_stmt
        MOVB      @_SCI_CharLen,#1,UNC  ; [CPU_] |820| 
	.dwpsn	file "../sci1.c",line 822,column 5,is_stmt
        B         $C$L120,UNC           ; [CPU_] |822| 
        ; branch occurs ; [] |822| 
$C$L115:    
	.dwpsn	file "../sci1.c",line 826,column 5,is_stmt
        INC       @_SCI_CharLen         ; [CPU_] |826| 
	.dwpsn	file "../sci1.c",line 827,column 5,is_stmt
        MOVL      XAR4,@_SCI_CharPtr    ; [CPU_] |827| 
        MOVL      XAR6,XAR4             ; [CPU_] |827| 
        MOV       AL,*-SP[1]            ; [CPU_] |827| 
        ADDB      XAR6,#1               ; [CPU_] |827| 
        MOVL      @_SCI_CharPtr,XAR6    ; [CPU_] |827| 
        MOV       *+XAR4[0],AL          ; [CPU_] |827| 
	.dwpsn	file "../sci1.c",line 828,column 5,is_stmt
        MOVW      DP,#_SCI_MsgInRS232+1 ; [CPU_U] 
        MOV       AL,@_SCI_MsgInRS232+1 ; [CPU_] |828| 
        MOVW      DP,#_SCI_CharLen      ; [CPU_U] 
        ADDB      AL,#3                 ; [CPU_] |828| 
        CMP       AL,@_SCI_CharLen      ; [CPU_] |828| 
        B         $C$L120,HI            ; [CPU_] |828| 
        ; branchcc occurs ; [] |828| 
	.dwpsn	file "../sci1.c",line 829,column 7,is_stmt
        MOVB      @_RS232RecvState,#3,UNC ; [CPU_] |829| 
	.dwpsn	file "../sci1.c",line 830,column 5,is_stmt
        B         $C$L120,UNC           ; [CPU_] |830| 
        ; branch occurs ; [] |830| 
$C$L116:    
	.dwpsn	file "../sci1.c",line 833,column 5,is_stmt
        MOV       @_ComStarted,#0       ; [CPU_] |833| 
	.dwpsn	file "../sci1.c",line 834,column 5,is_stmt
        MOV       AL,@_SCI_CharLen      ; [CPU_] |834| 
        ADDB      AL,#1                 ; [CPU_] |834| 
        MOV       *-SP[3],AL            ; [CPU_] |834| 
        MOV       @_SCI_CharLen,AL      ; [CPU_] |834| 
	.dwpsn	file "../sci1.c",line 835,column 5,is_stmt
        MOVL      XAR4,@_SCI_CharPtr    ; [CPU_] |835| 
        MOVL      XAR6,XAR4             ; [CPU_] |835| 
        MOV       AL,*-SP[1]            ; [CPU_] |835| 
        ADDB      XAR6,#1               ; [CPU_] |835| 
        MOVL      @_SCI_CharPtr,XAR6    ; [CPU_] |835| 
        MOV       *+XAR4[0],AL          ; [CPU_] |835| 
	.dwpsn	file "../sci1.c",line 836,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |836| 
        MOVL      XAR4,#_SCI_MsgInRS232+1 ; [CPU_U] |836| 
$C$DW$325	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$325, DW_AT_low_pc(0x00)
	.dwattr $C$DW$325, DW_AT_name("_SCI1_ComputeCRC16")
	.dwattr $C$DW$325, DW_AT_TI_call
        LCR       #_SCI1_ComputeCRC16   ; [CPU_] |836| 
        ; call occurs [#_SCI1_ComputeCRC16] ; [] |836| 
        MOV       *-SP[2],AL            ; [CPU_] |836| 
	.dwpsn	file "../sci1.c",line 837,column 5,is_stmt
        MOVW      DP,#_SCI_MsgInRS232+1 ; [CPU_U] 
        INC       @_SCI_MsgInRS232+1    ; [CPU_] |837| 
	.dwpsn	file "../sci1.c",line 838,column 5,is_stmt
        CMPB      AL,#0                 ; [CPU_] |838| 
        BF        $C$L117,NEQ           ; [CPU_] |838| 
        ; branchcc occurs ; [] |838| 
	.dwpsn	file "../sci1.c",line 840,column 7,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOVB      @_SCI_MsgAvailable,#1,UNC ; [CPU_] |840| 
	.dwpsn	file "../sci1.c",line 841,column 5,is_stmt
        B         $C$L118,UNC           ; [CPU_] |841| 
        ; branch occurs ; [] |841| 
$C$L117:    
	.dwpsn	file "../sci1.c",line 844,column 7,is_stmt
        MOVB      @_SCI_MsgInRS232,#255,UNC ; [CPU_] |844| 
	.dwpsn	file "../sci1.c",line 845,column 7,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOVB      @_SCI_MsgAvailable,#1,UNC ; [CPU_] |845| 
$C$L118:    
	.dwpsn	file "../sci1.c",line 847,column 5,is_stmt
        MOV       @_RS232RecvState,#0   ; [CPU_] |847| 
	.dwpsn	file "../sci1.c",line 848,column 5,is_stmt
        MOV       @_RS232Timer,#0       ; [CPU_] |848| 
	.dwpsn	file "../sci1.c",line 849,column 5,is_stmt
        B         $C$L120,UNC           ; [CPU_] |849| 
        ; branch occurs ; [] |849| 
$C$L119:    
	.dwpsn	file "../sci1.c",line 812,column 3,is_stmt
        MOV       AL,@_RS232RecvState   ; [CPU_] |812| 
        BF        $C$L114,EQ            ; [CPU_] |812| 
        ; branchcc occurs ; [] |812| 
        CMPB      AL,#2                 ; [CPU_] |812| 
        BF        $C$L115,EQ            ; [CPU_] |812| 
        ; branchcc occurs ; [] |812| 
        CMPB      AL,#3                 ; [CPU_] |812| 
        BF        $C$L116,EQ            ; [CPU_] |812| 
        ; branchcc occurs ; [] |812| 
        B         $C$L120,UNC           ; [CPU_] |812| 
        ; branch occurs ; [] |812| 
$C$L120:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -26
        MOV32     R3H,*--SP             ; [CPU_] 
        MOV32     R2H,*--SP             ; [CPU_] 
        MOV32     R1H,*--SP             ; [CPU_] 
        MOV32     R0H,*--SP             ; [CPU_] 
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -16
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        MOVL      XAR7,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -14
	.dwcfi	restore_reg, 18
        MOVL      XAR6,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -12
	.dwcfi	restore_reg, 16
        MOVL      XAR5,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -10
	.dwcfi	restore_reg, 14
        MOVL      XAR4,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -8
	.dwcfi	restore_reg, 12
        MOVL      XT,*--SP              ; [CPU_] 
	.dwcfi	cfa_offset, -6
	.dwcfi	restore_reg, 22
	.dwcfi	restore_reg, 21
        POP       AR1H:AR0H             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 5
	.dwcfi	restore_reg, 7
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$326	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$326, DW_AT_low_pc(0x00)
	.dwattr $C$DW$326, DW_AT_TI_return
        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$321, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$321, DW_AT_TI_end_line(0x356)
	.dwattr $C$DW$321, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$321

	.sect	".text:retain"
	.global	_RS232_TEI_Int

$C$DW$327	.dwtag  DW_TAG_subprogram, DW_AT_name("RS232_TEI_Int")
	.dwattr $C$DW$327, DW_AT_low_pc(_RS232_TEI_Int)
	.dwattr $C$DW$327, DW_AT_high_pc(0x00)
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_RS232_TEI_Int")
	.dwattr $C$DW$327, DW_AT_external
	.dwattr $C$DW$327, DW_AT_TI_begin_file("../sci1.c")
	.dwattr $C$DW$327, DW_AT_TI_begin_line(0x359)
	.dwattr $C$DW$327, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$327, DW_AT_TI_interrupt
	.dwattr $C$DW$327, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../sci1.c",line 858,column 1,is_stmt,address _RS232_TEI_Int

	.dwfde $C$DW$CIE, _RS232_TEI_Int

;***************************************************************
;* FNAME: _RS232_TEI_Int                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  6 SOE     *
;***************************************************************

_RS232_TEI_Int:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        MOVL      *SP++,XAR4            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 12, 4
	.dwcfi	save_reg_to_mem, 13, 5
	.dwcfi	cfa_offset, -6
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 6
	.dwcfi	save_reg_to_mem, 40, 7
	.dwcfi	cfa_offset, -8
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
	.dwpsn	file "../sci1.c",line 859,column 3,is_stmt
        MOVW      DP,#_SciRegs          ; [CPU_U] 
        MOVL      XAR4,@_SciRegs        ; [CPU_] |859| 
        ADDB      XAR4,#10              ; [CPU_] |859| 
        OR        *+XAR4[0],#0x0040     ; [CPU_] |859| 
	.dwpsn	file "../sci1.c",line 860,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        OR        @_PieCtrlRegs+1,#0x0100 ; [CPU_] |860| 
	.dwpsn	file "../sci1.c",line 861,column 1,is_stmt
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -6
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        MOVL      XAR4,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 12
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$328	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$328, DW_AT_low_pc(0x00)
	.dwattr $C$DW$328, DW_AT_TI_return
        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$327, DW_AT_TI_end_file("../sci1.c")
	.dwattr $C$DW$327, DW_AT_TI_end_line(0x35d)
	.dwattr $C$DW$327, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$327

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_DSP28x_usDelay
	.global	_ODV_Module1_Current_Average
	.global	_ODP_Module1_StandbyCurrent
	.global	_ODV_Module1_MaxDeltaVoltage
	.global	_ODV_Module1_Module_SOC_Calibration
	.global	_HAL_Enable
	.global	_ODV_CommError_Set
	.global	_ODV_Temperature_Average_data
	.global	_ODV_Module1_Current
	.global	_ODV_Module1_Voltage
	.global	_ODV_Module1_MinCellVoltage
	.global	_ODP_SafetyLimits_BalancingTimeout
	.global	_ODP_Module1_Module_SOC_Current
	.global	_ODV_Module1_MaxCellVoltage
	.global	_ODP_SafetyLimits_DamagedVoltage
	.global	_ODV_Temperature_Min
	.global	_ODV_SciSend
	.global	_ODV_Gateway_Current
	.global	_ODP_SafetyLimits_Umin
	.global	_ODP_SafetyLimits_Umax_bal_delta
	.global	_ODP_SafetyLimits_Umax
	.global	_MBX_pend
	.global	_MBX_post
	.global	_HAL_LectureADTemperature
	.global	_SEM_pend
	.global	_ODP_SafetyLimits_OverVoltage
	.global	_ODP_SafetyLimits_UnderVoltage
	.global	_ODP_SafetyLimits_Overcurrent
	.global	_ODP_SafetyLimits_Cell_Nb
	.global	_ODP_SafetyLimits_Resistor_Tmin
	.global	_ODP_SafetyLimits_Resistor_Tmax
	.global	_ODP_SafetyLimits_Mosfet_Tmin
	.global	_ODP_SafetyLimits_Mosfet_Tmax
	.global	_ODP_SafetyLimits_T3_min
	.global	_ODP_SafetyLimits_T3_max
	.global	_ODP_SafetyLimits_Umin_bal_delta
	.global	_ODP_SafetyLimits_UnderCurrent
	.global	_HAL_LectureTemperature
	.global	_ODV_Read_Inputs_16_Bit
	.global	_ODP_Board_SerialNumber
	.global	_CNV_Round
	.global	_PAR_Capacity_Total
	.global	_HAL_Current_Sum
	.global	_MMSConfig
	.global	_PAR_Capacity_Left
	.global	_ODV_Write_Outputs_16_Bit
	.global	_ODV_ErrorDsp_ErrorNumber
	.global	_gaincell
	.global	_PAR_Capacity_TotalLife_Used
	.global	_SciaRegs
	.global	_TSK_timerSem
	.global	_ODV_Read_Analogue_Input_16_Bit
	.global	_PieCtrlRegs
	.global	_GpioDataRegs
	.global	_ODP_Analogue_Input_Scaling_Float
	.global	_ODP_Analogue_Input_Offset_Integer
	.global	_sci_rx_mbox
	.global	_SysCtrlRegs
	.global	_GpioCtrlRegs
	.global	_PieVectTable
	.global	FS$$DIV
	.global	I$$DIV
	.global	L$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x48)
$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$329, DW_AT_name("cmd")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_cmd")
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$330, DW_AT_name("len")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$331, DW_AT_name("data")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$141	.dwtag  DW_TAG_typedef, DW_AT_name("T_UsbMessage")
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$141, DW_AT_language(DW_LANG_C)
$C$DW$T$142	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$T$142, DW_AT_address_class(0x16)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$332, DW_AT_name("can_wk")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_can_wk")
	.dwattr $C$DW$332, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$333, DW_AT_name("sw_wk")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_sw_wk")
	.dwattr $C$DW$333, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$334, DW_AT_name("bal_dic")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_bal_dic")
	.dwattr $C$DW$334, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$335, DW_AT_name("bal_ch")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_bal_ch")
	.dwattr $C$DW$335, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$336, DW_AT_name("bal_ocv")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_bal_ocv")
	.dwattr $C$DW$336, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$337, DW_AT_name("lem")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_lem")
	.dwattr $C$DW$337, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$338, DW_AT_name("shunt")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_shunt")
	.dwattr $C$DW$338, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$339, DW_AT_name("relay_on")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_relay_on")
	.dwattr $C$DW$339, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$340, DW_AT_name("ch_wk")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_ch_wk")
	.dwattr $C$DW$340, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$341, DW_AT_name("SOC2")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_SOC2")
	.dwattr $C$DW$341, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$342, DW_AT_name("foil")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_foil")
	.dwattr $C$DW$342, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$343, DW_AT_name("rel_sta")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_rel_sta")
	.dwattr $C$DW$343, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$344, DW_AT_name("b12")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_b12")
	.dwattr $C$DW$344, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$345, DW_AT_name("b13")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_b13")
	.dwattr $C$DW$345, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$346, DW_AT_name("b14")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_b14")
	.dwattr $C$DW$346, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$347, DW_AT_name("b15")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_b15")
	.dwattr $C$DW$347, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$143	.dwtag  DW_TAG_typedef, DW_AT_name("TMMSConfig")
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$143, DW_AT_language(DW_LANG_C)
$C$DW$T$144	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$T$144, DW_AT_address_class(0x16)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x04)
$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$348, DW_AT_name("baud")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_baud")
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$349, DW_AT_name("n")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$350, DW_AT_name("brr")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_brr")
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$145	.dwtag  DW_TAG_typedef, DW_AT_name("T_BaudCfg")
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$145, DW_AT_language(DW_LANG_C)
$C$DW$351	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$145)
$C$DW$T$146	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$351)

$C$DW$T$147	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$T$147, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$147, DW_AT_byte_size(0x20)
$C$DW$352	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$352, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$147


$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_name("AIODAT_BITS")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x02)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$353, DW_AT_name("rsvd1")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$353, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$354, DW_AT_name("rsvd2")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$354, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$355, DW_AT_name("AIO2")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_AIO2")
	.dwattr $C$DW$355, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$356, DW_AT_name("rsvd3")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$356, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$357, DW_AT_name("AIO4")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_AIO4")
	.dwattr $C$DW$357, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$358, DW_AT_name("rsvd4")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$358, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$359, DW_AT_name("AIO6")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_AIO6")
	.dwattr $C$DW$359, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$360, DW_AT_name("rsvd5")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$360, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$361, DW_AT_name("rsvd6")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$361, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$362, DW_AT_name("rsvd7")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$362, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$363, DW_AT_name("AIO10")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_AIO10")
	.dwattr $C$DW$363, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$364, DW_AT_name("rsvd8")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$364, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$365, DW_AT_name("AIO12")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_AIO12")
	.dwattr $C$DW$365, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$366, DW_AT_name("rsvd9")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$366, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$367, DW_AT_name("AIO14")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_AIO14")
	.dwattr $C$DW$367, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$368, DW_AT_name("rsvd10")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$368, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$369, DW_AT_name("rsvd11")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_rsvd11")
	.dwattr $C$DW$369, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24


$C$DW$T$26	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$26, DW_AT_name("AIODAT_REG")
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x02)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$370, DW_AT_name("all")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$371, DW_AT_name("bit")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_name("AIO_BITS")
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x02)
$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$372, DW_AT_name("rsvd1")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$372, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$373, DW_AT_name("rsvd2")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$373, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$374, DW_AT_name("AIO2")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_AIO2")
	.dwattr $C$DW$374, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$375, DW_AT_name("rsvd3")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$375, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$376	.dwtag  DW_TAG_member
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$376, DW_AT_name("AIO4")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_AIO4")
	.dwattr $C$DW$376, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$376, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$377, DW_AT_name("rsvd4")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$377, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$377, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$378, DW_AT_name("AIO6")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_AIO6")
	.dwattr $C$DW$378, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$379, DW_AT_name("rsvd5")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$379, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$380, DW_AT_name("rsvd6")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$380, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$381, DW_AT_name("rsvd7")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$381, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$382, DW_AT_name("AIO10")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_AIO10")
	.dwattr $C$DW$382, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$383, DW_AT_name("rsvd8")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$383, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$384, DW_AT_name("AIO12")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_AIO12")
	.dwattr $C$DW$384, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$385	.dwtag  DW_TAG_member
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$385, DW_AT_name("rsvd9")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$385, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$385, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$385, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$386	.dwtag  DW_TAG_member
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$386, DW_AT_name("AIO14")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_AIO14")
	.dwattr $C$DW$386, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$386, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$386, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$387	.dwtag  DW_TAG_member
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$387, DW_AT_name("rsvd10")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$387, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$387, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$387, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27


$C$DW$T$28	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$28, DW_AT_name("AIO_REG")
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x02)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$388, DW_AT_name("all")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$388, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$389, DW_AT_name("bit")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28


$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_name("CLKCTL_BITS")
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x01)
$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$390, DW_AT_name("OSCCLKSRCSEL")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_OSCCLKSRCSEL")
	.dwattr $C$DW$390, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$391, DW_AT_name("OSCCLKSRC2SEL")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_OSCCLKSRC2SEL")
	.dwattr $C$DW$391, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$392, DW_AT_name("WDCLKSRCSEL")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_WDCLKSRCSEL")
	.dwattr $C$DW$392, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$393, DW_AT_name("TMR2CLKSRCSEL")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_TMR2CLKSRCSEL")
	.dwattr $C$DW$393, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x02)
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$394, DW_AT_name("TMR2CLKPRESCALE")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_TMR2CLKPRESCALE")
	.dwattr $C$DW$394, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x03)
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$395, DW_AT_name("INTOSC1OFF")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_INTOSC1OFF")
	.dwattr $C$DW$395, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$396, DW_AT_name("INTOSC1HALTI")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_INTOSC1HALTI")
	.dwattr $C$DW$396, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$397, DW_AT_name("INTOSC2OFF")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_INTOSC2OFF")
	.dwattr $C$DW$397, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$398, DW_AT_name("INTOSC2HALTI")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_INTOSC2HALTI")
	.dwattr $C$DW$398, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$399, DW_AT_name("WDHALTI")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_WDHALTI")
	.dwattr $C$DW$399, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$400, DW_AT_name("XCLKINOFF")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_XCLKINOFF")
	.dwattr $C$DW$400, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$401, DW_AT_name("XTALOSCOFF")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_XTALOSCOFF")
	.dwattr $C$DW$401, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$402, DW_AT_name("NMIRESETSEL")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_NMIRESETSEL")
	.dwattr $C$DW$402, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$30, DW_AT_name("CLKCTL_REG")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x01)
$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$403, DW_AT_name("all")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$404, DW_AT_name("bit")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("EPWMCFG_BITS")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$405, DW_AT_name("CONFIG")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_CONFIG")
	.dwattr $C$DW$405, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$406, DW_AT_name("rsvd1")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$406, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$32, DW_AT_name("EPWMCFG_REG")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x01)
$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$407, DW_AT_name("all")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$408, DW_AT_name("bit")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("GPA1_BITS")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x02)
$C$DW$409	.dwtag  DW_TAG_member
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$409, DW_AT_name("GPIO0")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_GPIO0")
	.dwattr $C$DW$409, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$409, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$410, DW_AT_name("GPIO1")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_GPIO1")
	.dwattr $C$DW$410, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$411, DW_AT_name("GPIO2")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_GPIO2")
	.dwattr $C$DW$411, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$412, DW_AT_name("GPIO3")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_GPIO3")
	.dwattr $C$DW$412, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$413, DW_AT_name("GPIO4")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_GPIO4")
	.dwattr $C$DW$413, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$414, DW_AT_name("GPIO5")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_GPIO5")
	.dwattr $C$DW$414, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$415, DW_AT_name("GPIO6")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_GPIO6")
	.dwattr $C$DW$415, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$416, DW_AT_name("GPIO7")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_GPIO7")
	.dwattr $C$DW$416, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$417, DW_AT_name("GPIO8")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_GPIO8")
	.dwattr $C$DW$417, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$418, DW_AT_name("GPIO9")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_GPIO9")
	.dwattr $C$DW$418, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$419, DW_AT_name("GPIO10")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_GPIO10")
	.dwattr $C$DW$419, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$420	.dwtag  DW_TAG_member
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$420, DW_AT_name("GPIO11")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_GPIO11")
	.dwattr $C$DW$420, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$421, DW_AT_name("GPIO12")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_GPIO12")
	.dwattr $C$DW$421, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$422, DW_AT_name("GPIO13")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_GPIO13")
	.dwattr $C$DW$422, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$423, DW_AT_name("GPIO14")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_GPIO14")
	.dwattr $C$DW$423, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$423, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$424, DW_AT_name("GPIO15")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_GPIO15")
	.dwattr $C$DW$424, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$34, DW_AT_name("GPA1_REG")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x02)
$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$425, DW_AT_name("all")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$426, DW_AT_name("bit")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$35, DW_AT_name("GPA2_BITS")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x02)
$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$427, DW_AT_name("GPIO16")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_GPIO16")
	.dwattr $C$DW$427, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$428, DW_AT_name("GPIO17")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_GPIO17")
	.dwattr $C$DW$428, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$429, DW_AT_name("GPIO18")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_GPIO18")
	.dwattr $C$DW$429, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$430, DW_AT_name("GPIO19")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_GPIO19")
	.dwattr $C$DW$430, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$431	.dwtag  DW_TAG_member
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$431, DW_AT_name("GPIO20")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_GPIO20")
	.dwattr $C$DW$431, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$432, DW_AT_name("GPIO21")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_GPIO21")
	.dwattr $C$DW$432, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$433, DW_AT_name("GPIO22")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_GPIO22")
	.dwattr $C$DW$433, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$434, DW_AT_name("GPIO23")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_GPIO23")
	.dwattr $C$DW$434, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$435	.dwtag  DW_TAG_member
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$435, DW_AT_name("GPIO24")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_GPIO24")
	.dwattr $C$DW$435, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$436, DW_AT_name("GPIO25")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_GPIO25")
	.dwattr $C$DW$436, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$437, DW_AT_name("GPIO26")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_GPIO26")
	.dwattr $C$DW$437, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$438, DW_AT_name("GPIO27")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_GPIO27")
	.dwattr $C$DW$438, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$439, DW_AT_name("GPIO28")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_GPIO28")
	.dwattr $C$DW$439, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$440, DW_AT_name("GPIO29")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_GPIO29")
	.dwattr $C$DW$440, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$441, DW_AT_name("GPIO30")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_GPIO30")
	.dwattr $C$DW$441, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$442, DW_AT_name("GPIO31")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_GPIO31")
	.dwattr $C$DW$442, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35


$C$DW$T$36	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$36, DW_AT_name("GPA2_REG")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$443, DW_AT_name("all")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$444, DW_AT_name("bit")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$36


$C$DW$T$37	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$37, DW_AT_name("GPACTRL2_BITS")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x01)
$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$445, DW_AT_name("USB0IOEN")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_USB0IOEN")
	.dwattr $C$DW$445, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$446, DW_AT_name("rsvd1")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$446, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$37


$C$DW$T$38	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$38, DW_AT_name("GPACTRL2_REG")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x01)
$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$447, DW_AT_name("all")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$448, DW_AT_name("bit")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38


$C$DW$T$39	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$39, DW_AT_name("GPACTRL_BITS")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x02)
$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$449, DW_AT_name("QUALPRD0")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_QUALPRD0")
	.dwattr $C$DW$449, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$450, DW_AT_name("QUALPRD1")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_QUALPRD1")
	.dwattr $C$DW$450, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$451, DW_AT_name("QUALPRD2")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_QUALPRD2")
	.dwattr $C$DW$451, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$452, DW_AT_name("QUALPRD3")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_QUALPRD3")
	.dwattr $C$DW$452, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$39


$C$DW$T$40	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$40, DW_AT_name("GPACTRL_REG")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x02)
$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$453, DW_AT_name("all")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$454, DW_AT_name("bit")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40


$C$DW$T$41	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$41, DW_AT_name("GPADAT_BITS")
	.dwattr $C$DW$T$41, DW_AT_byte_size(0x02)
$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$455, DW_AT_name("GPIO0")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_GPIO0")
	.dwattr $C$DW$455, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$456, DW_AT_name("GPIO1")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_GPIO1")
	.dwattr $C$DW$456, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$457, DW_AT_name("GPIO2")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_GPIO2")
	.dwattr $C$DW$457, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$458, DW_AT_name("GPIO3")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_GPIO3")
	.dwattr $C$DW$458, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$459, DW_AT_name("GPIO4")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_GPIO4")
	.dwattr $C$DW$459, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$460, DW_AT_name("GPIO5")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_GPIO5")
	.dwattr $C$DW$460, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$461, DW_AT_name("GPIO6")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_GPIO6")
	.dwattr $C$DW$461, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$462, DW_AT_name("GPIO7")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_GPIO7")
	.dwattr $C$DW$462, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$463, DW_AT_name("GPIO8")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_GPIO8")
	.dwattr $C$DW$463, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$464, DW_AT_name("GPIO9")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_GPIO9")
	.dwattr $C$DW$464, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$465, DW_AT_name("GPIO10")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_GPIO10")
	.dwattr $C$DW$465, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$466, DW_AT_name("GPIO11")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_GPIO11")
	.dwattr $C$DW$466, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$467, DW_AT_name("GPIO12")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_GPIO12")
	.dwattr $C$DW$467, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$468, DW_AT_name("GPIO13")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_GPIO13")
	.dwattr $C$DW$468, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$469, DW_AT_name("GPIO14")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_GPIO14")
	.dwattr $C$DW$469, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$470, DW_AT_name("GPIO15")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_GPIO15")
	.dwattr $C$DW$470, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$471, DW_AT_name("GPIO16")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_GPIO16")
	.dwattr $C$DW$471, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$472, DW_AT_name("GPIO17")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_GPIO17")
	.dwattr $C$DW$472, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$473, DW_AT_name("GPIO18")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_GPIO18")
	.dwattr $C$DW$473, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$474, DW_AT_name("GPIO19")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_GPIO19")
	.dwattr $C$DW$474, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$475, DW_AT_name("GPIO20")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_GPIO20")
	.dwattr $C$DW$475, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$476, DW_AT_name("GPIO21")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_GPIO21")
	.dwattr $C$DW$476, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$477, DW_AT_name("GPIO22")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_GPIO22")
	.dwattr $C$DW$477, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$478	.dwtag  DW_TAG_member
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$478, DW_AT_name("GPIO23")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_GPIO23")
	.dwattr $C$DW$478, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$478, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$479, DW_AT_name("GPIO24")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_GPIO24")
	.dwattr $C$DW$479, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$480, DW_AT_name("GPIO25")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_GPIO25")
	.dwattr $C$DW$480, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$481, DW_AT_name("GPIO26")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_GPIO26")
	.dwattr $C$DW$481, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$482, DW_AT_name("GPIO27")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_GPIO27")
	.dwattr $C$DW$482, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$483, DW_AT_name("GPIO28")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_GPIO28")
	.dwattr $C$DW$483, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$484, DW_AT_name("GPIO29")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_GPIO29")
	.dwattr $C$DW$484, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$485, DW_AT_name("GPIO30")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_GPIO30")
	.dwattr $C$DW$485, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$486, DW_AT_name("GPIO31")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_GPIO31")
	.dwattr $C$DW$486, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$41


$C$DW$T$42	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$42, DW_AT_name("GPADAT_REG")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x02)
$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$487, DW_AT_name("all")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$488	.dwtag  DW_TAG_member
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$488, DW_AT_name("bit")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42


$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("GPB1_BITS")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x02)
$C$DW$489	.dwtag  DW_TAG_member
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$489, DW_AT_name("GPIO32")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_GPIO32")
	.dwattr $C$DW$489, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$489, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$490, DW_AT_name("GPIO33")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_GPIO33")
	.dwattr $C$DW$490, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$491	.dwtag  DW_TAG_member
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$491, DW_AT_name("GPIO34")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_GPIO34")
	.dwattr $C$DW$491, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$491, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$492	.dwtag  DW_TAG_member
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$492, DW_AT_name("GPIO35")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_GPIO35")
	.dwattr $C$DW$492, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$492, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$493	.dwtag  DW_TAG_member
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$493, DW_AT_name("GPIO36")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_GPIO36")
	.dwattr $C$DW$493, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$493, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$494	.dwtag  DW_TAG_member
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$494, DW_AT_name("GPIO37")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_GPIO37")
	.dwattr $C$DW$494, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$494, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$494, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$495, DW_AT_name("GPIO38")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_GPIO38")
	.dwattr $C$DW$495, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$496, DW_AT_name("GPIO39")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_GPIO39")
	.dwattr $C$DW$496, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$497, DW_AT_name("GPIO40")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_GPIO40")
	.dwattr $C$DW$497, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$498, DW_AT_name("GPIO41")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_GPIO41")
	.dwattr $C$DW$498, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$499, DW_AT_name("GPIO42")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_GPIO42")
	.dwattr $C$DW$499, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$500, DW_AT_name("GPIO43")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_GPIO43")
	.dwattr $C$DW$500, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$501, DW_AT_name("GPIO44")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_GPIO44")
	.dwattr $C$DW$501, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$502, DW_AT_name("rsvd1")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$502, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x06)
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$43


$C$DW$T$44	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$44, DW_AT_name("GPB1_REG")
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x02)
$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$503, DW_AT_name("all")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$504, DW_AT_name("bit")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$44


$C$DW$T$45	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$45, DW_AT_name("GPB2_BITS")
	.dwattr $C$DW$T$45, DW_AT_byte_size(0x02)
$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$505, DW_AT_name("rsvd1")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$505, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x04)
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$506, DW_AT_name("GPIO50")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_GPIO50")
	.dwattr $C$DW$506, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$507, DW_AT_name("GPIO51")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_GPIO51")
	.dwattr $C$DW$507, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$508, DW_AT_name("GPIO52")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_GPIO52")
	.dwattr $C$DW$508, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$509, DW_AT_name("GPIO53")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_GPIO53")
	.dwattr $C$DW$509, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x02)
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$510, DW_AT_name("GPIO54")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_GPIO54")
	.dwattr $C$DW$510, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$511, DW_AT_name("GPIO55")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_GPIO55")
	.dwattr $C$DW$511, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$512, DW_AT_name("GPIO56")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_GPIO56")
	.dwattr $C$DW$512, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$513, DW_AT_name("GPIO57")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_GPIO57")
	.dwattr $C$DW$513, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x02)
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$514, DW_AT_name("GPIO58")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_GPIO58")
	.dwattr $C$DW$514, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x02)
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$515, DW_AT_name("rsvd2")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$515, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0a)
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$45


$C$DW$T$46	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$46, DW_AT_name("GPB2_REG")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x02)
$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$516, DW_AT_name("all")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$517, DW_AT_name("bit")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$46


$C$DW$T$47	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$47, DW_AT_name("GPBCTRL_BITS")
	.dwattr $C$DW$T$47, DW_AT_byte_size(0x02)
$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$518, DW_AT_name("QUALPRD0")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_QUALPRD0")
	.dwattr $C$DW$518, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$519, DW_AT_name("QUALPRD1")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_QUALPRD1")
	.dwattr $C$DW$519, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$520, DW_AT_name("QUALPRD2")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_QUALPRD2")
	.dwattr $C$DW$520, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$521, DW_AT_name("QUALPRD3")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_QUALPRD3")
	.dwattr $C$DW$521, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$47


$C$DW$T$48	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$48, DW_AT_name("GPBCTRL_REG")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x02)
$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$522, DW_AT_name("all")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$523, DW_AT_name("bit")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$48


$C$DW$T$49	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$49, DW_AT_name("GPBDAT_BITS")
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x02)
$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$524, DW_AT_name("GPIO32")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_GPIO32")
	.dwattr $C$DW$524, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$525, DW_AT_name("GPIO33")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_GPIO33")
	.dwattr $C$DW$525, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$526, DW_AT_name("GPIO34")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_GPIO34")
	.dwattr $C$DW$526, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$527, DW_AT_name("GPIO35")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_GPIO35")
	.dwattr $C$DW$527, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$528, DW_AT_name("GPIO36")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_GPIO36")
	.dwattr $C$DW$528, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$529, DW_AT_name("GPIO37")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_GPIO37")
	.dwattr $C$DW$529, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$530, DW_AT_name("GPIO38")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_GPIO38")
	.dwattr $C$DW$530, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$531, DW_AT_name("GPIO39")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_GPIO39")
	.dwattr $C$DW$531, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$532, DW_AT_name("GPIO40")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_GPIO40")
	.dwattr $C$DW$532, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$533, DW_AT_name("GPIO41")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_GPIO41")
	.dwattr $C$DW$533, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$534, DW_AT_name("GPIO42")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_GPIO42")
	.dwattr $C$DW$534, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$535, DW_AT_name("GPIO43")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_GPIO43")
	.dwattr $C$DW$535, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$536, DW_AT_name("GPIO44")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_GPIO44")
	.dwattr $C$DW$536, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$537, DW_AT_name("rsvd1")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$537, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x03)
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$538, DW_AT_name("rsvd2")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$538, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$539, DW_AT_name("GPIO50")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_GPIO50")
	.dwattr $C$DW$539, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$540, DW_AT_name("GPIO51")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_GPIO51")
	.dwattr $C$DW$540, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$541, DW_AT_name("GPIO52")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_GPIO52")
	.dwattr $C$DW$541, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$542, DW_AT_name("GPIO53")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_GPIO53")
	.dwattr $C$DW$542, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$543, DW_AT_name("GPIO54")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_GPIO54")
	.dwattr $C$DW$543, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$544, DW_AT_name("GPIO55")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_GPIO55")
	.dwattr $C$DW$544, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$545, DW_AT_name("GPIO56")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_GPIO56")
	.dwattr $C$DW$545, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$546, DW_AT_name("GPIO57")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_GPIO57")
	.dwattr $C$DW$546, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$547, DW_AT_name("GPIO58")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_GPIO58")
	.dwattr $C$DW$547, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$548, DW_AT_name("rsvd3")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$548, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$49


$C$DW$T$50	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$50, DW_AT_name("GPBDAT_REG")
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x02)
$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$549, DW_AT_name("all")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$550, DW_AT_name("bit")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$50


$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("GPIO_CTRL_REGS")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x40)
$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$551, DW_AT_name("GPACTRL")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_GPACTRL")
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$552, DW_AT_name("GPAQSEL1")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_GPAQSEL1")
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$553, DW_AT_name("GPAQSEL2")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_GPAQSEL2")
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$554, DW_AT_name("GPAMUX1")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_GPAMUX1")
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$555, DW_AT_name("GPAMUX2")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_GPAMUX2")
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$556, DW_AT_name("GPADIR")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_GPADIR")
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$557, DW_AT_name("GPAPUD")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_GPAPUD")
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$558, DW_AT_name("GPACTRL2")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_GPACTRL2")
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$559, DW_AT_name("rsvd1")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$560, DW_AT_name("GPBCTRL")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_GPBCTRL")
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$561, DW_AT_name("GPBQSEL1")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_GPBQSEL1")
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$562, DW_AT_name("GPBQSEL2")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_GPBQSEL2")
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$563, DW_AT_name("GPBMUX1")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_GPBMUX1")
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$564, DW_AT_name("GPBMUX2")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_GPBMUX2")
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$565, DW_AT_name("GPBDIR")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_GPBDIR")
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$566, DW_AT_name("GPBPUD")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_GPBPUD")
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$567, DW_AT_name("rsvd2")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$568, DW_AT_name("AIOMUX1")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_AIOMUX1")
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$569, DW_AT_name("rsvd3")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$570, DW_AT_name("AIODIR")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_AIODIR")
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$571, DW_AT_name("rsvd4")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$54

$C$DW$572	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$54)
$C$DW$T$153	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$572)

$C$DW$T$56	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$56, DW_AT_name("GPIO_DATA_REGS")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x20)
$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$573, DW_AT_name("GPADAT")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_GPADAT")
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$574, DW_AT_name("GPASET")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_GPASET")
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$575, DW_AT_name("GPACLEAR")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_GPACLEAR")
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$576, DW_AT_name("GPATOGGLE")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_GPATOGGLE")
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$577, DW_AT_name("GPBDAT")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_GPBDAT")
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$578, DW_AT_name("GPBSET")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_GPBSET")
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$579, DW_AT_name("GPBCLEAR")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_GPBCLEAR")
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$580, DW_AT_name("GPBTOGGLE")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_GPBTOGGLE")
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$581, DW_AT_name("rsvd1")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$582, DW_AT_name("AIODAT")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_AIODAT")
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$583, DW_AT_name("AIOSET")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_AIOSET")
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$584, DW_AT_name("AIOCLEAR")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_AIOCLEAR")
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$585, DW_AT_name("AIOTOGGLE")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_AIOTOGGLE")
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56

$C$DW$586	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$56)
$C$DW$T$154	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$586)

$C$DW$T$57	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$57, DW_AT_name("INTOSC1TRIM_BITS")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x01)
$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$587, DW_AT_name("COARSETRIM")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_COARSETRIM")
	.dwattr $C$DW$587, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$588, DW_AT_name("rsvd1")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$588, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$589, DW_AT_name("FINETRIM")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_FINETRIM")
	.dwattr $C$DW$589, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x06)
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$590, DW_AT_name("rsvd2")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$590, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$57


$C$DW$T$58	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$58, DW_AT_name("INTOSC1TRIM_REG")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x01)
$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$591, DW_AT_name("all")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$592, DW_AT_name("bit")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$58


$C$DW$T$59	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$59, DW_AT_name("INTOSC2TRIM_BITS")
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x01)
$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$593, DW_AT_name("COARSETRIM")
	.dwattr $C$DW$593, DW_AT_TI_symbol_name("_COARSETRIM")
	.dwattr $C$DW$593, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$594, DW_AT_name("rsvd1")
	.dwattr $C$DW$594, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$594, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$595, DW_AT_name("FINETRIM")
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_FINETRIM")
	.dwattr $C$DW$595, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x06)
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$596, DW_AT_name("rsvd2")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$596, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$59


$C$DW$T$60	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$60, DW_AT_name("INTOSC2TRIM_REG")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x01)
$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$597, DW_AT_name("all")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$598, DW_AT_name("bit")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$60


$C$DW$T$61	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$61, DW_AT_name("JTAGDEBUG_BITS")
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x01)
$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$599, DW_AT_name("JTAGDIS")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_JTAGDIS")
	.dwattr $C$DW$599, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$600, DW_AT_name("rsvd1")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$600, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$61


$C$DW$T$62	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$62, DW_AT_name("JTAGDEBUG_REG")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)
$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$601, DW_AT_name("all")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$602, DW_AT_name("bit")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$62


$C$DW$T$70	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$70, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$70, DW_AT_byte_size(0x08)
$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$603, DW_AT_name("wListElem")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$604, DW_AT_name("wCount")
	.dwattr $C$DW$604, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$605, DW_AT_name("fxn")
	.dwattr $C$DW$605, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$70

$C$DW$T$134	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$134, DW_AT_language(DW_LANG_C)
$C$DW$T$65	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$65, DW_AT_address_class(0x16)
$C$DW$T$66	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_language(DW_LANG_C)

$C$DW$T$71	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$71, DW_AT_name("LOSPCP_BITS")
	.dwattr $C$DW$T$71, DW_AT_byte_size(0x01)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$606, DW_AT_name("LSPCLK")
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_LSPCLK")
	.dwattr $C$DW$606, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$607, DW_AT_name("rsvd1")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$607, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0d)
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$71


$C$DW$T$72	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$72, DW_AT_name("LOSPCP_REG")
	.dwattr $C$DW$T$72, DW_AT_byte_size(0x01)
$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$608, DW_AT_name("all")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$609, DW_AT_name("bit")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$72


$C$DW$T$73	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$73, DW_AT_name("LPMCR0_BITS")
	.dwattr $C$DW$T$73, DW_AT_byte_size(0x01)
$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$610, DW_AT_name("LPM")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_LPM")
	.dwattr $C$DW$610, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$611, DW_AT_name("QUALSTDBY")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_QUALSTDBY")
	.dwattr $C$DW$611, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x06)
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$612, DW_AT_name("rsvd1")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$612, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x07)
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$613, DW_AT_name("WDINTE")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_WDINTE")
	.dwattr $C$DW$613, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$73


$C$DW$T$74	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$74, DW_AT_name("LPMCR0_REG")
	.dwattr $C$DW$T$74, DW_AT_byte_size(0x01)
$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$614, DW_AT_name("all")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$615, DW_AT_name("bit")
	.dwattr $C$DW$615, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$74


$C$DW$T$81	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$81, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$81, DW_AT_byte_size(0x30)
$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$616, DW_AT_name("dataQue")
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$617, DW_AT_name("freeQue")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$618, DW_AT_name("dataSem")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$619, DW_AT_name("freeSem")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$620, DW_AT_name("segid")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$621, DW_AT_name("size")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$622, DW_AT_name("length")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$623, DW_AT_name("name")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$81

$C$DW$T$156	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$156, DW_AT_language(DW_LANG_C)
$C$DW$T$158	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$158, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$158, DW_AT_address_class(0x16)
$C$DW$T$159	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$159, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$T$159, DW_AT_language(DW_LANG_C)

$C$DW$T$82	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$82, DW_AT_name("PCLKCR0_BITS")
	.dwattr $C$DW$T$82, DW_AT_byte_size(0x01)
$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$624, DW_AT_name("HRPWMENCLK")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_HRPWMENCLK")
	.dwattr $C$DW$624, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$625, DW_AT_name("rsvd1")
	.dwattr $C$DW$625, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$625, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$626, DW_AT_name("TBCLKSYNC")
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_TBCLKSYNC")
	.dwattr $C$DW$626, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$627	.dwtag  DW_TAG_member
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$627, DW_AT_name("ADCENCLK")
	.dwattr $C$DW$627, DW_AT_TI_symbol_name("_ADCENCLK")
	.dwattr $C$DW$627, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$628, DW_AT_name("I2CAENCLK")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_I2CAENCLK")
	.dwattr $C$DW$628, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$629, DW_AT_name("rsvd2")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$629, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$630, DW_AT_name("rsvd3")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$630, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x02)
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$631, DW_AT_name("SPIAENCLK")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_SPIAENCLK")
	.dwattr $C$DW$631, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$632, DW_AT_name("SPIBENCLK")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_SPIBENCLK")
	.dwattr $C$DW$632, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$633, DW_AT_name("SCIAENCLK")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_SCIAENCLK")
	.dwattr $C$DW$633, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$634, DW_AT_name("SCIBENCLK")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_SCIBENCLK")
	.dwattr $C$DW$634, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$635, DW_AT_name("MCBSPAENCLK")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_MCBSPAENCLK")
	.dwattr $C$DW$635, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$636, DW_AT_name("rsvd4")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$636, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$637	.dwtag  DW_TAG_member
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$637, DW_AT_name("ECANAENCLK")
	.dwattr $C$DW$637, DW_AT_TI_symbol_name("_ECANAENCLK")
	.dwattr $C$DW$637, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$637, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$638, DW_AT_name("rsvd5")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$638, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$82


$C$DW$T$83	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$83, DW_AT_name("PCLKCR0_REG")
	.dwattr $C$DW$T$83, DW_AT_byte_size(0x01)
$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$639, DW_AT_name("all")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$640, DW_AT_name("bit")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$83


$C$DW$T$84	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$84, DW_AT_name("PCLKCR1_BITS")
	.dwattr $C$DW$T$84, DW_AT_byte_size(0x01)
$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$641, DW_AT_name("EPWM1ENCLK")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_EPWM1ENCLK")
	.dwattr $C$DW$641, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$642, DW_AT_name("EPWM2ENCLK")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_EPWM2ENCLK")
	.dwattr $C$DW$642, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$643, DW_AT_name("EPWM3ENCLK")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_EPWM3ENCLK")
	.dwattr $C$DW$643, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$644, DW_AT_name("EPWM4ENCLK")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_EPWM4ENCLK")
	.dwattr $C$DW$644, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$645, DW_AT_name("EPWM5ENCLK")
	.dwattr $C$DW$645, DW_AT_TI_symbol_name("_EPWM5ENCLK")
	.dwattr $C$DW$645, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$646, DW_AT_name("EPWM6ENCLK")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_EPWM6ENCLK")
	.dwattr $C$DW$646, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$647, DW_AT_name("EPWM7ENCLK")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_EPWM7ENCLK")
	.dwattr $C$DW$647, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$648, DW_AT_name("EPWM8ENCLK")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_EPWM8ENCLK")
	.dwattr $C$DW$648, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$649, DW_AT_name("ECAP1ENCLK")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_ECAP1ENCLK")
	.dwattr $C$DW$649, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$650, DW_AT_name("ECAP2ENCLK")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_ECAP2ENCLK")
	.dwattr $C$DW$650, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$651, DW_AT_name("ECAP3ENCLK")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_ECAP3ENCLK")
	.dwattr $C$DW$651, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$652	.dwtag  DW_TAG_member
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$652, DW_AT_name("rsvd1")
	.dwattr $C$DW$652, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$652, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x03)
	.dwattr $C$DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$652, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$653, DW_AT_name("EQEP1ENCLK")
	.dwattr $C$DW$653, DW_AT_TI_symbol_name("_EQEP1ENCLK")
	.dwattr $C$DW$653, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$654, DW_AT_name("EQEP2ENCLK")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_EQEP2ENCLK")
	.dwattr $C$DW$654, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$84


$C$DW$T$85	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$85, DW_AT_name("PCLKCR1_REG")
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x01)
$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$655, DW_AT_name("all")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$656, DW_AT_name("bit")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$85


$C$DW$T$86	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$86, DW_AT_name("PCLKCR2_BITS")
	.dwattr $C$DW$T$86, DW_AT_byte_size(0x01)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$657, DW_AT_name("rsvd1")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$657, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$658, DW_AT_name("HRCAP1ENCLK")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_HRCAP1ENCLK")
	.dwattr $C$DW$658, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$659, DW_AT_name("HRCAP2ENCLK")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_HRCAP2ENCLK")
	.dwattr $C$DW$659, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$660, DW_AT_name("HRCAP3ENCLK")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_HRCAP3ENCLK")
	.dwattr $C$DW$660, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$661, DW_AT_name("HRCAP4ENCLK")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_HRCAP4ENCLK")
	.dwattr $C$DW$661, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$662, DW_AT_name("rsvd2")
	.dwattr $C$DW$662, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$662, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$86


$C$DW$T$87	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$87, DW_AT_name("PCLKCR2_REG")
	.dwattr $C$DW$T$87, DW_AT_byte_size(0x01)
$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$663, DW_AT_name("all")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$664, DW_AT_name("bit")
	.dwattr $C$DW$664, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$87


$C$DW$T$88	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$88, DW_AT_name("PCLKCR3_BITS")
	.dwattr $C$DW$T$88, DW_AT_byte_size(0x01)
$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$665, DW_AT_name("COMP1ENCLK")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_COMP1ENCLK")
	.dwattr $C$DW$665, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$666, DW_AT_name("COMP2ENCLK")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_COMP2ENCLK")
	.dwattr $C$DW$666, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$667, DW_AT_name("COMP3ENCLK")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_COMP3ENCLK")
	.dwattr $C$DW$667, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$668, DW_AT_name("rsvd1")
	.dwattr $C$DW$668, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$668, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x05)
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$669, DW_AT_name("CPUTIMER0ENCLK")
	.dwattr $C$DW$669, DW_AT_TI_symbol_name("_CPUTIMER0ENCLK")
	.dwattr $C$DW$669, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$670, DW_AT_name("CPUTIMER1ENCLK")
	.dwattr $C$DW$670, DW_AT_TI_symbol_name("_CPUTIMER1ENCLK")
	.dwattr $C$DW$670, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$671, DW_AT_name("CPUTIMER2ENCLK")
	.dwattr $C$DW$671, DW_AT_TI_symbol_name("_CPUTIMER2ENCLK")
	.dwattr $C$DW$671, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$672, DW_AT_name("DMAENCLK")
	.dwattr $C$DW$672, DW_AT_TI_symbol_name("_DMAENCLK")
	.dwattr $C$DW$672, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$673, DW_AT_name("rsvd2")
	.dwattr $C$DW$673, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$673, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$674, DW_AT_name("rsvd3")
	.dwattr $C$DW$674, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$674, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$675, DW_AT_name("CLA1ENCLK")
	.dwattr $C$DW$675, DW_AT_TI_symbol_name("_CLA1ENCLK")
	.dwattr $C$DW$675, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$676	.dwtag  DW_TAG_member
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$676, DW_AT_name("USB0ENCLK")
	.dwattr $C$DW$676, DW_AT_TI_symbol_name("_USB0ENCLK")
	.dwattr $C$DW$676, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$676, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$88


$C$DW$T$89	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$89, DW_AT_name("PCLKCR3_REG")
	.dwattr $C$DW$T$89, DW_AT_byte_size(0x01)
$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$677, DW_AT_name("all")
	.dwattr $C$DW$677, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$678	.dwtag  DW_TAG_member
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$678, DW_AT_name("bit")
	.dwattr $C$DW$678, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$89


$C$DW$T$90	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$90, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$90, DW_AT_byte_size(0x01)
$C$DW$679	.dwtag  DW_TAG_member
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$679, DW_AT_name("ACK1")
	.dwattr $C$DW$679, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$679, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$679, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$680	.dwtag  DW_TAG_member
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$680, DW_AT_name("ACK2")
	.dwattr $C$DW$680, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$680, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$681	.dwtag  DW_TAG_member
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$681, DW_AT_name("ACK3")
	.dwattr $C$DW$681, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$681, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$681, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$681, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$682	.dwtag  DW_TAG_member
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$682, DW_AT_name("ACK4")
	.dwattr $C$DW$682, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$682, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$682, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$683	.dwtag  DW_TAG_member
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$683, DW_AT_name("ACK5")
	.dwattr $C$DW$683, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$683, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$683, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$684	.dwtag  DW_TAG_member
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$684, DW_AT_name("ACK6")
	.dwattr $C$DW$684, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$684, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$684, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$684, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$685, DW_AT_name("ACK7")
	.dwattr $C$DW$685, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$685, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$686	.dwtag  DW_TAG_member
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$686, DW_AT_name("ACK8")
	.dwattr $C$DW$686, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$686, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$686, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$687	.dwtag  DW_TAG_member
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$687, DW_AT_name("ACK9")
	.dwattr $C$DW$687, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$687, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$688	.dwtag  DW_TAG_member
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$688, DW_AT_name("ACK10")
	.dwattr $C$DW$688, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$688, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$688, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$689	.dwtag  DW_TAG_member
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$689, DW_AT_name("ACK11")
	.dwattr $C$DW$689, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$689, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$690	.dwtag  DW_TAG_member
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$690, DW_AT_name("ACK12")
	.dwattr $C$DW$690, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$690, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$690, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$691	.dwtag  DW_TAG_member
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$691, DW_AT_name("rsvd1")
	.dwattr $C$DW$691, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$691, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$691, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$90


$C$DW$T$91	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$91, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$91, DW_AT_byte_size(0x01)
$C$DW$692	.dwtag  DW_TAG_member
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$692, DW_AT_name("all")
	.dwattr $C$DW$692, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$692, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$692, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$693	.dwtag  DW_TAG_member
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$693, DW_AT_name("bit")
	.dwattr $C$DW$693, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$693, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$693, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$91


$C$DW$T$92	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$92, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$92, DW_AT_byte_size(0x01)
$C$DW$694	.dwtag  DW_TAG_member
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$694, DW_AT_name("ENPIE")
	.dwattr $C$DW$694, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$694, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$694, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$694, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$695, DW_AT_name("PIEVECT")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$695, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$92


$C$DW$T$93	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$93, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$93, DW_AT_byte_size(0x01)
$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$696, DW_AT_name("all")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$697, DW_AT_name("bit")
	.dwattr $C$DW$697, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$93


$C$DW$T$94	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$94, DW_AT_name("PIEIER_BITS")
	.dwattr $C$DW$T$94, DW_AT_byte_size(0x01)
$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$698, DW_AT_name("INTx1")
	.dwattr $C$DW$698, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$698, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$699, DW_AT_name("INTx2")
	.dwattr $C$DW$699, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$699, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$700	.dwtag  DW_TAG_member
	.dwattr $C$DW$700, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$700, DW_AT_name("INTx3")
	.dwattr $C$DW$700, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$700, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$700, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$700, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$701, DW_AT_name("INTx4")
	.dwattr $C$DW$701, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$701, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$702, DW_AT_name("INTx5")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$702, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$703, DW_AT_name("INTx6")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$703, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$704, DW_AT_name("INTx7")
	.dwattr $C$DW$704, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$704, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$705, DW_AT_name("INTx8")
	.dwattr $C$DW$705, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$705, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$706	.dwtag  DW_TAG_member
	.dwattr $C$DW$706, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$706, DW_AT_name("rsvd1")
	.dwattr $C$DW$706, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$706, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$706, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$94


$C$DW$T$95	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$95, DW_AT_name("PIEIER_REG")
	.dwattr $C$DW$T$95, DW_AT_byte_size(0x01)
$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$707, DW_AT_name("all")
	.dwattr $C$DW$707, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$708	.dwtag  DW_TAG_member
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$708, DW_AT_name("bit")
	.dwattr $C$DW$708, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$708, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$95


$C$DW$T$96	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$96, DW_AT_name("PIEIFR_BITS")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x01)
$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$709, DW_AT_name("INTx1")
	.dwattr $C$DW$709, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$709, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$710	.dwtag  DW_TAG_member
	.dwattr $C$DW$710, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$710, DW_AT_name("INTx2")
	.dwattr $C$DW$710, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$710, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$710, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$711	.dwtag  DW_TAG_member
	.dwattr $C$DW$711, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$711, DW_AT_name("INTx3")
	.dwattr $C$DW$711, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$711, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$711, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$712	.dwtag  DW_TAG_member
	.dwattr $C$DW$712, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$712, DW_AT_name("INTx4")
	.dwattr $C$DW$712, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$712, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$712, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$713	.dwtag  DW_TAG_member
	.dwattr $C$DW$713, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$713, DW_AT_name("INTx5")
	.dwattr $C$DW$713, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$713, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$713, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$714	.dwtag  DW_TAG_member
	.dwattr $C$DW$714, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$714, DW_AT_name("INTx6")
	.dwattr $C$DW$714, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$714, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$714, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$715, DW_AT_name("INTx7")
	.dwattr $C$DW$715, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$715, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$716	.dwtag  DW_TAG_member
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$716, DW_AT_name("INTx8")
	.dwattr $C$DW$716, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$716, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$717	.dwtag  DW_TAG_member
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$717, DW_AT_name("rsvd1")
	.dwattr $C$DW$717, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$717, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$717, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$717, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$96


$C$DW$T$97	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$97, DW_AT_name("PIEIFR_REG")
	.dwattr $C$DW$T$97, DW_AT_byte_size(0x01)
$C$DW$718	.dwtag  DW_TAG_member
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$718, DW_AT_name("all")
	.dwattr $C$DW$718, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$719	.dwtag  DW_TAG_member
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$719, DW_AT_name("bit")
	.dwattr $C$DW$719, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$719, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$97


$C$DW$T$98	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$98, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$98, DW_AT_byte_size(0x1a)
$C$DW$720	.dwtag  DW_TAG_member
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$720, DW_AT_name("PIECTRL")
	.dwattr $C$DW$720, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$720, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$721	.dwtag  DW_TAG_member
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$721, DW_AT_name("PIEACK")
	.dwattr $C$DW$721, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$721, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$722, DW_AT_name("PIEIER1")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$723, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$724, DW_AT_name("PIEIER2")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$725, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$726, DW_AT_name("PIEIER3")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$727, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$727, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$728, DW_AT_name("PIEIER4")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$729, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$730, DW_AT_name("PIEIER5")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$731, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$732, DW_AT_name("PIEIER6")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$733, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$734, DW_AT_name("PIEIER7")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$735, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$736, DW_AT_name("PIEIER8")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$737, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$738, DW_AT_name("PIEIER9")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$739, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$740, DW_AT_name("PIEIER10")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$741, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$742, DW_AT_name("PIEIER11")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$743, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$744, DW_AT_name("PIEIER12")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$745, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$98

$C$DW$746	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$98)
$C$DW$T$163	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$163, DW_AT_type(*$C$DW$746)

$C$DW$T$102	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$102, DW_AT_name("PIE_VECT_TABLE")
	.dwattr $C$DW$T$102, DW_AT_byte_size(0x100)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$747, DW_AT_name("PIE1_RESERVED")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_PIE1_RESERVED")
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$748, DW_AT_name("PIE2_RESERVED")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_PIE2_RESERVED")
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$749, DW_AT_name("PIE3_RESERVED")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_PIE3_RESERVED")
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$750, DW_AT_name("PIE4_RESERVED")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_PIE4_RESERVED")
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$751, DW_AT_name("PIE5_RESERVED")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_PIE5_RESERVED")
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$752, DW_AT_name("PIE6_RESERVED")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_PIE6_RESERVED")
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$753, DW_AT_name("PIE7_RESERVED")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_PIE7_RESERVED")
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$754, DW_AT_name("PIE8_RESERVED")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_PIE8_RESERVED")
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$755, DW_AT_name("PIE9_RESERVED")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_PIE9_RESERVED")
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$756, DW_AT_name("PIE10_RESERVED")
	.dwattr $C$DW$756, DW_AT_TI_symbol_name("_PIE10_RESERVED")
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$757, DW_AT_name("PIE11_RESERVED")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_PIE11_RESERVED")
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$758, DW_AT_name("PIE12_RESERVED")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_PIE12_RESERVED")
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$759, DW_AT_name("PIE13_RESERVED")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_PIE13_RESERVED")
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$760, DW_AT_name("TINT1")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_TINT1")
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$761, DW_AT_name("TINT2")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_TINT2")
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$762, DW_AT_name("DATALOG")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_DATALOG")
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$763, DW_AT_name("RTOSINT")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_RTOSINT")
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$764, DW_AT_name("EMUINT")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_EMUINT")
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$765, DW_AT_name("NMI")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_NMI")
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$766, DW_AT_name("ILLEGAL")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_ILLEGAL")
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$767	.dwtag  DW_TAG_member
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$767, DW_AT_name("USER1")
	.dwattr $C$DW$767, DW_AT_TI_symbol_name("_USER1")
	.dwattr $C$DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$767, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$768, DW_AT_name("USER2")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_USER2")
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$769, DW_AT_name("USER3")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_USER3")
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$770, DW_AT_name("USER4")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_USER4")
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$771, DW_AT_name("USER5")
	.dwattr $C$DW$771, DW_AT_TI_symbol_name("_USER5")
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$772, DW_AT_name("USER6")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_USER6")
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x32]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$773, DW_AT_name("USER7")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_USER7")
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$774, DW_AT_name("USER8")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_USER8")
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$775, DW_AT_name("USER9")
	.dwattr $C$DW$775, DW_AT_TI_symbol_name("_USER9")
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$776, DW_AT_name("USER10")
	.dwattr $C$DW$776, DW_AT_TI_symbol_name("_USER10")
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$777, DW_AT_name("USER11")
	.dwattr $C$DW$777, DW_AT_TI_symbol_name("_USER11")
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$778, DW_AT_name("USER12")
	.dwattr $C$DW$778, DW_AT_TI_symbol_name("_USER12")
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$779, DW_AT_name("ADCINT1")
	.dwattr $C$DW$779, DW_AT_TI_symbol_name("_ADCINT1")
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$780, DW_AT_name("ADCINT2")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_ADCINT2")
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x42]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$781, DW_AT_name("rsvd1_3")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_rsvd1_3")
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$782, DW_AT_name("XINT1")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_XINT1")
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x46]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$783, DW_AT_name("XINT2")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_XINT2")
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$784, DW_AT_name("ADCINT9")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_ADCINT9")
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x4a]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$785, DW_AT_name("TINT0")
	.dwattr $C$DW$785, DW_AT_TI_symbol_name("_TINT0")
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$786, DW_AT_name("WAKEINT")
	.dwattr $C$DW$786, DW_AT_TI_symbol_name("_WAKEINT")
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x4e]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$787	.dwtag  DW_TAG_member
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$787, DW_AT_name("EPWM1_TZINT")
	.dwattr $C$DW$787, DW_AT_TI_symbol_name("_EPWM1_TZINT")
	.dwattr $C$DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$787, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$788, DW_AT_name("EPWM2_TZINT")
	.dwattr $C$DW$788, DW_AT_TI_symbol_name("_EPWM2_TZINT")
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x52]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$789, DW_AT_name("EPWM3_TZINT")
	.dwattr $C$DW$789, DW_AT_TI_symbol_name("_EPWM3_TZINT")
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$790	.dwtag  DW_TAG_member
	.dwattr $C$DW$790, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$790, DW_AT_name("EPWM4_TZINT")
	.dwattr $C$DW$790, DW_AT_TI_symbol_name("_EPWM4_TZINT")
	.dwattr $C$DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$790, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$791, DW_AT_name("EPWM5_TZINT")
	.dwattr $C$DW$791, DW_AT_TI_symbol_name("_EPWM5_TZINT")
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$792, DW_AT_name("EPWM6_TZINT")
	.dwattr $C$DW$792, DW_AT_TI_symbol_name("_EPWM6_TZINT")
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$793, DW_AT_name("EPWM7_TZINT")
	.dwattr $C$DW$793, DW_AT_TI_symbol_name("_EPWM7_TZINT")
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$794	.dwtag  DW_TAG_member
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$794, DW_AT_name("EPWM8_TZINT")
	.dwattr $C$DW$794, DW_AT_TI_symbol_name("_EPWM8_TZINT")
	.dwattr $C$DW$794, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$794, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$795	.dwtag  DW_TAG_member
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$795, DW_AT_name("EPWM1_INT")
	.dwattr $C$DW$795, DW_AT_TI_symbol_name("_EPWM1_INT")
	.dwattr $C$DW$795, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$795, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$796	.dwtag  DW_TAG_member
	.dwattr $C$DW$796, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$796, DW_AT_name("EPWM2_INT")
	.dwattr $C$DW$796, DW_AT_TI_symbol_name("_EPWM2_INT")
	.dwattr $C$DW$796, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$796, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$797	.dwtag  DW_TAG_member
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$797, DW_AT_name("EPWM3_INT")
	.dwattr $C$DW$797, DW_AT_TI_symbol_name("_EPWM3_INT")
	.dwattr $C$DW$797, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$797, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$798	.dwtag  DW_TAG_member
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$798, DW_AT_name("EPWM4_INT")
	.dwattr $C$DW$798, DW_AT_TI_symbol_name("_EPWM4_INT")
	.dwattr $C$DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$798, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$799	.dwtag  DW_TAG_member
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$799, DW_AT_name("EPWM5_INT")
	.dwattr $C$DW$799, DW_AT_TI_symbol_name("_EPWM5_INT")
	.dwattr $C$DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$799, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$800	.dwtag  DW_TAG_member
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$800, DW_AT_name("EPWM6_INT")
	.dwattr $C$DW$800, DW_AT_TI_symbol_name("_EPWM6_INT")
	.dwattr $C$DW$800, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$800, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$801	.dwtag  DW_TAG_member
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$801, DW_AT_name("EPWM7_INT")
	.dwattr $C$DW$801, DW_AT_TI_symbol_name("_EPWM7_INT")
	.dwattr $C$DW$801, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$801, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$802	.dwtag  DW_TAG_member
	.dwattr $C$DW$802, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$802, DW_AT_name("EPWM8_INT")
	.dwattr $C$DW$802, DW_AT_TI_symbol_name("_EPWM8_INT")
	.dwattr $C$DW$802, DW_AT_data_member_location[DW_OP_plus_uconst 0x6e]
	.dwattr $C$DW$802, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$803	.dwtag  DW_TAG_member
	.dwattr $C$DW$803, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$803, DW_AT_name("ECAP1_INT")
	.dwattr $C$DW$803, DW_AT_TI_symbol_name("_ECAP1_INT")
	.dwattr $C$DW$803, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$803, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$804	.dwtag  DW_TAG_member
	.dwattr $C$DW$804, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$804, DW_AT_name("ECAP2_INT")
	.dwattr $C$DW$804, DW_AT_TI_symbol_name("_ECAP2_INT")
	.dwattr $C$DW$804, DW_AT_data_member_location[DW_OP_plus_uconst 0x72]
	.dwattr $C$DW$804, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$805	.dwtag  DW_TAG_member
	.dwattr $C$DW$805, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$805, DW_AT_name("ECAP3_INT")
	.dwattr $C$DW$805, DW_AT_TI_symbol_name("_ECAP3_INT")
	.dwattr $C$DW$805, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$805, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$806	.dwtag  DW_TAG_member
	.dwattr $C$DW$806, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$806, DW_AT_name("rsvd4_4")
	.dwattr $C$DW$806, DW_AT_TI_symbol_name("_rsvd4_4")
	.dwattr $C$DW$806, DW_AT_data_member_location[DW_OP_plus_uconst 0x76]
	.dwattr $C$DW$806, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$807	.dwtag  DW_TAG_member
	.dwattr $C$DW$807, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$807, DW_AT_name("rsvd4_5")
	.dwattr $C$DW$807, DW_AT_TI_symbol_name("_rsvd4_5")
	.dwattr $C$DW$807, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$807, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$808	.dwtag  DW_TAG_member
	.dwattr $C$DW$808, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$808, DW_AT_name("rsvd4_6")
	.dwattr $C$DW$808, DW_AT_TI_symbol_name("_rsvd4_6")
	.dwattr $C$DW$808, DW_AT_data_member_location[DW_OP_plus_uconst 0x7a]
	.dwattr $C$DW$808, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$809	.dwtag  DW_TAG_member
	.dwattr $C$DW$809, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$809, DW_AT_name("HRCAP1_INT")
	.dwattr $C$DW$809, DW_AT_TI_symbol_name("_HRCAP1_INT")
	.dwattr $C$DW$809, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$809, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$810	.dwtag  DW_TAG_member
	.dwattr $C$DW$810, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$810, DW_AT_name("HRCAP2_INT")
	.dwattr $C$DW$810, DW_AT_TI_symbol_name("_HRCAP2_INT")
	.dwattr $C$DW$810, DW_AT_data_member_location[DW_OP_plus_uconst 0x7e]
	.dwattr $C$DW$810, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$811	.dwtag  DW_TAG_member
	.dwattr $C$DW$811, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$811, DW_AT_name("EQEP1_INT")
	.dwattr $C$DW$811, DW_AT_TI_symbol_name("_EQEP1_INT")
	.dwattr $C$DW$811, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$811, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$812	.dwtag  DW_TAG_member
	.dwattr $C$DW$812, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$812, DW_AT_name("EQEP2_INT")
	.dwattr $C$DW$812, DW_AT_TI_symbol_name("_EQEP2_INT")
	.dwattr $C$DW$812, DW_AT_data_member_location[DW_OP_plus_uconst 0x82]
	.dwattr $C$DW$812, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$813	.dwtag  DW_TAG_member
	.dwattr $C$DW$813, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$813, DW_AT_name("rsvd5_3")
	.dwattr $C$DW$813, DW_AT_TI_symbol_name("_rsvd5_3")
	.dwattr $C$DW$813, DW_AT_data_member_location[DW_OP_plus_uconst 0x84]
	.dwattr $C$DW$813, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$814	.dwtag  DW_TAG_member
	.dwattr $C$DW$814, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$814, DW_AT_name("HRCAP3_INT")
	.dwattr $C$DW$814, DW_AT_TI_symbol_name("_HRCAP3_INT")
	.dwattr $C$DW$814, DW_AT_data_member_location[DW_OP_plus_uconst 0x86]
	.dwattr $C$DW$814, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$815	.dwtag  DW_TAG_member
	.dwattr $C$DW$815, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$815, DW_AT_name("HRCAP4_INT")
	.dwattr $C$DW$815, DW_AT_TI_symbol_name("_HRCAP4_INT")
	.dwattr $C$DW$815, DW_AT_data_member_location[DW_OP_plus_uconst 0x88]
	.dwattr $C$DW$815, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$816	.dwtag  DW_TAG_member
	.dwattr $C$DW$816, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$816, DW_AT_name("rsvd5_6")
	.dwattr $C$DW$816, DW_AT_TI_symbol_name("_rsvd5_6")
	.dwattr $C$DW$816, DW_AT_data_member_location[DW_OP_plus_uconst 0x8a]
	.dwattr $C$DW$816, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$817	.dwtag  DW_TAG_member
	.dwattr $C$DW$817, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$817, DW_AT_name("rsvd5_7")
	.dwattr $C$DW$817, DW_AT_TI_symbol_name("_rsvd5_7")
	.dwattr $C$DW$817, DW_AT_data_member_location[DW_OP_plus_uconst 0x8c]
	.dwattr $C$DW$817, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$818	.dwtag  DW_TAG_member
	.dwattr $C$DW$818, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$818, DW_AT_name("USB0_INT")
	.dwattr $C$DW$818, DW_AT_TI_symbol_name("_USB0_INT")
	.dwattr $C$DW$818, DW_AT_data_member_location[DW_OP_plus_uconst 0x8e]
	.dwattr $C$DW$818, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$819	.dwtag  DW_TAG_member
	.dwattr $C$DW$819, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$819, DW_AT_name("SPIRXINTA")
	.dwattr $C$DW$819, DW_AT_TI_symbol_name("_SPIRXINTA")
	.dwattr $C$DW$819, DW_AT_data_member_location[DW_OP_plus_uconst 0x90]
	.dwattr $C$DW$819, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$820	.dwtag  DW_TAG_member
	.dwattr $C$DW$820, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$820, DW_AT_name("SPITXINTA")
	.dwattr $C$DW$820, DW_AT_TI_symbol_name("_SPITXINTA")
	.dwattr $C$DW$820, DW_AT_data_member_location[DW_OP_plus_uconst 0x92]
	.dwattr $C$DW$820, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$821	.dwtag  DW_TAG_member
	.dwattr $C$DW$821, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$821, DW_AT_name("SPIRXINTB")
	.dwattr $C$DW$821, DW_AT_TI_symbol_name("_SPIRXINTB")
	.dwattr $C$DW$821, DW_AT_data_member_location[DW_OP_plus_uconst 0x94]
	.dwattr $C$DW$821, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$822	.dwtag  DW_TAG_member
	.dwattr $C$DW$822, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$822, DW_AT_name("SPITXINTB")
	.dwattr $C$DW$822, DW_AT_TI_symbol_name("_SPITXINTB")
	.dwattr $C$DW$822, DW_AT_data_member_location[DW_OP_plus_uconst 0x96]
	.dwattr $C$DW$822, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$823	.dwtag  DW_TAG_member
	.dwattr $C$DW$823, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$823, DW_AT_name("MRINTA")
	.dwattr $C$DW$823, DW_AT_TI_symbol_name("_MRINTA")
	.dwattr $C$DW$823, DW_AT_data_member_location[DW_OP_plus_uconst 0x98]
	.dwattr $C$DW$823, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$824	.dwtag  DW_TAG_member
	.dwattr $C$DW$824, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$824, DW_AT_name("MXINTA")
	.dwattr $C$DW$824, DW_AT_TI_symbol_name("_MXINTA")
	.dwattr $C$DW$824, DW_AT_data_member_location[DW_OP_plus_uconst 0x9a]
	.dwattr $C$DW$824, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$825	.dwtag  DW_TAG_member
	.dwattr $C$DW$825, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$825, DW_AT_name("rsvd6_7")
	.dwattr $C$DW$825, DW_AT_TI_symbol_name("_rsvd6_7")
	.dwattr $C$DW$825, DW_AT_data_member_location[DW_OP_plus_uconst 0x9c]
	.dwattr $C$DW$825, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$826	.dwtag  DW_TAG_member
	.dwattr $C$DW$826, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$826, DW_AT_name("rsvd6_8")
	.dwattr $C$DW$826, DW_AT_TI_symbol_name("_rsvd6_8")
	.dwattr $C$DW$826, DW_AT_data_member_location[DW_OP_plus_uconst 0x9e]
	.dwattr $C$DW$826, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$827	.dwtag  DW_TAG_member
	.dwattr $C$DW$827, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$827, DW_AT_name("DINTCH1")
	.dwattr $C$DW$827, DW_AT_TI_symbol_name("_DINTCH1")
	.dwattr $C$DW$827, DW_AT_data_member_location[DW_OP_plus_uconst 0xa0]
	.dwattr $C$DW$827, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$828	.dwtag  DW_TAG_member
	.dwattr $C$DW$828, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$828, DW_AT_name("DINTCH2")
	.dwattr $C$DW$828, DW_AT_TI_symbol_name("_DINTCH2")
	.dwattr $C$DW$828, DW_AT_data_member_location[DW_OP_plus_uconst 0xa2]
	.dwattr $C$DW$828, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$829	.dwtag  DW_TAG_member
	.dwattr $C$DW$829, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$829, DW_AT_name("DINTCH3")
	.dwattr $C$DW$829, DW_AT_TI_symbol_name("_DINTCH3")
	.dwattr $C$DW$829, DW_AT_data_member_location[DW_OP_plus_uconst 0xa4]
	.dwattr $C$DW$829, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$830	.dwtag  DW_TAG_member
	.dwattr $C$DW$830, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$830, DW_AT_name("DINTCH4")
	.dwattr $C$DW$830, DW_AT_TI_symbol_name("_DINTCH4")
	.dwattr $C$DW$830, DW_AT_data_member_location[DW_OP_plus_uconst 0xa6]
	.dwattr $C$DW$830, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$831	.dwtag  DW_TAG_member
	.dwattr $C$DW$831, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$831, DW_AT_name("DINTCH5")
	.dwattr $C$DW$831, DW_AT_TI_symbol_name("_DINTCH5")
	.dwattr $C$DW$831, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$831, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$832	.dwtag  DW_TAG_member
	.dwattr $C$DW$832, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$832, DW_AT_name("DINTCH6")
	.dwattr $C$DW$832, DW_AT_TI_symbol_name("_DINTCH6")
	.dwattr $C$DW$832, DW_AT_data_member_location[DW_OP_plus_uconst 0xaa]
	.dwattr $C$DW$832, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$833	.dwtag  DW_TAG_member
	.dwattr $C$DW$833, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$833, DW_AT_name("rsvd7_7")
	.dwattr $C$DW$833, DW_AT_TI_symbol_name("_rsvd7_7")
	.dwattr $C$DW$833, DW_AT_data_member_location[DW_OP_plus_uconst 0xac]
	.dwattr $C$DW$833, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$834	.dwtag  DW_TAG_member
	.dwattr $C$DW$834, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$834, DW_AT_name("rsvd7_8")
	.dwattr $C$DW$834, DW_AT_TI_symbol_name("_rsvd7_8")
	.dwattr $C$DW$834, DW_AT_data_member_location[DW_OP_plus_uconst 0xae]
	.dwattr $C$DW$834, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$835	.dwtag  DW_TAG_member
	.dwattr $C$DW$835, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$835, DW_AT_name("I2CINT1A")
	.dwattr $C$DW$835, DW_AT_TI_symbol_name("_I2CINT1A")
	.dwattr $C$DW$835, DW_AT_data_member_location[DW_OP_plus_uconst 0xb0]
	.dwattr $C$DW$835, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$836	.dwtag  DW_TAG_member
	.dwattr $C$DW$836, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$836, DW_AT_name("I2CINT2A")
	.dwattr $C$DW$836, DW_AT_TI_symbol_name("_I2CINT2A")
	.dwattr $C$DW$836, DW_AT_data_member_location[DW_OP_plus_uconst 0xb2]
	.dwattr $C$DW$836, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$837	.dwtag  DW_TAG_member
	.dwattr $C$DW$837, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$837, DW_AT_name("rsvd8_3")
	.dwattr $C$DW$837, DW_AT_TI_symbol_name("_rsvd8_3")
	.dwattr $C$DW$837, DW_AT_data_member_location[DW_OP_plus_uconst 0xb4]
	.dwattr $C$DW$837, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$838	.dwtag  DW_TAG_member
	.dwattr $C$DW$838, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$838, DW_AT_name("rsvd8_4")
	.dwattr $C$DW$838, DW_AT_TI_symbol_name("_rsvd8_4")
	.dwattr $C$DW$838, DW_AT_data_member_location[DW_OP_plus_uconst 0xb6]
	.dwattr $C$DW$838, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$839	.dwtag  DW_TAG_member
	.dwattr $C$DW$839, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$839, DW_AT_name("rsvd8_5")
	.dwattr $C$DW$839, DW_AT_TI_symbol_name("_rsvd8_5")
	.dwattr $C$DW$839, DW_AT_data_member_location[DW_OP_plus_uconst 0xb8]
	.dwattr $C$DW$839, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$840	.dwtag  DW_TAG_member
	.dwattr $C$DW$840, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$840, DW_AT_name("rsvd8_6")
	.dwattr $C$DW$840, DW_AT_TI_symbol_name("_rsvd8_6")
	.dwattr $C$DW$840, DW_AT_data_member_location[DW_OP_plus_uconst 0xba]
	.dwattr $C$DW$840, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$841	.dwtag  DW_TAG_member
	.dwattr $C$DW$841, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$841, DW_AT_name("rsvd8_7")
	.dwattr $C$DW$841, DW_AT_TI_symbol_name("_rsvd8_7")
	.dwattr $C$DW$841, DW_AT_data_member_location[DW_OP_plus_uconst 0xbc]
	.dwattr $C$DW$841, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$842	.dwtag  DW_TAG_member
	.dwattr $C$DW$842, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$842, DW_AT_name("rsvd8_8")
	.dwattr $C$DW$842, DW_AT_TI_symbol_name("_rsvd8_8")
	.dwattr $C$DW$842, DW_AT_data_member_location[DW_OP_plus_uconst 0xbe]
	.dwattr $C$DW$842, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$843	.dwtag  DW_TAG_member
	.dwattr $C$DW$843, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$843, DW_AT_name("SCIRXINTA")
	.dwattr $C$DW$843, DW_AT_TI_symbol_name("_SCIRXINTA")
	.dwattr $C$DW$843, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$843, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$844	.dwtag  DW_TAG_member
	.dwattr $C$DW$844, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$844, DW_AT_name("SCITXINTA")
	.dwattr $C$DW$844, DW_AT_TI_symbol_name("_SCITXINTA")
	.dwattr $C$DW$844, DW_AT_data_member_location[DW_OP_plus_uconst 0xc2]
	.dwattr $C$DW$844, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$845	.dwtag  DW_TAG_member
	.dwattr $C$DW$845, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$845, DW_AT_name("SCIRXINTB")
	.dwattr $C$DW$845, DW_AT_TI_symbol_name("_SCIRXINTB")
	.dwattr $C$DW$845, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$845, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$846	.dwtag  DW_TAG_member
	.dwattr $C$DW$846, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$846, DW_AT_name("SCITXINTB")
	.dwattr $C$DW$846, DW_AT_TI_symbol_name("_SCITXINTB")
	.dwattr $C$DW$846, DW_AT_data_member_location[DW_OP_plus_uconst 0xc6]
	.dwattr $C$DW$846, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$847	.dwtag  DW_TAG_member
	.dwattr $C$DW$847, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$847, DW_AT_name("ECAN0INTA")
	.dwattr $C$DW$847, DW_AT_TI_symbol_name("_ECAN0INTA")
	.dwattr $C$DW$847, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$847, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$848	.dwtag  DW_TAG_member
	.dwattr $C$DW$848, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$848, DW_AT_name("ECAN1INTA")
	.dwattr $C$DW$848, DW_AT_TI_symbol_name("_ECAN1INTA")
	.dwattr $C$DW$848, DW_AT_data_member_location[DW_OP_plus_uconst 0xca]
	.dwattr $C$DW$848, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$849	.dwtag  DW_TAG_member
	.dwattr $C$DW$849, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$849, DW_AT_name("rsvd9_7")
	.dwattr $C$DW$849, DW_AT_TI_symbol_name("_rsvd9_7")
	.dwattr $C$DW$849, DW_AT_data_member_location[DW_OP_plus_uconst 0xcc]
	.dwattr $C$DW$849, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$850	.dwtag  DW_TAG_member
	.dwattr $C$DW$850, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$850, DW_AT_name("rsvd9_8")
	.dwattr $C$DW$850, DW_AT_TI_symbol_name("_rsvd9_8")
	.dwattr $C$DW$850, DW_AT_data_member_location[DW_OP_plus_uconst 0xce]
	.dwattr $C$DW$850, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$851	.dwtag  DW_TAG_member
	.dwattr $C$DW$851, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$851, DW_AT_name("rsvd10_1")
	.dwattr $C$DW$851, DW_AT_TI_symbol_name("_rsvd10_1")
	.dwattr $C$DW$851, DW_AT_data_member_location[DW_OP_plus_uconst 0xd0]
	.dwattr $C$DW$851, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$852	.dwtag  DW_TAG_member
	.dwattr $C$DW$852, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$852, DW_AT_name("rsvd10_2")
	.dwattr $C$DW$852, DW_AT_TI_symbol_name("_rsvd10_2")
	.dwattr $C$DW$852, DW_AT_data_member_location[DW_OP_plus_uconst 0xd2]
	.dwattr $C$DW$852, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$853	.dwtag  DW_TAG_member
	.dwattr $C$DW$853, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$853, DW_AT_name("ADCINT3")
	.dwattr $C$DW$853, DW_AT_TI_symbol_name("_ADCINT3")
	.dwattr $C$DW$853, DW_AT_data_member_location[DW_OP_plus_uconst 0xd4]
	.dwattr $C$DW$853, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$854	.dwtag  DW_TAG_member
	.dwattr $C$DW$854, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$854, DW_AT_name("ADCINT4")
	.dwattr $C$DW$854, DW_AT_TI_symbol_name("_ADCINT4")
	.dwattr $C$DW$854, DW_AT_data_member_location[DW_OP_plus_uconst 0xd6]
	.dwattr $C$DW$854, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$855	.dwtag  DW_TAG_member
	.dwattr $C$DW$855, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$855, DW_AT_name("ADCINT5")
	.dwattr $C$DW$855, DW_AT_TI_symbol_name("_ADCINT5")
	.dwattr $C$DW$855, DW_AT_data_member_location[DW_OP_plus_uconst 0xd8]
	.dwattr $C$DW$855, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$856	.dwtag  DW_TAG_member
	.dwattr $C$DW$856, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$856, DW_AT_name("ADCINT6")
	.dwattr $C$DW$856, DW_AT_TI_symbol_name("_ADCINT6")
	.dwattr $C$DW$856, DW_AT_data_member_location[DW_OP_plus_uconst 0xda]
	.dwattr $C$DW$856, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$857	.dwtag  DW_TAG_member
	.dwattr $C$DW$857, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$857, DW_AT_name("ADCINT7")
	.dwattr $C$DW$857, DW_AT_TI_symbol_name("_ADCINT7")
	.dwattr $C$DW$857, DW_AT_data_member_location[DW_OP_plus_uconst 0xdc]
	.dwattr $C$DW$857, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$858	.dwtag  DW_TAG_member
	.dwattr $C$DW$858, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$858, DW_AT_name("ADCINT8")
	.dwattr $C$DW$858, DW_AT_TI_symbol_name("_ADCINT8")
	.dwattr $C$DW$858, DW_AT_data_member_location[DW_OP_plus_uconst 0xde]
	.dwattr $C$DW$858, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$859	.dwtag  DW_TAG_member
	.dwattr $C$DW$859, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$859, DW_AT_name("CLA1_INT1")
	.dwattr $C$DW$859, DW_AT_TI_symbol_name("_CLA1_INT1")
	.dwattr $C$DW$859, DW_AT_data_member_location[DW_OP_plus_uconst 0xe0]
	.dwattr $C$DW$859, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$860	.dwtag  DW_TAG_member
	.dwattr $C$DW$860, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$860, DW_AT_name("CLA1_INT2")
	.dwattr $C$DW$860, DW_AT_TI_symbol_name("_CLA1_INT2")
	.dwattr $C$DW$860, DW_AT_data_member_location[DW_OP_plus_uconst 0xe2]
	.dwattr $C$DW$860, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$861	.dwtag  DW_TAG_member
	.dwattr $C$DW$861, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$861, DW_AT_name("CLA1_INT3")
	.dwattr $C$DW$861, DW_AT_TI_symbol_name("_CLA1_INT3")
	.dwattr $C$DW$861, DW_AT_data_member_location[DW_OP_plus_uconst 0xe4]
	.dwattr $C$DW$861, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$862	.dwtag  DW_TAG_member
	.dwattr $C$DW$862, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$862, DW_AT_name("CLA1_INT4")
	.dwattr $C$DW$862, DW_AT_TI_symbol_name("_CLA1_INT4")
	.dwattr $C$DW$862, DW_AT_data_member_location[DW_OP_plus_uconst 0xe6]
	.dwattr $C$DW$862, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$863	.dwtag  DW_TAG_member
	.dwattr $C$DW$863, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$863, DW_AT_name("CLA1_INT5")
	.dwattr $C$DW$863, DW_AT_TI_symbol_name("_CLA1_INT5")
	.dwattr $C$DW$863, DW_AT_data_member_location[DW_OP_plus_uconst 0xe8]
	.dwattr $C$DW$863, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$864	.dwtag  DW_TAG_member
	.dwattr $C$DW$864, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$864, DW_AT_name("CLA1_INT6")
	.dwattr $C$DW$864, DW_AT_TI_symbol_name("_CLA1_INT6")
	.dwattr $C$DW$864, DW_AT_data_member_location[DW_OP_plus_uconst 0xea]
	.dwattr $C$DW$864, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$865	.dwtag  DW_TAG_member
	.dwattr $C$DW$865, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$865, DW_AT_name("CLA1_INT7")
	.dwattr $C$DW$865, DW_AT_TI_symbol_name("_CLA1_INT7")
	.dwattr $C$DW$865, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$865, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$866	.dwtag  DW_TAG_member
	.dwattr $C$DW$866, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$866, DW_AT_name("CLA1_INT8")
	.dwattr $C$DW$866, DW_AT_TI_symbol_name("_CLA1_INT8")
	.dwattr $C$DW$866, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$866, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$867	.dwtag  DW_TAG_member
	.dwattr $C$DW$867, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$867, DW_AT_name("XINT3")
	.dwattr $C$DW$867, DW_AT_TI_symbol_name("_XINT3")
	.dwattr $C$DW$867, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$867, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$868	.dwtag  DW_TAG_member
	.dwattr $C$DW$868, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$868, DW_AT_name("rsvd12_2")
	.dwattr $C$DW$868, DW_AT_TI_symbol_name("_rsvd12_2")
	.dwattr $C$DW$868, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$868, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$869	.dwtag  DW_TAG_member
	.dwattr $C$DW$869, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$869, DW_AT_name("rsvd12_3")
	.dwattr $C$DW$869, DW_AT_TI_symbol_name("_rsvd12_3")
	.dwattr $C$DW$869, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$869, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$870	.dwtag  DW_TAG_member
	.dwattr $C$DW$870, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$870, DW_AT_name("rsvd12_4")
	.dwattr $C$DW$870, DW_AT_TI_symbol_name("_rsvd12_4")
	.dwattr $C$DW$870, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$870, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$871	.dwtag  DW_TAG_member
	.dwattr $C$DW$871, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$871, DW_AT_name("rsvd12_5")
	.dwattr $C$DW$871, DW_AT_TI_symbol_name("_rsvd12_5")
	.dwattr $C$DW$871, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$871, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$872	.dwtag  DW_TAG_member
	.dwattr $C$DW$872, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$872, DW_AT_name("rsvd12_6")
	.dwattr $C$DW$872, DW_AT_TI_symbol_name("_rsvd12_6")
	.dwattr $C$DW$872, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$872, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$873	.dwtag  DW_TAG_member
	.dwattr $C$DW$873, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$873, DW_AT_name("LVF")
	.dwattr $C$DW$873, DW_AT_TI_symbol_name("_LVF")
	.dwattr $C$DW$873, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$873, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$874	.dwtag  DW_TAG_member
	.dwattr $C$DW$874, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$874, DW_AT_name("LUF")
	.dwattr $C$DW$874, DW_AT_TI_symbol_name("_LUF")
	.dwattr $C$DW$874, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$874, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$102


$C$DW$T$103	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$103, DW_AT_name("PLL2CTL_BITS")
	.dwattr $C$DW$T$103, DW_AT_byte_size(0x01)
$C$DW$875	.dwtag  DW_TAG_member
	.dwattr $C$DW$875, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$875, DW_AT_name("PLL2CLKSRCSEL")
	.dwattr $C$DW$875, DW_AT_TI_symbol_name("_PLL2CLKSRCSEL")
	.dwattr $C$DW$875, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$875, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$875, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$876	.dwtag  DW_TAG_member
	.dwattr $C$DW$876, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$876, DW_AT_name("PLL2EN")
	.dwattr $C$DW$876, DW_AT_TI_symbol_name("_PLL2EN")
	.dwattr $C$DW$876, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$876, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$876, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$877	.dwtag  DW_TAG_member
	.dwattr $C$DW$877, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$877, DW_AT_name("rsvd1")
	.dwattr $C$DW$877, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$877, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0d)
	.dwattr $C$DW$877, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$877, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$103


$C$DW$T$104	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$104, DW_AT_name("PLL2CTL_REG")
	.dwattr $C$DW$T$104, DW_AT_byte_size(0x01)
$C$DW$878	.dwtag  DW_TAG_member
	.dwattr $C$DW$878, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$878, DW_AT_name("all")
	.dwattr $C$DW$878, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$878, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$878, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$879	.dwtag  DW_TAG_member
	.dwattr $C$DW$879, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$879, DW_AT_name("bit")
	.dwattr $C$DW$879, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$879, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$879, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$104


$C$DW$T$105	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$105, DW_AT_name("PLL2MULT_BITS")
	.dwattr $C$DW$T$105, DW_AT_byte_size(0x01)
$C$DW$880	.dwtag  DW_TAG_member
	.dwattr $C$DW$880, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$880, DW_AT_name("PLL2MULT")
	.dwattr $C$DW$880, DW_AT_TI_symbol_name("_PLL2MULT")
	.dwattr $C$DW$880, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x04)
	.dwattr $C$DW$880, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$880, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$881	.dwtag  DW_TAG_member
	.dwattr $C$DW$881, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$881, DW_AT_name("rsvd1")
	.dwattr $C$DW$881, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$881, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0c)
	.dwattr $C$DW$881, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$881, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$105


$C$DW$T$106	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$106, DW_AT_name("PLL2MULT_REG")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x01)
$C$DW$882	.dwtag  DW_TAG_member
	.dwattr $C$DW$882, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$882, DW_AT_name("all")
	.dwattr $C$DW$882, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$882, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$882, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$883	.dwtag  DW_TAG_member
	.dwattr $C$DW$883, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$883, DW_AT_name("bit")
	.dwattr $C$DW$883, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$883, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$883, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$106


$C$DW$T$107	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$107, DW_AT_name("PLL2STS_BITS")
	.dwattr $C$DW$T$107, DW_AT_byte_size(0x01)
$C$DW$884	.dwtag  DW_TAG_member
	.dwattr $C$DW$884, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$884, DW_AT_name("PLL2LOCKS")
	.dwattr $C$DW$884, DW_AT_TI_symbol_name("_PLL2LOCKS")
	.dwattr $C$DW$884, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$884, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$884, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$885	.dwtag  DW_TAG_member
	.dwattr $C$DW$885, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$885, DW_AT_name("rsvd1")
	.dwattr $C$DW$885, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$885, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$885, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$885, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$107


$C$DW$T$108	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$108, DW_AT_name("PLL2STS_REG")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x01)
$C$DW$886	.dwtag  DW_TAG_member
	.dwattr $C$DW$886, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$886, DW_AT_name("all")
	.dwattr $C$DW$886, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$886, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$886, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$887	.dwtag  DW_TAG_member
	.dwattr $C$DW$887, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$887, DW_AT_name("bit")
	.dwattr $C$DW$887, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$887, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$887, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$108


$C$DW$T$109	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$109, DW_AT_name("PLLCR_BITS")
	.dwattr $C$DW$T$109, DW_AT_byte_size(0x01)
$C$DW$888	.dwtag  DW_TAG_member
	.dwattr $C$DW$888, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$888, DW_AT_name("DIV")
	.dwattr $C$DW$888, DW_AT_TI_symbol_name("_DIV")
	.dwattr $C$DW$888, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$888, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$888, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$889	.dwtag  DW_TAG_member
	.dwattr $C$DW$889, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$889, DW_AT_name("rsvd1")
	.dwattr $C$DW$889, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$889, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0b)
	.dwattr $C$DW$889, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$889, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$109


$C$DW$T$110	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$110, DW_AT_name("PLLCR_REG")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x01)
$C$DW$890	.dwtag  DW_TAG_member
	.dwattr $C$DW$890, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$890, DW_AT_name("all")
	.dwattr $C$DW$890, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$890, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$890, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$891	.dwtag  DW_TAG_member
	.dwattr $C$DW$891, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$891, DW_AT_name("bit")
	.dwattr $C$DW$891, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$891, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$891, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$110


$C$DW$T$111	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$111, DW_AT_name("PLLSTS_BITS")
	.dwattr $C$DW$T$111, DW_AT_byte_size(0x01)
$C$DW$892	.dwtag  DW_TAG_member
	.dwattr $C$DW$892, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$892, DW_AT_name("PLLLOCKS")
	.dwattr $C$DW$892, DW_AT_TI_symbol_name("_PLLLOCKS")
	.dwattr $C$DW$892, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$892, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$892, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$893	.dwtag  DW_TAG_member
	.dwattr $C$DW$893, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$893, DW_AT_name("rsvd1")
	.dwattr $C$DW$893, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$893, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$893, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$893, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$894	.dwtag  DW_TAG_member
	.dwattr $C$DW$894, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$894, DW_AT_name("PLLOFF")
	.dwattr $C$DW$894, DW_AT_TI_symbol_name("_PLLOFF")
	.dwattr $C$DW$894, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$894, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$894, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$895	.dwtag  DW_TAG_member
	.dwattr $C$DW$895, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$895, DW_AT_name("MCLKSTS")
	.dwattr $C$DW$895, DW_AT_TI_symbol_name("_MCLKSTS")
	.dwattr $C$DW$895, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$895, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$895, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$896	.dwtag  DW_TAG_member
	.dwattr $C$DW$896, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$896, DW_AT_name("MCLKCLR")
	.dwattr $C$DW$896, DW_AT_TI_symbol_name("_MCLKCLR")
	.dwattr $C$DW$896, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$896, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$896, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$897	.dwtag  DW_TAG_member
	.dwattr $C$DW$897, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$897, DW_AT_name("OSCOFF")
	.dwattr $C$DW$897, DW_AT_TI_symbol_name("_OSCOFF")
	.dwattr $C$DW$897, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$897, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$897, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$898	.dwtag  DW_TAG_member
	.dwattr $C$DW$898, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$898, DW_AT_name("MCLKOFF")
	.dwattr $C$DW$898, DW_AT_TI_symbol_name("_MCLKOFF")
	.dwattr $C$DW$898, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$898, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$898, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$899	.dwtag  DW_TAG_member
	.dwattr $C$DW$899, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$899, DW_AT_name("DIVSEL")
	.dwattr $C$DW$899, DW_AT_TI_symbol_name("_DIVSEL")
	.dwattr $C$DW$899, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x02)
	.dwattr $C$DW$899, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$899, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$900	.dwtag  DW_TAG_member
	.dwattr $C$DW$900, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$900, DW_AT_name("rsvd2")
	.dwattr $C$DW$900, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$900, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x06)
	.dwattr $C$DW$900, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$900, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$901	.dwtag  DW_TAG_member
	.dwattr $C$DW$901, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$901, DW_AT_name("NORMRDYE")
	.dwattr $C$DW$901, DW_AT_TI_symbol_name("_NORMRDYE")
	.dwattr $C$DW$901, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$901, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$901, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$111


$C$DW$T$112	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$112, DW_AT_name("PLLSTS_REG")
	.dwattr $C$DW$T$112, DW_AT_byte_size(0x01)
$C$DW$902	.dwtag  DW_TAG_member
	.dwattr $C$DW$902, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$902, DW_AT_name("all")
	.dwattr $C$DW$902, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$902, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$902, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$903	.dwtag  DW_TAG_member
	.dwattr $C$DW$903, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$903, DW_AT_name("bit")
	.dwattr $C$DW$903, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$903, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$903, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$112


$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x04)
$C$DW$904	.dwtag  DW_TAG_member
	.dwattr $C$DW$904, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$904, DW_AT_name("next")
	.dwattr $C$DW$904, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$904, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$904, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$905	.dwtag  DW_TAG_member
	.dwattr $C$DW$905, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$905, DW_AT_name("prev")
	.dwattr $C$DW$905, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$905, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$905, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114

$C$DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)
$C$DW$T$113	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x16)

$C$DW$T$115	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$115, DW_AT_name("SCICCR_BITS")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x01)
$C$DW$906	.dwtag  DW_TAG_member
	.dwattr $C$DW$906, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$906, DW_AT_name("SCICHAR")
	.dwattr $C$DW$906, DW_AT_TI_symbol_name("_SCICHAR")
	.dwattr $C$DW$906, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$906, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$906, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$907	.dwtag  DW_TAG_member
	.dwattr $C$DW$907, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$907, DW_AT_name("ADDRIDLE_MODE")
	.dwattr $C$DW$907, DW_AT_TI_symbol_name("_ADDRIDLE_MODE")
	.dwattr $C$DW$907, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$907, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$907, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$908	.dwtag  DW_TAG_member
	.dwattr $C$DW$908, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$908, DW_AT_name("LOOPBKENA")
	.dwattr $C$DW$908, DW_AT_TI_symbol_name("_LOOPBKENA")
	.dwattr $C$DW$908, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$908, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$908, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$909	.dwtag  DW_TAG_member
	.dwattr $C$DW$909, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$909, DW_AT_name("PARITYENA")
	.dwattr $C$DW$909, DW_AT_TI_symbol_name("_PARITYENA")
	.dwattr $C$DW$909, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$909, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$909, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$910	.dwtag  DW_TAG_member
	.dwattr $C$DW$910, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$910, DW_AT_name("PARITY")
	.dwattr $C$DW$910, DW_AT_TI_symbol_name("_PARITY")
	.dwattr $C$DW$910, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$910, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$910, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$911	.dwtag  DW_TAG_member
	.dwattr $C$DW$911, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$911, DW_AT_name("STOPBITS")
	.dwattr $C$DW$911, DW_AT_TI_symbol_name("_STOPBITS")
	.dwattr $C$DW$911, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$911, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$911, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$912	.dwtag  DW_TAG_member
	.dwattr $C$DW$912, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$912, DW_AT_name("rsvd1")
	.dwattr $C$DW$912, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$912, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$912, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$912, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$115


$C$DW$T$116	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$116, DW_AT_name("SCICCR_REG")
	.dwattr $C$DW$T$116, DW_AT_byte_size(0x01)
$C$DW$913	.dwtag  DW_TAG_member
	.dwattr $C$DW$913, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$913, DW_AT_name("all")
	.dwattr $C$DW$913, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$913, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$913, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$914	.dwtag  DW_TAG_member
	.dwattr $C$DW$914, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$914, DW_AT_name("bit")
	.dwattr $C$DW$914, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$914, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$914, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$116


$C$DW$T$117	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$117, DW_AT_name("SCICTL1_BITS")
	.dwattr $C$DW$T$117, DW_AT_byte_size(0x01)
$C$DW$915	.dwtag  DW_TAG_member
	.dwattr $C$DW$915, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$915, DW_AT_name("RXENA")
	.dwattr $C$DW$915, DW_AT_TI_symbol_name("_RXENA")
	.dwattr $C$DW$915, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$915, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$915, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$916	.dwtag  DW_TAG_member
	.dwattr $C$DW$916, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$916, DW_AT_name("TXENA")
	.dwattr $C$DW$916, DW_AT_TI_symbol_name("_TXENA")
	.dwattr $C$DW$916, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$916, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$916, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$917	.dwtag  DW_TAG_member
	.dwattr $C$DW$917, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$917, DW_AT_name("SLEEP")
	.dwattr $C$DW$917, DW_AT_TI_symbol_name("_SLEEP")
	.dwattr $C$DW$917, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$917, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$917, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$918	.dwtag  DW_TAG_member
	.dwattr $C$DW$918, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$918, DW_AT_name("TXWAKE")
	.dwattr $C$DW$918, DW_AT_TI_symbol_name("_TXWAKE")
	.dwattr $C$DW$918, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$918, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$918, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$919	.dwtag  DW_TAG_member
	.dwattr $C$DW$919, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$919, DW_AT_name("rsvd1")
	.dwattr $C$DW$919, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$919, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$919, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$919, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$920	.dwtag  DW_TAG_member
	.dwattr $C$DW$920, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$920, DW_AT_name("SWRESET")
	.dwattr $C$DW$920, DW_AT_TI_symbol_name("_SWRESET")
	.dwattr $C$DW$920, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$920, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$920, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$921	.dwtag  DW_TAG_member
	.dwattr $C$DW$921, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$921, DW_AT_name("RXERRINTENA")
	.dwattr $C$DW$921, DW_AT_TI_symbol_name("_RXERRINTENA")
	.dwattr $C$DW$921, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$921, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$921, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$922	.dwtag  DW_TAG_member
	.dwattr $C$DW$922, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$922, DW_AT_name("rsvd2")
	.dwattr $C$DW$922, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$922, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x09)
	.dwattr $C$DW$922, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$922, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$117


$C$DW$T$118	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$118, DW_AT_name("SCICTL1_REG")
	.dwattr $C$DW$T$118, DW_AT_byte_size(0x01)
$C$DW$923	.dwtag  DW_TAG_member
	.dwattr $C$DW$923, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$923, DW_AT_name("all")
	.dwattr $C$DW$923, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$923, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$923, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$924	.dwtag  DW_TAG_member
	.dwattr $C$DW$924, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$924, DW_AT_name("bit")
	.dwattr $C$DW$924, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$924, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$924, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$118


$C$DW$T$119	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$119, DW_AT_name("SCICTL2_BITS")
	.dwattr $C$DW$T$119, DW_AT_byte_size(0x01)
$C$DW$925	.dwtag  DW_TAG_member
	.dwattr $C$DW$925, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$925, DW_AT_name("TXINTENA")
	.dwattr $C$DW$925, DW_AT_TI_symbol_name("_TXINTENA")
	.dwattr $C$DW$925, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$925, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$925, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$926	.dwtag  DW_TAG_member
	.dwattr $C$DW$926, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$926, DW_AT_name("RXBKINTENA")
	.dwattr $C$DW$926, DW_AT_TI_symbol_name("_RXBKINTENA")
	.dwattr $C$DW$926, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$926, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$926, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$927	.dwtag  DW_TAG_member
	.dwattr $C$DW$927, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$927, DW_AT_name("rsvd1")
	.dwattr $C$DW$927, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$927, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x04)
	.dwattr $C$DW$927, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$927, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$928	.dwtag  DW_TAG_member
	.dwattr $C$DW$928, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$928, DW_AT_name("TXEMPTY")
	.dwattr $C$DW$928, DW_AT_TI_symbol_name("_TXEMPTY")
	.dwattr $C$DW$928, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$928, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$928, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$929	.dwtag  DW_TAG_member
	.dwattr $C$DW$929, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$929, DW_AT_name("TXRDY")
	.dwattr $C$DW$929, DW_AT_TI_symbol_name("_TXRDY")
	.dwattr $C$DW$929, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$929, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$929, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$930	.dwtag  DW_TAG_member
	.dwattr $C$DW$930, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$930, DW_AT_name("rsvd2")
	.dwattr $C$DW$930, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$930, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$930, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$930, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$119


$C$DW$T$120	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$120, DW_AT_name("SCICTL2_REG")
	.dwattr $C$DW$T$120, DW_AT_byte_size(0x01)
$C$DW$931	.dwtag  DW_TAG_member
	.dwattr $C$DW$931, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$931, DW_AT_name("all")
	.dwattr $C$DW$931, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$931, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$931, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$932	.dwtag  DW_TAG_member
	.dwattr $C$DW$932, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$932, DW_AT_name("bit")
	.dwattr $C$DW$932, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$932, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$932, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$120


$C$DW$T$121	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$121, DW_AT_name("SCIFFCT_BITS")
	.dwattr $C$DW$T$121, DW_AT_byte_size(0x01)
$C$DW$933	.dwtag  DW_TAG_member
	.dwattr $C$DW$933, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$933, DW_AT_name("FFTXDLY")
	.dwattr $C$DW$933, DW_AT_TI_symbol_name("_FFTXDLY")
	.dwattr $C$DW$933, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$933, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$933, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$934	.dwtag  DW_TAG_member
	.dwattr $C$DW$934, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$934, DW_AT_name("rsvd1")
	.dwattr $C$DW$934, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$934, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$934, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$934, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$935	.dwtag  DW_TAG_member
	.dwattr $C$DW$935, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$935, DW_AT_name("CDC")
	.dwattr $C$DW$935, DW_AT_TI_symbol_name("_CDC")
	.dwattr $C$DW$935, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$935, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$935, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$936	.dwtag  DW_TAG_member
	.dwattr $C$DW$936, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$936, DW_AT_name("ABDCLR")
	.dwattr $C$DW$936, DW_AT_TI_symbol_name("_ABDCLR")
	.dwattr $C$DW$936, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$936, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$936, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$937	.dwtag  DW_TAG_member
	.dwattr $C$DW$937, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$937, DW_AT_name("ABD")
	.dwattr $C$DW$937, DW_AT_TI_symbol_name("_ABD")
	.dwattr $C$DW$937, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$937, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$937, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$121


$C$DW$T$122	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$122, DW_AT_name("SCIFFCT_REG")
	.dwattr $C$DW$T$122, DW_AT_byte_size(0x01)
$C$DW$938	.dwtag  DW_TAG_member
	.dwattr $C$DW$938, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$938, DW_AT_name("all")
	.dwattr $C$DW$938, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$938, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$938, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$939	.dwtag  DW_TAG_member
	.dwattr $C$DW$939, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$939, DW_AT_name("bit")
	.dwattr $C$DW$939, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$939, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$939, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$122


$C$DW$T$123	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$123, DW_AT_name("SCIFFRX_BITS")
	.dwattr $C$DW$T$123, DW_AT_byte_size(0x01)
$C$DW$940	.dwtag  DW_TAG_member
	.dwattr $C$DW$940, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$940, DW_AT_name("RXFFIL")
	.dwattr $C$DW$940, DW_AT_TI_symbol_name("_RXFFIL")
	.dwattr $C$DW$940, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$940, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$940, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$941	.dwtag  DW_TAG_member
	.dwattr $C$DW$941, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$941, DW_AT_name("RXFFIENA")
	.dwattr $C$DW$941, DW_AT_TI_symbol_name("_RXFFIENA")
	.dwattr $C$DW$941, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$941, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$941, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$942	.dwtag  DW_TAG_member
	.dwattr $C$DW$942, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$942, DW_AT_name("RXFFINTCLR")
	.dwattr $C$DW$942, DW_AT_TI_symbol_name("_RXFFINTCLR")
	.dwattr $C$DW$942, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$942, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$942, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$943	.dwtag  DW_TAG_member
	.dwattr $C$DW$943, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$943, DW_AT_name("RXFFINT")
	.dwattr $C$DW$943, DW_AT_TI_symbol_name("_RXFFINT")
	.dwattr $C$DW$943, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$943, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$943, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$944	.dwtag  DW_TAG_member
	.dwattr $C$DW$944, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$944, DW_AT_name("RXFFST")
	.dwattr $C$DW$944, DW_AT_TI_symbol_name("_RXFFST")
	.dwattr $C$DW$944, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$944, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$944, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$945	.dwtag  DW_TAG_member
	.dwattr $C$DW$945, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$945, DW_AT_name("RXFIFORESET")
	.dwattr $C$DW$945, DW_AT_TI_symbol_name("_RXFIFORESET")
	.dwattr $C$DW$945, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$945, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$945, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$946	.dwtag  DW_TAG_member
	.dwattr $C$DW$946, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$946, DW_AT_name("RXFFOVRCLR")
	.dwattr $C$DW$946, DW_AT_TI_symbol_name("_RXFFOVRCLR")
	.dwattr $C$DW$946, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$946, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$946, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$947	.dwtag  DW_TAG_member
	.dwattr $C$DW$947, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$947, DW_AT_name("RXFFOVF")
	.dwattr $C$DW$947, DW_AT_TI_symbol_name("_RXFFOVF")
	.dwattr $C$DW$947, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$947, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$947, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$123


$C$DW$T$124	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$124, DW_AT_name("SCIFFRX_REG")
	.dwattr $C$DW$T$124, DW_AT_byte_size(0x01)
$C$DW$948	.dwtag  DW_TAG_member
	.dwattr $C$DW$948, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$948, DW_AT_name("all")
	.dwattr $C$DW$948, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$948, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$948, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$949	.dwtag  DW_TAG_member
	.dwattr $C$DW$949, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$949, DW_AT_name("bit")
	.dwattr $C$DW$949, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$949, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$949, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$124


$C$DW$T$125	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$125, DW_AT_name("SCIFFTX_BITS")
	.dwattr $C$DW$T$125, DW_AT_byte_size(0x01)
$C$DW$950	.dwtag  DW_TAG_member
	.dwattr $C$DW$950, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$950, DW_AT_name("TXFFIL")
	.dwattr $C$DW$950, DW_AT_TI_symbol_name("_TXFFIL")
	.dwattr $C$DW$950, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$950, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$950, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$951	.dwtag  DW_TAG_member
	.dwattr $C$DW$951, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$951, DW_AT_name("TXFFIENA")
	.dwattr $C$DW$951, DW_AT_TI_symbol_name("_TXFFIENA")
	.dwattr $C$DW$951, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$951, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$951, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$952	.dwtag  DW_TAG_member
	.dwattr $C$DW$952, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$952, DW_AT_name("TXFFINTCLR")
	.dwattr $C$DW$952, DW_AT_TI_symbol_name("_TXFFINTCLR")
	.dwattr $C$DW$952, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$952, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$952, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$953	.dwtag  DW_TAG_member
	.dwattr $C$DW$953, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$953, DW_AT_name("TXFFINT")
	.dwattr $C$DW$953, DW_AT_TI_symbol_name("_TXFFINT")
	.dwattr $C$DW$953, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$953, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$953, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$954	.dwtag  DW_TAG_member
	.dwattr $C$DW$954, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$954, DW_AT_name("TXFFST")
	.dwattr $C$DW$954, DW_AT_TI_symbol_name("_TXFFST")
	.dwattr $C$DW$954, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$954, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$954, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$955	.dwtag  DW_TAG_member
	.dwattr $C$DW$955, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$955, DW_AT_name("TXFIFOXRESET")
	.dwattr $C$DW$955, DW_AT_TI_symbol_name("_TXFIFOXRESET")
	.dwattr $C$DW$955, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$955, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$955, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$956	.dwtag  DW_TAG_member
	.dwattr $C$DW$956, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$956, DW_AT_name("SCIFFENA")
	.dwattr $C$DW$956, DW_AT_TI_symbol_name("_SCIFFENA")
	.dwattr $C$DW$956, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$956, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$956, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$957	.dwtag  DW_TAG_member
	.dwattr $C$DW$957, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$957, DW_AT_name("SCIRST")
	.dwattr $C$DW$957, DW_AT_TI_symbol_name("_SCIRST")
	.dwattr $C$DW$957, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$957, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$957, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$125


$C$DW$T$126	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$126, DW_AT_name("SCIFFTX_REG")
	.dwattr $C$DW$T$126, DW_AT_byte_size(0x01)
$C$DW$958	.dwtag  DW_TAG_member
	.dwattr $C$DW$958, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$958, DW_AT_name("all")
	.dwattr $C$DW$958, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$958, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$958, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$959	.dwtag  DW_TAG_member
	.dwattr $C$DW$959, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$959, DW_AT_name("bit")
	.dwattr $C$DW$959, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$959, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$959, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$126


$C$DW$T$127	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$127, DW_AT_name("SCIPRI_BITS")
	.dwattr $C$DW$T$127, DW_AT_byte_size(0x01)
$C$DW$960	.dwtag  DW_TAG_member
	.dwattr $C$DW$960, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$960, DW_AT_name("rsvd1")
	.dwattr $C$DW$960, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$960, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$960, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$960, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$961	.dwtag  DW_TAG_member
	.dwattr $C$DW$961, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$961, DW_AT_name("FREE")
	.dwattr $C$DW$961, DW_AT_TI_symbol_name("_FREE")
	.dwattr $C$DW$961, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$961, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$961, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$962	.dwtag  DW_TAG_member
	.dwattr $C$DW$962, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$962, DW_AT_name("SOFT")
	.dwattr $C$DW$962, DW_AT_TI_symbol_name("_SOFT")
	.dwattr $C$DW$962, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$962, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$962, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$963	.dwtag  DW_TAG_member
	.dwattr $C$DW$963, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$963, DW_AT_name("rsvd2")
	.dwattr $C$DW$963, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$963, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x03)
	.dwattr $C$DW$963, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$963, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$964	.dwtag  DW_TAG_member
	.dwattr $C$DW$964, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$964, DW_AT_name("rsvd3")
	.dwattr $C$DW$964, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$964, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$964, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$964, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$127


$C$DW$T$128	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$128, DW_AT_name("SCIPRI_REG")
	.dwattr $C$DW$T$128, DW_AT_byte_size(0x01)
$C$DW$965	.dwtag  DW_TAG_member
	.dwattr $C$DW$965, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$965, DW_AT_name("all")
	.dwattr $C$DW$965, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$965, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$965, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$966	.dwtag  DW_TAG_member
	.dwattr $C$DW$966, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$966, DW_AT_name("bit")
	.dwattr $C$DW$966, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$966, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$966, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$128


$C$DW$T$129	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$129, DW_AT_name("SCIRXBUF_BITS")
	.dwattr $C$DW$T$129, DW_AT_byte_size(0x01)
$C$DW$967	.dwtag  DW_TAG_member
	.dwattr $C$DW$967, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$967, DW_AT_name("RXDT")
	.dwattr $C$DW$967, DW_AT_TI_symbol_name("_RXDT")
	.dwattr $C$DW$967, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$967, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$967, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$968	.dwtag  DW_TAG_member
	.dwattr $C$DW$968, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$968, DW_AT_name("rsvd1")
	.dwattr $C$DW$968, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$968, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x06)
	.dwattr $C$DW$968, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$968, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$969	.dwtag  DW_TAG_member
	.dwattr $C$DW$969, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$969, DW_AT_name("SCIFFPE")
	.dwattr $C$DW$969, DW_AT_TI_symbol_name("_SCIFFPE")
	.dwattr $C$DW$969, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$969, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$969, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$970	.dwtag  DW_TAG_member
	.dwattr $C$DW$970, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$970, DW_AT_name("SCIFFFE")
	.dwattr $C$DW$970, DW_AT_TI_symbol_name("_SCIFFFE")
	.dwattr $C$DW$970, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$970, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$970, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$129


$C$DW$T$130	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$130, DW_AT_name("SCIRXBUF_REG")
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x01)
$C$DW$971	.dwtag  DW_TAG_member
	.dwattr $C$DW$971, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$971, DW_AT_name("all")
	.dwattr $C$DW$971, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$971, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$971, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$972	.dwtag  DW_TAG_member
	.dwattr $C$DW$972, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$972, DW_AT_name("bit")
	.dwattr $C$DW$972, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$972, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$972, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$130


$C$DW$T$131	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$131, DW_AT_name("SCIRXST_BITS")
	.dwattr $C$DW$T$131, DW_AT_byte_size(0x01)
$C$DW$973	.dwtag  DW_TAG_member
	.dwattr $C$DW$973, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$973, DW_AT_name("rsvd1")
	.dwattr $C$DW$973, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$973, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$973, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$973, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$974	.dwtag  DW_TAG_member
	.dwattr $C$DW$974, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$974, DW_AT_name("RXWAKE")
	.dwattr $C$DW$974, DW_AT_TI_symbol_name("_RXWAKE")
	.dwattr $C$DW$974, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$974, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$974, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$975	.dwtag  DW_TAG_member
	.dwattr $C$DW$975, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$975, DW_AT_name("PE")
	.dwattr $C$DW$975, DW_AT_TI_symbol_name("_PE")
	.dwattr $C$DW$975, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$975, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$975, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$976	.dwtag  DW_TAG_member
	.dwattr $C$DW$976, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$976, DW_AT_name("OE")
	.dwattr $C$DW$976, DW_AT_TI_symbol_name("_OE")
	.dwattr $C$DW$976, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$976, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$976, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$977	.dwtag  DW_TAG_member
	.dwattr $C$DW$977, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$977, DW_AT_name("FE")
	.dwattr $C$DW$977, DW_AT_TI_symbol_name("_FE")
	.dwattr $C$DW$977, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$977, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$977, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$978	.dwtag  DW_TAG_member
	.dwattr $C$DW$978, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$978, DW_AT_name("BRKDT")
	.dwattr $C$DW$978, DW_AT_TI_symbol_name("_BRKDT")
	.dwattr $C$DW$978, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$978, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$978, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$979	.dwtag  DW_TAG_member
	.dwattr $C$DW$979, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$979, DW_AT_name("RXRDY")
	.dwattr $C$DW$979, DW_AT_TI_symbol_name("_RXRDY")
	.dwattr $C$DW$979, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$979, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$979, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$980	.dwtag  DW_TAG_member
	.dwattr $C$DW$980, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$980, DW_AT_name("RXERROR")
	.dwattr $C$DW$980, DW_AT_TI_symbol_name("_RXERROR")
	.dwattr $C$DW$980, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$980, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$980, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$981	.dwtag  DW_TAG_member
	.dwattr $C$DW$981, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$981, DW_AT_name("rsvd2")
	.dwattr $C$DW$981, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$981, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$981, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$981, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$131


$C$DW$T$132	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$132, DW_AT_name("SCIRXST_REG")
	.dwattr $C$DW$T$132, DW_AT_byte_size(0x01)
$C$DW$982	.dwtag  DW_TAG_member
	.dwattr $C$DW$982, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$982, DW_AT_name("all")
	.dwattr $C$DW$982, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$982, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$982, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$983	.dwtag  DW_TAG_member
	.dwattr $C$DW$983, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$983, DW_AT_name("bit")
	.dwattr $C$DW$983, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$983, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$983, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$132


$C$DW$T$133	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$133, DW_AT_name("SCI_REGS")
	.dwattr $C$DW$T$133, DW_AT_byte_size(0x10)
$C$DW$984	.dwtag  DW_TAG_member
	.dwattr $C$DW$984, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$984, DW_AT_name("SCICCR")
	.dwattr $C$DW$984, DW_AT_TI_symbol_name("_SCICCR")
	.dwattr $C$DW$984, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$984, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$985	.dwtag  DW_TAG_member
	.dwattr $C$DW$985, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$985, DW_AT_name("SCICTL1")
	.dwattr $C$DW$985, DW_AT_TI_symbol_name("_SCICTL1")
	.dwattr $C$DW$985, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$985, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$986	.dwtag  DW_TAG_member
	.dwattr $C$DW$986, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$986, DW_AT_name("SCIHBAUD")
	.dwattr $C$DW$986, DW_AT_TI_symbol_name("_SCIHBAUD")
	.dwattr $C$DW$986, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$986, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$987	.dwtag  DW_TAG_member
	.dwattr $C$DW$987, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$987, DW_AT_name("SCILBAUD")
	.dwattr $C$DW$987, DW_AT_TI_symbol_name("_SCILBAUD")
	.dwattr $C$DW$987, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$987, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$988	.dwtag  DW_TAG_member
	.dwattr $C$DW$988, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$988, DW_AT_name("SCICTL2")
	.dwattr $C$DW$988, DW_AT_TI_symbol_name("_SCICTL2")
	.dwattr $C$DW$988, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$988, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$989	.dwtag  DW_TAG_member
	.dwattr $C$DW$989, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$989, DW_AT_name("SCIRXST")
	.dwattr $C$DW$989, DW_AT_TI_symbol_name("_SCIRXST")
	.dwattr $C$DW$989, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$989, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$990	.dwtag  DW_TAG_member
	.dwattr $C$DW$990, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$990, DW_AT_name("SCIRXEMU")
	.dwattr $C$DW$990, DW_AT_TI_symbol_name("_SCIRXEMU")
	.dwattr $C$DW$990, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$990, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$991	.dwtag  DW_TAG_member
	.dwattr $C$DW$991, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$991, DW_AT_name("SCIRXBUF")
	.dwattr $C$DW$991, DW_AT_TI_symbol_name("_SCIRXBUF")
	.dwattr $C$DW$991, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$991, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$992	.dwtag  DW_TAG_member
	.dwattr $C$DW$992, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$992, DW_AT_name("rsvd1")
	.dwattr $C$DW$992, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$992, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$992, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$993	.dwtag  DW_TAG_member
	.dwattr $C$DW$993, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$993, DW_AT_name("SCITXBUF")
	.dwattr $C$DW$993, DW_AT_TI_symbol_name("_SCITXBUF")
	.dwattr $C$DW$993, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$993, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$994	.dwtag  DW_TAG_member
	.dwattr $C$DW$994, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$994, DW_AT_name("SCIFFTX")
	.dwattr $C$DW$994, DW_AT_TI_symbol_name("_SCIFFTX")
	.dwattr $C$DW$994, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$994, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$995	.dwtag  DW_TAG_member
	.dwattr $C$DW$995, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$995, DW_AT_name("SCIFFRX")
	.dwattr $C$DW$995, DW_AT_TI_symbol_name("_SCIFFRX")
	.dwattr $C$DW$995, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$995, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$996	.dwtag  DW_TAG_member
	.dwattr $C$DW$996, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$996, DW_AT_name("SCIFFCT")
	.dwattr $C$DW$996, DW_AT_TI_symbol_name("_SCIFFCT")
	.dwattr $C$DW$996, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$996, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$997	.dwtag  DW_TAG_member
	.dwattr $C$DW$997, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$997, DW_AT_name("rsvd2")
	.dwattr $C$DW$997, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$997, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$997, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$998	.dwtag  DW_TAG_member
	.dwattr $C$DW$998, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$998, DW_AT_name("rsvd3")
	.dwattr $C$DW$998, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$998, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$998, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$999	.dwtag  DW_TAG_member
	.dwattr $C$DW$999, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$999, DW_AT_name("SCIPRI")
	.dwattr $C$DW$999, DW_AT_TI_symbol_name("_SCIPRI")
	.dwattr $C$DW$999, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$999, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$133

$C$DW$1000	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1000, DW_AT_type(*$C$DW$T$133)
$C$DW$T$176	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$176, DW_AT_type(*$C$DW$1000)
$C$DW$T$177	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$177, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$T$177, DW_AT_address_class(0x16)

$C$DW$T$135	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$135, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$135, DW_AT_byte_size(0x10)
$C$DW$1001	.dwtag  DW_TAG_member
	.dwattr $C$DW$1001, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$1001, DW_AT_name("job")
	.dwattr $C$DW$1001, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$1001, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1001, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1002	.dwtag  DW_TAG_member
	.dwattr $C$DW$1002, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$1002, DW_AT_name("count")
	.dwattr $C$DW$1002, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$1002, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1002, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1003	.dwtag  DW_TAG_member
	.dwattr $C$DW$1003, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$1003, DW_AT_name("pendQ")
	.dwattr $C$DW$1003, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$1003, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$1003, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1004	.dwtag  DW_TAG_member
	.dwattr $C$DW$1004, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$1004, DW_AT_name("name")
	.dwattr $C$DW$1004, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$1004, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$1004, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$135

$C$DW$T$75	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$T$75, DW_AT_language(DW_LANG_C)
$C$DW$T$179	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$179, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$T$179, DW_AT_address_class(0x16)
$C$DW$T$180	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$180, DW_AT_type(*$C$DW$T$179)
	.dwattr $C$DW$T$180, DW_AT_language(DW_LANG_C)

$C$DW$T$138	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$138, DW_AT_name("SYS_CTRL_REGS")
	.dwattr $C$DW$T$138, DW_AT_byte_size(0x30)
$C$DW$1005	.dwtag  DW_TAG_member
	.dwattr $C$DW$1005, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$1005, DW_AT_name("XCLK")
	.dwattr $C$DW$1005, DW_AT_TI_symbol_name("_XCLK")
	.dwattr $C$DW$1005, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1005, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1006	.dwtag  DW_TAG_member
	.dwattr $C$DW$1006, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$1006, DW_AT_name("PLLSTS")
	.dwattr $C$DW$1006, DW_AT_TI_symbol_name("_PLLSTS")
	.dwattr $C$DW$1006, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1006, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1007	.dwtag  DW_TAG_member
	.dwattr $C$DW$1007, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$1007, DW_AT_name("CLKCTL")
	.dwattr $C$DW$1007, DW_AT_TI_symbol_name("_CLKCTL")
	.dwattr $C$DW$1007, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1007, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1008	.dwtag  DW_TAG_member
	.dwattr $C$DW$1008, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1008, DW_AT_name("PLLLOCKPRD")
	.dwattr $C$DW$1008, DW_AT_TI_symbol_name("_PLLLOCKPRD")
	.dwattr $C$DW$1008, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1008, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1009	.dwtag  DW_TAG_member
	.dwattr $C$DW$1009, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$1009, DW_AT_name("INTOSC1TRIM")
	.dwattr $C$DW$1009, DW_AT_TI_symbol_name("_INTOSC1TRIM")
	.dwattr $C$DW$1009, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1009, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1010	.dwtag  DW_TAG_member
	.dwattr $C$DW$1010, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1010, DW_AT_name("rsvd1")
	.dwattr $C$DW$1010, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1010, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$1010, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1011	.dwtag  DW_TAG_member
	.dwattr $C$DW$1011, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$1011, DW_AT_name("INTOSC2TRIM")
	.dwattr $C$DW$1011, DW_AT_TI_symbol_name("_INTOSC2TRIM")
	.dwattr $C$DW$1011, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1011, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1012	.dwtag  DW_TAG_member
	.dwattr $C$DW$1012, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$1012, DW_AT_name("rsvd2")
	.dwattr $C$DW$1012, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$1012, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$1012, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1013	.dwtag  DW_TAG_member
	.dwattr $C$DW$1013, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$1013, DW_AT_name("PCLKCR2")
	.dwattr $C$DW$1013, DW_AT_TI_symbol_name("_PCLKCR2")
	.dwattr $C$DW$1013, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$1013, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1014	.dwtag  DW_TAG_member
	.dwattr $C$DW$1014, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1014, DW_AT_name("rsvd3")
	.dwattr $C$DW$1014, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$1014, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$1014, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1015	.dwtag  DW_TAG_member
	.dwattr $C$DW$1015, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$1015, DW_AT_name("LOSPCP")
	.dwattr $C$DW$1015, DW_AT_TI_symbol_name("_LOSPCP")
	.dwattr $C$DW$1015, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$1015, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1016	.dwtag  DW_TAG_member
	.dwattr $C$DW$1016, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$1016, DW_AT_name("PCLKCR0")
	.dwattr $C$DW$1016, DW_AT_TI_symbol_name("_PCLKCR0")
	.dwattr $C$DW$1016, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1016, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1017	.dwtag  DW_TAG_member
	.dwattr $C$DW$1017, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$1017, DW_AT_name("PCLKCR1")
	.dwattr $C$DW$1017, DW_AT_TI_symbol_name("_PCLKCR1")
	.dwattr $C$DW$1017, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$1017, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1018	.dwtag  DW_TAG_member
	.dwattr $C$DW$1018, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$1018, DW_AT_name("LPMCR0")
	.dwattr $C$DW$1018, DW_AT_TI_symbol_name("_LPMCR0")
	.dwattr $C$DW$1018, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$1018, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1019	.dwtag  DW_TAG_member
	.dwattr $C$DW$1019, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1019, DW_AT_name("rsvd4")
	.dwattr $C$DW$1019, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$1019, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$1019, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1020	.dwtag  DW_TAG_member
	.dwattr $C$DW$1020, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$1020, DW_AT_name("PCLKCR3")
	.dwattr $C$DW$1020, DW_AT_TI_symbol_name("_PCLKCR3")
	.dwattr $C$DW$1020, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1020, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1021	.dwtag  DW_TAG_member
	.dwattr $C$DW$1021, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$1021, DW_AT_name("PLLCR")
	.dwattr $C$DW$1021, DW_AT_TI_symbol_name("_PLLCR")
	.dwattr $C$DW$1021, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$1021, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1022	.dwtag  DW_TAG_member
	.dwattr $C$DW$1022, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1022, DW_AT_name("SCSR")
	.dwattr $C$DW$1022, DW_AT_TI_symbol_name("_SCSR")
	.dwattr $C$DW$1022, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$1022, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1023	.dwtag  DW_TAG_member
	.dwattr $C$DW$1023, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1023, DW_AT_name("WDCNTR")
	.dwattr $C$DW$1023, DW_AT_TI_symbol_name("_WDCNTR")
	.dwattr $C$DW$1023, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$1023, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1024	.dwtag  DW_TAG_member
	.dwattr $C$DW$1024, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1024, DW_AT_name("rsvd5")
	.dwattr $C$DW$1024, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$1024, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1024, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1025	.dwtag  DW_TAG_member
	.dwattr $C$DW$1025, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1025, DW_AT_name("WDKEY")
	.dwattr $C$DW$1025, DW_AT_TI_symbol_name("_WDKEY")
	.dwattr $C$DW$1025, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$1025, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1026	.dwtag  DW_TAG_member
	.dwattr $C$DW$1026, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$1026, DW_AT_name("rsvd6")
	.dwattr $C$DW$1026, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$1026, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$1026, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1027	.dwtag  DW_TAG_member
	.dwattr $C$DW$1027, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1027, DW_AT_name("WDCR")
	.dwattr $C$DW$1027, DW_AT_TI_symbol_name("_WDCR")
	.dwattr $C$DW$1027, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$1027, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1028	.dwtag  DW_TAG_member
	.dwattr $C$DW$1028, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$1028, DW_AT_name("JTAGDEBUG")
	.dwattr $C$DW$1028, DW_AT_TI_symbol_name("_JTAGDEBUG")
	.dwattr $C$DW$1028, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$1028, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1029	.dwtag  DW_TAG_member
	.dwattr $C$DW$1029, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$1029, DW_AT_name("rsvd7")
	.dwattr $C$DW$1029, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$1029, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b]
	.dwattr $C$DW$1029, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1030	.dwtag  DW_TAG_member
	.dwattr $C$DW$1030, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$1030, DW_AT_name("PLL2CTL")
	.dwattr $C$DW$1030, DW_AT_TI_symbol_name("_PLL2CTL")
	.dwattr $C$DW$1030, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1030, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1031	.dwtag  DW_TAG_member
	.dwattr $C$DW$1031, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1031, DW_AT_name("rsvd8")
	.dwattr $C$DW$1031, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$1031, DW_AT_data_member_location[DW_OP_plus_uconst 0x21]
	.dwattr $C$DW$1031, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1032	.dwtag  DW_TAG_member
	.dwattr $C$DW$1032, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$1032, DW_AT_name("PLL2MULT")
	.dwattr $C$DW$1032, DW_AT_TI_symbol_name("_PLL2MULT")
	.dwattr $C$DW$1032, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$1032, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1033	.dwtag  DW_TAG_member
	.dwattr $C$DW$1033, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1033, DW_AT_name("rsvd9")
	.dwattr $C$DW$1033, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$1033, DW_AT_data_member_location[DW_OP_plus_uconst 0x23]
	.dwattr $C$DW$1033, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1034	.dwtag  DW_TAG_member
	.dwattr $C$DW$1034, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$1034, DW_AT_name("PLL2STS")
	.dwattr $C$DW$1034, DW_AT_TI_symbol_name("_PLL2STS")
	.dwattr $C$DW$1034, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1034, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1035	.dwtag  DW_TAG_member
	.dwattr $C$DW$1035, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1035, DW_AT_name("rsvd10")
	.dwattr $C$DW$1035, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$1035, DW_AT_data_member_location[DW_OP_plus_uconst 0x25]
	.dwattr $C$DW$1035, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1036	.dwtag  DW_TAG_member
	.dwattr $C$DW$1036, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1036, DW_AT_name("SYSCLK2CNTR")
	.dwattr $C$DW$1036, DW_AT_TI_symbol_name("_SYSCLK2CNTR")
	.dwattr $C$DW$1036, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$1036, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1037	.dwtag  DW_TAG_member
	.dwattr $C$DW$1037, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$1037, DW_AT_name("rsvd11")
	.dwattr $C$DW$1037, DW_AT_TI_symbol_name("_rsvd11")
	.dwattr $C$DW$1037, DW_AT_data_member_location[DW_OP_plus_uconst 0x27]
	.dwattr $C$DW$1037, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1038	.dwtag  DW_TAG_member
	.dwattr $C$DW$1038, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$1038, DW_AT_name("EPWMCFG")
	.dwattr $C$DW$1038, DW_AT_TI_symbol_name("_EPWMCFG")
	.dwattr $C$DW$1038, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$1038, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1039	.dwtag  DW_TAG_member
	.dwattr $C$DW$1039, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$1039, DW_AT_name("rsvd12")
	.dwattr $C$DW$1039, DW_AT_TI_symbol_name("_rsvd12")
	.dwattr $C$DW$1039, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b]
	.dwattr $C$DW$1039, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$138

$C$DW$1040	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1040, DW_AT_type(*$C$DW$T$138)
$C$DW$T$181	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$181, DW_AT_type(*$C$DW$1040)

$C$DW$T$139	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$139, DW_AT_name("XCLK_BITS")
	.dwattr $C$DW$T$139, DW_AT_byte_size(0x01)
$C$DW$1041	.dwtag  DW_TAG_member
	.dwattr $C$DW$1041, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1041, DW_AT_name("XCLKOUTDIV")
	.dwattr $C$DW$1041, DW_AT_TI_symbol_name("_XCLKOUTDIV")
	.dwattr $C$DW$1041, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$1041, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1041, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1042	.dwtag  DW_TAG_member
	.dwattr $C$DW$1042, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1042, DW_AT_name("rsvd1")
	.dwattr $C$DW$1042, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$1042, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x04)
	.dwattr $C$DW$1042, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1042, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1043	.dwtag  DW_TAG_member
	.dwattr $C$DW$1043, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1043, DW_AT_name("XCLKINSEL")
	.dwattr $C$DW$1043, DW_AT_TI_symbol_name("_XCLKINSEL")
	.dwattr $C$DW$1043, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$1043, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1043, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1044	.dwtag  DW_TAG_member
	.dwattr $C$DW$1044, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1044, DW_AT_name("rsvd2")
	.dwattr $C$DW$1044, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$1044, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x09)
	.dwattr $C$DW$1044, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1044, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$139


$C$DW$T$140	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$140, DW_AT_name("XCLK_REG")
	.dwattr $C$DW$T$140, DW_AT_byte_size(0x01)
$C$DW$1045	.dwtag  DW_TAG_member
	.dwattr $C$DW$1045, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$1045, DW_AT_name("all")
	.dwattr $C$DW$1045, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$1045, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1045, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1046	.dwtag  DW_TAG_member
	.dwattr $C$DW$1046, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$1046, DW_AT_name("bit")
	.dwattr $C$DW$1046, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$1046, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1046, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$140

$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$182	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$182, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$182, DW_AT_language(DW_LANG_C)

$C$DW$T$67	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$1047	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1047, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$T$67

$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x16)
$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)

$C$DW$T$99	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
$C$DW$T$100	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$100, DW_AT_address_class(0x16)
$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("PINT")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x46)
$C$DW$1048	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1048, DW_AT_upper_bound(0x45)
	.dwendtag $C$DW$T$19

$C$DW$T$193	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$193, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$193, DW_AT_address_class(0x16)
$C$DW$1049	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1049, DW_AT_type(*$C$DW$T$6)
$C$DW$T$195	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$195, DW_AT_type(*$C$DW$1049)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)

$C$DW$T$199	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$199, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$199, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$199, DW_AT_byte_size(0x17)
$C$DW$1050	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1050, DW_AT_upper_bound(0x16)
	.dwendtag $C$DW$T$199

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$201	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$201, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$201, DW_AT_language(DW_LANG_C)
$C$DW$T$186	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$186, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$186, DW_AT_address_class(0x16)

$C$DW$T$212	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$212, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$212, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$212, DW_AT_byte_size(0x02)
$C$DW$1051	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1051, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$212


$C$DW$T$213	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$213, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$213, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$213, DW_AT_byte_size(0x04)
$C$DW$1052	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1052, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$213

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)

$C$DW$T$51	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$51, DW_AT_byte_size(0x18)
$C$DW$1053	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1053, DW_AT_upper_bound(0x17)
	.dwendtag $C$DW$T$51


$C$DW$T$52	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x02)
$C$DW$1054	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1054, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$52


$C$DW$T$53	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x04)
$C$DW$1055	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1055, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$53


$C$DW$T$55	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x08)
$C$DW$1056	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1056, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$55


$C$DW$T$136	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$136, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$136, DW_AT_byte_size(0x03)
$C$DW$1057	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1057, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$136


$C$DW$T$137	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$137, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$137, DW_AT_byte_size(0x05)
$C$DW$1058	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1058, DW_AT_upper_bound(0x04)
	.dwendtag $C$DW$T$137

$C$DW$T$77	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$77, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)

$C$DW$T$219	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$219, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$219, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$219, DW_AT_byte_size(0x2e)
$C$DW$1059	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1059, DW_AT_upper_bound(0x16)
	.dwendtag $C$DW$T$219

$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("Uint32")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$76	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$1060	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1060, DW_AT_type(*$C$DW$T$16)
$C$DW$T$221	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$221, DW_AT_type(*$C$DW$1060)

$C$DW$T$222	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$222, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$222, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$222, DW_AT_byte_size(0x2e)
$C$DW$1061	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1061, DW_AT_upper_bound(0x16)
	.dwendtag $C$DW$T$222

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$79	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$79, DW_AT_address_class(0x16)
$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$1062	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$1062, DW_AT_location[DW_OP_reg0]
$C$DW$1063	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$1063, DW_AT_location[DW_OP_reg1]
$C$DW$1064	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$1064, DW_AT_location[DW_OP_reg2]
$C$DW$1065	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$1065, DW_AT_location[DW_OP_reg3]
$C$DW$1066	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$1066, DW_AT_location[DW_OP_reg20]
$C$DW$1067	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$1067, DW_AT_location[DW_OP_reg21]
$C$DW$1068	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$1068, DW_AT_location[DW_OP_reg22]
$C$DW$1069	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$1069, DW_AT_location[DW_OP_reg23]
$C$DW$1070	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$1070, DW_AT_location[DW_OP_reg24]
$C$DW$1071	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$1071, DW_AT_location[DW_OP_reg25]
$C$DW$1072	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$1072, DW_AT_location[DW_OP_reg26]
$C$DW$1073	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$1073, DW_AT_location[DW_OP_reg28]
$C$DW$1074	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$1074, DW_AT_location[DW_OP_reg29]
$C$DW$1075	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$1075, DW_AT_location[DW_OP_reg30]
$C$DW$1076	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$1076, DW_AT_location[DW_OP_reg31]
$C$DW$1077	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$1077, DW_AT_location[DW_OP_regx 0x20]
$C$DW$1078	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$1078, DW_AT_location[DW_OP_regx 0x21]
$C$DW$1079	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$1079, DW_AT_location[DW_OP_regx 0x22]
$C$DW$1080	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$1080, DW_AT_location[DW_OP_regx 0x23]
$C$DW$1081	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$1081, DW_AT_location[DW_OP_regx 0x24]
$C$DW$1082	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$1082, DW_AT_location[DW_OP_regx 0x25]
$C$DW$1083	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$1083, DW_AT_location[DW_OP_regx 0x26]
$C$DW$1084	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$1084, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$1085	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$1085, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$1086	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$1086, DW_AT_location[DW_OP_reg4]
$C$DW$1087	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$1087, DW_AT_location[DW_OP_reg6]
$C$DW$1088	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$1088, DW_AT_location[DW_OP_reg8]
$C$DW$1089	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$1089, DW_AT_location[DW_OP_reg10]
$C$DW$1090	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$1090, DW_AT_location[DW_OP_reg12]
$C$DW$1091	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$1091, DW_AT_location[DW_OP_reg14]
$C$DW$1092	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$1092, DW_AT_location[DW_OP_reg16]
$C$DW$1093	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$1093, DW_AT_location[DW_OP_reg17]
$C$DW$1094	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$1094, DW_AT_location[DW_OP_reg18]
$C$DW$1095	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$1095, DW_AT_location[DW_OP_reg19]
$C$DW$1096	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$1096, DW_AT_location[DW_OP_reg5]
$C$DW$1097	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$1097, DW_AT_location[DW_OP_reg7]
$C$DW$1098	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$1098, DW_AT_location[DW_OP_reg9]
$C$DW$1099	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$1099, DW_AT_location[DW_OP_reg11]
$C$DW$1100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$1100, DW_AT_location[DW_OP_reg13]
$C$DW$1101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$1101, DW_AT_location[DW_OP_reg15]
$C$DW$1102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$1102, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$1103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$1103, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$1104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$1104, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$1105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$1105, DW_AT_location[DW_OP_regx 0x30]
$C$DW$1106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$1106, DW_AT_location[DW_OP_regx 0x33]
$C$DW$1107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$1107, DW_AT_location[DW_OP_regx 0x34]
$C$DW$1108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$1108, DW_AT_location[DW_OP_regx 0x37]
$C$DW$1109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$1109, DW_AT_location[DW_OP_regx 0x38]
$C$DW$1110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$1110, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$1111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$1111, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$1112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$1112, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$1113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$1113, DW_AT_location[DW_OP_regx 0x40]
$C$DW$1114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$1114, DW_AT_location[DW_OP_regx 0x43]
$C$DW$1115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$1115, DW_AT_location[DW_OP_regx 0x44]
$C$DW$1116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$1116, DW_AT_location[DW_OP_regx 0x47]
$C$DW$1117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$1117, DW_AT_location[DW_OP_regx 0x48]
$C$DW$1118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$1118, DW_AT_location[DW_OP_regx 0x49]
$C$DW$1119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$1119, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$1120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$1120, DW_AT_location[DW_OP_regx 0x27]
$C$DW$1121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$1121, DW_AT_location[DW_OP_regx 0x28]
$C$DW$1122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$1122, DW_AT_location[DW_OP_reg27]
$C$DW$1123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$1123, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

