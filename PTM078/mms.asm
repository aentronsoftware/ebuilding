;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Tue May 25 14:03:54 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../mms.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\EBuilding\PTM078")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestCurrentEnable+0,32
	.bits	0,16			; _TestCurrentEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_EventResetVoltL+0,32
	.bits	1,16			; _EventResetVoltL @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_InitOK+0,32
	.bits	0,16			; _InitOK @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestPositionEnable+0,32
	.bits	1,16			; _TestPositionEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ReceiveNew+0,32
	.bits	0,16			; _ReceiveNew @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_UnderCurrent+0,32
	.bits	0,16			; _UnderCurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SequenceRunning+0,32
	.bits	0,16			; _SequenceRunning @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_current_counter+0,32
	.bits	0,32			; _GV_current_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_Delta_Counter+0,32
	.bits	0,32			; _GV_Delta_Counter @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_GetLogNB")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_PAR_GetLogNB")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddLog")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_PAR_AddLog")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("genCRC32Table")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_genCRC32Table")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external

$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_SetParamDependantVars")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external

$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("WARN_ErrorParam")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_WARN_ErrorParam")
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external

$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorDigOvld")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_ERR_ErrorDigOvld")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external

$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorUnderVoltage")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverCurrent")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Unlock")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_USB_Unlock")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external

$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_HandleWarning")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_ERR_HandleWarning")
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$11


$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Random")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_HAL_Random")
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external

$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Unlock")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_HAL_Unlock")
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Stop")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_USB_Stop")
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external

$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Reset")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_HAL_Reset")
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external

$C$DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("setNodeId")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_setNodeId")
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$90)
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$17


$C$DW$20	.dwtag  DW_TAG_subprogram, DW_AT_name("REC_StartRecorder")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_REC_StartRecorder")
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Init")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_HAL_Init")
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external

$C$DW$22	.dwtag  DW_TAG_subprogram, DW_AT_name("canInit")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_canInit")
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$9)
$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$22


$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("RS232ReceiveEnable")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_RS232ReceiveEnable")
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external

$C$DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_Command")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_SCI1_Command")
	.dwattr $C$DW$26, DW_AT_declaration
	.dwattr $C$DW$26, DW_AT_external

$C$DW$27	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_Receive")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_SCI1_Receive")
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_TimeOut")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ODP_CommError_TimeOut")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Low_Voltage_Current_Allow")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Low_Voltage_Current_Allow")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("ODV_CommError_Set")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_ODV_CommError_Set")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$30, DW_AT_declaration
	.dwattr $C$DW$30, DW_AT_external
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Temp_Delay")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Temp_Delay")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$31, DW_AT_declaration
	.dwattr $C$DW$31, DW_AT_external
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Current_C_D_Mode")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_ODP_Current_C_D_Mode")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$32, DW_AT_declaration
	.dwattr $C$DW$32, DW_AT_external
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineMode")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_ODV_MachineMode")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_DischargeAllowed")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ODV_Current_DischargeAllowed")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Low_Voltage_Charge_Delay")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Low_Voltage_Charge_Delay")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Low_Voltage_Current_Delay")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Low_Voltage_Current_Delay")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_ChargeAllowed")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ODV_Current_ChargeAllowed")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$37, DW_AT_declaration
	.dwattr $C$DW$37, DW_AT_external
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineEvent")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_ODV_MachineEvent")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_SleepTimeout")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_ODP_SafetyLimits_SleepTimeout")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$39, DW_AT_declaration
	.dwattr $C$DW$39, DW_AT_external
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_LogNB")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ODV_Gateway_LogNB")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$42, DW_AT_declaration
	.dwattr $C$DW$42, DW_AT_external
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SciSend")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_ODV_SciSend")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Variables")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_ODV_Recorder_Variables")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$44, DW_AT_declaration
	.dwattr $C$DW$44, DW_AT_external
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$46, DW_AT_declaration
	.dwattr $C$DW$46, DW_AT_external
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$47, DW_AT_declaration
	.dwattr $C$DW$47, DW_AT_external
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$48, DW_AT_declaration
	.dwattr $C$DW$48, DW_AT_external
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_State")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_ODV_Gateway_State")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$51, DW_AT_declaration
	.dwattr $C$DW$51, DW_AT_external
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MaxDeltaVoltage")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_ODV_Module1_MaxDeltaVoltage")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MaxCellVoltage")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_ODV_Module1_MaxCellVoltage")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$54, DW_AT_declaration
	.dwattr $C$DW$54, DW_AT_external
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Controlword")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_ODV_Controlword")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Current")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_ODV_Module1_Current")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$56, DW_AT_declaration
	.dwattr $C$DW$56, DW_AT_external
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_SOC")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ODV_Module1_SOC")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$57, DW_AT_declaration
	.dwattr $C$DW$57, DW_AT_external
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Debug")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_ODV_Debug")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$58, DW_AT_declaration
	.dwattr $C$DW$58, DW_AT_external
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("SCI_MsgAvailable")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_SCI_MsgAvailable")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$59, DW_AT_declaration
	.dwattr $C$DW$59, DW_AT_external
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("HAL_Enable")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_HAL_Enable")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$60, DW_AT_declaration
	.dwattr $C$DW$60, DW_AT_external
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("SCI_Available")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_SCI_Available")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$61, DW_AT_declaration
	.dwattr $C$DW$61, DW_AT_external
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("HAL_NewCurPoint")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_HAL_NewCurPoint")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$62, DW_AT_declaration
	.dwattr $C$DW$62, DW_AT_external
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MinCellVoltage")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ODV_Module1_MinCellVoltage")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$63, DW_AT_declaration
	.dwattr $C$DW$63, DW_AT_external
	.global	_TestCurrentEnable
_TestCurrentEnable:	.usect	".ebss",1,1,0
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("TestCurrentEnable")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_TestCurrentEnable")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_addr _TestCurrentEnable]
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$64, DW_AT_external
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SleepCurrent")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_ODP_SleepCurrent")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$65, DW_AT_declaration
	.dwattr $C$DW$65, DW_AT_external
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_BaudRate")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ODP_Board_BaudRate")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$66, DW_AT_declaration
	.dwattr $C$DW$66, DW_AT_external
	.global	_EventResetVoltL
_EventResetVoltL:	.usect	".ebss",1,1,0
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("EventResetVoltL")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_EventResetVoltL")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_addr _EventResetVoltL]
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$67, DW_AT_external
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_LowVoltage_ErrCounter")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_ODP_CommError_LowVoltage_ErrCounter")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$68, DW_AT_declaration
	.dwattr $C$DW$68, DW_AT_external
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_Delay")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ODP_CommError_Delay")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_external
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Status")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_ODV_Module1_Status")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$70, DW_AT_declaration
	.dwattr $C$DW$70, DW_AT_external
	.global	_InitOK
_InitOK:	.usect	".ebss",1,1,0
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("InitOK")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_InitOK")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_addr _InitOK]
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$71, DW_AT_external
	.global	_TestPositionEnable
_TestPositionEnable:	.usect	".ebss",1,1,0
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("TestPositionEnable")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_TestPositionEnable")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_addr _TestPositionEnable]
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$72, DW_AT_external
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("HAL_CellOK")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_HAL_CellOK")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$73, DW_AT_declaration
	.dwattr $C$DW$73, DW_AT_external
	.global	_ReceiveNew
_ReceiveNew:	.usect	".ebss",1,1,0
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("ReceiveNew")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_ReceiveNew")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_addr _ReceiveNew]
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$74, DW_AT_external
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_Config")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_ODP_Board_Config")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$75, DW_AT_declaration
	.dwattr $C$DW$75, DW_AT_external
	.global	_UnderCurrent
_UnderCurrent:	.usect	".ebss",1,1,0
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("UnderCurrent")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_UnderCurrent")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_addr _UnderCurrent]
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$76, DW_AT_external
	.global	_SequenceRunning
_SequenceRunning:	.usect	".ebss",1,1,0
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("SequenceRunning")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_SequenceRunning")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_addr _SequenceRunning]
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$77, DW_AT_external

$C$DW$78	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_InitParam")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_PAR_InitParam")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$78, DW_AT_declaration
	.dwattr $C$DW$78, DW_AT_external
$C$DW$79	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$90)
	.dwendtag $C$DW$78


$C$DW$80	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_UpdateCode")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_PAR_UpdateCode")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$80, DW_AT_declaration
	.dwattr $C$DW$80, DW_AT_external
$C$DW$81	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$80


$C$DW$82	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearError")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_ERR_ClearError")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$82, DW_AT_declaration
	.dwattr $C$DW$82, DW_AT_external

$C$DW$83	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarning")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_ERR_ClearWarning")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$83, DW_AT_declaration
	.dwattr $C$DW$83, DW_AT_external

$C$DW$84	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_CommandNoWait")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_I2C_CommandNoWait")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$84, DW_AT_declaration
	.dwattr $C$DW$84, DW_AT_external
$C$DW$85	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$9)
$C$DW$86	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$44)
$C$DW$87	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$9)
$C$DW$88	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$9)
$C$DW$89	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$68)
	.dwendtag $C$DW$84


$C$DW$90	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Start")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_USB_Start")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$90, DW_AT_declaration
	.dwattr $C$DW$90, DW_AT_external

$C$DW$91	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$91, DW_AT_declaration
	.dwattr $C$DW$91, DW_AT_external
$C$DW$92	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$9)
$C$DW$93	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$44)
$C$DW$94	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$9)
$C$DW$95	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$91


$C$DW$96	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddMultiUnits")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$96, DW_AT_declaration
	.dwattr $C$DW$96, DW_AT_external
$C$DW$97	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$96

$C$DW$98	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Multiunits")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_ODV_Recorder_Multiunits")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$98, DW_AT_declaration
	.dwattr $C$DW$98, DW_AT_external

$C$DW$99	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$99, DW_AT_declaration
	.dwattr $C$DW$99, DW_AT_external
$C$DW$100	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$150)
$C$DW$101	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$157)
$C$DW$102	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$42)
	.dwendtag $C$DW$99

$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("ODP_VersionParameters")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_ODP_VersionParameters")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$103, DW_AT_declaration
	.dwattr $C$DW$103, DW_AT_external

$C$DW$104	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$104, DW_AT_declaration
	.dwattr $C$DW$104, DW_AT_external
$C$DW$105	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$150)
$C$DW$106	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$157)
$C$DW$107	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$42)
	.dwendtag $C$DW$104


$C$DW$108	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$108, DW_AT_declaration
	.dwattr $C$DW$108, DW_AT_external
$C$DW$109	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$156)
$C$DW$110	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$42)
	.dwendtag $C$DW$108


$C$DW$111	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddVariables")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_PAR_AddVariables")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$111, DW_AT_declaration
	.dwattr $C$DW$111, DW_AT_external
$C$DW$112	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$111


$C$DW$113	.dwtag  DW_TAG_subprogram, DW_AT_name("setState")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_setState")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$113, DW_AT_declaration
	.dwattr $C$DW$113, DW_AT_external
$C$DW$114	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$90)
$C$DW$115	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$87)
	.dwendtag $C$DW$113

$C$DW$116	.dwtag  DW_TAG_variable, DW_AT_name("TimeLogIndex")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_TimeLogIndex")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$116, DW_AT_declaration
	.dwattr $C$DW$116, DW_AT_external

$C$DW$117	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_Round")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_CNV_Round")
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$117, DW_AT_declaration
	.dwattr $C$DW$117, DW_AT_external
$C$DW$118	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$117

$C$DW$119	.dwtag  DW_TAG_variable, DW_AT_name("ODV_StoreParameters")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_ODV_StoreParameters")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$119, DW_AT_declaration
	.dwattr $C$DW$119, DW_AT_external
$C$DW$120	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RestoreDefaultParameters")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_ODV_RestoreDefaultParameters")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$120, DW_AT_declaration
	.dwattr $C$DW$120, DW_AT_external
$C$DW$121	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Capacity")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_ODP_Battery_Capacity")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$121, DW_AT_declaration
	.dwattr $C$DW$121, DW_AT_external
$C$DW$122	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RandomNB")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_ODP_RandomNB")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$122, DW_AT_declaration
	.dwattr $C$DW$122, DW_AT_external
$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SysTick_ms")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_ODV_SysTick_ms")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$123, DW_AT_declaration
	.dwattr $C$DW$123, DW_AT_external

$C$DW$124	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_StoreODSubIndex")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$124, DW_AT_declaration
	.dwattr $C$DW$124, DW_AT_external
$C$DW$125	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$90)
$C$DW$126	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$9)
$C$DW$127	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$124

$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("MMSConfig")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_MMSConfig")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$128, DW_AT_declaration
	.dwattr $C$DW$128, DW_AT_external

$C$DW$129	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteAllPermanentParam")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$129, DW_AT_declaration
	.dwattr $C$DW$129, DW_AT_external
$C$DW$130	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$90)
	.dwendtag $C$DW$129

	.global	_GV_current_counter
_GV_current_counter:	.usect	".ebss",2,1,1
$C$DW$131	.dwtag  DW_TAG_variable, DW_AT_name("GV_current_counter")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_GV_current_counter")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_addr _GV_current_counter]
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$131, DW_AT_external
$C$DW$132	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Total")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_PAR_Capacity_Total")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$132, DW_AT_declaration
	.dwattr $C$DW$132, DW_AT_external
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ResetHW")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_ODV_ResetHW")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$133, DW_AT_declaration
	.dwattr $C$DW$133, DW_AT_external
	.global	_BoardODdata
_BoardODdata:	.usect	".ebss",2,1,1
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_addr _BoardODdata]
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$134, DW_AT_external

$C$DW$135	.dwtag  DW_TAG_subprogram, DW_AT_name("getCRC32_cpu")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_getCRC32_cpu")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$135, DW_AT_declaration
	.dwattr $C$DW$135, DW_AT_external
$C$DW$136	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$13)
$C$DW$137	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$99)
$C$DW$138	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$141)
$C$DW$139	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$135


$C$DW$140	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteStatisticParam")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$140, DW_AT_declaration
	.dwattr $C$DW$140, DW_AT_external
$C$DW$141	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$8)
	.dwendtag $C$DW$140

$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$190)
	.dwattr $C$DW$142, DW_AT_declaration
	.dwattr $C$DW$142, DW_AT_external
	.global	_GV_Delta_Counter
_GV_Delta_Counter:	.usect	".ebss",2,1,1
$C$DW$143	.dwtag  DW_TAG_variable, DW_AT_name("GV_Delta_Counter")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_GV_Delta_Counter")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_addr _GV_Delta_Counter]
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$143, DW_AT_external
$C$DW$144	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Errorcode")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_ODV_Gateway_Errorcode")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$144, DW_AT_declaration
	.dwattr $C$DW$144, DW_AT_external
$C$DW$145	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$145, DW_AT_declaration
	.dwattr $C$DW$145, DW_AT_external
$C$DW$146	.dwtag  DW_TAG_variable, DW_AT_name("ODP_OnTime")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_ODP_OnTime")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$146, DW_AT_declaration
	.dwattr $C$DW$146, DW_AT_external
$C$DW$147	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$147, DW_AT_declaration
	.dwattr $C$DW$147, DW_AT_external
$C$DW$148	.dwtag  DW_TAG_variable, DW_AT_name("ODV_CommError_Counter")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_ODV_CommError_Counter")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$148, DW_AT_declaration
	.dwattr $C$DW$148, DW_AT_external
$C$DW$149	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Date_Time")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_ODV_Gateway_Date_Time")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$149, DW_AT_declaration
	.dwattr $C$DW$149, DW_AT_external
	.global	_BootST
_BootST:	.usect	"BootCommand",4,1,1
$C$DW$150	.dwtag  DW_TAG_variable, DW_AT_name("BootST")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_BootST")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_addr _BootST]
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$150, DW_AT_external
$C$DW$151	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$151, DW_AT_declaration
	.dwattr $C$DW$151, DW_AT_external
$C$DW$152	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RTC_Text")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_ODV_RTC_Text")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$152, DW_AT_declaration
	.dwattr $C$DW$152, DW_AT_external
$C$DW$153	.dwtag  DW_TAG_variable, DW_AT_name("golden_CRC_values")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_golden_CRC_values")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$205)
	.dwattr $C$DW$153, DW_AT_declaration
	.dwattr $C$DW$153, DW_AT_external
$C$DW$154	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$154, DW_AT_declaration
	.dwattr $C$DW$154, DW_AT_external
$C$DW$155	.dwtag  DW_TAG_variable, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$155, DW_AT_declaration
	.dwattr $C$DW$155, DW_AT_external
$C$DW$156	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$156, DW_AT_declaration
	.dwattr $C$DW$156, DW_AT_external
$C$DW$157	.dwtag  DW_TAG_variable, DW_AT_name("mailboxSDOout")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_mailboxSDOout")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$157, DW_AT_declaration
	.dwattr $C$DW$157, DW_AT_external
$C$DW$158	.dwtag  DW_TAG_variable, DW_AT_name("can_rx_mbox")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_can_rx_mbox")
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$158, DW_AT_declaration
	.dwattr $C$DW$158, DW_AT_external
$C$DW$159	.dwtag  DW_TAG_variable, DW_AT_name("sci_rx_mbox")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_sci_rx_mbox")
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$159, DW_AT_declaration
	.dwattr $C$DW$159, DW_AT_external
$C$DW$160	.dwtag  DW_TAG_variable, DW_AT_name("SCI_MsgInRS232")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_SCI_MsgInRS232")
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$160, DW_AT_declaration
	.dwattr $C$DW$160, DW_AT_external
$C$DW$161	.dwtag  DW_TAG_variable, DW_AT_name("ODI_mms_dict_Data")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_ODI_mms_dict_Data")
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$161, DW_AT_declaration
	.dwattr $C$DW$161, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1935212 
	.sect	".text"
	.global	_CheckfirmwareCRC

$C$DW$162	.dwtag  DW_TAG_subprogram, DW_AT_name("CheckfirmwareCRC")
	.dwattr $C$DW$162, DW_AT_low_pc(_CheckfirmwareCRC)
	.dwattr $C$DW$162, DW_AT_high_pc(0x00)
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_CheckfirmwareCRC")
	.dwattr $C$DW$162, DW_AT_external
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$162, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$162, DW_AT_TI_begin_line(0x41)
	.dwattr $C$DW$162, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$162, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../mms.c",line 66,column 1,is_stmt,address _CheckfirmwareCRC

	.dwfde $C$DW$CIE, _CheckfirmwareCRC

;***************************************************************
;* FNAME: _CheckfirmwareCRC             FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_CheckfirmwareCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$163	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$163, DW_AT_location[DW_OP_breg20 -2]
$C$DW$164	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$164, DW_AT_location[DW_OP_breg20 -4]
$C$DW$165	.dwtag  DW_TAG_variable, DW_AT_name("crc_rec")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_crc_rec")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_breg20 -12]
	.dwpsn	file "../mms.c",line 70,column 8,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |70| 
        B         $C$L3,UNC             ; [CPU_] |70| 
        ; branch occurs ; [] |70| 
$C$L1:    
	.dwpsn	file "../mms.c",line 72,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVZ      AR4,SP                ; [CPU_U] |72| 
        MOVL      XAR7,#_golden_CRC_values+2 ; [CPU_U] |72| 
        MOV       ACC,*-SP[2] << 3      ; [CPU_] |72| 
        SUBB      XAR4,#12              ; [CPU_U] |72| 
        ADDL      XAR7,ACC              ; [CPU_] |72| 
        RPT       #7
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |72| 
	.dwpsn	file "../mms.c",line 73,column 5,is_stmt
        MOV       ACC,*-SP[8] << #1     ; [CPU_] |73| 
        MOV       *-SP[1],AL            ; [CPU_] |73| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |73| 
        MOVB      XAR5,#0               ; [CPU_] |73| 
        MOVB      ACC,#0                ; [CPU_] |73| 
$C$DW$166	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$166, DW_AT_low_pc(0x00)
	.dwattr $C$DW$166, DW_AT_name("_getCRC32_cpu")
	.dwattr $C$DW$166, DW_AT_TI_call
        LCR       #_getCRC32_cpu        ; [CPU_] |73| 
        ; call occurs [#_getCRC32_cpu] ; [] |73| 
        MOVL      *-SP[4],ACC           ; [CPU_] |73| 
	.dwpsn	file "../mms.c",line 74,column 5,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |74| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |74| 
        BF        $C$L2,EQ              ; [CPU_] |74| 
        ; branchcc occurs ; [] |74| 
	.dwpsn	file "../mms.c",line 75,column 7,is_stmt
        MOVB      AL,#0                 ; [CPU_] |75| 
        B         $C$L4,UNC             ; [CPU_] |75| 
        ; branch occurs ; [] |75| 
$C$L2:    
	.dwpsn	file "../mms.c",line 70,column 47,is_stmt
        INC       *-SP[2]               ; [CPU_] |70| 
$C$L3:    
	.dwpsn	file "../mms.c",line 70,column 15,is_stmt
        MOVW      DP,#_golden_CRC_values+1 ; [CPU_U] 
        MOV       AL,@_golden_CRC_values+1 ; [CPU_] |70| 
        CMP       AL,*-SP[2]            ; [CPU_] |70| 
        B         $C$L1,HI              ; [CPU_] |70| 
        ; branchcc occurs ; [] |70| 
	.dwpsn	file "../mms.c",line 86,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |86| 
$C$L4:    
	.dwpsn	file "../mms.c",line 87,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$167	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$167, DW_AT_low_pc(0x00)
	.dwattr $C$DW$167, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$162, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$162, DW_AT_TI_end_line(0x57)
	.dwattr $C$DW$162, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$162

	.sect	".text"
	.global	_FnResetNode

$C$DW$168	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetNode")
	.dwattr $C$DW$168, DW_AT_low_pc(_FnResetNode)
	.dwattr $C$DW$168, DW_AT_high_pc(0x00)
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_FnResetNode")
	.dwattr $C$DW$168, DW_AT_external
	.dwattr $C$DW$168, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$168, DW_AT_TI_begin_line(0x5b)
	.dwattr $C$DW$168, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$168, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../mms.c",line 91,column 29,is_stmt,address _FnResetNode

	.dwfde $C$DW$CIE, _FnResetNode
$C$DW$169	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$169, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnResetNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$170	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |91| 
	.dwpsn	file "../mms.c",line 94,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$171	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$171, DW_AT_low_pc(0x00)
	.dwattr $C$DW$171, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$168, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$168, DW_AT_TI_end_line(0x5e)
	.dwattr $C$DW$168, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$168

	.sect	".text"
	.global	_FnResetCommunications

$C$DW$172	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetCommunications")
	.dwattr $C$DW$172, DW_AT_low_pc(_FnResetCommunications)
	.dwattr $C$DW$172, DW_AT_high_pc(0x00)
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_FnResetCommunications")
	.dwattr $C$DW$172, DW_AT_external
	.dwattr $C$DW$172, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$172, DW_AT_TI_begin_line(0x61)
	.dwattr $C$DW$172, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$172, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../mms.c",line 97,column 39,is_stmt,address _FnResetCommunications

	.dwfde $C$DW$CIE, _FnResetCommunications
$C$DW$173	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$173, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetCommunications        FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 19 Auto,  0 SOE     *
;***************************************************************

_FnResetCommunications:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$174	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$174, DW_AT_location[DW_OP_breg20 -2]
$C$DW$175	.dwtag  DW_TAG_variable, DW_AT_name("node_id")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$175, DW_AT_location[DW_OP_breg20 -3]
$C$DW$176	.dwtag  DW_TAG_variable, DW_AT_name("dummy_m")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_dummy_m")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$202)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_breg20 -19]
        MOVL      *-SP[2],XAR4          ; [CPU_] |97| 
	.dwpsn	file "../mms.c",line 102,column 3,is_stmt
$C$L5:    
        MOVZ      AR5,SP                ; [CPU_U] |102| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |102| 
        MOVB      AL,#0                 ; [CPU_] |102| 
        SUBB      XAR5,#19              ; [CPU_U] |102| 
$C$DW$177	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$177, DW_AT_low_pc(0x00)
	.dwattr $C$DW$177, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$177, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |102| 
        ; call occurs [#_MBX_pend] ; [] |102| 
        CMPB      AL,#0                 ; [CPU_] |102| 
        BF        $C$L5,NEQ             ; [CPU_] |102| 
        ; branchcc occurs ; [] |102| 
	.dwpsn	file "../mms.c",line 103,column 3,is_stmt
$C$L6:    
        MOVZ      AR5,SP                ; [CPU_U] |103| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |103| 
        MOVB      AL,#0                 ; [CPU_] |103| 
        SUBB      XAR5,#19              ; [CPU_U] |103| 
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$178, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |103| 
        ; call occurs [#_MBX_pend] ; [] |103| 
        CMPB      AL,#0                 ; [CPU_] |103| 
        BF        $C$L6,NEQ             ; [CPU_] |103| 
        ; branchcc occurs ; [] |103| 
	.dwpsn	file "../mms.c",line 104,column 3,is_stmt
$C$L7:    
        MOVZ      AR5,SP                ; [CPU_U] |104| 
        MOVL      XAR4,#_can_rx_mbox    ; [CPU_U] |104| 
        MOVB      AL,#0                 ; [CPU_] |104| 
        SUBB      XAR5,#19              ; [CPU_U] |104| 
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$179, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |104| 
        ; call occurs [#_MBX_pend] ; [] |104| 
        CMPB      AL,#0                 ; [CPU_] |104| 
        BF        $C$L7,NEQ             ; [CPU_] |104| 
        ; branchcc occurs ; [] |104| 
	.dwpsn	file "../mms.c",line 106,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOV       AL,@_ODP_Board_RevisionNumber ; [CPU_] |106| 
        MOV       *-SP[3],AL            ; [CPU_] |106| 
	.dwpsn	file "../mms.c",line 113,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |113| 
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("_setNodeId")
	.dwattr $C$DW$180, DW_AT_TI_call
        LCR       #_setNodeId           ; [CPU_] |113| 
        ; call occurs [#_setNodeId] ; [] |113| 
	.dwpsn	file "../mms.c",line 116,column 3,is_stmt
        MOV       AH,*-SP[3]            ; [CPU_] |116| 
        MOVW      DP,#_ODP_Board_BaudRate ; [CPU_U] 
        MOV       AL,@_ODP_Board_BaudRate ; [CPU_] |116| 
$C$DW$181	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$181, DW_AT_low_pc(0x00)
	.dwattr $C$DW$181, DW_AT_name("_canInit")
	.dwattr $C$DW$181, DW_AT_TI_call
        LCR       #_canInit             ; [CPU_] |116| 
        ; call occurs [#_canInit] ; [] |116| 
	.dwpsn	file "../mms.c",line 119,column 1,is_stmt
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$172, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$172, DW_AT_TI_end_line(0x77)
	.dwattr $C$DW$172, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$172

	.sect	".text"
	.global	_FnInitialiseNode

$C$DW$183	.dwtag  DW_TAG_subprogram, DW_AT_name("FnInitialiseNode")
	.dwattr $C$DW$183, DW_AT_low_pc(_FnInitialiseNode)
	.dwattr $C$DW$183, DW_AT_high_pc(0x00)
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_FnInitialiseNode")
	.dwattr $C$DW$183, DW_AT_external
	.dwattr $C$DW$183, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$183, DW_AT_TI_begin_line(0x7d)
	.dwattr $C$DW$183, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$183, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../mms.c",line 125,column 35,is_stmt,address _FnInitialiseNode

	.dwfde $C$DW$CIE, _FnInitialiseNode
$C$DW$184	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$184, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnInitialiseNode             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_FnInitialiseNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$185	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$185, DW_AT_location[DW_OP_breg20 -2]
$C$DW$186	.dwtag  DW_TAG_variable, DW_AT_name("CrcFirmwareTest")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_CrcFirmwareTest")
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$186, DW_AT_location[DW_OP_breg20 -3]
        MOVL      *-SP[2],XAR4          ; [CPU_] |125| 
	.dwpsn	file "../mms.c",line 126,column 26,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |126| 
	.dwpsn	file "../mms.c",line 128,column 3,is_stmt
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_name("_genCRC32Table")
	.dwattr $C$DW$187, DW_AT_TI_call
        LCR       #_genCRC32Table       ; [CPU_] |128| 
        ; call occurs [#_genCRC32Table] ; [] |128| 
	.dwpsn	file "../mms.c",line 130,column 3,is_stmt
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_name("_CheckfirmwareCRC")
	.dwattr $C$DW$188, DW_AT_TI_call
        LCR       #_CheckfirmwareCRC    ; [CPU_] |130| 
        ; call occurs [#_CheckfirmwareCRC] ; [] |130| 
        MOV       *-SP[3],AL            ; [CPU_] |130| 
	.dwpsn	file "../mms.c",line 131,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+19   ; [CPU_U] 
        OR        @_PieCtrlRegs+19,#0x0020 ; [CPU_] |131| 
	.dwpsn	file "../mms.c",line 132,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |132| 
	.dwpsn	file "../mms.c",line 133,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,#_ODP_Board_Config ; [CPU_U] |133| 
        MOVL      @_MMSConfig,XAR4      ; [CPU_] |133| 
	.dwpsn	file "../mms.c",line 134,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |134| 
$C$DW$189	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$189, DW_AT_low_pc(0x00)
	.dwattr $C$DW$189, DW_AT_name("_PAR_InitParam")
	.dwattr $C$DW$189, DW_AT_TI_call
        LCR       #_PAR_InitParam       ; [CPU_] |134| 
        ; call occurs [#_PAR_InitParam] ; [] |134| 
        CMPB      AL,#0                 ; [CPU_] |134| 
        BF        $C$L8,EQ              ; [CPU_] |134| 
        ; branchcc occurs ; [] |134| 
        MOV       AL,*-SP[3]            ; [CPU_] |134| 
        CMPB      AL,#1                 ; [CPU_] |134| 
        BF        $C$L8,NEQ             ; [CPU_] |134| 
        ; branchcc occurs ; [] |134| 
	.dwpsn	file "../mms.c",line 136,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#1,UNC       ; [CPU_] |136| 
	.dwpsn	file "../mms.c",line 137,column 3,is_stmt
        B         $C$L9,UNC             ; [CPU_] |137| 
        ; branch occurs ; [] |137| 
$C$L8:    
	.dwpsn	file "../mms.c",line 139,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#2,UNC       ; [CPU_] |139| 
	.dwpsn	file "../mms.c",line 140,column 5,is_stmt
$C$DW$190	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$190, DW_AT_low_pc(0x00)
	.dwattr $C$DW$190, DW_AT_name("_WARN_ErrorParam")
	.dwattr $C$DW$190, DW_AT_TI_call
        LCR       #_WARN_ErrorParam     ; [CPU_] |140| 
        ; call occurs [#_WARN_ErrorParam] ; [] |140| 
$C$L9:    
	.dwpsn	file "../mms.c",line 142,column 3,is_stmt
$C$DW$191	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$191, DW_AT_low_pc(0x00)
	.dwattr $C$DW$191, DW_AT_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$191, DW_AT_TI_call
        LCR       #_PAR_SetParamDependantVars ; [CPU_] |142| 
        ; call occurs [#_PAR_SetParamDependantVars] ; [] |142| 
	.dwpsn	file "../mms.c",line 143,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$192	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$192, DW_AT_low_pc(0x00)
	.dwattr $C$DW$192, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$183, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$183, DW_AT_TI_end_line(0x8f)
	.dwattr $C$DW$183, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$183

	.sect	".text"
	.global	_FnEnterPreOperational

$C$DW$193	.dwtag  DW_TAG_subprogram, DW_AT_name("FnEnterPreOperational")
	.dwattr $C$DW$193, DW_AT_low_pc(_FnEnterPreOperational)
	.dwattr $C$DW$193, DW_AT_high_pc(0x00)
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_FnEnterPreOperational")
	.dwattr $C$DW$193, DW_AT_external
	.dwattr $C$DW$193, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$193, DW_AT_TI_begin_line(0x93)
	.dwattr $C$DW$193, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$193, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../mms.c",line 147,column 40,is_stmt,address _FnEnterPreOperational

	.dwfde $C$DW$CIE, _FnEnterPreOperational
$C$DW$194	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnEnterPreOperational        FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnEnterPreOperational:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$195	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |147| 
	.dwpsn	file "../mms.c",line 148,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOVB      @_ODV_Controlword,#6,UNC ; [CPU_] |148| 
	.dwpsn	file "../mms.c",line 149,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$193, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$193, DW_AT_TI_end_line(0x95)
	.dwattr $C$DW$193, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$193

	.sect	".text"
	.global	_FnStartNode

$C$DW$197	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStartNode")
	.dwattr $C$DW$197, DW_AT_low_pc(_FnStartNode)
	.dwattr $C$DW$197, DW_AT_high_pc(0x00)
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_FnStartNode")
	.dwattr $C$DW$197, DW_AT_external
	.dwattr $C$DW$197, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$197, DW_AT_TI_begin_line(0x98)
	.dwattr $C$DW$197, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$197, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../mms.c",line 152,column 30,is_stmt,address _FnStartNode

	.dwfde $C$DW$CIE, _FnStartNode
$C$DW$198	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStartNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStartNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$199	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |152| 
	.dwpsn	file "../mms.c",line 154,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$200	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$200, DW_AT_low_pc(0x00)
	.dwattr $C$DW$200, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$197, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$197, DW_AT_TI_end_line(0x9a)
	.dwattr $C$DW$197, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$197

	.sect	".text"
	.global	_FnStopNode

$C$DW$201	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStopNode")
	.dwattr $C$DW$201, DW_AT_low_pc(_FnStopNode)
	.dwattr $C$DW$201, DW_AT_high_pc(0x00)
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_FnStopNode")
	.dwattr $C$DW$201, DW_AT_external
	.dwattr $C$DW$201, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$201, DW_AT_TI_begin_line(0x9d)
	.dwattr $C$DW$201, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$201, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../mms.c",line 157,column 29,is_stmt,address _FnStopNode

	.dwfde $C$DW$CIE, _FnStopNode
$C$DW$202	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$202, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStopNode                   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStopNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$203	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$203, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |157| 
	.dwpsn	file "../mms.c",line 159,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |159| 
	.dwpsn	file "../mms.c",line 160,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$201, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$201, DW_AT_TI_end_line(0xa0)
	.dwattr $C$DW$201, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$201

	.sect	".text"
	.global	_canOpenInit

$C$DW$205	.dwtag  DW_TAG_subprogram, DW_AT_name("canOpenInit")
	.dwattr $C$DW$205, DW_AT_low_pc(_canOpenInit)
	.dwattr $C$DW$205, DW_AT_high_pc(0x00)
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_canOpenInit")
	.dwattr $C$DW$205, DW_AT_external
	.dwattr $C$DW$205, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$205, DW_AT_TI_begin_line(0xa4)
	.dwattr $C$DW$205, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$205, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../mms.c",line 166,column 1,is_stmt,address _canOpenInit

	.dwfde $C$DW$CIE, _canOpenInit

;***************************************************************
;* FNAME: _canOpenInit                  FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_canOpenInit:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../mms.c",line 168,column 3,is_stmt
        MOVL      XAR4,#_ODI_mms_dict_Data ; [CPU_U] |168| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      @_BoardODdata,XAR4    ; [CPU_] |168| 
	.dwpsn	file "../mms.c",line 170,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |170| 
        MOVB      XAR0,#84              ; [CPU_] |170| 
        MOVL      XAR4,#_FnInitialiseNode ; [CPU_U] |170| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |170| 
	.dwpsn	file "../mms.c",line 171,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |171| 
        MOVB      XAR0,#86              ; [CPU_] |171| 
        MOVL      XAR4,#_FnEnterPreOperational ; [CPU_U] |171| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |171| 
	.dwpsn	file "../mms.c",line 172,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |172| 
        MOVB      XAR0,#88              ; [CPU_] |172| 
        MOVL      XAR4,#_FnStartNode    ; [CPU_U] |172| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |172| 
	.dwpsn	file "../mms.c",line 173,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |173| 
        MOVB      XAR0,#90              ; [CPU_] |173| 
        MOVL      XAR4,#_FnStopNode     ; [CPU_U] |173| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |173| 
	.dwpsn	file "../mms.c",line 174,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |174| 
        MOVB      XAR0,#92              ; [CPU_] |174| 
        MOVL      XAR4,#_FnResetNode    ; [CPU_U] |174| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |174| 
	.dwpsn	file "../mms.c",line 175,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |175| 
        MOVB      XAR0,#94              ; [CPU_] |175| 
        MOVL      XAR4,#_FnResetCommunications ; [CPU_U] |175| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |175| 
	.dwpsn	file "../mms.c",line 177,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |177| 
        MOVB      XAR0,#252             ; [CPU_] |177| 
        MOVL      XAR4,#_PAR_StoreODSubIndex ; [CPU_U] |177| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |177| 
	.dwpsn	file "../mms.c",line 179,column 3,is_stmt
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |179| 
        MOVB      ACC,#0                ; [CPU_] |179| 
        MOVB      XAR0,#254             ; [CPU_] |179| 
        MOVL      *+XAR4[AR0],ACC       ; [CPU_] |179| 
	.dwpsn	file "../mms.c",line 183,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |183| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |183| 
$C$DW$206	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$206, DW_AT_low_pc(0x00)
	.dwattr $C$DW$206, DW_AT_name("_setState")
	.dwattr $C$DW$206, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |183| 
        ; call occurs [#_setState] ; [] |183| 
	.dwpsn	file "../mms.c",line 184,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |184| 
$C$DW$207	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$207, DW_AT_low_pc(0x00)
	.dwattr $C$DW$207, DW_AT_name("_FnResetCommunications")
	.dwattr $C$DW$207, DW_AT_TI_call
        LCR       #_FnResetCommunications ; [CPU_] |184| 
        ; call occurs [#_FnResetCommunications] ; [] |184| 
	.dwpsn	file "../mms.c",line 191,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AL,#5                 ; [CPU_] |191| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |191| 
$C$DW$208	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$208, DW_AT_low_pc(0x00)
	.dwattr $C$DW$208, DW_AT_name("_setState")
	.dwattr $C$DW$208, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |191| 
        ; call occurs [#_setState] ; [] |191| 
	.dwpsn	file "../mms.c",line 193,column 1,is_stmt
$C$DW$209	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$209, DW_AT_low_pc(0x00)
	.dwattr $C$DW$209, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$205, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$205, DW_AT_TI_end_line(0xc1)
	.dwattr $C$DW$205, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$205

	.sect	".text"
	.global	_taskFnGestionMachine

$C$DW$210	.dwtag  DW_TAG_subprogram, DW_AT_name("taskFnGestionMachine")
	.dwattr $C$DW$210, DW_AT_low_pc(_taskFnGestionMachine)
	.dwattr $C$DW$210, DW_AT_high_pc(0x00)
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_taskFnGestionMachine")
	.dwattr $C$DW$210, DW_AT_external
	.dwattr $C$DW$210, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$210, DW_AT_TI_begin_line(0xce)
	.dwattr $C$DW$210, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$210, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../mms.c",line 206,column 32,is_stmt,address _taskFnGestionMachine

	.dwfde $C$DW$CIE, _taskFnGestionMachine

;***************************************************************
;* FNAME: _taskFnGestionMachine         FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_taskFnGestionMachine:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$211	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$211, DW_AT_location[DW_OP_breg20 -2]
$C$DW$212	.dwtag  DW_TAG_variable, DW_AT_name("statusi2c")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_statusi2c")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_breg20 -3]
$C$DW$213	.dwtag  DW_TAG_variable, DW_AT_name("old_time")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_old_time")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_breg20 -6]
$C$DW$214	.dwtag  DW_TAG_variable, DW_AT_name("count")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_breg20 -8]
	.dwpsn	file "../mms.c",line 207,column 13,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |207| 
	.dwpsn	file "../mms.c",line 207,column 28,is_stmt
        MOVB      *-SP[3],#20,UNC       ; [CPU_] |207| 
	.dwpsn	file "../mms.c",line 208,column 26,is_stmt
        MOVB      ACC,#0                ; [CPU_] |208| 
        MOVL      *-SP[8],ACC           ; [CPU_] |208| 
	.dwpsn	file "../mms.c",line 212,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |212| 
        MOVB      AL,#2                 ; [CPU_] |212| 
$C$DW$215	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$215, DW_AT_low_pc(0x00)
	.dwattr $C$DW$215, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$215, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |212| 
        ; call occurs [#_SEM_pend] ; [] |212| 
	.dwpsn	file "../mms.c",line 213,column 3,is_stmt
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_name("_HAL_Init")
	.dwattr $C$DW$216, DW_AT_TI_call
        LCR       #_HAL_Init            ; [CPU_] |213| 
        ; call occurs [#_HAL_Init] ; [] |213| 
	.dwpsn	file "../mms.c",line 214,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |214| 
        BF        $C$L11,NEQ            ; [CPU_] |214| 
        ; branchcc occurs ; [] |214| 
$C$L10:    
	.dwpsn	file "../mms.c",line 215,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |215| 
        MOVB      AL,#1                 ; [CPU_] |215| 
$C$DW$217	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$217, DW_AT_low_pc(0x00)
	.dwattr $C$DW$217, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$217, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |215| 
        ; call occurs [#_SEM_pend] ; [] |215| 
	.dwpsn	file "../mms.c",line 214,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |214| 
        BF        $C$L10,EQ             ; [CPU_] |214| 
        ; branchcc occurs ; [] |214| 
$C$L11:    
	.dwpsn	file "../mms.c",line 217,column 3,is_stmt
$C$DW$218	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$218, DW_AT_low_pc(0x00)
	.dwattr $C$DW$218, DW_AT_name("_USB_Start")
	.dwattr $C$DW$218, DW_AT_TI_call
        LCR       #_USB_Start           ; [CPU_] |217| 
        ; call occurs [#_USB_Start] ; [] |217| 
	.dwpsn	file "../mms.c",line 218,column 3,is_stmt
        MOVW      DP,#_SCI_MsgInRS232   ; [CPU_U] 
        MOVB      @_SCI_MsgInRS232,#70,UNC ; [CPU_] |218| 
	.dwpsn	file "../mms.c",line 218,column 42,is_stmt
$C$DW$219	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$219, DW_AT_low_pc(0x00)
	.dwattr $C$DW$219, DW_AT_name("_SCI1_Command")
	.dwattr $C$DW$219, DW_AT_TI_call
        LCR       #_SCI1_Command        ; [CPU_] |218| 
        ; call occurs [#_SCI1_Command] ; [] |218| 
	.dwpsn	file "../mms.c",line 219,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |219| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |219| 
$C$DW$220	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$220, DW_AT_low_pc(0x00)
	.dwattr $C$DW$220, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$220, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |219| 
        ; call occurs [#_SEM_pend] ; [] |219| 
	.dwpsn	file "../mms.c",line 220,column 3,is_stmt
        MOVW      DP,#_SCI_MsgInRS232   ; [CPU_U] 
        MOVB      @_SCI_MsgInRS232,#65,UNC ; [CPU_] |220| 
	.dwpsn	file "../mms.c",line 220,column 38,is_stmt
$C$DW$221	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$221, DW_AT_low_pc(0x00)
	.dwattr $C$DW$221, DW_AT_name("_SCI1_Command")
	.dwattr $C$DW$221, DW_AT_TI_call
        LCR       #_SCI1_Command        ; [CPU_] |220| 
        ; call occurs [#_SCI1_Command] ; [] |220| 
	.dwpsn	file "../mms.c",line 221,column 10,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOV       AL,@_SCI_MsgAvailable ; [CPU_] |221| 
        BF        $C$L13,NEQ            ; [CPU_] |221| 
        ; branchcc occurs ; [] |221| 
$C$L12:    
	.dwpsn	file "../mms.c",line 222,column 4,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |222| 
        MOVB      AL,#1                 ; [CPU_] |222| 
$C$DW$222	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$222, DW_AT_low_pc(0x00)
	.dwattr $C$DW$222, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$222, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |222| 
        ; call occurs [#_SEM_pend] ; [] |222| 
	.dwpsn	file "../mms.c",line 221,column 10,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOV       AL,@_SCI_MsgAvailable ; [CPU_] |221| 
        BF        $C$L12,EQ             ; [CPU_] |221| 
        ; branchcc occurs ; [] |221| 
$C$L13:    
	.dwpsn	file "../mms.c",line 224,column 3,is_stmt
$C$DW$223	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$223, DW_AT_low_pc(0x00)
	.dwattr $C$DW$223, DW_AT_name("_SCI1_Receive")
	.dwattr $C$DW$223, DW_AT_TI_call
        LCR       #_SCI1_Receive        ; [CPU_] |224| 
        ; call occurs [#_SCI1_Receive] ; [] |224| 
	.dwpsn	file "../mms.c",line 225,column 3,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOV       @_SCI_MsgAvailable,#0 ; [CPU_] |225| 
	.dwpsn	file "../mms.c",line 226,column 3,is_stmt
        MOVW      DP,#_ODP_VersionParameters ; [CPU_U] 
        MOV       AL,@_ODP_VersionParameters ; [CPU_] |226| 
        CMPB      AL,#128               ; [CPU_] |226| 
        B         $C$L14,HI             ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
	.dwpsn	file "../mms.c",line 227,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |227| 
        MOVB      AL,#1                 ; [CPU_] |227| 
$C$DW$224	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$224, DW_AT_low_pc(0x00)
	.dwattr $C$DW$224, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$224, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |227| 
        ; call occurs [#_SEM_pend] ; [] |227| 
	.dwpsn	file "../mms.c",line 228,column 5,is_stmt
        MOVW      DP,#_SCI_MsgInRS232   ; [CPU_U] 
        MOVB      @_SCI_MsgInRS232,#82,UNC ; [CPU_] |228| 
	.dwpsn	file "../mms.c",line 228,column 44,is_stmt
$C$DW$225	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$225, DW_AT_low_pc(0x00)
	.dwattr $C$DW$225, DW_AT_name("_SCI1_Command")
	.dwattr $C$DW$225, DW_AT_TI_call
        LCR       #_SCI1_Command        ; [CPU_] |228| 
        ; call occurs [#_SCI1_Command] ; [] |228| 
	.dwpsn	file "../mms.c",line 229,column 5,is_stmt
        MOVW      DP,#_ODP_VersionParameters ; [CPU_U] 
        MOVB      @_ODP_VersionParameters,#129,UNC ; [CPU_] |229| 
	.dwpsn	file "../mms.c",line 230,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |230| 
        MOV       AL,#8194              ; [CPU_] |230| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |230| 
$C$DW$226	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$226, DW_AT_low_pc(0x00)
	.dwattr $C$DW$226, DW_AT_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$226, DW_AT_TI_call
        LCR       #_PAR_StoreODSubIndex ; [CPU_] |230| 
        ; call occurs [#_PAR_StoreODSubIndex] ; [] |230| 
$C$L14:    
	.dwpsn	file "../mms.c",line 232,column 3,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |232| 
	.dwpsn	file "../mms.c",line 233,column 3,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |233| 
        MOVL      *-SP[6],ACC           ; [CPU_] |233| 
	.dwpsn	file "../mms.c",line 234,column 3,is_stmt
        B         $C$L28,UNC            ; [CPU_] |234| 
        ; branch occurs ; [] |234| 
$C$L15:    
	.dwpsn	file "../mms.c",line 238,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVB      XAR6,#1               ; [CPU_] |238| 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |238| 
        MOVB      AH,#0                 ; [CPU_] |238| 
        ANDB      AL,#0x3f              ; [CPU_] |238| 
        CMPL      ACC,XAR6              ; [CPU_] |238| 
        B         $C$L16,HI             ; [CPU_] |238| 
        ; branchcc occurs ; [] |238| 
        MOVB      XAR6,#62              ; [CPU_] |238| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |238| 
        MOVB      AH,#0                 ; [CPU_] |238| 
        ANDB      AL,#0x3f              ; [CPU_] |238| 
        CMPL      ACC,XAR6              ; [CPU_] |238| 
        B         $C$L16,LO             ; [CPU_] |238| 
        ; branchcc occurs ; [] |238| 
	.dwpsn	file "../mms.c",line 240,column 7,is_stmt
        MOVB      *-SP[3],#20,UNC       ; [CPU_] |240| 
$C$L16:    
	.dwpsn	file "../mms.c",line 242,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |242| 
        CMPB      AL,#20                ; [CPU_] |242| 
        BF        $C$L17,NEQ            ; [CPU_] |242| 
        ; branchcc occurs ; [] |242| 
	.dwpsn	file "../mms.c",line 243,column 7,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |243| 
        MOV       *-SP[1],#0            ; [CPU_] |243| 
        MOVB      AL,#9                 ; [CPU_] |243| 
        MOVL      XAR4,#_ODV_Gateway_Date_Time ; [CPU_U] |243| 
        MOVB      AH,#7                 ; [CPU_] |243| 
        SUBB      XAR5,#3               ; [CPU_U] |243| 
$C$DW$227	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$227, DW_AT_low_pc(0x00)
	.dwattr $C$DW$227, DW_AT_name("_I2C_CommandNoWait")
	.dwattr $C$DW$227, DW_AT_TI_call
        LCR       #_I2C_CommandNoWait   ; [CPU_] |243| 
        ; call occurs [#_I2C_CommandNoWait] ; [] |243| 
$C$L17:    
	.dwpsn	file "../mms.c",line 245,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |245| 
        CMPB      AL,#10                ; [CPU_] |245| 
        BF        $C$L18,NEQ            ; [CPU_] |245| 
        ; branchcc occurs ; [] |245| 
	.dwpsn	file "../mms.c",line 246,column 7,is_stmt
        MOVB      *-SP[3],#30,UNC       ; [CPU_] |246| 
	.dwpsn	file "../mms.c",line 247,column 7,is_stmt
        MOV       AL,#-1                ; [CPU_] |247| 
$C$DW$228	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$228, DW_AT_low_pc(0x00)
	.dwattr $C$DW$228, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$228, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |247| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |247| 
	.dwpsn	file "../mms.c",line 248,column 7,is_stmt
        MOV       AL,#-5                ; [CPU_] |248| 
$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$229, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |248| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |248| 
$C$L18:    
	.dwpsn	file "../mms.c",line 250,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |250| 
        MOVL      *-SP[6],ACC           ; [CPU_] |250| 
	.dwpsn	file "../mms.c",line 251,column 5,is_stmt
        MOVB      XAR6,#10              ; [CPU_] |251| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |251| 
        SUBL      ACC,*-SP[8]           ; [CPU_] |251| 
        CMPL      ACC,XAR6              ; [CPU_] |251| 
        B         $C$L21,LO             ; [CPU_] |251| 
        ; branchcc occurs ; [] |251| 
	.dwpsn	file "../mms.c",line 252,column 7,is_stmt
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |252| 
        MOVL      *-SP[8],ACC           ; [CPU_] |252| 
	.dwpsn	file "../mms.c",line 253,column 7,is_stmt
        MOVB      *-SP[2],#71,UNC       ; [CPU_] |253| 
	.dwpsn	file "../mms.c",line 254,column 7,is_stmt
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        MOVB      ACC,#3                ; [CPU_] |254| 
        CMPL      ACC,@_ODV_CommError_Counter ; [CPU_] |254| 
        B         $C$L20,LOS            ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
	.dwpsn	file "../mms.c",line 255,column 9,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |255| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |255| 
        MOVB      AL,#0                 ; [CPU_] |255| 
        SUBB      XAR5,#2               ; [CPU_U] |255| 
$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_name("_MBX_post")
	.dwattr $C$DW$230, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |255| 
        ; call occurs [#_MBX_post] ; [] |255| 
        CMPB      AL,#0                 ; [CPU_] |255| 
        BF        $C$L19,NEQ            ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
	.dwpsn	file "../mms.c",line 256,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |256| 
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        ADDL      @_ODV_CommError_Counter,ACC ; [CPU_] |256| 
	.dwpsn	file "../mms.c",line 257,column 11,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOV       @_SCI_Available,#0    ; [CPU_] |257| 
	.dwpsn	file "../mms.c",line 258,column 11,is_stmt
$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_name("_RS232ReceiveEnable")
	.dwattr $C$DW$231, DW_AT_TI_call
        LCR       #_RS232ReceiveEnable  ; [CPU_] |258| 
        ; call occurs [#_RS232ReceiveEnable] ; [] |258| 
	.dwpsn	file "../mms.c",line 261,column 9,is_stmt
        B         $C$L20,UNC            ; [CPU_] |261| 
        ; branch occurs ; [] |261| 
$C$L19:    
	.dwpsn	file "../mms.c",line 262,column 14,is_stmt
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        MOVL      ACC,@_ODV_CommError_Counter ; [CPU_] |262| 
        BF        $C$L20,EQ             ; [CPU_] |262| 
        ; branchcc occurs ; [] |262| 
	.dwpsn	file "../mms.c",line 262,column 41,is_stmt
        MOVB      ACC,#1                ; [CPU_] |262| 
        SUBL      @_ODV_CommError_Counter,ACC ; [CPU_] |262| 
$C$L20:    
	.dwpsn	file "../mms.c",line 264,column 7,is_stmt
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_name("_USB_Unlock")
	.dwattr $C$DW$232, DW_AT_TI_call
        LCR       #_USB_Unlock          ; [CPU_] |264| 
        ; call occurs [#_USB_Unlock] ; [] |264| 
$C$L21:    
	.dwpsn	file "../mms.c",line 266,column 5,is_stmt
        MOVW      DP,#_ODV_CommError_Set ; [CPU_U] 
        MOV       AL,@_ODV_CommError_Set ; [CPU_] |266| 
        CMPB      AL,#1                 ; [CPU_] |266| 
        BF        $C$L23,NEQ            ; [CPU_] |266| 
        ; branchcc occurs ; [] |266| 
	.dwpsn	file "../mms.c",line 267,column 7,is_stmt
        MOV       @_ODV_CommError_Set,#0 ; [CPU_] |267| 
	.dwpsn	file "../mms.c",line 268,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |268| 
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        MOVL      @_ODV_CommError_Counter,ACC ; [CPU_] |268| 
	.dwpsn	file "../mms.c",line 269,column 7,is_stmt
$C$L22:    
        MOVZ      AR5,SP                ; [CPU_U] |269| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |269| 
        MOVB      AL,#0                 ; [CPU_] |269| 
        SUBB      XAR5,#2               ; [CPU_U] |269| 
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$233, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |269| 
        ; call occurs [#_MBX_pend] ; [] |269| 
        CMPB      AL,#0                 ; [CPU_] |269| 
        BF        $C$L22,NEQ            ; [CPU_] |269| 
        ; branchcc occurs ; [] |269| 
	.dwpsn	file "../mms.c",line 270,column 7,is_stmt
$C$DW$234	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$234, DW_AT_low_pc(0x00)
	.dwattr $C$DW$234, DW_AT_name("_RS232ReceiveEnable")
	.dwattr $C$DW$234, DW_AT_TI_call
        LCR       #_RS232ReceiveEnable  ; [CPU_] |270| 
        ; call occurs [#_RS232ReceiveEnable] ; [] |270| 
$C$L23:    
	.dwpsn	file "../mms.c",line 272,column 5,is_stmt
        MOVW      DP,#_ODV_CommError_Set ; [CPU_U] 
        MOV       AL,@_ODV_CommError_Set ; [CPU_] |272| 
        CMPB      AL,#2                 ; [CPU_] |272| 
        BF        $C$L25,NEQ            ; [CPU_] |272| 
        ; branchcc occurs ; [] |272| 
	.dwpsn	file "../mms.c",line 273,column 7,is_stmt
        MOV       @_ODV_CommError_Set,#0 ; [CPU_] |273| 
	.dwpsn	file "../mms.c",line 274,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |274| 
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        MOVL      @_ODV_CommError_Counter,ACC ; [CPU_] |274| 
	.dwpsn	file "../mms.c",line 275,column 7,is_stmt
$C$L24:    
        MOVZ      AR5,SP                ; [CPU_U] |275| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |275| 
        MOVB      AL,#0                 ; [CPU_] |275| 
        SUBB      XAR5,#2               ; [CPU_U] |275| 
$C$DW$235	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$235, DW_AT_low_pc(0x00)
	.dwattr $C$DW$235, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$235, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |275| 
        ; call occurs [#_MBX_pend] ; [] |275| 
        CMPB      AL,#0                 ; [CPU_] |275| 
        BF        $C$L24,NEQ            ; [CPU_] |275| 
        ; branchcc occurs ; [] |275| 
	.dwpsn	file "../mms.c",line 276,column 7,is_stmt
        MOVB      *-SP[2],#70,UNC       ; [CPU_] |276| 
	.dwpsn	file "../mms.c",line 276,column 31,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |276| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |276| 
        MOVB      AL,#0                 ; [CPU_] |276| 
        SUBB      XAR5,#2               ; [CPU_U] |276| 
$C$DW$236	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$236, DW_AT_low_pc(0x00)
	.dwattr $C$DW$236, DW_AT_name("_MBX_post")
	.dwattr $C$DW$236, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |276| 
        ; call occurs [#_MBX_post] ; [] |276| 
$C$L25:    
	.dwpsn	file "../mms.c",line 278,column 5,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOV       AL,@_SCI_Available    ; [CPU_] |278| 
        BF        $C$L26,EQ             ; [CPU_] |278| 
        ; branchcc occurs ; [] |278| 
	.dwpsn	file "../mms.c",line 279,column 7,is_stmt
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |279| 
        MOVL      XAR5,#_SCI_MsgInRS232 ; [CPU_U] |279| 
        MOVB      AL,#0                 ; [CPU_] |279| 
$C$DW$237	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$237, DW_AT_low_pc(0x00)
	.dwattr $C$DW$237, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$237, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |279| 
        ; call occurs [#_MBX_pend] ; [] |279| 
        CMPB      AL,#0                 ; [CPU_] |279| 
        BF        $C$L26,EQ             ; [CPU_] |279| 
        ; branchcc occurs ; [] |279| 
	.dwpsn	file "../mms.c",line 280,column 9,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOV       @_SCI_Available,#0    ; [CPU_] |280| 
	.dwpsn	file "../mms.c",line 281,column 9,is_stmt
$C$DW$238	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$238, DW_AT_low_pc(0x00)
	.dwattr $C$DW$238, DW_AT_name("_SCI1_Command")
	.dwattr $C$DW$238, DW_AT_TI_call
        LCR       #_SCI1_Command        ; [CPU_] |281| 
        ; call occurs [#_SCI1_Command] ; [] |281| 
$C$L26:    
	.dwpsn	file "../mms.c",line 284,column 5,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOV       AL,@_SCI_MsgAvailable ; [CPU_] |284| 
        BF        $C$L27,EQ             ; [CPU_] |284| 
        ; branchcc occurs ; [] |284| 
	.dwpsn	file "../mms.c",line 285,column 7,is_stmt
        MOV       @_SCI_MsgAvailable,#0 ; [CPU_] |285| 
	.dwpsn	file "../mms.c",line 287,column 7,is_stmt
$C$DW$239	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$239, DW_AT_low_pc(0x00)
	.dwattr $C$DW$239, DW_AT_name("_SCI1_Receive")
	.dwattr $C$DW$239, DW_AT_TI_call
        LCR       #_SCI1_Receive        ; [CPU_] |287| 
        ; call occurs [#_SCI1_Receive] ; [] |287| 
	.dwpsn	file "../mms.c",line 288,column 7,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |288| 
$C$L27:    
	.dwpsn	file "../mms.c",line 290,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |290| 
        MOVB      AL,#1                 ; [CPU_] |290| 
$C$DW$240	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$240, DW_AT_low_pc(0x00)
	.dwattr $C$DW$240, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$240, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |290| 
        ; call occurs [#_SEM_pend] ; [] |290| 
$C$L28:    
	.dwpsn	file "../mms.c",line 234,column 9,is_stmt
        MOVW      DP,#_BootST           ; [CPU_U] 
        MOVL      ACC,@_BootST          ; [CPU_] |234| 
        BF        $C$L15,EQ             ; [CPU_] |234| 
        ; branchcc occurs ; [] |234| 
	.dwpsn	file "../mms.c",line 292,column 3,is_stmt
$C$DW$241	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$241, DW_AT_low_pc(0x00)
	.dwattr $C$DW$241, DW_AT_name("_USB_Stop")
	.dwattr $C$DW$241, DW_AT_TI_call
        LCR       #_USB_Stop            ; [CPU_] |292| 
        ; call occurs [#_USB_Stop] ; [] |292| 
	.dwpsn	file "../mms.c",line 293,column 3,is_stmt
        MOVB      AL,#2                 ; [CPU_] |293| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |293| 
$C$DW$242	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$242, DW_AT_low_pc(0x00)
	.dwattr $C$DW$242, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$242, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |293| 
        ; call occurs [#_SEM_pend] ; [] |293| 
	.dwpsn	file "../mms.c",line 294,column 3,is_stmt
$C$DW$243	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$243, DW_AT_low_pc(0x00)
	.dwattr $C$DW$243, DW_AT_name("_HAL_Reset")
	.dwattr $C$DW$243, DW_AT_TI_call
        LCR       #_HAL_Reset           ; [CPU_] |294| 
        ; call occurs [#_HAL_Reset] ; [] |294| 
	.dwpsn	file "../mms.c",line 295,column 3,is_stmt
        MOV       AL,#28009             ; [CPU_] |295| 
        MOV       AH,#21093             ; [CPU_] |295| 
        MOVW      DP,#_BootST           ; [CPU_U] 
        MOVL      @_BootST,ACC          ; [CPU_] |295| 
	.dwpsn	file "../mms.c",line 296,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOVL      ACC,@_ODP_Board_RevisionNumber ; [CPU_] |296| 
        MOVW      DP,#_BootST+2         ; [CPU_U] 
        MOVL      @_BootST+2,ACC        ; [CPU_] |296| 
	.dwpsn	file "../mms.c",line 297,column 3,is_stmt
 LB 0x3F7FF6 
	.dwpsn	file "../mms.c",line 298,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$244	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$244, DW_AT_low_pc(0x00)
	.dwattr $C$DW$244, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$210, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$210, DW_AT_TI_end_line(0x12a)
	.dwattr $C$DW$210, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$210

	.sect	".text"
	.global	_TaskSciSendReceive

$C$DW$245	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskSciSendReceive")
	.dwattr $C$DW$245, DW_AT_low_pc(_TaskSciSendReceive)
	.dwattr $C$DW$245, DW_AT_high_pc(0x00)
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_TaskSciSendReceive")
	.dwattr $C$DW$245, DW_AT_external
	.dwattr $C$DW$245, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$245, DW_AT_TI_begin_line(0x130)
	.dwattr $C$DW$245, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$245, DW_AT_TI_max_frame_size(-28)
	.dwpsn	file "../mms.c",line 304,column 30,is_stmt,address _TaskSciSendReceive

	.dwfde $C$DW$CIE, _TaskSciSendReceive

;***************************************************************
;* FNAME: _TaskSciSendReceive           FR SIZE:  26           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter, 24 Auto,  0 SOE     *
;***************************************************************

_TaskSciSendReceive:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -28
$C$DW$246	.dwtag  DW_TAG_variable, DW_AT_name("start")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_start")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_breg20 -4]
$C$DW$247	.dwtag  DW_TAG_variable, DW_AT_name("insulation_counter")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_insulation_counter")
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$247, DW_AT_location[DW_OP_breg20 -6]
$C$DW$248	.dwtag  DW_TAG_variable, DW_AT_name("current_counter")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_current_counter")
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$248, DW_AT_location[DW_OP_breg20 -8]
$C$DW$249	.dwtag  DW_TAG_variable, DW_AT_name("sleep_counter")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_sleep_counter")
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$249, DW_AT_location[DW_OP_breg20 -10]
$C$DW$250	.dwtag  DW_TAG_variable, DW_AT_name("volt_count")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_volt_count")
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$250, DW_AT_location[DW_OP_breg20 -12]
$C$DW$251	.dwtag  DW_TAG_variable, DW_AT_name("Temp_counter_Delay")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_Temp_counter_Delay")
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$251, DW_AT_location[DW_OP_breg20 -14]
$C$DW$252	.dwtag  DW_TAG_variable, DW_AT_name("Low_VoltaCounter")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_Low_VoltaCounter")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_breg20 -16]
$C$DW$253	.dwtag  DW_TAG_variable, DW_AT_name("Low_Voltage_CurrentCounter")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_Low_Voltage_CurrentCounter")
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$253, DW_AT_location[DW_OP_breg20 -18]
$C$DW$254	.dwtag  DW_TAG_variable, DW_AT_name("cur")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_cur")
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$254, DW_AT_location[DW_OP_breg20 -19]
$C$DW$255	.dwtag  DW_TAG_variable, DW_AT_name("volt")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_volt")
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$255, DW_AT_location[DW_OP_breg20 -20]
$C$DW$256	.dwtag  DW_TAG_variable, DW_AT_name("volt1")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_volt1")
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$256, DW_AT_location[DW_OP_breg20 -21]
$C$DW$257	.dwtag  DW_TAG_variable, DW_AT_name("curf")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_curf")
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$257, DW_AT_location[DW_OP_breg20 -24]
$C$DW$258	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$258, DW_AT_location[DW_OP_breg20 -25]
$C$DW$259	.dwtag  DW_TAG_variable, DW_AT_name("checkenable")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_checkenable")
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$259, DW_AT_location[DW_OP_breg20 -26]
	.dwpsn	file "../mms.c",line 305,column 45,is_stmt
        MOVB      ACC,#0                ; [CPU_] |305| 
        MOVL      *-SP[6],ACC           ; [CPU_] |305| 
	.dwpsn	file "../mms.c",line 305,column 66,is_stmt
        MOVL      *-SP[8],ACC           ; [CPU_] |305| 
	.dwpsn	file "../mms.c",line 305,column 85,is_stmt
        MOVL      *-SP[10],ACC          ; [CPU_] |305| 
	.dwpsn	file "../mms.c",line 305,column 101,is_stmt
        MOVL      *-SP[12],ACC          ; [CPU_] |305| 
	.dwpsn	file "../mms.c",line 305,column 125,is_stmt
        MOVL      *-SP[14],ACC          ; [CPU_] |305| 
	.dwpsn	file "../mms.c",line 305,column 147,is_stmt
        MOVL      *-SP[16],ACC          ; [CPU_] |305| 
	.dwpsn	file "../mms.c",line 305,column 179,is_stmt
        MOVL      *-SP[18],ACC          ; [CPU_] |305| 
	.dwpsn	file "../mms.c",line 309,column 21,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |309| 
	.dwpsn	file "../mms.c",line 310,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#-1 ; [CPU_] |310| 
	.dwpsn	file "../mms.c",line 311,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |311| 
        BF        $C$L30,NEQ            ; [CPU_] |311| 
        ; branchcc occurs ; [] |311| 
$C$L29:    
	.dwpsn	file "../mms.c",line 312,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |312| 
        MOVB      AL,#1                 ; [CPU_] |312| 
$C$DW$260	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$260, DW_AT_low_pc(0x00)
	.dwattr $C$DW$260, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$260, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |312| 
        ; call occurs [#_SEM_pend] ; [] |312| 
	.dwpsn	file "../mms.c",line 311,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |311| 
        BF        $C$L29,EQ             ; [CPU_] |311| 
        ; branchcc occurs ; [] |311| 
$C$L30:    
	.dwpsn	file "../mms.c",line 314,column 10,is_stmt
        CMPB      AL,#1                 ; [CPU_] |314| 
        BF        $C$L32,EQ             ; [CPU_] |314| 
        ; branchcc occurs ; [] |314| 
$C$L31:    
	.dwpsn	file "../mms.c",line 316,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |316| 
        MOVB      AL,#1                 ; [CPU_] |316| 
$C$DW$261	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$261, DW_AT_low_pc(0x00)
	.dwattr $C$DW$261, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$261, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |316| 
        ; call occurs [#_SEM_pend] ; [] |316| 
	.dwpsn	file "../mms.c",line 314,column 10,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |314| 
        CMPB      AL,#1                 ; [CPU_] |314| 
        BF        $C$L31,NEQ            ; [CPU_] |314| 
        ; branchcc occurs ; [] |314| 
$C$L32:    
	.dwpsn	file "../mms.c",line 318,column 3,is_stmt
        MOVW      DP,#_ODV_MachineEvent ; [CPU_U] 
        MOV       @_ODV_MachineEvent,#0 ; [CPU_] |318| 
	.dwpsn	file "../mms.c",line 319,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |319| 
	.dwpsn	file "../mms.c",line 320,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |320| 
	.dwpsn	file "../mms.c",line 321,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |321| 
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      @_ODV_Gateway_Errorcode,ACC ; [CPU_] |321| 
	.dwpsn	file "../mms.c",line 323,column 3,is_stmt
        MOVW      DP,#_ODV_Module1_Status ; [CPU_U] 
        MOV       @_ODV_Module1_Status,#0 ; [CPU_] |323| 
	.dwpsn	file "../mms.c",line 324,column 3,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |324| 
        MOVL      *-SP[4],ACC           ; [CPU_] |324| 
	.dwpsn	file "../mms.c",line 325,column 9,is_stmt
        MOVW      DP,#_HAL_Enable       ; [CPU_U] 
        MOV       AL,@_HAL_Enable       ; [CPU_] |325| 
        BF        $C$L34,NEQ            ; [CPU_] |325| 
        ; branchcc occurs ; [] |325| 
$C$L33:    
	.dwpsn	file "../mms.c",line 326,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |326| 
        MOVB      AL,#1                 ; [CPU_] |326| 
$C$DW$262	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$262, DW_AT_low_pc(0x00)
	.dwattr $C$DW$262, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$262, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |326| 
        ; call occurs [#_SEM_pend] ; [] |326| 
	.dwpsn	file "../mms.c",line 325,column 9,is_stmt
        MOVW      DP,#_HAL_Enable       ; [CPU_U] 
        MOV       AL,@_HAL_Enable       ; [CPU_] |325| 
        BF        $C$L33,EQ             ; [CPU_] |325| 
        ; branchcc occurs ; [] |325| 
$C$L34:    
	.dwpsn	file "../mms.c",line 328,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |328| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |328| 
        BF        $C$L35,TC             ; [CPU_] |328| 
        ; branchcc occurs ; [] |328| 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |328| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |328| 
        LSR       AL,1                  ; [CPU_] |328| 
        CMPB      AL,#1                 ; [CPU_] |328| 
        BF        $C$L35,NEQ            ; [CPU_] |328| 
        ; branchcc occurs ; [] |328| 
	.dwpsn	file "../mms.c",line 330,column 3,is_stmt
        MOVW      DP,#_ODP_CommError_TimeOut ; [CPU_U] 
        MOV       @_ODP_CommError_TimeOut,#0 ; [CPU_] |330| 
$C$L35:    
	.dwpsn	file "../mms.c",line 332,column 10,is_stmt
$C$L36:    
	.dwpsn	file "../mms.c",line 334,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |334| 
        CMPB      AL,#8                 ; [CPU_] |334| 
        BF        $C$L37,NEQ            ; [CPU_] |334| 
        ; branchcc occurs ; [] |334| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |334| 
        CMPB      AL,#8                 ; [CPU_] |334| 
        BF        $C$L37,EQ             ; [CPU_] |334| 
        ; branchcc occurs ; [] |334| 
	.dwpsn	file "../mms.c",line 335,column 7,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |335| 
        MOVL      *-SP[4],ACC           ; [CPU_] |335| 
	.dwpsn	file "../mms.c",line 336,column 7,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |336| 
	.dwpsn	file "../mms.c",line 337,column 7,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#8,UNC ; [CPU_] |337| 
$C$L37:    
	.dwpsn	file "../mms.c",line 339,column 5,is_stmt
        MOVW      DP,#_ODV_Module1_MaxCellVoltage ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_MaxCellVoltage << #1 ; [CPU_] |339| 
        MOV       *-SP[20],AL           ; [CPU_] |339| 
	.dwpsn	file "../mms.c",line 340,column 5,is_stmt
        MOVW      DP,#_ODV_Module1_MinCellVoltage ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_MinCellVoltage << #1 ; [CPU_] |340| 
        MOV       *-SP[21],AL           ; [CPU_] |340| 
	.dwpsn	file "../mms.c",line 341,column 5,is_stmt
        MOVIZ     R1H,#16896            ; [CPU_] |341| 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        I16TOF32  R0H,@_ODV_Module1_Current ; [CPU_] |341| 
$C$DW$263	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$263, DW_AT_low_pc(0x00)
	.dwattr $C$DW$263, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$263, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |341| 
        ; call occurs [#FS$$DIV] ; [] |341| 
        MOV32     *-SP[24],R0H          ; [CPU_] |341| 
	.dwpsn	file "../mms.c",line 342,column 5,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |342| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |342| 
        BF        $C$L38,TC             ; [CPU_] |342| 
        ; branchcc occurs ; [] |342| 
	.dwpsn	file "../mms.c",line 342,column 30,is_stmt
        ZERO      R0H                   ; [CPU_] |342| 
        MOV32     *-SP[24],R0H          ; [CPU_] |342| 
$C$L38:    
	.dwpsn	file "../mms.c",line 343,column 5,is_stmt
$C$DW$264	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$264, DW_AT_low_pc(0x00)
	.dwattr $C$DW$264, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$264, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |343| 
        ; call occurs [#_CNV_Round] ; [] |343| 
        MOV       *-SP[19],AL           ; [CPU_] |343| 
	.dwpsn	file "../mms.c",line 346,column 5,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax_bal_delta ; [CPU_U] 
        MOVB      XAR6,#50              ; [CPU_] |346| 
        MOV       AL,@_ODP_SafetyLimits_Umax_bal_delta ; [CPU_] |346| 
        LSR       AL,1                  ; [CPU_] |346| 
        MOVW      DP,#_ODV_Module1_MaxDeltaVoltage ; [CPU_U] 
        MOV       T,AL                  ; [CPU_] |346| 
        MPYB      ACC,T,#100            ; [CPU_] |346| 
        MOVL      XAR7,ACC              ; [CPU_] |346| 
        MOVU      ACC,@_ODV_Module1_MaxDeltaVoltage ; [CPU_] |346| 
        RPT       #15
||     SUBCU     ACC,AR6               ; [CPU_] |346| 
        MOV       T,AL                  ; [CPU_] |346| 
        MOV       AL,AR7                ; [CPU_] |346| 
        MPYB      P,T,#100              ; [CPU_] |346| 
        CMP       AL,PL                 ; [CPU_] |346| 
        B         $C$L39,HI             ; [CPU_] |346| 
        ; branchcc occurs ; [] |346| 
	.dwpsn	file "../mms.c",line 348,column 6,is_stmt
        MOVB      ACC,#32               ; [CPU_] |348| 
$C$DW$265	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$265, DW_AT_low_pc(0x00)
	.dwattr $C$DW$265, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$265, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |348| 
        ; call occurs [#_ERR_HandleWarning] ; [] |348| 
$C$L39:    
	.dwpsn	file "../mms.c",line 350,column 5,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax_bal_delta ; [CPU_U] 
        MOV       T,@_ODP_SafetyLimits_Umax_bal_delta ; [CPU_] |350| 
        MOVB      XAR6,#50              ; [CPU_] |350| 
        MPYB      ACC,T,#100            ; [CPU_] |350| 
        MOVW      DP,#_ODV_Module1_MaxDeltaVoltage ; [CPU_U] 
        MOVL      XAR7,ACC              ; [CPU_] |350| 
        MOVU      ACC,@_ODV_Module1_MaxDeltaVoltage ; [CPU_] |350| 
        RPT       #15
||     SUBCU     ACC,AR6               ; [CPU_] |350| 
        MOV       T,AL                  ; [CPU_] |350| 
        MPYB      P,T,#100              ; [CPU_] |350| 
        MOV       AL,AR7                ; [CPU_] |350| 
        CMP       AL,PL                 ; [CPU_] |350| 
        B         $C$L40,HI             ; [CPU_] |350| 
        ; branchcc occurs ; [] |350| 
	.dwpsn	file "../mms.c",line 352,column 6,is_stmt
        MOVB      ACC,#1                ; [CPU_] |352| 
        MOVW      DP,#_GV_Delta_Counter ; [CPU_U] 
        ADDL      @_GV_Delta_Counter,ACC ; [CPU_] |352| 
	.dwpsn	file "../mms.c",line 354,column 8,is_stmt
        MOV       AL,*-SP[19]           ; [CPU_] |354| 
        CMPB      AL,#3                 ; [CPU_] |354| 
        B         $C$L41,LEQ            ; [CPU_] |354| 
        ; branchcc occurs ; [] |354| 
	.dwpsn	file "../mms.c",line 356,column 9,is_stmt
        MOV       T,#1000               ; [CPU_] |356| 
        MOVW      DP,#_ODP_SafetyLimits_Temp_Delay ; [CPU_U] 
        MPY       ACC,T,@_ODP_SafetyLimits_Temp_Delay ; [CPU_] |356| 
        MOVW      DP,#_GV_Delta_Counter ; [CPU_U] 
        MOVU      ACC,AL                ; [CPU_] |356| 
        CMPL      ACC,@_GV_Delta_Counter ; [CPU_] |356| 
        B         $C$L41,HIS            ; [CPU_] |356| 
        ; branchcc occurs ; [] |356| 
	.dwpsn	file "../mms.c",line 358,column 10,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |358| 
        BF        $C$L41,NEQ            ; [CPU_] |358| 
        ; branchcc occurs ; [] |358| 
	.dwpsn	file "../mms.c",line 358,column 45,is_stmt
$C$DW$266	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$266, DW_AT_low_pc(0x00)
	.dwattr $C$DW$266, DW_AT_name("_ERR_ErrorDigOvld")
	.dwattr $C$DW$266, DW_AT_TI_call
        LCR       #_ERR_ErrorDigOvld    ; [CPU_] |358| 
        ; call occurs [#_ERR_ErrorDigOvld] ; [] |358| 
	.dwpsn	file "../mms.c",line 362,column 5,is_stmt
        B         $C$L41,UNC            ; [CPU_] |362| 
        ; branch occurs ; [] |362| 
$C$L40:    
	.dwpsn	file "../mms.c",line 365,column 6,is_stmt
        MOVB      ACC,#0                ; [CPU_] |365| 
        MOVW      DP,#_GV_Delta_Counter ; [CPU_U] 
        MOVL      @_GV_Delta_Counter,ACC ; [CPU_] |365| 
$C$L41:    
	.dwpsn	file "../mms.c",line 368,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AH,@_ODV_MachineMode  ; [CPU_] |368| 
        MOVW      DP,#_ODV_Module1_Status ; [CPU_U] 
        ANDB      AH,#0x0f              ; [CPU_] |368| 
        MOV       AL,@_ODV_Module1_Status ; [CPU_] |368| 
        ANDB      AL,#0xc0              ; [CPU_] |368| 
        ADD       AL,AH                 ; [CPU_] |368| 
        MOV       @_ODV_Module1_Status,AL ; [CPU_] |368| 
	.dwpsn	file "../mms.c",line 369,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |369| 
        B         $C$L92,LEQ            ; [CPU_] |369| 
        ; branchcc occurs ; [] |369| 
        CMPB      AL,#4                 ; [CPU_] |369| 
        B         $C$L92,GT             ; [CPU_] |369| 
        ; branchcc occurs ; [] |369| 
	.dwpsn	file "../mms.c",line 370,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Overcurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Overcurrent ; [CPU_] |370| 
        CMP       AL,*-SP[19]           ; [CPU_] |370| 
        B         $C$L42,LT             ; [CPU_] |370| 
        ; branchcc occurs ; [] |370| 
        MOVW      DP,#_ODP_SafetyLimits_UnderCurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderCurrent ; [CPU_] |370| 
        CMP       AL,*-SP[19]           ; [CPU_] |370| 
        B         $C$L43,LEQ            ; [CPU_] |370| 
        ; branchcc occurs ; [] |370| 
$C$L42:    
	.dwpsn	file "../mms.c",line 372,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |372| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        ADDL      @_GV_current_counter,ACC ; [CPU_] |372| 
	.dwpsn	file "../mms.c",line 373,column 8,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |373| 
        AND       AL,*+XAR4[0],#0x0020  ; [CPU_] |373| 
        LSR       AL,5                  ; [CPU_] |373| 
        CMPB      AL,#1                 ; [CPU_] |373| 
        BF        $C$L44,NEQ            ; [CPU_] |373| 
        ; branchcc occurs ; [] |373| 
        MOV       T,#1000               ; [CPU_] |373| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |373| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        CMPL      ACC,@_GV_current_counter ; [CPU_] |373| 
        B         $C$L44,HIS            ; [CPU_] |373| 
        ; branchcc occurs ; [] |373| 
	.dwpsn	file "../mms.c",line 374,column 9,is_stmt
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$267, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |374| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |374| 
	.dwpsn	file "../mms.c",line 376,column 7,is_stmt
        B         $C$L44,UNC            ; [CPU_] |376| 
        ; branch occurs ; [] |376| 
$C$L43:    
	.dwpsn	file "../mms.c",line 379,column 8,is_stmt
        MOVB      ACC,#0                ; [CPU_] |379| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        MOVL      @_GV_current_counter,ACC ; [CPU_] |379| 
$C$L44:    
	.dwpsn	file "../mms.c",line 382,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umin ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umin ; [CPU_] |382| 
        CMP       AL,*-SP[21]           ; [CPU_] |382| 
        B         $C$L45,LEQ            ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
	.dwpsn	file "../mms.c",line 384,column 8,is_stmt
        MOVB      ACC,#32               ; [CPU_] |384| 
$C$DW$268	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$268, DW_AT_low_pc(0x00)
	.dwattr $C$DW$268, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$268, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |384| 
        ; call occurs [#_ERR_HandleWarning] ; [] |384| 
$C$L45:    
	.dwpsn	file "../mms.c",line 387,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |387| 
        CMP       AL,*-SP[21]           ; [CPU_] |387| 
        B         $C$L46,GEQ            ; [CPU_] |387| 
        ; branchcc occurs ; [] |387| 
	.dwpsn	file "../mms.c",line 389,column 8,is_stmt
        MOVB      ACC,#8                ; [CPU_] |389| 
$C$DW$269	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$269, DW_AT_low_pc(0x00)
	.dwattr $C$DW$269, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$269, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |389| 
        ; call occurs [#_ERR_HandleWarning] ; [] |389| 
$C$L46:    
	.dwpsn	file "../mms.c",line 392,column 7,is_stmt
        MOV32     R0H,*-SP[24]          ; [CPU_] |392| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |392| 
        NOP       ; [CPU_] 
        CMPF32    R0H,#0                ; [CPU_] |392| 
        MOVST0    ZF, NF                ; [CPU_] |392| 
        B         $C$L47,LT             ; [CPU_] |392| 
        ; branchcc occurs ; [] |392| 
        MOV32     R0H,*-SP[24]          ; [CPU_] |392| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |392| 
        B         $C$L48,UNC            ; [CPU_] |392| 
        ; branch occurs ; [] |392| 
$C$L47:    
        MOV32     R0H,*-SP[24]          ; [CPU_] |392| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |392| 
        NOP       ; [CPU_] 
        NEGF32    R0H,R0H               ; [CPU_] |392| 
$C$L48:    
        MOVW      DP,#_ODP_SleepCurrent ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_SleepCurrent ; [CPU_] |392| 
        NOP       ; [CPU_] 
        CMPF32    R0H,R1H               ; [CPU_] |392| 
        MOVST0    ZF, NF                ; [CPU_] |392| 
        B         $C$L49,GEQ            ; [CPU_] |392| 
        ; branchcc occurs ; [] |392| 
	.dwpsn	file "../mms.c",line 393,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |393| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |393| 
        MOVL      *-SP[10],ACC          ; [CPU_] |393| 
	.dwpsn	file "../mms.c",line 394,column 9,is_stmt
        MOV       T,#60000              ; [CPU_] |394| 
        MOVW      DP,#_ODP_SafetyLimits_SleepTimeout ; [CPU_U] 
        MPYU      ACC,T,@_ODP_SafetyLimits_SleepTimeout ; [CPU_] |394| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |394| 
        B         $C$L50,HIS            ; [CPU_] |394| 
        ; branchcc occurs ; [] |394| 
	.dwpsn	file "../mms.c",line 395,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |395| 
        MOVL      *-SP[10],ACC          ; [CPU_] |395| 
	.dwpsn	file "../mms.c",line 396,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#64,UNC ; [CPU_] |396| 
	.dwpsn	file "../mms.c",line 397,column 11,is_stmt
        MOVB      ACC,#16               ; [CPU_] |397| 
$C$DW$270	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$270, DW_AT_low_pc(0x00)
	.dwattr $C$DW$270, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$270, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |397| 
        ; call occurs [#_ERR_HandleWarning] ; [] |397| 
	.dwpsn	file "../mms.c",line 399,column 7,is_stmt
        B         $C$L50,UNC            ; [CPU_] |399| 
        ; branch occurs ; [] |399| 
$C$L49:    
	.dwpsn	file "../mms.c",line 400,column 12,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |400| 
        BF        $C$L50,EQ             ; [CPU_] |400| 
        ; branchcc occurs ; [] |400| 
	.dwpsn	file "../mms.c",line 400,column 35,is_stmt
        MOVB      ACC,#1                ; [CPU_] |400| 
        SUBL      *-SP[10],ACC          ; [CPU_] |400| 
$C$L50:    
	.dwpsn	file "../mms.c",line 401,column 7,is_stmt
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_DischargeAllowed ; [CPU_] |401| 
        NEG       AL                    ; [CPU_] |401| 
        CMP       AL,*-SP[19]           ; [CPU_] |401| 
        B         $C$L51,GT             ; [CPU_] |401| 
        ; branchcc occurs ; [] |401| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_ChargeAllowed ; [CPU_] |401| 
        CMP       AL,*-SP[19]           ; [CPU_] |401| 
        B         $C$L52,GEQ            ; [CPU_] |401| 
        ; branchcc occurs ; [] |401| 
$C$L51:    
	.dwpsn	file "../mms.c",line 402,column 9,is_stmt
        MOVB      ACC,#64               ; [CPU_] |402| 
$C$DW$271	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$271, DW_AT_low_pc(0x00)
	.dwattr $C$DW$271, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$271, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |402| 
        ; call occurs [#_ERR_HandleWarning] ; [] |402| 
	.dwpsn	file "../mms.c",line 403,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |403| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |403| 
        MOVL      *-SP[8],ACC           ; [CPU_] |403| 
	.dwpsn	file "../mms.c",line 404,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |404| 
        AND       AL,*+XAR4[0],#0x0020  ; [CPU_] |404| 
        LSR       AL,5                  ; [CPU_] |404| 
        CMPB      AL,#1                 ; [CPU_] |404| 
        BF        $C$L53,NEQ            ; [CPU_] |404| 
        ; branchcc occurs ; [] |404| 
        MOV       T,#1000               ; [CPU_] |404| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |404| 
        CMPL      ACC,*-SP[8]           ; [CPU_] |404| 
        B         $C$L53,HIS            ; [CPU_] |404| 
        ; branchcc occurs ; [] |404| 
	.dwpsn	file "../mms.c",line 407,column 7,is_stmt
        B         $C$L53,UNC            ; [CPU_] |407| 
        ; branch occurs ; [] |407| 
$C$L52:    
	.dwpsn	file "../mms.c",line 408,column 12,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |408| 
        BF        $C$L53,EQ             ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
	.dwpsn	file "../mms.c",line 408,column 37,is_stmt
        MOVB      ACC,#1                ; [CPU_] |408| 
        SUBL      *-SP[8],ACC           ; [CPU_] |408| 
$C$L53:    
	.dwpsn	file "../mms.c",line 409,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |409| 
        BF        $C$L92,NEQ            ; [CPU_] |409| 
        ; branchcc occurs ; [] |409| 
	.dwpsn	file "../mms.c",line 410,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |410| 
        AND       AL,*+XAR4[0],#0x0800  ; [CPU_] |410| 
        LSR       AL,11                 ; [CPU_] |410| 
        CMPB      AL,#1                 ; [CPU_] |410| 
        BF        $C$L54,NEQ            ; [CPU_] |410| 
        ; branchcc occurs ; [] |410| 
	.dwpsn	file "../mms.c",line 411,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |411| 
$C$L54:    
	.dwpsn	file "../mms.c",line 413,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |413| 
        AND       AL,*+XAR4[0],#0x0080  ; [CPU_] |413| 
        LSR       AL,7                  ; [CPU_] |413| 
        CMPB      AL,#1                 ; [CPU_] |413| 
        BF        $C$L55,NEQ            ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
	.dwpsn	file "../mms.c",line 414,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |414| 
$C$L55:    
	.dwpsn	file "../mms.c",line 416,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |416| 
	.dwpsn	file "../mms.c",line 419,column 5,is_stmt
        B         $C$L92,UNC            ; [CPU_] |419| 
        ; branch occurs ; [] |419| 
$C$L56:    
	.dwpsn	file "../mms.c",line 422,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |422| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |422| 
        LSR       AL,1                  ; [CPU_] |422| 
        CMPB      AL,#1                 ; [CPU_] |422| 
        BF        $C$L57,NEQ            ; [CPU_] |422| 
        ; branchcc occurs ; [] |422| 
	.dwpsn	file "../mms.c",line 423,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |423| 
	.dwpsn	file "../mms.c",line 424,column 9,is_stmt
        B         $C$L58,UNC            ; [CPU_] |424| 
        ; branch occurs ; [] |424| 
$C$L57:    
	.dwpsn	file "../mms.c",line 425,column 14,is_stmt
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |425| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |425| 
        BF        $C$L58,TC             ; [CPU_] |425| 
        ; branchcc occurs ; [] |425| 
	.dwpsn	file "../mms.c",line 426,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |426| 
$C$L58:    
	.dwpsn	file "../mms.c",line 428,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |428| 
        CMP       AL,*-SP[21]           ; [CPU_] |428| 
        B         $C$L59,LEQ            ; [CPU_] |428| 
        ; branchcc occurs ; [] |428| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |428| 
        BF        $C$L59,NEQ            ; [CPU_] |428| 
        ; branchcc occurs ; [] |428| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |428| 
        CMPB      AL,#128               ; [CPU_] |428| 
        BF        $C$L59,EQ             ; [CPU_] |428| 
        ; branchcc occurs ; [] |428| 
	.dwpsn	file "../mms.c",line 429,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |429| 
$C$L59:    
	.dwpsn	file "../mms.c",line 431,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |431| 
        BF        $C$L94,EQ             ; [CPU_] |431| 
        ; branchcc occurs ; [] |431| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |431| 
        BF        $C$L94,NEQ            ; [CPU_] |431| 
        ; branchcc occurs ; [] |431| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |431| 
        CMPB      AL,#128               ; [CPU_] |431| 
        BF        $C$L94,EQ             ; [CPU_] |431| 
        ; branchcc occurs ; [] |431| 
	.dwpsn	file "../mms.c",line 432,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |432| 
        AND       AL,*+XAR4[0],#0x0080  ; [CPU_] |432| 
        LSR       AL,7                  ; [CPU_] |432| 
        CMPB      AL,#1                 ; [CPU_] |432| 
        BF        $C$L60,NEQ            ; [CPU_] |432| 
        ; branchcc occurs ; [] |432| 
	.dwpsn	file "../mms.c",line 433,column 13,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#1,UNC ; [CPU_] |433| 
$C$L60:    
	.dwpsn	file "../mms.c",line 435,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |435| 
        AND       AL,*+XAR4[0],#0x0800  ; [CPU_] |435| 
        LSR       AL,11                 ; [CPU_] |435| 
        CMPB      AL,#1                 ; [CPU_] |435| 
        BF        $C$L61,NEQ            ; [CPU_] |435| 
        ; branchcc occurs ; [] |435| 
	.dwpsn	file "../mms.c",line 436,column 13,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#1,UNC ; [CPU_] |436| 
$C$L61:    
	.dwpsn	file "../mms.c",line 438,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |438| 
        MOVL      *-SP[4],ACC           ; [CPU_] |438| 
	.dwpsn	file "../mms.c",line 439,column 11,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |439| 
	.dwpsn	file "../mms.c",line 440,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |440| 
	.dwpsn	file "../mms.c",line 442,column 7,is_stmt
        B         $C$L94,UNC            ; [CPU_] |442| 
        ; branch occurs ; [] |442| 
$C$L62:    
	.dwpsn	file "../mms.c",line 445,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |445| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |445| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |445| 
        CMPL      ACC,XAR6              ; [CPU_] |445| 
        B         $C$L63,LO             ; [CPU_] |445| 
        ; branchcc occurs ; [] |445| 
	.dwpsn	file "../mms.c",line 445,column 43,is_stmt
        MOVB      *-SP[26],#1,UNC       ; [CPU_] |445| 
$C$L63:    
	.dwpsn	file "../mms.c",line 446,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |446| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        CMP       AL,@_ODV_Current_ChargeAllowed ; [CPU_] |446| 
        B         $C$L64,HIS            ; [CPU_] |446| 
        ; branchcc occurs ; [] |446| 
	.dwpsn	file "../mms.c",line 446,column 63,is_stmt
        MOV       @_ODV_Current_ChargeAllowed,AL ; [CPU_] |446| 
$C$L64:    
	.dwpsn	file "../mms.c",line 448,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |448| 
        CMP       AL,*-SP[20]           ; [CPU_] |448| 
        B         $C$L65,LEQ            ; [CPU_] |448| 
        ; branchcc occurs ; [] |448| 
        MOV       AL,*-SP[26]           ; [CPU_] |448| 
        BF        $C$L65,EQ             ; [CPU_] |448| 
        ; branchcc occurs ; [] |448| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |448| 
        BF        $C$L65,NEQ            ; [CPU_] |448| 
        ; branchcc occurs ; [] |448| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |448| 
        CMPB      AL,#128               ; [CPU_] |448| 
        BF        $C$L65,EQ             ; [CPU_] |448| 
        ; branchcc occurs ; [] |448| 
	.dwpsn	file "../mms.c",line 449,column 11,is_stmt
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |449| 
	.dwpsn	file "../mms.c",line 450,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |450| 
        MOVL      *-SP[4],ACC           ; [CPU_] |450| 
	.dwpsn	file "../mms.c",line 451,column 11,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |451| 
	.dwpsn	file "../mms.c",line 452,column 11,is_stmt
$C$DW$272	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$272, DW_AT_low_pc(0x00)
	.dwattr $C$DW$272, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$272, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |452| 
        ; call occurs [#_ERR_ClearWarning] ; [] |452| 
$C$L65:    
	.dwpsn	file "../mms.c",line 454,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |454| 
        CMP       AL,*-SP[21]           ; [CPU_] |454| 
        B         $C$L66,LEQ            ; [CPU_] |454| 
        ; branchcc occurs ; [] |454| 
	.dwpsn	file "../mms.c",line 455,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |455| 
	.dwpsn	file "../mms.c",line 456,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |456| 
        MOVL      *-SP[4],ACC           ; [CPU_] |456| 
	.dwpsn	file "../mms.c",line 457,column 11,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |457| 
$C$L66:    
	.dwpsn	file "../mms.c",line 459,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |459| 
        CMP       AL,*-SP[21]           ; [CPU_] |459| 
        B         $C$L94,GEQ            ; [CPU_] |459| 
        ; branchcc occurs ; [] |459| 
        MOV       AL,*-SP[26]           ; [CPU_] |459| 
        BF        $C$L94,EQ             ; [CPU_] |459| 
        ; branchcc occurs ; [] |459| 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Current ; [CPU_] |459| 
        CMPB      AL,#6                 ; [CPU_] |459| 
        B         $C$L94,LEQ            ; [CPU_] |459| 
        ; branchcc occurs ; [] |459| 
	.dwpsn	file "../mms.c",line 463,column 7,is_stmt
        B         $C$L94,UNC            ; [CPU_] |463| 
        ; branch occurs ; [] |463| 
$C$L67:    
	.dwpsn	file "../mms.c",line 466,column 6,is_stmt
        MOVW      DP,#_ODV_Module1_SOC  ; [CPU_U] 
        MOV       @_ODV_Module1_SOC,#0  ; [CPU_] |466| 
	.dwpsn	file "../mms.c",line 467,column 6,is_stmt
        MOVB      ACC,#32               ; [CPU_] |467| 
$C$DW$273	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$273, DW_AT_low_pc(0x00)
	.dwattr $C$DW$273, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$273, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |467| 
        ; call occurs [#_ERR_HandleWarning] ; [] |467| 
	.dwpsn	file "../mms.c",line 468,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |468| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |468| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |468| 
        CMPL      ACC,XAR6              ; [CPU_] |468| 
        B         $C$L68,LO             ; [CPU_] |468| 
        ; branchcc occurs ; [] |468| 
	.dwpsn	file "../mms.c",line 468,column 43,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |468| 
$C$L68:    
	.dwpsn	file "../mms.c",line 470,column 9,is_stmt
        MOVW      DP,#_ODV_Module1_MinCellVoltage ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_MinCellVoltage << #1 ; [CPU_] |470| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        CMP       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |470| 
        B         $C$L71,HI             ; [CPU_] |470| 
        ; branchcc occurs ; [] |470| 
	.dwpsn	file "../mms.c",line 472,column 10,is_stmt
        MOV       AL,*-SP[19]           ; [CPU_] |472| 
        CMPB      AL,#2                 ; [CPU_] |472| 
        B         $C$L72,GT             ; [CPU_] |472| 
        ; branchcc occurs ; [] |472| 
	.dwpsn	file "../mms.c",line 475,column 10,is_stmt
	.dwpsn	file "../mms.c",line 476,column 15,is_stmt
        CMP       AL,#-1                ; [CPU_] |476| 
        B         $C$L69,GEQ            ; [CPU_] |476| 
        ; branchcc occurs ; [] |476| 
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Current_Allow ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Low_Voltage_Current_Allow ; [CPU_] |476| 
        CMP       AL,*-SP[19]           ; [CPU_] |476| 
        B         $C$L69,GEQ            ; [CPU_] |476| 
        ; branchcc occurs ; [] |476| 
	.dwpsn	file "../mms.c",line 478,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |478| 
        ADDL      ACC,*-SP[18]          ; [CPU_] |478| 
        MOVL      *-SP[18],ACC          ; [CPU_] |478| 
	.dwpsn	file "../mms.c",line 479,column 5,is_stmt
        MOV       T,#1000               ; [CPU_] |479| 
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Current_Delay ; [CPU_U] 
        MPY       ACC,T,@_ODP_SafetyLimits_Low_Voltage_Current_Delay ; [CPU_] |479| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVU      ACC,AL                ; [CPU_] |479| 
        CMPL      ACC,@_ODV_SysTick_ms  ; [CPU_] |479| 
        B         $C$L72,HIS            ; [CPU_] |479| 
        ; branchcc occurs ; [] |479| 
	.dwpsn	file "../mms.c",line 487,column 5,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Current_Delay ; [CPU_U] 
        MPY       ACC,T,@_ODP_SafetyLimits_Low_Voltage_Current_Delay ; [CPU_] |487| 
        MOVU      ACC,AL                ; [CPU_] |487| 
        CMPL      ACC,*-SP[18]          ; [CPU_] |487| 
        B         $C$L72,HIS            ; [CPU_] |487| 
        ; branchcc occurs ; [] |487| 
	.dwpsn	file "../mms.c",line 489,column 7,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |489| 
        BF        $C$L72,NEQ            ; [CPU_] |489| 
        ; branchcc occurs ; [] |489| 
	.dwpsn	file "../mms.c",line 489,column 42,is_stmt
$C$DW$274	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$274, DW_AT_low_pc(0x00)
	.dwattr $C$DW$274, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$274, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |489| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |489| 
	.dwpsn	file "../mms.c",line 492,column 10,is_stmt
        B         $C$L72,UNC            ; [CPU_] |492| 
        ; branch occurs ; [] |492| 
$C$L69:    
	.dwpsn	file "../mms.c",line 495,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |495| 
        ADDL      ACC,*-SP[16]          ; [CPU_] |495| 
        MOVL      *-SP[16],ACC          ; [CPU_] |495| 
	.dwpsn	file "../mms.c",line 496,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |496| 
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Charge_Delay ; [CPU_U] 
        MPY       ACC,T,@_ODP_SafetyLimits_Low_Voltage_Charge_Delay ; [CPU_] |496| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVU      ACC,AL                ; [CPU_] |496| 
        CMPL      ACC,@_ODV_SysTick_ms  ; [CPU_] |496| 
        B         $C$L72,HIS            ; [CPU_] |496| 
        ; branchcc occurs ; [] |496| 
	.dwpsn	file "../mms.c",line 498,column 12,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Charge_Delay ; [CPU_U] 
        MPY       ACC,T,@_ODP_SafetyLimits_Low_Voltage_Charge_Delay ; [CPU_] |498| 
        MOVU      ACC,AL                ; [CPU_] |498| 
        CMPL      ACC,*-SP[16]          ; [CPU_] |498| 
        B         $C$L72,HIS            ; [CPU_] |498| 
        ; branchcc occurs ; [] |498| 
	.dwpsn	file "../mms.c",line 500,column 14,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |500| 
        BF        $C$L72,NEQ            ; [CPU_] |500| 
        ; branchcc occurs ; [] |500| 
	.dwpsn	file "../mms.c",line 502,column 16,is_stmt
        MOVW      DP,#_ODP_CommError_LowVoltage_ErrCounter ; [CPU_U] 
        MOV       AL,@_ODP_CommError_LowVoltage_ErrCounter ; [CPU_] |502| 
        CMPB      AL,#250               ; [CPU_] |502| 
        B         $C$L70,LOS            ; [CPU_] |502| 
        ; branchcc occurs ; [] |502| 
	.dwpsn	file "../mms.c",line 502,column 60,is_stmt
        MOVB      @_ODP_CommError_LowVoltage_ErrCounter,#255,UNC ; [CPU_] |502| 
$C$L70:    
	.dwpsn	file "../mms.c",line 503,column 16,is_stmt
        INC       @_ODP_CommError_LowVoltage_ErrCounter ; [CPU_] |503| 
	.dwpsn	file "../mms.c",line 504,column 18,is_stmt
        MOV       AL,#-5                ; [CPU_] |504| 
$C$DW$275	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$275, DW_AT_low_pc(0x00)
	.dwattr $C$DW$275, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$275, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |504| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |504| 
	.dwpsn	file "../mms.c",line 505,column 16,is_stmt
$C$DW$276	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$276, DW_AT_low_pc(0x00)
	.dwattr $C$DW$276, DW_AT_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$276, DW_AT_TI_call
        LCR       #_ERR_ErrorUnderVoltage ; [CPU_] |505| 
        ; call occurs [#_ERR_ErrorUnderVoltage] ; [] |505| 
	.dwpsn	file "../mms.c",line 510,column 10,is_stmt
        B         $C$L72,UNC            ; [CPU_] |510| 
        ; branch occurs ; [] |510| 
$C$L71:    
	.dwpsn	file "../mms.c",line 514,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |514| 
        MOVL      *-SP[18],ACC          ; [CPU_] |514| 
$C$L72:    
	.dwpsn	file "../mms.c",line 518,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |518| 
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        CMP       AL,@_ODV_Current_DischargeAllowed ; [CPU_] |518| 
        B         $C$L73,HIS            ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
	.dwpsn	file "../mms.c",line 518,column 65,is_stmt
        MOV       @_ODV_Current_DischargeAllowed,AL ; [CPU_] |518| 
$C$L73:    
	.dwpsn	file "../mms.c",line 520,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_] |520| 
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        CMP       AL,@_ODV_Current_DischargeAllowed ; [CPU_] |520| 
        B         $C$L74,HIS            ; [CPU_] |520| 
        ; branchcc occurs ; [] |520| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |520| 
        CMP       AL,*-SP[21]           ; [CPU_] |520| 
        B         $C$L74,LEQ            ; [CPU_] |520| 
        ; branchcc occurs ; [] |520| 
	.dwpsn	file "../mms.c",line 521,column 11,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_] |521| 
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_DischargeAllowed,AL ; [CPU_] |521| 
$C$L74:    
	.dwpsn	file "../mms.c",line 523,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |523| 
        CMP       AL,*-SP[21]           ; [CPU_] |523| 
        B         $C$L75,GEQ            ; [CPU_] |523| 
        ; branchcc occurs ; [] |523| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |523| 
        BF        $C$L75,NEQ            ; [CPU_] |523| 
        ; branchcc occurs ; [] |523| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |523| 
        CMPB      AL,#128               ; [CPU_] |523| 
        BF        $C$L76,NEQ            ; [CPU_] |523| 
        ; branchcc occurs ; [] |523| 
$C$L75:    
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |523| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |523| 
        BF        $C$L94,TC             ; [CPU_] |523| 
        ; branchcc occurs ; [] |523| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |523| 
        CMP       AL,*-SP[21]           ; [CPU_] |523| 
        B         $C$L94,GEQ            ; [CPU_] |523| 
        ; branchcc occurs ; [] |523| 
$C$L76:    
	.dwpsn	file "../mms.c",line 524,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |524| 
        MOVL      *-SP[4],ACC           ; [CPU_] |524| 
	.dwpsn	file "../mms.c",line 525,column 11,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |525| 
	.dwpsn	file "../mms.c",line 526,column 11,is_stmt
$C$DW$277	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$277, DW_AT_low_pc(0x00)
	.dwattr $C$DW$277, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$277, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |526| 
        ; call occurs [#_ERR_ClearWarning] ; [] |526| 
	.dwpsn	file "../mms.c",line 527,column 11,is_stmt
        MOVB      *-SP[25],#81,UNC      ; [CPU_] |527| 
	.dwpsn	file "../mms.c",line 528,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |528| 
	.dwpsn	file "../mms.c",line 529,column 11,is_stmt
        MOVB      AL,#0                 ; [CPU_] |529| 
        MOVZ      AR5,SP                ; [CPU_U] |529| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |529| 
        SUBB      XAR5,#25              ; [CPU_U] |529| 
$C$DW$278	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$278, DW_AT_low_pc(0x00)
	.dwattr $C$DW$278, DW_AT_name("_MBX_post")
	.dwattr $C$DW$278, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |529| 
        ; call occurs [#_MBX_post] ; [] |529| 
	.dwpsn	file "../mms.c",line 531,column 7,is_stmt
        B         $C$L94,UNC            ; [CPU_] |531| 
        ; branch occurs ; [] |531| 
$C$L77:    
	.dwpsn	file "../mms.c",line 535,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |535| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |535| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |535| 
        CMPL      ACC,XAR6              ; [CPU_] |535| 
        B         $C$L79,LO             ; [CPU_] |535| 
        ; branchcc occurs ; [] |535| 
        MOV       AL,*-SP[26]           ; [CPU_] |535| 
        BF        $C$L79,NEQ            ; [CPU_] |535| 
        ; branchcc occurs ; [] |535| 
	.dwpsn	file "../mms.c",line 536,column 11,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |536| 
        CMP       AL,*-SP[21]           ; [CPU_] |536| 
        B         $C$L78,LEQ            ; [CPU_] |536| 
        ; branchcc occurs ; [] |536| 
	.dwpsn	file "../mms.c",line 537,column 13,is_stmt
        MOVB      ACC,#32               ; [CPU_] |537| 
$C$DW$279	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$279, DW_AT_low_pc(0x00)
	.dwattr $C$DW$279, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$279, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |537| 
        ; call occurs [#_ERR_HandleWarning] ; [] |537| 
	.dwpsn	file "../mms.c",line 538,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |538| 
	.dwpsn	file "../mms.c",line 539,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |539| 
        MOVL      *-SP[4],ACC           ; [CPU_] |539| 
	.dwpsn	file "../mms.c",line 540,column 13,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |540| 
	.dwpsn	file "../mms.c",line 541,column 11,is_stmt
        B         $C$L79,UNC            ; [CPU_] |541| 
        ; branch occurs ; [] |541| 
$C$L78:    
	.dwpsn	file "../mms.c",line 543,column 13,is_stmt
        MOVB      *-SP[25],#81,UNC      ; [CPU_] |543| 
	.dwpsn	file "../mms.c",line 544,column 13,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |544| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |544| 
        MOVB      AL,#0                 ; [CPU_] |544| 
        SUBB      XAR5,#25              ; [CPU_U] |544| 
$C$DW$280	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$280, DW_AT_low_pc(0x00)
	.dwattr $C$DW$280, DW_AT_name("_MBX_post")
	.dwattr $C$DW$280, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |544| 
        ; call occurs [#_MBX_post] ; [] |544| 
	.dwpsn	file "../mms.c",line 545,column 13,is_stmt
        MOVB      *-SP[26],#1,UNC       ; [CPU_] |545| 
$C$L79:    
	.dwpsn	file "../mms.c",line 548,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |548| 
        CMP       AL,*-SP[21]           ; [CPU_] |548| 
        B         $C$L80,LEQ            ; [CPU_] |548| 
        ; branchcc occurs ; [] |548| 
	.dwpsn	file "../mms.c",line 549,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |549| 
	.dwpsn	file "../mms.c",line 550,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |550| 
        MOVL      *-SP[4],ACC           ; [CPU_] |550| 
	.dwpsn	file "../mms.c",line 551,column 11,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |551| 
$C$L80:    
	.dwpsn	file "../mms.c",line 553,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |553| 
        CMPB      AL,#2                 ; [CPU_] |553| 
        BF        $C$L81,NEQ            ; [CPU_] |553| 
        ; branchcc occurs ; [] |553| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |553| 
        BF        $C$L82,EQ             ; [CPU_] |553| 
        ; branchcc occurs ; [] |553| 
$C$L81:    
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |553| 
        CMP       AL,*-SP[21]           ; [CPU_] |553| 
        B         $C$L83,LEQ            ; [CPU_] |553| 
        ; branchcc occurs ; [] |553| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |553| 
        BF        $C$L83,NEQ            ; [CPU_] |553| 
        ; branchcc occurs ; [] |553| 
$C$L82:    
	.dwpsn	file "../mms.c",line 554,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |554| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |554| 
        MOVL      *-SP[12],ACC          ; [CPU_] |554| 
	.dwpsn	file "../mms.c",line 555,column 11,is_stmt
        MOVB      ACC,#32               ; [CPU_] |555| 
$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$281, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |555| 
        ; call occurs [#_ERR_HandleWarning] ; [] |555| 
	.dwpsn	file "../mms.c",line 556,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |556| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |556| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |556| 
        B         $C$L94,HIS            ; [CPU_] |556| 
        ; branchcc occurs ; [] |556| 
	.dwpsn	file "../mms.c",line 557,column 13,is_stmt
        MOVB      ACC,#32               ; [CPU_] |557| 
$C$DW$282	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$282, DW_AT_low_pc(0x00)
	.dwattr $C$DW$282, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$282, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |557| 
        ; call occurs [#_ERR_HandleWarning] ; [] |557| 
	.dwpsn	file "../mms.c",line 559,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |559| 
	.dwpsn	file "../mms.c",line 560,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |560| 
        MOVL      *-SP[12],ACC          ; [CPU_] |560| 
	.dwpsn	file "../mms.c",line 561,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |561| 
        MOVL      *-SP[4],ACC           ; [CPU_] |561| 
	.dwpsn	file "../mms.c",line 562,column 13,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |562| 
	.dwpsn	file "../mms.c",line 564,column 9,is_stmt
        B         $C$L94,UNC            ; [CPU_] |564| 
        ; branch occurs ; [] |564| 
$C$L83:    
	.dwpsn	file "../mms.c",line 565,column 14,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |565| 
        CMPB      AL,#4                 ; [CPU_] |565| 
        BF        $C$L84,NEQ            ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |565| 
        BF        $C$L85,EQ             ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
$C$L84:    
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |565| 
        CMP       AL,*-SP[20]           ; [CPU_] |565| 
        B         $C$L86,GEQ            ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
        MOV       AL,*-SP[26]           ; [CPU_] |565| 
        BF        $C$L86,EQ             ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |565| 
        BF        $C$L86,NEQ            ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
$C$L85:    
	.dwpsn	file "../mms.c",line 566,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |566| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |566| 
        MOVL      *-SP[12],ACC          ; [CPU_] |566| 
	.dwpsn	file "../mms.c",line 567,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |567| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |567| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |567| 
        B         $C$L94,HIS            ; [CPU_] |567| 
        ; branchcc occurs ; [] |567| 
	.dwpsn	file "../mms.c",line 568,column 14,is_stmt
        MOVB      ACC,#8                ; [CPU_] |568| 
$C$DW$283	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$283, DW_AT_low_pc(0x00)
	.dwattr $C$DW$283, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$283, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |568| 
        ; call occurs [#_ERR_HandleWarning] ; [] |568| 
	.dwpsn	file "../mms.c",line 570,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#4,UNC ; [CPU_] |570| 
	.dwpsn	file "../mms.c",line 571,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |571| 
        MOVL      *-SP[12],ACC          ; [CPU_] |571| 
	.dwpsn	file "../mms.c",line 572,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |572| 
        MOVL      *-SP[4],ACC           ; [CPU_] |572| 
	.dwpsn	file "../mms.c",line 573,column 13,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |573| 
	.dwpsn	file "../mms.c",line 575,column 9,is_stmt
        B         $C$L94,UNC            ; [CPU_] |575| 
        ; branch occurs ; [] |575| 
$C$L86:    
	.dwpsn	file "../mms.c",line 576,column 14,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |576| 
        BF        $C$L94,EQ             ; [CPU_] |576| 
        ; branchcc occurs ; [] |576| 
	.dwpsn	file "../mms.c",line 576,column 34,is_stmt
        MOVB      ACC,#1                ; [CPU_] |576| 
        SUBL      *-SP[12],ACC          ; [CPU_] |576| 
	.dwpsn	file "../mms.c",line 577,column 7,is_stmt
        B         $C$L94,UNC            ; [CPU_] |577| 
        ; branch occurs ; [] |577| 
$C$L87:    
	.dwpsn	file "../mms.c",line 580,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |580| 
        MOVL      *-SP[4],ACC           ; [CPU_] |580| 
	.dwpsn	file "../mms.c",line 581,column 9,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |581| 
	.dwpsn	file "../mms.c",line 582,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#128,UNC ; [CPU_] |582| 
$C$L88:    
	.dwpsn	file "../mms.c",line 586,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |586| 
        MOVW      DP,#_ODP_CommError_Delay ; [CPU_U] 
        MOVZ      AR6,@_ODP_CommError_Delay ; [CPU_] |586| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |586| 
        CMPL      ACC,XAR6              ; [CPU_] |586| 
        B         $C$L89,LO             ; [CPU_] |586| 
        ; branchcc occurs ; [] |586| 
        MOV       AL,*-SP[26]           ; [CPU_] |586| 
        BF        $C$L89,NEQ            ; [CPU_] |586| 
        ; branchcc occurs ; [] |586| 
	.dwpsn	file "../mms.c",line 587,column 11,is_stmt
        MOVB      *-SP[26],#1,UNC       ; [CPU_] |587| 
	.dwpsn	file "../mms.c",line 588,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |588| 
	.dwpsn	file "../mms.c",line 589,column 11,is_stmt
$C$DW$284	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$284, DW_AT_low_pc(0x00)
	.dwattr $C$DW$284, DW_AT_name("_PAR_AddLog")
	.dwattr $C$DW$284, DW_AT_TI_call
        LCR       #_PAR_AddLog          ; [CPU_] |589| 
        ; call occurs [#_PAR_AddLog] ; [] |589| 
$C$L89:    
	.dwpsn	file "../mms.c",line 591,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |591| 
        BF        $C$L90,NEQ            ; [CPU_] |591| 
        ; branchcc occurs ; [] |591| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |591| 
        CMPB      AL,#8                 ; [CPU_] |591| 
        BF        $C$L90,EQ             ; [CPU_] |591| 
        ; branchcc occurs ; [] |591| 
	.dwpsn	file "../mms.c",line 592,column 11,is_stmt
        MOVB      @_ODV_MachineMode,#16,UNC ; [CPU_] |592| 
$C$L90:    
	.dwpsn	file "../mms.c",line 594,column 9,is_stmt
        MOVW      DP,#_HAL_CellOK       ; [CPU_U] 
        MOV       AL,@_HAL_CellOK       ; [CPU_] |594| 
        CMPB      AL,#2                 ; [CPU_] |594| 
        BF        $C$L94,NEQ            ; [CPU_] |594| 
        ; branchcc occurs ; [] |594| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |594| 
        BF        $C$L94,NEQ            ; [CPU_] |594| 
        ; branchcc occurs ; [] |594| 
	.dwpsn	file "../mms.c",line 595,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#16,UNC ; [CPU_] |595| 
	.dwpsn	file "../mms.c",line 597,column 7,is_stmt
        B         $C$L94,UNC            ; [CPU_] |597| 
        ; branch occurs ; [] |597| 
$C$L91:    
	.dwpsn	file "../mms.c",line 599,column 9,is_stmt
$C$DW$285	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$285, DW_AT_low_pc(0x00)
	.dwattr $C$DW$285, DW_AT_name("_ERR_ClearError")
	.dwattr $C$DW$285, DW_AT_TI_call
        LCR       #_ERR_ClearError      ; [CPU_] |599| 
        ; call occurs [#_ERR_ClearError] ; [] |599| 
        CMPB      AL,#0                 ; [CPU_] |599| 
        BF        $C$L94,EQ             ; [CPU_] |599| 
        ; branchcc occurs ; [] |599| 
	.dwpsn	file "../mms.c",line 600,column 11,is_stmt
$C$DW$286	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$286, DW_AT_low_pc(0x00)
	.dwattr $C$DW$286, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$286, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |600| 
        ; call occurs [#_ERR_ClearWarning] ; [] |600| 
	.dwpsn	file "../mms.c",line 601,column 11,is_stmt
        MOVB      *-SP[25],#70,UNC      ; [CPU_] |601| 
	.dwpsn	file "../mms.c",line 602,column 11,is_stmt
        MOVB      AL,#0                 ; [CPU_] |602| 
        MOVZ      AR5,SP                ; [CPU_U] |602| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |602| 
        SUBB      XAR5,#25              ; [CPU_U] |602| 
$C$DW$287	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$287, DW_AT_low_pc(0x00)
	.dwattr $C$DW$287, DW_AT_name("_MBX_post")
	.dwattr $C$DW$287, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |602| 
        ; call occurs [#_MBX_post] ; [] |602| 
        CMPB      AL,#0                 ; [CPU_] |602| 
        BF        $C$L94,EQ             ; [CPU_] |602| 
        ; branchcc occurs ; [] |602| 
	.dwpsn	file "../mms.c",line 606,column 11,is_stmt
	.dwpsn	file "../mms.c",line 608,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |608| 
	.dwpsn	file "../mms.c",line 609,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |609| 
        MOVL      *-SP[4],ACC           ; [CPU_] |609| 
	.dwpsn	file "../mms.c",line 610,column 13,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |610| 
	.dwpsn	file "../mms.c",line 613,column 7,is_stmt
        B         $C$L94,UNC            ; [CPU_] |613| 
        ; branch occurs ; [] |613| 
$C$L92:    
	.dwpsn	file "../mms.c",line 419,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |419| 
        CMPB      AL,#8                 ; [CPU_] |419| 
        B         $C$L93,GT             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        CMPB      AL,#8                 ; [CPU_] |419| 
        BF        $C$L88,EQ             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        CMPB      AL,#0                 ; [CPU_] |419| 
        BF        $C$L56,EQ             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        CMPB      AL,#1                 ; [CPU_] |419| 
        BF        $C$L77,EQ             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        CMPB      AL,#2                 ; [CPU_] |419| 
        BF        $C$L67,EQ             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        CMPB      AL,#4                 ; [CPU_] |419| 
        BF        $C$L62,EQ             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        B         $C$L88,UNC            ; [CPU_] |419| 
        ; branch occurs ; [] |419| 
$C$L93:    
        CMPB      AL,#16                ; [CPU_] |419| 
        BF        $C$L91,EQ             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        CMPB      AL,#64                ; [CPU_] |419| 
        BF        $C$L87,EQ             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        CMPB      AL,#128               ; [CPU_] |419| 
        BF        $C$L88,EQ             ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        B         $C$L88,UNC            ; [CPU_] |419| 
        ; branch occurs ; [] |419| 
$C$L94:    
	.dwpsn	file "../mms.c",line 615,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |615| 
        MOVB      AL,#1                 ; [CPU_] |615| 
$C$DW$288	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$288, DW_AT_low_pc(0x00)
	.dwattr $C$DW$288, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$288, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |615| 
        ; call occurs [#_SEM_pend] ; [] |615| 
	.dwpsn	file "../mms.c",line 332,column 10,is_stmt
        B         $C$L36,UNC            ; [CPU_] |332| 
        ; branch occurs ; [] |332| 
	.dwattr $C$DW$245, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$245, DW_AT_TI_end_line(0x269)
	.dwattr $C$DW$245, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$245

	.sect	".text"
	.global	_SecurityCallBack

$C$DW$289	.dwtag  DW_TAG_subprogram, DW_AT_name("SecurityCallBack")
	.dwattr $C$DW$289, DW_AT_low_pc(_SecurityCallBack)
	.dwattr $C$DW$289, DW_AT_high_pc(0x00)
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_SecurityCallBack")
	.dwattr $C$DW$289, DW_AT_external
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$289, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$289, DW_AT_TI_begin_line(0x26d)
	.dwattr $C$DW$289, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$289, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 621,column 95,is_stmt,address _SecurityCallBack

	.dwfde $C$DW$CIE, _SecurityCallBack
$C$DW$290	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$290, DW_AT_location[DW_OP_reg12]
$C$DW$291	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$291, DW_AT_location[DW_OP_reg14]
$C$DW$292	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$292, DW_AT_location[DW_OP_reg0]
$C$DW$293	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$293, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SecurityCallBack             FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SecurityCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$294	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$294, DW_AT_location[DW_OP_breg20 -2]
$C$DW$295	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$295, DW_AT_location[DW_OP_breg20 -4]
$C$DW$296	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$296, DW_AT_location[DW_OP_breg20 -5]
$C$DW$297	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$297, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |621| 
        MOV       *-SP[5],AL            ; [CPU_] |621| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |621| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |621| 
	.dwpsn	file "../mms.c",line 622,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |622| 
        BF        $C$L95,NEQ            ; [CPU_] |622| 
        ; branchcc occurs ; [] |622| 
	.dwpsn	file "../mms.c",line 623,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |623| 
        BF        $C$L95,NEQ            ; [CPU_] |623| 
        ; branchcc occurs ; [] |623| 
	.dwpsn	file "../mms.c",line 623,column 28,is_stmt
$C$DW$298	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$298, DW_AT_low_pc(0x00)
	.dwattr $C$DW$298, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$298, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |623| 
        ; call occurs [#_HAL_Random] ; [] |623| 
$C$L95:    
	.dwpsn	file "../mms.c",line 625,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |625| 
        CMPB      AL,#1                 ; [CPU_] |625| 
        BF        $C$L97,NEQ            ; [CPU_] |625| 
        ; branchcc occurs ; [] |625| 
	.dwpsn	file "../mms.c",line 626,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |626| 
        BF        $C$L96,NEQ            ; [CPU_] |626| 
        ; branchcc occurs ; [] |626| 
	.dwpsn	file "../mms.c",line 626,column 28,is_stmt
$C$DW$299	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$299, DW_AT_low_pc(0x00)
	.dwattr $C$DW$299, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$299, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |626| 
        ; call occurs [#_HAL_Random] ; [] |626| 
        B         $C$L97,UNC            ; [CPU_] |626| 
        ; branch occurs ; [] |626| 
$C$L96:    
	.dwpsn	file "../mms.c",line 627,column 10,is_stmt
$C$DW$300	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$300, DW_AT_low_pc(0x00)
	.dwattr $C$DW$300, DW_AT_name("_HAL_Unlock")
	.dwattr $C$DW$300, DW_AT_TI_call
        LCR       #_HAL_Unlock          ; [CPU_] |627| 
        ; call occurs [#_HAL_Unlock] ; [] |627| 
$C$L97:    
	.dwpsn	file "../mms.c",line 629,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |629| 
	.dwpsn	file "../mms.c",line 630,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$301	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$301, DW_AT_low_pc(0x00)
	.dwattr $C$DW$301, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$289, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$289, DW_AT_TI_end_line(0x276)
	.dwattr $C$DW$289, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$289

	.sect	".text"
	.global	_LogNBCallback

$C$DW$302	.dwtag  DW_TAG_subprogram, DW_AT_name("LogNBCallback")
	.dwattr $C$DW$302, DW_AT_low_pc(_LogNBCallback)
	.dwattr $C$DW$302, DW_AT_high_pc(0x00)
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_LogNBCallback")
	.dwattr $C$DW$302, DW_AT_external
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$302, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$302, DW_AT_TI_begin_line(0x279)
	.dwattr $C$DW$302, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$302, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 633,column 92,is_stmt,address _LogNBCallback

	.dwfde $C$DW$CIE, _LogNBCallback
$C$DW$303	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$303, DW_AT_location[DW_OP_reg12]
$C$DW$304	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$304, DW_AT_location[DW_OP_reg14]
$C$DW$305	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$305, DW_AT_location[DW_OP_reg0]
$C$DW$306	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$306, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LogNBCallback                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LogNBCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$307	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$307, DW_AT_location[DW_OP_breg20 -2]
$C$DW$308	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$308, DW_AT_location[DW_OP_breg20 -4]
$C$DW$309	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$309, DW_AT_location[DW_OP_breg20 -5]
$C$DW$310	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$310, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |633| 
        MOV       *-SP[5],AL            ; [CPU_] |633| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |633| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |633| 
	.dwpsn	file "../mms.c",line 635,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |635| 
        CMPB      AL,#1                 ; [CPU_] |635| 
        BF        $C$L98,NEQ            ; [CPU_] |635| 
        ; branchcc occurs ; [] |635| 
	.dwpsn	file "../mms.c",line 636,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_LogNB ; [CPU_] |636| 
$C$DW$311	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$311, DW_AT_low_pc(0x00)
	.dwattr $C$DW$311, DW_AT_name("_PAR_GetLogNB")
	.dwattr $C$DW$311, DW_AT_TI_call
        LCR       #_PAR_GetLogNB        ; [CPU_] |636| 
        ; call occurs [#_PAR_GetLogNB] ; [] |636| 
	.dwpsn	file "../mms.c",line 637,column 3,is_stmt
        B         $C$L99,UNC            ; [CPU_] |637| 
        ; branch occurs ; [] |637| 
$C$L98:    
	.dwpsn	file "../mms.c",line 639,column 5,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       AL,@_TimeLogIndex     ; [CPU_] |639| 
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       @_ODV_Gateway_LogNB,AL ; [CPU_] |639| 
$C$L99:    
	.dwpsn	file "../mms.c",line 641,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |641| 
	.dwpsn	file "../mms.c",line 642,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$312	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$312, DW_AT_low_pc(0x00)
	.dwattr $C$DW$312, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$302, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$302, DW_AT_TI_end_line(0x282)
	.dwattr $C$DW$302, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$302

	.sect	".text"
	.global	_WriteTextCallback

$C$DW$313	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteTextCallback")
	.dwattr $C$DW$313, DW_AT_low_pc(_WriteTextCallback)
	.dwattr $C$DW$313, DW_AT_high_pc(0x00)
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_WriteTextCallback")
	.dwattr $C$DW$313, DW_AT_external
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$313, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$313, DW_AT_TI_begin_line(0x285)
	.dwattr $C$DW$313, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$313, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 645,column 96,is_stmt,address _WriteTextCallback

	.dwfde $C$DW$CIE, _WriteTextCallback
$C$DW$314	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$314, DW_AT_location[DW_OP_reg12]
$C$DW$315	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$315, DW_AT_location[DW_OP_reg14]
$C$DW$316	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$316, DW_AT_location[DW_OP_reg0]
$C$DW$317	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$317, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteTextCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteTextCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$318	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$318, DW_AT_location[DW_OP_breg20 -2]
$C$DW$319	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$319, DW_AT_location[DW_OP_breg20 -4]
$C$DW$320	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$320, DW_AT_location[DW_OP_breg20 -5]
$C$DW$321	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$321, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |645| 
        MOV       *-SP[5],AL            ; [CPU_] |645| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |645| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |645| 
	.dwpsn	file "../mms.c",line 647,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |647| 
        CMPB      AL,#1                 ; [CPU_] |647| 
        BF        $C$L100,NEQ           ; [CPU_] |647| 
        ; branchcc occurs ; [] |647| 
	.dwpsn	file "../mms.c",line 649,column 5,is_stmt
        MOVB      AL,#10                ; [CPU_] |649| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |649| 
        MOVB      AH,#8                 ; [CPU_] |649| 
        MOVB      XAR5,#0               ; [CPU_] |649| 
$C$DW$322	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$322, DW_AT_low_pc(0x00)
	.dwattr $C$DW$322, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$322, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |649| 
        ; call occurs [#_I2C_Command] ; [] |649| 
	.dwpsn	file "../mms.c",line 650,column 3,is_stmt
        B         $C$L101,UNC           ; [CPU_] |650| 
        ; branch occurs ; [] |650| 
$C$L100:    
	.dwpsn	file "../mms.c",line 653,column 5,is_stmt
        MOVB      AL,#9                 ; [CPU_] |653| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |653| 
        MOVB      AH,#8                 ; [CPU_] |653| 
        MOVB      XAR5,#0               ; [CPU_] |653| 
$C$DW$323	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$323, DW_AT_low_pc(0x00)
	.dwattr $C$DW$323, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$323, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |653| 
        ; call occurs [#_I2C_Command] ; [] |653| 
$C$L101:    
	.dwpsn	file "../mms.c",line 655,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |655| 
	.dwpsn	file "../mms.c",line 656,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$324	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$324, DW_AT_low_pc(0x00)
	.dwattr $C$DW$324, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$313, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$313, DW_AT_TI_end_line(0x290)
	.dwattr $C$DW$313, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$313

	.sect	".text"
	.global	_MotCurCallback

$C$DW$325	.dwtag  DW_TAG_subprogram, DW_AT_name("MotCurCallback")
	.dwattr $C$DW$325, DW_AT_low_pc(_MotCurCallback)
	.dwattr $C$DW$325, DW_AT_high_pc(0x00)
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_MotCurCallback")
	.dwattr $C$DW$325, DW_AT_external
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$325, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$325, DW_AT_TI_begin_line(0x292)
	.dwattr $C$DW$325, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$325, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 658,column 93,is_stmt,address _MotCurCallback

	.dwfde $C$DW$CIE, _MotCurCallback
$C$DW$326	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$326, DW_AT_location[DW_OP_reg12]
$C$DW$327	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$327, DW_AT_location[DW_OP_reg14]
$C$DW$328	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$328, DW_AT_location[DW_OP_reg0]
$C$DW$329	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$329, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MotCurCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MotCurCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$330	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$330, DW_AT_location[DW_OP_breg20 -2]
$C$DW$331	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$331, DW_AT_location[DW_OP_breg20 -4]
$C$DW$332	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$332, DW_AT_location[DW_OP_breg20 -5]
$C$DW$333	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$333, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |658| 
        MOV       *-SP[5],AL            ; [CPU_] |658| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |658| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |658| 
	.dwpsn	file "../mms.c",line 659,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |659| 
        CMPB      AL,#1                 ; [CPU_] |659| 
        BF        $C$L102,NEQ           ; [CPU_] |659| 
        ; branchcc occurs ; [] |659| 
	.dwpsn	file "../mms.c",line 660,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |660| 
        CMPB      AL,#2                 ; [CPU_] |660| 
        BF        $C$L102,NEQ           ; [CPU_] |660| 
        ; branchcc occurs ; [] |660| 
	.dwpsn	file "../mms.c",line 661,column 7,is_stmt
        MOVW      DP,#_HAL_NewCurPoint  ; [CPU_U] 
        MOVB      @_HAL_NewCurPoint,#1,UNC ; [CPU_] |661| 
$C$L102:    
	.dwpsn	file "../mms.c",line 664,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |664| 
	.dwpsn	file "../mms.c",line 665,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$334	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$334, DW_AT_low_pc(0x00)
	.dwattr $C$DW$334, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$325, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$325, DW_AT_TI_end_line(0x299)
	.dwattr $C$DW$325, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$325

	.sect	".text"
	.global	_SciSendCallback

$C$DW$335	.dwtag  DW_TAG_subprogram, DW_AT_name("SciSendCallback")
	.dwattr $C$DW$335, DW_AT_low_pc(_SciSendCallback)
	.dwattr $C$DW$335, DW_AT_high_pc(0x00)
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_SciSendCallback")
	.dwattr $C$DW$335, DW_AT_external
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$335, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$335, DW_AT_TI_begin_line(0x29c)
	.dwattr $C$DW$335, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$335, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 668,column 94,is_stmt,address _SciSendCallback

	.dwfde $C$DW$CIE, _SciSendCallback
$C$DW$336	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$336, DW_AT_location[DW_OP_reg12]
$C$DW$337	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$337, DW_AT_location[DW_OP_reg14]
$C$DW$338	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$338, DW_AT_location[DW_OP_reg0]
$C$DW$339	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$339, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SciSendCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SciSendCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$340	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$340, DW_AT_location[DW_OP_breg20 -2]
$C$DW$341	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$341, DW_AT_location[DW_OP_breg20 -4]
$C$DW$342	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$342, DW_AT_location[DW_OP_breg20 -5]
$C$DW$343	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$343, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |668| 
        MOV       *-SP[5],AL            ; [CPU_] |668| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |668| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |668| 
	.dwpsn	file "../mms.c",line 669,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |669| 
        CMPB      AL,#1                 ; [CPU_] |669| 
        BF        $C$L103,NEQ           ; [CPU_] |669| 
        ; branchcc occurs ; [] |669| 
	.dwpsn	file "../mms.c",line 670,column 5,is_stmt
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |670| 
        MOVL      XAR5,#_ODV_SciSend    ; [CPU_U] |670| 
        MOVB      AL,#0                 ; [CPU_] |670| 
$C$DW$344	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$344, DW_AT_low_pc(0x00)
	.dwattr $C$DW$344, DW_AT_name("_MBX_post")
	.dwattr $C$DW$344, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |670| 
        ; call occurs [#_MBX_post] ; [] |670| 
$C$L103:    
	.dwpsn	file "../mms.c",line 673,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |673| 
	.dwpsn	file "../mms.c",line 674,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$345	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$345, DW_AT_low_pc(0x00)
	.dwattr $C$DW$345, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$335, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$335, DW_AT_TI_end_line(0x2a2)
	.dwattr $C$DW$335, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$335

	.sect	".text"
	.global	_ConfigCallback

$C$DW$346	.dwtag  DW_TAG_subprogram, DW_AT_name("ConfigCallback")
	.dwattr $C$DW$346, DW_AT_low_pc(_ConfigCallback)
	.dwattr $C$DW$346, DW_AT_high_pc(0x00)
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_ConfigCallback")
	.dwattr $C$DW$346, DW_AT_external
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$346, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$346, DW_AT_TI_begin_line(0x2a4)
	.dwattr $C$DW$346, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$346, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 676,column 93,is_stmt,address _ConfigCallback

	.dwfde $C$DW$CIE, _ConfigCallback
$C$DW$347	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$347, DW_AT_location[DW_OP_reg12]
$C$DW$348	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$348, DW_AT_location[DW_OP_reg14]
$C$DW$349	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$349, DW_AT_location[DW_OP_reg0]
$C$DW$350	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$350, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ConfigCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ConfigCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$351	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$351, DW_AT_location[DW_OP_breg20 -2]
$C$DW$352	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$352, DW_AT_location[DW_OP_breg20 -4]
$C$DW$353	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$353, DW_AT_location[DW_OP_breg20 -5]
$C$DW$354	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$354, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |676| 
        MOV       *-SP[5],AL            ; [CPU_] |676| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |676| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |676| 
	.dwpsn	file "../mms.c",line 677,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |677| 
        CMPB      AL,#1                 ; [CPU_] |677| 
        BF        $C$L105,NEQ           ; [CPU_] |677| 
        ; branchcc occurs ; [] |677| 
	.dwpsn	file "../mms.c",line 678,column 5,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |678| 
        AND       AL,*+XAR4[0],#0x0080  ; [CPU_] |678| 
        LSR       AL,7                  ; [CPU_] |678| 
        CMPB      AL,#1                 ; [CPU_] |678| 
        BF        $C$L104,NEQ           ; [CPU_] |678| 
        ; branchcc occurs ; [] |678| 
	.dwpsn	file "../mms.c",line 678,column 35,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#1,UNC ; [CPU_] |678| 
$C$L104:    
	.dwpsn	file "../mms.c",line 679,column 5,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |679| 
        TBIT      *+XAR4[0],#7          ; [CPU_] |679| 
        BF        $C$L105,TC            ; [CPU_] |679| 
        ; branchcc occurs ; [] |679| 
	.dwpsn	file "../mms.c",line 679,column 35,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |679| 
$C$L105:    
	.dwpsn	file "../mms.c",line 681,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |681| 
	.dwpsn	file "../mms.c",line 682,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$355	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$355, DW_AT_low_pc(0x00)
	.dwattr $C$DW$355, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$346, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$346, DW_AT_TI_end_line(0x2aa)
	.dwattr $C$DW$346, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$346

	.sect	".text"
	.global	_BatteryCallBack

$C$DW$356	.dwtag  DW_TAG_subprogram, DW_AT_name("BatteryCallBack")
	.dwattr $C$DW$356, DW_AT_low_pc(_BatteryCallBack)
	.dwattr $C$DW$356, DW_AT_high_pc(0x00)
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_BatteryCallBack")
	.dwattr $C$DW$356, DW_AT_external
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$356, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$356, DW_AT_TI_begin_line(0x2ac)
	.dwattr $C$DW$356, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$356, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 684,column 94,is_stmt,address _BatteryCallBack

	.dwfde $C$DW$CIE, _BatteryCallBack
$C$DW$357	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$357, DW_AT_location[DW_OP_reg12]
$C$DW$358	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$358, DW_AT_location[DW_OP_reg14]
$C$DW$359	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$359, DW_AT_location[DW_OP_reg0]
$C$DW$360	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$360, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _BatteryCallBack              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_BatteryCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$361	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$361, DW_AT_location[DW_OP_breg20 -2]
$C$DW$362	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$362, DW_AT_location[DW_OP_breg20 -4]
$C$DW$363	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$363, DW_AT_location[DW_OP_breg20 -5]
$C$DW$364	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$364, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |684| 
        MOV       *-SP[5],AL            ; [CPU_] |684| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |684| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |684| 
	.dwpsn	file "../mms.c",line 685,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |685| 
        CMPB      AL,#1                 ; [CPU_] |685| 
        BF        $C$L106,NEQ           ; [CPU_] |685| 
        ; branchcc occurs ; [] |685| 
	.dwpsn	file "../mms.c",line 686,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |686| 
        CMPB      AL,#1                 ; [CPU_] |686| 
        BF        $C$L106,NEQ           ; [CPU_] |686| 
        ; branchcc occurs ; [] |686| 
	.dwpsn	file "../mms.c",line 689,column 7,is_stmt
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOV32     R0H,@_ODP_Battery_Capacity ; [CPU_] |689| 
        MPYF32    R0H,R0H,#17761        ; [CPU_] |689| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R0H,#16896        ; [CPU_] |689| 
$C$DW$365	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$365, DW_AT_low_pc(0x00)
	.dwattr $C$DW$365, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$365, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |689| 
        ; call occurs [#_CNV_Round] ; [] |689| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      @_PAR_Capacity_Total,ACC ; [CPU_] |689| 
	.dwpsn	file "../mms.c",line 691,column 7,is_stmt
        MOVW      DP,#_PAR_Capacity_TotalLife_Used ; [CPU_U] 
        ZAPA      ; [CPU_] |691| 
        MOVL      @_PAR_Capacity_TotalLife_Used,P ; [CPU_] |691| 
        MOVL      @_PAR_Capacity_TotalLife_Used+2,ACC ; [CPU_] |691| 
$C$L106:    
	.dwpsn	file "../mms.c",line 694,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |694| 
	.dwpsn	file "../mms.c",line 695,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$366	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$366, DW_AT_low_pc(0x00)
	.dwattr $C$DW$366, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$356, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$356, DW_AT_TI_end_line(0x2b7)
	.dwattr $C$DW$356, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$356

	.sect	".text"
	.global	_StartRecorderCallBack

$C$DW$367	.dwtag  DW_TAG_subprogram, DW_AT_name("StartRecorderCallBack")
	.dwattr $C$DW$367, DW_AT_low_pc(_StartRecorderCallBack)
	.dwattr $C$DW$367, DW_AT_high_pc(0x00)
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_StartRecorderCallBack")
	.dwattr $C$DW$367, DW_AT_external
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$367, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$367, DW_AT_TI_begin_line(0x2b9)
	.dwattr $C$DW$367, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$367, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 697,column 100,is_stmt,address _StartRecorderCallBack

	.dwfde $C$DW$CIE, _StartRecorderCallBack
$C$DW$368	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$368, DW_AT_location[DW_OP_reg12]
$C$DW$369	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$369, DW_AT_location[DW_OP_reg14]
$C$DW$370	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$370, DW_AT_location[DW_OP_reg0]
$C$DW$371	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$371, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _StartRecorderCallBack        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_StartRecorderCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$372	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$372, DW_AT_location[DW_OP_breg20 -2]
$C$DW$373	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$373, DW_AT_location[DW_OP_breg20 -4]
$C$DW$374	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$374, DW_AT_location[DW_OP_breg20 -5]
$C$DW$375	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$375, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |697| 
        MOV       *-SP[5],AL            ; [CPU_] |697| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |697| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |697| 
	.dwpsn	file "../mms.c",line 699,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |699| 
        CMPB      AL,#1                 ; [CPU_] |699| 
        BF        $C$L107,NEQ           ; [CPU_] |699| 
        ; branchcc occurs ; [] |699| 
	.dwpsn	file "../mms.c",line 700,column 5,is_stmt
$C$DW$376	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$376, DW_AT_low_pc(0x00)
	.dwattr $C$DW$376, DW_AT_name("_REC_StartRecorder")
	.dwattr $C$DW$376, DW_AT_TI_call
        LCR       #_REC_StartRecorder   ; [CPU_] |700| 
        ; call occurs [#_REC_StartRecorder] ; [] |700| 
$C$L107:    
	.dwpsn	file "../mms.c",line 702,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |702| 
	.dwpsn	file "../mms.c",line 703,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$377	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$377, DW_AT_low_pc(0x00)
	.dwattr $C$DW$377, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$367, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$367, DW_AT_TI_end_line(0x2bf)
	.dwattr $C$DW$367, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$367

	.sect	".text"
	.global	_MultiunitsCallback

$C$DW$378	.dwtag  DW_TAG_subprogram, DW_AT_name("MultiunitsCallback")
	.dwattr $C$DW$378, DW_AT_low_pc(_MultiunitsCallback)
	.dwattr $C$DW$378, DW_AT_high_pc(0x00)
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_MultiunitsCallback")
	.dwattr $C$DW$378, DW_AT_external
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$378, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$378, DW_AT_TI_begin_line(0x2c1)
	.dwattr $C$DW$378, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$378, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 705,column 97,is_stmt,address _MultiunitsCallback

	.dwfde $C$DW$CIE, _MultiunitsCallback
$C$DW$379	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$379, DW_AT_location[DW_OP_reg12]
$C$DW$380	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$380, DW_AT_location[DW_OP_reg14]
$C$DW$381	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$381, DW_AT_location[DW_OP_reg0]
$C$DW$382	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$382, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MultiunitsCallback           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MultiunitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$383	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$383, DW_AT_location[DW_OP_breg20 -2]
$C$DW$384	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$384, DW_AT_location[DW_OP_breg20 -4]
$C$DW$385	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$385, DW_AT_location[DW_OP_breg20 -5]
$C$DW$386	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$386, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |705| 
        MOV       *-SP[5],AL            ; [CPU_] |705| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |705| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |705| 
	.dwpsn	file "../mms.c",line 707,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |707| 
        CMPB      AL,#1                 ; [CPU_] |707| 
        BF        $C$L108,NEQ           ; [CPU_] |707| 
        ; branchcc occurs ; [] |707| 
	.dwpsn	file "../mms.c",line 708,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Multiunits ; [CPU_] |708| 
$C$DW$387	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$387, DW_AT_low_pc(0x00)
	.dwattr $C$DW$387, DW_AT_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$387, DW_AT_TI_call
        LCR       #_PAR_AddMultiUnits   ; [CPU_] |708| 
        ; call occurs [#_PAR_AddMultiUnits] ; [] |708| 
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       @_ODV_Recorder_Multiunits,AL ; [CPU_] |708| 
$C$L108:    
	.dwpsn	file "../mms.c",line 710,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |710| 
	.dwpsn	file "../mms.c",line 711,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$388	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$388, DW_AT_low_pc(0x00)
	.dwattr $C$DW$388, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$378, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$378, DW_AT_TI_end_line(0x2c7)
	.dwattr $C$DW$378, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$378

	.sect	".text"
	.global	_VariablesCallback

$C$DW$389	.dwtag  DW_TAG_subprogram, DW_AT_name("VariablesCallback")
	.dwattr $C$DW$389, DW_AT_low_pc(_VariablesCallback)
	.dwattr $C$DW$389, DW_AT_high_pc(0x00)
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_VariablesCallback")
	.dwattr $C$DW$389, DW_AT_external
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$389, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$389, DW_AT_TI_begin_line(0x2c9)
	.dwattr $C$DW$389, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$389, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 713,column 96,is_stmt,address _VariablesCallback

	.dwfde $C$DW$CIE, _VariablesCallback
$C$DW$390	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$390, DW_AT_location[DW_OP_reg12]
$C$DW$391	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$391, DW_AT_location[DW_OP_reg14]
$C$DW$392	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$392, DW_AT_location[DW_OP_reg0]
$C$DW$393	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$393, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VariablesCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VariablesCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$394	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$394, DW_AT_location[DW_OP_breg20 -2]
$C$DW$395	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$395, DW_AT_location[DW_OP_breg20 -4]
$C$DW$396	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$396, DW_AT_location[DW_OP_breg20 -5]
$C$DW$397	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$397, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |713| 
        MOV       *-SP[5],AL            ; [CPU_] |713| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |713| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |713| 
	.dwpsn	file "../mms.c",line 715,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |715| 
        CMPB      AL,#1                 ; [CPU_] |715| 
        BF        $C$L109,NEQ           ; [CPU_] |715| 
        ; branchcc occurs ; [] |715| 
	.dwpsn	file "../mms.c",line 716,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Variables ; [CPU_] |716| 
$C$DW$398	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$398, DW_AT_low_pc(0x00)
	.dwattr $C$DW$398, DW_AT_name("_PAR_AddVariables")
	.dwattr $C$DW$398, DW_AT_TI_call
        LCR       #_PAR_AddVariables    ; [CPU_] |716| 
        ; call occurs [#_PAR_AddVariables] ; [] |716| 
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       @_ODV_Recorder_Variables,AL ; [CPU_] |716| 
$C$L109:    
	.dwpsn	file "../mms.c",line 718,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |718| 
	.dwpsn	file "../mms.c",line 719,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$399	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$399, DW_AT_low_pc(0x00)
	.dwattr $C$DW$399, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$389, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$389, DW_AT_TI_end_line(0x2cf)
	.dwattr $C$DW$389, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$389

	.sect	".text"
	.global	_WriteOutputs8BitCallback

$C$DW$400	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs8BitCallback")
	.dwattr $C$DW$400, DW_AT_low_pc(_WriteOutputs8BitCallback)
	.dwattr $C$DW$400, DW_AT_high_pc(0x00)
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_WriteOutputs8BitCallback")
	.dwattr $C$DW$400, DW_AT_external
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$400, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$400, DW_AT_TI_begin_line(0x2d2)
	.dwattr $C$DW$400, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$400, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 722,column 103,is_stmt,address _WriteOutputs8BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs8BitCallback
$C$DW$401	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$401, DW_AT_location[DW_OP_reg12]
$C$DW$402	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$402, DW_AT_location[DW_OP_reg14]
$C$DW$403	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$403, DW_AT_location[DW_OP_reg0]
$C$DW$404	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$404, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs8BitCallback     FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs8BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$405	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$405, DW_AT_location[DW_OP_breg20 -2]
$C$DW$406	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$406, DW_AT_location[DW_OP_breg20 -4]
$C$DW$407	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$407, DW_AT_location[DW_OP_breg20 -5]
$C$DW$408	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$408, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |722| 
        MOV       *-SP[5],AL            ; [CPU_] |722| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |722| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |722| 
	.dwpsn	file "../mms.c",line 723,column 3,is_stmt
	.dwpsn	file "../mms.c",line 724,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |724| 
	.dwpsn	file "../mms.c",line 725,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$409	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$409, DW_AT_low_pc(0x00)
	.dwattr $C$DW$409, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$400, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$400, DW_AT_TI_end_line(0x2d5)
	.dwattr $C$DW$400, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$400

	.sect	".text"
	.global	_WriteOutputs16BitCallback

$C$DW$410	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs16BitCallback")
	.dwattr $C$DW$410, DW_AT_low_pc(_WriteOutputs16BitCallback)
	.dwattr $C$DW$410, DW_AT_high_pc(0x00)
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_WriteOutputs16BitCallback")
	.dwattr $C$DW$410, DW_AT_external
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$410, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$410, DW_AT_TI_begin_line(0x2d7)
	.dwattr $C$DW$410, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$410, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 727,column 104,is_stmt,address _WriteOutputs16BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs16BitCallback
$C$DW$411	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$411, DW_AT_location[DW_OP_reg12]
$C$DW$412	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$412, DW_AT_location[DW_OP_reg14]
$C$DW$413	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$413, DW_AT_location[DW_OP_reg0]
$C$DW$414	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$414, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs16BitCallback    FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs16BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$415	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$415, DW_AT_location[DW_OP_breg20 -2]
$C$DW$416	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$416, DW_AT_location[DW_OP_breg20 -4]
$C$DW$417	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$417, DW_AT_location[DW_OP_breg20 -5]
$C$DW$418	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$418, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |727| 
        MOV       *-SP[5],AL            ; [CPU_] |727| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |727| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |727| 
	.dwpsn	file "../mms.c",line 728,column 3,is_stmt
	.dwpsn	file "../mms.c",line 731,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |731| 
	.dwpsn	file "../mms.c",line 732,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$419	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$419, DW_AT_low_pc(0x00)
	.dwattr $C$DW$419, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$410, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$410, DW_AT_TI_end_line(0x2dc)
	.dwattr $C$DW$410, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$410

	.sect	".text"
	.global	_ReadInputs8BitsCallback

$C$DW$420	.dwtag  DW_TAG_subprogram, DW_AT_name("ReadInputs8BitsCallback")
	.dwattr $C$DW$420, DW_AT_low_pc(_ReadInputs8BitsCallback)
	.dwattr $C$DW$420, DW_AT_high_pc(0x00)
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_ReadInputs8BitsCallback")
	.dwattr $C$DW$420, DW_AT_external
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$420, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$420, DW_AT_TI_begin_line(0x2de)
	.dwattr $C$DW$420, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$420, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 734,column 102,is_stmt,address _ReadInputs8BitsCallback

	.dwfde $C$DW$CIE, _ReadInputs8BitsCallback
$C$DW$421	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$421, DW_AT_location[DW_OP_reg12]
$C$DW$422	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$422, DW_AT_location[DW_OP_reg14]
$C$DW$423	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$423, DW_AT_location[DW_OP_reg0]
$C$DW$424	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$424, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ReadInputs8BitsCallback      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ReadInputs8BitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$425	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$425, DW_AT_location[DW_OP_breg20 -2]
$C$DW$426	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$426, DW_AT_location[DW_OP_breg20 -4]
$C$DW$427	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$427, DW_AT_location[DW_OP_breg20 -5]
$C$DW$428	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$428, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |734| 
        MOV       *-SP[5],AL            ; [CPU_] |734| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |734| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |734| 
	.dwpsn	file "../mms.c",line 735,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |735| 
	.dwpsn	file "../mms.c",line 736,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$429	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$429, DW_AT_low_pc(0x00)
	.dwattr $C$DW$429, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$420, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$420, DW_AT_TI_end_line(0x2e0)
	.dwattr $C$DW$420, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$420

	.sect	".text"
	.global	_ControlWordCallBack

$C$DW$430	.dwtag  DW_TAG_subprogram, DW_AT_name("ControlWordCallBack")
	.dwattr $C$DW$430, DW_AT_low_pc(_ControlWordCallBack)
	.dwattr $C$DW$430, DW_AT_high_pc(0x00)
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_ControlWordCallBack")
	.dwattr $C$DW$430, DW_AT_external
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$430, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$430, DW_AT_TI_begin_line(0x2e2)
	.dwattr $C$DW$430, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$430, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 738,column 98,is_stmt,address _ControlWordCallBack

	.dwfde $C$DW$CIE, _ControlWordCallBack
$C$DW$431	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$431, DW_AT_location[DW_OP_reg12]
$C$DW$432	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$432, DW_AT_location[DW_OP_reg14]
$C$DW$433	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$433, DW_AT_location[DW_OP_reg0]
$C$DW$434	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$434, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ControlWordCallBack          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ControlWordCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$435	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$435, DW_AT_location[DW_OP_breg20 -2]
$C$DW$436	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$436, DW_AT_location[DW_OP_breg20 -4]
$C$DW$437	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$437, DW_AT_location[DW_OP_breg20 -5]
$C$DW$438	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$438, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |738| 
        MOV       *-SP[5],AL            ; [CPU_] |738| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |738| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |738| 
	.dwpsn	file "../mms.c",line 739,column 3,is_stmt
	.dwpsn	file "../mms.c",line 740,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |740| 
	.dwpsn	file "../mms.c",line 741,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$439	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$439, DW_AT_low_pc(0x00)
	.dwattr $C$DW$439, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$430, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$430, DW_AT_TI_end_line(0x2e5)
	.dwattr $C$DW$430, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$430

	.sect	".text"
	.global	_VersionCallback

$C$DW$440	.dwtag  DW_TAG_subprogram, DW_AT_name("VersionCallback")
	.dwattr $C$DW$440, DW_AT_low_pc(_VersionCallback)
	.dwattr $C$DW$440, DW_AT_high_pc(0x00)
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_VersionCallback")
	.dwattr $C$DW$440, DW_AT_external
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$440, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$440, DW_AT_TI_begin_line(0x2e7)
	.dwattr $C$DW$440, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$440, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 743,column 94,is_stmt,address _VersionCallback

	.dwfde $C$DW$CIE, _VersionCallback
$C$DW$441	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$441, DW_AT_location[DW_OP_reg12]
$C$DW$442	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$442, DW_AT_location[DW_OP_reg14]
$C$DW$443	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$443, DW_AT_location[DW_OP_reg0]
$C$DW$444	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$444, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VersionCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VersionCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$445	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$445, DW_AT_location[DW_OP_breg20 -2]
$C$DW$446	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$446, DW_AT_location[DW_OP_breg20 -4]
$C$DW$447	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$447, DW_AT_location[DW_OP_breg20 -5]
$C$DW$448	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$448, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |743| 
        MOV       *-SP[5],AL            ; [CPU_] |743| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |743| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |743| 
	.dwpsn	file "../mms.c",line 745,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |745| 
	.dwpsn	file "../mms.c",line 746,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$449	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$449, DW_AT_low_pc(0x00)
	.dwattr $C$DW$449, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$440, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$440, DW_AT_TI_end_line(0x2ea)
	.dwattr $C$DW$440, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$440

	.sect	".text"
	.global	_WriteAnalogueOutputsCallback

$C$DW$450	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteAnalogueOutputsCallback")
	.dwattr $C$DW$450, DW_AT_low_pc(_WriteAnalogueOutputsCallback)
	.dwattr $C$DW$450, DW_AT_high_pc(0x00)
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_WriteAnalogueOutputsCallback")
	.dwattr $C$DW$450, DW_AT_external
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$450, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$450, DW_AT_TI_begin_line(0x2ec)
	.dwattr $C$DW$450, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$450, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 748,column 107,is_stmt,address _WriteAnalogueOutputsCallback

	.dwfde $C$DW$CIE, _WriteAnalogueOutputsCallback
$C$DW$451	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$451, DW_AT_location[DW_OP_reg12]
$C$DW$452	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$452, DW_AT_location[DW_OP_reg14]
$C$DW$453	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$453, DW_AT_location[DW_OP_reg0]
$C$DW$454	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$454, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteAnalogueOutputsCallback FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteAnalogueOutputsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$455	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$455, DW_AT_location[DW_OP_breg20 -2]
$C$DW$456	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$456, DW_AT_location[DW_OP_breg20 -4]
$C$DW$457	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$457, DW_AT_location[DW_OP_breg20 -5]
$C$DW$458	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$458, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |748| 
        MOV       *-SP[5],AL            ; [CPU_] |748| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |748| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |748| 
	.dwpsn	file "../mms.c",line 750,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |750| 
	.dwpsn	file "../mms.c",line 751,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$459	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$459, DW_AT_low_pc(0x00)
	.dwattr $C$DW$459, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$450, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$450, DW_AT_TI_end_line(0x2ef)
	.dwattr $C$DW$450, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$450

	.sect	".text"
	.global	_CommErrorSetCallback

$C$DW$460	.dwtag  DW_TAG_subprogram, DW_AT_name("CommErrorSetCallback")
	.dwattr $C$DW$460, DW_AT_low_pc(_CommErrorSetCallback)
	.dwattr $C$DW$460, DW_AT_high_pc(0x00)
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_CommErrorSetCallback")
	.dwattr $C$DW$460, DW_AT_external
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$460, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$460, DW_AT_TI_begin_line(0x2f2)
	.dwattr $C$DW$460, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$460, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 754,column 99,is_stmt,address _CommErrorSetCallback

	.dwfde $C$DW$CIE, _CommErrorSetCallback
$C$DW$461	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$461, DW_AT_location[DW_OP_reg12]
$C$DW$462	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$462, DW_AT_location[DW_OP_reg14]
$C$DW$463	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$463, DW_AT_location[DW_OP_reg0]
$C$DW$464	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$464, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _CommErrorSetCallback         FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_CommErrorSetCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$465	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$465, DW_AT_location[DW_OP_breg20 -2]
$C$DW$466	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$466, DW_AT_location[DW_OP_breg20 -4]
$C$DW$467	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$467, DW_AT_location[DW_OP_breg20 -5]
$C$DW$468	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$468, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |754| 
        MOV       *-SP[5],AL            ; [CPU_] |754| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |754| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |754| 
	.dwpsn	file "../mms.c",line 755,column 3,is_stmt
	.dwpsn	file "../mms.c",line 758,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |758| 
	.dwpsn	file "../mms.c",line 759,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$469	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$469, DW_AT_low_pc(0x00)
	.dwattr $C$DW$469, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$460, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$460, DW_AT_TI_end_line(0x2f7)
	.dwattr $C$DW$460, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$460

	.sect	".text"
	.global	_SaveAllParameters

$C$DW$470	.dwtag  DW_TAG_subprogram, DW_AT_name("SaveAllParameters")
	.dwattr $C$DW$470, DW_AT_low_pc(_SaveAllParameters)
	.dwattr $C$DW$470, DW_AT_high_pc(0x00)
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_SaveAllParameters")
	.dwattr $C$DW$470, DW_AT_external
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$470, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$470, DW_AT_TI_begin_line(0x2f9)
	.dwattr $C$DW$470, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$470, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 761,column 96,is_stmt,address _SaveAllParameters

	.dwfde $C$DW$CIE, _SaveAllParameters
$C$DW$471	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$471, DW_AT_location[DW_OP_reg12]
$C$DW$472	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$472, DW_AT_location[DW_OP_reg14]
$C$DW$473	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$473, DW_AT_location[DW_OP_reg0]
$C$DW$474	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$474, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SaveAllParameters            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SaveAllParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$475	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$475, DW_AT_location[DW_OP_breg20 -2]
$C$DW$476	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$476, DW_AT_location[DW_OP_breg20 -4]
$C$DW$477	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$477, DW_AT_location[DW_OP_breg20 -5]
$C$DW$478	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$478, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |761| 
        MOV       *-SP[5],AL            ; [CPU_] |761| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |761| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |761| 
	.dwpsn	file "../mms.c",line 762,column 3,is_stmt
        MOVW      DP,#_ODV_StoreParameters ; [CPU_U] 
        MOV       AL,#30309             ; [CPU_] |762| 
        MOV       AH,#29537             ; [CPU_] |762| 
        CMPL      ACC,@_ODV_StoreParameters ; [CPU_] |762| 
        BF        $C$L110,NEQ           ; [CPU_] |762| 
        ; branchcc occurs ; [] |762| 
	.dwpsn	file "../mms.c",line 763,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |763| 
        MOVL      @_ODV_StoreParameters,ACC ; [CPU_] |763| 
	.dwpsn	file "../mms.c",line 764,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |764| 
$C$DW$479	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$479, DW_AT_low_pc(0x00)
	.dwattr $C$DW$479, DW_AT_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$479, DW_AT_TI_call
        LCR       #_PAR_WriteAllPermanentParam ; [CPU_] |764| 
        ; call occurs [#_PAR_WriteAllPermanentParam] ; [] |764| 
$C$L110:    
	.dwpsn	file "../mms.c",line 766,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |766| 
	.dwpsn	file "../mms.c",line 767,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$480	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$480, DW_AT_low_pc(0x00)
	.dwattr $C$DW$480, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$470, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$470, DW_AT_TI_end_line(0x2ff)
	.dwattr $C$DW$470, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$470

	.sect	".text"
	.global	_LoadDefaultParameters

$C$DW$481	.dwtag  DW_TAG_subprogram, DW_AT_name("LoadDefaultParameters")
	.dwattr $C$DW$481, DW_AT_low_pc(_LoadDefaultParameters)
	.dwattr $C$DW$481, DW_AT_high_pc(0x00)
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_LoadDefaultParameters")
	.dwattr $C$DW$481, DW_AT_external
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$481, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$481, DW_AT_TI_begin_line(0x301)
	.dwattr $C$DW$481, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$481, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 769,column 100,is_stmt,address _LoadDefaultParameters

	.dwfde $C$DW$CIE, _LoadDefaultParameters
$C$DW$482	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$482, DW_AT_location[DW_OP_reg12]
$C$DW$483	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$483, DW_AT_location[DW_OP_reg14]
$C$DW$484	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$484, DW_AT_location[DW_OP_reg0]
$C$DW$485	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$485, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LoadDefaultParameters        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LoadDefaultParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$486	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$486, DW_AT_location[DW_OP_breg20 -2]
$C$DW$487	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$487, DW_AT_location[DW_OP_breg20 -4]
$C$DW$488	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$488, DW_AT_location[DW_OP_breg20 -5]
$C$DW$489	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$489, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |769| 
        MOV       *-SP[5],AL            ; [CPU_] |769| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |769| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |769| 
	.dwpsn	file "../mms.c",line 770,column 3,is_stmt
        MOVW      DP,#_ODV_RestoreDefaultParameters ; [CPU_U] 
        MOV       AL,#24932             ; [CPU_] |770| 
        MOV       AH,#27759             ; [CPU_] |770| 
        CMPL      ACC,@_ODV_RestoreDefaultParameters ; [CPU_] |770| 
        BF        $C$L111,NEQ           ; [CPU_] |770| 
        ; branchcc occurs ; [] |770| 
	.dwpsn	file "../mms.c",line 771,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |771| 
        MOVL      @_ODV_RestoreDefaultParameters,ACC ; [CPU_] |771| 
	.dwpsn	file "../mms.c",line 772,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |772| 
$C$DW$490	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$490, DW_AT_low_pc(0x00)
	.dwattr $C$DW$490, DW_AT_name("_PAR_UpdateCode")
	.dwattr $C$DW$490, DW_AT_TI_call
        LCR       #_PAR_UpdateCode      ; [CPU_] |772| 
        ; call occurs [#_PAR_UpdateCode] ; [] |772| 
	.dwpsn	file "../mms.c",line 773,column 5,is_stmt
        MOVW      DP,#_BootST           ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |773| 
        MOVL      @_BootST,ACC          ; [CPU_] |773| 
$C$L111:    
	.dwpsn	file "../mms.c",line 775,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |775| 
	.dwpsn	file "../mms.c",line 776,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$491	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$491, DW_AT_low_pc(0x00)
	.dwattr $C$DW$491, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$481, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$481, DW_AT_TI_end_line(0x308)
	.dwattr $C$DW$481, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$481

	.sect	".text"
	.global	_DebugCallBack

$C$DW$492	.dwtag  DW_TAG_subprogram, DW_AT_name("DebugCallBack")
	.dwattr $C$DW$492, DW_AT_low_pc(_DebugCallBack)
	.dwattr $C$DW$492, DW_AT_high_pc(0x00)
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_DebugCallBack")
	.dwattr $C$DW$492, DW_AT_external
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$492, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$492, DW_AT_TI_begin_line(0x30b)
	.dwattr $C$DW$492, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$492, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 779,column 92,is_stmt,address _DebugCallBack

	.dwfde $C$DW$CIE, _DebugCallBack
$C$DW$493	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$493, DW_AT_location[DW_OP_reg12]
$C$DW$494	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$494, DW_AT_location[DW_OP_reg14]
$C$DW$495	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$495, DW_AT_location[DW_OP_reg0]
$C$DW$496	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$496, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _DebugCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_DebugCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$497	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$497, DW_AT_location[DW_OP_breg20 -2]
$C$DW$498	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$498, DW_AT_location[DW_OP_breg20 -4]
$C$DW$499	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$499, DW_AT_location[DW_OP_breg20 -5]
$C$DW$500	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$500, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |779| 
        MOV       *-SP[5],AL            ; [CPU_] |779| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |779| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |779| 
	.dwpsn	file "../mms.c",line 780,column 3,is_stmt
        MOVW      DP,#_ODV_Debug        ; [CPU_U] 
        MOV       @_ODV_Debug,#0        ; [CPU_] |780| 
	.dwpsn	file "../mms.c",line 781,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |781| 
	.dwpsn	file "../mms.c",line 782,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$501	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$501, DW_AT_low_pc(0x00)
	.dwattr $C$DW$501, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$492, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$492, DW_AT_TI_end_line(0x30e)
	.dwattr $C$DW$492, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$492

	.sect	".text"
	.global	_ResetCallBack

$C$DW$502	.dwtag  DW_TAG_subprogram, DW_AT_name("ResetCallBack")
	.dwattr $C$DW$502, DW_AT_low_pc(_ResetCallBack)
	.dwattr $C$DW$502, DW_AT_high_pc(0x00)
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_ResetCallBack")
	.dwattr $C$DW$502, DW_AT_external
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$502, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$502, DW_AT_TI_begin_line(0x310)
	.dwattr $C$DW$502, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$502, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 784,column 92,is_stmt,address _ResetCallBack

	.dwfde $C$DW$CIE, _ResetCallBack
$C$DW$503	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$503, DW_AT_location[DW_OP_reg12]
$C$DW$504	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$504, DW_AT_location[DW_OP_reg14]
$C$DW$505	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$505, DW_AT_location[DW_OP_reg0]
$C$DW$506	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$506, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ResetCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ResetCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$507	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$507, DW_AT_location[DW_OP_breg20 -2]
$C$DW$508	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$508, DW_AT_location[DW_OP_breg20 -4]
$C$DW$509	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$509, DW_AT_location[DW_OP_breg20 -5]
$C$DW$510	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$510, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |784| 
        MOV       *-SP[5],AL            ; [CPU_] |784| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |784| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |784| 
	.dwpsn	file "../mms.c",line 785,column 3,is_stmt
        MOVW      DP,#_ODV_ResetHW      ; [CPU_U] 
        MOV       AL,#29295             ; [CPU_] |785| 
        MOV       AH,#31333             ; [CPU_] |785| 
        CMPL      ACC,@_ODV_ResetHW     ; [CPU_] |785| 
        BF        $C$L112,NEQ           ; [CPU_] |785| 
        ; branchcc occurs ; [] |785| 
	.dwpsn	file "../mms.c",line 786,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |786| 
        MOVL      @_ODV_ResetHW,ACC     ; [CPU_] |786| 
	.dwpsn	file "../mms.c",line 787,column 5,is_stmt
        MOVW      DP,#_BootST           ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |787| 
        MOVL      @_BootST,ACC          ; [CPU_] |787| 
$C$L112:    
	.dwpsn	file "../mms.c",line 789,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |789| 
	.dwpsn	file "../mms.c",line 790,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$511	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$511, DW_AT_low_pc(0x00)
	.dwattr $C$DW$511, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$502, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$502, DW_AT_TI_end_line(0x316)
	.dwattr $C$DW$502, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$502

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_PAR_GetLogNB
	.global	_PAR_AddLog
	.global	_genCRC32Table
	.global	_PAR_SetParamDependantVars
	.global	_WARN_ErrorParam
	.global	_ERR_ErrorDigOvld
	.global	_ERR_ErrorUnderVoltage
	.global	_ERR_ErrorOverCurrent
	.global	_USB_Unlock
	.global	_ERR_HandleWarning
	.global	_HAL_Random
	.global	_HAL_Unlock
	.global	_USB_Stop
	.global	_HAL_Reset
	.global	_setNodeId
	.global	_REC_StartRecorder
	.global	_HAL_Init
	.global	_canInit
	.global	_RS232ReceiveEnable
	.global	_SCI1_Command
	.global	_SCI1_Receive
	.global	_ODP_CommError_TimeOut
	.global	_ODP_SafetyLimits_Low_Voltage_Current_Allow
	.global	_ODV_CommError_Set
	.global	_ODP_SafetyLimits_Temp_Delay
	.global	_ODP_Current_C_D_Mode
	.global	_ODV_MachineMode
	.global	_ODV_Current_DischargeAllowed
	.global	_ODP_SafetyLimits_Low_Voltage_Charge_Delay
	.global	_ODP_SafetyLimits_Low_Voltage_Current_Delay
	.global	_ODV_Current_ChargeAllowed
	.global	_ODV_MachineEvent
	.global	_ODP_SafetyLimits_SleepTimeout
	.global	_ODP_SafetyLimits_Umax
	.global	_ODV_Gateway_LogNB
	.global	_ODP_SafetyLimits_Umax_bal_delta
	.global	_ODV_SciSend
	.global	_ODV_Recorder_Variables
	.global	_ODP_SafetyLimits_Umin
	.global	_ODP_SafetyLimits_Voltage_delay
	.global	_ODP_SafetyLimits_UnderVoltage
	.global	_ODP_SafetyLimits_UnderCurrent
	.global	_ODP_SafetyLimits_Charge_In_Thres_Cur
	.global	_ODV_Gateway_State
	.global	_ODP_SafetyLimits_Current_delay
	.global	_ODP_SafetyLimits_Overcurrent
	.global	_ODV_Module1_MaxDeltaVoltage
	.global	_ODV_Module1_MaxCellVoltage
	.global	_ODV_Controlword
	.global	_ODV_Module1_Current
	.global	_ODV_Module1_SOC
	.global	_ODV_Debug
	.global	_SCI_MsgAvailable
	.global	_HAL_Enable
	.global	_SCI_Available
	.global	_HAL_NewCurPoint
	.global	_ODV_Module1_MinCellVoltage
	.global	_ODP_SleepCurrent
	.global	_ODP_Board_BaudRate
	.global	_ODP_CommError_LowVoltage_ErrCounter
	.global	_ODP_CommError_Delay
	.global	_ODV_Module1_Status
	.global	_HAL_CellOK
	.global	_ODP_Board_Config
	.global	_PAR_InitParam
	.global	_PAR_UpdateCode
	.global	_ERR_ClearError
	.global	_ERR_ClearWarning
	.global	_I2C_CommandNoWait
	.global	_USB_Start
	.global	_I2C_Command
	.global	_PAR_AddMultiUnits
	.global	_ODV_Recorder_Multiunits
	.global	_MBX_post
	.global	_ODP_VersionParameters
	.global	_MBX_pend
	.global	_SEM_pend
	.global	_PAR_AddVariables
	.global	_setState
	.global	_TimeLogIndex
	.global	_CNV_Round
	.global	_ODV_StoreParameters
	.global	_ODV_RestoreDefaultParameters
	.global	_ODP_Battery_Capacity
	.global	_ODP_RandomNB
	.global	_ODV_SysTick_ms
	.global	_PAR_StoreODSubIndex
	.global	_MMSConfig
	.global	_PAR_WriteAllPermanentParam
	.global	_PAR_Capacity_Total
	.global	_ODV_ResetHW
	.global	_getCRC32_cpu
	.global	_PAR_WriteStatisticParam
	.global	_ODV_Write_Outputs_16_Bit
	.global	_ODV_Gateway_Errorcode
	.global	_ODV_ErrorDsp_ErrorNumber
	.global	_ODP_OnTime
	.global	_ODP_Board_RevisionNumber
	.global	_ODV_CommError_Counter
	.global	_ODV_Gateway_Date_Time
	.global	_PAR_Capacity_TotalLife_Used
	.global	_ODV_RTC_Text
	.global	_golden_CRC_values
	.global	_TSK_timerSem
	.global	_PieCtrlRegs
	.global	_can_tx_mbox
	.global	_mailboxSDOout
	.global	_can_rx_mbox
	.global	_sci_rx_mbox
	.global	_SCI_MsgInRS232
	.global	_ODI_mms_dict_Data
	.global	FS$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$140	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$140, DW_AT_byte_size(0x01)
$C$DW$512	.dwtag  DW_TAG_enumerator, DW_AT_name("EVEN"), DW_AT_const_value(0x00)
$C$DW$513	.dwtag  DW_TAG_enumerator, DW_AT_name("ODD"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$140

$C$DW$T$141	.dwtag  DW_TAG_typedef, DW_AT_name("parity_t")
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$T$141, DW_AT_language(DW_LANG_C)

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$514, DW_AT_name("cob_id")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$515, DW_AT_name("rtr")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$516, DW_AT_name("len")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$517, DW_AT_name("data")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$131	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$131, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$518, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$519, DW_AT_name("csSDO")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$520, DW_AT_name("csEmergency")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$521, DW_AT_name("csSYNC")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$522, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$523, DW_AT_name("csPDO")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$524, DW_AT_name("csLSS")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$88	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$88, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$525, DW_AT_name("errCode")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$526, DW_AT_name("errRegMask")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$527, DW_AT_name("active")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$122	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)

$C$DW$T$123	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$123, DW_AT_byte_size(0x18)
$C$DW$528	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$528, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$123


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$529, DW_AT_name("index")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$530, DW_AT_name("subindex")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$531, DW_AT_name("size")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$532, DW_AT_name("address")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$128	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$128, DW_AT_language(DW_LANG_C)
$C$DW$T$129	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$T$129, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$533, DW_AT_name("SwitchOn")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$533, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$534, DW_AT_name("EnableVolt")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$534, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$535, DW_AT_name("QuickStop")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$535, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$536, DW_AT_name("EnableOperation")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$536, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$537, DW_AT_name("OpModeSpecific")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_OpModeSpecific")
	.dwattr $C$DW$537, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$538, DW_AT_name("ResetFault")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$538, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$539, DW_AT_name("Halt")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$539, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$540, DW_AT_name("Oms")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$540, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$541, DW_AT_name("Rsvd")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$541, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$542, DW_AT_name("Manufacturer")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$542, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$543, DW_AT_name("SwitchOn")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$543, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$544, DW_AT_name("EnableVolt")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$544, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$545, DW_AT_name("QuickStop")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$545, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$546, DW_AT_name("EnableOperation")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$546, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$547, DW_AT_name("Rsvd0")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_Rsvd0")
	.dwattr $C$DW$547, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$548, DW_AT_name("ResetFault")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$548, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$549, DW_AT_name("Halt")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$549, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$550, DW_AT_name("Oms")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$550, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$551, DW_AT_name("Rsvd")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$551, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$552, DW_AT_name("Manufacturer")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$552, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x01)
$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$553, DW_AT_name("SwitchOn")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$553, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$554, DW_AT_name("EnableVolt")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$554, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$555, DW_AT_name("QuickStop")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$555, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$556, DW_AT_name("EnableOperation")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$556, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$557, DW_AT_name("NewSetPoint")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_NewSetPoint")
	.dwattr $C$DW$557, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$558, DW_AT_name("ChangeSetImm")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_ChangeSetImm")
	.dwattr $C$DW$558, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$559, DW_AT_name("Abs_Rel")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_Abs_Rel")
	.dwattr $C$DW$559, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$560, DW_AT_name("ResetFault")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$560, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$561, DW_AT_name("Halt")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$561, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$562, DW_AT_name("ChangeSetPos")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_ChangeSetPos")
	.dwattr $C$DW$562, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$563, DW_AT_name("Rsvd")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$563, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$564, DW_AT_name("Manufacturer")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$564, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$565, DW_AT_name("can_wk")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_can_wk")
	.dwattr $C$DW$565, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$566, DW_AT_name("sw_wk")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_sw_wk")
	.dwattr $C$DW$566, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$567, DW_AT_name("bal_dic")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_bal_dic")
	.dwattr $C$DW$567, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$568, DW_AT_name("bal_ch")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_bal_ch")
	.dwattr $C$DW$568, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$569, DW_AT_name("bal_ocv")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_bal_ocv")
	.dwattr $C$DW$569, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$570, DW_AT_name("lem")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_lem")
	.dwattr $C$DW$570, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$571, DW_AT_name("shunt")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_shunt")
	.dwattr $C$DW$571, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$572, DW_AT_name("relay_on")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_relay_on")
	.dwattr $C$DW$572, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$573, DW_AT_name("ch_wk")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_ch_wk")
	.dwattr $C$DW$573, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$574, DW_AT_name("SOC2")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_SOC2")
	.dwattr $C$DW$574, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$575, DW_AT_name("foil")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_foil")
	.dwattr $C$DW$575, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$576, DW_AT_name("rel_sta")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_rel_sta")
	.dwattr $C$DW$576, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$577, DW_AT_name("b12")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_b12")
	.dwattr $C$DW$577, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$578, DW_AT_name("b13")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_b13")
	.dwattr $C$DW$578, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$579, DW_AT_name("b14")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_b14")
	.dwattr $C$DW$579, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$580, DW_AT_name("b15")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_b15")
	.dwattr $C$DW$580, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27

$C$DW$T$142	.dwtag  DW_TAG_typedef, DW_AT_name("TMMSConfig")
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$142, DW_AT_language(DW_LANG_C)
$C$DW$T$143	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$143, DW_AT_address_class(0x16)

$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x48)
$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$581, DW_AT_name("cmd")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_cmd")
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$582, DW_AT_name("len")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$583, DW_AT_name("data")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29

$C$DW$T$144	.dwtag  DW_TAG_typedef, DW_AT_name("T_UsbMessage")
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$144, DW_AT_language(DW_LANG_C)

$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x04)
$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$584, DW_AT_name("Command")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_Command")
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$585, DW_AT_name("NodeId")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_NodeId")
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30

$C$DW$T$145	.dwtag  DW_TAG_typedef, DW_AT_name("TBoot")
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$145, DW_AT_language(DW_LANG_C)

$C$DW$T$31	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$586, DW_AT_name("ControlWord")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_ControlWord")
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$587, DW_AT_name("AnyMode")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_AnyMode")
	.dwattr $C$DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$588, DW_AT_name("VelocityMode")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_VelocityMode")
	.dwattr $C$DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$589, DW_AT_name("PositionMode")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_PositionMode")
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31

$C$DW$T$146	.dwtag  DW_TAG_typedef, DW_AT_name("TControlword")
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$146, DW_AT_language(DW_LANG_C)

$C$DW$T$39	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$39, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x08)
$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$590, DW_AT_name("wListElem")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$591, DW_AT_name("wCount")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$592, DW_AT_name("fxn")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$39

$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$34	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$34, DW_AT_address_class(0x16)
$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)

$C$DW$T$46	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$46, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x30)
$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$593, DW_AT_name("dataQue")
	.dwattr $C$DW$593, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$594, DW_AT_name("freeQue")
	.dwattr $C$DW$594, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$595, DW_AT_name("dataSem")
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$596, DW_AT_name("freeSem")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$597, DW_AT_name("segid")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$598, DW_AT_name("size")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$599, DW_AT_name("length")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$600, DW_AT_name("name")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$46

$C$DW$T$147	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$147, DW_AT_language(DW_LANG_C)
$C$DW$T$149	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$149, DW_AT_address_class(0x16)
$C$DW$T$150	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$T$150, DW_AT_language(DW_LANG_C)

$C$DW$T$48	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$48, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x01)
$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$601, DW_AT_name("ACK1")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$601, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$602, DW_AT_name("ACK2")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$602, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$603, DW_AT_name("ACK3")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$603, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$604, DW_AT_name("ACK4")
	.dwattr $C$DW$604, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$604, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$605, DW_AT_name("ACK5")
	.dwattr $C$DW$605, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$605, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$606, DW_AT_name("ACK6")
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$606, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$607, DW_AT_name("ACK7")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$607, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$608, DW_AT_name("ACK8")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$608, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$609, DW_AT_name("ACK9")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$609, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$610, DW_AT_name("ACK10")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$610, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$611, DW_AT_name("ACK11")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$611, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$612, DW_AT_name("ACK12")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$612, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$613, DW_AT_name("rsvd1")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$613, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$48


$C$DW$T$49	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$49, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x01)
$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$614, DW_AT_name("all")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$615, DW_AT_name("bit")
	.dwattr $C$DW$615, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$49


$C$DW$T$50	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$50, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x01)
$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$616, DW_AT_name("ENPIE")
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$616, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$617, DW_AT_name("PIEVECT")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$617, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$50


$C$DW$T$51	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$51, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$51, DW_AT_byte_size(0x01)
$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$618, DW_AT_name("all")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$619, DW_AT_name("bit")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$51


$C$DW$T$52	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$52, DW_AT_name("PIEIER_BITS")
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x01)
$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$620, DW_AT_name("INTx1")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$620, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$621, DW_AT_name("INTx2")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$621, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$622, DW_AT_name("INTx3")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$622, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$623, DW_AT_name("INTx4")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$623, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$624, DW_AT_name("INTx5")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$624, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$625, DW_AT_name("INTx6")
	.dwattr $C$DW$625, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$625, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$626, DW_AT_name("INTx7")
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$626, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$627	.dwtag  DW_TAG_member
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$627, DW_AT_name("INTx8")
	.dwattr $C$DW$627, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$627, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$628, DW_AT_name("rsvd1")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$628, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$52


$C$DW$T$53	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$53, DW_AT_name("PIEIER_REG")
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x01)
$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$629, DW_AT_name("all")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$630, DW_AT_name("bit")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$53


$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("PIEIFR_BITS")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x01)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$631, DW_AT_name("INTx1")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$631, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$632, DW_AT_name("INTx2")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$632, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$633, DW_AT_name("INTx3")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$633, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$634, DW_AT_name("INTx4")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$634, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$635, DW_AT_name("INTx5")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$635, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$636, DW_AT_name("INTx6")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$636, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$637	.dwtag  DW_TAG_member
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$637, DW_AT_name("INTx7")
	.dwattr $C$DW$637, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$637, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$637, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$638, DW_AT_name("INTx8")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$638, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$639, DW_AT_name("rsvd1")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$639, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$54


$C$DW$T$55	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$55, DW_AT_name("PIEIFR_REG")
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x01)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$640, DW_AT_name("all")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$641, DW_AT_name("bit")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$55


$C$DW$T$56	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$56, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x1a)
$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$642, DW_AT_name("PIECTRL")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$643, DW_AT_name("PIEACK")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$644, DW_AT_name("PIEIER1")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$645, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$645, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$646, DW_AT_name("PIEIER2")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$647, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$648, DW_AT_name("PIEIER3")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$649, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$650, DW_AT_name("PIEIER4")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$651, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$652	.dwtag  DW_TAG_member
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$652, DW_AT_name("PIEIER5")
	.dwattr $C$DW$652, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$652, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$653, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$653, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$654, DW_AT_name("PIEIER6")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$655, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$656, DW_AT_name("PIEIER7")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$657, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$658, DW_AT_name("PIEIER8")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$659, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$660, DW_AT_name("PIEIER9")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$661, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$662, DW_AT_name("PIEIER10")
	.dwattr $C$DW$662, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$663, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$664, DW_AT_name("PIEIER11")
	.dwattr $C$DW$664, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$665, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$666, DW_AT_name("PIEIER12")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$667, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56

$C$DW$668	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$56)
$C$DW$T$153	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$668)

$C$DW$T$58	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$58, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x04)
$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$669, DW_AT_name("next")
	.dwattr $C$DW$669, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$670, DW_AT_name("prev")
	.dwattr $C$DW$670, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$58

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$57	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$57, DW_AT_address_class(0x16)

$C$DW$T$60	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$60, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x10)
$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$671, DW_AT_name("job")
	.dwattr $C$DW$671, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$672, DW_AT_name("count")
	.dwattr $C$DW$672, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$673, DW_AT_name("pendQ")
	.dwattr $C$DW$673, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$674, DW_AT_name("name")
	.dwattr $C$DW$674, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$60

$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$155	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$155, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$155, DW_AT_address_class(0x16)
$C$DW$T$156	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$T$156, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$157	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$157, DW_AT_language(DW_LANG_C)

$C$DW$T$36	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$675	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$35)
	.dwendtag $C$DW$T$36

$C$DW$T$37	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_address_class(0x16)
$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)

$C$DW$T$91	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$91, DW_AT_language(DW_LANG_C)
$C$DW$676	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$90)
	.dwendtag $C$DW$T$91

$C$DW$T$92	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_address_class(0x16)
$C$DW$T$93	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)
$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$105	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$105, DW_AT_language(DW_LANG_C)
$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("post_sync_t")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$T$94	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)
$C$DW$T$96	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$96, DW_AT_language(DW_LANG_C)

$C$DW$T$100	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$677	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$90)
$C$DW$678	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$100

$C$DW$T$101	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_address_class(0x16)
$C$DW$T$133	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$133, DW_AT_language(DW_LANG_C)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)
$C$DW$T$106	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$106, DW_AT_language(DW_LANG_C)

$C$DW$T$124	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$124, DW_AT_language(DW_LANG_C)
$C$DW$679	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$90)
$C$DW$680	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$6)
$C$DW$681	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$9)
$C$DW$682	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$124

$C$DW$T$125	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_address_class(0x16)
$C$DW$T$126	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$683	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$683, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19


$C$DW$T$28	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x46)
$C$DW$684	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$684, DW_AT_upper_bound(0x45)
	.dwendtag $C$DW$T$28

$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x16)
$C$DW$685	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$6)
$C$DW$T$79	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$685)
$C$DW$T$80	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$80, DW_AT_address_class(0x16)
$C$DW$686	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$6)
$C$DW$T$180	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$180, DW_AT_type(*$C$DW$686)
$C$DW$T$127	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$127, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$98	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$98, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$181	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$181, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$181, DW_AT_language(DW_LANG_C)
$C$DW$687	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$9)
$C$DW$T$77	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$687)
$C$DW$T$78	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_address_class(0x16)
$C$DW$T$99	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$99, DW_AT_address_class(0x16)

$C$DW$T$190	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$190, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$190, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$190, DW_AT_byte_size(0x02)
$C$DW$688	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$688, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$190

$C$DW$T$61	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$81	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$689	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$6)
$C$DW$690	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$81

$C$DW$T$82	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_address_class(0x16)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)
$C$DW$T$97	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$97, DW_AT_address_class(0x16)

$C$DW$T$108	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)
$C$DW$691	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$90)
$C$DW$692	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$71)
$C$DW$693	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$6)
$C$DW$694	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$108

$C$DW$T$109	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$109, DW_AT_address_class(0x16)
$C$DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
$C$DW$695	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$110)
$C$DW$T$111	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$695)
$C$DW$T$112	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$112, DW_AT_address_class(0x16)
$C$DW$T$113	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x16)

$C$DW$T$117	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$696	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$90)
$C$DW$697	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$9)
$C$DW$698	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$117

$C$DW$T$118	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$118, DW_AT_address_class(0x16)
$C$DW$T$119	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$119, DW_AT_language(DW_LANG_C)
$C$DW$699	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$13)
$C$DW$T$200	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$200, DW_AT_type(*$C$DW$699)
$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$62	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$62, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)

$C$DW$T$202	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$202, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$202, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$202, DW_AT_byte_size(0x10)
$C$DW$700	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$700, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$202


$C$DW$T$63	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$63, DW_AT_name("crc_record")
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x08)
$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$701, DW_AT_name("crc_alg_ID")
	.dwattr $C$DW$701, DW_AT_TI_symbol_name("_crc_alg_ID")
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$702, DW_AT_name("page_id")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_page_id")
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$703, DW_AT_name("addr")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_addr")
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$704, DW_AT_name("size")
	.dwattr $C$DW$704, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$705, DW_AT_name("crc_value")
	.dwattr $C$DW$705, DW_AT_TI_symbol_name("_crc_value")
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$63

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_RECORD")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$65	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$65, DW_AT_byte_size(0x08)
$C$DW$706	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$706, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$65


$C$DW$T$66	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$66, DW_AT_name("crc_table")
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x0a)
$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$707, DW_AT_name("rec_size")
	.dwattr $C$DW$707, DW_AT_TI_symbol_name("_rec_size")
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$708	.dwtag  DW_TAG_member
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$708, DW_AT_name("num_recs")
	.dwattr $C$DW$708, DW_AT_TI_symbol_name("_num_recs")
	.dwattr $C$DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$708, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$709, DW_AT_name("recs")
	.dwattr $C$DW$709, DW_AT_TI_symbol_name("_recs")
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$66

$C$DW$T$205	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_TABLE")
	.dwattr $C$DW$T$205, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$205, DW_AT_language(DW_LANG_C)

$C$DW$T$120	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$120, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$120, DW_AT_byte_size(0x01)
$C$DW$710	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$711	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$120

$C$DW$T$121	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$121, DW_AT_language(DW_LANG_C)

$C$DW$T$86	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$86, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$86, DW_AT_byte_size(0x01)
$C$DW$712	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$713	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$714	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$715	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$716	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$717	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$718	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$719	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$86

$C$DW$T$87	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$T$87, DW_AT_language(DW_LANG_C)

$C$DW$T$103	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$103, DW_AT_byte_size(0x80)
$C$DW$720	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$720, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$103


$C$DW$T$67	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$67, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x06)
$C$DW$721	.dwtag  DW_TAG_member
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$721, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$721, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$721, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$722, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$723, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$724, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$725, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$726, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$67

$C$DW$T$74	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$74, DW_AT_language(DW_LANG_C)
$C$DW$727	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$74)
$C$DW$T$75	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$727)
$C$DW$T$76	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$T$76, DW_AT_address_class(0x16)

$C$DW$T$130	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$130, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x132)
$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$728, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$729, DW_AT_name("objdict")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$730, DW_AT_name("PDO_status")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$731, DW_AT_name("firstIndex")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$732, DW_AT_name("lastIndex")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$733, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$734, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$735, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$736, DW_AT_name("transfers")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$737, DW_AT_name("nodeState")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$738, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$739, DW_AT_name("initialisation")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$740, DW_AT_name("preOperational")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$741, DW_AT_name("operational")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$742, DW_AT_name("stopped")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$743, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$744, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$745, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$746, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$746, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$747, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$748, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$749, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$750, DW_AT_name("heartbeatError")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$751, DW_AT_name("NMTable")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$752, DW_AT_name("syncTimer")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$753, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$754, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$755, DW_AT_name("post_sync")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_post_sync")
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$756, DW_AT_name("post_TPDO")
	.dwattr $C$DW$756, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$757, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$758, DW_AT_name("toggle")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$759, DW_AT_name("canHandle")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$760, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$761, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$762, DW_AT_name("globalCallback")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$763, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$764, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$765, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$766, DW_AT_name("dcf_request")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$767	.dwtag  DW_TAG_member
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$767, DW_AT_name("error_state")
	.dwattr $C$DW$767, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$767, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$768, DW_AT_name("error_history_size")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$769, DW_AT_name("error_number")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$770, DW_AT_name("error_first_element")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$771, DW_AT_name("error_register")
	.dwattr $C$DW$771, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$772, DW_AT_name("error_cobid")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$773, DW_AT_name("error_data")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$774, DW_AT_name("post_emcy")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$775, DW_AT_name("lss_transfer")
	.dwattr $C$DW$775, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$776, DW_AT_name("eeprom_index")
	.dwattr $C$DW$776, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$777, DW_AT_name("eeprom_size")
	.dwattr $C$DW$777, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$130

$C$DW$T$89	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$T$89, DW_AT_language(DW_LANG_C)
$C$DW$T$90	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$90, DW_AT_address_class(0x16)

$C$DW$T$132	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$132, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$132, DW_AT_byte_size(0x0e)
$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$778, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$778, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$779, DW_AT_name("event_timer")
	.dwattr $C$DW$779, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$780, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$781, DW_AT_name("last_message")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$132

$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)
$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x16)

$C$DW$T$134	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$134, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$134, DW_AT_byte_size(0x14)
$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$782, DW_AT_name("nodeId")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$783, DW_AT_name("whoami")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$784, DW_AT_name("state")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$785, DW_AT_name("toggle")
	.dwattr $C$DW$785, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$786, DW_AT_name("abortCode")
	.dwattr $C$DW$786, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$787	.dwtag  DW_TAG_member
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$787, DW_AT_name("index")
	.dwattr $C$DW$787, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$787, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$788, DW_AT_name("subIndex")
	.dwattr $C$DW$788, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$789, DW_AT_name("port")
	.dwattr $C$DW$789, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$790	.dwtag  DW_TAG_member
	.dwattr $C$DW$790, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$790, DW_AT_name("count")
	.dwattr $C$DW$790, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$790, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$791, DW_AT_name("offset")
	.dwattr $C$DW$791, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$792, DW_AT_name("datap")
	.dwattr $C$DW$792, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$793, DW_AT_name("dataType")
	.dwattr $C$DW$793, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$794	.dwtag  DW_TAG_member
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$794, DW_AT_name("timer")
	.dwattr $C$DW$794, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$794, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$794, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$795	.dwtag  DW_TAG_member
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$795, DW_AT_name("Callback")
	.dwattr $C$DW$795, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$795, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$795, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$134

$C$DW$T$84	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)

$C$DW$T$85	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x3c)
$C$DW$796	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$796, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$85


$C$DW$T$138	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$138, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$138, DW_AT_byte_size(0x04)
$C$DW$797	.dwtag  DW_TAG_member
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$797, DW_AT_name("pSubindex")
	.dwattr $C$DW$797, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$797, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$797, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$798	.dwtag  DW_TAG_member
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$798, DW_AT_name("bSubCount")
	.dwattr $C$DW$798, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$798, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$799	.dwtag  DW_TAG_member
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$799, DW_AT_name("index")
	.dwattr $C$DW$799, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$799, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$138

$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$800	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$69)
$C$DW$T$70	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$800)
$C$DW$T$71	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$71, DW_AT_address_class(0x16)

$C$DW$T$114	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$114, DW_AT_language(DW_LANG_C)
$C$DW$801	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$9)
$C$DW$802	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$802, DW_AT_type(*$C$DW$T$97)
$C$DW$803	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$803, DW_AT_type(*$C$DW$T$113)
	.dwendtag $C$DW$T$114

$C$DW$T$115	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$115, DW_AT_address_class(0x16)
$C$DW$T$116	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$116, DW_AT_language(DW_LANG_C)

$C$DW$T$139	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$139, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$139, DW_AT_byte_size(0x08)
$C$DW$804	.dwtag  DW_TAG_member
	.dwattr $C$DW$804, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$804, DW_AT_name("bAccessType")
	.dwattr $C$DW$804, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$804, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$804, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$805	.dwtag  DW_TAG_member
	.dwattr $C$DW$805, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$805, DW_AT_name("bDataType")
	.dwattr $C$DW$805, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$805, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$805, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$806	.dwtag  DW_TAG_member
	.dwattr $C$DW$806, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$806, DW_AT_name("size")
	.dwattr $C$DW$806, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$806, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$806, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$807	.dwtag  DW_TAG_member
	.dwattr $C$DW$807, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$807, DW_AT_name("pObject")
	.dwattr $C$DW$807, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$807, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$807, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$808	.dwtag  DW_TAG_member
	.dwattr $C$DW$808, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$808, DW_AT_name("bProcessor")
	.dwattr $C$DW$808, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$808, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$808, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$139

$C$DW$809	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$809, DW_AT_type(*$C$DW$T$139)
$C$DW$T$135	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$809)
$C$DW$T$136	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$T$136, DW_AT_language(DW_LANG_C)
$C$DW$T$137	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$T$137, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$810	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$810, DW_AT_location[DW_OP_reg0]
$C$DW$811	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$811, DW_AT_location[DW_OP_reg1]
$C$DW$812	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$812, DW_AT_location[DW_OP_reg2]
$C$DW$813	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$813, DW_AT_location[DW_OP_reg3]
$C$DW$814	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$814, DW_AT_location[DW_OP_reg20]
$C$DW$815	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$815, DW_AT_location[DW_OP_reg21]
$C$DW$816	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$816, DW_AT_location[DW_OP_reg22]
$C$DW$817	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$817, DW_AT_location[DW_OP_reg23]
$C$DW$818	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$818, DW_AT_location[DW_OP_reg24]
$C$DW$819	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$819, DW_AT_location[DW_OP_reg25]
$C$DW$820	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$820, DW_AT_location[DW_OP_reg26]
$C$DW$821	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$821, DW_AT_location[DW_OP_reg28]
$C$DW$822	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$822, DW_AT_location[DW_OP_reg29]
$C$DW$823	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$823, DW_AT_location[DW_OP_reg30]
$C$DW$824	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$824, DW_AT_location[DW_OP_reg31]
$C$DW$825	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$825, DW_AT_location[DW_OP_regx 0x20]
$C$DW$826	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$826, DW_AT_location[DW_OP_regx 0x21]
$C$DW$827	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$827, DW_AT_location[DW_OP_regx 0x22]
$C$DW$828	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$828, DW_AT_location[DW_OP_regx 0x23]
$C$DW$829	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$829, DW_AT_location[DW_OP_regx 0x24]
$C$DW$830	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$830, DW_AT_location[DW_OP_regx 0x25]
$C$DW$831	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$831, DW_AT_location[DW_OP_regx 0x26]
$C$DW$832	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$832, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$833	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$833, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$834	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$834, DW_AT_location[DW_OP_reg4]
$C$DW$835	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$835, DW_AT_location[DW_OP_reg6]
$C$DW$836	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$836, DW_AT_location[DW_OP_reg8]
$C$DW$837	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$837, DW_AT_location[DW_OP_reg10]
$C$DW$838	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$838, DW_AT_location[DW_OP_reg12]
$C$DW$839	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$839, DW_AT_location[DW_OP_reg14]
$C$DW$840	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$840, DW_AT_location[DW_OP_reg16]
$C$DW$841	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$841, DW_AT_location[DW_OP_reg17]
$C$DW$842	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$842, DW_AT_location[DW_OP_reg18]
$C$DW$843	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$843, DW_AT_location[DW_OP_reg19]
$C$DW$844	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$844, DW_AT_location[DW_OP_reg5]
$C$DW$845	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$845, DW_AT_location[DW_OP_reg7]
$C$DW$846	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$846, DW_AT_location[DW_OP_reg9]
$C$DW$847	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$847, DW_AT_location[DW_OP_reg11]
$C$DW$848	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$848, DW_AT_location[DW_OP_reg13]
$C$DW$849	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$849, DW_AT_location[DW_OP_reg15]
$C$DW$850	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$850, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$851	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$851, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$852	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$852, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$853	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$853, DW_AT_location[DW_OP_regx 0x30]
$C$DW$854	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$854, DW_AT_location[DW_OP_regx 0x33]
$C$DW$855	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$855, DW_AT_location[DW_OP_regx 0x34]
$C$DW$856	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$856, DW_AT_location[DW_OP_regx 0x37]
$C$DW$857	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$857, DW_AT_location[DW_OP_regx 0x38]
$C$DW$858	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$858, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$859	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$859, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$860	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$860, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$861	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$861, DW_AT_location[DW_OP_regx 0x40]
$C$DW$862	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$862, DW_AT_location[DW_OP_regx 0x43]
$C$DW$863	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$863, DW_AT_location[DW_OP_regx 0x44]
$C$DW$864	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$864, DW_AT_location[DW_OP_regx 0x47]
$C$DW$865	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$865, DW_AT_location[DW_OP_regx 0x48]
$C$DW$866	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$866, DW_AT_location[DW_OP_regx 0x49]
$C$DW$867	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$867, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$868	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$868, DW_AT_location[DW_OP_regx 0x27]
$C$DW$869	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$869, DW_AT_location[DW_OP_regx 0x28]
$C$DW$870	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$870, DW_AT_location[DW_OP_reg27]
$C$DW$871	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$871, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

