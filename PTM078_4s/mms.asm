;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Mon Aug 31 10:04:20 2020                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../mms.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\R_Module_48V_Standard\PTM078_4s")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_InitOK+0,32
	.bits	0,16			; _InitOK @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestPositionEnable+0,32
	.bits	1,16			; _TestPositionEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_UnderCurrent+0,32
	.bits	0,16			; _UnderCurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestCurrentEnable+0,32
	.bits	0,16			; _TestCurrentEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SequenceRunning+0,32
	.bits	0,16			; _SequenceRunning @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ReceiveNew+0,32
	.bits	0,16			; _ReceiveNew @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_current_counter+0,32
	.bits	0,32			; _GV_current_counter @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorComm")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_ERR_ErrorComm")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverCurrent")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorUnderVoltage")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_HandleWarning")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_ERR_HandleWarning")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$4


$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("WARN_ErrorParam")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_WARN_ErrorParam")
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external

$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_SetParamDependantVars")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external

$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_GetLogNB")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_PAR_GetLogNB")
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$8


$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("genCRC32Table")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_genCRC32Table")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external

$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddLog")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_PAR_AddLog")
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external

$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Init")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_HAL_Init")
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("REC_StartRecorder")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_REC_StartRecorder")
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external

$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Random")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_HAL_Random")
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("setNodeId")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_setNodeId")
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$90)
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$15


$C$DW$18	.dwtag  DW_TAG_subprogram, DW_AT_name("canInit")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_canInit")
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$9)
$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$18


$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Unlock")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_HAL_Unlock")
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external

$C$DW$22	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_Command")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_SCI1_Command")
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external

$C$DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("RS232ReceiveEnable")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_RS232ReceiveEnable")
	.dwattr $C$DW$23, DW_AT_declaration
	.dwattr $C$DW$23, DW_AT_external

$C$DW$24	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Unlock")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_USB_Unlock")
	.dwattr $C$DW$24, DW_AT_declaration
	.dwattr $C$DW$24, DW_AT_external

$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Reset")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_HAL_Reset")
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external

$C$DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Stop")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_USB_Stop")
	.dwattr $C$DW$26, DW_AT_declaration
	.dwattr $C$DW$26, DW_AT_external

$C$DW$27	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI1_Receive")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_SCI1_Receive")
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("ODV_CommError_Set")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ODV_CommError_Set")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Low_Voltage_Charge_Delay")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Low_Voltage_Charge_Delay")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_ChargeAllowed")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_ODV_Current_ChargeAllowed")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$30, DW_AT_declaration
	.dwattr $C$DW$30, DW_AT_external
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Low_Voltage_Current_Allow")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Low_Voltage_Current_Allow")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$31, DW_AT_declaration
	.dwattr $C$DW$31, DW_AT_external
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Current_C_D_Mode")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_ODP_Current_C_D_Mode")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$32, DW_AT_declaration
	.dwattr $C$DW$32, DW_AT_external
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_DischargeAllowed")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_ODV_Current_DischargeAllowed")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineEvent")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ODV_MachineEvent")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_LogNB")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_ODV_Gateway_LogNB")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_State")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_ODV_Gateway_State")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$37, DW_AT_declaration
	.dwattr $C$DW$37, DW_AT_external
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Multiunits")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_ODV_Recorder_Multiunits")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$39, DW_AT_declaration
	.dwattr $C$DW$39, DW_AT_external
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("ODP_VersionParameters")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_ODP_VersionParameters")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SciSend")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ODV_SciSend")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Variables")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_ODV_Recorder_Variables")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$42, DW_AT_declaration
	.dwattr $C$DW$42, DW_AT_external
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$44, DW_AT_declaration
	.dwattr $C$DW$44, DW_AT_external
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Low_Voltage_Current_Delay")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Low_Voltage_Current_Delay")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_SleepTimeout")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_ODP_SafetyLimits_SleepTimeout")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$46, DW_AT_declaration
	.dwattr $C$DW$46, DW_AT_external
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$47, DW_AT_declaration
	.dwattr $C$DW$47, DW_AT_external
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$48, DW_AT_declaration
	.dwattr $C$DW$48, DW_AT_external
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("HAL_Enable")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_HAL_Enable")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$51, DW_AT_declaration
	.dwattr $C$DW$51, DW_AT_external
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("SCI_MsgAvailable")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_SCI_MsgAvailable")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("HAL_CellOK")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_HAL_CellOK")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Controlword")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_ODV_Controlword")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$54, DW_AT_declaration
	.dwattr $C$DW$54, DW_AT_external
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("HAL_NewCurPoint")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_HAL_NewCurPoint")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Debug")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_ODV_Debug")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$56, DW_AT_declaration
	.dwattr $C$DW$56, DW_AT_external
	.global	_InitOK
_InitOK:	.usect	".ebss",1,1,0
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("InitOK")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_InitOK")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_addr _InitOK]
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$57, DW_AT_external
	.global	_TestPositionEnable
_TestPositionEnable:	.usect	".ebss",1,1,0
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("TestPositionEnable")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_TestPositionEnable")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_addr _TestPositionEnable]
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$58, DW_AT_external
	.global	_UnderCurrent
_UnderCurrent:	.usect	".ebss",1,1,0
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("UnderCurrent")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_UnderCurrent")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_addr _UnderCurrent]
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$59, DW_AT_external
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("SCI_Available")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_SCI_Available")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$60, DW_AT_declaration
	.dwattr $C$DW$60, DW_AT_external
	.global	_TestCurrentEnable
_TestCurrentEnable:	.usect	".ebss",1,1,0
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("TestCurrentEnable")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_TestCurrentEnable")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_addr _TestCurrentEnable]
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$61, DW_AT_external
	.global	_SequenceRunning
_SequenceRunning:	.usect	".ebss",1,1,0
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("SequenceRunning")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_SequenceRunning")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_addr _SequenceRunning]
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$62, DW_AT_external
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_BaudRate")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ODP_Board_BaudRate")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$63, DW_AT_declaration
	.dwattr $C$DW$63, DW_AT_external
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SleepCurrent")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_ODP_SleepCurrent")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$64, DW_AT_declaration
	.dwattr $C$DW$64, DW_AT_external
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_Config")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_ODP_Board_Config")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$65, DW_AT_declaration
	.dwattr $C$DW$65, DW_AT_external
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_TimeOut")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ODP_CommError_TimeOut")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$66, DW_AT_declaration
	.dwattr $C$DW$66, DW_AT_external
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_Delay")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_ODP_CommError_Delay")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$67, DW_AT_declaration
	.dwattr $C$DW$67, DW_AT_external
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineMode")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_ODV_MachineMode")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$68, DW_AT_declaration
	.dwattr $C$DW$68, DW_AT_external
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MinCellVoltage")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ODV_Module1_MinCellVoltage")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_external
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_MaxCellVoltage")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_ODV_Module1_MaxCellVoltage")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$70, DW_AT_declaration
	.dwattr $C$DW$70, DW_AT_external
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Current")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_ODV_Module1_Current")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$71, DW_AT_declaration
	.dwattr $C$DW$71, DW_AT_external
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Status")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_ODV_Module1_Status")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$72, DW_AT_declaration
	.dwattr $C$DW$72, DW_AT_external
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_SOC")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_ODV_Module1_SOC")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$73, DW_AT_declaration
	.dwattr $C$DW$73, DW_AT_external
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SOC_SOC2")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_ODV_SOC_SOC2")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$74, DW_AT_declaration
	.dwattr $C$DW$74, DW_AT_external

$C$DW$75	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarning")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_ERR_ClearWarning")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$75, DW_AT_declaration
	.dwattr $C$DW$75, DW_AT_external

$C$DW$76	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_InitParam")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_PAR_InitParam")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$76, DW_AT_declaration
	.dwattr $C$DW$76, DW_AT_external
$C$DW$77	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$90)
	.dwendtag $C$DW$76

$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("TimeLogIndex")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_TimeLogIndex")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$78, DW_AT_declaration
	.dwattr $C$DW$78, DW_AT_external

$C$DW$79	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$79, DW_AT_declaration
	.dwattr $C$DW$79, DW_AT_external
$C$DW$80	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$9)
$C$DW$81	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$44)
$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$9)
$C$DW$83	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$79


$C$DW$84	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_UpdateCode")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_PAR_UpdateCode")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$84, DW_AT_declaration
	.dwattr $C$DW$84, DW_AT_external
$C$DW$85	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$84


$C$DW$86	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Start")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_USB_Start")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$86, DW_AT_declaration
	.dwattr $C$DW$86, DW_AT_external

$C$DW$87	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_CommandNoWait")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_I2C_CommandNoWait")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$87, DW_AT_declaration
	.dwattr $C$DW$87, DW_AT_external
$C$DW$88	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$9)
$C$DW$89	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$44)
$C$DW$90	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$9)
$C$DW$91	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$9)
$C$DW$92	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$68)
	.dwendtag $C$DW$87


$C$DW$93	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$93, DW_AT_declaration
	.dwattr $C$DW$93, DW_AT_external
$C$DW$94	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$150)
$C$DW$95	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$157)
$C$DW$96	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$42)
	.dwendtag $C$DW$93


$C$DW$97	.dwtag  DW_TAG_subprogram, DW_AT_name("setState")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_setState")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$97, DW_AT_declaration
	.dwattr $C$DW$97, DW_AT_external
$C$DW$98	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$90)
$C$DW$99	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$87)
	.dwendtag $C$DW$97

	.global	_ReceiveNew
_ReceiveNew:	.usect	".ebss",1,1,0
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("ReceiveNew")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_ReceiveNew")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_addr _ReceiveNew]
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$100, DW_AT_external

$C$DW$101	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$101, DW_AT_declaration
	.dwattr $C$DW$101, DW_AT_external
$C$DW$102	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$150)
$C$DW$103	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$157)
$C$DW$104	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$42)
	.dwendtag $C$DW$101


$C$DW$105	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$105, DW_AT_declaration
	.dwattr $C$DW$105, DW_AT_external
$C$DW$106	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$156)
$C$DW$107	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$42)
	.dwendtag $C$DW$105


$C$DW$108	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddMultiUnits")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$108, DW_AT_declaration
	.dwattr $C$DW$108, DW_AT_external
$C$DW$109	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$108


$C$DW$110	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearError")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_ERR_ClearError")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$110, DW_AT_declaration
	.dwattr $C$DW$110, DW_AT_external

$C$DW$111	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddVariables")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_PAR_AddVariables")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$111, DW_AT_declaration
	.dwattr $C$DW$111, DW_AT_external
$C$DW$112	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$111


$C$DW$113	.dwtag  DW_TAG_subprogram, DW_AT_name("getCRC32_cpu")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_getCRC32_cpu")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$113, DW_AT_declaration
	.dwattr $C$DW$113, DW_AT_external
$C$DW$114	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$13)
$C$DW$115	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$99)
$C$DW$116	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$141)
$C$DW$117	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$113


$C$DW$118	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_Round")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_CNV_Round")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$118, DW_AT_declaration
	.dwattr $C$DW$118, DW_AT_external
$C$DW$119	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$118


$C$DW$120	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteStatisticParam")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$120, DW_AT_declaration
	.dwattr $C$DW$120, DW_AT_external
$C$DW$121	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$8)
	.dwendtag $C$DW$120

$C$DW$122	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RandomNB")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_ODP_RandomNB")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$122, DW_AT_declaration
	.dwattr $C$DW$122, DW_AT_external
$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Capacity")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_ODP_Battery_Capacity")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$123, DW_AT_declaration
	.dwattr $C$DW$123, DW_AT_external
$C$DW$124	.dwtag  DW_TAG_variable, DW_AT_name("MMSConfig")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_MMSConfig")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$124, DW_AT_declaration
	.dwattr $C$DW$124, DW_AT_external

$C$DW$125	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_StoreODSubIndex")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$125, DW_AT_declaration
	.dwattr $C$DW$125, DW_AT_external
$C$DW$126	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$90)
$C$DW$127	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$9)
$C$DW$128	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$125


$C$DW$129	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteAllPermanentParam")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$129, DW_AT_declaration
	.dwattr $C$DW$129, DW_AT_external
$C$DW$130	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$90)
	.dwendtag $C$DW$129

$C$DW$131	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Total")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_PAR_Capacity_Total")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$131, DW_AT_declaration
	.dwattr $C$DW$131, DW_AT_external
$C$DW$132	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Left")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_PAR_Capacity_Left")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$132, DW_AT_declaration
	.dwattr $C$DW$132, DW_AT_external
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("ODV_StoreParameters")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_ODV_StoreParameters")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$133, DW_AT_declaration
	.dwattr $C$DW$133, DW_AT_external
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SysTick_ms")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_ODV_SysTick_ms")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$134, DW_AT_declaration
	.dwattr $C$DW$134, DW_AT_external
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RestoreDefaultParameters")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_ODV_RestoreDefaultParameters")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$135, DW_AT_declaration
	.dwattr $C$DW$135, DW_AT_external
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$190)
	.dwattr $C$DW$136, DW_AT_declaration
	.dwattr $C$DW$136, DW_AT_external
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ResetHW")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_ODV_ResetHW")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$137, DW_AT_declaration
	.dwattr $C$DW$137, DW_AT_external
	.global	_GV_current_counter
_GV_current_counter:	.usect	".ebss",2,1,1
$C$DW$138	.dwtag  DW_TAG_variable, DW_AT_name("GV_current_counter")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_GV_current_counter")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_addr _GV_current_counter]
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$138, DW_AT_external
	.global	_BoardODdata
_BoardODdata:	.usect	".ebss",2,1,1
$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_addr _BoardODdata]
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$139, DW_AT_external
$C$DW$140	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Errorcode")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_ODV_Gateway_Errorcode")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$140, DW_AT_declaration
	.dwattr $C$DW$140, DW_AT_external
$C$DW$141	.dwtag  DW_TAG_variable, DW_AT_name("ODP_OnTime")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_ODP_OnTime")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$141, DW_AT_declaration
	.dwattr $C$DW$141, DW_AT_external
$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$142, DW_AT_declaration
	.dwattr $C$DW$142, DW_AT_external
$C$DW$143	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$143, DW_AT_declaration
	.dwattr $C$DW$143, DW_AT_external
$C$DW$144	.dwtag  DW_TAG_variable, DW_AT_name("ODV_CommError_Counter")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_ODV_CommError_Counter")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$144, DW_AT_declaration
	.dwattr $C$DW$144, DW_AT_external
$C$DW$145	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RTC_Text")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_ODV_RTC_Text")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$145, DW_AT_declaration
	.dwattr $C$DW$145, DW_AT_external
	.global	_BootST
_BootST:	.usect	"BootCommand",4,1,1
$C$DW$146	.dwtag  DW_TAG_variable, DW_AT_name("BootST")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_BootST")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_addr _BootST]
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$146, DW_AT_external
$C$DW$147	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$147, DW_AT_declaration
	.dwattr $C$DW$147, DW_AT_external
$C$DW$148	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Date_Time")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_ODV_Gateway_Date_Time")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$148, DW_AT_declaration
	.dwattr $C$DW$148, DW_AT_external
$C$DW$149	.dwtag  DW_TAG_variable, DW_AT_name("golden_CRC_values")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_golden_CRC_values")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$205)
	.dwattr $C$DW$149, DW_AT_declaration
	.dwattr $C$DW$149, DW_AT_external
$C$DW$150	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$150, DW_AT_declaration
	.dwattr $C$DW$150, DW_AT_external
$C$DW$151	.dwtag  DW_TAG_variable, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$151, DW_AT_declaration
	.dwattr $C$DW$151, DW_AT_external
$C$DW$152	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$152, DW_AT_declaration
	.dwattr $C$DW$152, DW_AT_external
$C$DW$153	.dwtag  DW_TAG_variable, DW_AT_name("can_rx_mbox")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_can_rx_mbox")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$153, DW_AT_declaration
	.dwattr $C$DW$153, DW_AT_external
$C$DW$154	.dwtag  DW_TAG_variable, DW_AT_name("mailboxSDOout")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_mailboxSDOout")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$154, DW_AT_declaration
	.dwattr $C$DW$154, DW_AT_external
$C$DW$155	.dwtag  DW_TAG_variable, DW_AT_name("sci_rx_mbox")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_sci_rx_mbox")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$155, DW_AT_declaration
	.dwattr $C$DW$155, DW_AT_external
$C$DW$156	.dwtag  DW_TAG_variable, DW_AT_name("SCI_MsgInRS232")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_SCI_MsgInRS232")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$156, DW_AT_declaration
	.dwattr $C$DW$156, DW_AT_external
$C$DW$157	.dwtag  DW_TAG_variable, DW_AT_name("ODI_mms_dict_Data")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_ODI_mms_dict_Data")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$157, DW_AT_declaration
	.dwattr $C$DW$157, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0868013 
	.sect	".text"
	.global	_CheckfirmwareCRC

$C$DW$158	.dwtag  DW_TAG_subprogram, DW_AT_name("CheckfirmwareCRC")
	.dwattr $C$DW$158, DW_AT_low_pc(_CheckfirmwareCRC)
	.dwattr $C$DW$158, DW_AT_high_pc(0x00)
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_CheckfirmwareCRC")
	.dwattr $C$DW$158, DW_AT_external
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$158, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$158, DW_AT_TI_begin_line(0x40)
	.dwattr $C$DW$158, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$158, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../mms.c",line 65,column 1,is_stmt,address _CheckfirmwareCRC

	.dwfde $C$DW$CIE, _CheckfirmwareCRC

;***************************************************************
;* FNAME: _CheckfirmwareCRC             FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_CheckfirmwareCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$159	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$159, DW_AT_location[DW_OP_breg20 -2]
$C$DW$160	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$160, DW_AT_location[DW_OP_breg20 -4]
$C$DW$161	.dwtag  DW_TAG_variable, DW_AT_name("crc_rec")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_crc_rec")
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$161, DW_AT_location[DW_OP_breg20 -12]
	.dwpsn	file "../mms.c",line 69,column 8,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |69| 
        B         $C$L3,UNC             ; [CPU_] |69| 
        ; branch occurs ; [] |69| 
$C$L1:    
	.dwpsn	file "../mms.c",line 71,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVZ      AR4,SP                ; [CPU_U] |71| 
        MOVL      XAR7,#_golden_CRC_values+2 ; [CPU_U] |71| 
        MOV       ACC,*-SP[2] << 3      ; [CPU_] |71| 
        SUBB      XAR4,#12              ; [CPU_U] |71| 
        ADDL      XAR7,ACC              ; [CPU_] |71| 
        RPT       #7
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |71| 
	.dwpsn	file "../mms.c",line 72,column 5,is_stmt
        MOV       ACC,*-SP[8] << #1     ; [CPU_] |72| 
        MOV       *-SP[1],AL            ; [CPU_] |72| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |72| 
        MOVB      XAR5,#0               ; [CPU_] |72| 
        MOVB      ACC,#0                ; [CPU_] |72| 
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_getCRC32_cpu")
	.dwattr $C$DW$162, DW_AT_TI_call
        LCR       #_getCRC32_cpu        ; [CPU_] |72| 
        ; call occurs [#_getCRC32_cpu] ; [] |72| 
        MOVL      *-SP[4],ACC           ; [CPU_] |72| 
	.dwpsn	file "../mms.c",line 73,column 5,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |73| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |73| 
        BF        $C$L2,EQ              ; [CPU_] |73| 
        ; branchcc occurs ; [] |73| 
	.dwpsn	file "../mms.c",line 74,column 7,is_stmt
        MOVB      AL,#0                 ; [CPU_] |74| 
        B         $C$L4,UNC             ; [CPU_] |74| 
        ; branch occurs ; [] |74| 
$C$L2:    
	.dwpsn	file "../mms.c",line 69,column 47,is_stmt
        INC       *-SP[2]               ; [CPU_] |69| 
$C$L3:    
	.dwpsn	file "../mms.c",line 69,column 15,is_stmt
        MOVW      DP,#_golden_CRC_values+1 ; [CPU_U] 
        MOV       AL,@_golden_CRC_values+1 ; [CPU_] |69| 
        CMP       AL,*-SP[2]            ; [CPU_] |69| 
        B         $C$L1,HI              ; [CPU_] |69| 
        ; branchcc occurs ; [] |69| 
	.dwpsn	file "../mms.c",line 85,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |85| 
$C$L4:    
	.dwpsn	file "../mms.c",line 86,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$158, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$158, DW_AT_TI_end_line(0x56)
	.dwattr $C$DW$158, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$158

	.sect	".text"
	.global	_FnResetNode

$C$DW$164	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetNode")
	.dwattr $C$DW$164, DW_AT_low_pc(_FnResetNode)
	.dwattr $C$DW$164, DW_AT_high_pc(0x00)
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_FnResetNode")
	.dwattr $C$DW$164, DW_AT_external
	.dwattr $C$DW$164, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$164, DW_AT_TI_begin_line(0x5a)
	.dwattr $C$DW$164, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$164, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../mms.c",line 90,column 29,is_stmt,address _FnResetNode

	.dwfde $C$DW$CIE, _FnResetNode
$C$DW$165	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnResetNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$166	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$166, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |90| 
	.dwpsn	file "../mms.c",line 93,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$167	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$167, DW_AT_low_pc(0x00)
	.dwattr $C$DW$167, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$164, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$164, DW_AT_TI_end_line(0x5d)
	.dwattr $C$DW$164, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$164

	.sect	".text"
	.global	_FnResetCommunications

$C$DW$168	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetCommunications")
	.dwattr $C$DW$168, DW_AT_low_pc(_FnResetCommunications)
	.dwattr $C$DW$168, DW_AT_high_pc(0x00)
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_FnResetCommunications")
	.dwattr $C$DW$168, DW_AT_external
	.dwattr $C$DW$168, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$168, DW_AT_TI_begin_line(0x60)
	.dwattr $C$DW$168, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$168, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../mms.c",line 96,column 39,is_stmt,address _FnResetCommunications

	.dwfde $C$DW$CIE, _FnResetCommunications
$C$DW$169	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$169, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetCommunications        FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 19 Auto,  0 SOE     *
;***************************************************************

_FnResetCommunications:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$170	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_breg20 -2]
$C$DW$171	.dwtag  DW_TAG_variable, DW_AT_name("node_id")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_breg20 -3]
$C$DW$172	.dwtag  DW_TAG_variable, DW_AT_name("dummy_m")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_dummy_m")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$202)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_breg20 -19]
        MOVL      *-SP[2],XAR4          ; [CPU_] |96| 
	.dwpsn	file "../mms.c",line 101,column 3,is_stmt
$C$L5:    
        MOVZ      AR5,SP                ; [CPU_U] |101| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |101| 
        MOVB      AL,#0                 ; [CPU_] |101| 
        SUBB      XAR5,#19              ; [CPU_U] |101| 
$C$DW$173	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$173, DW_AT_low_pc(0x00)
	.dwattr $C$DW$173, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$173, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |101| 
        ; call occurs [#_MBX_pend] ; [] |101| 
        CMPB      AL,#0                 ; [CPU_] |101| 
        BF        $C$L5,NEQ             ; [CPU_] |101| 
        ; branchcc occurs ; [] |101| 
	.dwpsn	file "../mms.c",line 102,column 3,is_stmt
$C$L6:    
        MOVZ      AR5,SP                ; [CPU_U] |102| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |102| 
        MOVB      AL,#0                 ; [CPU_] |102| 
        SUBB      XAR5,#19              ; [CPU_U] |102| 
$C$DW$174	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$174, DW_AT_low_pc(0x00)
	.dwattr $C$DW$174, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$174, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |102| 
        ; call occurs [#_MBX_pend] ; [] |102| 
        CMPB      AL,#0                 ; [CPU_] |102| 
        BF        $C$L6,NEQ             ; [CPU_] |102| 
        ; branchcc occurs ; [] |102| 
	.dwpsn	file "../mms.c",line 103,column 3,is_stmt
$C$L7:    
        MOVZ      AR5,SP                ; [CPU_U] |103| 
        MOVL      XAR4,#_can_rx_mbox    ; [CPU_U] |103| 
        MOVB      AL,#0                 ; [CPU_] |103| 
        SUBB      XAR5,#19              ; [CPU_U] |103| 
$C$DW$175	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$175, DW_AT_low_pc(0x00)
	.dwattr $C$DW$175, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$175, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |103| 
        ; call occurs [#_MBX_pend] ; [] |103| 
        CMPB      AL,#0                 ; [CPU_] |103| 
        BF        $C$L7,NEQ             ; [CPU_] |103| 
        ; branchcc occurs ; [] |103| 
	.dwpsn	file "../mms.c",line 105,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOV       AL,@_ODP_Board_RevisionNumber ; [CPU_] |105| 
        MOV       *-SP[3],AL            ; [CPU_] |105| 
	.dwpsn	file "../mms.c",line 112,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |112| 
$C$DW$176	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$176, DW_AT_low_pc(0x00)
	.dwattr $C$DW$176, DW_AT_name("_setNodeId")
	.dwattr $C$DW$176, DW_AT_TI_call
        LCR       #_setNodeId           ; [CPU_] |112| 
        ; call occurs [#_setNodeId] ; [] |112| 
	.dwpsn	file "../mms.c",line 115,column 3,is_stmt
        MOV       AH,*-SP[3]            ; [CPU_] |115| 
        MOVW      DP,#_ODP_Board_BaudRate ; [CPU_U] 
        MOV       AL,@_ODP_Board_BaudRate ; [CPU_] |115| 
$C$DW$177	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$177, DW_AT_low_pc(0x00)
	.dwattr $C$DW$177, DW_AT_name("_canInit")
	.dwattr $C$DW$177, DW_AT_TI_call
        LCR       #_canInit             ; [CPU_] |115| 
        ; call occurs [#_canInit] ; [] |115| 
	.dwpsn	file "../mms.c",line 118,column 1,is_stmt
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$168, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$168, DW_AT_TI_end_line(0x76)
	.dwattr $C$DW$168, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$168

	.sect	".text"
	.global	_FnInitialiseNode

$C$DW$179	.dwtag  DW_TAG_subprogram, DW_AT_name("FnInitialiseNode")
	.dwattr $C$DW$179, DW_AT_low_pc(_FnInitialiseNode)
	.dwattr $C$DW$179, DW_AT_high_pc(0x00)
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_FnInitialiseNode")
	.dwattr $C$DW$179, DW_AT_external
	.dwattr $C$DW$179, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$179, DW_AT_TI_begin_line(0x7c)
	.dwattr $C$DW$179, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$179, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../mms.c",line 124,column 35,is_stmt,address _FnInitialiseNode

	.dwfde $C$DW$CIE, _FnInitialiseNode
$C$DW$180	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$180, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnInitialiseNode             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_FnInitialiseNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$181	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$181, DW_AT_location[DW_OP_breg20 -2]
$C$DW$182	.dwtag  DW_TAG_variable, DW_AT_name("CrcFirmwareTest")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_CrcFirmwareTest")
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$182, DW_AT_location[DW_OP_breg20 -3]
        MOVL      *-SP[2],XAR4          ; [CPU_] |124| 
	.dwpsn	file "../mms.c",line 125,column 26,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |125| 
	.dwpsn	file "../mms.c",line 127,column 3,is_stmt
$C$DW$183	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$183, DW_AT_low_pc(0x00)
	.dwattr $C$DW$183, DW_AT_name("_genCRC32Table")
	.dwattr $C$DW$183, DW_AT_TI_call
        LCR       #_genCRC32Table       ; [CPU_] |127| 
        ; call occurs [#_genCRC32Table] ; [] |127| 
	.dwpsn	file "../mms.c",line 129,column 3,is_stmt
$C$DW$184	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$184, DW_AT_low_pc(0x00)
	.dwattr $C$DW$184, DW_AT_name("_CheckfirmwareCRC")
	.dwattr $C$DW$184, DW_AT_TI_call
        LCR       #_CheckfirmwareCRC    ; [CPU_] |129| 
        ; call occurs [#_CheckfirmwareCRC] ; [] |129| 
        MOV       *-SP[3],AL            ; [CPU_] |129| 
	.dwpsn	file "../mms.c",line 130,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+19   ; [CPU_U] 
        OR        @_PieCtrlRegs+19,#0x0020 ; [CPU_] |130| 
	.dwpsn	file "../mms.c",line 131,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |131| 
	.dwpsn	file "../mms.c",line 132,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,#_ODP_Board_Config ; [CPU_U] |132| 
        MOVL      @_MMSConfig,XAR4      ; [CPU_] |132| 
	.dwpsn	file "../mms.c",line 133,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |133| 
$C$DW$185	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$185, DW_AT_low_pc(0x00)
	.dwattr $C$DW$185, DW_AT_name("_PAR_InitParam")
	.dwattr $C$DW$185, DW_AT_TI_call
        LCR       #_PAR_InitParam       ; [CPU_] |133| 
        ; call occurs [#_PAR_InitParam] ; [] |133| 
        CMPB      AL,#0                 ; [CPU_] |133| 
        BF        $C$L8,EQ              ; [CPU_] |133| 
        ; branchcc occurs ; [] |133| 
        MOV       AL,*-SP[3]            ; [CPU_] |133| 
        CMPB      AL,#1                 ; [CPU_] |133| 
        BF        $C$L8,NEQ             ; [CPU_] |133| 
        ; branchcc occurs ; [] |133| 
	.dwpsn	file "../mms.c",line 135,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#1,UNC       ; [CPU_] |135| 
	.dwpsn	file "../mms.c",line 136,column 3,is_stmt
        B         $C$L9,UNC             ; [CPU_] |136| 
        ; branch occurs ; [] |136| 
$C$L8:    
	.dwpsn	file "../mms.c",line 138,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#2,UNC       ; [CPU_] |138| 
	.dwpsn	file "../mms.c",line 139,column 5,is_stmt
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("_WARN_ErrorParam")
	.dwattr $C$DW$186, DW_AT_TI_call
        LCR       #_WARN_ErrorParam     ; [CPU_] |139| 
        ; call occurs [#_WARN_ErrorParam] ; [] |139| 
$C$L9:    
	.dwpsn	file "../mms.c",line 141,column 3,is_stmt
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$187, DW_AT_TI_call
        LCR       #_PAR_SetParamDependantVars ; [CPU_] |141| 
        ; call occurs [#_PAR_SetParamDependantVars] ; [] |141| 
	.dwpsn	file "../mms.c",line 142,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$179, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$179, DW_AT_TI_end_line(0x8e)
	.dwattr $C$DW$179, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$179

	.sect	".text"
	.global	_FnEnterPreOperational

$C$DW$189	.dwtag  DW_TAG_subprogram, DW_AT_name("FnEnterPreOperational")
	.dwattr $C$DW$189, DW_AT_low_pc(_FnEnterPreOperational)
	.dwattr $C$DW$189, DW_AT_high_pc(0x00)
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_FnEnterPreOperational")
	.dwattr $C$DW$189, DW_AT_external
	.dwattr $C$DW$189, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$189, DW_AT_TI_begin_line(0x92)
	.dwattr $C$DW$189, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$189, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../mms.c",line 146,column 40,is_stmt,address _FnEnterPreOperational

	.dwfde $C$DW$CIE, _FnEnterPreOperational
$C$DW$190	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$190, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnEnterPreOperational        FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnEnterPreOperational:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$191	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$191, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |146| 
	.dwpsn	file "../mms.c",line 147,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOVB      @_ODV_Controlword,#6,UNC ; [CPU_] |147| 
	.dwpsn	file "../mms.c",line 148,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$192	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$192, DW_AT_low_pc(0x00)
	.dwattr $C$DW$192, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$189, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$189, DW_AT_TI_end_line(0x94)
	.dwattr $C$DW$189, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$189

	.sect	".text"
	.global	_FnStartNode

$C$DW$193	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStartNode")
	.dwattr $C$DW$193, DW_AT_low_pc(_FnStartNode)
	.dwattr $C$DW$193, DW_AT_high_pc(0x00)
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_FnStartNode")
	.dwattr $C$DW$193, DW_AT_external
	.dwattr $C$DW$193, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$193, DW_AT_TI_begin_line(0x97)
	.dwattr $C$DW$193, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$193, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../mms.c",line 151,column 30,is_stmt,address _FnStartNode

	.dwfde $C$DW$CIE, _FnStartNode
$C$DW$194	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStartNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStartNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$195	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |151| 
	.dwpsn	file "../mms.c",line 153,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$193, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$193, DW_AT_TI_end_line(0x99)
	.dwattr $C$DW$193, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$193

	.sect	".text"
	.global	_FnStopNode

$C$DW$197	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStopNode")
	.dwattr $C$DW$197, DW_AT_low_pc(_FnStopNode)
	.dwattr $C$DW$197, DW_AT_high_pc(0x00)
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_FnStopNode")
	.dwattr $C$DW$197, DW_AT_external
	.dwattr $C$DW$197, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$197, DW_AT_TI_begin_line(0x9c)
	.dwattr $C$DW$197, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$197, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../mms.c",line 156,column 29,is_stmt,address _FnStopNode

	.dwfde $C$DW$CIE, _FnStopNode
$C$DW$198	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStopNode                   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStopNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$199	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |156| 
	.dwpsn	file "../mms.c",line 158,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |158| 
	.dwpsn	file "../mms.c",line 159,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$200	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$200, DW_AT_low_pc(0x00)
	.dwattr $C$DW$200, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$197, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$197, DW_AT_TI_end_line(0x9f)
	.dwattr $C$DW$197, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$197

	.sect	".text"
	.global	_canOpenInit

$C$DW$201	.dwtag  DW_TAG_subprogram, DW_AT_name("canOpenInit")
	.dwattr $C$DW$201, DW_AT_low_pc(_canOpenInit)
	.dwattr $C$DW$201, DW_AT_high_pc(0x00)
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_canOpenInit")
	.dwattr $C$DW$201, DW_AT_external
	.dwattr $C$DW$201, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$201, DW_AT_TI_begin_line(0xa3)
	.dwattr $C$DW$201, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$201, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../mms.c",line 165,column 1,is_stmt,address _canOpenInit

	.dwfde $C$DW$CIE, _canOpenInit

;***************************************************************
;* FNAME: _canOpenInit                  FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_canOpenInit:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../mms.c",line 167,column 3,is_stmt
        MOVL      XAR4,#_ODI_mms_dict_Data ; [CPU_U] |167| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      @_BoardODdata,XAR4    ; [CPU_] |167| 
	.dwpsn	file "../mms.c",line 169,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |169| 
        MOVB      XAR0,#84              ; [CPU_] |169| 
        MOVL      XAR4,#_FnInitialiseNode ; [CPU_U] |169| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |169| 
	.dwpsn	file "../mms.c",line 170,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |170| 
        MOVB      XAR0,#86              ; [CPU_] |170| 
        MOVL      XAR4,#_FnEnterPreOperational ; [CPU_U] |170| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |170| 
	.dwpsn	file "../mms.c",line 171,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |171| 
        MOVB      XAR0,#88              ; [CPU_] |171| 
        MOVL      XAR4,#_FnStartNode    ; [CPU_U] |171| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |171| 
	.dwpsn	file "../mms.c",line 172,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |172| 
        MOVB      XAR0,#90              ; [CPU_] |172| 
        MOVL      XAR4,#_FnStopNode     ; [CPU_U] |172| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |172| 
	.dwpsn	file "../mms.c",line 173,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |173| 
        MOVB      XAR0,#92              ; [CPU_] |173| 
        MOVL      XAR4,#_FnResetNode    ; [CPU_U] |173| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |173| 
	.dwpsn	file "../mms.c",line 174,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |174| 
        MOVB      XAR0,#94              ; [CPU_] |174| 
        MOVL      XAR4,#_FnResetCommunications ; [CPU_U] |174| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |174| 
	.dwpsn	file "../mms.c",line 176,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |176| 
        MOVB      XAR0,#252             ; [CPU_] |176| 
        MOVL      XAR4,#_PAR_StoreODSubIndex ; [CPU_U] |176| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |176| 
	.dwpsn	file "../mms.c",line 178,column 3,is_stmt
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |178| 
        MOVB      ACC,#0                ; [CPU_] |178| 
        MOVB      XAR0,#254             ; [CPU_] |178| 
        MOVL      *+XAR4[AR0],ACC       ; [CPU_] |178| 
	.dwpsn	file "../mms.c",line 182,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |182| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |182| 
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_name("_setState")
	.dwattr $C$DW$202, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |182| 
        ; call occurs [#_setState] ; [] |182| 
	.dwpsn	file "../mms.c",line 183,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |183| 
$C$DW$203	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$203, DW_AT_low_pc(0x00)
	.dwattr $C$DW$203, DW_AT_name("_FnResetCommunications")
	.dwattr $C$DW$203, DW_AT_TI_call
        LCR       #_FnResetCommunications ; [CPU_] |183| 
        ; call occurs [#_FnResetCommunications] ; [] |183| 
	.dwpsn	file "../mms.c",line 190,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AL,#5                 ; [CPU_] |190| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |190| 
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_name("_setState")
	.dwattr $C$DW$204, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |190| 
        ; call occurs [#_setState] ; [] |190| 
	.dwpsn	file "../mms.c",line 192,column 1,is_stmt
$C$DW$205	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$205, DW_AT_low_pc(0x00)
	.dwattr $C$DW$205, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$201, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$201, DW_AT_TI_end_line(0xc0)
	.dwattr $C$DW$201, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$201

	.sect	".text"
	.global	_taskFnGestionMachine

$C$DW$206	.dwtag  DW_TAG_subprogram, DW_AT_name("taskFnGestionMachine")
	.dwattr $C$DW$206, DW_AT_low_pc(_taskFnGestionMachine)
	.dwattr $C$DW$206, DW_AT_high_pc(0x00)
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_taskFnGestionMachine")
	.dwattr $C$DW$206, DW_AT_external
	.dwattr $C$DW$206, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$206, DW_AT_TI_begin_line(0xcd)
	.dwattr $C$DW$206, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$206, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../mms.c",line 205,column 32,is_stmt,address _taskFnGestionMachine

	.dwfde $C$DW$CIE, _taskFnGestionMachine

;***************************************************************
;* FNAME: _taskFnGestionMachine         FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_taskFnGestionMachine:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$207	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_breg20 -2]
$C$DW$208	.dwtag  DW_TAG_variable, DW_AT_name("statusi2c")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_statusi2c")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_breg20 -3]
$C$DW$209	.dwtag  DW_TAG_variable, DW_AT_name("old_time")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_old_time")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_breg20 -6]
$C$DW$210	.dwtag  DW_TAG_variable, DW_AT_name("count")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$210, DW_AT_location[DW_OP_breg20 -8]
	.dwpsn	file "../mms.c",line 206,column 13,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |206| 
	.dwpsn	file "../mms.c",line 206,column 28,is_stmt
        MOVB      *-SP[3],#20,UNC       ; [CPU_] |206| 
	.dwpsn	file "../mms.c",line 207,column 26,is_stmt
        MOVB      ACC,#0                ; [CPU_] |207| 
        MOVL      *-SP[8],ACC           ; [CPU_] |207| 
	.dwpsn	file "../mms.c",line 211,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |211| 
        MOVB      AL,#2                 ; [CPU_] |211| 
$C$DW$211	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$211, DW_AT_low_pc(0x00)
	.dwattr $C$DW$211, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$211, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |211| 
        ; call occurs [#_SEM_pend] ; [] |211| 
	.dwpsn	file "../mms.c",line 212,column 3,is_stmt
$C$DW$212	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$212, DW_AT_low_pc(0x00)
	.dwattr $C$DW$212, DW_AT_name("_HAL_Init")
	.dwattr $C$DW$212, DW_AT_TI_call
        LCR       #_HAL_Init            ; [CPU_] |212| 
        ; call occurs [#_HAL_Init] ; [] |212| 
	.dwpsn	file "../mms.c",line 213,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |213| 
        BF        $C$L11,NEQ            ; [CPU_] |213| 
        ; branchcc occurs ; [] |213| 
$C$L10:    
	.dwpsn	file "../mms.c",line 214,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |214| 
        MOVB      AL,#1                 ; [CPU_] |214| 
$C$DW$213	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$213, DW_AT_low_pc(0x00)
	.dwattr $C$DW$213, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$213, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |214| 
        ; call occurs [#_SEM_pend] ; [] |214| 
	.dwpsn	file "../mms.c",line 213,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |213| 
        BF        $C$L10,EQ             ; [CPU_] |213| 
        ; branchcc occurs ; [] |213| 
$C$L11:    
	.dwpsn	file "../mms.c",line 216,column 3,is_stmt
$C$DW$214	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$214, DW_AT_low_pc(0x00)
	.dwattr $C$DW$214, DW_AT_name("_USB_Start")
	.dwattr $C$DW$214, DW_AT_TI_call
        LCR       #_USB_Start           ; [CPU_] |216| 
        ; call occurs [#_USB_Start] ; [] |216| 
	.dwpsn	file "../mms.c",line 217,column 3,is_stmt
        MOVW      DP,#_SCI_MsgInRS232   ; [CPU_U] 
        MOVB      @_SCI_MsgInRS232,#70,UNC ; [CPU_] |217| 
	.dwpsn	file "../mms.c",line 217,column 42,is_stmt
$C$DW$215	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$215, DW_AT_low_pc(0x00)
	.dwattr $C$DW$215, DW_AT_name("_SCI1_Command")
	.dwattr $C$DW$215, DW_AT_TI_call
        LCR       #_SCI1_Command        ; [CPU_] |217| 
        ; call occurs [#_SCI1_Command] ; [] |217| 
	.dwpsn	file "../mms.c",line 218,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |218| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |218| 
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$216, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |218| 
        ; call occurs [#_SEM_pend] ; [] |218| 
	.dwpsn	file "../mms.c",line 219,column 3,is_stmt
        MOVW      DP,#_SCI_MsgInRS232   ; [CPU_U] 
        MOVB      @_SCI_MsgInRS232,#65,UNC ; [CPU_] |219| 
	.dwpsn	file "../mms.c",line 219,column 38,is_stmt
$C$DW$217	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$217, DW_AT_low_pc(0x00)
	.dwattr $C$DW$217, DW_AT_name("_SCI1_Command")
	.dwattr $C$DW$217, DW_AT_TI_call
        LCR       #_SCI1_Command        ; [CPU_] |219| 
        ; call occurs [#_SCI1_Command] ; [] |219| 
	.dwpsn	file "../mms.c",line 220,column 10,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOV       AL,@_SCI_MsgAvailable ; [CPU_] |220| 
        BF        $C$L13,NEQ            ; [CPU_] |220| 
        ; branchcc occurs ; [] |220| 
$C$L12:    
	.dwpsn	file "../mms.c",line 221,column 4,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |221| 
        MOVB      AL,#1                 ; [CPU_] |221| 
$C$DW$218	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$218, DW_AT_low_pc(0x00)
	.dwattr $C$DW$218, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$218, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |221| 
        ; call occurs [#_SEM_pend] ; [] |221| 
	.dwpsn	file "../mms.c",line 220,column 10,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOV       AL,@_SCI_MsgAvailable ; [CPU_] |220| 
        BF        $C$L12,EQ             ; [CPU_] |220| 
        ; branchcc occurs ; [] |220| 
$C$L13:    
	.dwpsn	file "../mms.c",line 223,column 3,is_stmt
$C$DW$219	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$219, DW_AT_low_pc(0x00)
	.dwattr $C$DW$219, DW_AT_name("_SCI1_Receive")
	.dwattr $C$DW$219, DW_AT_TI_call
        LCR       #_SCI1_Receive        ; [CPU_] |223| 
        ; call occurs [#_SCI1_Receive] ; [] |223| 
	.dwpsn	file "../mms.c",line 224,column 3,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOV       @_SCI_MsgAvailable,#0 ; [CPU_] |224| 
	.dwpsn	file "../mms.c",line 225,column 3,is_stmt
        MOVW      DP,#_ODP_VersionParameters ; [CPU_U] 
        MOV       AL,@_ODP_VersionParameters ; [CPU_] |225| 
        CMPB      AL,#128               ; [CPU_] |225| 
        B         $C$L14,HI             ; [CPU_] |225| 
        ; branchcc occurs ; [] |225| 
	.dwpsn	file "../mms.c",line 226,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |226| 
        MOVB      AL,#1                 ; [CPU_] |226| 
$C$DW$220	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$220, DW_AT_low_pc(0x00)
	.dwattr $C$DW$220, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$220, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |226| 
        ; call occurs [#_SEM_pend] ; [] |226| 
	.dwpsn	file "../mms.c",line 227,column 5,is_stmt
        MOVW      DP,#_SCI_MsgInRS232   ; [CPU_U] 
        MOVB      @_SCI_MsgInRS232,#82,UNC ; [CPU_] |227| 
	.dwpsn	file "../mms.c",line 227,column 44,is_stmt
$C$DW$221	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$221, DW_AT_low_pc(0x00)
	.dwattr $C$DW$221, DW_AT_name("_SCI1_Command")
	.dwattr $C$DW$221, DW_AT_TI_call
        LCR       #_SCI1_Command        ; [CPU_] |227| 
        ; call occurs [#_SCI1_Command] ; [] |227| 
	.dwpsn	file "../mms.c",line 228,column 5,is_stmt
        MOVW      DP,#_ODP_VersionParameters ; [CPU_U] 
        MOVB      @_ODP_VersionParameters,#129,UNC ; [CPU_] |228| 
	.dwpsn	file "../mms.c",line 229,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |229| 
        MOV       AL,#8194              ; [CPU_] |229| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |229| 
$C$DW$222	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$222, DW_AT_low_pc(0x00)
	.dwattr $C$DW$222, DW_AT_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$222, DW_AT_TI_call
        LCR       #_PAR_StoreODSubIndex ; [CPU_] |229| 
        ; call occurs [#_PAR_StoreODSubIndex] ; [] |229| 
$C$L14:    
	.dwpsn	file "../mms.c",line 231,column 3,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |231| 
	.dwpsn	file "../mms.c",line 232,column 3,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |232| 
        MOVL      *-SP[6],ACC           ; [CPU_] |232| 
	.dwpsn	file "../mms.c",line 233,column 3,is_stmt
        B         $C$L28,UNC            ; [CPU_] |233| 
        ; branch occurs ; [] |233| 
$C$L15:    
	.dwpsn	file "../mms.c",line 237,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVB      XAR6,#1               ; [CPU_] |237| 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |237| 
        MOVB      AH,#0                 ; [CPU_] |237| 
        ANDB      AL,#0x3f              ; [CPU_] |237| 
        CMPL      ACC,XAR6              ; [CPU_] |237| 
        B         $C$L16,HI             ; [CPU_] |237| 
        ; branchcc occurs ; [] |237| 
        MOVB      XAR6,#62              ; [CPU_] |237| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |237| 
        MOVB      AH,#0                 ; [CPU_] |237| 
        ANDB      AL,#0x3f              ; [CPU_] |237| 
        CMPL      ACC,XAR6              ; [CPU_] |237| 
        B         $C$L16,LO             ; [CPU_] |237| 
        ; branchcc occurs ; [] |237| 
	.dwpsn	file "../mms.c",line 239,column 7,is_stmt
        MOVB      *-SP[3],#20,UNC       ; [CPU_] |239| 
$C$L16:    
	.dwpsn	file "../mms.c",line 241,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |241| 
        CMPB      AL,#20                ; [CPU_] |241| 
        BF        $C$L17,NEQ            ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
	.dwpsn	file "../mms.c",line 242,column 7,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |242| 
        MOV       *-SP[1],#0            ; [CPU_] |242| 
        MOVB      AL,#9                 ; [CPU_] |242| 
        MOVL      XAR4,#_ODV_Gateway_Date_Time ; [CPU_U] |242| 
        MOVB      AH,#7                 ; [CPU_] |242| 
        SUBB      XAR5,#3               ; [CPU_U] |242| 
$C$DW$223	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$223, DW_AT_low_pc(0x00)
	.dwattr $C$DW$223, DW_AT_name("_I2C_CommandNoWait")
	.dwattr $C$DW$223, DW_AT_TI_call
        LCR       #_I2C_CommandNoWait   ; [CPU_] |242| 
        ; call occurs [#_I2C_CommandNoWait] ; [] |242| 
$C$L17:    
	.dwpsn	file "../mms.c",line 244,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |244| 
        CMPB      AL,#10                ; [CPU_] |244| 
        BF        $C$L18,NEQ            ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
	.dwpsn	file "../mms.c",line 245,column 7,is_stmt
        MOVB      *-SP[3],#30,UNC       ; [CPU_] |245| 
	.dwpsn	file "../mms.c",line 246,column 7,is_stmt
        MOV       AL,#-1                ; [CPU_] |246| 
$C$DW$224	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$224, DW_AT_low_pc(0x00)
	.dwattr $C$DW$224, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$224, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |246| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |246| 
	.dwpsn	file "../mms.c",line 247,column 7,is_stmt
        MOV       AL,#-5                ; [CPU_] |247| 
$C$DW$225	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$225, DW_AT_low_pc(0x00)
	.dwattr $C$DW$225, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$225, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |247| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |247| 
$C$L18:    
	.dwpsn	file "../mms.c",line 249,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |249| 
        MOVL      *-SP[6],ACC           ; [CPU_] |249| 
	.dwpsn	file "../mms.c",line 250,column 5,is_stmt
        MOVB      XAR6,#10              ; [CPU_] |250| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |250| 
        SUBL      ACC,*-SP[8]           ; [CPU_] |250| 
        CMPL      ACC,XAR6              ; [CPU_] |250| 
        B         $C$L21,LO             ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
	.dwpsn	file "../mms.c",line 251,column 7,is_stmt
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |251| 
        MOVL      *-SP[8],ACC           ; [CPU_] |251| 
	.dwpsn	file "../mms.c",line 252,column 7,is_stmt
        MOVB      *-SP[2],#71,UNC       ; [CPU_] |252| 
	.dwpsn	file "../mms.c",line 253,column 7,is_stmt
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        MOVB      ACC,#3                ; [CPU_] |253| 
        CMPL      ACC,@_ODV_CommError_Counter ; [CPU_] |253| 
        B         $C$L20,LOS            ; [CPU_] |253| 
        ; branchcc occurs ; [] |253| 
	.dwpsn	file "../mms.c",line 254,column 9,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |254| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |254| 
        MOVB      AL,#0                 ; [CPU_] |254| 
        SUBB      XAR5,#2               ; [CPU_U] |254| 
$C$DW$226	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$226, DW_AT_low_pc(0x00)
	.dwattr $C$DW$226, DW_AT_name("_MBX_post")
	.dwattr $C$DW$226, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |254| 
        ; call occurs [#_MBX_post] ; [] |254| 
        CMPB      AL,#0                 ; [CPU_] |254| 
        BF        $C$L19,NEQ            ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
	.dwpsn	file "../mms.c",line 255,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |255| 
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        ADDL      @_ODV_CommError_Counter,ACC ; [CPU_] |255| 
	.dwpsn	file "../mms.c",line 256,column 11,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOV       @_SCI_Available,#0    ; [CPU_] |256| 
	.dwpsn	file "../mms.c",line 257,column 11,is_stmt
$C$DW$227	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$227, DW_AT_low_pc(0x00)
	.dwattr $C$DW$227, DW_AT_name("_RS232ReceiveEnable")
	.dwattr $C$DW$227, DW_AT_TI_call
        LCR       #_RS232ReceiveEnable  ; [CPU_] |257| 
        ; call occurs [#_RS232ReceiveEnable] ; [] |257| 
	.dwpsn	file "../mms.c",line 258,column 11,is_stmt
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        MOVB      ACC,#3                ; [CPU_] |258| 
        CMPL      ACC,@_ODV_CommError_Counter ; [CPU_] |258| 
        B         $C$L20,HI             ; [CPU_] |258| 
        ; branchcc occurs ; [] |258| 
	.dwpsn	file "../mms.c",line 259,column 13,is_stmt
$C$DW$228	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$228, DW_AT_low_pc(0x00)
	.dwattr $C$DW$228, DW_AT_name("_ERR_ErrorComm")
	.dwattr $C$DW$228, DW_AT_TI_call
        LCR       #_ERR_ErrorComm       ; [CPU_] |259| 
        ; call occurs [#_ERR_ErrorComm] ; [] |259| 
	.dwpsn	file "../mms.c",line 260,column 9,is_stmt
        B         $C$L20,UNC            ; [CPU_] |260| 
        ; branch occurs ; [] |260| 
$C$L19:    
	.dwpsn	file "../mms.c",line 261,column 14,is_stmt
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        MOVL      ACC,@_ODV_CommError_Counter ; [CPU_] |261| 
        BF        $C$L20,EQ             ; [CPU_] |261| 
        ; branchcc occurs ; [] |261| 
	.dwpsn	file "../mms.c",line 261,column 41,is_stmt
        MOVB      ACC,#1                ; [CPU_] |261| 
        SUBL      @_ODV_CommError_Counter,ACC ; [CPU_] |261| 
$C$L20:    
	.dwpsn	file "../mms.c",line 263,column 7,is_stmt
$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_name("_USB_Unlock")
	.dwattr $C$DW$229, DW_AT_TI_call
        LCR       #_USB_Unlock          ; [CPU_] |263| 
        ; call occurs [#_USB_Unlock] ; [] |263| 
$C$L21:    
	.dwpsn	file "../mms.c",line 265,column 5,is_stmt
        MOVW      DP,#_ODV_CommError_Set ; [CPU_U] 
        MOV       AL,@_ODV_CommError_Set ; [CPU_] |265| 
        CMPB      AL,#1                 ; [CPU_] |265| 
        BF        $C$L23,NEQ            ; [CPU_] |265| 
        ; branchcc occurs ; [] |265| 
	.dwpsn	file "../mms.c",line 266,column 7,is_stmt
        MOV       @_ODV_CommError_Set,#0 ; [CPU_] |266| 
	.dwpsn	file "../mms.c",line 267,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |267| 
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        MOVL      @_ODV_CommError_Counter,ACC ; [CPU_] |267| 
	.dwpsn	file "../mms.c",line 268,column 7,is_stmt
$C$L22:    
        MOVZ      AR5,SP                ; [CPU_U] |268| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |268| 
        MOVB      AL,#0                 ; [CPU_] |268| 
        SUBB      XAR5,#2               ; [CPU_U] |268| 
$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$230, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |268| 
        ; call occurs [#_MBX_pend] ; [] |268| 
        CMPB      AL,#0                 ; [CPU_] |268| 
        BF        $C$L22,NEQ            ; [CPU_] |268| 
        ; branchcc occurs ; [] |268| 
	.dwpsn	file "../mms.c",line 269,column 7,is_stmt
$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_name("_RS232ReceiveEnable")
	.dwattr $C$DW$231, DW_AT_TI_call
        LCR       #_RS232ReceiveEnable  ; [CPU_] |269| 
        ; call occurs [#_RS232ReceiveEnable] ; [] |269| 
$C$L23:    
	.dwpsn	file "../mms.c",line 271,column 5,is_stmt
        MOVW      DP,#_ODV_CommError_Set ; [CPU_U] 
        MOV       AL,@_ODV_CommError_Set ; [CPU_] |271| 
        CMPB      AL,#2                 ; [CPU_] |271| 
        BF        $C$L25,NEQ            ; [CPU_] |271| 
        ; branchcc occurs ; [] |271| 
	.dwpsn	file "../mms.c",line 272,column 7,is_stmt
        MOV       @_ODV_CommError_Set,#0 ; [CPU_] |272| 
	.dwpsn	file "../mms.c",line 273,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |273| 
        MOVW      DP,#_ODV_CommError_Counter ; [CPU_U] 
        MOVL      @_ODV_CommError_Counter,ACC ; [CPU_] |273| 
	.dwpsn	file "../mms.c",line 274,column 7,is_stmt
$C$L24:    
        MOVZ      AR5,SP                ; [CPU_U] |274| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |274| 
        MOVB      AL,#0                 ; [CPU_] |274| 
        SUBB      XAR5,#2               ; [CPU_U] |274| 
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$232, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |274| 
        ; call occurs [#_MBX_pend] ; [] |274| 
        CMPB      AL,#0                 ; [CPU_] |274| 
        BF        $C$L24,NEQ            ; [CPU_] |274| 
        ; branchcc occurs ; [] |274| 
	.dwpsn	file "../mms.c",line 275,column 7,is_stmt
        MOVB      *-SP[2],#70,UNC       ; [CPU_] |275| 
	.dwpsn	file "../mms.c",line 275,column 31,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |275| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |275| 
        MOVB      AL,#0                 ; [CPU_] |275| 
        SUBB      XAR5,#2               ; [CPU_U] |275| 
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_name("_MBX_post")
	.dwattr $C$DW$233, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |275| 
        ; call occurs [#_MBX_post] ; [] |275| 
$C$L25:    
	.dwpsn	file "../mms.c",line 277,column 5,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOV       AL,@_SCI_Available    ; [CPU_] |277| 
        BF        $C$L26,EQ             ; [CPU_] |277| 
        ; branchcc occurs ; [] |277| 
	.dwpsn	file "../mms.c",line 278,column 7,is_stmt
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |278| 
        MOVL      XAR5,#_SCI_MsgInRS232 ; [CPU_U] |278| 
        MOVB      AL,#0                 ; [CPU_] |278| 
$C$DW$234	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$234, DW_AT_low_pc(0x00)
	.dwattr $C$DW$234, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$234, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |278| 
        ; call occurs [#_MBX_pend] ; [] |278| 
        CMPB      AL,#0                 ; [CPU_] |278| 
        BF        $C$L26,EQ             ; [CPU_] |278| 
        ; branchcc occurs ; [] |278| 
	.dwpsn	file "../mms.c",line 279,column 9,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOV       @_SCI_Available,#0    ; [CPU_] |279| 
	.dwpsn	file "../mms.c",line 280,column 9,is_stmt
$C$DW$235	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$235, DW_AT_low_pc(0x00)
	.dwattr $C$DW$235, DW_AT_name("_SCI1_Command")
	.dwattr $C$DW$235, DW_AT_TI_call
        LCR       #_SCI1_Command        ; [CPU_] |280| 
        ; call occurs [#_SCI1_Command] ; [] |280| 
$C$L26:    
	.dwpsn	file "../mms.c",line 283,column 5,is_stmt
        MOVW      DP,#_SCI_MsgAvailable ; [CPU_U] 
        MOV       AL,@_SCI_MsgAvailable ; [CPU_] |283| 
        BF        $C$L27,EQ             ; [CPU_] |283| 
        ; branchcc occurs ; [] |283| 
	.dwpsn	file "../mms.c",line 284,column 7,is_stmt
        MOV       @_SCI_MsgAvailable,#0 ; [CPU_] |284| 
	.dwpsn	file "../mms.c",line 286,column 7,is_stmt
$C$DW$236	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$236, DW_AT_low_pc(0x00)
	.dwattr $C$DW$236, DW_AT_name("_SCI1_Receive")
	.dwattr $C$DW$236, DW_AT_TI_call
        LCR       #_SCI1_Receive        ; [CPU_] |286| 
        ; call occurs [#_SCI1_Receive] ; [] |286| 
	.dwpsn	file "../mms.c",line 287,column 7,is_stmt
        MOVW      DP,#_SCI_Available    ; [CPU_U] 
        MOVB      @_SCI_Available,#1,UNC ; [CPU_] |287| 
$C$L27:    
	.dwpsn	file "../mms.c",line 289,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |289| 
        MOVB      AL,#1                 ; [CPU_] |289| 
$C$DW$237	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$237, DW_AT_low_pc(0x00)
	.dwattr $C$DW$237, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$237, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |289| 
        ; call occurs [#_SEM_pend] ; [] |289| 
$C$L28:    
	.dwpsn	file "../mms.c",line 233,column 9,is_stmt
        MOVW      DP,#_BootST           ; [CPU_U] 
        MOVL      ACC,@_BootST          ; [CPU_] |233| 
        BF        $C$L15,EQ             ; [CPU_] |233| 
        ; branchcc occurs ; [] |233| 
	.dwpsn	file "../mms.c",line 291,column 3,is_stmt
$C$DW$238	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$238, DW_AT_low_pc(0x00)
	.dwattr $C$DW$238, DW_AT_name("_USB_Stop")
	.dwattr $C$DW$238, DW_AT_TI_call
        LCR       #_USB_Stop            ; [CPU_] |291| 
        ; call occurs [#_USB_Stop] ; [] |291| 
	.dwpsn	file "../mms.c",line 292,column 3,is_stmt
        MOVB      AL,#2                 ; [CPU_] |292| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |292| 
$C$DW$239	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$239, DW_AT_low_pc(0x00)
	.dwattr $C$DW$239, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$239, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |292| 
        ; call occurs [#_SEM_pend] ; [] |292| 
	.dwpsn	file "../mms.c",line 293,column 3,is_stmt
$C$DW$240	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$240, DW_AT_low_pc(0x00)
	.dwattr $C$DW$240, DW_AT_name("_HAL_Reset")
	.dwattr $C$DW$240, DW_AT_TI_call
        LCR       #_HAL_Reset           ; [CPU_] |293| 
        ; call occurs [#_HAL_Reset] ; [] |293| 
	.dwpsn	file "../mms.c",line 294,column 3,is_stmt
        MOV       AL,#28009             ; [CPU_] |294| 
        MOV       AH,#21093             ; [CPU_] |294| 
        MOVW      DP,#_BootST           ; [CPU_U] 
        MOVL      @_BootST,ACC          ; [CPU_] |294| 
	.dwpsn	file "../mms.c",line 295,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOVL      ACC,@_ODP_Board_RevisionNumber ; [CPU_] |295| 
        MOVW      DP,#_BootST+2         ; [CPU_U] 
        MOVL      @_BootST+2,ACC        ; [CPU_] |295| 
	.dwpsn	file "../mms.c",line 296,column 3,is_stmt
 LB 0x3F7FF6 
	.dwpsn	file "../mms.c",line 297,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$241	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$241, DW_AT_low_pc(0x00)
	.dwattr $C$DW$241, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$206, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$206, DW_AT_TI_end_line(0x129)
	.dwattr $C$DW$206, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$206

	.sect	".text"
	.global	_TaskSciSendReceive

$C$DW$242	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskSciSendReceive")
	.dwattr $C$DW$242, DW_AT_low_pc(_TaskSciSendReceive)
	.dwattr $C$DW$242, DW_AT_high_pc(0x00)
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_TaskSciSendReceive")
	.dwattr $C$DW$242, DW_AT_external
	.dwattr $C$DW$242, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$242, DW_AT_TI_begin_line(0x12f)
	.dwattr $C$DW$242, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$242, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../mms.c",line 303,column 30,is_stmt,address _TaskSciSendReceive

	.dwfde $C$DW$CIE, _TaskSciSendReceive

;***************************************************************
;* FNAME: _TaskSciSendReceive           FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter, 18 Auto,  0 SOE     *
;***************************************************************

_TaskSciSendReceive:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$243	.dwtag  DW_TAG_variable, DW_AT_name("start")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_start")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_breg20 -4]
$C$DW$244	.dwtag  DW_TAG_variable, DW_AT_name("insulation_counter")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_insulation_counter")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_breg20 -6]
$C$DW$245	.dwtag  DW_TAG_variable, DW_AT_name("current_counter")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_current_counter")
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$245, DW_AT_location[DW_OP_breg20 -8]
$C$DW$246	.dwtag  DW_TAG_variable, DW_AT_name("sleep_counter")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_sleep_counter")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_breg20 -10]
$C$DW$247	.dwtag  DW_TAG_variable, DW_AT_name("volt_count")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_volt_count")
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$247, DW_AT_location[DW_OP_breg20 -12]
$C$DW$248	.dwtag  DW_TAG_variable, DW_AT_name("cur")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_cur")
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$248, DW_AT_location[DW_OP_breg20 -13]
$C$DW$249	.dwtag  DW_TAG_variable, DW_AT_name("volt")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_volt")
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$249, DW_AT_location[DW_OP_breg20 -14]
$C$DW$250	.dwtag  DW_TAG_variable, DW_AT_name("volt1")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_volt1")
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$250, DW_AT_location[DW_OP_breg20 -15]
$C$DW$251	.dwtag  DW_TAG_variable, DW_AT_name("curf")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_curf")
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$251, DW_AT_location[DW_OP_breg20 -18]
$C$DW$252	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_breg20 -19]
$C$DW$253	.dwtag  DW_TAG_variable, DW_AT_name("checkenable")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_checkenable")
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$253, DW_AT_location[DW_OP_breg20 -20]
	.dwpsn	file "../mms.c",line 304,column 45,is_stmt
        MOVB      ACC,#0                ; [CPU_] |304| 
        MOVL      *-SP[6],ACC           ; [CPU_] |304| 
	.dwpsn	file "../mms.c",line 304,column 66,is_stmt
        MOVL      *-SP[8],ACC           ; [CPU_] |304| 
	.dwpsn	file "../mms.c",line 304,column 85,is_stmt
        MOVL      *-SP[10],ACC          ; [CPU_] |304| 
	.dwpsn	file "../mms.c",line 304,column 101,is_stmt
        MOVL      *-SP[12],ACC          ; [CPU_] |304| 
	.dwpsn	file "../mms.c",line 308,column 21,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |308| 
	.dwpsn	file "../mms.c",line 309,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#-1 ; [CPU_] |309| 
	.dwpsn	file "../mms.c",line 310,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |310| 
        BF        $C$L30,NEQ            ; [CPU_] |310| 
        ; branchcc occurs ; [] |310| 
$C$L29:    
	.dwpsn	file "../mms.c",line 311,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |311| 
        MOVB      AL,#1                 ; [CPU_] |311| 
$C$DW$254	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$254, DW_AT_low_pc(0x00)
	.dwattr $C$DW$254, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$254, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |311| 
        ; call occurs [#_SEM_pend] ; [] |311| 
	.dwpsn	file "../mms.c",line 310,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |310| 
        BF        $C$L29,EQ             ; [CPU_] |310| 
        ; branchcc occurs ; [] |310| 
$C$L30:    
	.dwpsn	file "../mms.c",line 313,column 10,is_stmt
        CMPB      AL,#1                 ; [CPU_] |313| 
        BF        $C$L32,EQ             ; [CPU_] |313| 
        ; branchcc occurs ; [] |313| 
$C$L31:    
	.dwpsn	file "../mms.c",line 315,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |315| 
        MOVB      AL,#1                 ; [CPU_] |315| 
$C$DW$255	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$255, DW_AT_low_pc(0x00)
	.dwattr $C$DW$255, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$255, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |315| 
        ; call occurs [#_SEM_pend] ; [] |315| 
	.dwpsn	file "../mms.c",line 313,column 10,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |313| 
        CMPB      AL,#1                 ; [CPU_] |313| 
        BF        $C$L31,NEQ            ; [CPU_] |313| 
        ; branchcc occurs ; [] |313| 
$C$L32:    
	.dwpsn	file "../mms.c",line 317,column 3,is_stmt
        MOVW      DP,#_ODV_MachineEvent ; [CPU_U] 
        MOV       @_ODV_MachineEvent,#0 ; [CPU_] |317| 
	.dwpsn	file "../mms.c",line 318,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |318| 
	.dwpsn	file "../mms.c",line 319,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |319| 
	.dwpsn	file "../mms.c",line 320,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |320| 
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      @_ODV_Gateway_Errorcode,ACC ; [CPU_] |320| 
	.dwpsn	file "../mms.c",line 322,column 3,is_stmt
        MOVW      DP,#_ODV_Module1_Status ; [CPU_U] 
        MOV       @_ODV_Module1_Status,#0 ; [CPU_] |322| 
	.dwpsn	file "../mms.c",line 323,column 3,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |323| 
        MOVL      *-SP[4],ACC           ; [CPU_] |323| 
	.dwpsn	file "../mms.c",line 324,column 9,is_stmt
        MOVW      DP,#_HAL_Enable       ; [CPU_U] 
        MOV       AL,@_HAL_Enable       ; [CPU_] |324| 
        BF        $C$L34,NEQ            ; [CPU_] |324| 
        ; branchcc occurs ; [] |324| 
$C$L33:    
	.dwpsn	file "../mms.c",line 325,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |325| 
        MOVB      AL,#1                 ; [CPU_] |325| 
$C$DW$256	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$256, DW_AT_low_pc(0x00)
	.dwattr $C$DW$256, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$256, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |325| 
        ; call occurs [#_SEM_pend] ; [] |325| 
	.dwpsn	file "../mms.c",line 324,column 9,is_stmt
        MOVW      DP,#_HAL_Enable       ; [CPU_U] 
        MOV       AL,@_HAL_Enable       ; [CPU_] |324| 
        BF        $C$L33,EQ             ; [CPU_] |324| 
        ; branchcc occurs ; [] |324| 
$C$L34:    
	.dwpsn	file "../mms.c",line 327,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |327| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |327| 
        BF        $C$L35,TC             ; [CPU_] |327| 
        ; branchcc occurs ; [] |327| 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |327| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |327| 
        LSR       AL,1                  ; [CPU_] |327| 
        CMPB      AL,#1                 ; [CPU_] |327| 
        BF        $C$L35,NEQ            ; [CPU_] |327| 
        ; branchcc occurs ; [] |327| 
	.dwpsn	file "../mms.c",line 329,column 3,is_stmt
        MOVW      DP,#_ODP_CommError_TimeOut ; [CPU_U] 
        MOV       @_ODP_CommError_TimeOut,#0 ; [CPU_] |329| 
$C$L35:    
	.dwpsn	file "../mms.c",line 331,column 10,is_stmt
$C$L36:    
	.dwpsn	file "../mms.c",line 333,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |333| 
        CMPB      AL,#8                 ; [CPU_] |333| 
        BF        $C$L37,NEQ            ; [CPU_] |333| 
        ; branchcc occurs ; [] |333| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |333| 
        CMPB      AL,#8                 ; [CPU_] |333| 
        BF        $C$L37,EQ             ; [CPU_] |333| 
        ; branchcc occurs ; [] |333| 
	.dwpsn	file "../mms.c",line 334,column 7,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |334| 
        MOVL      *-SP[4],ACC           ; [CPU_] |334| 
	.dwpsn	file "../mms.c",line 335,column 7,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |335| 
	.dwpsn	file "../mms.c",line 336,column 7,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#8,UNC ; [CPU_] |336| 
$C$L37:    
	.dwpsn	file "../mms.c",line 338,column 5,is_stmt
        MOVW      DP,#_ODV_Module1_MaxCellVoltage ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_MaxCellVoltage << #1 ; [CPU_] |338| 
        MOV       *-SP[14],AL           ; [CPU_] |338| 
	.dwpsn	file "../mms.c",line 339,column 5,is_stmt
        MOVW      DP,#_ODV_Module1_MinCellVoltage ; [CPU_U] 
        MOV       ACC,@_ODV_Module1_MinCellVoltage << #1 ; [CPU_] |339| 
        MOV       *-SP[15],AL           ; [CPU_] |339| 
	.dwpsn	file "../mms.c",line 340,column 5,is_stmt
        MOVIZ     R1H,#16896            ; [CPU_] |340| 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        I16TOF32  R0H,@_ODV_Module1_Current ; [CPU_] |340| 
$C$DW$257	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$257, DW_AT_low_pc(0x00)
	.dwattr $C$DW$257, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$257, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |340| 
        ; call occurs [#FS$$DIV] ; [] |340| 
        MOV32     *-SP[18],R0H          ; [CPU_] |340| 
	.dwpsn	file "../mms.c",line 341,column 5,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |341| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |341| 
        BF        $C$L38,TC             ; [CPU_] |341| 
        ; branchcc occurs ; [] |341| 
	.dwpsn	file "../mms.c",line 341,column 30,is_stmt
        ZERO      R0H                   ; [CPU_] |341| 
        MOV32     *-SP[18],R0H          ; [CPU_] |341| 
$C$L38:    
	.dwpsn	file "../mms.c",line 342,column 5,is_stmt
$C$DW$258	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$258, DW_AT_low_pc(0x00)
	.dwattr $C$DW$258, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$258, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |342| 
        ; call occurs [#_CNV_Round] ; [] |342| 
        MOV       *-SP[13],AL           ; [CPU_] |342| 
	.dwpsn	file "../mms.c",line 343,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AH,@_ODV_MachineMode  ; [CPU_] |343| 
        MOVW      DP,#_ODV_Module1_Status ; [CPU_U] 
        ANDB      AH,#0x0f              ; [CPU_] |343| 
        MOV       AL,@_ODV_Module1_Status ; [CPU_] |343| 
        ANDB      AL,#0xc0              ; [CPU_] |343| 
        ADD       AL,AH                 ; [CPU_] |343| 
        MOV       @_ODV_Module1_Status,AL ; [CPU_] |343| 
	.dwpsn	file "../mms.c",line 344,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |344| 
        B         $C$L88,LEQ            ; [CPU_] |344| 
        ; branchcc occurs ; [] |344| 
        CMPB      AL,#4                 ; [CPU_] |344| 
        B         $C$L88,GT             ; [CPU_] |344| 
        ; branchcc occurs ; [] |344| 
	.dwpsn	file "../mms.c",line 345,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Overcurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Overcurrent ; [CPU_] |345| 
        CMP       AL,*-SP[13]           ; [CPU_] |345| 
        B         $C$L39,LT             ; [CPU_] |345| 
        ; branchcc occurs ; [] |345| 
        MOVW      DP,#_ODP_SafetyLimits_UnderCurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderCurrent ; [CPU_] |345| 
        CMP       AL,*-SP[13]           ; [CPU_] |345| 
        B         $C$L40,LEQ            ; [CPU_] |345| 
        ; branchcc occurs ; [] |345| 
$C$L39:    
	.dwpsn	file "../mms.c",line 348,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |348| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        ADDL      @_GV_current_counter,ACC ; [CPU_] |348| 
	.dwpsn	file "../mms.c",line 349,column 8,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |349| 
        AND       AL,*+XAR4[0],#0x0020  ; [CPU_] |349| 
        LSR       AL,5                  ; [CPU_] |349| 
        CMPB      AL,#1                 ; [CPU_] |349| 
        BF        $C$L40,NEQ            ; [CPU_] |349| 
        ; branchcc occurs ; [] |349| 
        MOV       T,#1000               ; [CPU_] |349| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |349| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        CMPL      ACC,@_GV_current_counter ; [CPU_] |349| 
        B         $C$L40,HIS            ; [CPU_] |349| 
        ; branchcc occurs ; [] |349| 
	.dwpsn	file "../mms.c",line 350,column 9,is_stmt
$C$DW$259	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$259, DW_AT_low_pc(0x00)
	.dwattr $C$DW$259, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$259, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |350| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |350| 
$C$L40:    
	.dwpsn	file "../mms.c",line 354,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umin ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umin ; [CPU_] |354| 
        CMP       AL,*-SP[15]           ; [CPU_] |354| 
        B         $C$L41,LEQ            ; [CPU_] |354| 
        ; branchcc occurs ; [] |354| 
	.dwpsn	file "../mms.c",line 356,column 8,is_stmt
        MOVB      ACC,#32               ; [CPU_] |356| 
$C$DW$260	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$260, DW_AT_low_pc(0x00)
	.dwattr $C$DW$260, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$260, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |356| 
        ; call occurs [#_ERR_HandleWarning] ; [] |356| 
$C$L41:    
	.dwpsn	file "../mms.c",line 359,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |359| 
        CMP       AL,*-SP[15]           ; [CPU_] |359| 
        B         $C$L42,GEQ            ; [CPU_] |359| 
        ; branchcc occurs ; [] |359| 
	.dwpsn	file "../mms.c",line 361,column 8,is_stmt
        MOVB      ACC,#8                ; [CPU_] |361| 
$C$DW$261	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$261, DW_AT_low_pc(0x00)
	.dwattr $C$DW$261, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$261, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |361| 
        ; call occurs [#_ERR_HandleWarning] ; [] |361| 
$C$L42:    
	.dwpsn	file "../mms.c",line 364,column 7,is_stmt
        MOV32     R0H,*-SP[18]          ; [CPU_] |364| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |364| 
        NOP       ; [CPU_] 
        CMPF32    R0H,#0                ; [CPU_] |364| 
        MOVST0    ZF, NF                ; [CPU_] |364| 
        B         $C$L43,LT             ; [CPU_] |364| 
        ; branchcc occurs ; [] |364| 
        MOV32     R0H,*-SP[18]          ; [CPU_] |364| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |364| 
        B         $C$L44,UNC            ; [CPU_] |364| 
        ; branch occurs ; [] |364| 
$C$L43:    
        MOV32     R0H,*-SP[18]          ; [CPU_] |364| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |364| 
        NOP       ; [CPU_] 
        NEGF32    R0H,R0H               ; [CPU_] |364| 
$C$L44:    
        MOVW      DP,#_ODP_SleepCurrent ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_SleepCurrent ; [CPU_] |364| 
        NOP       ; [CPU_] 
        CMPF32    R0H,R1H               ; [CPU_] |364| 
        MOVST0    ZF, NF                ; [CPU_] |364| 
        B         $C$L45,GEQ            ; [CPU_] |364| 
        ; branchcc occurs ; [] |364| 
	.dwpsn	file "../mms.c",line 365,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |365| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |365| 
        MOVL      *-SP[10],ACC          ; [CPU_] |365| 
	.dwpsn	file "../mms.c",line 366,column 9,is_stmt
        MOV       T,#60000              ; [CPU_] |366| 
        MOVW      DP,#_ODP_SafetyLimits_SleepTimeout ; [CPU_U] 
        MPYU      ACC,T,@_ODP_SafetyLimits_SleepTimeout ; [CPU_] |366| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |366| 
        B         $C$L46,HIS            ; [CPU_] |366| 
        ; branchcc occurs ; [] |366| 
	.dwpsn	file "../mms.c",line 367,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |367| 
        MOVL      *-SP[10],ACC          ; [CPU_] |367| 
	.dwpsn	file "../mms.c",line 368,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#64,UNC ; [CPU_] |368| 
	.dwpsn	file "../mms.c",line 369,column 11,is_stmt
        MOVB      ACC,#16               ; [CPU_] |369| 
$C$DW$262	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$262, DW_AT_low_pc(0x00)
	.dwattr $C$DW$262, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$262, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |369| 
        ; call occurs [#_ERR_HandleWarning] ; [] |369| 
	.dwpsn	file "../mms.c",line 371,column 7,is_stmt
        B         $C$L46,UNC            ; [CPU_] |371| 
        ; branch occurs ; [] |371| 
$C$L45:    
	.dwpsn	file "../mms.c",line 372,column 12,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |372| 
        BF        $C$L46,EQ             ; [CPU_] |372| 
        ; branchcc occurs ; [] |372| 
	.dwpsn	file "../mms.c",line 372,column 35,is_stmt
        MOVB      ACC,#1                ; [CPU_] |372| 
        SUBL      *-SP[10],ACC          ; [CPU_] |372| 
$C$L46:    
	.dwpsn	file "../mms.c",line 373,column 7,is_stmt
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_DischargeAllowed ; [CPU_] |373| 
        NEG       AL                    ; [CPU_] |373| 
        CMP       AL,*-SP[13]           ; [CPU_] |373| 
        B         $C$L47,GT             ; [CPU_] |373| 
        ; branchcc occurs ; [] |373| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_ChargeAllowed ; [CPU_] |373| 
        CMP       AL,*-SP[13]           ; [CPU_] |373| 
        B         $C$L48,GEQ            ; [CPU_] |373| 
        ; branchcc occurs ; [] |373| 
$C$L47:    
	.dwpsn	file "../mms.c",line 374,column 9,is_stmt
        MOVB      ACC,#64               ; [CPU_] |374| 
$C$DW$263	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$263, DW_AT_low_pc(0x00)
	.dwattr $C$DW$263, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$263, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |374| 
        ; call occurs [#_ERR_HandleWarning] ; [] |374| 
	.dwpsn	file "../mms.c",line 375,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |375| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |375| 
        MOVL      *-SP[8],ACC           ; [CPU_] |375| 
	.dwpsn	file "../mms.c",line 376,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |376| 
        AND       AL,*+XAR4[0],#0x0020  ; [CPU_] |376| 
        LSR       AL,5                  ; [CPU_] |376| 
        CMPB      AL,#1                 ; [CPU_] |376| 
        BF        $C$L49,NEQ            ; [CPU_] |376| 
        ; branchcc occurs ; [] |376| 
        MOV       T,#1000               ; [CPU_] |376| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |376| 
        CMPL      ACC,*-SP[8]           ; [CPU_] |376| 
        B         $C$L49,HIS            ; [CPU_] |376| 
        ; branchcc occurs ; [] |376| 
	.dwpsn	file "../mms.c",line 379,column 7,is_stmt
        B         $C$L49,UNC            ; [CPU_] |379| 
        ; branch occurs ; [] |379| 
$C$L48:    
	.dwpsn	file "../mms.c",line 380,column 12,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |380| 
        BF        $C$L49,EQ             ; [CPU_] |380| 
        ; branchcc occurs ; [] |380| 
	.dwpsn	file "../mms.c",line 380,column 37,is_stmt
        MOVB      ACC,#1                ; [CPU_] |380| 
        SUBL      *-SP[8],ACC           ; [CPU_] |380| 
$C$L49:    
	.dwpsn	file "../mms.c",line 381,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |381| 
        BF        $C$L88,NEQ            ; [CPU_] |381| 
        ; branchcc occurs ; [] |381| 
	.dwpsn	file "../mms.c",line 382,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |382| 
        AND       AL,*+XAR4[0],#0x0800  ; [CPU_] |382| 
        LSR       AL,11                 ; [CPU_] |382| 
        CMPB      AL,#1                 ; [CPU_] |382| 
        BF        $C$L50,NEQ            ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
	.dwpsn	file "../mms.c",line 383,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |383| 
$C$L50:    
	.dwpsn	file "../mms.c",line 385,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |385| 
        AND       AL,*+XAR4[0],#0x0080  ; [CPU_] |385| 
        LSR       AL,7                  ; [CPU_] |385| 
        CMPB      AL,#1                 ; [CPU_] |385| 
        BF        $C$L51,NEQ            ; [CPU_] |385| 
        ; branchcc occurs ; [] |385| 
	.dwpsn	file "../mms.c",line 386,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |386| 
$C$L51:    
	.dwpsn	file "../mms.c",line 388,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |388| 
	.dwpsn	file "../mms.c",line 391,column 5,is_stmt
        B         $C$L88,UNC            ; [CPU_] |391| 
        ; branch occurs ; [] |391| 
$C$L52:    
	.dwpsn	file "../mms.c",line 394,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |394| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |394| 
        LSR       AL,1                  ; [CPU_] |394| 
        CMPB      AL,#1                 ; [CPU_] |394| 
        BF        $C$L53,NEQ            ; [CPU_] |394| 
        ; branchcc occurs ; [] |394| 
	.dwpsn	file "../mms.c",line 395,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |395| 
	.dwpsn	file "../mms.c",line 396,column 9,is_stmt
        B         $C$L54,UNC            ; [CPU_] |396| 
        ; branch occurs ; [] |396| 
$C$L53:    
	.dwpsn	file "../mms.c",line 397,column 14,is_stmt
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |397| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |397| 
        BF        $C$L54,TC             ; [CPU_] |397| 
        ; branchcc occurs ; [] |397| 
	.dwpsn	file "../mms.c",line 398,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |398| 
$C$L54:    
	.dwpsn	file "../mms.c",line 400,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |400| 
        CMP       AL,*-SP[15]           ; [CPU_] |400| 
        B         $C$L55,LEQ            ; [CPU_] |400| 
        ; branchcc occurs ; [] |400| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |400| 
        BF        $C$L55,NEQ            ; [CPU_] |400| 
        ; branchcc occurs ; [] |400| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |400| 
        CMPB      AL,#128               ; [CPU_] |400| 
        BF        $C$L55,EQ             ; [CPU_] |400| 
        ; branchcc occurs ; [] |400| 
	.dwpsn	file "../mms.c",line 401,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |401| 
$C$L55:    
	.dwpsn	file "../mms.c",line 403,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |403| 
        BF        $C$L90,EQ             ; [CPU_] |403| 
        ; branchcc occurs ; [] |403| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |403| 
        BF        $C$L90,NEQ            ; [CPU_] |403| 
        ; branchcc occurs ; [] |403| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |403| 
        CMPB      AL,#128               ; [CPU_] |403| 
        BF        $C$L90,EQ             ; [CPU_] |403| 
        ; branchcc occurs ; [] |403| 
	.dwpsn	file "../mms.c",line 404,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |404| 
        AND       AL,*+XAR4[0],#0x0080  ; [CPU_] |404| 
        LSR       AL,7                  ; [CPU_] |404| 
        CMPB      AL,#1                 ; [CPU_] |404| 
        BF        $C$L56,NEQ            ; [CPU_] |404| 
        ; branchcc occurs ; [] |404| 
	.dwpsn	file "../mms.c",line 405,column 13,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#1,UNC ; [CPU_] |405| 
$C$L56:    
	.dwpsn	file "../mms.c",line 407,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |407| 
        AND       AL,*+XAR4[0],#0x0800  ; [CPU_] |407| 
        LSR       AL,11                 ; [CPU_] |407| 
        CMPB      AL,#1                 ; [CPU_] |407| 
        BF        $C$L57,NEQ            ; [CPU_] |407| 
        ; branchcc occurs ; [] |407| 
	.dwpsn	file "../mms.c",line 408,column 13,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#1,UNC ; [CPU_] |408| 
$C$L57:    
	.dwpsn	file "../mms.c",line 410,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |410| 
        MOVL      *-SP[4],ACC           ; [CPU_] |410| 
	.dwpsn	file "../mms.c",line 411,column 11,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |411| 
	.dwpsn	file "../mms.c",line 412,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |412| 
	.dwpsn	file "../mms.c",line 414,column 7,is_stmt
        B         $C$L90,UNC            ; [CPU_] |414| 
        ; branch occurs ; [] |414| 
$C$L58:    
	.dwpsn	file "../mms.c",line 417,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |417| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |417| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |417| 
        CMPL      ACC,XAR6              ; [CPU_] |417| 
        B         $C$L59,LO             ; [CPU_] |417| 
        ; branchcc occurs ; [] |417| 
	.dwpsn	file "../mms.c",line 417,column 43,is_stmt
        MOVB      *-SP[20],#1,UNC       ; [CPU_] |417| 
$C$L59:    
	.dwpsn	file "../mms.c",line 418,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |418| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        CMP       AL,@_ODV_Current_ChargeAllowed ; [CPU_] |418| 
        B         $C$L60,HIS            ; [CPU_] |418| 
        ; branchcc occurs ; [] |418| 
	.dwpsn	file "../mms.c",line 418,column 63,is_stmt
        MOV       @_ODV_Current_ChargeAllowed,AL ; [CPU_] |418| 
$C$L60:    
	.dwpsn	file "../mms.c",line 420,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |420| 
        CMP       AL,*-SP[14]           ; [CPU_] |420| 
        B         $C$L61,LEQ            ; [CPU_] |420| 
        ; branchcc occurs ; [] |420| 
        MOV       AL,*-SP[20]           ; [CPU_] |420| 
        BF        $C$L61,EQ             ; [CPU_] |420| 
        ; branchcc occurs ; [] |420| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |420| 
        BF        $C$L61,NEQ            ; [CPU_] |420| 
        ; branchcc occurs ; [] |420| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |420| 
        CMPB      AL,#128               ; [CPU_] |420| 
        BF        $C$L61,EQ             ; [CPU_] |420| 
        ; branchcc occurs ; [] |420| 
	.dwpsn	file "../mms.c",line 421,column 11,is_stmt
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |421| 
	.dwpsn	file "../mms.c",line 422,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |422| 
        MOVL      *-SP[4],ACC           ; [CPU_] |422| 
	.dwpsn	file "../mms.c",line 423,column 11,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |423| 
	.dwpsn	file "../mms.c",line 424,column 11,is_stmt
$C$DW$264	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$264, DW_AT_low_pc(0x00)
	.dwattr $C$DW$264, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$264, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |424| 
        ; call occurs [#_ERR_ClearWarning] ; [] |424| 
$C$L61:    
	.dwpsn	file "../mms.c",line 426,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |426| 
        CMP       AL,*-SP[15]           ; [CPU_] |426| 
        B         $C$L62,LEQ            ; [CPU_] |426| 
        ; branchcc occurs ; [] |426| 
	.dwpsn	file "../mms.c",line 427,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |427| 
	.dwpsn	file "../mms.c",line 428,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |428| 
        MOVL      *-SP[4],ACC           ; [CPU_] |428| 
	.dwpsn	file "../mms.c",line 429,column 11,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |429| 
$C$L62:    
	.dwpsn	file "../mms.c",line 431,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |431| 
        CMP       AL,*-SP[15]           ; [CPU_] |431| 
        B         $C$L90,GEQ            ; [CPU_] |431| 
        ; branchcc occurs ; [] |431| 
        MOV       AL,*-SP[20]           ; [CPU_] |431| 
        BF        $C$L90,EQ             ; [CPU_] |431| 
        ; branchcc occurs ; [] |431| 
        MOVW      DP,#_ODV_Module1_Current ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Current ; [CPU_] |431| 
        CMPB      AL,#6                 ; [CPU_] |431| 
        B         $C$L90,LEQ            ; [CPU_] |431| 
        ; branchcc occurs ; [] |431| 
	.dwpsn	file "../mms.c",line 432,column 11,is_stmt
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      ACC,@_PAR_Capacity_Total ; [CPU_] |432| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOVL      @_PAR_Capacity_Left,ACC ; [CPU_] |432| 
	.dwpsn	file "../mms.c",line 434,column 7,is_stmt
        B         $C$L90,UNC            ; [CPU_] |434| 
        ; branch occurs ; [] |434| 
$C$L63:    
	.dwpsn	file "../mms.c",line 437,column 6,is_stmt
        MOVW      DP,#_ODV_Module1_SOC  ; [CPU_U] 
        MOV       @_ODV_Module1_SOC,#0  ; [CPU_] |437| 
	.dwpsn	file "../mms.c",line 438,column 6,is_stmt
        MOVB      ACC,#32               ; [CPU_] |438| 
$C$DW$265	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$265, DW_AT_low_pc(0x00)
	.dwattr $C$DW$265, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$265, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |438| 
        ; call occurs [#_ERR_HandleWarning] ; [] |438| 
	.dwpsn	file "../mms.c",line 439,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |439| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |439| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |439| 
        CMPL      ACC,XAR6              ; [CPU_] |439| 
        B         $C$L64,LO             ; [CPU_] |439| 
        ; branchcc occurs ; [] |439| 
	.dwpsn	file "../mms.c",line 439,column 43,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |439| 
$C$L64:    
	.dwpsn	file "../mms.c",line 440,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |440| 
        CMP       AL,*-SP[15]           ; [CPU_] |440| 
        B         $C$L67,LEQ            ; [CPU_] |440| 
        ; branchcc occurs ; [] |440| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |440| 
        BF        $C$L67,NEQ            ; [CPU_] |440| 
        ; branchcc occurs ; [] |440| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |440| 
        CMPB      AL,#128               ; [CPU_] |440| 
        BF        $C$L67,EQ             ; [CPU_] |440| 
        ; branchcc occurs ; [] |440| 
	.dwpsn	file "../mms.c",line 442,column 10,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |442| 
        CMPB      AL,#2                 ; [CPU_] |442| 
        B         $C$L67,GT             ; [CPU_] |442| 
        ; branchcc occurs ; [] |442| 
	.dwpsn	file "../mms.c",line 445,column 10,is_stmt
	.dwpsn	file "../mms.c",line 446,column 15,is_stmt
        CMP       AL,#-1                ; [CPU_] |446| 
        B         $C$L65,GEQ            ; [CPU_] |446| 
        ; branchcc occurs ; [] |446| 
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Current_Allow ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Low_Voltage_Current_Allow ; [CPU_] |446| 
        CMP       AL,*-SP[13]           ; [CPU_] |446| 
        B         $C$L65,GEQ            ; [CPU_] |446| 
        ; branchcc occurs ; [] |446| 
	.dwpsn	file "../mms.c",line 448,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |448| 
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Current_Delay ; [CPU_U] 
        MPY       ACC,T,@_ODP_SafetyLimits_Low_Voltage_Current_Delay ; [CPU_] |448| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVU      ACC,AL                ; [CPU_] |448| 
        CMPL      ACC,@_ODV_SysTick_ms  ; [CPU_] |448| 
        B         $C$L67,HIS            ; [CPU_] |448| 
        ; branchcc occurs ; [] |448| 
	.dwpsn	file "../mms.c",line 448,column 83,is_stmt
$C$DW$266	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$266, DW_AT_low_pc(0x00)
	.dwattr $C$DW$266, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$266, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |448| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |448| 
	.dwpsn	file "../mms.c",line 449,column 10,is_stmt
        B         $C$L67,UNC            ; [CPU_] |449| 
        ; branch occurs ; [] |449| 
$C$L65:    
	.dwpsn	file "../mms.c",line 450,column 15,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Current_Allow ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Low_Voltage_Current_Allow ; [CPU_] |450| 
        CMP       AL,*-SP[13]           ; [CPU_] |450| 
        B         $C$L66,LEQ            ; [CPU_] |450| 
        ; branchcc occurs ; [] |450| 
	.dwpsn	file "../mms.c",line 452,column 11,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Current_Delay ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Low_Voltage_Current_Delay ; [CPU_] |452| 
        MOV       T,#1000               ; [CPU_] |452| 
        LSR       AL,1                  ; [CPU_] |452| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MPY       ACC,T,AL              ; [CPU_] |452| 
        MOVU      ACC,AL                ; [CPU_] |452| 
        CMPL      ACC,@_ODV_SysTick_ms  ; [CPU_] |452| 
        B         $C$L67,HIS            ; [CPU_] |452| 
        ; branchcc occurs ; [] |452| 
	.dwpsn	file "../mms.c",line 452,column 87,is_stmt
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$267, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |452| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |452| 
	.dwpsn	file "../mms.c",line 453,column 10,is_stmt
        B         $C$L67,UNC            ; [CPU_] |453| 
        ; branch occurs ; [] |453| 
$C$L66:    
	.dwpsn	file "../mms.c",line 456,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |456| 
        MOVW      DP,#_ODP_SafetyLimits_Low_Voltage_Charge_Delay ; [CPU_U] 
        MPY       ACC,T,@_ODP_SafetyLimits_Low_Voltage_Charge_Delay ; [CPU_] |456| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVU      ACC,AL                ; [CPU_] |456| 
        CMPL      ACC,@_ODV_SysTick_ms  ; [CPU_] |456| 
        B         $C$L67,HIS            ; [CPU_] |456| 
        ; branchcc occurs ; [] |456| 
	.dwpsn	file "../mms.c",line 456,column 82,is_stmt
$C$DW$268	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$268, DW_AT_low_pc(0x00)
	.dwattr $C$DW$268, DW_AT_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$268, DW_AT_TI_call
        LCR       #_ERR_ErrorUnderVoltage ; [CPU_] |456| 
        ; call occurs [#_ERR_ErrorUnderVoltage] ; [] |456| 
$C$L67:    
	.dwpsn	file "../mms.c",line 460,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |460| 
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        CMP       AL,@_ODV_Current_DischargeAllowed ; [CPU_] |460| 
        B         $C$L68,HIS            ; [CPU_] |460| 
        ; branchcc occurs ; [] |460| 
	.dwpsn	file "../mms.c",line 460,column 65,is_stmt
        MOV       @_ODV_Current_DischargeAllowed,AL ; [CPU_] |460| 
$C$L68:    
	.dwpsn	file "../mms.c",line 462,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_] |462| 
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        CMP       AL,@_ODV_Current_DischargeAllowed ; [CPU_] |462| 
        B         $C$L69,HIS            ; [CPU_] |462| 
        ; branchcc occurs ; [] |462| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |462| 
        CMP       AL,*-SP[15]           ; [CPU_] |462| 
        B         $C$L69,LEQ            ; [CPU_] |462| 
        ; branchcc occurs ; [] |462| 
	.dwpsn	file "../mms.c",line 463,column 11,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_] |463| 
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       @_ODV_Current_DischargeAllowed,AL ; [CPU_] |463| 
$C$L69:    
	.dwpsn	file "../mms.c",line 465,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |465| 
        CMP       AL,*-SP[15]           ; [CPU_] |465| 
        B         $C$L70,GEQ            ; [CPU_] |465| 
        ; branchcc occurs ; [] |465| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |465| 
        BF        $C$L70,NEQ            ; [CPU_] |465| 
        ; branchcc occurs ; [] |465| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |465| 
        CMPB      AL,#128               ; [CPU_] |465| 
        BF        $C$L71,NEQ            ; [CPU_] |465| 
        ; branchcc occurs ; [] |465| 
$C$L70:    
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |465| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |465| 
        BF        $C$L90,TC             ; [CPU_] |465| 
        ; branchcc occurs ; [] |465| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |465| 
        CMP       AL,*-SP[15]           ; [CPU_] |465| 
        B         $C$L90,GEQ            ; [CPU_] |465| 
        ; branchcc occurs ; [] |465| 
$C$L71:    
	.dwpsn	file "../mms.c",line 466,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |466| 
        MOVL      *-SP[4],ACC           ; [CPU_] |466| 
	.dwpsn	file "../mms.c",line 467,column 11,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |467| 
	.dwpsn	file "../mms.c",line 468,column 11,is_stmt
$C$DW$269	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$269, DW_AT_low_pc(0x00)
	.dwattr $C$DW$269, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$269, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |468| 
        ; call occurs [#_ERR_ClearWarning] ; [] |468| 
	.dwpsn	file "../mms.c",line 469,column 11,is_stmt
        MOVB      *-SP[19],#81,UNC      ; [CPU_] |469| 
	.dwpsn	file "../mms.c",line 470,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |470| 
	.dwpsn	file "../mms.c",line 471,column 11,is_stmt
        MOVB      AL,#0                 ; [CPU_] |471| 
        MOVZ      AR5,SP                ; [CPU_U] |471| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |471| 
        SUBB      XAR5,#19              ; [CPU_U] |471| 
$C$DW$270	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$270, DW_AT_low_pc(0x00)
	.dwattr $C$DW$270, DW_AT_name("_MBX_post")
	.dwattr $C$DW$270, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |471| 
        ; call occurs [#_MBX_post] ; [] |471| 
	.dwpsn	file "../mms.c",line 473,column 7,is_stmt
        B         $C$L90,UNC            ; [CPU_] |473| 
        ; branch occurs ; [] |473| 
$C$L72:    
	.dwpsn	file "../mms.c",line 477,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |477| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |477| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |477| 
        CMPL      ACC,XAR6              ; [CPU_] |477| 
        B         $C$L74,LO             ; [CPU_] |477| 
        ; branchcc occurs ; [] |477| 
        MOV       AL,*-SP[20]           ; [CPU_] |477| 
        BF        $C$L74,NEQ            ; [CPU_] |477| 
        ; branchcc occurs ; [] |477| 
	.dwpsn	file "../mms.c",line 478,column 11,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |478| 
        CMP       AL,*-SP[15]           ; [CPU_] |478| 
        B         $C$L73,LEQ            ; [CPU_] |478| 
        ; branchcc occurs ; [] |478| 
	.dwpsn	file "../mms.c",line 479,column 13,is_stmt
        MOVB      ACC,#32               ; [CPU_] |479| 
$C$DW$271	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$271, DW_AT_low_pc(0x00)
	.dwattr $C$DW$271, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$271, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |479| 
        ; call occurs [#_ERR_HandleWarning] ; [] |479| 
	.dwpsn	file "../mms.c",line 480,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |480| 
	.dwpsn	file "../mms.c",line 481,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |481| 
        MOVL      *-SP[4],ACC           ; [CPU_] |481| 
	.dwpsn	file "../mms.c",line 482,column 13,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |482| 
	.dwpsn	file "../mms.c",line 483,column 11,is_stmt
        B         $C$L74,UNC            ; [CPU_] |483| 
        ; branch occurs ; [] |483| 
$C$L73:    
	.dwpsn	file "../mms.c",line 485,column 13,is_stmt
        MOVB      *-SP[19],#81,UNC      ; [CPU_] |485| 
	.dwpsn	file "../mms.c",line 486,column 13,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |486| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |486| 
        MOVB      AL,#0                 ; [CPU_] |486| 
        SUBB      XAR5,#19              ; [CPU_U] |486| 
$C$DW$272	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$272, DW_AT_low_pc(0x00)
	.dwattr $C$DW$272, DW_AT_name("_MBX_post")
	.dwattr $C$DW$272, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |486| 
        ; call occurs [#_MBX_post] ; [] |486| 
	.dwpsn	file "../mms.c",line 487,column 13,is_stmt
        MOVB      *-SP[20],#1,UNC       ; [CPU_] |487| 
$C$L74:    
	.dwpsn	file "../mms.c",line 490,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |490| 
        CMP       AL,*-SP[15]           ; [CPU_] |490| 
        B         $C$L75,LEQ            ; [CPU_] |490| 
        ; branchcc occurs ; [] |490| 
	.dwpsn	file "../mms.c",line 491,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |491| 
	.dwpsn	file "../mms.c",line 492,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |492| 
        MOVL      *-SP[4],ACC           ; [CPU_] |492| 
	.dwpsn	file "../mms.c",line 493,column 11,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |493| 
$C$L75:    
	.dwpsn	file "../mms.c",line 495,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |495| 
        CMPB      AL,#2                 ; [CPU_] |495| 
        BF        $C$L76,NEQ            ; [CPU_] |495| 
        ; branchcc occurs ; [] |495| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |495| 
        BF        $C$L77,EQ             ; [CPU_] |495| 
        ; branchcc occurs ; [] |495| 
$C$L76:    
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |495| 
        CMP       AL,*-SP[15]           ; [CPU_] |495| 
        B         $C$L78,LEQ            ; [CPU_] |495| 
        ; branchcc occurs ; [] |495| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |495| 
        BF        $C$L78,NEQ            ; [CPU_] |495| 
        ; branchcc occurs ; [] |495| 
$C$L77:    
	.dwpsn	file "../mms.c",line 496,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |496| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |496| 
        MOVL      *-SP[12],ACC          ; [CPU_] |496| 
	.dwpsn	file "../mms.c",line 497,column 11,is_stmt
        MOVB      ACC,#32               ; [CPU_] |497| 
$C$DW$273	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$273, DW_AT_low_pc(0x00)
	.dwattr $C$DW$273, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$273, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |497| 
        ; call occurs [#_ERR_HandleWarning] ; [] |497| 
	.dwpsn	file "../mms.c",line 498,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |498| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |498| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |498| 
        B         $C$L90,HIS            ; [CPU_] |498| 
        ; branchcc occurs ; [] |498| 
	.dwpsn	file "../mms.c",line 499,column 13,is_stmt
        MOVB      ACC,#32               ; [CPU_] |499| 
$C$DW$274	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$274, DW_AT_low_pc(0x00)
	.dwattr $C$DW$274, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$274, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |499| 
        ; call occurs [#_ERR_HandleWarning] ; [] |499| 
	.dwpsn	file "../mms.c",line 501,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |501| 
	.dwpsn	file "../mms.c",line 502,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |502| 
        MOVL      *-SP[12],ACC          ; [CPU_] |502| 
	.dwpsn	file "../mms.c",line 503,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |503| 
        MOVL      *-SP[4],ACC           ; [CPU_] |503| 
	.dwpsn	file "../mms.c",line 504,column 13,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |504| 
	.dwpsn	file "../mms.c",line 506,column 9,is_stmt
        B         $C$L90,UNC            ; [CPU_] |506| 
        ; branch occurs ; [] |506| 
$C$L78:    
	.dwpsn	file "../mms.c",line 507,column 14,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |507| 
        CMPB      AL,#4                 ; [CPU_] |507| 
        BF        $C$L79,NEQ            ; [CPU_] |507| 
        ; branchcc occurs ; [] |507| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |507| 
        BF        $C$L80,EQ             ; [CPU_] |507| 
        ; branchcc occurs ; [] |507| 
$C$L79:    
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |507| 
        CMP       AL,*-SP[14]           ; [CPU_] |507| 
        B         $C$L81,GEQ            ; [CPU_] |507| 
        ; branchcc occurs ; [] |507| 
        MOV       AL,*-SP[20]           ; [CPU_] |507| 
        BF        $C$L81,EQ             ; [CPU_] |507| 
        ; branchcc occurs ; [] |507| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |507| 
        BF        $C$L81,NEQ            ; [CPU_] |507| 
        ; branchcc occurs ; [] |507| 
$C$L80:    
	.dwpsn	file "../mms.c",line 508,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |508| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |508| 
        MOVL      *-SP[12],ACC          ; [CPU_] |508| 
	.dwpsn	file "../mms.c",line 509,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |509| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |509| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |509| 
        B         $C$L90,HIS            ; [CPU_] |509| 
        ; branchcc occurs ; [] |509| 
	.dwpsn	file "../mms.c",line 510,column 14,is_stmt
        MOVB      ACC,#8                ; [CPU_] |510| 
$C$DW$275	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$275, DW_AT_low_pc(0x00)
	.dwattr $C$DW$275, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$275, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |510| 
        ; call occurs [#_ERR_HandleWarning] ; [] |510| 
	.dwpsn	file "../mms.c",line 512,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#4,UNC ; [CPU_] |512| 
	.dwpsn	file "../mms.c",line 513,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |513| 
        MOVL      *-SP[12],ACC          ; [CPU_] |513| 
	.dwpsn	file "../mms.c",line 514,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |514| 
        MOVL      *-SP[4],ACC           ; [CPU_] |514| 
	.dwpsn	file "../mms.c",line 515,column 13,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |515| 
	.dwpsn	file "../mms.c",line 517,column 9,is_stmt
        B         $C$L90,UNC            ; [CPU_] |517| 
        ; branch occurs ; [] |517| 
$C$L81:    
	.dwpsn	file "../mms.c",line 518,column 14,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |518| 
        BF        $C$L90,EQ             ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
	.dwpsn	file "../mms.c",line 518,column 34,is_stmt
        MOVB      ACC,#1                ; [CPU_] |518| 
        SUBL      *-SP[12],ACC          ; [CPU_] |518| 
	.dwpsn	file "../mms.c",line 519,column 7,is_stmt
        B         $C$L90,UNC            ; [CPU_] |519| 
        ; branch occurs ; [] |519| 
$C$L82:    
	.dwpsn	file "../mms.c",line 522,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |522| 
        MOVL      *-SP[4],ACC           ; [CPU_] |522| 
	.dwpsn	file "../mms.c",line 523,column 9,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |523| 
	.dwpsn	file "../mms.c",line 524,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#128,UNC ; [CPU_] |524| 
$C$L83:    
	.dwpsn	file "../mms.c",line 528,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |528| 
        MOVW      DP,#_ODP_CommError_Delay ; [CPU_U] 
        MOVZ      AR6,@_ODP_CommError_Delay ; [CPU_] |528| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |528| 
        CMPL      ACC,XAR6              ; [CPU_] |528| 
        B         $C$L84,LO             ; [CPU_] |528| 
        ; branchcc occurs ; [] |528| 
        MOV       AL,*-SP[20]           ; [CPU_] |528| 
        BF        $C$L84,NEQ            ; [CPU_] |528| 
        ; branchcc occurs ; [] |528| 
	.dwpsn	file "../mms.c",line 529,column 11,is_stmt
        MOVB      *-SP[20],#1,UNC       ; [CPU_] |529| 
	.dwpsn	file "../mms.c",line 530,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |530| 
	.dwpsn	file "../mms.c",line 531,column 11,is_stmt
$C$DW$276	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$276, DW_AT_low_pc(0x00)
	.dwattr $C$DW$276, DW_AT_name("_PAR_AddLog")
	.dwattr $C$DW$276, DW_AT_TI_call
        LCR       #_PAR_AddLog          ; [CPU_] |531| 
        ; call occurs [#_PAR_AddLog] ; [] |531| 
$C$L84:    
	.dwpsn	file "../mms.c",line 533,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |533| 
        BF        $C$L85,NEQ            ; [CPU_] |533| 
        ; branchcc occurs ; [] |533| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |533| 
        CMPB      AL,#8                 ; [CPU_] |533| 
        BF        $C$L85,EQ             ; [CPU_] |533| 
        ; branchcc occurs ; [] |533| 
	.dwpsn	file "../mms.c",line 534,column 11,is_stmt
        MOVB      @_ODV_MachineMode,#16,UNC ; [CPU_] |534| 
$C$L85:    
	.dwpsn	file "../mms.c",line 536,column 9,is_stmt
        MOVW      DP,#_HAL_CellOK       ; [CPU_U] 
        MOV       AL,@_HAL_CellOK       ; [CPU_] |536| 
        CMPB      AL,#2                 ; [CPU_] |536| 
        BF        $C$L90,NEQ            ; [CPU_] |536| 
        ; branchcc occurs ; [] |536| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |536| 
        BF        $C$L90,NEQ            ; [CPU_] |536| 
        ; branchcc occurs ; [] |536| 
	.dwpsn	file "../mms.c",line 537,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#16,UNC ; [CPU_] |537| 
	.dwpsn	file "../mms.c",line 539,column 7,is_stmt
        B         $C$L90,UNC            ; [CPU_] |539| 
        ; branch occurs ; [] |539| 
$C$L86:    
	.dwpsn	file "../mms.c",line 541,column 9,is_stmt
$C$DW$277	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$277, DW_AT_low_pc(0x00)
	.dwattr $C$DW$277, DW_AT_name("_ERR_ClearError")
	.dwattr $C$DW$277, DW_AT_TI_call
        LCR       #_ERR_ClearError      ; [CPU_] |541| 
        ; call occurs [#_ERR_ClearError] ; [] |541| 
        CMPB      AL,#0                 ; [CPU_] |541| 
        BF        $C$L90,EQ             ; [CPU_] |541| 
        ; branchcc occurs ; [] |541| 
	.dwpsn	file "../mms.c",line 542,column 11,is_stmt
$C$DW$278	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$278, DW_AT_low_pc(0x00)
	.dwattr $C$DW$278, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$278, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |542| 
        ; call occurs [#_ERR_ClearWarning] ; [] |542| 
	.dwpsn	file "../mms.c",line 543,column 11,is_stmt
        MOVB      *-SP[19],#70,UNC      ; [CPU_] |543| 
	.dwpsn	file "../mms.c",line 544,column 11,is_stmt
        MOVB      AL,#0                 ; [CPU_] |544| 
        MOVZ      AR5,SP                ; [CPU_U] |544| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |544| 
        SUBB      XAR5,#19              ; [CPU_U] |544| 
$C$DW$279	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$279, DW_AT_low_pc(0x00)
	.dwattr $C$DW$279, DW_AT_name("_MBX_post")
	.dwattr $C$DW$279, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |544| 
        ; call occurs [#_MBX_post] ; [] |544| 
        CMPB      AL,#0                 ; [CPU_] |544| 
        BF        $C$L87,NEQ            ; [CPU_] |544| 
        ; branchcc occurs ; [] |544| 
	.dwpsn	file "../mms.c",line 545,column 13,is_stmt
$C$DW$280	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$280, DW_AT_low_pc(0x00)
	.dwattr $C$DW$280, DW_AT_name("_ERR_ErrorComm")
	.dwattr $C$DW$280, DW_AT_TI_call
        LCR       #_ERR_ErrorComm       ; [CPU_] |545| 
        ; call occurs [#_ERR_ErrorComm] ; [] |545| 
	.dwpsn	file "../mms.c",line 546,column 11,is_stmt
        B         $C$L90,UNC            ; [CPU_] |546| 
        ; branch occurs ; [] |546| 
$C$L87:    
	.dwpsn	file "../mms.c",line 548,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |548| 
	.dwpsn	file "../mms.c",line 549,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |549| 
        MOVL      *-SP[4],ACC           ; [CPU_] |549| 
	.dwpsn	file "../mms.c",line 550,column 13,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |550| 
	.dwpsn	file "../mms.c",line 553,column 7,is_stmt
        B         $C$L90,UNC            ; [CPU_] |553| 
        ; branch occurs ; [] |553| 
$C$L88:    
	.dwpsn	file "../mms.c",line 391,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |391| 
        CMPB      AL,#8                 ; [CPU_] |391| 
        B         $C$L89,GT             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        CMPB      AL,#8                 ; [CPU_] |391| 
        BF        $C$L83,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        CMPB      AL,#0                 ; [CPU_] |391| 
        BF        $C$L52,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        CMPB      AL,#1                 ; [CPU_] |391| 
        BF        $C$L72,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        CMPB      AL,#2                 ; [CPU_] |391| 
        BF        $C$L63,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        CMPB      AL,#4                 ; [CPU_] |391| 
        BF        $C$L58,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        B         $C$L83,UNC            ; [CPU_] |391| 
        ; branch occurs ; [] |391| 
$C$L89:    
        CMPB      AL,#16                ; [CPU_] |391| 
        BF        $C$L86,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        CMPB      AL,#64                ; [CPU_] |391| 
        BF        $C$L82,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        CMPB      AL,#128               ; [CPU_] |391| 
        BF        $C$L83,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        B         $C$L83,UNC            ; [CPU_] |391| 
        ; branch occurs ; [] |391| 
$C$L90:    
	.dwpsn	file "../mms.c",line 555,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |555| 
        MOVB      AL,#1                 ; [CPU_] |555| 
$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$281, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |555| 
        ; call occurs [#_SEM_pend] ; [] |555| 
	.dwpsn	file "../mms.c",line 331,column 10,is_stmt
        B         $C$L36,UNC            ; [CPU_] |331| 
        ; branch occurs ; [] |331| 
	.dwattr $C$DW$242, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$242, DW_AT_TI_end_line(0x22d)
	.dwattr $C$DW$242, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$242

	.sect	".text"
	.global	_SecurityCallBack

$C$DW$282	.dwtag  DW_TAG_subprogram, DW_AT_name("SecurityCallBack")
	.dwattr $C$DW$282, DW_AT_low_pc(_SecurityCallBack)
	.dwattr $C$DW$282, DW_AT_high_pc(0x00)
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_SecurityCallBack")
	.dwattr $C$DW$282, DW_AT_external
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$282, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$282, DW_AT_TI_begin_line(0x231)
	.dwattr $C$DW$282, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$282, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 561,column 95,is_stmt,address _SecurityCallBack

	.dwfde $C$DW$CIE, _SecurityCallBack
$C$DW$283	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$283, DW_AT_location[DW_OP_reg12]
$C$DW$284	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$284, DW_AT_location[DW_OP_reg14]
$C$DW$285	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$285, DW_AT_location[DW_OP_reg0]
$C$DW$286	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$286, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SecurityCallBack             FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SecurityCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$287	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$287, DW_AT_location[DW_OP_breg20 -2]
$C$DW$288	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$288, DW_AT_location[DW_OP_breg20 -4]
$C$DW$289	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$289, DW_AT_location[DW_OP_breg20 -5]
$C$DW$290	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$290, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |561| 
        MOV       *-SP[5],AL            ; [CPU_] |561| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |561| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |561| 
	.dwpsn	file "../mms.c",line 562,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |562| 
        BF        $C$L91,NEQ            ; [CPU_] |562| 
        ; branchcc occurs ; [] |562| 
	.dwpsn	file "../mms.c",line 563,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |563| 
        BF        $C$L91,NEQ            ; [CPU_] |563| 
        ; branchcc occurs ; [] |563| 
	.dwpsn	file "../mms.c",line 563,column 28,is_stmt
$C$DW$291	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$291, DW_AT_low_pc(0x00)
	.dwattr $C$DW$291, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$291, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |563| 
        ; call occurs [#_HAL_Random] ; [] |563| 
$C$L91:    
	.dwpsn	file "../mms.c",line 565,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |565| 
        CMPB      AL,#1                 ; [CPU_] |565| 
        BF        $C$L93,NEQ            ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
	.dwpsn	file "../mms.c",line 566,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |566| 
        BF        $C$L92,NEQ            ; [CPU_] |566| 
        ; branchcc occurs ; [] |566| 
	.dwpsn	file "../mms.c",line 566,column 28,is_stmt
$C$DW$292	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$292, DW_AT_low_pc(0x00)
	.dwattr $C$DW$292, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$292, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |566| 
        ; call occurs [#_HAL_Random] ; [] |566| 
        B         $C$L93,UNC            ; [CPU_] |566| 
        ; branch occurs ; [] |566| 
$C$L92:    
	.dwpsn	file "../mms.c",line 567,column 10,is_stmt
$C$DW$293	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$293, DW_AT_low_pc(0x00)
	.dwattr $C$DW$293, DW_AT_name("_HAL_Unlock")
	.dwattr $C$DW$293, DW_AT_TI_call
        LCR       #_HAL_Unlock          ; [CPU_] |567| 
        ; call occurs [#_HAL_Unlock] ; [] |567| 
$C$L93:    
	.dwpsn	file "../mms.c",line 569,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |569| 
	.dwpsn	file "../mms.c",line 570,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$294	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$294, DW_AT_low_pc(0x00)
	.dwattr $C$DW$294, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$282, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$282, DW_AT_TI_end_line(0x23a)
	.dwattr $C$DW$282, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$282

	.sect	".text"
	.global	_LogNBCallback

$C$DW$295	.dwtag  DW_TAG_subprogram, DW_AT_name("LogNBCallback")
	.dwattr $C$DW$295, DW_AT_low_pc(_LogNBCallback)
	.dwattr $C$DW$295, DW_AT_high_pc(0x00)
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_LogNBCallback")
	.dwattr $C$DW$295, DW_AT_external
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$295, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$295, DW_AT_TI_begin_line(0x23d)
	.dwattr $C$DW$295, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$295, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 573,column 92,is_stmt,address _LogNBCallback

	.dwfde $C$DW$CIE, _LogNBCallback
$C$DW$296	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$296, DW_AT_location[DW_OP_reg12]
$C$DW$297	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$297, DW_AT_location[DW_OP_reg14]
$C$DW$298	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$298, DW_AT_location[DW_OP_reg0]
$C$DW$299	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$299, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LogNBCallback                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LogNBCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$300	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$300, DW_AT_location[DW_OP_breg20 -2]
$C$DW$301	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$301, DW_AT_location[DW_OP_breg20 -4]
$C$DW$302	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$302, DW_AT_location[DW_OP_breg20 -5]
$C$DW$303	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$303, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |573| 
        MOV       *-SP[5],AL            ; [CPU_] |573| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |573| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |573| 
	.dwpsn	file "../mms.c",line 575,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |575| 
        CMPB      AL,#1                 ; [CPU_] |575| 
        BF        $C$L94,NEQ            ; [CPU_] |575| 
        ; branchcc occurs ; [] |575| 
	.dwpsn	file "../mms.c",line 576,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_LogNB ; [CPU_] |576| 
$C$DW$304	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$304, DW_AT_low_pc(0x00)
	.dwattr $C$DW$304, DW_AT_name("_PAR_GetLogNB")
	.dwattr $C$DW$304, DW_AT_TI_call
        LCR       #_PAR_GetLogNB        ; [CPU_] |576| 
        ; call occurs [#_PAR_GetLogNB] ; [] |576| 
	.dwpsn	file "../mms.c",line 577,column 3,is_stmt
        B         $C$L95,UNC            ; [CPU_] |577| 
        ; branch occurs ; [] |577| 
$C$L94:    
	.dwpsn	file "../mms.c",line 579,column 5,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       AL,@_TimeLogIndex     ; [CPU_] |579| 
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       @_ODV_Gateway_LogNB,AL ; [CPU_] |579| 
$C$L95:    
	.dwpsn	file "../mms.c",line 581,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |581| 
	.dwpsn	file "../mms.c",line 582,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$305	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$305, DW_AT_low_pc(0x00)
	.dwattr $C$DW$305, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$295, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$295, DW_AT_TI_end_line(0x246)
	.dwattr $C$DW$295, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$295

	.sect	".text"
	.global	_WriteTextCallback

$C$DW$306	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteTextCallback")
	.dwattr $C$DW$306, DW_AT_low_pc(_WriteTextCallback)
	.dwattr $C$DW$306, DW_AT_high_pc(0x00)
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_WriteTextCallback")
	.dwattr $C$DW$306, DW_AT_external
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$306, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$306, DW_AT_TI_begin_line(0x249)
	.dwattr $C$DW$306, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$306, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 585,column 96,is_stmt,address _WriteTextCallback

	.dwfde $C$DW$CIE, _WriteTextCallback
$C$DW$307	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$307, DW_AT_location[DW_OP_reg12]
$C$DW$308	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$308, DW_AT_location[DW_OP_reg14]
$C$DW$309	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$309, DW_AT_location[DW_OP_reg0]
$C$DW$310	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$310, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteTextCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteTextCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$311	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$311, DW_AT_location[DW_OP_breg20 -2]
$C$DW$312	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$312, DW_AT_location[DW_OP_breg20 -4]
$C$DW$313	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$313, DW_AT_location[DW_OP_breg20 -5]
$C$DW$314	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$314, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |585| 
        MOV       *-SP[5],AL            ; [CPU_] |585| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |585| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |585| 
	.dwpsn	file "../mms.c",line 587,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |587| 
        CMPB      AL,#1                 ; [CPU_] |587| 
        BF        $C$L96,NEQ            ; [CPU_] |587| 
        ; branchcc occurs ; [] |587| 
	.dwpsn	file "../mms.c",line 589,column 5,is_stmt
        MOVB      AL,#10                ; [CPU_] |589| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |589| 
        MOVB      AH,#8                 ; [CPU_] |589| 
        MOVB      XAR5,#0               ; [CPU_] |589| 
$C$DW$315	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$315, DW_AT_low_pc(0x00)
	.dwattr $C$DW$315, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$315, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |589| 
        ; call occurs [#_I2C_Command] ; [] |589| 
	.dwpsn	file "../mms.c",line 590,column 3,is_stmt
        B         $C$L97,UNC            ; [CPU_] |590| 
        ; branch occurs ; [] |590| 
$C$L96:    
	.dwpsn	file "../mms.c",line 593,column 5,is_stmt
        MOVB      AL,#9                 ; [CPU_] |593| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |593| 
        MOVB      AH,#8                 ; [CPU_] |593| 
        MOVB      XAR5,#0               ; [CPU_] |593| 
$C$DW$316	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$316, DW_AT_low_pc(0x00)
	.dwattr $C$DW$316, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$316, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |593| 
        ; call occurs [#_I2C_Command] ; [] |593| 
$C$L97:    
	.dwpsn	file "../mms.c",line 595,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |595| 
	.dwpsn	file "../mms.c",line 596,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$317	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$317, DW_AT_low_pc(0x00)
	.dwattr $C$DW$317, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$306, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$306, DW_AT_TI_end_line(0x254)
	.dwattr $C$DW$306, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$306

	.sect	".text"
	.global	_MotCurCallback

$C$DW$318	.dwtag  DW_TAG_subprogram, DW_AT_name("MotCurCallback")
	.dwattr $C$DW$318, DW_AT_low_pc(_MotCurCallback)
	.dwattr $C$DW$318, DW_AT_high_pc(0x00)
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_MotCurCallback")
	.dwattr $C$DW$318, DW_AT_external
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$318, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$318, DW_AT_TI_begin_line(0x256)
	.dwattr $C$DW$318, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$318, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 598,column 93,is_stmt,address _MotCurCallback

	.dwfde $C$DW$CIE, _MotCurCallback
$C$DW$319	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$319, DW_AT_location[DW_OP_reg12]
$C$DW$320	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$320, DW_AT_location[DW_OP_reg14]
$C$DW$321	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$321, DW_AT_location[DW_OP_reg0]
$C$DW$322	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$322, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MotCurCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MotCurCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$323	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$323, DW_AT_location[DW_OP_breg20 -2]
$C$DW$324	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$324, DW_AT_location[DW_OP_breg20 -4]
$C$DW$325	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$325, DW_AT_location[DW_OP_breg20 -5]
$C$DW$326	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$326, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |598| 
        MOV       *-SP[5],AL            ; [CPU_] |598| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |598| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |598| 
	.dwpsn	file "../mms.c",line 599,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |599| 
        CMPB      AL,#1                 ; [CPU_] |599| 
        BF        $C$L98,NEQ            ; [CPU_] |599| 
        ; branchcc occurs ; [] |599| 
	.dwpsn	file "../mms.c",line 600,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |600| 
        CMPB      AL,#2                 ; [CPU_] |600| 
        BF        $C$L98,NEQ            ; [CPU_] |600| 
        ; branchcc occurs ; [] |600| 
	.dwpsn	file "../mms.c",line 601,column 7,is_stmt
        MOVW      DP,#_HAL_NewCurPoint  ; [CPU_U] 
        MOVB      @_HAL_NewCurPoint,#1,UNC ; [CPU_] |601| 
$C$L98:    
	.dwpsn	file "../mms.c",line 604,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |604| 
	.dwpsn	file "../mms.c",line 605,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$327	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$327, DW_AT_low_pc(0x00)
	.dwattr $C$DW$327, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$318, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$318, DW_AT_TI_end_line(0x25d)
	.dwattr $C$DW$318, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$318

	.sect	".text"
	.global	_SciSendCallback

$C$DW$328	.dwtag  DW_TAG_subprogram, DW_AT_name("SciSendCallback")
	.dwattr $C$DW$328, DW_AT_low_pc(_SciSendCallback)
	.dwattr $C$DW$328, DW_AT_high_pc(0x00)
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_SciSendCallback")
	.dwattr $C$DW$328, DW_AT_external
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$328, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$328, DW_AT_TI_begin_line(0x260)
	.dwattr $C$DW$328, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$328, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 608,column 94,is_stmt,address _SciSendCallback

	.dwfde $C$DW$CIE, _SciSendCallback
$C$DW$329	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$329, DW_AT_location[DW_OP_reg12]
$C$DW$330	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$330, DW_AT_location[DW_OP_reg14]
$C$DW$331	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$331, DW_AT_location[DW_OP_reg0]
$C$DW$332	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$332, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SciSendCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SciSendCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$333	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$333, DW_AT_location[DW_OP_breg20 -2]
$C$DW$334	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$334, DW_AT_location[DW_OP_breg20 -4]
$C$DW$335	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$335, DW_AT_location[DW_OP_breg20 -5]
$C$DW$336	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$336, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |608| 
        MOV       *-SP[5],AL            ; [CPU_] |608| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |608| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |608| 
	.dwpsn	file "../mms.c",line 609,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |609| 
        CMPB      AL,#1                 ; [CPU_] |609| 
        BF        $C$L99,NEQ            ; [CPU_] |609| 
        ; branchcc occurs ; [] |609| 
	.dwpsn	file "../mms.c",line 610,column 5,is_stmt
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |610| 
        MOVL      XAR5,#_ODV_SciSend    ; [CPU_U] |610| 
        MOVB      AL,#0                 ; [CPU_] |610| 
$C$DW$337	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$337, DW_AT_low_pc(0x00)
	.dwattr $C$DW$337, DW_AT_name("_MBX_post")
	.dwattr $C$DW$337, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |610| 
        ; call occurs [#_MBX_post] ; [] |610| 
$C$L99:    
	.dwpsn	file "../mms.c",line 613,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |613| 
	.dwpsn	file "../mms.c",line 614,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$338	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$338, DW_AT_low_pc(0x00)
	.dwattr $C$DW$338, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$328, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$328, DW_AT_TI_end_line(0x266)
	.dwattr $C$DW$328, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$328

	.sect	".text"
	.global	_ConfigCallback

$C$DW$339	.dwtag  DW_TAG_subprogram, DW_AT_name("ConfigCallback")
	.dwattr $C$DW$339, DW_AT_low_pc(_ConfigCallback)
	.dwattr $C$DW$339, DW_AT_high_pc(0x00)
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_ConfigCallback")
	.dwattr $C$DW$339, DW_AT_external
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$339, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$339, DW_AT_TI_begin_line(0x268)
	.dwattr $C$DW$339, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$339, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 616,column 93,is_stmt,address _ConfigCallback

	.dwfde $C$DW$CIE, _ConfigCallback
$C$DW$340	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$340, DW_AT_location[DW_OP_reg12]
$C$DW$341	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$341, DW_AT_location[DW_OP_reg14]
$C$DW$342	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$342, DW_AT_location[DW_OP_reg0]
$C$DW$343	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$343, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ConfigCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ConfigCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$344	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$344, DW_AT_location[DW_OP_breg20 -2]
$C$DW$345	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$345, DW_AT_location[DW_OP_breg20 -4]
$C$DW$346	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$346, DW_AT_location[DW_OP_breg20 -5]
$C$DW$347	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$347, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |616| 
        MOV       *-SP[5],AL            ; [CPU_] |616| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |616| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |616| 
	.dwpsn	file "../mms.c",line 617,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |617| 
        CMPB      AL,#1                 ; [CPU_] |617| 
        BF        $C$L101,NEQ           ; [CPU_] |617| 
        ; branchcc occurs ; [] |617| 
	.dwpsn	file "../mms.c",line 618,column 5,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |618| 
        AND       AL,*+XAR4[0],#0x0080  ; [CPU_] |618| 
        LSR       AL,7                  ; [CPU_] |618| 
        CMPB      AL,#1                 ; [CPU_] |618| 
        BF        $C$L100,NEQ           ; [CPU_] |618| 
        ; branchcc occurs ; [] |618| 
	.dwpsn	file "../mms.c",line 618,column 35,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#1,UNC ; [CPU_] |618| 
$C$L100:    
	.dwpsn	file "../mms.c",line 619,column 5,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |619| 
        TBIT      *+XAR4[0],#7          ; [CPU_] |619| 
        BF        $C$L101,TC            ; [CPU_] |619| 
        ; branchcc occurs ; [] |619| 
	.dwpsn	file "../mms.c",line 619,column 35,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |619| 
$C$L101:    
	.dwpsn	file "../mms.c",line 621,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |621| 
	.dwpsn	file "../mms.c",line 622,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$348	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$348, DW_AT_low_pc(0x00)
	.dwattr $C$DW$348, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$339, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$339, DW_AT_TI_end_line(0x26e)
	.dwattr $C$DW$339, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$339

	.sect	".text"
	.global	_BatteryCallBack

$C$DW$349	.dwtag  DW_TAG_subprogram, DW_AT_name("BatteryCallBack")
	.dwattr $C$DW$349, DW_AT_low_pc(_BatteryCallBack)
	.dwattr $C$DW$349, DW_AT_high_pc(0x00)
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_BatteryCallBack")
	.dwattr $C$DW$349, DW_AT_external
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$349, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$349, DW_AT_TI_begin_line(0x270)
	.dwattr $C$DW$349, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$349, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../mms.c",line 624,column 94,is_stmt,address _BatteryCallBack

	.dwfde $C$DW$CIE, _BatteryCallBack
$C$DW$350	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$350, DW_AT_location[DW_OP_reg12]
$C$DW$351	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$351, DW_AT_location[DW_OP_reg14]
$C$DW$352	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$352, DW_AT_location[DW_OP_reg0]
$C$DW$353	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$353, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _BatteryCallBack              FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_BatteryCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$354	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$354, DW_AT_location[DW_OP_breg20 -6]
$C$DW$355	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$355, DW_AT_location[DW_OP_breg20 -8]
$C$DW$356	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$356, DW_AT_location[DW_OP_breg20 -9]
$C$DW$357	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$357, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[10],AH           ; [CPU_] |624| 
        MOV       *-SP[9],AL            ; [CPU_] |624| 
        MOVL      *-SP[8],XAR5          ; [CPU_] |624| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |624| 
	.dwpsn	file "../mms.c",line 625,column 3,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |625| 
        CMPB      AL,#1                 ; [CPU_] |625| 
        BF        $C$L102,NEQ           ; [CPU_] |625| 
        ; branchcc occurs ; [] |625| 
	.dwpsn	file "../mms.c",line 626,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |626| 
        CMPB      AL,#1                 ; [CPU_] |626| 
        BF        $C$L102,NEQ           ; [CPU_] |626| 
        ; branchcc occurs ; [] |626| 
	.dwpsn	file "../mms.c",line 627,column 7,is_stmt
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOVIZ     R0H,#18607            ; [CPU_] |627| 
        MOV32     R1H,@_ODP_Battery_Capacity ; [CPU_] |627| 
        MOVXI     R0H,#51200            ; [CPU_] |627| 
        MPYF32    R0H,R0H,R1H           ; [CPU_] |627| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R0H,#16896        ; [CPU_] |627| 
$C$DW$358	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$358, DW_AT_low_pc(0x00)
	.dwattr $C$DW$358, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$358, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |627| 
        ; call occurs [#_CNV_Round] ; [] |627| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      @_PAR_Capacity_Total,ACC ; [CPU_] |627| 
	.dwpsn	file "../mms.c",line 628,column 7,is_stmt
        MOVW      DP,#_ODV_SOC_SOC2     ; [CPU_U] 
        MOVU      ACC,@_ODV_SOC_SOC2    ; [CPU_] |628| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      XT,ACC                ; [CPU_] |628| 
        MOVB      ACC,#100              ; [CPU_] |628| 
        MOVL      *-SP[4],ACC           ; [CPU_] |628| 
        QMPYXUL   P,XT,@_PAR_Capacity_Total ; [CPU_] |628| 
        MOV       *-SP[2],#0            ; [CPU_] |628| 
        MOVL      XAR6,P                ; [CPU_] |628| 
        MOV       *-SP[1],#0            ; [CPU_] |628| 
        MOVL      ACC,XAR6              ; [CPU_] |628| 
        IMPYL     P,XT,@_PAR_Capacity_Total ; [CPU_] |628| 
$C$DW$359	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$359, DW_AT_low_pc(0x00)
	.dwattr $C$DW$359, DW_AT_name("ULL$$DIV")
	.dwattr $C$DW$359, DW_AT_TI_call
        LCR       #ULL$$DIV             ; [CPU_] |628| 
        ; call occurs [#ULL$$DIV] ; [] |628| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOVL      @_PAR_Capacity_Left,P ; [CPU_] |628| 
	.dwpsn	file "../mms.c",line 629,column 7,is_stmt
        MOVW      DP,#_PAR_Capacity_TotalLife_Used ; [CPU_U] 
        ZAPA      ; [CPU_] |629| 
        MOVL      @_PAR_Capacity_TotalLife_Used,P ; [CPU_] |629| 
        MOVL      @_PAR_Capacity_TotalLife_Used+2,ACC ; [CPU_] |629| 
$C$L102:    
	.dwpsn	file "../mms.c",line 632,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |632| 
	.dwpsn	file "../mms.c",line 633,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$360	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$360, DW_AT_low_pc(0x00)
	.dwattr $C$DW$360, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$349, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$349, DW_AT_TI_end_line(0x279)
	.dwattr $C$DW$349, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$349

	.sect	".text"
	.global	_StartRecorderCallBack

$C$DW$361	.dwtag  DW_TAG_subprogram, DW_AT_name("StartRecorderCallBack")
	.dwattr $C$DW$361, DW_AT_low_pc(_StartRecorderCallBack)
	.dwattr $C$DW$361, DW_AT_high_pc(0x00)
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_StartRecorderCallBack")
	.dwattr $C$DW$361, DW_AT_external
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$361, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$361, DW_AT_TI_begin_line(0x27b)
	.dwattr $C$DW$361, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$361, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 635,column 100,is_stmt,address _StartRecorderCallBack

	.dwfde $C$DW$CIE, _StartRecorderCallBack
$C$DW$362	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$362, DW_AT_location[DW_OP_reg12]
$C$DW$363	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$363, DW_AT_location[DW_OP_reg14]
$C$DW$364	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$364, DW_AT_location[DW_OP_reg0]
$C$DW$365	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$365, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _StartRecorderCallBack        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_StartRecorderCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$366	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$366, DW_AT_location[DW_OP_breg20 -2]
$C$DW$367	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$367, DW_AT_location[DW_OP_breg20 -4]
$C$DW$368	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$368, DW_AT_location[DW_OP_breg20 -5]
$C$DW$369	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$369, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |635| 
        MOV       *-SP[5],AL            ; [CPU_] |635| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |635| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |635| 
	.dwpsn	file "../mms.c",line 637,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |637| 
        CMPB      AL,#1                 ; [CPU_] |637| 
        BF        $C$L103,NEQ           ; [CPU_] |637| 
        ; branchcc occurs ; [] |637| 
	.dwpsn	file "../mms.c",line 638,column 5,is_stmt
$C$DW$370	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$370, DW_AT_low_pc(0x00)
	.dwattr $C$DW$370, DW_AT_name("_REC_StartRecorder")
	.dwattr $C$DW$370, DW_AT_TI_call
        LCR       #_REC_StartRecorder   ; [CPU_] |638| 
        ; call occurs [#_REC_StartRecorder] ; [] |638| 
$C$L103:    
	.dwpsn	file "../mms.c",line 640,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |640| 
	.dwpsn	file "../mms.c",line 641,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$371	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$371, DW_AT_low_pc(0x00)
	.dwattr $C$DW$371, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$361, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$361, DW_AT_TI_end_line(0x281)
	.dwattr $C$DW$361, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$361

	.sect	".text"
	.global	_MultiunitsCallback

$C$DW$372	.dwtag  DW_TAG_subprogram, DW_AT_name("MultiunitsCallback")
	.dwattr $C$DW$372, DW_AT_low_pc(_MultiunitsCallback)
	.dwattr $C$DW$372, DW_AT_high_pc(0x00)
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_MultiunitsCallback")
	.dwattr $C$DW$372, DW_AT_external
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$372, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$372, DW_AT_TI_begin_line(0x283)
	.dwattr $C$DW$372, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$372, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 643,column 97,is_stmt,address _MultiunitsCallback

	.dwfde $C$DW$CIE, _MultiunitsCallback
$C$DW$373	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$373, DW_AT_location[DW_OP_reg12]
$C$DW$374	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$374, DW_AT_location[DW_OP_reg14]
$C$DW$375	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$375, DW_AT_location[DW_OP_reg0]
$C$DW$376	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$376, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MultiunitsCallback           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MultiunitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$377	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$377, DW_AT_location[DW_OP_breg20 -2]
$C$DW$378	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$378, DW_AT_location[DW_OP_breg20 -4]
$C$DW$379	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$379, DW_AT_location[DW_OP_breg20 -5]
$C$DW$380	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$380, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |643| 
        MOV       *-SP[5],AL            ; [CPU_] |643| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |643| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |643| 
	.dwpsn	file "../mms.c",line 645,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |645| 
        CMPB      AL,#1                 ; [CPU_] |645| 
        BF        $C$L104,NEQ           ; [CPU_] |645| 
        ; branchcc occurs ; [] |645| 
	.dwpsn	file "../mms.c",line 646,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Multiunits ; [CPU_] |646| 
$C$DW$381	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$381, DW_AT_low_pc(0x00)
	.dwattr $C$DW$381, DW_AT_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$381, DW_AT_TI_call
        LCR       #_PAR_AddMultiUnits   ; [CPU_] |646| 
        ; call occurs [#_PAR_AddMultiUnits] ; [] |646| 
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       @_ODV_Recorder_Multiunits,AL ; [CPU_] |646| 
$C$L104:    
	.dwpsn	file "../mms.c",line 648,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |648| 
	.dwpsn	file "../mms.c",line 649,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$382	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$382, DW_AT_low_pc(0x00)
	.dwattr $C$DW$382, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$372, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$372, DW_AT_TI_end_line(0x289)
	.dwattr $C$DW$372, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$372

	.sect	".text"
	.global	_VariablesCallback

$C$DW$383	.dwtag  DW_TAG_subprogram, DW_AT_name("VariablesCallback")
	.dwattr $C$DW$383, DW_AT_low_pc(_VariablesCallback)
	.dwattr $C$DW$383, DW_AT_high_pc(0x00)
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_VariablesCallback")
	.dwattr $C$DW$383, DW_AT_external
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$383, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$383, DW_AT_TI_begin_line(0x28b)
	.dwattr $C$DW$383, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$383, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 651,column 96,is_stmt,address _VariablesCallback

	.dwfde $C$DW$CIE, _VariablesCallback
$C$DW$384	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$384, DW_AT_location[DW_OP_reg12]
$C$DW$385	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$385, DW_AT_location[DW_OP_reg14]
$C$DW$386	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$386, DW_AT_location[DW_OP_reg0]
$C$DW$387	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$387, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VariablesCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VariablesCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$388	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$388, DW_AT_location[DW_OP_breg20 -2]
$C$DW$389	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$389, DW_AT_location[DW_OP_breg20 -4]
$C$DW$390	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$390, DW_AT_location[DW_OP_breg20 -5]
$C$DW$391	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$391, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |651| 
        MOV       *-SP[5],AL            ; [CPU_] |651| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |651| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |651| 
	.dwpsn	file "../mms.c",line 653,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |653| 
        CMPB      AL,#1                 ; [CPU_] |653| 
        BF        $C$L105,NEQ           ; [CPU_] |653| 
        ; branchcc occurs ; [] |653| 
	.dwpsn	file "../mms.c",line 654,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Variables ; [CPU_] |654| 
$C$DW$392	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$392, DW_AT_low_pc(0x00)
	.dwattr $C$DW$392, DW_AT_name("_PAR_AddVariables")
	.dwattr $C$DW$392, DW_AT_TI_call
        LCR       #_PAR_AddVariables    ; [CPU_] |654| 
        ; call occurs [#_PAR_AddVariables] ; [] |654| 
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       @_ODV_Recorder_Variables,AL ; [CPU_] |654| 
$C$L105:    
	.dwpsn	file "../mms.c",line 656,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |656| 
	.dwpsn	file "../mms.c",line 657,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$393	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$393, DW_AT_low_pc(0x00)
	.dwattr $C$DW$393, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$383, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$383, DW_AT_TI_end_line(0x291)
	.dwattr $C$DW$383, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$383

	.sect	".text"
	.global	_WriteOutputs8BitCallback

$C$DW$394	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs8BitCallback")
	.dwattr $C$DW$394, DW_AT_low_pc(_WriteOutputs8BitCallback)
	.dwattr $C$DW$394, DW_AT_high_pc(0x00)
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_WriteOutputs8BitCallback")
	.dwattr $C$DW$394, DW_AT_external
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$394, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$394, DW_AT_TI_begin_line(0x294)
	.dwattr $C$DW$394, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$394, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 660,column 103,is_stmt,address _WriteOutputs8BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs8BitCallback
$C$DW$395	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$395, DW_AT_location[DW_OP_reg12]
$C$DW$396	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$396, DW_AT_location[DW_OP_reg14]
$C$DW$397	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$397, DW_AT_location[DW_OP_reg0]
$C$DW$398	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$398, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs8BitCallback     FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs8BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$399	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$399, DW_AT_location[DW_OP_breg20 -2]
$C$DW$400	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$400, DW_AT_location[DW_OP_breg20 -4]
$C$DW$401	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$401, DW_AT_location[DW_OP_breg20 -5]
$C$DW$402	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$402, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |660| 
        MOV       *-SP[5],AL            ; [CPU_] |660| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |660| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |660| 
	.dwpsn	file "../mms.c",line 661,column 3,is_stmt
	.dwpsn	file "../mms.c",line 662,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |662| 
	.dwpsn	file "../mms.c",line 663,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$403	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$403, DW_AT_low_pc(0x00)
	.dwattr $C$DW$403, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$394, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$394, DW_AT_TI_end_line(0x297)
	.dwattr $C$DW$394, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$394

	.sect	".text"
	.global	_WriteOutputs16BitCallback

$C$DW$404	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs16BitCallback")
	.dwattr $C$DW$404, DW_AT_low_pc(_WriteOutputs16BitCallback)
	.dwattr $C$DW$404, DW_AT_high_pc(0x00)
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_WriteOutputs16BitCallback")
	.dwattr $C$DW$404, DW_AT_external
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$404, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$404, DW_AT_TI_begin_line(0x299)
	.dwattr $C$DW$404, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$404, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 665,column 104,is_stmt,address _WriteOutputs16BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs16BitCallback
$C$DW$405	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$405, DW_AT_location[DW_OP_reg12]
$C$DW$406	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$406, DW_AT_location[DW_OP_reg14]
$C$DW$407	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$407, DW_AT_location[DW_OP_reg0]
$C$DW$408	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$408, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs16BitCallback    FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs16BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$409	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$409, DW_AT_location[DW_OP_breg20 -2]
$C$DW$410	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$410, DW_AT_location[DW_OP_breg20 -4]
$C$DW$411	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$411, DW_AT_location[DW_OP_breg20 -5]
$C$DW$412	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$412, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |665| 
        MOV       *-SP[5],AL            ; [CPU_] |665| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |665| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |665| 
	.dwpsn	file "../mms.c",line 666,column 3,is_stmt
	.dwpsn	file "../mms.c",line 669,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |669| 
	.dwpsn	file "../mms.c",line 670,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$413	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$413, DW_AT_low_pc(0x00)
	.dwattr $C$DW$413, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$404, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$404, DW_AT_TI_end_line(0x29e)
	.dwattr $C$DW$404, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$404

	.sect	".text"
	.global	_ReadInputs8BitsCallback

$C$DW$414	.dwtag  DW_TAG_subprogram, DW_AT_name("ReadInputs8BitsCallback")
	.dwattr $C$DW$414, DW_AT_low_pc(_ReadInputs8BitsCallback)
	.dwattr $C$DW$414, DW_AT_high_pc(0x00)
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_ReadInputs8BitsCallback")
	.dwattr $C$DW$414, DW_AT_external
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$414, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$414, DW_AT_TI_begin_line(0x2a0)
	.dwattr $C$DW$414, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$414, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 672,column 102,is_stmt,address _ReadInputs8BitsCallback

	.dwfde $C$DW$CIE, _ReadInputs8BitsCallback
$C$DW$415	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$415, DW_AT_location[DW_OP_reg12]
$C$DW$416	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$416, DW_AT_location[DW_OP_reg14]
$C$DW$417	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$417, DW_AT_location[DW_OP_reg0]
$C$DW$418	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$418, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ReadInputs8BitsCallback      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ReadInputs8BitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$419	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$419, DW_AT_location[DW_OP_breg20 -2]
$C$DW$420	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$420, DW_AT_location[DW_OP_breg20 -4]
$C$DW$421	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$421, DW_AT_location[DW_OP_breg20 -5]
$C$DW$422	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$422, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |672| 
        MOV       *-SP[5],AL            ; [CPU_] |672| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |672| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |672| 
	.dwpsn	file "../mms.c",line 673,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |673| 
	.dwpsn	file "../mms.c",line 674,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$423	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$423, DW_AT_low_pc(0x00)
	.dwattr $C$DW$423, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$414, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$414, DW_AT_TI_end_line(0x2a2)
	.dwattr $C$DW$414, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$414

	.sect	".text"
	.global	_ControlWordCallBack

$C$DW$424	.dwtag  DW_TAG_subprogram, DW_AT_name("ControlWordCallBack")
	.dwattr $C$DW$424, DW_AT_low_pc(_ControlWordCallBack)
	.dwattr $C$DW$424, DW_AT_high_pc(0x00)
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_ControlWordCallBack")
	.dwattr $C$DW$424, DW_AT_external
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$424, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$424, DW_AT_TI_begin_line(0x2a4)
	.dwattr $C$DW$424, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$424, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 676,column 98,is_stmt,address _ControlWordCallBack

	.dwfde $C$DW$CIE, _ControlWordCallBack
$C$DW$425	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$425, DW_AT_location[DW_OP_reg12]
$C$DW$426	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$426, DW_AT_location[DW_OP_reg14]
$C$DW$427	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$427, DW_AT_location[DW_OP_reg0]
$C$DW$428	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$428, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ControlWordCallBack          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ControlWordCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$429	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$429, DW_AT_location[DW_OP_breg20 -2]
$C$DW$430	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$430, DW_AT_location[DW_OP_breg20 -4]
$C$DW$431	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$431, DW_AT_location[DW_OP_breg20 -5]
$C$DW$432	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$432, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |676| 
        MOV       *-SP[5],AL            ; [CPU_] |676| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |676| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |676| 
	.dwpsn	file "../mms.c",line 677,column 3,is_stmt
	.dwpsn	file "../mms.c",line 678,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |678| 
	.dwpsn	file "../mms.c",line 679,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$433	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$433, DW_AT_low_pc(0x00)
	.dwattr $C$DW$433, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$424, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$424, DW_AT_TI_end_line(0x2a7)
	.dwattr $C$DW$424, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$424

	.sect	".text"
	.global	_VersionCallback

$C$DW$434	.dwtag  DW_TAG_subprogram, DW_AT_name("VersionCallback")
	.dwattr $C$DW$434, DW_AT_low_pc(_VersionCallback)
	.dwattr $C$DW$434, DW_AT_high_pc(0x00)
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_VersionCallback")
	.dwattr $C$DW$434, DW_AT_external
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$434, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$434, DW_AT_TI_begin_line(0x2a9)
	.dwattr $C$DW$434, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$434, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 681,column 94,is_stmt,address _VersionCallback

	.dwfde $C$DW$CIE, _VersionCallback
$C$DW$435	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$435, DW_AT_location[DW_OP_reg12]
$C$DW$436	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$436, DW_AT_location[DW_OP_reg14]
$C$DW$437	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$437, DW_AT_location[DW_OP_reg0]
$C$DW$438	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$438, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VersionCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VersionCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$439	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$439, DW_AT_location[DW_OP_breg20 -2]
$C$DW$440	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$440, DW_AT_location[DW_OP_breg20 -4]
$C$DW$441	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$441, DW_AT_location[DW_OP_breg20 -5]
$C$DW$442	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$442, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |681| 
        MOV       *-SP[5],AL            ; [CPU_] |681| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |681| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |681| 
	.dwpsn	file "../mms.c",line 683,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |683| 
	.dwpsn	file "../mms.c",line 684,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$443	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$443, DW_AT_low_pc(0x00)
	.dwattr $C$DW$443, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$434, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$434, DW_AT_TI_end_line(0x2ac)
	.dwattr $C$DW$434, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$434

	.sect	".text"
	.global	_WriteAnalogueOutputsCallback

$C$DW$444	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteAnalogueOutputsCallback")
	.dwattr $C$DW$444, DW_AT_low_pc(_WriteAnalogueOutputsCallback)
	.dwattr $C$DW$444, DW_AT_high_pc(0x00)
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_WriteAnalogueOutputsCallback")
	.dwattr $C$DW$444, DW_AT_external
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$444, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$444, DW_AT_TI_begin_line(0x2ae)
	.dwattr $C$DW$444, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$444, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 686,column 107,is_stmt,address _WriteAnalogueOutputsCallback

	.dwfde $C$DW$CIE, _WriteAnalogueOutputsCallback
$C$DW$445	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$445, DW_AT_location[DW_OP_reg12]
$C$DW$446	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$446, DW_AT_location[DW_OP_reg14]
$C$DW$447	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$447, DW_AT_location[DW_OP_reg0]
$C$DW$448	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$448, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteAnalogueOutputsCallback FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteAnalogueOutputsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$449	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$449, DW_AT_location[DW_OP_breg20 -2]
$C$DW$450	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$450, DW_AT_location[DW_OP_breg20 -4]
$C$DW$451	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$451, DW_AT_location[DW_OP_breg20 -5]
$C$DW$452	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$452, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |686| 
        MOV       *-SP[5],AL            ; [CPU_] |686| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |686| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |686| 
	.dwpsn	file "../mms.c",line 688,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |688| 
	.dwpsn	file "../mms.c",line 689,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$453	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$453, DW_AT_low_pc(0x00)
	.dwattr $C$DW$453, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$444, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$444, DW_AT_TI_end_line(0x2b1)
	.dwattr $C$DW$444, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$444

	.sect	".text"
	.global	_CommErrorSetCallback

$C$DW$454	.dwtag  DW_TAG_subprogram, DW_AT_name("CommErrorSetCallback")
	.dwattr $C$DW$454, DW_AT_low_pc(_CommErrorSetCallback)
	.dwattr $C$DW$454, DW_AT_high_pc(0x00)
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_CommErrorSetCallback")
	.dwattr $C$DW$454, DW_AT_external
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$454, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$454, DW_AT_TI_begin_line(0x2b4)
	.dwattr $C$DW$454, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$454, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 692,column 99,is_stmt,address _CommErrorSetCallback

	.dwfde $C$DW$CIE, _CommErrorSetCallback
$C$DW$455	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$455, DW_AT_location[DW_OP_reg12]
$C$DW$456	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$456, DW_AT_location[DW_OP_reg14]
$C$DW$457	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$457, DW_AT_location[DW_OP_reg0]
$C$DW$458	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$458, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _CommErrorSetCallback         FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_CommErrorSetCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$459	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$459, DW_AT_location[DW_OP_breg20 -2]
$C$DW$460	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$460, DW_AT_location[DW_OP_breg20 -4]
$C$DW$461	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$461, DW_AT_location[DW_OP_breg20 -5]
$C$DW$462	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$462, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |692| 
        MOV       *-SP[5],AL            ; [CPU_] |692| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |692| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |692| 
	.dwpsn	file "../mms.c",line 693,column 3,is_stmt
	.dwpsn	file "../mms.c",line 696,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |696| 
	.dwpsn	file "../mms.c",line 697,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$463	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$463, DW_AT_low_pc(0x00)
	.dwattr $C$DW$463, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$454, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$454, DW_AT_TI_end_line(0x2b9)
	.dwattr $C$DW$454, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$454

	.sect	".text"
	.global	_SaveAllParameters

$C$DW$464	.dwtag  DW_TAG_subprogram, DW_AT_name("SaveAllParameters")
	.dwattr $C$DW$464, DW_AT_low_pc(_SaveAllParameters)
	.dwattr $C$DW$464, DW_AT_high_pc(0x00)
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_SaveAllParameters")
	.dwattr $C$DW$464, DW_AT_external
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$464, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$464, DW_AT_TI_begin_line(0x2bb)
	.dwattr $C$DW$464, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$464, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 699,column 96,is_stmt,address _SaveAllParameters

	.dwfde $C$DW$CIE, _SaveAllParameters
$C$DW$465	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$465, DW_AT_location[DW_OP_reg12]
$C$DW$466	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$466, DW_AT_location[DW_OP_reg14]
$C$DW$467	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$467, DW_AT_location[DW_OP_reg0]
$C$DW$468	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$468, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SaveAllParameters            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SaveAllParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$469	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$469, DW_AT_location[DW_OP_breg20 -2]
$C$DW$470	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$470, DW_AT_location[DW_OP_breg20 -4]
$C$DW$471	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$471, DW_AT_location[DW_OP_breg20 -5]
$C$DW$472	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$472, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |699| 
        MOV       *-SP[5],AL            ; [CPU_] |699| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |699| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |699| 
	.dwpsn	file "../mms.c",line 700,column 3,is_stmt
        MOVW      DP,#_ODV_StoreParameters ; [CPU_U] 
        MOV       AL,#30309             ; [CPU_] |700| 
        MOV       AH,#29537             ; [CPU_] |700| 
        CMPL      ACC,@_ODV_StoreParameters ; [CPU_] |700| 
        BF        $C$L106,NEQ           ; [CPU_] |700| 
        ; branchcc occurs ; [] |700| 
	.dwpsn	file "../mms.c",line 701,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |701| 
        MOVL      @_ODV_StoreParameters,ACC ; [CPU_] |701| 
	.dwpsn	file "../mms.c",line 702,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |702| 
$C$DW$473	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$473, DW_AT_low_pc(0x00)
	.dwattr $C$DW$473, DW_AT_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$473, DW_AT_TI_call
        LCR       #_PAR_WriteAllPermanentParam ; [CPU_] |702| 
        ; call occurs [#_PAR_WriteAllPermanentParam] ; [] |702| 
$C$L106:    
	.dwpsn	file "../mms.c",line 704,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |704| 
	.dwpsn	file "../mms.c",line 705,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$474	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$474, DW_AT_low_pc(0x00)
	.dwattr $C$DW$474, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$464, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$464, DW_AT_TI_end_line(0x2c1)
	.dwattr $C$DW$464, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$464

	.sect	".text"
	.global	_LoadDefaultParameters

$C$DW$475	.dwtag  DW_TAG_subprogram, DW_AT_name("LoadDefaultParameters")
	.dwattr $C$DW$475, DW_AT_low_pc(_LoadDefaultParameters)
	.dwattr $C$DW$475, DW_AT_high_pc(0x00)
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_LoadDefaultParameters")
	.dwattr $C$DW$475, DW_AT_external
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$475, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$475, DW_AT_TI_begin_line(0x2c3)
	.dwattr $C$DW$475, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$475, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 707,column 100,is_stmt,address _LoadDefaultParameters

	.dwfde $C$DW$CIE, _LoadDefaultParameters
$C$DW$476	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$476, DW_AT_location[DW_OP_reg12]
$C$DW$477	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$477, DW_AT_location[DW_OP_reg14]
$C$DW$478	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$478, DW_AT_location[DW_OP_reg0]
$C$DW$479	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$479, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LoadDefaultParameters        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LoadDefaultParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$480	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$480, DW_AT_location[DW_OP_breg20 -2]
$C$DW$481	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$481, DW_AT_location[DW_OP_breg20 -4]
$C$DW$482	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$482, DW_AT_location[DW_OP_breg20 -5]
$C$DW$483	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$483, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |707| 
        MOV       *-SP[5],AL            ; [CPU_] |707| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |707| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |707| 
	.dwpsn	file "../mms.c",line 708,column 3,is_stmt
        MOVW      DP,#_ODV_RestoreDefaultParameters ; [CPU_U] 
        MOV       AL,#24932             ; [CPU_] |708| 
        MOV       AH,#27759             ; [CPU_] |708| 
        CMPL      ACC,@_ODV_RestoreDefaultParameters ; [CPU_] |708| 
        BF        $C$L107,NEQ           ; [CPU_] |708| 
        ; branchcc occurs ; [] |708| 
	.dwpsn	file "../mms.c",line 709,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |709| 
        MOVL      @_ODV_RestoreDefaultParameters,ACC ; [CPU_] |709| 
	.dwpsn	file "../mms.c",line 710,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |710| 
$C$DW$484	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$484, DW_AT_low_pc(0x00)
	.dwattr $C$DW$484, DW_AT_name("_PAR_UpdateCode")
	.dwattr $C$DW$484, DW_AT_TI_call
        LCR       #_PAR_UpdateCode      ; [CPU_] |710| 
        ; call occurs [#_PAR_UpdateCode] ; [] |710| 
	.dwpsn	file "../mms.c",line 711,column 5,is_stmt
        MOVW      DP,#_BootST           ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |711| 
        MOVL      @_BootST,ACC          ; [CPU_] |711| 
$C$L107:    
	.dwpsn	file "../mms.c",line 713,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |713| 
	.dwpsn	file "../mms.c",line 714,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$485	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$485, DW_AT_low_pc(0x00)
	.dwattr $C$DW$485, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$475, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$475, DW_AT_TI_end_line(0x2ca)
	.dwattr $C$DW$475, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$475

	.sect	".text"
	.global	_DebugCallBack

$C$DW$486	.dwtag  DW_TAG_subprogram, DW_AT_name("DebugCallBack")
	.dwattr $C$DW$486, DW_AT_low_pc(_DebugCallBack)
	.dwattr $C$DW$486, DW_AT_high_pc(0x00)
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_DebugCallBack")
	.dwattr $C$DW$486, DW_AT_external
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$486, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$486, DW_AT_TI_begin_line(0x2cd)
	.dwattr $C$DW$486, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$486, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 717,column 92,is_stmt,address _DebugCallBack

	.dwfde $C$DW$CIE, _DebugCallBack
$C$DW$487	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$487, DW_AT_location[DW_OP_reg12]
$C$DW$488	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$488, DW_AT_location[DW_OP_reg14]
$C$DW$489	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$489, DW_AT_location[DW_OP_reg0]
$C$DW$490	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$490, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _DebugCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_DebugCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$491	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$491, DW_AT_location[DW_OP_breg20 -2]
$C$DW$492	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$492, DW_AT_location[DW_OP_breg20 -4]
$C$DW$493	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$493, DW_AT_location[DW_OP_breg20 -5]
$C$DW$494	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$494, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |717| 
        MOV       *-SP[5],AL            ; [CPU_] |717| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |717| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |717| 
	.dwpsn	file "../mms.c",line 718,column 3,is_stmt
        MOVW      DP,#_ODV_Debug        ; [CPU_U] 
        MOV       @_ODV_Debug,#0        ; [CPU_] |718| 
	.dwpsn	file "../mms.c",line 719,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |719| 
	.dwpsn	file "../mms.c",line 720,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$495	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$495, DW_AT_low_pc(0x00)
	.dwattr $C$DW$495, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$486, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$486, DW_AT_TI_end_line(0x2d0)
	.dwattr $C$DW$486, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$486

	.sect	".text"
	.global	_ResetCallBack

$C$DW$496	.dwtag  DW_TAG_subprogram, DW_AT_name("ResetCallBack")
	.dwattr $C$DW$496, DW_AT_low_pc(_ResetCallBack)
	.dwattr $C$DW$496, DW_AT_high_pc(0x00)
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_ResetCallBack")
	.dwattr $C$DW$496, DW_AT_external
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$496, DW_AT_TI_begin_file("../mms.c")
	.dwattr $C$DW$496, DW_AT_TI_begin_line(0x2d2)
	.dwattr $C$DW$496, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$496, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../mms.c",line 722,column 92,is_stmt,address _ResetCallBack

	.dwfde $C$DW$CIE, _ResetCallBack
$C$DW$497	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$497, DW_AT_location[DW_OP_reg12]
$C$DW$498	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$498, DW_AT_location[DW_OP_reg14]
$C$DW$499	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$499, DW_AT_location[DW_OP_reg0]
$C$DW$500	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$500, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ResetCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ResetCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$501	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$501, DW_AT_location[DW_OP_breg20 -2]
$C$DW$502	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$502, DW_AT_location[DW_OP_breg20 -4]
$C$DW$503	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$503, DW_AT_location[DW_OP_breg20 -5]
$C$DW$504	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$504, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |722| 
        MOV       *-SP[5],AL            ; [CPU_] |722| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |722| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |722| 
	.dwpsn	file "../mms.c",line 723,column 3,is_stmt
        MOVW      DP,#_ODV_ResetHW      ; [CPU_U] 
        MOV       AL,#29295             ; [CPU_] |723| 
        MOV       AH,#31333             ; [CPU_] |723| 
        CMPL      ACC,@_ODV_ResetHW     ; [CPU_] |723| 
        BF        $C$L108,NEQ           ; [CPU_] |723| 
        ; branchcc occurs ; [] |723| 
	.dwpsn	file "../mms.c",line 724,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |724| 
        MOVL      @_ODV_ResetHW,ACC     ; [CPU_] |724| 
	.dwpsn	file "../mms.c",line 725,column 5,is_stmt
        MOVW      DP,#_BootST           ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |725| 
        MOVL      @_BootST,ACC          ; [CPU_] |725| 
$C$L108:    
	.dwpsn	file "../mms.c",line 727,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |727| 
	.dwpsn	file "../mms.c",line 728,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$505	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$505, DW_AT_low_pc(0x00)
	.dwattr $C$DW$505, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$496, DW_AT_TI_end_file("../mms.c")
	.dwattr $C$DW$496, DW_AT_TI_end_line(0x2d8)
	.dwattr $C$DW$496, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$496

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_ERR_ErrorComm
	.global	_ERR_ErrorOverCurrent
	.global	_ERR_ErrorUnderVoltage
	.global	_ERR_HandleWarning
	.global	_WARN_ErrorParam
	.global	_PAR_SetParamDependantVars
	.global	_PAR_GetLogNB
	.global	_genCRC32Table
	.global	_PAR_AddLog
	.global	_HAL_Init
	.global	_REC_StartRecorder
	.global	_HAL_Random
	.global	_setNodeId
	.global	_canInit
	.global	_HAL_Unlock
	.global	_SCI1_Command
	.global	_RS232ReceiveEnable
	.global	_USB_Unlock
	.global	_HAL_Reset
	.global	_USB_Stop
	.global	_SCI1_Receive
	.global	_ODV_CommError_Set
	.global	_ODP_SafetyLimits_Low_Voltage_Charge_Delay
	.global	_ODV_Current_ChargeAllowed
	.global	_ODP_SafetyLimits_Low_Voltage_Current_Allow
	.global	_ODP_Current_C_D_Mode
	.global	_ODV_Current_DischargeAllowed
	.global	_ODV_MachineEvent
	.global	_ODV_Gateway_LogNB
	.global	_ODV_Gateway_State
	.global	_ODP_SafetyLimits_Umin
	.global	_ODP_SafetyLimits_Umax
	.global	_ODV_Recorder_Multiunits
	.global	_ODP_VersionParameters
	.global	_ODV_SciSend
	.global	_ODV_Recorder_Variables
	.global	_ODP_SafetyLimits_UnderCurrent
	.global	_ODP_SafetyLimits_Current_delay
	.global	_ODP_SafetyLimits_Low_Voltage_Current_Delay
	.global	_ODP_SafetyLimits_SleepTimeout
	.global	_ODP_SafetyLimits_Overcurrent
	.global	_ODP_SafetyLimits_Charge_In_Thres_Cur
	.global	_ODP_SafetyLimits_Voltage_delay
	.global	_ODP_SafetyLimits_UnderVoltage
	.global	_HAL_Enable
	.global	_SCI_MsgAvailable
	.global	_HAL_CellOK
	.global	_ODV_Controlword
	.global	_HAL_NewCurPoint
	.global	_ODV_Debug
	.global	_SCI_Available
	.global	_ODP_Board_BaudRate
	.global	_ODP_SleepCurrent
	.global	_ODP_Board_Config
	.global	_ODP_CommError_TimeOut
	.global	_ODP_CommError_Delay
	.global	_ODV_MachineMode
	.global	_ODV_Module1_MinCellVoltage
	.global	_ODV_Module1_MaxCellVoltage
	.global	_ODV_Module1_Current
	.global	_ODV_Module1_Status
	.global	_ODV_Module1_SOC
	.global	_ODV_SOC_SOC2
	.global	_ERR_ClearWarning
	.global	_PAR_InitParam
	.global	_TimeLogIndex
	.global	_I2C_Command
	.global	_PAR_UpdateCode
	.global	_USB_Start
	.global	_I2C_CommandNoWait
	.global	_MBX_post
	.global	_setState
	.global	_MBX_pend
	.global	_SEM_pend
	.global	_PAR_AddMultiUnits
	.global	_ERR_ClearError
	.global	_PAR_AddVariables
	.global	_getCRC32_cpu
	.global	_CNV_Round
	.global	_PAR_WriteStatisticParam
	.global	_ODP_RandomNB
	.global	_ODP_Battery_Capacity
	.global	_MMSConfig
	.global	_PAR_StoreODSubIndex
	.global	_PAR_WriteAllPermanentParam
	.global	_PAR_Capacity_Total
	.global	_PAR_Capacity_Left
	.global	_ODV_StoreParameters
	.global	_ODV_SysTick_ms
	.global	_ODV_RestoreDefaultParameters
	.global	_ODV_Write_Outputs_16_Bit
	.global	_ODV_ResetHW
	.global	_ODV_Gateway_Errorcode
	.global	_ODP_OnTime
	.global	_ODV_ErrorDsp_ErrorNumber
	.global	_ODP_Board_RevisionNumber
	.global	_ODV_CommError_Counter
	.global	_ODV_RTC_Text
	.global	_PAR_Capacity_TotalLife_Used
	.global	_ODV_Gateway_Date_Time
	.global	_golden_CRC_values
	.global	_TSK_timerSem
	.global	_PieCtrlRegs
	.global	_can_tx_mbox
	.global	_can_rx_mbox
	.global	_mailboxSDOout
	.global	_sci_rx_mbox
	.global	_SCI_MsgInRS232
	.global	_ODI_mms_dict_Data
	.global	FS$$DIV
	.global	ULL$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$140	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$140, DW_AT_byte_size(0x01)
$C$DW$506	.dwtag  DW_TAG_enumerator, DW_AT_name("EVEN"), DW_AT_const_value(0x00)
$C$DW$507	.dwtag  DW_TAG_enumerator, DW_AT_name("ODD"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$140

$C$DW$T$141	.dwtag  DW_TAG_typedef, DW_AT_name("parity_t")
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$T$141, DW_AT_language(DW_LANG_C)

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$508, DW_AT_name("cob_id")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$509, DW_AT_name("rtr")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$510, DW_AT_name("len")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$511, DW_AT_name("data")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$131	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$131, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$512, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$513, DW_AT_name("csSDO")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$514, DW_AT_name("csEmergency")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$515, DW_AT_name("csSYNC")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$516, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$517, DW_AT_name("csPDO")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$518, DW_AT_name("csLSS")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$88	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$88, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$519, DW_AT_name("errCode")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$520, DW_AT_name("errRegMask")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$521, DW_AT_name("active")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$122	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)

$C$DW$T$123	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$123, DW_AT_byte_size(0x18)
$C$DW$522	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$522, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$123


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$523, DW_AT_name("index")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$524, DW_AT_name("subindex")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$525, DW_AT_name("size")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$526, DW_AT_name("address")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$128	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$128, DW_AT_language(DW_LANG_C)
$C$DW$T$129	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$T$129, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$527, DW_AT_name("SwitchOn")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$527, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$528, DW_AT_name("EnableVolt")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$528, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$529, DW_AT_name("QuickStop")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$529, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$530, DW_AT_name("EnableOperation")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$530, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$531, DW_AT_name("OpModeSpecific")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_OpModeSpecific")
	.dwattr $C$DW$531, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$532, DW_AT_name("ResetFault")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$532, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$533, DW_AT_name("Halt")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$533, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$534, DW_AT_name("Oms")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$534, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$535, DW_AT_name("Rsvd")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$535, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$536, DW_AT_name("Manufacturer")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$536, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$537, DW_AT_name("SwitchOn")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$537, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$538, DW_AT_name("EnableVolt")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$538, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$539, DW_AT_name("QuickStop")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$539, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$540, DW_AT_name("EnableOperation")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$540, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$541, DW_AT_name("Rsvd0")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_Rsvd0")
	.dwattr $C$DW$541, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$542, DW_AT_name("ResetFault")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$542, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$543, DW_AT_name("Halt")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$543, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$544, DW_AT_name("Oms")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$544, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$545, DW_AT_name("Rsvd")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$545, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$546, DW_AT_name("Manufacturer")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$546, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x01)
$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$547, DW_AT_name("SwitchOn")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$547, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$548, DW_AT_name("EnableVolt")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$548, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$549, DW_AT_name("QuickStop")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$549, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$550, DW_AT_name("EnableOperation")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$550, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$551, DW_AT_name("NewSetPoint")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_NewSetPoint")
	.dwattr $C$DW$551, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$552, DW_AT_name("ChangeSetImm")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_ChangeSetImm")
	.dwattr $C$DW$552, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$553, DW_AT_name("Abs_Rel")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_Abs_Rel")
	.dwattr $C$DW$553, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$554, DW_AT_name("ResetFault")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$554, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$555, DW_AT_name("Halt")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$555, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$556, DW_AT_name("ChangeSetPos")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_ChangeSetPos")
	.dwattr $C$DW$556, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$557, DW_AT_name("Rsvd")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$557, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$558, DW_AT_name("Manufacturer")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$558, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$559, DW_AT_name("can_wk")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_can_wk")
	.dwattr $C$DW$559, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$560, DW_AT_name("sw_wk")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_sw_wk")
	.dwattr $C$DW$560, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$561, DW_AT_name("bal_dic")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_bal_dic")
	.dwattr $C$DW$561, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$562, DW_AT_name("bal_ch")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_bal_ch")
	.dwattr $C$DW$562, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$563, DW_AT_name("bal_ocv")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_bal_ocv")
	.dwattr $C$DW$563, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$564, DW_AT_name("lem")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_lem")
	.dwattr $C$DW$564, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$565, DW_AT_name("shunt")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_shunt")
	.dwattr $C$DW$565, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$566, DW_AT_name("relay_on")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_relay_on")
	.dwattr $C$DW$566, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$567, DW_AT_name("ch_wk")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_ch_wk")
	.dwattr $C$DW$567, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$568, DW_AT_name("SOC2")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_SOC2")
	.dwattr $C$DW$568, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$569, DW_AT_name("foil")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_foil")
	.dwattr $C$DW$569, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$570, DW_AT_name("rel_sta")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_rel_sta")
	.dwattr $C$DW$570, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$571, DW_AT_name("b12")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_b12")
	.dwattr $C$DW$571, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$572, DW_AT_name("b13")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_b13")
	.dwattr $C$DW$572, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$573, DW_AT_name("b14")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_b14")
	.dwattr $C$DW$573, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$574, DW_AT_name("b15")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_b15")
	.dwattr $C$DW$574, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27

$C$DW$T$142	.dwtag  DW_TAG_typedef, DW_AT_name("TMMSConfig")
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$142, DW_AT_language(DW_LANG_C)
$C$DW$T$143	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$143, DW_AT_address_class(0x16)

$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x48)
$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$575, DW_AT_name("cmd")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_cmd")
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$576, DW_AT_name("len")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$577, DW_AT_name("data")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29

$C$DW$T$144	.dwtag  DW_TAG_typedef, DW_AT_name("T_UsbMessage")
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$144, DW_AT_language(DW_LANG_C)

$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x04)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$578, DW_AT_name("Command")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_Command")
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$579, DW_AT_name("NodeId")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_NodeId")
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30

$C$DW$T$145	.dwtag  DW_TAG_typedef, DW_AT_name("TBoot")
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$145, DW_AT_language(DW_LANG_C)

$C$DW$T$31	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$580, DW_AT_name("ControlWord")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_ControlWord")
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$581, DW_AT_name("AnyMode")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_AnyMode")
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$582, DW_AT_name("VelocityMode")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_VelocityMode")
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$583, DW_AT_name("PositionMode")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_PositionMode")
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31

$C$DW$T$146	.dwtag  DW_TAG_typedef, DW_AT_name("TControlword")
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$146, DW_AT_language(DW_LANG_C)

$C$DW$T$39	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$39, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x08)
$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$584, DW_AT_name("wListElem")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$585, DW_AT_name("wCount")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$586, DW_AT_name("fxn")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$39

$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$34	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$34, DW_AT_address_class(0x16)
$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)

$C$DW$T$46	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$46, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x30)
$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$587, DW_AT_name("dataQue")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$588, DW_AT_name("freeQue")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$589, DW_AT_name("dataSem")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$590, DW_AT_name("freeSem")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$591, DW_AT_name("segid")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$592, DW_AT_name("size")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$593, DW_AT_name("length")
	.dwattr $C$DW$593, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$594, DW_AT_name("name")
	.dwattr $C$DW$594, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$46

$C$DW$T$147	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$147, DW_AT_language(DW_LANG_C)
$C$DW$T$149	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$149, DW_AT_address_class(0x16)
$C$DW$T$150	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$T$150, DW_AT_language(DW_LANG_C)

$C$DW$T$48	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$48, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x01)
$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$595, DW_AT_name("ACK1")
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$595, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$596, DW_AT_name("ACK2")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$596, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$597, DW_AT_name("ACK3")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$597, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$598, DW_AT_name("ACK4")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$598, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$599, DW_AT_name("ACK5")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$599, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$600, DW_AT_name("ACK6")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$600, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$601, DW_AT_name("ACK7")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$601, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$602, DW_AT_name("ACK8")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$602, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$603, DW_AT_name("ACK9")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$603, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$604, DW_AT_name("ACK10")
	.dwattr $C$DW$604, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$604, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$605, DW_AT_name("ACK11")
	.dwattr $C$DW$605, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$605, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$606, DW_AT_name("ACK12")
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$606, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$607, DW_AT_name("rsvd1")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$607, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$48


$C$DW$T$49	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$49, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x01)
$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$608, DW_AT_name("all")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$609, DW_AT_name("bit")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$49


$C$DW$T$50	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$50, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x01)
$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$610, DW_AT_name("ENPIE")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$610, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$611, DW_AT_name("PIEVECT")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$611, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$50


$C$DW$T$51	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$51, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$51, DW_AT_byte_size(0x01)
$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$612, DW_AT_name("all")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$613, DW_AT_name("bit")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$51


$C$DW$T$52	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$52, DW_AT_name("PIEIER_BITS")
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x01)
$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$614, DW_AT_name("INTx1")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$614, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$615, DW_AT_name("INTx2")
	.dwattr $C$DW$615, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$615, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$616, DW_AT_name("INTx3")
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$616, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$617, DW_AT_name("INTx4")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$617, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$618, DW_AT_name("INTx5")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$618, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$619, DW_AT_name("INTx6")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$619, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$620, DW_AT_name("INTx7")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$620, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$621, DW_AT_name("INTx8")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$621, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$622, DW_AT_name("rsvd1")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$622, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$52


$C$DW$T$53	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$53, DW_AT_name("PIEIER_REG")
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x01)
$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$623, DW_AT_name("all")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$624, DW_AT_name("bit")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$53


$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("PIEIFR_BITS")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x01)
$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$625, DW_AT_name("INTx1")
	.dwattr $C$DW$625, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$625, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$626, DW_AT_name("INTx2")
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$626, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$627	.dwtag  DW_TAG_member
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$627, DW_AT_name("INTx3")
	.dwattr $C$DW$627, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$627, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$628, DW_AT_name("INTx4")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$628, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$629, DW_AT_name("INTx5")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$629, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$630, DW_AT_name("INTx6")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$630, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$631, DW_AT_name("INTx7")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$631, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$632, DW_AT_name("INTx8")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$632, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$633, DW_AT_name("rsvd1")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$633, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$54


$C$DW$T$55	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$55, DW_AT_name("PIEIFR_REG")
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x01)
$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$634, DW_AT_name("all")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$635, DW_AT_name("bit")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$55


$C$DW$T$56	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$56, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x1a)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$636, DW_AT_name("PIECTRL")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$637	.dwtag  DW_TAG_member
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$637, DW_AT_name("PIEACK")
	.dwattr $C$DW$637, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$637, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$638, DW_AT_name("PIEIER1")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$639, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$640, DW_AT_name("PIEIER2")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$641, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$642, DW_AT_name("PIEIER3")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$643, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$644, DW_AT_name("PIEIER4")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$645, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$645, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$646, DW_AT_name("PIEIER5")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$647, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$648, DW_AT_name("PIEIER6")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$649, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$650, DW_AT_name("PIEIER7")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$651, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$652	.dwtag  DW_TAG_member
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$652, DW_AT_name("PIEIER8")
	.dwattr $C$DW$652, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$652, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$653, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$653, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$654, DW_AT_name("PIEIER9")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$655, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$656, DW_AT_name("PIEIER10")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$657, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$658, DW_AT_name("PIEIER11")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$659, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$660, DW_AT_name("PIEIER12")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$661, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56

$C$DW$662	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$56)
$C$DW$T$153	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$662)

$C$DW$T$58	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$58, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x04)
$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$663, DW_AT_name("next")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$664, DW_AT_name("prev")
	.dwattr $C$DW$664, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$58

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$57	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$57, DW_AT_address_class(0x16)

$C$DW$T$60	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$60, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x10)
$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$665, DW_AT_name("job")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$666, DW_AT_name("count")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$667, DW_AT_name("pendQ")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$668, DW_AT_name("name")
	.dwattr $C$DW$668, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$60

$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$155	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$155, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$155, DW_AT_address_class(0x16)
$C$DW$T$156	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$T$156, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$157	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$157, DW_AT_language(DW_LANG_C)

$C$DW$T$36	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$669	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$35)
	.dwendtag $C$DW$T$36

$C$DW$T$37	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_address_class(0x16)
$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)

$C$DW$T$91	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$91, DW_AT_language(DW_LANG_C)
$C$DW$670	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$90)
	.dwendtag $C$DW$T$91

$C$DW$T$92	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_address_class(0x16)
$C$DW$T$93	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)
$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$105	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$105, DW_AT_language(DW_LANG_C)
$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("post_sync_t")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$T$94	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)
$C$DW$T$96	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$96, DW_AT_language(DW_LANG_C)

$C$DW$T$100	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$671	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$90)
$C$DW$672	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$100

$C$DW$T$101	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_address_class(0x16)
$C$DW$T$133	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$133, DW_AT_language(DW_LANG_C)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)
$C$DW$T$106	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$106, DW_AT_language(DW_LANG_C)

$C$DW$T$124	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$124, DW_AT_language(DW_LANG_C)
$C$DW$673	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$90)
$C$DW$674	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$6)
$C$DW$675	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$9)
$C$DW$676	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$124

$C$DW$T$125	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_address_class(0x16)
$C$DW$T$126	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$677	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$677, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19


$C$DW$T$28	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x46)
$C$DW$678	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$678, DW_AT_upper_bound(0x45)
	.dwendtag $C$DW$T$28

$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x16)
$C$DW$679	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$6)
$C$DW$T$79	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$679)
$C$DW$T$80	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$80, DW_AT_address_class(0x16)
$C$DW$680	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$6)
$C$DW$T$180	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$180, DW_AT_type(*$C$DW$680)
$C$DW$T$127	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$127, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$98	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$98, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$181	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$181, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$181, DW_AT_language(DW_LANG_C)
$C$DW$681	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$9)
$C$DW$T$77	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$681)
$C$DW$T$78	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_address_class(0x16)
$C$DW$T$99	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$99, DW_AT_address_class(0x16)

$C$DW$T$190	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$190, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$190, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$190, DW_AT_byte_size(0x02)
$C$DW$682	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$682, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$190

$C$DW$T$61	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$81	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$683	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$6)
$C$DW$684	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$81

$C$DW$T$82	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_address_class(0x16)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)
$C$DW$T$97	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$97, DW_AT_address_class(0x16)

$C$DW$T$108	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)
$C$DW$685	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$90)
$C$DW$686	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$71)
$C$DW$687	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$6)
$C$DW$688	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$108

$C$DW$T$109	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$109, DW_AT_address_class(0x16)
$C$DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
$C$DW$689	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$110)
$C$DW$T$111	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$689)
$C$DW$T$112	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$112, DW_AT_address_class(0x16)
$C$DW$T$113	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x16)

$C$DW$T$117	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$690	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$90)
$C$DW$691	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$9)
$C$DW$692	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$117

$C$DW$T$118	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$118, DW_AT_address_class(0x16)
$C$DW$T$119	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$119, DW_AT_language(DW_LANG_C)
$C$DW$693	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$13)
$C$DW$T$200	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$200, DW_AT_type(*$C$DW$693)
$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$62	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$62, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)

$C$DW$T$202	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$202, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$202, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$202, DW_AT_byte_size(0x10)
$C$DW$694	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$694, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$202


$C$DW$T$63	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$63, DW_AT_name("crc_record")
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x08)
$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$695, DW_AT_name("crc_alg_ID")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_crc_alg_ID")
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$696, DW_AT_name("page_id")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_page_id")
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$697, DW_AT_name("addr")
	.dwattr $C$DW$697, DW_AT_TI_symbol_name("_addr")
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$698, DW_AT_name("size")
	.dwattr $C$DW$698, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$699, DW_AT_name("crc_value")
	.dwattr $C$DW$699, DW_AT_TI_symbol_name("_crc_value")
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$63

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_RECORD")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$65	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$65, DW_AT_byte_size(0x08)
$C$DW$700	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$700, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$65


$C$DW$T$66	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$66, DW_AT_name("crc_table")
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x0a)
$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$701, DW_AT_name("rec_size")
	.dwattr $C$DW$701, DW_AT_TI_symbol_name("_rec_size")
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$702, DW_AT_name("num_recs")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_num_recs")
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$703, DW_AT_name("recs")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_recs")
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$66

$C$DW$T$205	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_TABLE")
	.dwattr $C$DW$T$205, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$205, DW_AT_language(DW_LANG_C)

$C$DW$T$120	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$120, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$120, DW_AT_byte_size(0x01)
$C$DW$704	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$705	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$120

$C$DW$T$121	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$121, DW_AT_language(DW_LANG_C)

$C$DW$T$86	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$86, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$86, DW_AT_byte_size(0x01)
$C$DW$706	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$707	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$708	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$709	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$710	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$711	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$712	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$713	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$86

$C$DW$T$87	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$T$87, DW_AT_language(DW_LANG_C)

$C$DW$T$103	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$103, DW_AT_byte_size(0x80)
$C$DW$714	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$714, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$103


$C$DW$T$67	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$67, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x06)
$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$715, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$715, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$716	.dwtag  DW_TAG_member
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$716, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$716, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$717	.dwtag  DW_TAG_member
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$717, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$717, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$717, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$717, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$718	.dwtag  DW_TAG_member
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$718, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$718, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$719	.dwtag  DW_TAG_member
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$719, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$719, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$719, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$720	.dwtag  DW_TAG_member
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$720, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$720, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$720, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$67

$C$DW$T$74	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$74, DW_AT_language(DW_LANG_C)
$C$DW$721	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$74)
$C$DW$T$75	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$721)
$C$DW$T$76	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$T$76, DW_AT_address_class(0x16)

$C$DW$T$130	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$130, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x132)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$722, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$723, DW_AT_name("objdict")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$724, DW_AT_name("PDO_status")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$725, DW_AT_name("firstIndex")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$726, DW_AT_name("lastIndex")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$727, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$727, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$728, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$729, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$730, DW_AT_name("transfers")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$731, DW_AT_name("nodeState")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$732, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$733, DW_AT_name("initialisation")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$734, DW_AT_name("preOperational")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$735, DW_AT_name("operational")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$736, DW_AT_name("stopped")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$737, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$738, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$739, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$740, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$741, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$742, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$743, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$744, DW_AT_name("heartbeatError")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$745, DW_AT_name("NMTable")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$746, DW_AT_name("syncTimer")
	.dwattr $C$DW$746, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$747, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$748, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$749, DW_AT_name("post_sync")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_post_sync")
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$750, DW_AT_name("post_TPDO")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$751, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$752, DW_AT_name("toggle")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$753, DW_AT_name("canHandle")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$754, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$755, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$756, DW_AT_name("globalCallback")
	.dwattr $C$DW$756, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$757, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$758, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$759, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$760, DW_AT_name("dcf_request")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$761, DW_AT_name("error_state")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$762, DW_AT_name("error_history_size")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$763, DW_AT_name("error_number")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$764, DW_AT_name("error_first_element")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$765, DW_AT_name("error_register")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$766, DW_AT_name("error_cobid")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$767	.dwtag  DW_TAG_member
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$767, DW_AT_name("error_data")
	.dwattr $C$DW$767, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$767, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$768, DW_AT_name("post_emcy")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$769, DW_AT_name("lss_transfer")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$770, DW_AT_name("eeprom_index")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$771, DW_AT_name("eeprom_size")
	.dwattr $C$DW$771, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$130

$C$DW$T$89	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$T$89, DW_AT_language(DW_LANG_C)
$C$DW$T$90	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$90, DW_AT_address_class(0x16)

$C$DW$T$132	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$132, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$132, DW_AT_byte_size(0x0e)
$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$772, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$773, DW_AT_name("event_timer")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$774, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$775, DW_AT_name("last_message")
	.dwattr $C$DW$775, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$132

$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)
$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x16)

$C$DW$T$134	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$134, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$134, DW_AT_byte_size(0x14)
$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$776, DW_AT_name("nodeId")
	.dwattr $C$DW$776, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$777, DW_AT_name("whoami")
	.dwattr $C$DW$777, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$778, DW_AT_name("state")
	.dwattr $C$DW$778, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$779, DW_AT_name("toggle")
	.dwattr $C$DW$779, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$780, DW_AT_name("abortCode")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$781, DW_AT_name("index")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$782, DW_AT_name("subIndex")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$783, DW_AT_name("port")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$784, DW_AT_name("count")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$785, DW_AT_name("offset")
	.dwattr $C$DW$785, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$786, DW_AT_name("datap")
	.dwattr $C$DW$786, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$787	.dwtag  DW_TAG_member
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$787, DW_AT_name("dataType")
	.dwattr $C$DW$787, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$787, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$788, DW_AT_name("timer")
	.dwattr $C$DW$788, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$789, DW_AT_name("Callback")
	.dwattr $C$DW$789, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$134

$C$DW$T$84	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)

$C$DW$T$85	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x3c)
$C$DW$790	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$790, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$85


$C$DW$T$138	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$138, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$138, DW_AT_byte_size(0x04)
$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$791, DW_AT_name("pSubindex")
	.dwattr $C$DW$791, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$792, DW_AT_name("bSubCount")
	.dwattr $C$DW$792, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$793, DW_AT_name("index")
	.dwattr $C$DW$793, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$138

$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$794	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$69)
$C$DW$T$70	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$794)
$C$DW$T$71	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$71, DW_AT_address_class(0x16)

$C$DW$T$114	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$114, DW_AT_language(DW_LANG_C)
$C$DW$795	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$9)
$C$DW$796	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$796, DW_AT_type(*$C$DW$T$97)
$C$DW$797	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$113)
	.dwendtag $C$DW$T$114

$C$DW$T$115	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$115, DW_AT_address_class(0x16)
$C$DW$T$116	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$116, DW_AT_language(DW_LANG_C)

$C$DW$T$139	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$139, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$139, DW_AT_byte_size(0x08)
$C$DW$798	.dwtag  DW_TAG_member
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$798, DW_AT_name("bAccessType")
	.dwattr $C$DW$798, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$798, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$799	.dwtag  DW_TAG_member
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$799, DW_AT_name("bDataType")
	.dwattr $C$DW$799, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$799, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$800	.dwtag  DW_TAG_member
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$800, DW_AT_name("size")
	.dwattr $C$DW$800, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$800, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$800, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$801	.dwtag  DW_TAG_member
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$801, DW_AT_name("pObject")
	.dwattr $C$DW$801, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$801, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$801, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$802	.dwtag  DW_TAG_member
	.dwattr $C$DW$802, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$802, DW_AT_name("bProcessor")
	.dwattr $C$DW$802, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$802, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$802, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$139

$C$DW$803	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$803, DW_AT_type(*$C$DW$T$139)
$C$DW$T$135	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$803)
$C$DW$T$136	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$T$136, DW_AT_language(DW_LANG_C)
$C$DW$T$137	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$T$137, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$804	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$804, DW_AT_location[DW_OP_reg0]
$C$DW$805	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$805, DW_AT_location[DW_OP_reg1]
$C$DW$806	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$806, DW_AT_location[DW_OP_reg2]
$C$DW$807	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$807, DW_AT_location[DW_OP_reg3]
$C$DW$808	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$808, DW_AT_location[DW_OP_reg20]
$C$DW$809	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$809, DW_AT_location[DW_OP_reg21]
$C$DW$810	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$810, DW_AT_location[DW_OP_reg22]
$C$DW$811	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$811, DW_AT_location[DW_OP_reg23]
$C$DW$812	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$812, DW_AT_location[DW_OP_reg24]
$C$DW$813	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$813, DW_AT_location[DW_OP_reg25]
$C$DW$814	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$814, DW_AT_location[DW_OP_reg26]
$C$DW$815	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$815, DW_AT_location[DW_OP_reg28]
$C$DW$816	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$816, DW_AT_location[DW_OP_reg29]
$C$DW$817	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$817, DW_AT_location[DW_OP_reg30]
$C$DW$818	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$818, DW_AT_location[DW_OP_reg31]
$C$DW$819	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$819, DW_AT_location[DW_OP_regx 0x20]
$C$DW$820	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$820, DW_AT_location[DW_OP_regx 0x21]
$C$DW$821	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$821, DW_AT_location[DW_OP_regx 0x22]
$C$DW$822	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$822, DW_AT_location[DW_OP_regx 0x23]
$C$DW$823	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$823, DW_AT_location[DW_OP_regx 0x24]
$C$DW$824	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$824, DW_AT_location[DW_OP_regx 0x25]
$C$DW$825	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$825, DW_AT_location[DW_OP_regx 0x26]
$C$DW$826	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$826, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$827	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$827, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$828	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$828, DW_AT_location[DW_OP_reg4]
$C$DW$829	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$829, DW_AT_location[DW_OP_reg6]
$C$DW$830	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$830, DW_AT_location[DW_OP_reg8]
$C$DW$831	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$831, DW_AT_location[DW_OP_reg10]
$C$DW$832	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$832, DW_AT_location[DW_OP_reg12]
$C$DW$833	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$833, DW_AT_location[DW_OP_reg14]
$C$DW$834	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$834, DW_AT_location[DW_OP_reg16]
$C$DW$835	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$835, DW_AT_location[DW_OP_reg17]
$C$DW$836	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$836, DW_AT_location[DW_OP_reg18]
$C$DW$837	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$837, DW_AT_location[DW_OP_reg19]
$C$DW$838	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$838, DW_AT_location[DW_OP_reg5]
$C$DW$839	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$839, DW_AT_location[DW_OP_reg7]
$C$DW$840	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$840, DW_AT_location[DW_OP_reg9]
$C$DW$841	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$841, DW_AT_location[DW_OP_reg11]
$C$DW$842	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$842, DW_AT_location[DW_OP_reg13]
$C$DW$843	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$843, DW_AT_location[DW_OP_reg15]
$C$DW$844	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$844, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$845	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$845, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$846	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$846, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$847	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$847, DW_AT_location[DW_OP_regx 0x30]
$C$DW$848	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$848, DW_AT_location[DW_OP_regx 0x33]
$C$DW$849	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$849, DW_AT_location[DW_OP_regx 0x34]
$C$DW$850	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$850, DW_AT_location[DW_OP_regx 0x37]
$C$DW$851	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$851, DW_AT_location[DW_OP_regx 0x38]
$C$DW$852	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$852, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$853	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$853, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$854	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$854, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$855	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$855, DW_AT_location[DW_OP_regx 0x40]
$C$DW$856	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$856, DW_AT_location[DW_OP_regx 0x43]
$C$DW$857	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$857, DW_AT_location[DW_OP_regx 0x44]
$C$DW$858	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$858, DW_AT_location[DW_OP_regx 0x47]
$C$DW$859	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$859, DW_AT_location[DW_OP_regx 0x48]
$C$DW$860	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$860, DW_AT_location[DW_OP_regx 0x49]
$C$DW$861	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$861, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$862	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$862, DW_AT_location[DW_OP_regx 0x27]
$C$DW$863	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$863, DW_AT_location[DW_OP_regx 0x28]
$C$DW$864	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$864, DW_AT_location[DW_OP_reg27]
$C$DW$865	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$865, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

