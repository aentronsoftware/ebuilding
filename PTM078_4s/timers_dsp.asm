;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Mon Aug 31 10:04:26 2020                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\R_Module_48V_Standard\PTM078_4s")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_WaitTime+0,32
	.bits	0,32			; _WaitTime @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_TimeBuffer+0,32
	.bits	0,32			; _TimeBuffer @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("TimeDispatch")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_TimeDispatch")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_postBinary")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_SEM_postBinary")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$37)
	.dwendtag $C$DW$2


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pendBinary")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_SEM_pendBinary")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$37)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$44)
	.dwendtag $C$DW$4

	.global	_WaitTime
_WaitTime:	.usect	".ebss",2,1,1
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("WaitTime")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_WaitTime")
	.dwattr $C$DW$7, DW_AT_location[DW_OP_addr _WaitTime]
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$7, DW_AT_external
	.global	_TimeBuffer
_TimeBuffer:	.usect	".ebss",2,1,1
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("TimeBuffer")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_TimeBuffer")
	.dwattr $C$DW$8, DW_AT_location[DW_OP_addr _TimeBuffer]
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$8, DW_AT_external

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("CLK_getltime")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_CLK_getltime")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("CanFestival_mutex")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_CanFestival_mutex")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("CanTimer_sem")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_CanTimer_sem")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1220413 
	.sect	".text"
	.global	_EnterMutex

$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("EnterMutex")
	.dwattr $C$DW$12, DW_AT_low_pc(_EnterMutex)
	.dwattr $C$DW$12, DW_AT_high_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_EnterMutex")
	.dwattr $C$DW$12, DW_AT_external
	.dwattr $C$DW$12, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$12, DW_AT_TI_begin_line(0x0b)
	.dwattr $C$DW$12, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$12, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 12,column 1,is_stmt,address _EnterMutex

	.dwfde $C$DW$CIE, _EnterMutex

;***************************************************************
;* FNAME: _EnterMutex                   FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_EnterMutex:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 13,column 3,is_stmt
        MOVL      XAR4,#_CanFestival_mutex ; [CPU_U] |13| 
        MOV       AL,#65535             ; [CPU_] |13| 
$C$DW$13	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$13, DW_AT_low_pc(0x00)
	.dwattr $C$DW$13, DW_AT_name("_SEM_pendBinary")
	.dwattr $C$DW$13, DW_AT_TI_call
        LCR       #_SEM_pendBinary      ; [CPU_] |13| 
        ; call occurs [#_SEM_pendBinary] ; [] |13| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 14,column 1,is_stmt
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$12, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$12, DW_AT_TI_end_line(0x0e)
	.dwattr $C$DW$12, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$12

	.sect	".text"
	.global	_LeaveMutex

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("LeaveMutex")
	.dwattr $C$DW$15, DW_AT_low_pc(_LeaveMutex)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_LeaveMutex")
	.dwattr $C$DW$15, DW_AT_external
	.dwattr $C$DW$15, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0x10)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$15, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 17,column 1,is_stmt,address _LeaveMutex

	.dwfde $C$DW$CIE, _LeaveMutex

;***************************************************************
;* FNAME: _LeaveMutex                   FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_LeaveMutex:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 18,column 3,is_stmt
        MOVL      XAR4,#_CanFestival_mutex ; [CPU_U] |18| 
$C$DW$16	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$16, DW_AT_low_pc(0x00)
	.dwattr $C$DW$16, DW_AT_name("_SEM_postBinary")
	.dwattr $C$DW$16, DW_AT_TI_call
        LCR       #_SEM_postBinary      ; [CPU_] |18| 
        ; call occurs [#_SEM_postBinary] ; [] |18| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 19,column 1,is_stmt
$C$DW$17	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$17, DW_AT_low_pc(0x00)
	.dwattr $C$DW$17, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$15, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0x13)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$15

	.sect	".text"
	.global	_setTimer

$C$DW$18	.dwtag  DW_TAG_subprogram, DW_AT_name("setTimer")
	.dwattr $C$DW$18, DW_AT_low_pc(_setTimer)
	.dwattr $C$DW$18, DW_AT_high_pc(0x00)
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_setTimer")
	.dwattr $C$DW$18, DW_AT_external
	.dwattr $C$DW$18, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$18, DW_AT_TI_begin_line(0x16)
	.dwattr $C$DW$18, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$18, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 23,column 1,is_stmt,address _setTimer

	.dwfde $C$DW$CIE, _setTimer
$C$DW$19	.dwtag  DW_TAG_formal_parameter, DW_AT_name("value")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _setTimer                     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_setTimer:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("value")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],P             ; [CPU_] |23| 
        MOVL      *-SP[2],ACC           ; [CPU_] |23| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 24,column 3,is_stmt
        MOVL      XAR4,#_CanTimer_sem   ; [CPU_U] |24| 
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("_SEM_postBinary")
	.dwattr $C$DW$21, DW_AT_TI_call
        LCR       #_SEM_postBinary      ; [CPU_] |24| 
        ; call occurs [#_SEM_postBinary] ; [] |24| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 25,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$18, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$18, DW_AT_TI_end_line(0x19)
	.dwattr $C$DW$18, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$18

	.sect	".text"
	.global	_setTimer2

$C$DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("setTimer2")
	.dwattr $C$DW$23, DW_AT_low_pc(_setTimer2)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_setTimer2")
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0x1b)
	.dwattr $C$DW$23, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 28,column 1,is_stmt,address _setTimer2

	.dwfde $C$DW$CIE, _setTimer2
$C$DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_name("value")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _setTimer2                    FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_setTimer2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("value")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -8]
        MOVL      *-SP[8],P             ; [CPU_] |28| 
        MOVL      *-SP[6],ACC           ; [CPU_] |28| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 29,column 3,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |29| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |29| 
        MOV       *-SP[2],#0            ; [CPU_] |29| 
        MOV       *-SP[1],#0            ; [CPU_] |29| 
        MOVL      P,*-SP[8]             ; [CPU_] |29| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |29| 
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("ULL$$CMP")
	.dwattr $C$DW$26, DW_AT_TI_call
        FFC       XAR7,#ULL$$CMP        ; [CPU_] |29| 
        ; call occurs [#ULL$$CMP] ; [] |29| 
        CMPB      AL,#0                 ; [CPU_] |29| 
        B         $C$L1,LEQ             ; [CPU_] |29| 
        ; branchcc occurs ; [] |29| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |29| 
        MOVL      XAR5,*-SP[6]          ; [CPU_] |29| 
        B         $C$L2,UNC             ; [CPU_] |29| 
        ; branch occurs ; [] |29| 
$C$L1:    
        MOVB      XAR5,#0               ; [CPU_] |29| 
$C$L2:    
        MOVL      XAR6,#1000            ; [CPU_U] |29| 
        MOVL      *-SP[4],XAR6          ; [CPU_] |29| 
        MOVL      ACC,XAR5              ; [CPU_] |29| 
        MOVL      P,XAR4                ; [CPU_] |29| 
        MOV       *-SP[2],#0            ; [CPU_] |29| 
        MOV       *-SP[1],#0            ; [CPU_] |29| 
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("ULL$$DIV")
	.dwattr $C$DW$27, DW_AT_TI_call
        LCR       #ULL$$DIV             ; [CPU_] |29| 
        ; call occurs [#ULL$$DIV] ; [] |29| 
        MOVW      DP,#_WaitTime         ; [CPU_U] 
        MOVL      @_WaitTime,P          ; [CPU_] |29| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 30,column 3,is_stmt
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$28, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |30| 
        ; call occurs [#_CLK_getltime] ; [] |30| 
        MOVW      DP,#_TimeBuffer       ; [CPU_U] 
        MOVL      @_TimeBuffer,ACC      ; [CPU_] |30| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 31,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$23, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0x1f)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$23

	.sect	".text"
	.global	_getElapsedTime

$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("getElapsedTime")
	.dwattr $C$DW$30, DW_AT_low_pc(_getElapsedTime)
	.dwattr $C$DW$30, DW_AT_high_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_getElapsedTime")
	.dwattr $C$DW$30, DW_AT_external
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$30, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$30, DW_AT_TI_begin_line(0x21)
	.dwattr $C$DW$30, DW_AT_TI_begin_column(0x09)
	.dwattr $C$DW$30, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 34,column 1,is_stmt,address _getElapsedTime

	.dwfde $C$DW$CIE, _getElapsedTime

;***************************************************************
;* FNAME: _getElapsedTime               FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_getElapsedTime:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("tmp")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_tmp")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -2]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 37,column 3,is_stmt
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$32, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |37| 
        ; call occurs [#_CLK_getltime] ; [] |37| 
        MOVW      DP,#_TimeBuffer       ; [CPU_U] 
        SUBL      ACC,@_TimeBuffer      ; [CPU_] |37| 
        MOVL      *-SP[2],ACC           ; [CPU_] |37| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 38,column 3,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |38| 
        MOVL      XT,XAR4               ; [CPU_] |38| 
        IMPYXUL   P,XT,*-SP[2]          ; [CPU_] |38| 
        MOVB      ACC,#0                ; [CPU_] |38| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 39,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$30, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$30, DW_AT_TI_end_line(0x27)
	.dwattr $C$DW$30, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$30

	.sect	".text"
	.global	_timer_thread

$C$DW$34	.dwtag  DW_TAG_subprogram, DW_AT_name("timer_thread")
	.dwattr $C$DW$34, DW_AT_low_pc(_timer_thread)
	.dwattr $C$DW$34, DW_AT_high_pc(0x00)
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_timer_thread")
	.dwattr $C$DW$34, DW_AT_external
	.dwattr $C$DW$34, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$34, DW_AT_TI_begin_line(0x29)
	.dwattr $C$DW$34, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$34, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 42,column 1,is_stmt,address _timer_thread

	.dwfde $C$DW$CIE, _timer_thread

;***************************************************************
;* FNAME: _timer_thread                 FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_timer_thread:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 43,column 3,is_stmt
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$35, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |43| 
        ; call occurs [#_CLK_getltime] ; [] |43| 
        MOVW      DP,#_TimeBuffer       ; [CPU_U] 
        MOVL      @_TimeBuffer,ACC      ; [CPU_] |43| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 44,column 9,is_stmt
$C$L3:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 46,column 5,is_stmt
        MOVW      DP,#_WaitTime         ; [CPU_U] 
        MOVL      XAR4,#_CanTimer_sem   ; [CPU_U] |46| 
        MOV       AL,@_WaitTime         ; [CPU_] |46| 
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_name("_SEM_pendBinary")
	.dwattr $C$DW$36, DW_AT_TI_call
        LCR       #_SEM_pendBinary      ; [CPU_] |46| 
        ; call occurs [#_SEM_pendBinary] ; [] |46| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 47,column 5,is_stmt
$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x00)
	.dwattr $C$DW$37, DW_AT_name("_EnterMutex")
	.dwattr $C$DW$37, DW_AT_TI_call
        LCR       #_EnterMutex          ; [CPU_] |47| 
        ; call occurs [#_EnterMutex] ; [] |47| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 48,column 5,is_stmt
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("_TimeDispatch")
	.dwattr $C$DW$38, DW_AT_TI_call
        LCR       #_TimeDispatch        ; [CPU_] |48| 
        ; call occurs [#_TimeDispatch] ; [] |48| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 49,column 5,is_stmt
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("_LeaveMutex")
	.dwattr $C$DW$39, DW_AT_TI_call
        LCR       #_LeaveMutex          ; [CPU_] |49| 
        ; call occurs [#_LeaveMutex] ; [] |49| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c",line 44,column 9,is_stmt
        B         $C$L3,UNC             ; [CPU_] |44| 
        ; branch occurs ; [] |44| 
	.dwattr $C$DW$34, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/timers_dsp.c")
	.dwattr $C$DW$34, DW_AT_TI_end_line(0x33)
	.dwattr $C$DW$34, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$34

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_TimeDispatch
	.global	_SEM_postBinary
	.global	_SEM_pendBinary
	.global	_CLK_getltime
	.global	_CanFestival_mutex
	.global	_CanTimer_sem
	.global	ULL$$CMP
	.global	ULL$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x08)
$C$DW$40	.dwtag  DW_TAG_member
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$40, DW_AT_name("wListElem")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$40, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$40, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$41	.dwtag  DW_TAG_member
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$41, DW_AT_name("wCount")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$41, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$41, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$42	.dwtag  DW_TAG_member
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$42, DW_AT_name("fxn")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$42, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$42, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26

$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$21	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$21, DW_AT_address_class(0x16)
$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x04)
$C$DW$43	.dwtag  DW_TAG_member
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$43, DW_AT_name("next")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$43, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$44	.dwtag  DW_TAG_member
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$44, DW_AT_name("prev")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$44, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$44, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28

$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$27	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$27, DW_AT_address_class(0x16)

$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x10)
$C$DW$45	.dwtag  DW_TAG_member
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$45, DW_AT_name("job")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$45, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$45, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$46	.dwtag  DW_TAG_member
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$46, DW_AT_name("count")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$46, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$46, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$47	.dwtag  DW_TAG_member
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$47, DW_AT_name("pendQ")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$47, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$48	.dwtag  DW_TAG_member
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$48, DW_AT_name("name")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$48, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33

$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x16)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)

$C$DW$T$23	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$49	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$22)
	.dwendtag $C$DW$T$23

$C$DW$T$24	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_address_class(0x16)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("LgUns")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$31	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$31, DW_AT_address_class(0x16)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg0]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg1]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg2]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg3]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg20]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg21]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg22]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg23]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg24]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg25]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg26]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg28]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg29]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg30]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg31]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x20]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x21]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x22]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x23]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x24]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x25]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x26]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg4]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg6]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg8]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg10]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg12]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg14]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg16]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg17]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg18]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg19]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg5]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg7]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg9]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg11]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg13]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg15]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x30]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x33]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x34]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x37]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x38]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x40]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x43]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x44]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x47]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x48]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x49]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x27]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x28]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg27]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

