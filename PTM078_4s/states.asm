;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Mon Aug 31 10:04:24 2020                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\R_Module_48V_Standard\PTM078_4s")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_make10ms+0,32
	.bits	0,16			; _make10ms @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_CommTimeout+0,32
	.bits	0,32			; _CommTimeout @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("proceedNMTstateChange")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_proceedNMTstateChange")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$66)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$116)
	.dwendtag $C$DW$1


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("proceedNODE_GUARD")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_proceedNODE_GUARD")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$66)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$116)
	.dwendtag $C$DW$4


$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("heartbeatInit")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_heartbeatInit")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$7


$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("heartbeatStop")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_heartbeatStop")
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$9


$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("proceedEMCY")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_proceedEMCY")
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$66)
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$116)
	.dwendtag $C$DW$11


$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("PDOStop")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_PDOStop")
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$14


$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("startSYNC")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_startSYNC")
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$16


$C$DW$18	.dwtag  DW_TAG_subprogram, DW_AT_name("resetSDO")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_resetSDO")
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$18


$C$DW$20	.dwtag  DW_TAG_subprogram, DW_AT_name("PDOInit")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_PDOInit")
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external
$C$DW$21	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$20


$C$DW$22	.dwtag  DW_TAG_subprogram, DW_AT_name("emergencyInit")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_emergencyInit")
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$22


$C$DW$24	.dwtag  DW_TAG_subprogram, DW_AT_name("emergencyStop")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_emergencyStop")
	.dwattr $C$DW$24, DW_AT_declaration
	.dwattr $C$DW$24, DW_AT_external
$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$24


$C$DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("stopSYNC")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_stopSYNC")
	.dwattr $C$DW$26, DW_AT_declaration
	.dwattr $C$DW$26, DW_AT_external
$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$26

$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Module1_Alive_Counter")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ODV_Module1_Alive_Counter")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
	.global	_make10ms
_make10ms:	.usect	".ebss",1,1,0
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("make10ms")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_make10ms")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_addr _make10ms]
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$29, DW_AT_external

$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("proceedSYNC")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_proceedSYNC")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$30, DW_AT_declaration
	.dwattr $C$DW$30, DW_AT_external
$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$30


$C$DW$32	.dwtag  DW_TAG_subprogram, DW_AT_name("masterSendNMTstateChange")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_masterSendNMTstateChange")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$32, DW_AT_declaration
	.dwattr $C$DW$32, DW_AT_external
$C$DW$33	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$66)
$C$DW$34	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$6)
$C$DW$35	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$32


$C$DW$36	.dwtag  DW_TAG_subprogram, DW_AT_name("proceedSDO")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_proceedSDO")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
$C$DW$37	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$66)
$C$DW$38	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$116)
$C$DW$39	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$36


$C$DW$40	.dwtag  DW_TAG_subprogram, DW_AT_name("proceedPDO")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_proceedPDO")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external
$C$DW$41	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$66)
$C$DW$42	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$116)
	.dwendtag $C$DW$40


$C$DW$43	.dwtag  DW_TAG_subprogram, DW_AT_name("slaveSendBootUp")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_slaveSendBootUp")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$43

$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_State")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_ODV_Gateway_State")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external

$C$DW$46	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$46, DW_AT_declaration
	.dwattr $C$DW$46, DW_AT_external
$C$DW$47	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$122)
$C$DW$48	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$123)
$C$DW$49	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$34)
	.dwendtag $C$DW$46

$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SysTick_ms")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_ODV_SysTick_ms")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external
	.global	_CommTimeout
_CommTimeout:	.usect	".ebss",2,1,1
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("CommTimeout")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_CommTimeout")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_addr _CommTimeout]
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$51, DW_AT_external
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
	.sect	".econst"
	.align	1
_$P$T2$3:
	.bits	0,16			; _$P$T2$3._csBoot_Up @ 0
	.bits	1,16			; _$P$T2$3._csSDO @ 16
	.bits	1,16			; _$P$T2$3._csEmergency @ 32
	.bits	1,16			; _$P$T2$3._csSYNC @ 48
	.bits	1,16			; _$P$T2$3._csHeartbeat @ 64
	.bits	1,16			; _$P$T2$3._csPDO @ 80
	.bits	0,16			; _$P$T2$3._csLSS @ 96

$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("$P$T2$3")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_$P$T2$3")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_addr _$P$T2$3]
	.sect	".econst"
	.align	1
_$P$T3$4:
	.bits	0,16			; _$P$T3$4._csBoot_Up @ 0
	.bits	0,16			; _$P$T3$4._csSDO @ 16
	.bits	0,16			; _$P$T3$4._csEmergency @ 32
	.bits	0,16			; _$P$T3$4._csSYNC @ 48
	.bits	1,16			; _$P$T3$4._csHeartbeat @ 64
	.bits	0,16			; _$P$T3$4._csPDO @ 80
	.bits	1,16			; _$P$T3$4._csLSS @ 96

$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("$P$T3$4")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_$P$T3$4")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_addr _$P$T3$4]
	.sect	".econst"
	.align	1
_$P$T0$1:
	.bits	1,16			; _$P$T0$1._csBoot_Up @ 0
	.bits	0,16			; _$P$T0$1._csSDO @ 16
	.bits	0,16			; _$P$T0$1._csEmergency @ 32
	.bits	0,16			; _$P$T0$1._csSYNC @ 48
	.bits	0,16			; _$P$T0$1._csHeartbeat @ 64
	.bits	0,16			; _$P$T0$1._csPDO @ 80
	.bits	0,16			; _$P$T0$1._csLSS @ 96

$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("$P$T0$1")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_$P$T0$1")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_addr _$P$T0$1]
	.sect	".econst"
	.align	1
_$P$T1$2:
	.bits	0,16			; _$P$T1$2._csBoot_Up @ 0
	.bits	1,16			; _$P$T1$2._csSDO @ 16
	.bits	1,16			; _$P$T1$2._csEmergency @ 32
	.bits	1,16			; _$P$T1$2._csSYNC @ 48
	.bits	1,16			; _$P$T1$2._csHeartbeat @ 64
	.bits	0,16			; _$P$T1$2._csPDO @ 80
	.bits	1,16			; _$P$T1$2._csLSS @ 96

$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("$P$T1$2")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_$P$T1$2")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_addr _$P$T1$2]
	.sect	".econst:_$P$T4$5"
	.clink
	.align	2
_$P$T4$5:
	.bits	512,32			; _$P$T4$5[0] @ 0
	.bits	768,32			; _$P$T4$5[1] @ 32
	.bits	1024,32			; _$P$T4$5[2] @ 64
	.bits	1280,32			; _$P$T4$5[3] @ 96

$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("$P$T4$5")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_$P$T4$5")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_addr _$P$T4$5]
	.sect	".econst:_$P$T5$6"
	.clink
	.align	2
_$P$T5$6:
	.bits	384,32			; _$P$T5$6[0] @ 0
	.bits	640,32			; _$P$T5$6[1] @ 32
	.bits	896,32			; _$P$T5$6[2] @ 64
	.bits	1152,32			; _$P$T5$6[3] @ 96

$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("$P$T5$6")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_$P$T5$6")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_addr _$P$T5$6]
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("usb_tx_mbox")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_usb_tx_mbox")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$60, DW_AT_declaration
	.dwattr $C$DW$60, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0603212 
	.sect	".text"
	.global	_getState

$C$DW$61	.dwtag  DW_TAG_subprogram, DW_AT_name("getState")
	.dwattr $C$DW$61, DW_AT_low_pc(_getState)
	.dwattr $C$DW$61, DW_AT_high_pc(0x00)
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_getState")
	.dwattr $C$DW$61, DW_AT_external
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$61, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$61, DW_AT_TI_begin_line(0x35)
	.dwattr $C$DW$61, DW_AT_TI_begin_column(0x0d)
	.dwattr $C$DW$61, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 54,column 1,is_stmt,address _getState

	.dwfde $C$DW$CIE, _getState
$C$DW$62	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _getState                     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_getState:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |54| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 55,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |55| 
        MOVB      XAR0,#76              ; [CPU_] |55| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |55| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 56,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$64	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$64, DW_AT_low_pc(0x00)
	.dwattr $C$DW$64, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$61, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$61, DW_AT_TI_end_line(0x38)
	.dwattr $C$DW$61, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$61

	.sect	".text"
	.global	_canDispatch

$C$DW$65	.dwtag  DW_TAG_subprogram, DW_AT_name("canDispatch")
	.dwattr $C$DW$65, DW_AT_low_pc(_canDispatch)
	.dwattr $C$DW$65, DW_AT_high_pc(0x00)
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_canDispatch")
	.dwattr $C$DW$65, DW_AT_external
	.dwattr $C$DW$65, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$65, DW_AT_TI_begin_line(0x47)
	.dwattr $C$DW$65, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$65, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 72,column 1,is_stmt,address _canDispatch

	.dwfde $C$DW$CIE, _canDispatch
$C$DW$66	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg12]
$C$DW$67	.dwtag  DW_TAG_formal_parameter, DW_AT_name("m")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg14]
$C$DW$68	.dwtag  DW_TAG_formal_parameter, DW_AT_name("port")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _canDispatch                  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_canDispatch:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_breg20 -2]
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -4]
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("port")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_breg20 -5]
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("cob_id")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[5],AL            ; [CPU_] |72| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |72| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |72| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 73,column 15,is_stmt
        MOVL      XAR7,*-SP[4]          ; [CPU_] |73| 
        MOV       AL,*XAR7              ; [CPU_] |73| 
        MOV       *-SP[6],AL            ; [CPU_] |73| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 75,column 3,is_stmt
        B         $C$L13,UNC            ; [CPU_] |75| 
        ; branch occurs ; [] |75| 
$C$L1:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 78,column 4,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |78| 
        CMPB      AL,#128               ; [CPU_] |78| 
        BF        $C$L5,NEQ             ; [CPU_] |78| 
        ; branchcc occurs ; [] |78| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 80,column 5,is_stmt
        MOVW      DP,#_make10ms         ; [CPU_U] 
        MOV       AL,@_make10ms         ; [CPU_] |80| 
        CMPB      AL,#31                ; [CPU_] |80| 
        BF        $C$L3,NEQ             ; [CPU_] |80| 
        ; branchcc occurs ; [] |80| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 82,column 7,is_stmt
        MOVW      DP,#_ODV_Module1_Alive_Counter ; [CPU_U] 
        MOV       AL,@_ODV_Module1_Alive_Counter ; [CPU_] |82| 
        CMPB      AL,#15                ; [CPU_] |82| 
        B         $C$L2,LO              ; [CPU_] |82| 
        ; branchcc occurs ; [] |82| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 84,column 9,is_stmt
        MOV       @_ODV_Module1_Alive_Counter,#0 ; [CPU_] |84| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 85,column 9,is_stmt
        MOVW      DP,#_make10ms         ; [CPU_U] 
        MOV       @_make10ms,#0         ; [CPU_] |85| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 86,column 8,is_stmt
        B         $C$L4,UNC             ; [CPU_] |86| 
        ; branch occurs ; [] |86| 
$C$L2:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 89,column 9,is_stmt
        INC       @_ODV_Module1_Alive_Counter ; [CPU_] |89| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 90,column 9,is_stmt
        MOVW      DP,#_make10ms         ; [CPU_U] 
        MOV       @_make10ms,#0         ; [CPU_] |90| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 92,column 6,is_stmt
        B         $C$L4,UNC             ; [CPU_] |92| 
        ; branch occurs ; [] |92| 
$C$L3:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 95,column 7,is_stmt
        INC       @_make10ms            ; [CPU_] |95| 
$C$L4:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 98,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |98| 
        MOVB      XAR0,#80              ; [CPU_] |98| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |98| 
        BF        $C$L14,EQ             ; [CPU_] |98| 
        ; branchcc occurs ; [] |98| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 99,column 6,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |99| 
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_name("_proceedSYNC")
	.dwattr $C$DW$73, DW_AT_TI_call
        LCR       #_proceedSYNC         ; [CPU_] |99| 
        ; call occurs [#_proceedSYNC] ; [] |99| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 100,column 4,is_stmt
        B         $C$L14,UNC            ; [CPU_] |100| 
        ; branch occurs ; [] |100| 
$C$L5:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 100,column 11,is_stmt
        MOVZ      AR6,*-SP[6]           ; [CPU_] |100| 
        MOVB      ACC,#128              ; [CPU_] |100| 
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        ADDL      ACC,@_ODP_Board_RevisionNumber ; [CPU_] |100| 
        CMPL      ACC,XAR6              ; [CPU_] |100| 
        BF        $C$L6,NEQ             ; [CPU_] |100| 
        ; branchcc occurs ; [] |100| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 102,column 6,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |102| 
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,*+XAR4[3]          ; [CPU_] |102| 
        MOV       @_ODV_Gateway_State,AL ; [CPU_] |102| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 103,column 6,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |103| 
        MOVW      DP,#_CommTimeout      ; [CPU_U] 
        MOVL      @_CommTimeout,ACC     ; [CPU_] |103| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 106,column 7,is_stmt
        B         $C$L14,UNC            ; [CPU_] |106| 
        ; branch occurs ; [] |106| 
$C$L6:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 107,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |107| 
        MOVB      XAR0,#79              ; [CPU_] |107| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |107| 
        BF        $C$L14,EQ             ; [CPU_] |107| 
        ; branchcc occurs ; [] |107| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 108,column 6,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |108| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |108| 
$C$DW$74	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$74, DW_AT_low_pc(0x00)
	.dwattr $C$DW$74, DW_AT_name("_proceedEMCY")
	.dwattr $C$DW$74, DW_AT_TI_call
        LCR       #_proceedEMCY         ; [CPU_] |108| 
        ; call occurs [#_proceedEMCY] ; [] |108| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 109,column 4,is_stmt
        B         $C$L14,UNC            ; [CPU_] |109| 
        ; branch occurs ; [] |109| 
$C$L7:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 119,column 4,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |119| 
        MOVB      XAR0,#82              ; [CPU_] |119| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |119| 
        BF        $C$L14,EQ             ; [CPU_] |119| 
        ; branchcc occurs ; [] |119| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 120,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |120| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |120| 
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_name("_proceedPDO")
	.dwattr $C$DW$75, DW_AT_TI_call
        LCR       #_proceedPDO          ; [CPU_] |120| 
        ; call occurs [#_proceedPDO] ; [] |120| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 121,column 4,is_stmt
        B         $C$L14,UNC            ; [CPU_] |121| 
        ; branch occurs ; [] |121| 
$C$L8:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 123,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |123| 
        MOVB      XAR0,#78              ; [CPU_] |123| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |123| 
        BF        $C$L14,EQ             ; [CPU_] |123| 
        ; branchcc occurs ; [] |123| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 124,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |124| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |124| 
        MOV       AL,*-SP[5]            ; [CPU_] |124| 
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_name("_proceedSDO")
	.dwattr $C$DW$76, DW_AT_TI_call
        LCR       #_proceedSDO          ; [CPU_] |124| 
        ; call occurs [#_proceedSDO] ; [] |124| 
        CMPB      AL,#15                ; [CPU_] |124| 
        BF        $C$L14,NEQ            ; [CPU_] |124| 
        ; branchcc occurs ; [] |124| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 125,column 11,is_stmt
        MOVL      XAR5,*-SP[4]          ; [CPU_] |125| 
        MOVL      XAR4,#_usb_tx_mbox    ; [CPU_U] |125| 
        MOVB      AL,#0                 ; [CPU_] |125| 
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("_MBX_post")
	.dwattr $C$DW$77, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |125| 
        ; call occurs [#_MBX_post] ; [] |125| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 126,column 7,is_stmt
        B         $C$L14,UNC            ; [CPU_] |126| 
        ; branch occurs ; [] |126| 
$C$L9:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 129,column 6,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |129| 
        MOVB      XAR0,#78              ; [CPU_] |129| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |129| 
        BF        $C$L14,EQ             ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 130,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |130| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |130| 
        MOV       AL,*-SP[5]            ; [CPU_] |130| 
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_name("_proceedSDO")
	.dwattr $C$DW$78, DW_AT_TI_call
        LCR       #_proceedSDO          ; [CPU_] |130| 
        ; call occurs [#_proceedSDO] ; [] |130| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 131,column 4,is_stmt
        B         $C$L14,UNC            ; [CPU_] |131| 
        ; branch occurs ; [] |131| 
$C$L10:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 133,column 4,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |133| 
        MOVB      XAR0,#81              ; [CPU_] |133| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |133| 
        BF        $C$L11,EQ             ; [CPU_] |133| 
        ; branchcc occurs ; [] |133| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 134,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |134| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |134| 
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_name("_proceedNODE_GUARD")
	.dwattr $C$DW$79, DW_AT_TI_call
        LCR       #_proceedNODE_GUARD   ; [CPU_] |134| 
        ; call occurs [#_proceedNODE_GUARD] ; [] |134| 
$C$L11:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 135,column 4,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |135| 
        MOVB      XAR0,#12              ; [CPU_] |135| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |135| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |135| 
        BF        $C$L14,NEQ            ; [CPU_] |135| 
        ; branchcc occurs ; [] |135| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 136,column 6,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        AND       AL,*-SP[6],#0x007f    ; [CPU_] |136| 
        MOVB      AH,#1                 ; [CPU_] |136| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |136| 
$C$DW$80	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$80, DW_AT_low_pc(0x00)
	.dwattr $C$DW$80, DW_AT_name("_masterSendNMTstateChange")
	.dwattr $C$DW$80, DW_AT_TI_call
        LCR       #_masterSendNMTstateChange ; [CPU_] |136| 
        ; call occurs [#_masterSendNMTstateChange] ; [] |136| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 137,column 4,is_stmt
        B         $C$L14,UNC            ; [CPU_] |137| 
        ; branch occurs ; [] |137| 
$C$L12:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 139,column 4,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |139| 
        MOVB      XAR0,#12              ; [CPU_] |139| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |139| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |139| 
        BF        $C$L14,EQ             ; [CPU_] |139| 
        ; branchcc occurs ; [] |139| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 141,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |141| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |141| 
$C$DW$81	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$81, DW_AT_low_pc(0x00)
	.dwattr $C$DW$81, DW_AT_name("_proceedNMTstateChange")
	.dwattr $C$DW$81, DW_AT_TI_call
        LCR       #_proceedNMTstateChange ; [CPU_] |141| 
        ; call occurs [#_proceedNMTstateChange] ; [] |141| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 156,column 2,is_stmt
        B         $C$L14,UNC            ; [CPU_] |156| 
        ; branch occurs ; [] |156| 
$C$L13:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 75,column 3,is_stmt
        LSR       AL,7                  ; [CPU_] |75| 
        CMPB      AL,#14                ; [CPU_] |75| 
        B         $C$L14,HI             ; [CPU_] |75| 
        ; branchcc occurs ; [] |75| 
        MOV       ACC,AL << #1          ; [CPU_] |75| 
        MOVL      XAR7,#$C$SW1          ; [CPU_U] |75| 
        MOVZ      AR6,AL                ; [CPU_] |75| 
        MOVL      ACC,XAR7              ; [CPU_] |75| 
        ADDU      ACC,AR6               ; [CPU_] |75| 
        MOVL      XAR7,ACC              ; [CPU_] |75| 
        MOVL      XAR7,*XAR7            ; [CPU_] |75| 
        LB        *XAR7                 ; [CPU_] |75| 
        ; branch occurs ; [] |75| 
	.sect	".switch:_canDispatch"
	.clink
$C$SW1:	.long	$C$L12	; 0
	.long	$C$L1	; 1
	.long	$C$L14	; 0
	.long	$C$L7	; 3
	.long	$C$L7	; 4
	.long	$C$L7	; 5
	.long	$C$L7	; 6
	.long	$C$L7	; 7
	.long	$C$L7	; 8
	.long	$C$L7	; 9
	.long	$C$L7	; 10
	.long	$C$L8	; 11
	.long	$C$L9	; 12
	.long	$C$L14	; 0
	.long	$C$L10	; 14
	.sect	".text"
$C$L14:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$82	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$82, DW_AT_low_pc(0x00)
	.dwattr $C$DW$82, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$65, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$65, DW_AT_TI_end_line(0x9d)
	.dwattr $C$DW$65, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$65

	.sect	".text"
	.global	_switchCommunicationState

$C$DW$83	.dwtag  DW_TAG_subprogram, DW_AT_name("switchCommunicationState")
	.dwattr $C$DW$83, DW_AT_low_pc(_switchCommunicationState)
	.dwattr $C$DW$83, DW_AT_high_pc(0x00)
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_switchCommunicationState")
	.dwattr $C$DW$83, DW_AT_external
	.dwattr $C$DW$83, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$83, DW_AT_TI_begin_line(0xb1)
	.dwattr $C$DW$83, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$83, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 178,column 1,is_stmt,address _switchCommunicationState

	.dwfde $C$DW$CIE, _switchCommunicationState
$C$DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg12]
$C$DW$85	.dwtag  DW_TAG_formal_parameter, DW_AT_name("newCommunicationState")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_newCommunicationState")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _switchCommunicationState     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_switchCommunicationState:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -2]
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("newCommunicationState")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_newCommunicationState")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR5          ; [CPU_] |178| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |178| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 182,column 2,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |182| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |182| 
        BF        $C$L15,EQ             ; [CPU_] |182| 
        ; branchcc occurs ; [] |182| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |182| 
        MOVB      XAR0,#78              ; [CPU_] |182| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |182| 
        BF        $C$L15,NEQ            ; [CPU_] |182| 
        ; branchcc occurs ; [] |182| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |182| 
        MOVB      XAR0,#78              ; [CPU_] |182| 
        MOVB      *+XAR4[AR0],#1,UNC    ; [CPU_] |182| 
        B         $C$L16,UNC            ; [CPU_] |182| 
        ; branch occurs ; [] |182| 
$C$L15:    
        MOVL      XAR4,*-SP[4]          ; [CPU_] |182| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |182| 
        BF        $C$L16,NEQ            ; [CPU_] |182| 
        ; branchcc occurs ; [] |182| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |182| 
        MOVB      XAR0,#78              ; [CPU_] |182| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |182| 
        CMPB      AL,#1                 ; [CPU_] |182| 
        BF        $C$L16,NEQ            ; [CPU_] |182| 
        ; branchcc occurs ; [] |182| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |182| 
        MOVB      XAR0,#78              ; [CPU_] |182| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |182| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |182| 
$C$DW$88	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$88, DW_AT_low_pc(0x00)
	.dwattr $C$DW$88, DW_AT_name("_resetSDO")
	.dwattr $C$DW$88, DW_AT_TI_call
        LCR       #_resetSDO            ; [CPU_] |182| 
        ; call occurs [#_resetSDO] ; [] |182| 
$C$L16:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 183,column 2,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |183| 
        MOV       AL,*+XAR4[3]          ; [CPU_] |183| 
        BF        $C$L17,EQ             ; [CPU_] |183| 
        ; branchcc occurs ; [] |183| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |183| 
        MOVB      XAR0,#80              ; [CPU_] |183| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |183| 
        BF        $C$L17,NEQ            ; [CPU_] |183| 
        ; branchcc occurs ; [] |183| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |183| 
        MOVB      XAR0,#80              ; [CPU_] |183| 
        MOVB      *+XAR4[AR0],#1,UNC    ; [CPU_] |183| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |183| 
$C$DW$89	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$89, DW_AT_low_pc(0x00)
	.dwattr $C$DW$89, DW_AT_name("_startSYNC")
	.dwattr $C$DW$89, DW_AT_TI_call
        LCR       #_startSYNC           ; [CPU_] |183| 
        ; call occurs [#_startSYNC] ; [] |183| 
        B         $C$L18,UNC            ; [CPU_] |183| 
        ; branch occurs ; [] |183| 
$C$L17:    
        MOVL      XAR4,*-SP[4]          ; [CPU_] |183| 
        MOV       AL,*+XAR4[3]          ; [CPU_] |183| 
        BF        $C$L18,NEQ            ; [CPU_] |183| 
        ; branchcc occurs ; [] |183| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |183| 
        MOVB      XAR0,#80              ; [CPU_] |183| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |183| 
        CMPB      AL,#1                 ; [CPU_] |183| 
        BF        $C$L18,NEQ            ; [CPU_] |183| 
        ; branchcc occurs ; [] |183| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |183| 
        MOVB      XAR0,#80              ; [CPU_] |183| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |183| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |183| 
$C$DW$90	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$90, DW_AT_low_pc(0x00)
	.dwattr $C$DW$90, DW_AT_name("_stopSYNC")
	.dwattr $C$DW$90, DW_AT_TI_call
        LCR       #_stopSYNC            ; [CPU_] |183| 
        ; call occurs [#_stopSYNC] ; [] |183| 
$C$L18:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 184,column 2,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |184| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |184| 
        BF        $C$L19,EQ             ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |184| 
        MOVB      XAR0,#81              ; [CPU_] |184| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |184| 
        BF        $C$L19,NEQ            ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |184| 
        MOVB      XAR0,#81              ; [CPU_] |184| 
        MOVB      *+XAR4[AR0],#1,UNC    ; [CPU_] |184| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |184| 
$C$DW$91	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$91, DW_AT_low_pc(0x00)
	.dwattr $C$DW$91, DW_AT_name("_heartbeatInit")
	.dwattr $C$DW$91, DW_AT_TI_call
        LCR       #_heartbeatInit       ; [CPU_] |184| 
        ; call occurs [#_heartbeatInit] ; [] |184| 
        B         $C$L20,UNC            ; [CPU_] |184| 
        ; branch occurs ; [] |184| 
$C$L19:    
        MOVL      XAR4,*-SP[4]          ; [CPU_] |184| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |184| 
        BF        $C$L20,NEQ            ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |184| 
        MOVB      XAR0,#81              ; [CPU_] |184| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |184| 
        CMPB      AL,#1                 ; [CPU_] |184| 
        BF        $C$L20,NEQ            ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |184| 
        MOVB      XAR0,#81              ; [CPU_] |184| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |184| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |184| 
$C$DW$92	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$92, DW_AT_low_pc(0x00)
	.dwattr $C$DW$92, DW_AT_name("_heartbeatStop")
	.dwattr $C$DW$92, DW_AT_TI_call
        LCR       #_heartbeatStop       ; [CPU_] |184| 
        ; call occurs [#_heartbeatStop] ; [] |184| 
$C$L20:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 185,column 2,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |185| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |185| 
        BF        $C$L21,EQ             ; [CPU_] |185| 
        ; branchcc occurs ; [] |185| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |185| 
        MOVB      XAR0,#79              ; [CPU_] |185| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |185| 
        BF        $C$L21,NEQ            ; [CPU_] |185| 
        ; branchcc occurs ; [] |185| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |185| 
        MOVB      XAR0,#79              ; [CPU_] |185| 
        MOVB      *+XAR4[AR0],#1,UNC    ; [CPU_] |185| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |185| 
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("_emergencyInit")
	.dwattr $C$DW$93, DW_AT_TI_call
        LCR       #_emergencyInit       ; [CPU_] |185| 
        ; call occurs [#_emergencyInit] ; [] |185| 
        B         $C$L22,UNC            ; [CPU_] |185| 
        ; branch occurs ; [] |185| 
$C$L21:    
        MOVL      XAR4,*-SP[4]          ; [CPU_] |185| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |185| 
        BF        $C$L22,NEQ            ; [CPU_] |185| 
        ; branchcc occurs ; [] |185| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |185| 
        MOVB      XAR0,#79              ; [CPU_] |185| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |185| 
        CMPB      AL,#1                 ; [CPU_] |185| 
        BF        $C$L22,NEQ            ; [CPU_] |185| 
        ; branchcc occurs ; [] |185| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |185| 
        MOVB      XAR0,#79              ; [CPU_] |185| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |185| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |185| 
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("_emergencyStop")
	.dwattr $C$DW$94, DW_AT_TI_call
        LCR       #_emergencyStop       ; [CPU_] |185| 
        ; call occurs [#_emergencyStop] ; [] |185| 
$C$L22:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 186,column 2,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |186| 
        MOV       AL,*+XAR4[5]          ; [CPU_] |186| 
        BF        $C$L23,EQ             ; [CPU_] |186| 
        ; branchcc occurs ; [] |186| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |186| 
        MOVB      XAR0,#82              ; [CPU_] |186| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |186| 
        BF        $C$L23,NEQ            ; [CPU_] |186| 
        ; branchcc occurs ; [] |186| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |186| 
        MOVB      XAR0,#82              ; [CPU_] |186| 
        MOVB      *+XAR4[AR0],#1,UNC    ; [CPU_] |186| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |186| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("_PDOInit")
	.dwattr $C$DW$95, DW_AT_TI_call
        LCR       #_PDOInit             ; [CPU_] |186| 
        ; call occurs [#_PDOInit] ; [] |186| 
        B         $C$L24,UNC            ; [CPU_] |186| 
        ; branch occurs ; [] |186| 
$C$L23:    
        MOVL      XAR4,*-SP[4]          ; [CPU_] |186| 
        MOV       AL,*+XAR4[5]          ; [CPU_] |186| 
        BF        $C$L24,NEQ            ; [CPU_] |186| 
        ; branchcc occurs ; [] |186| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |186| 
        MOVB      XAR0,#82              ; [CPU_] |186| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |186| 
        CMPB      AL,#1                 ; [CPU_] |186| 
        BF        $C$L24,NEQ            ; [CPU_] |186| 
        ; branchcc occurs ; [] |186| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |186| 
        MOVB      XAR0,#82              ; [CPU_] |186| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |186| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |186| 
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_name("_PDOStop")
	.dwattr $C$DW$96, DW_AT_TI_call
        LCR       #_PDOStop             ; [CPU_] |186| 
        ; call occurs [#_PDOStop] ; [] |186| 
$C$L24:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 187,column 2,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |187| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |187| 
        BF        $C$L25,EQ             ; [CPU_] |187| 
        ; branchcc occurs ; [] |187| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |187| 
        MOVB      XAR0,#77              ; [CPU_] |187| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |187| 
        BF        $C$L25,NEQ            ; [CPU_] |187| 
        ; branchcc occurs ; [] |187| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |187| 
        MOVB      XAR0,#77              ; [CPU_] |187| 
        MOVB      *+XAR4[AR0],#1,UNC    ; [CPU_] |187| 
        B         $C$L26,UNC            ; [CPU_] |187| 
        ; branch occurs ; [] |187| 
$C$L25:    
        MOVL      XAR4,*-SP[4]          ; [CPU_] |187| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |187| 
        BF        $C$L26,NEQ            ; [CPU_] |187| 
        ; branchcc occurs ; [] |187| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |187| 
        MOVB      XAR0,#77              ; [CPU_] |187| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |187| 
        CMPB      AL,#1                 ; [CPU_] |187| 
        BF        $C$L26,NEQ            ; [CPU_] |187| 
        ; branchcc occurs ; [] |187| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |187| 
        MOVB      XAR0,#77              ; [CPU_] |187| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |187| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |187| 
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_name("_slaveSendBootUp")
	.dwattr $C$DW$97, DW_AT_TI_call
        LCR       #_slaveSendBootUp     ; [CPU_] |187| 
        ; call occurs [#_slaveSendBootUp] ; [] |187| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 188,column 1,is_stmt
$C$L26:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$98	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$98, DW_AT_low_pc(0x00)
	.dwattr $C$DW$98, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$83, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$83, DW_AT_TI_end_line(0xbc)
	.dwattr $C$DW$83, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$83

	.sect	".text"
	.global	_setState

$C$DW$99	.dwtag  DW_TAG_subprogram, DW_AT_name("setState")
	.dwattr $C$DW$99, DW_AT_low_pc(_setState)
	.dwattr $C$DW$99, DW_AT_high_pc(0x00)
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_setState")
	.dwattr $C$DW$99, DW_AT_external
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$99, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$99, DW_AT_TI_begin_line(0xc6)
	.dwattr $C$DW$99, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$99, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 199,column 1,is_stmt,address _setState

	.dwfde $C$DW$CIE, _setState
$C$DW$100	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_reg12]
$C$DW$101	.dwtag  DW_TAG_formal_parameter, DW_AT_name("newState")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_newState")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _setState                     FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_setState:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_breg20 -2]
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("newState")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_newState")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AL            ; [CPU_] |199| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |199| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 202,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |202| 
        MOVB      XAR0,#76              ; [CPU_] |202| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |202| 
        CMP       AL,*-SP[3]            ; [CPU_] |202| 
        BF        $C$L35,EQ             ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 203,column 3,is_stmt
        B         $C$L34,UNC            ; [CPU_] |203| 
        ; branch occurs ; [] |203| 
$C$L27:    

$C$DW$104	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("newCommunicationState")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_newCommunicationState")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -10]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 206,column 27,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |206| 
        MOVL      XAR7,#_$P$T0$1        ; [CPU_U] |206| 
        SUBB      XAR4,#10              ; [CPU_U] |206| 
        RPT       #6
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |206| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 207,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |207| 
        MOVB      XAR0,#76              ; [CPU_] |207| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |207| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 208,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |208| 
        MOVZ      AR5,SP                ; [CPU_U] |208| 
        SUBB      XAR5,#10              ; [CPU_U] |208| 
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("_switchCommunicationState")
	.dwattr $C$DW$106, DW_AT_TI_call
        LCR       #_switchCommunicationState ; [CPU_] |208| 
        ; call occurs [#_switchCommunicationState] ; [] |208| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 211,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |211| 
        MOVB      XAR0,#84              ; [CPU_] |211| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |211| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |211| 
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_TI_call
	.dwattr $C$DW$107, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |211| 
        ; call occurs [XAR7] ; [] |211| 
	.dwendtag $C$DW$104

$C$L28:    

$C$DW$108	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$109	.dwtag  DW_TAG_variable, DW_AT_name("newCommunicationState")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_newCommunicationState")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_breg20 -10]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 222,column 27,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |222| 
        MOVL      XAR7,#_$P$T1$2        ; [CPU_U] |222| 
        SUBB      XAR4,#10              ; [CPU_U] |222| 
        RPT       #6
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |222| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 223,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |223| 
        MOVB      XAR0,#76              ; [CPU_] |223| 
        MOVB      *+XAR4[AR0],#127,UNC  ; [CPU_] |223| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 225,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |225| 
        MOVZ      AR5,SP                ; [CPU_U] |225| 
        SUBB      XAR5,#10              ; [CPU_U] |225| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_switchCommunicationState")
	.dwattr $C$DW$110, DW_AT_TI_call
        LCR       #_switchCommunicationState ; [CPU_] |225| 
        ; call occurs [#_switchCommunicationState] ; [] |225| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 226,column 5,is_stmt
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 230,column 17,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |230| 
        MOVB      XAR0,#86              ; [CPU_] |230| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |230| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |230| 
$C$DW$111	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$111, DW_AT_low_pc(0x00)
	.dwattr $C$DW$111, DW_AT_TI_call
	.dwattr $C$DW$111, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |230| 
        ; call occurs [XAR7] ; [] |230| 
	.dwendtag $C$DW$108

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 232,column 4,is_stmt
        B         $C$L35,UNC            ; [CPU_] |232| 
        ; branch occurs ; [] |232| 
$C$L29:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 235,column 4,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |235| 
        MOVB      XAR0,#76              ; [CPU_] |235| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |235| 
        BF        $C$L30,NEQ            ; [CPU_] |235| 
        ; branchcc occurs ; [] |235| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 235,column 39,is_stmt
        MOVB      AL,#255               ; [CPU_] |235| 
        B         $C$L36,UNC            ; [CPU_] |235| 
        ; branch occurs ; [] |235| 
$C$L30:    

$C$DW$112	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$113	.dwtag  DW_TAG_variable, DW_AT_name("newCommunicationState")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_newCommunicationState")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$113, DW_AT_location[DW_OP_breg20 -10]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 237,column 27,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |237| 
        MOVL      XAR7,#_$P$T2$3        ; [CPU_U] |237| 
        SUBB      XAR4,#10              ; [CPU_U] |237| 
        RPT       #6
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |237| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 238,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |238| 
        MOVB      XAR0,#76              ; [CPU_] |238| 
        MOVB      *+XAR4[AR0],#5,UNC    ; [CPU_] |238| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 240,column 17,is_stmt
        MOVB      *-SP[3],#5,UNC        ; [CPU_] |240| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 241,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |241| 
        MOVZ      AR5,SP                ; [CPU_U] |241| 
        SUBB      XAR5,#10              ; [CPU_U] |241| 
$C$DW$114	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$114, DW_AT_low_pc(0x00)
	.dwattr $C$DW$114, DW_AT_name("_switchCommunicationState")
	.dwattr $C$DW$114, DW_AT_TI_call
        LCR       #_switchCommunicationState ; [CPU_] |241| 
        ; call occurs [#_switchCommunicationState] ; [] |241| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 242,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |242| 
        MOVB      XAR0,#88              ; [CPU_] |242| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |242| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |242| 
$C$DW$115	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$115, DW_AT_low_pc(0x00)
	.dwattr $C$DW$115, DW_AT_TI_call
	.dwattr $C$DW$115, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |242| 
        ; call occurs [XAR7] ; [] |242| 
	.dwendtag $C$DW$112

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 244,column 4,is_stmt
        B         $C$L35,UNC            ; [CPU_] |244| 
        ; branch occurs ; [] |244| 
$C$L31:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 247,column 4,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |247| 
        MOVB      XAR0,#76              ; [CPU_] |247| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |247| 
        BF        $C$L32,NEQ            ; [CPU_] |247| 
        ; branchcc occurs ; [] |247| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 247,column 39,is_stmt
        MOVB      AL,#255               ; [CPU_] |247| 
        B         $C$L36,UNC            ; [CPU_] |247| 
        ; branch occurs ; [] |247| 
$C$L32:    

$C$DW$116	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$117	.dwtag  DW_TAG_variable, DW_AT_name("newCommunicationState")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_newCommunicationState")
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$117, DW_AT_location[DW_OP_breg20 -10]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 249,column 27,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |249| 
        MOVL      XAR7,#_$P$T3$4        ; [CPU_U] |249| 
        SUBB      XAR4,#10              ; [CPU_U] |249| 
        RPT       #6
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |249| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 250,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |250| 
        MOVB      XAR0,#76              ; [CPU_] |250| 
        MOVB      *+XAR4[AR0],#4,UNC    ; [CPU_] |250| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 252,column 5,is_stmt
        MOVB      *-SP[3],#4,UNC        ; [CPU_] |252| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 253,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |253| 
        MOVZ      AR5,SP                ; [CPU_U] |253| 
        SUBB      XAR5,#10              ; [CPU_U] |253| 
$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_name("_switchCommunicationState")
	.dwattr $C$DW$118, DW_AT_TI_call
        LCR       #_switchCommunicationState ; [CPU_] |253| 
        ; call occurs [#_switchCommunicationState] ; [] |253| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 254,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |254| 
        MOVB      XAR0,#90              ; [CPU_] |254| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |254| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |254| 
$C$DW$119	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$119, DW_AT_low_pc(0x00)
	.dwattr $C$DW$119, DW_AT_TI_call
	.dwattr $C$DW$119, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |254| 
        ; call occurs [XAR7] ; [] |254| 
	.dwendtag $C$DW$116

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 256,column 4,is_stmt
        B         $C$L35,UNC            ; [CPU_] |256| 
        ; branch occurs ; [] |256| 
$C$L33:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 258,column 5,is_stmt
        MOVB      AL,#255               ; [CPU_] |258| 
        B         $C$L36,UNC            ; [CPU_] |258| 
        ; branch occurs ; [] |258| 
$C$L34:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 203,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |203| 
        BF        $C$L27,EQ             ; [CPU_] |203| 
        ; branchcc occurs ; [] |203| 
        CMPB      AL,#4                 ; [CPU_] |203| 
        BF        $C$L31,EQ             ; [CPU_] |203| 
        ; branchcc occurs ; [] |203| 
        CMPB      AL,#5                 ; [CPU_] |203| 
        BF        $C$L29,EQ             ; [CPU_] |203| 
        ; branchcc occurs ; [] |203| 
        CMPB      AL,#127               ; [CPU_] |203| 
        BF        $C$L28,EQ             ; [CPU_] |203| 
        ; branchcc occurs ; [] |203| 
        B         $C$L33,UNC            ; [CPU_] |203| 
        ; branch occurs ; [] |203| 
$C$L35:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 265,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |265| 
        MOVB      XAR0,#76              ; [CPU_] |265| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |265| 
$C$L36:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 266,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$99, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$99, DW_AT_TI_end_line(0x10a)
	.dwattr $C$DW$99, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$99

	.sect	".text"
	.global	_getNodeId

$C$DW$121	.dwtag  DW_TAG_subprogram, DW_AT_name("getNodeId")
	.dwattr $C$DW$121, DW_AT_low_pc(_getNodeId)
	.dwattr $C$DW$121, DW_AT_high_pc(0x00)
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_getNodeId")
	.dwattr $C$DW$121, DW_AT_external
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$121, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$121, DW_AT_TI_begin_line(0x113)
	.dwattr $C$DW$121, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$121, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 276,column 1,is_stmt,address _getNodeId

	.dwfde $C$DW$CIE, _getNodeId
$C$DW$122	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _getNodeId                    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_getNodeId:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$123, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |276| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 277,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |277| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |277| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |277| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 278,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$121, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$121, DW_AT_TI_end_line(0x116)
	.dwattr $C$DW$121, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$121

	.sect	".text"
	.global	_setNodeId

$C$DW$125	.dwtag  DW_TAG_subprogram, DW_AT_name("setNodeId")
	.dwattr $C$DW$125, DW_AT_low_pc(_setNodeId)
	.dwattr $C$DW$125, DW_AT_high_pc(0x00)
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_setNodeId")
	.dwattr $C$DW$125, DW_AT_external
	.dwattr $C$DW$125, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$125, DW_AT_TI_begin_line(0x11e)
	.dwattr $C$DW$125, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$125, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 287,column 1,is_stmt,address _setNodeId

	.dwfde $C$DW$CIE, _setNodeId
$C$DW$126	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_reg12]
$C$DW$127	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nodeId")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _setNodeId                    FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 16 Auto,  0 SOE     *
;***************************************************************

_setNodeId:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_breg20 -2]
$C$DW$129	.dwtag  DW_TAG_variable, DW_AT_name("nodeId")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_breg20 -3]
$C$DW$130	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$130, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[3],AL            ; [CPU_] |287| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |287| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 288,column 16,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |288| 
        MOVL      XAR7,*+XAR4[6]        ; [CPU_] |288| 
        MOV       AL,*XAR7              ; [CPU_] |288| 
        MOV       *-SP[4],AL            ; [CPU_] |288| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 298,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |298| 
        BF        $C$L54,EQ             ; [CPU_] |298| 
        ; branchcc occurs ; [] |298| 
        CMPB      AL,#127               ; [CPU_] |298| 
        B         $C$L54,HI             ; [CPU_] |298| 
        ; branchcc occurs ; [] |298| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 300,column 4,is_stmt
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 303,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |303| 
        BF        $C$L40,EQ             ; [CPU_] |303| 
        ; branchcc occurs ; [] |303| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 305,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |305| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |305| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |305| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |305| 
        MOVL      XAR5,*+XAR5[0]        ; [CPU_] |305| 
        LSL       ACC,2                 ; [CPU_] |305| 
        ADDL      XAR4,ACC              ; [CPU_] |305| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |305| 
        MOVZ      AR6,*+XAR5[0]         ; [CPU_] |305| 
        MOVB      XAR0,#12              ; [CPU_] |305| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |305| 
        ADD       AR6,#1536             ; [CPU_] |305| 
        MOVU      ACC,AR6               ; [CPU_] |305| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |305| 
        BF        $C$L37,EQ             ; [CPU_] |305| 
        ; branchcc occurs ; [] |305| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |305| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |305| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |305| 
        CMPB      AL,#255               ; [CPU_] |305| 
        BF        $C$L38,NEQ            ; [CPU_] |305| 
        ; branchcc occurs ; [] |305| 
$C$L37:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 307,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |307| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |307| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |307| 
        LSL       ACC,2                 ; [CPU_] |307| 
        ADDL      XAR4,ACC              ; [CPU_] |307| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |307| 
        MOVZ      AR6,*-SP[3]           ; [CPU_] |307| 
        MOVB      XAR0,#12              ; [CPU_] |307| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |307| 
        ADD       AR6,#1536             ; [CPU_] |307| 
        MOVU      ACC,AR6               ; [CPU_] |307| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |307| 
$C$L38:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 310,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |310| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |310| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |310| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |310| 
        MOVL      XAR5,*+XAR5[0]        ; [CPU_] |310| 
        LSL       ACC,2                 ; [CPU_] |310| 
        ADDL      XAR4,ACC              ; [CPU_] |310| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |310| 
        MOVZ      AR6,*+XAR5[0]         ; [CPU_] |310| 
        MOVB      XAR0,#20              ; [CPU_] |310| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |310| 
        ADD       AR6,#1408             ; [CPU_] |310| 
        MOVU      ACC,AR6               ; [CPU_] |310| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |310| 
        BF        $C$L39,EQ             ; [CPU_] |310| 
        ; branchcc occurs ; [] |310| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |310| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |310| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |310| 
        CMPB      AL,#255               ; [CPU_] |310| 
        BF        $C$L40,NEQ            ; [CPU_] |310| 
        ; branchcc occurs ; [] |310| 
$C$L39:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 312,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |312| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |312| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |312| 
        LSL       ACC,2                 ; [CPU_] |312| 
        ADDL      XAR4,ACC              ; [CPU_] |312| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |312| 
        MOVZ      AR6,*-SP[3]           ; [CPU_] |312| 
        MOVB      XAR0,#20              ; [CPU_] |312| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |312| 
        ADD       AR6,#1408             ; [CPU_] |312| 
        MOVU      ACC,AR6               ; [CPU_] |312| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |312| 
$C$L40:    

$C$DW$131	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$132	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$132, DW_AT_location[DW_OP_breg20 -5]
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_breg20 -6]
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("lastIndex")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_breg20 -7]
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("cobID")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_cobID")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_breg20 -16]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 325,column 12,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |325| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 326,column 18,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |326| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |326| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |326| 
        MOV       *-SP[6],AL            ; [CPU_] |326| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 327,column 21,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |327| 
        MOVB      XAR0,#8               ; [CPU_] |327| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |327| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |327| 
        MOV       *-SP[7],AL            ; [CPU_] |327| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 328,column 11,is_stmt
        MOVL      XAR7,#_$P$T4$5        ; [CPU_U] |328| 
        MOVZ      AR4,SP                ; [CPU_U] |328| 
        SUBB      XAR4,#16              ; [CPU_U] |328| 
        RPT       #7
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |328| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 329,column 5,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |329| 
        BF        $C$L45,EQ             ; [CPU_] |329| 
        ; branchcc occurs ; [] |329| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 329,column 18,is_stmt
        B         $C$L44,UNC            ; [CPU_] |329| 
        ; branch occurs ; [] |329| 
$C$L41:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 330,column 7,is_stmt
        MOVL      XAR5,*-SP[2]          ; [CPU_] |330| 
        MOVL      XAR5,*+XAR5[0]        ; [CPU_] |330| 
        MOVZ      AR4,SP                ; [CPU_U] |330| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |330| 
        SUBB      XAR4,#16              ; [CPU_U] |330| 
        MOVZ      AR6,*+XAR5[0]         ; [CPU_] |330| 
        LSL       ACC,1                 ; [CPU_] |330| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |330| 
        ADDL      XAR4,ACC              ; [CPU_] |330| 
        MOVL      ACC,XAR6              ; [CPU_] |330| 
        ADDL      ACC,*+XAR4[0]         ; [CPU_] |330| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |330| 
        MOVL      XAR6,ACC              ; [CPU_] |330| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |330| 
        LSL       ACC,2                 ; [CPU_] |330| 
        ADDL      XAR4,ACC              ; [CPU_] |330| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |330| 
        MOVB      XAR0,#12              ; [CPU_] |330| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |330| 
        MOVL      ACC,XAR6              ; [CPU_] |330| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |330| 
        BF        $C$L42,EQ             ; [CPU_] |330| 
        ; branchcc occurs ; [] |330| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |330| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |330| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |330| 
        CMPB      AL,#255               ; [CPU_] |330| 
        BF        $C$L43,NEQ            ; [CPU_] |330| 
        ; branchcc occurs ; [] |330| 
$C$L42:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 331,column 8,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |331| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |331| 
        MOVZ      AR6,*-SP[3]           ; [CPU_] |331| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |331| 
        SUBB      XAR4,#16              ; [CPU_U] |331| 
        LSL       ACC,1                 ; [CPU_] |331| 
        ADDL      XAR4,ACC              ; [CPU_] |331| 
        MOVL      ACC,XAR6              ; [CPU_] |331| 
        ADDL      ACC,*+XAR4[0]         ; [CPU_] |331| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |331| 
        MOVL      XAR6,ACC              ; [CPU_] |331| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |331| 
        LSL       ACC,2                 ; [CPU_] |331| 
        ADDL      XAR4,ACC              ; [CPU_] |331| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |331| 
        MOVB      XAR0,#12              ; [CPU_] |331| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |331| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |331| 
$C$L43:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 332,column 7,is_stmt
        INC       *-SP[5]               ; [CPU_] |332| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 333,column 7,is_stmt
        INC       *-SP[6]               ; [CPU_] |333| 
$C$L44:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 329,column 25,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |329| 
        CMP       AL,*-SP[6]            ; [CPU_] |329| 
        B         $C$L45,LO             ; [CPU_] |329| 
        ; branchcc occurs ; [] |329| 
        MOV       AL,*-SP[5]            ; [CPU_] |329| 
        CMPB      AL,#4                 ; [CPU_] |329| 
        B         $C$L41,LO             ; [CPU_] |329| 
        ; branchcc occurs ; [] |329| 
$C$L45:    
	.dwendtag $C$DW$131


$C$DW$136	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_breg20 -5]
$C$DW$138	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$138, DW_AT_location[DW_OP_breg20 -6]
$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("lastIndex")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$139, DW_AT_location[DW_OP_breg20 -7]
$C$DW$140	.dwtag  DW_TAG_variable, DW_AT_name("cobID")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_cobID")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$140, DW_AT_location[DW_OP_breg20 -16]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 338,column 12,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |338| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 339,column 18,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |339| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |339| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |339| 
        MOV       *-SP[6],AL            ; [CPU_] |339| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 340,column 21,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |340| 
        MOVB      XAR0,#8               ; [CPU_] |340| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |340| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |340| 
        MOV       *-SP[7],AL            ; [CPU_] |340| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 341,column 11,is_stmt
        MOVL      XAR7,#_$P$T5$6        ; [CPU_U] |341| 
        MOVZ      AR4,SP                ; [CPU_U] |341| 
        SUBB      XAR4,#16              ; [CPU_U] |341| 
        RPT       #7
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |341| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 342,column 5,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |342| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 343,column 5,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |343| 
        BF        $C$L51,EQ             ; [CPU_] |343| 
        ; branchcc occurs ; [] |343| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 343,column 18,is_stmt
        MOV       T,#14                 ; [CPU_] |346| 
        B         $C$L50,UNC            ; [CPU_] |343| 
        ; branch occurs ; [] |343| 
$C$L46:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 344,column 7,is_stmt
        MOVL      XAR5,*-SP[2]          ; [CPU_] |344| 
        MOVL      XAR5,*+XAR5[0]        ; [CPU_] |344| 
        MOVZ      AR4,SP                ; [CPU_U] |344| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |344| 
        SUBB      XAR4,#16              ; [CPU_U] |344| 
        MOVZ      AR6,*+XAR5[0]         ; [CPU_] |344| 
        LSL       ACC,1                 ; [CPU_] |344| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |344| 
        ADDL      XAR4,ACC              ; [CPU_] |344| 
        MOVL      ACC,XAR6              ; [CPU_] |344| 
        ADDL      ACC,*+XAR4[0]         ; [CPU_] |344| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |344| 
        MOVL      XAR6,ACC              ; [CPU_] |344| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |344| 
        LSL       ACC,2                 ; [CPU_] |344| 
        ADDL      XAR4,ACC              ; [CPU_] |344| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |344| 
        MOVB      XAR0,#12              ; [CPU_] |344| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |344| 
        MOVL      ACC,XAR6              ; [CPU_] |344| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |344| 
        BF        $C$L47,EQ             ; [CPU_] |344| 
        ; branchcc occurs ; [] |344| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |344| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |344| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |344| 
        CMPB      AL,#255               ; [CPU_] |344| 
        BF        $C$L48,NEQ            ; [CPU_] |344| 
        ; branchcc occurs ; [] |344| 
$C$L47:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 345,column 8,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |345| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |345| 
        MOVZ      AR6,*-SP[3]           ; [CPU_] |345| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |345| 
        SUBB      XAR4,#16              ; [CPU_U] |345| 
        LSL       ACC,1                 ; [CPU_] |345| 
        ADDL      XAR4,ACC              ; [CPU_] |345| 
        MOVL      ACC,XAR6              ; [CPU_] |345| 
        ADDL      ACC,*+XAR4[0]         ; [CPU_] |345| 
        MOVL      XAR4,*+XAR5[2]        ; [CPU_] |345| 
        MOVL      XAR6,ACC              ; [CPU_] |345| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |345| 
        LSL       ACC,2                 ; [CPU_] |345| 
        ADDL      XAR4,ACC              ; [CPU_] |345| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |345| 
        MOVB      XAR0,#12              ; [CPU_] |345| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |345| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |345| 
$C$L48:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 346,column 7,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |346| 
        CMPB      AL,#4                 ; [CPU_] |346| 
        B         $C$L49,HIS            ; [CPU_] |346| 
        ; branchcc occurs ; [] |346| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 346,column 18,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |346| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |346| 
        MPYXU     ACC,T,*-SP[5]         ; [CPU_] |346| 
        ADDL      XAR4,ACC              ; [CPU_] |346| 
        MOV       AL,*-SP[3]            ; [CPU_] |346| 
        ADDB      AL,#-1                ; [CPU_] |346| 
        ANDB      AL,#0x1f              ; [CPU_] |346| 
        MOV       *+XAR4[0],AL          ; [CPU_] |346| 
$C$L49:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 347,column 7,is_stmt
        INC       *-SP[5]               ; [CPU_] |347| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 348,column 7,is_stmt
        INC       *-SP[6]               ; [CPU_] |348| 
$C$L50:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 343,column 25,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |343| 
        CMP       AL,*-SP[6]            ; [CPU_] |343| 
        B         $C$L51,LO             ; [CPU_] |343| 
        ; branchcc occurs ; [] |343| 
        MOV       AL,*-SP[5]            ; [CPU_] |343| 
        CMPB      AL,#4                 ; [CPU_] |343| 
        B         $C$L46,LO             ; [CPU_] |343| 
        ; branchcc occurs ; [] |343| 
$C$L51:    
	.dwendtag $C$DW$136

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 353,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |353| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |353| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |353| 
        MOVL      XAR0,#272             ; [CPU_] |353| 
        MOVL      XAR5,*+XAR5[AR0]      ; [CPU_] |353| 
        MOVB      AL,#128               ; [CPU_] |353| 
        ADD       AL,*+XAR4[0]          ; [CPU_] |353| 
        MOVU      ACC,AL                ; [CPU_] |353| 
        CMPL      ACC,*+XAR5[0]         ; [CPU_] |353| 
        BF        $C$L52,EQ             ; [CPU_] |353| 
        ; branchcc occurs ; [] |353| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |353| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |353| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |353| 
        CMPB      AL,#255               ; [CPU_] |353| 
        BF        $C$L53,NEQ            ; [CPU_] |353| 
        ; branchcc occurs ; [] |353| 
$C$L52:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 354,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |354| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |354| 
        MOVB      AL,#128               ; [CPU_] |354| 
        ADD       AL,*-SP[3]            ; [CPU_] |354| 
        MOVU      ACC,AL                ; [CPU_] |354| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |354| 
$C$L53:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 357,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |357| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |357| 
        MOV       AL,*-SP[3]            ; [CPU_] |357| 
        MOV       *+XAR4[0],AL          ; [CPU_] |357| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 358,column 1,is_stmt
$C$L54:    
        SUBB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$141	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$141, DW_AT_low_pc(0x00)
	.dwattr $C$DW$141, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$125, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$125, DW_AT_TI_end_line(0x166)
	.dwattr $C$DW$125, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$125

	.sect	".text"
	.global	__initialisation

$C$DW$142	.dwtag  DW_TAG_subprogram, DW_AT_name("_initialisation")
	.dwattr $C$DW$142, DW_AT_low_pc(__initialisation)
	.dwattr $C$DW$142, DW_AT_high_pc(0x00)
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("__initialisation")
	.dwattr $C$DW$142, DW_AT_external
	.dwattr $C$DW$142, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$142, DW_AT_TI_begin_line(0x168)
	.dwattr $C$DW$142, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$142, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 360,column 33,is_stmt,address __initialisation

	.dwfde $C$DW$CIE, __initialisation
$C$DW$143	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: __initialisation              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

__initialisation:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$144	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$144, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |360| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 360,column 34,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$145	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$145, DW_AT_low_pc(0x00)
	.dwattr $C$DW$145, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$142, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$142, DW_AT_TI_end_line(0x168)
	.dwattr $C$DW$142, DW_AT_TI_end_column(0x22)
	.dwendentry
	.dwendtag $C$DW$142

	.sect	".text"
	.global	__preOperational

$C$DW$146	.dwtag  DW_TAG_subprogram, DW_AT_name("_preOperational")
	.dwattr $C$DW$146, DW_AT_low_pc(__preOperational)
	.dwattr $C$DW$146, DW_AT_high_pc(0x00)
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("__preOperational")
	.dwattr $C$DW$146, DW_AT_external
	.dwattr $C$DW$146, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$146, DW_AT_TI_begin_line(0x169)
	.dwattr $C$DW$146, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$146, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 361,column 33,is_stmt,address __preOperational

	.dwfde $C$DW$CIE, __preOperational
$C$DW$147	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: __preOperational              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

__preOperational:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$148	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$148, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |361| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 361,column 34,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$149	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$149, DW_AT_low_pc(0x00)
	.dwattr $C$DW$149, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$146, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$146, DW_AT_TI_end_line(0x169)
	.dwattr $C$DW$146, DW_AT_TI_end_column(0x22)
	.dwendentry
	.dwendtag $C$DW$146

	.sect	".text"
	.global	__operational

$C$DW$150	.dwtag  DW_TAG_subprogram, DW_AT_name("_operational")
	.dwattr $C$DW$150, DW_AT_low_pc(__operational)
	.dwattr $C$DW$150, DW_AT_high_pc(0x00)
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("__operational")
	.dwattr $C$DW$150, DW_AT_external
	.dwattr $C$DW$150, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$150, DW_AT_TI_begin_line(0x16a)
	.dwattr $C$DW$150, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$150, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 362,column 30,is_stmt,address __operational

	.dwfde $C$DW$CIE, __operational
$C$DW$151	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$151, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: __operational                 FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

__operational:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$152	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$152, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |362| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 362,column 31,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$153	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$153, DW_AT_low_pc(0x00)
	.dwattr $C$DW$153, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$150, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$150, DW_AT_TI_end_line(0x16a)
	.dwattr $C$DW$150, DW_AT_TI_end_column(0x1f)
	.dwendentry
	.dwendtag $C$DW$150

	.sect	".text"
	.global	__stopped

$C$DW$154	.dwtag  DW_TAG_subprogram, DW_AT_name("_stopped")
	.dwattr $C$DW$154, DW_AT_low_pc(__stopped)
	.dwattr $C$DW$154, DW_AT_high_pc(0x00)
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("__stopped")
	.dwattr $C$DW$154, DW_AT_external
	.dwattr $C$DW$154, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$154, DW_AT_TI_begin_line(0x16b)
	.dwattr $C$DW$154, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$154, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 363,column 26,is_stmt,address __stopped

	.dwfde $C$DW$CIE, __stopped
$C$DW$155	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$155, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: __stopped                     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

__stopped:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$156	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$156, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |363| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c",line 363,column 27,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$157	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$157, DW_AT_low_pc(0x00)
	.dwattr $C$DW$157, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$154, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/R_Module_48V_Standard/common/Festival/states.c")
	.dwattr $C$DW$154, DW_AT_TI_end_line(0x16b)
	.dwattr $C$DW$154, DW_AT_TI_end_column(0x1b)
	.dwendentry
	.dwendtag $C$DW$154

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_proceedNMTstateChange
	.global	_proceedNODE_GUARD
	.global	_heartbeatInit
	.global	_heartbeatStop
	.global	_proceedEMCY
	.global	_PDOStop
	.global	_startSYNC
	.global	_resetSDO
	.global	_PDOInit
	.global	_emergencyInit
	.global	_emergencyStop
	.global	_stopSYNC
	.global	_ODV_Module1_Alive_Counter
	.global	_proceedSYNC
	.global	_masterSendNMTstateChange
	.global	_proceedSDO
	.global	_proceedPDO
	.global	_slaveSendBootUp
	.global	_ODV_Gateway_State
	.global	_MBX_post
	.global	_ODV_SysTick_ms
	.global	_BoardODdata
	.global	_ODP_Board_RevisionNumber
	.global	_usb_tx_mbox

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$158, DW_AT_name("cob_id")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$159, DW_AT_name("rtr")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$160, DW_AT_name("len")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$161, DW_AT_name("data")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)
$C$DW$T$116	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$116, DW_AT_address_class(0x16)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$162	.dwtag  DW_TAG_member
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$162, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$162, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$163, DW_AT_name("csSDO")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$164, DW_AT_name("csEmergency")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$165, DW_AT_name("csSYNC")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$166, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$167, DW_AT_name("csPDO")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$168, DW_AT_name("csLSS")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$168, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)
$C$DW$T$117	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$117, DW_AT_address_class(0x16)
$C$DW$169	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$64)
$C$DW$T$118	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$169)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$170, DW_AT_name("errCode")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$171, DW_AT_name("errRegMask")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$172, DW_AT_name("active")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)

$C$DW$T$99	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x18)
$C$DW$173	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$173, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$99


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$174, DW_AT_name("index")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$175, DW_AT_name("subindex")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$176, DW_AT_name("size")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$177, DW_AT_name("address")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)

$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x08)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$178, DW_AT_name("wListElem")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$179, DW_AT_name("wCount")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$180, DW_AT_name("fxn")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31

$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$26	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$26, DW_AT_address_class(0x16)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)

$C$DW$T$38	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$38, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x30)
$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$181, DW_AT_name("dataQue")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$182, DW_AT_name("freeQue")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$183, DW_AT_name("dataSem")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$184, DW_AT_name("freeSem")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$185, DW_AT_name("segid")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$186, DW_AT_name("size")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$187, DW_AT_name("length")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$188, DW_AT_name("name")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38

$C$DW$T$119	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$119, DW_AT_language(DW_LANG_C)
$C$DW$T$121	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$121, DW_AT_address_class(0x16)
$C$DW$T$122	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)

$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x04)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$189, DW_AT_name("next")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$190, DW_AT_name("prev")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x16)

$C$DW$T$42	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$42, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x10)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$191, DW_AT_name("job")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$192, DW_AT_name("count")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$193	.dwtag  DW_TAG_member
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$193, DW_AT_name("pendQ")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$193, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$193, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$194, DW_AT_name("name")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$123	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)

$C$DW$T$28	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$195	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$27)
	.dwendtag $C$DW$T$28

$C$DW$T$29	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_address_class(0x16)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)

$C$DW$T$67	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$196	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$T$67

$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x16)
$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$T$81	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("post_sync_t")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)

$C$DW$T$76	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$197	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$66)
$C$DW$198	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$76

$C$DW$T$77	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x16)
$C$DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$T$82	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)

$C$DW$T$100	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$199	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$66)
$C$DW$200	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$6)
$C$DW$201	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$9)
$C$DW$202	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$100

$C$DW$T$101	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_address_class(0x16)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$203	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$203, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$204	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$6)
$C$DW$T$55	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$204)
$C$DW$T$56	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$T$56, DW_AT_address_class(0x16)
$C$DW$T$103	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$139	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$139, DW_AT_language(DW_LANG_C)
$C$DW$205	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$9)
$C$DW$T$53	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$205)
$C$DW$T$54	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x16)
$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$57	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$206	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$6)
$C$DW$207	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$57

$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x16)

$C$DW$T$84	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$208	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$66)
$C$DW$209	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$47)
$C$DW$210	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$6)
$C$DW$211	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$84

$C$DW$T$85	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_address_class(0x16)
$C$DW$T$86	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_language(DW_LANG_C)
$C$DW$212	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$86)
$C$DW$T$87	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$212)
$C$DW$T$88	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$88, DW_AT_address_class(0x16)
$C$DW$T$89	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_address_class(0x16)

$C$DW$T$93	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)
$C$DW$213	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$66)
$C$DW$214	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$9)
$C$DW$215	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$93

$C$DW$T$94	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_address_class(0x16)
$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)

$C$DW$T$142	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$142, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$142, DW_AT_byte_size(0x08)
$C$DW$216	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$216, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$142

$C$DW$217	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$13)
$C$DW$T$143	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$217)

$C$DW$T$144	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$T$144, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$144, DW_AT_byte_size(0x08)
$C$DW$218	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$218, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$144

$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x16)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)

$C$DW$T$96	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$96, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x01)
$C$DW$219	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$220	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$96

$C$DW$T$97	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)

$C$DW$T$62	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$62, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)
$C$DW$221	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$222	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$223	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$224	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$225	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$226	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$227	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$228	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$62

$C$DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)

$C$DW$T$79	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$79, DW_AT_byte_size(0x80)
$C$DW$229	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$229, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$79


$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x06)
$C$DW$230	.dwtag  DW_TAG_member
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$230, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$230, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$231, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$231, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$232	.dwtag  DW_TAG_member
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$232, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$232, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$233, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$234	.dwtag  DW_TAG_member
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$234, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$234, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$235, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$43

$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$236	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$50)
$C$DW$T$51	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$236)
$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x16)

$C$DW$T$106	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$106, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x132)
$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$237, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$238, DW_AT_name("objdict")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$239, DW_AT_name("PDO_status")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$240	.dwtag  DW_TAG_member
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$240, DW_AT_name("firstIndex")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$240, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$240, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$241, DW_AT_name("lastIndex")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$242, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$243, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$244, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$245, DW_AT_name("transfers")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$246, DW_AT_name("nodeState")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$247, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$248, DW_AT_name("initialisation")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$249, DW_AT_name("preOperational")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$250, DW_AT_name("operational")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$251, DW_AT_name("stopped")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$252, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$253, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$254, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$255, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$256, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$257, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$258, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$259, DW_AT_name("heartbeatError")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$260, DW_AT_name("NMTable")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$261, DW_AT_name("syncTimer")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$262	.dwtag  DW_TAG_member
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$262, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$262, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$262, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$263, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$264, DW_AT_name("post_sync")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_post_sync")
	.dwattr $C$DW$264, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$265, DW_AT_name("post_TPDO")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$266	.dwtag  DW_TAG_member
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$266, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$266, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$266, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$267	.dwtag  DW_TAG_member
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$267, DW_AT_name("toggle")
	.dwattr $C$DW$267, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$267, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$267, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$268	.dwtag  DW_TAG_member
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$268, DW_AT_name("canHandle")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$268, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$268, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$269	.dwtag  DW_TAG_member
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$269, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$269, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$269, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$270, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$270, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$271	.dwtag  DW_TAG_member
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$271, DW_AT_name("globalCallback")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$271, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$272	.dwtag  DW_TAG_member
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$272, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$272, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$272, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$273	.dwtag  DW_TAG_member
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$273, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$273, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$274	.dwtag  DW_TAG_member
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$274, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$274, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$274, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$275	.dwtag  DW_TAG_member
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$275, DW_AT_name("dcf_request")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$275, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$275, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$276	.dwtag  DW_TAG_member
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$276, DW_AT_name("error_state")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$276, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$276, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$277	.dwtag  DW_TAG_member
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$277, DW_AT_name("error_history_size")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$277, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$277, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$278	.dwtag  DW_TAG_member
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$278, DW_AT_name("error_number")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$278, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$278, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$279	.dwtag  DW_TAG_member
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$279, DW_AT_name("error_first_element")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$279, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$279, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$280	.dwtag  DW_TAG_member
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$280, DW_AT_name("error_register")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$280, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$280, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$281	.dwtag  DW_TAG_member
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$281, DW_AT_name("error_cobid")
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$281, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$281, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$282	.dwtag  DW_TAG_member
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$282, DW_AT_name("error_data")
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$282, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$282, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$283	.dwtag  DW_TAG_member
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$283, DW_AT_name("post_emcy")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$283, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$283, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$284	.dwtag  DW_TAG_member
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$284, DW_AT_name("lss_transfer")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$284, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$284, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$285	.dwtag  DW_TAG_member
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$285, DW_AT_name("eeprom_index")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$285, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$285, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$286	.dwtag  DW_TAG_member
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$286, DW_AT_name("eeprom_size")
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$286, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$286, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$106

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x16)

$C$DW$T$108	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$108, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x0e)
$C$DW$287	.dwtag  DW_TAG_member
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$287, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$287, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$287, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$288	.dwtag  DW_TAG_member
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$288, DW_AT_name("event_timer")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$288, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$288, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$289, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$290, DW_AT_name("last_message")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$108

$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$49	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x16)

$C$DW$T$110	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$110, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x14)
$C$DW$291	.dwtag  DW_TAG_member
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$291, DW_AT_name("nodeId")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$291, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$292	.dwtag  DW_TAG_member
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$292, DW_AT_name("whoami")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$292, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$292, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$293	.dwtag  DW_TAG_member
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$293, DW_AT_name("state")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$293, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$294, DW_AT_name("toggle")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$295	.dwtag  DW_TAG_member
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$295, DW_AT_name("abortCode")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$295, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$295, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$296	.dwtag  DW_TAG_member
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$296, DW_AT_name("index")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$296, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$296, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$297, DW_AT_name("subIndex")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$297, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$298	.dwtag  DW_TAG_member
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$298, DW_AT_name("port")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$298, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$299	.dwtag  DW_TAG_member
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$299, DW_AT_name("count")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$299, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$299, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$300	.dwtag  DW_TAG_member
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$300, DW_AT_name("offset")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$300, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$301	.dwtag  DW_TAG_member
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$301, DW_AT_name("datap")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$301, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$301, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$302	.dwtag  DW_TAG_member
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$302, DW_AT_name("dataType")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$302, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$302, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$303	.dwtag  DW_TAG_member
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$303, DW_AT_name("timer")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$303, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$303, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$304, DW_AT_name("Callback")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$304, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$110

$C$DW$T$60	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)

$C$DW$T$61	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x3c)
$C$DW$305	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$305, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$61


$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x04)
$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$306, DW_AT_name("pSubindex")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$307, DW_AT_name("bSubCount")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$308, DW_AT_name("index")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114

$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$309	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$45)
$C$DW$T$46	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$309)
$C$DW$T$47	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x16)

$C$DW$T$90	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$310	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$9)
$C$DW$311	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$73)
$C$DW$312	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$89)
	.dwendtag $C$DW$T$90

$C$DW$T$91	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x16)
$C$DW$T$92	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)

$C$DW$T$115	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$115, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x08)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$313, DW_AT_name("bAccessType")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$314, DW_AT_name("bDataType")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$315, DW_AT_name("size")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$316, DW_AT_name("pObject")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$317, DW_AT_name("bProcessor")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$115

$C$DW$318	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$115)
$C$DW$T$111	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$318)
$C$DW$T$112	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$112, DW_AT_language(DW_LANG_C)
$C$DW$T$113	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$319	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$319, DW_AT_location[DW_OP_reg0]
$C$DW$320	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$320, DW_AT_location[DW_OP_reg1]
$C$DW$321	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$321, DW_AT_location[DW_OP_reg2]
$C$DW$322	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$322, DW_AT_location[DW_OP_reg3]
$C$DW$323	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$323, DW_AT_location[DW_OP_reg20]
$C$DW$324	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$324, DW_AT_location[DW_OP_reg21]
$C$DW$325	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$325, DW_AT_location[DW_OP_reg22]
$C$DW$326	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$326, DW_AT_location[DW_OP_reg23]
$C$DW$327	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$327, DW_AT_location[DW_OP_reg24]
$C$DW$328	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$328, DW_AT_location[DW_OP_reg25]
$C$DW$329	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$329, DW_AT_location[DW_OP_reg26]
$C$DW$330	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$330, DW_AT_location[DW_OP_reg28]
$C$DW$331	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$331, DW_AT_location[DW_OP_reg29]
$C$DW$332	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$332, DW_AT_location[DW_OP_reg30]
$C$DW$333	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$333, DW_AT_location[DW_OP_reg31]
$C$DW$334	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$334, DW_AT_location[DW_OP_regx 0x20]
$C$DW$335	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$335, DW_AT_location[DW_OP_regx 0x21]
$C$DW$336	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$336, DW_AT_location[DW_OP_regx 0x22]
$C$DW$337	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$337, DW_AT_location[DW_OP_regx 0x23]
$C$DW$338	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$338, DW_AT_location[DW_OP_regx 0x24]
$C$DW$339	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$339, DW_AT_location[DW_OP_regx 0x25]
$C$DW$340	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$340, DW_AT_location[DW_OP_regx 0x26]
$C$DW$341	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$341, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$342	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$342, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$343	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$343, DW_AT_location[DW_OP_reg4]
$C$DW$344	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$344, DW_AT_location[DW_OP_reg6]
$C$DW$345	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$345, DW_AT_location[DW_OP_reg8]
$C$DW$346	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$346, DW_AT_location[DW_OP_reg10]
$C$DW$347	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$347, DW_AT_location[DW_OP_reg12]
$C$DW$348	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$348, DW_AT_location[DW_OP_reg14]
$C$DW$349	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$349, DW_AT_location[DW_OP_reg16]
$C$DW$350	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$350, DW_AT_location[DW_OP_reg17]
$C$DW$351	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$351, DW_AT_location[DW_OP_reg18]
$C$DW$352	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$352, DW_AT_location[DW_OP_reg19]
$C$DW$353	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$353, DW_AT_location[DW_OP_reg5]
$C$DW$354	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$354, DW_AT_location[DW_OP_reg7]
$C$DW$355	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$355, DW_AT_location[DW_OP_reg9]
$C$DW$356	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$356, DW_AT_location[DW_OP_reg11]
$C$DW$357	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$357, DW_AT_location[DW_OP_reg13]
$C$DW$358	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$358, DW_AT_location[DW_OP_reg15]
$C$DW$359	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$359, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$360	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$360, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$361	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$361, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$362	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$362, DW_AT_location[DW_OP_regx 0x30]
$C$DW$363	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$363, DW_AT_location[DW_OP_regx 0x33]
$C$DW$364	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$364, DW_AT_location[DW_OP_regx 0x34]
$C$DW$365	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$365, DW_AT_location[DW_OP_regx 0x37]
$C$DW$366	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$366, DW_AT_location[DW_OP_regx 0x38]
$C$DW$367	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$367, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$368	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$368, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$369	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$369, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$370	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$370, DW_AT_location[DW_OP_regx 0x40]
$C$DW$371	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$371, DW_AT_location[DW_OP_regx 0x43]
$C$DW$372	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$372, DW_AT_location[DW_OP_regx 0x44]
$C$DW$373	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$373, DW_AT_location[DW_OP_regx 0x47]
$C$DW$374	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$374, DW_AT_location[DW_OP_regx 0x48]
$C$DW$375	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$375, DW_AT_location[DW_OP_regx 0x49]
$C$DW$376	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$376, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$377	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$377, DW_AT_location[DW_OP_regx 0x27]
$C$DW$378	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$378, DW_AT_location[DW_OP_regx 0x28]
$C$DW$379	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$379, DW_AT_location[DW_OP_reg27]
$C$DW$380	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$380, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

