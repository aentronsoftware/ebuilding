

#include "mmscfg.h"
#include "uc.h"
#include "dspspecs.h"
#include "ngtestspec.h"
#include "hal.h"


const TTempTable TempTable[TEMP_VAL_NB] =
{//NTC R/T=8016 EPCOS B57703M0103G040 10k, Semitec NTC103AT-11
//T[�C],AD
 {-55,{4095.9,4095.9}}//in order to disable the under temperature limit
,{-50,{4034.8,3974.4}}
,{-45,{4010.0,3936.1}}
,{-40,{3976.8,3888.7}}
,{-35,{3932.9,3829.3}}
,{-30,{3876.0,3757.4}}
,{-25,{3803.3,3670.3}}
,{-20,{3712.5,3568.4}}
,{-15,{3601.2,3449.2}}
,{-10,{3468.2,3314.6}}
,{-05,{3312.3,3162.2}}
,{00,{3134.9,2996.6}}
,{05,{2937.9,2817.3}}
,{10,{2725.4,2630.4}}
,{15,{2502.2,2436.4}}
,{20,{2274.2,2241.2}}
,{25,{2047.5,2047.5}}
,{30,{1827.2,1858.9}}
,{35,{1617.8,1677.6}}
,{40,{1423.2,1507.6}}
,{45,{1245.1,1348.7}}
,{50,{1084.6,1203.1}}
,{55,{941.6,1069.7 }}
,{60,{815.9,949.8  }}
,{65,{705.9,841.9  }}
,{70,{610.5,746.1  }}
,{75,{528.2,660.7  }}
,{80,{457.6,585.4  }}
,{85,{396.5,518.9  }}
,{90,{344.2,460.2  }}
,{95,{299.3,408.5  }}
,{100,{260.7,363.1  }}
,{105,{227.6,323.3  }}
,{110,{199.2,288.4  }}
,{115,{174.6,254.0  }}
,{120,{153.4,225.0  }}
,{125,{0.0,0.0  }}
};

