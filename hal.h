/*
 * hal.h
 *
 *  Created on: 8 mai 2013
 *      Author: remip
 */

#ifndef HAL_H_
#define HAL_H_

//generic
#define HAL_OUT1  1
#define HAL_OUT2  2
#define HAL_OUT3  3
#define HAL_OUT4  4
#define HAL_OUT5  5
#define HAL_OUT6  6

#define HAL_OUT_MAX 6

#define HAL_IN1   17
#define HAL_IN2   18
#define HAL_IN3   19
#define HAL_IN4   20

//defines
#define HAL_ON  1
#define HAL_OFF 0

#define MEASURE_CELL1           0
#define MEASURE_CELL2           1
#define MEASURE_CELL3           2
#define MEASURE_CELL4           3
#define MEASURE_CELL5           4
#define MEASURE_CELL6           5
#define MEASURE_CELL7           6
#define MEASURE_CELL8           7
#define MEASURE_CELL9           8
#define MEASURE_CELL10          9
#define MEASURE_CELL11          10
#define MEASURE_CELL12          11
#define MEASURE_CELL13          12
#define MEASURE_CELL14          13
#define PIC_MEASURE_TEMP2       21  //aux0 = rv3 = balancing resistor
#define PIC_MEASURE_NTC3        20  //aux1 = NTC3
#define MEASURE_SHUNT           19  //aux2 = internal lem
#define PIC_MEASURE_FOIL        18  //aux3 = foil
#define PIC_MEASURE_TEMP1       17  //aux4 = rv2 = mosfet
#define PIC_MEASURE_HAS500      16  //aux5 = external lem
#define PIC_MEASURE_TEMP4       15  //aux6 = rv4 = cell temp2 NTC2 T4
#define PIC_MEASURE_TEMP3       14  //aux7 = rv1 = cell temp1 NTC1 T3
#define PIC_MEASURE_RELAY       22

#define PIC_MEASURE_LOCK_CUR  15
#define MEASURE_AUX0          21 //=temp1

#define PIC_MEASURE_MAX       15

typedef int16 TMeasure[PIC_MEASURE_MAX];


#define TEMP_VAL_NB   37
typedef struct{
  int16 temp;
  float adval[2];
}TTempTable;
extern const TTempTable TempTable[TEMP_VAL_NB];

typedef struct {
  uint16 can_wk:1;//0
  uint16 sw_wk:1;
  uint16 bal_dic:1;//2
  uint16 bal_ch:1;
  uint16 bal_ocv:1;//4
  uint16 lem:1;
  uint16 shunt:1;
  uint16 relay_on:1;//7
  uint16 ch_wk:1;
  uint16 SOC2:1;
  uint16 foil:1;
  uint16 rel_sta:1;
  uint16 b12:1;
  uint16 b13:1;
  uint16 b14:1;
  uint16 b15:1;
}TMMSConfig;
extern TMMSConfig* MMSConfig;

//relay handling
#define RELAY_ERROR   20
#define RELAY_TIMEOUT ODP_Contactor_Delay
#define RELAY_SEUILA  ODP_Contactor_Setup  //7.5V
#define RELAY_SEUILB  ODP_Contactor_Hold   //2.25V

#define RELAY_TIMEDELAY_STATE 16
#define RELAY_STILL_STATE     15

#define RELAY_BIT1  1
#define RELAY_BIT2  2
#define RELAY_BIT3  4

#define RELAY_VALUE1  EPwm2Regs.CMPA.half.CMPA
#define RELAY_VALUE2  EPwm3Regs.CMPA.half.CMPA
#define RELAY_VALUE3  EPwm3Regs.CMPB

#define LED_VALUE   GpioDataRegs.GPADAT.bit.GPIO24

//Definition for buffer.c
#define MEASBUF_SIZE  64
#define MEAS_CHANNELS  PIC_MEASURE_MAX

#define MOTOR1_INDEX 0
#define MOTOR2_INDEX 1

#define MOTOR_INDEX_MAX 2

//************ GATEWAY_STATE_XXX *************************
#define GATEWAY_STATE_NORMAL               1
#define GATEWAY_STATE_CHARGE_ONLY          2
#define GATEWAY_STATE_DISCHARGE_ONLY       4
#define GATEWAY_STATE_STILL                0
#define GATEWAY_STATE_CRASH                8

#define GATEWAY_STATE_ERROR_BIT         0x80
#define GATEWAY_STATE_WARN_BIT          0x40


#define GATEWAY_MODE_NOINI -1
#define GATEWAY_MODE_CRASH  8
#define GATEWAY_MODE_STILL  0
#define GATEWAY_MODE_DONLY  4
#define GATEWAY_MODE_CONLY  2
#define GATEWAY_MODE_NORM   1
#define GATEWAY_MODE_PRECH  3
#define GATEWAY_MODE_24ON   6
#define GATEWAY_MODE_CLR   16
#define GATEWAY_MODE_ERROR_IN 0x40
#define GATEWAY_MODE_ERROR  0x80


#define DRIVEABILITY_MODE_NONE     0
#define DRIVEABILITY_MODE_SWITCH1  1
#define DRIVEABILITY_MODE_SWITCH2  2

#define THROTTLE_INPUT_VOLTAGE_SPEED_LOW 100  //2600rpm*0.1 = 260rpm ->(/15) = 17.33 qc/s

//#define DECELERATE_TIMEOUT      1500 //10s
//#define DECELERATE_SPEED_MIN        6 //si POSITION_COUNTER_MAX = 200ms => 30qc/s

#define PARAM_INDEX_START 0x200A
#define PARAM_MAX         28

#define PIC_STATE_STATUS_BIT_PARAM_MODIFIED 1

#define P_RATIO    100
#define FLOW_RATIO 10

#define PO2_AIR 15922 //159.22*P_RATIO mmHg 20.95%*760mmHg
#define PATMO_STANDARD 76000 //760*P_RATIO mmHg

void HAL_Init(void);


#define BUTTON_MINUS      0 //Button-
#define BUTTON_PLUS       1 //Button+
#define BUTTON_V          2 //Button V
#define BUTTON_D          3 //Button D

#define BUTTON_NONE     255

#define BUTTON_MAX   4

#define ADS_AD_MAX      1500
#define ADS_ONE_POS     ADS_AD_MAX/10
#define ADS_HALF_VAL    ADS_AD_MAX/2
#define ADS_POTENTIOMETRE_VALUE
#define ADS_BUTTONV_VALUE
#define ADS_BUTTOND_VALUE

//*************** EVENTS **********************
#define EVENT_NONE               0
#define EVENT_INVERSE_FORWARD    1
#define EVENT_INVERSE_REVERSE    2
#define EVENT_STOP_MIN           3
#define EVENT_STOP_MAX           4
#define EVENT_UNDERCURRENT       5
#define EVENT_UNDERCURRENT_START 6
#define EVENT_UNDERCURRENT_TIME  7

//*************** INVERSE STATES *************
#define INVERSE_NONE      0
#define INVERSE_ENABLE    1
#define INVERSE_RUNNING   2


void HAL_ButtonStateMachine(int16 button_index);

#define NTC_TYPE0 0 //smd 0603
#define NTC_TYPE1 1 //wire ntc

#define PUSH_TYPE_SHORT   0
#define PUSH_TYPE_NORMAL  1
#define PUSH_TYPE_LONG    2
#define PUSH_TYPE_EXLONG  3
#define PUSH_TYPE_NONE  255

bool1 HAL_GetButton(uint8 *button_index,uint8 *push_type);

#define PDO_PERIOD 20 //ms
extern bool1 HAL_NewCurPoint;
extern int32 HAL_Position;
extern int32 HAL_LastPosition;
extern uint16 HAL_DoorClosed;
extern uint16 HAL_RelayState;
extern uint8 HAL_CellOK;
extern bool1 HAL_Enable;
extern uint32 PAR_Capacity_Left;
extern uint32 PAR_Capacity_Total;
extern int32 HAL_Current_Sum;
extern uint64 PAR_Capacity_TotalLife_Used;
extern int32 PAR_Operating_Hours_day;

void HAL_Unlock(void);
void HAL_Random(void);
void HAL_Reset(void);
void USB_Stop(void);
int16 HAL_LectureTemperature(float val, uint8 type);
uint16 HAL_LectureADTemperature(int16 vali, uint8 type);

#endif /* HAL_H_ */
